# 0.6.0 (2023-11-12)

- **[Breaking change]** Remove `eternalfest upgrade` command and drop support for legacy project manifests.
- **[Breaking change]** Remove `"locked"` state for game options.
- **[Change]** Deprecate `"disabled"` for game modes, `"hidden"` should be used instead.
- **[Feature]** Add new `"hidden"` state and new `default` boolean for game options.
- **[Feature]** Allow specifying whether the game should be pinned when scheduling an update in guided mode.
- **[Feature]** Full support for localizing game metadata (name, icon, modes & options, etc).
- **[Feature]** Allow overriding family list in profiles.
- **[Fix]** Update to `loader@5.1.2`, `efc@0.4.4`.

# 0.5.7 (2023-03-02)

- **[Change]** Update to `loader@5.1.1`.
- **[Change]** Update project template to new devkit/config format.
- **[Feature]** Include translation files when uploading games.
- **[Feature]** Add `login` command and local session support.
- **[Fix]** Fix publication date selection for updates.
- **[Fix]** Fix support for Node 14 & 15.
- **[Fix]** Update to `efc@0.4.3`.

# 0.5.2 (2023-02-10)

- **[Fix]** Fix broken patcher packages version resolution.

# 0.5.0 (2023-02-10)

- **[Breaking change]** Use Game API v2 when talking to the server.
- **[Breaking change]** Update devkit and remove obsolete tools.
- **[Breaking change]** Update to `efc@0.4.0`.
- **[Feature]** Allow the user to select the game locale.
- **[Change]** Improve error reporting.
- **[Fix]** Various fixes to project initialization.

# 0.4.4 (2022-01-13)

- **[Change]** Update `eternalfest init` to use `patchman@0.10.7` and Yarn 3.

# 0.4.2 (2021-10-06)

- **[Breaking change]** Authenticate using Eternaltwin credentials.
- **[Change]** Recommand `yarn up` on project init.

# 0.3.12 (2021-03-29)

- **[Fix]** Republished due to borked previous publish.

# 0.3.11 (2021-03-29)

- **[Fix]** Republished due to borked previous publish.

# 0.3.10 (2021-03-29)

- **[Breaking change]** Update to `api-core@0.15`.
- **[Fix]** Do not emit asset files in `eternalfest init` when assets are disabled.
- **[Fix]** Update dependencies.

# 0.3.9 (2020-06-01)

- **[Fix]** Update to Angular 10 (Release Candidate 2).

# 0.3.8 (2020-05-25)

- **[Fix]** Fix missing `.gitignore` in project template files.

# 0.3.7 (2020-05-25)

- **[Feature]** Implement `eternalfest init` subcommand.
- **[Fix]** Fix patcher data path support in `eternalfest upgrade`.

# 0.3.1 (2020-02-05)

- **[Fix]** Update dependencies.

# 0.3.0 (2020-01-29)

- **[Breaking change]** Update to `loader@4.0.0`.
- **[Breaking change]** Update to `efc@0.2`.
- **[Feature]** Add support for obfuscation.
- **[Fix]** Improve error messages on upload.

# 0.3.0-beta.1 (2020-01-28)

- **[Breaking change]** Update to `efc@0.2` preview.
- **[Feature]** Add `build` subcommand.
- **[Fix]** Fix `upgrade` issues.

# 0.3.0-beta.0 (2020-01-27)

- **[Breaking change]** Update manifest format.

# 0.2.44 (2020-01-01)

- **[Fix]** Update dependencies.

# 0.2.43 (2019-12-19)

- **[Fix]** Update loader: fix support for musics in non-obfuscated games.
- **[Fix]** Update to `api-core@0.12`.

# 0.2.42 (2019-12-04)

- **[Fix]** Update loader: fix obfuscation issues.

# 0.2.41 (2019-11-28)

- **[Fix]** Update loader: use obfuscation.

# 0.2.40 (2019-11-24)

- **[Feature]** Update efc: extend level set support.
- **[Fix]** Only display manifest warnings for project in current directory.

# 0.2.39 (2019-10-23)

- **[Fix]** Update to `api-core@0.11`.

# 0.2.38 (2019-09-05)

- **[Fix]** Update loader: fix level-set support for obfuscated games, add deprecation warnings
- **[Fix]** Update efc: Improve asset validation errors, add level set signatures

# 0.2.37 (2019-09-01)

- **[Fix]** Update dependencies.

# 0.2.36 (2019-08-23)

- **[Feature]** Add support for projects without a `game-swf` resource.
- **[Fix]** Update loader: fix game content injection into obfuscated games.

# 0.2.35 (2019-08-22)

- **[Feature]** Update to `api-core@0.10`: `RunStart` now has the user items.
- **[Feature]** Add support for profiles.

# 0.2.34 (2019-07-03)

- **[Feature]** Expose the whole devkit CLI: add `extractor`, `mapper` and `deobfuscator` subcommands.
- **[Fix]** Update dependencies

# 0.2.33 (2019-05-15)

- **[Feature]** Update to `api-core@0.9`.

# 0.2.32 (2019-05-15)

- **[Feature]** Add `--api` flag for the `upload` subcommand.
- **[Fix]** Update dependencies:
  - `@eternalfest/api-core$0.8.0`: Refactor file service.
  - `@eternalfest/devkit@1.0.19`: Fix SVG support.
  - `@eternalfest/efc@0.1.5`: Fix grid chunks.

# 0.2.31 (2019-04-29)

- **[Fix]** Update to Angular 8 and fix component style errors.
- **[Fix]** Update dependencies.

# 0.2.30 (2019-04-14)

- **[Fix]** Update loader.
- **[Fix]** Update devkit.

# 0.2.29 (2019-04-01)

- **[Fix]** Fix new game uploads.

# 0.2.28 (2019-04-01)

- **[Fix]** Fix project icon resolution.
- **[Fix]** Update devkit to version from 2019-03-29.

# 0.2.27 (2019-04-01)

- **[Fix]** Update icon with `eternalfest upload`.
- **[Fix]** Update dependencies.

# 0.2.26 (2019-03-05)

- **[Fix]** Fix game id used to send updates.

# 0.2.25 (2019-03-05)

- **[Fix]** Fix `remote_id` resolution.
- **[Fix]** Update devkit to version from 2019-03-05.

# 0.2.24 (2019-03-04)

- **[Feature]** Add `upload` subcommand.
- **[Fix]** Update dependencies.
- **[Internal]** Add description to `NotImplemented` errors.

# 0.2.23 (2019-02-05)

- **[Fix]** Update loader: fix `hud_label` duration and implement `print` for
  `SwfSocketConsoleModule`.
- **[Fix]** Update devkit.
- **[Fix]** Update efc: Fix dimension separators, improve error reporter

# 0.2.22 (2019-01-23)

- **[Fix]** Fix `efc` subcommand.
- **[Fix]** Update loader: fix background color and logo.
- **[Fix]** Update devkit.

# 0.2.21 (2019-01-08)

- **[Feature]** Add `efc` command.
- **[Fix]** Fix `--version` command.

# 0.2.20 (2019-01-03)

- **[Feature]** Add support for full run results.
- **[Fix]** Fix `Editor` and `Bundler` buttons.

# 0.2.19 (2018-12-31)

- **[Feature]** Add support for run start and run result.
- **[Fix]** Add mock user and session services.

# 0.2.18 (2018-12-23)

- **[Fix]** Display project names in the project list
- **[Fix]** Fix serving static files on Windows.

# 0.2.17 (2018-12-23)

- **[Fix]** Fix recent projects corruption.

# 0.2.16 (2018-12-23)

- **[Fix]** Drop dependency on `fs-extra`.

# 0.2.15 (2018-12-23)

- **[Fix]** Add missing `reflect-metadata` runtime dependency.

# 0.2.14 (2018-12-23)

- **[Fix]** Add missing `yargs` runtime dependency.

# 0.2.13 (2018-12-23)

- **[Feature]** Use the same REST API as `eternalfest.net`
- **[Fix]** Update to Angular 7

# 0.2.12 (2018-11-21)

- **[Fix]** Update devkit: fix fields serialization.

# 0.2.11 (2018-11-13)

- **[Fix]** Update bundler: fix generation of games with a `lang.xml`.

# 0.2.10 (2018-10-16)

- **[Fix]** Update editor: finish fixing missing enemies when loading a level file.

# 0.2.9 (2018-10-16)

- **[Fix]** Update editor: fix missing enemies when loading a level file.

# 0.2.8 (2018-10-12)

- **[Feature]** Update editor: better error messages and custom `assets` support
- **[Internal]** Remove `sous-la-colline` example.

# 0.2.7 (2018-09-05)

- **[Feature]** Support passing CLI arguments to `editor` and `bundler` subcommands.
- **[Internal]** Update TS options (ESM compatibility).

# 0.2.6 (2018-09-04)

- **[Fix]** Update `@eternalfest/loader`, support external 2 mods relying on local dev mode.

# 0.2.5 (2018-09-04)

- **[Fix]** Fix app data directory resolution on Windows
- **[Fix]** Fix `contree-commune` and `immeuble-siman` examples.

# 0.2.4 (2017-07-23)

- **[Feature]** Add interactive console component to report logs.
- **[Feature]** Add support for bidirectional communication between Flash and Javascript (SWF Socket).
- **[Feature]** Implement Flash debugger over SWF sockets.
- **[Feature]** Enable launcher buttons to start the level editor and game bundler.

# 0.2.3 (2017-07-02)

- **[Feature]** Add menu to set the run options before testing a project
- **[Feature]** Add support for local connection with the SWF files. This is used to
  redirect the logs of the engine patcher to the browser console.

# 0.2.2 (2017-06-26)

- **[Internal]** Remove `external` from this repo. The new repo
  is <http://gitlab.com/eternalfest/external>.
- **[Internal]** Split the Angular app and the server backend builds. This allows to add support
  for internationalization and fixes a few issues with external node modules.
- **[Fix]** Fix the error about missing `System` (when performing server-side rendering).
- **[Fix]** Do not cache project configuration, especially when refreshing its page.
- **[Fix]** Update loader to `3.0.0-alpha.4`. This fixes support for content-patched deobfuscated games.

# 0.2.1 (2017-06-22)

- **[Feature]** Update loader to `3.0.0-alpha.3`. This adds support for engine patchers v3.0.0.
- **[Feature]** Add support for absolute resource paths.

# 0.2.0 (2017-06-21)

- **[Feature]** Add support for Command Line Interface (CLI)
- **[Feature]** Add support for engine patcher (external 1 and external 2)
- **[Feature]** Remember recent projects
- **[Feature]** Switch to Angular application for the interface

# 0.1.0 (2017-06-15)

- **[Feature]** Compile and run `sous-la-colline` with the Gradle Devkit, Haxe Loader
  and HTTP-based Eternaldev.
- **[Internal]** Create CHANGELOG.md
