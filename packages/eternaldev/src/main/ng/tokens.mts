import {InjectionToken} from "@angular/core";
import {AuthContext} from "@eternalfest/core/auth/auth-context";

import {Api as EfApi} from "../rest/index.mjs";

export const AUTH_CONTEXT: InjectionToken<AuthContext> = new InjectionToken("AuthContext");

export const EF_API: InjectionToken<EfApi> = new InjectionToken("EfApi");
