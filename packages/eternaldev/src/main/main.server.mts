import {Server} from "node:http";

import type {InjectionToken, StaticProvider, Type} from "@angular/core";
import efGame from "@eternalfest/game";
import loader from "@eternalfest/loader";
import {$LocaleId} from "@eternaltwin/core/core/locale-id";
import koaCors from "@koa/cors";
import Router from "@koa/router";
import * as furi from "furi";
import Koa, {DefaultContext, type Request} from "koa";
import koaLogger from "koa-logger";
import koaMount from "koa-mount";
import koaSend from "koa-send";
import koaStaticCache from "koa-static-cache";
import sysPath from "path";

import {ServerAppConfig} from "../server/config.mjs";
import {createKoaLocaleNegotiator, LocaleNegotiator} from "./koa-locale-negotiation.mjs";
import {LocaleId, NgLocalId, toNgLocale} from "./locales.mjs";
import {NgKoaEngine} from "./ng-koa-engine.mjs";
import {createApiRouter} from "./rest/index.mjs";
import {LocalEfApi, LocalEfApiOptions, withEfApi} from "./with-ef-api.mjs";

interface FrontendConfig {
  api: LocalEfApi,
  isProduction: boolean;
  port: number;
  appDir: URL;
  url: URL;
}

/**
 * Interface for the SSR module built by Angular
 *
 * This corresponds to the exports of `./src/server/ssr.ts`.
 */
interface SsrModule {
  CommonEngine: any,
  AppServerModule: Type<{}>,
  BROWSER_DIR: URL,
  SERVER_DIR: URL,
  SERVER_INDEX: URL,
  REQUEST: InjectionToken<Request>,
  APP_BASE_HREF: InjectionToken<string>,
  BACKEND_URI: InjectionToken<string>,
  INTERNAL_AUTH_KEY: InjectionToken<string>,
  ROUTES: readonly string[],
}

export async function main(config: FrontendConfig): Promise<void> {
  console.log("Frontent configuration:");
  console.log(`port = ${config.port}`);
  console.log(`appDir = ${config.appDir}`);
  console.log(`url = ${config.url}`);

  const ngApps = await importSsr();
  const defaultApp: SsrModule | undefined = ngApps.get("en-US");
  if (defaultApp === undefined) {
    throw new Error("missing app for locale en-US (default)");
  }

  const appConfig: ServerAppConfig = {
    externalUri: config.url,
    isProduction: config.isProduction,
    backendPort: config.port,
  };

  const appRouters: Map<LocaleId, Koa> = new Map();
  for (const localeId of $LocaleId.values) {
    const ngLocaleId = toNgLocale(localeId);
    const app = ngApps.get(ngLocaleId);
    if (app === undefined) {
      throw new Error(`missing app for locale: ${localeId} (Angular locale ${ngLocaleId})`);
    }
    const appRouter: Koa = await loadAppRouter(app, appConfig);
    appRouters.set(localeId, appRouter);
  }
  console.log(`Available locales: ${[...appRouters.keys()].join(", ")}`);
  const defaultRouter: Koa | undefined = await loadAppRouter(defaultApp, appConfig);

  const router: Koa = new Koa();

  router.use(koaLogger());
  if (!config.isProduction) {
    // make sure that there is no trailing slash in the cors header, or it will not work
    const angularLiveServer = "http://localhost:4200/";
    console.info(`dev env: enable CORS from ${angularLiveServer}`);
    router.use(koaCors({origin: angularLiveServer, credentials: true}));


    router.use(koaMount("/assets/loader.swf", sendLoader));

    router.use(koaMount("/assets/game.swf", sendGame));

    console.info("dev env: enable API handler");
    const apiRouter: Router = createApiRouter(config.api);
    router.use(koaMount("/api/v1", apiRouter.routes()));
    router.use(koaMount("/api/v1", apiRouter.allowedMethods()));
  }

  const ONE_DAY: number = 24 * 3600;
  // Just use the parent dir of the default app to find the browser root (kinda hacky)
  const staticDir = furi.join(defaultApp.BROWSER_DIR, "..");
  router.use(koaStaticCache(furi.toSysPath(staticDir), {maxAge: ONE_DAY}));

  const i18nRouter: Koa = createI18nRouter(defaultRouter, appRouters);
  router.use(koaMount("/", i18nRouter as any as Koa.Middleware));

  const server: Server = router.listen(config.port ?? 0, () => {
    const addressInfo = server.address();
    let listenInfo: string;
    if (typeof addressInfo === "string") {
      listenInfo = `unix socket ${furi.fromSysPath(addressInfo)}`;
    } else if (addressInfo !== null) {
      listenInfo = `port ${addressInfo.port}`;
    } else {
      throw new Error("failed to bind frontend server");
    }
    console.log(`SSR server ready on ${listenInfo}, externally available at ${config.url}`);
  });
}

async function sendLoader(cx: Koa.Context): Promise<void> {
  const loaderFuri: URL = loader.getLoaderUri(loader.Version.Flash8);
  const loaderSysPath: string = furi.toSysPath(loaderFuri.toString());
  const loaderDir: string = sysPath.dirname(loaderSysPath);
  const loaderBasename: string = sysPath.basename(loaderSysPath);
  await koaSend(cx, loaderBasename, {root: loaderDir});
}

async function sendGame(cx: Koa.Context): Promise<void> {
  const gameFuri: URL = efGame.getGameUri();
  const gameSysPath: string = furi.toSysPath(gameFuri.toString());
  const gameDir: string = sysPath.dirname(gameSysPath);
  const gameBasename: string = sysPath.basename(gameSysPath);
  await koaSend(cx, gameBasename, {root: gameDir});
}

function createI18nRouter(defaultRouter: Koa, localizedRouters: Map<LocaleId, Koa>): Koa {
  const router: Koa = new Koa();

  const localeNegotiator: LocaleNegotiator<Koa.Context> = createKoaLocaleNegotiator({
    cookieName: "locale",
    queryName: "l",
    supportedLocales: localizedRouters.keys(),
  });

  const defaultMiddleware: Koa.Middleware = koaMount(defaultRouter as any as Koa.Middleware) as Koa.Middleware;
  const localizedMiddlewares: Map<LocaleId, Koa.Middleware> = new Map();
  for (const [locale, app] of localizedRouters) {
    localizedMiddlewares.set(locale, koaMount(app as any as Koa.Middleware) as Koa.Middleware);
  }

  router.use(async (cx, next) => {
    const locale: LocaleId | undefined = localeNegotiator(cx);
    if (locale !== undefined) {
      const middleware: Koa.Middleware | undefined = localizedMiddlewares.get(locale);
      if (middleware !== undefined) {
        return middleware(cx, next);
      }
      // We matched a locale but don't have a corresponding router
      // TODO: Log warning? We should never reach this point since available
      //       locales are generated from available routers.
    }
    return defaultMiddleware(cx, next);
  });

  return router;
}

async function loadAppRouter(app: SsrModule, serverAppConfig: ServerAppConfig): Promise<Koa> {
  return ngToKoa(app, serverAppConfig);
}

async function importSsr(): Promise<Map<NgLocalId, SsrModule>> {
  return new Map([
    ["de", await import("#ssr-de-de")],
    ["en-US", await import("#ssr-en-us")],
    ["es", await import("#ssr-es-sp")],
    ["fr", await import("#ssr-fr-fr")],
  ]);
}

function resolveServerOptions(options?: Partial<ServerAppConfig>): ServerAppConfig {
  let isProduction: boolean = false;
  if (options === undefined) {
    options = {};
  } else {
    isProduction = options.isProduction === true;
  }
  const externalUri: URL | undefined = options.externalUri;
  if (isProduction) {
    if (externalUri === undefined) {
      throw new Error("Aborting: Missing server option `externalBaseUri` in production mode");
    }
  }
  const backendPort: undefined | number = options.backendPort;
  if (typeof backendPort !== "number") {
    throw new Error("Missing `backendPort` configuration");
  }
  return {externalUri, isProduction, backendPort};
}

/**
 * Resolves the fully qualified URL from the path and query
 */
function fullyQualifyUrl(options: ServerAppConfig, pathAndQuery: string): URL {
  if (options.externalUri !== undefined) {
    return new URL(pathAndQuery, options.externalUri);
  } else {
    return new URL(pathAndQuery, "http://localhost/");
  }
}

// The Express app is exported so that it can be used by serverless Functions.
export async function ngToKoa(app: SsrModule, options?: Partial<ServerAppConfig>): Promise<Koa> {
  const config: ServerAppConfig = resolveServerOptions(options);

  // const serverDir = app.SERVER_DIR;
  const serverIndex = app.SERVER_INDEX;
  const browserDir = app.BROWSER_DIR;

  const koa = new Koa();

  const providers: StaticProvider[] = [];
  if (config.externalUri !== undefined) {
    providers.push({provide: app.APP_BASE_HREF, useValue: config.externalUri.toString()});
  }

  const engine: NgKoaEngine = await NgKoaEngine.create({
    CommonEngine: app.CommonEngine,
    serverIndex,
    bootstrap: app.AppServerModule,
    providers,
    browserDir,
  });

  const router = new Router();
  // TODO: Fix `koajs/router` type definitions to accept a readonly ROUTES.
  router.get([...app.ROUTES], ngRender);
  koa.use(router.routes());
  koa.use(router.allowedMethods());

  async function ngRender(cx: DefaultContext): Promise<void> {
    const backendUri = new URL("http://localhost/");
    backendUri.port = config.backendPort.toString(10);
    const reqUrl: URL = fullyQualifyUrl(config, cx.request.originalUrl);
    cx.response.body = await engine.render({
      url: reqUrl,
      providers: [
        {provide: app.REQUEST, useValue: cx.request},
        {provide: app.BACKEND_URI, useValue: backendUri.toString()},
      ],
    });
  }

  // const ONE_DAY: number = 24 * 3600;
  // koa.use(koaStaticCache(furi.toSysPath(browserDir), {maxAge: ONE_DAY}));

  return koa;
}

export async function startServer(options: LocalEfApiOptions): Promise<void> {
  return withEfApi(options, (efApi): Promise<never> => {
    // Create a never-resolving promise so the API is never closed
    return new Promise<never>(() => {
      main({
        api: efApi,
        isProduction: false,
        port: 50317,
        appDir: furi.join(import.meta.url, "../.."),
        url: new URL("http://localhost:50317/"),
      });
    });
  });
}
