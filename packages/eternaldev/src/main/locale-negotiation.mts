// interface GetLocaleResolverOptions {
//   queryName?: string;
//   cookieName?: string;
//   supportedLocales: Iterable<Locale>;
//   defaultLocale: Locale;
// }
//
// type LocaleResolver = (req: Request) => Locale;
//
// function getLocaleResolver(options: GetLocaleResolverOptions): LocaleResolver {
//   const supported: Map<string, Locale> = new Map();
//   for (const locale of options.supportedLocales) {
//     const normalized: string = locale.toLocaleLowerCase().replace(/_/g, "-");
//     supported.set(normalized, locale);
//     const parts: string[] = normalized.split("-");
//     if (parts.length > 1) {
//       supported.set(parts[0], locale);
//     }
//   }
//
//   return (req: Request): Locale => {
//     let requested: string | undefined = undefined;
//     if (typeof options.queryName === "string") {
//       requested = req.query[options.queryName];
//       if (typeof requested === "string") {
//         requested = requested.toLocaleLowerCase().replace(/_/g, "-");
//         const result: Locale | undefined = supported.get(requested);
//         if (result !== undefined) {
//           return result;
//         }
//       }
//     }
//     if (typeof options.cookieName === "string") {
//       requested = req.cookies[options.cookieName];
//       if (typeof requested === "string") {
//         requested = requested.toLocaleLowerCase().replace(/_/g, "-");
//         const result: Locale | undefined = supported.get(requested);
//         if (result !== undefined) {
//           return result;
//         }
//       }
//     }
//     const languages: string[] = accepts(req).languages();
//     for (let language of languages) {
//       language = language.toLocaleLowerCase().replace(/_/g, "-");
//       let result: Locale | undefined = supported.get(language);
//       if (result !== undefined) {
//         return result;
//       }
//       const parts: string[] = language.split("-");
//       if (parts.length > 1) {
//         result = supported.get(parts[0]);
//         if (result !== undefined) {
//           return result;
//         }
//       }
//     }
//     return options.defaultLocale;
//   };
// }
//

//   const localeResolver: LocaleResolver = getLocaleResolver({
//     queryName: "l",
//     cookieName: "locale",
//     supportedLocales: LOCALES,
//     defaultLocale: DEFAULT_LOCALE,
//   });
//
