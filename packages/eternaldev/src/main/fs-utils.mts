import fs from "fs";
import * as furi from "furi";
import sysPath from "path";

export async function outputFileAsync(filePath: URL, data: Uint8Array): Promise<void> {
  await mkdirpAsync(furi.fromSysPath(sysPath.dirname(furi.toSysPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

export async function mkdirpAsync(dirPath: fs.PathLike): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath, {recursive: true}, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function writeFileAsync(filePath: fs.PathLike, data: Uint8Array): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

export async function readFileAsync(filePath: fs.PathLike): Promise<Uint8Array> {
  return new Promise<Uint8Array>((resolve, reject) => {
    fs.readFile(filePath, (err: NodeJS.ErrnoException | null, data: Uint8Array) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}

export async function readTextAsync(filePath: fs.PathLike): Promise<string> {
  return new Promise<string>((resolve, reject) => {
    fs.readFile(filePath, {encoding: "utf-8"}, (err: NodeJS.ErrnoException | null, data: string) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
