import appDataPath from "appdata-path";
import fs from "fs";
import * as furi from "furi";
import sysPath from "path";

const APP_NAME: string = "eternalfest";
const APP_BASE: URL = furi.fromSysPath(appDataPath.getAppDataPath(APP_NAME));

function getAppUrl(components: ReadonlyArray<string>): URL {
  return furi.join(APP_BASE.toString(), components);
}

export async function readAppFile(components: ReadonlyArray<string>): Promise<Buffer> {
  return readFileAsync(getAppUrl(components));
}

export async function writeAppFile(components: ReadonlyArray<string>, data: Buffer): Promise<void> {
  return outputFileAsync(getAppUrl(components), data);
}

export async function readAppText(components: ReadonlyArray<string>): Promise<string> {
  return (await readAppFile(components)).toString("utf-8");
}

export async function writeAppText(components: ReadonlyArray<string>, text: string): Promise<void> {
  return writeAppFile(components, Buffer.from(text));
}

export async function readAppJson(components: ReadonlyArray<string>): Promise<any> {
  return JSON.parse(await readAppText(components));
}

export async function writeAppJson(components: ReadonlyArray<string>, data: any): Promise<void> {
  return writeAppText(components, JSON.stringify(data, null, 2));
}

async function outputFileAsync(filePath: URL, data: Buffer): Promise<void> {
  await mkdirpAsync(furi.fromSysPath(sysPath.dirname(furi.toSysPath(filePath.toString()))));
  await writeFileAsync(filePath, data);
}

async function mkdirpAsync(dirPath: URL): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.mkdir(dirPath as URL, {recursive: true}, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

async function writeFileAsync(filePath: URL, data: Buffer): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath as URL, data, (err: NodeJS.ErrnoException | null) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve();
      }
    });
  });
}

async function readFileAsync(filePath: URL): Promise<Buffer> {
  return new Promise<Buffer>((resolve, reject) => {
    fs.readFile(filePath as URL, (err: NodeJS.ErrnoException | null, data: Buffer) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(data);
      }
    });
  });
}
