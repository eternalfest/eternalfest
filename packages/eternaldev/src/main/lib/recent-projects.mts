import { Url } from "@eternalfest/core/types/url";
import * as furi from "furi";

import { readAppJson, writeAppJson } from "./app-data.mjs";

const RECENT_PROJECTS_FILE: ReadonlyArray<string> = ["recent-projects.json"];
const MAX_RECENT_PROJECTS: number = 20;

export async function getRecentProjects(): Promise<Url[]> {
  try {
    const result: any = await readAppJson(RECENT_PROJECTS_FILE);
    if (Array.isArray(result)) {
      return result.map((projectPath) => furi.fromSysPath(projectPath));
    } else if (Array.isArray(result.projects)) {
      return result.projects.map((projectUrl: string) => new Url(projectUrl));
    } else {
      return [];
    }
  } catch (err) {
    if ((err as NodeJS.ErrnoException).code === "ENOENT") {
      console.warn(err);
    }
    return [];
  }
}

export async function setRecentProjects(projects: ReadonlyArray<Url>): Promise<void> {
  return writeAppJson(RECENT_PROJECTS_FILE, {projects: projects.map((project) => project.toString())});
}

export async function touchRecentProject(project: Url): Promise<void> {
  const oldProjects: ReadonlyArray<Url> = await getRecentProjects();
  const projects: string[] = oldProjects.map((project) => project.toString());
  const idx: number = projects.indexOf(project.toString());
  if (idx >= 0) {
    projects.splice(idx, 1);
  }
  projects.unshift(project.toString());
  if (projects.length > MAX_RECENT_PROJECTS) {
    projects.length = MAX_RECENT_PROJECTS;
  }

  return setRecentProjects(projects.map((project) => new Url(project)));
}
