import efDevkit from "@eternalfest/devkit";
import Router from "@koa/router";
import Koa from "koa";

export function createDevkitRouter(): Router {
  const router: Router = new Router();

  router.post("/editor", openEditor);

  async function openEditor(cx: Koa.Context): Promise<void> {
    cx.status = 202;
    efDevkit.levelEditor.exec({
      args: [],
      cwd: process.cwd(),
      stdio: "inherit",
    });
  }

  return router;
}
