import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {$Blob, Blob} from "@eternalfest/core/blob/blob";
import {$CreateBlobOptions, CreateBlobOptions} from "@eternalfest/core/blob/create-blob-options";
import {BlobService} from "@eternalfest/core/blob/service";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import {readOrThrow} from "kryo";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";

import {KoaAuth} from "./koa-auth.mjs";

export interface Api {
  koaAuth: KoaAuth;
  blob: BlobService;
}

export function createBlobsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createBlob]));

  async function createBlob(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const options: CreateBlobOptions = readOrThrow($CreateBlobOptions, JSON_VALUE_READER, cx.request.body);
    const blob: Blob = await api.blob.createBlob(auth, options);
    cx.response.body = $Blob.write(JSON_VALUE_WRITER, blob);
  }

  router.get("/:blob_id", getFileById);

  async function getFileById(cx: Koa.Context): Promise<void> {
    const rawBlobId = cx.params["blob_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const blobId: UuidHex = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawBlobId);
    const blob: Blob = await api.blob.getBlobById(auth, {id: blobId});
    cx.response.body = $Blob.write(JSON_VALUE_WRITER, blob);
  }

  router.get("/:blob_id/raw", getRawBlobById);

  async function getRawBlobById(cx: Koa.Context): Promise<void> {
    const rawBlobId = cx.params["blob_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const blobId: UuidHex = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawBlobId);
    const blob: Blob = await api.blob.getBlobById(auth, {id: blobId});
    cx.response.set("Content-Length", blob.byteSize.toString(10));
    cx.response.set("Content-Type", blob.mediaType);
    if (api.blob.hasImmutableBlobs) {
      cx.response.set("Cache-Control", "public,max-age=31536000,immutable");
    }
    cx.response.body = await api.blob.readBlobData(auth, {id: blobId});
  }

  return router;
}
