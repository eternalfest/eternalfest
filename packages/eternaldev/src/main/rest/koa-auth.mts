import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { AuthService } from "@eternalfest/core/auth/service";
import crypto from "crypto";
import Koa from "koa";

export const SESSION_COOKIE: string = "ef_sid";
const AUTHORIZATION_HEADER: string = "Authorization";
export const CSRF_TOKEN_COOKIE: string = "XSRF-TOKEN";
export const CSRF_TOKEN_HEADER: string = "X-XSRF-TOKEN";

export class KoaAuth {
  private readonly authService: AuthService;

  constructor(auth: AuthService) {
    this.authService = auth;
  }

  public async auth(cx: Koa.Context): Promise<AuthContext> {
    const sessionCookie: string | undefined = cx.cookies.get(SESSION_COOKIE);
    const authorizationHeader: string | string[] | undefined = Reflect.get(cx.request.headers, AUTHORIZATION_HEADER.toLowerCase());
    if (Array.isArray(authorizationHeader)) {
      throw new Error("Unexpected array value for the `Authorization` header");
    }
    return this.authService.authenticateHttp({sessionCookie, authorizationHeader});
  }

  public async getOrCreateCsrfToken(cx: Koa.Context): Promise<string> {
    let token: string | undefined = cx.cookies.get(CSRF_TOKEN_COOKIE);
    if (token === undefined) {
      token = await this.createCsrfToken();
      this.setCsrfToken(cx, token);
    }
    return token;
  }

  private setCsrfToken(cx: Koa.Context, value: string): void {
    cx.cookies.set(CSRF_TOKEN_COOKIE, value, {httpOnly: true, sameSite: "lax"});
  }

  private async createCsrfToken(): Promise<string> {
    return new Promise<string>((resolve, reject) => {
      crypto.randomBytes(16, (err: Error | null, buffer: Buffer): void => {
        if (err !== null) {
          reject(err);
        } else {
          resolve(buffer.toString("hex"));
        }
      });
    });
  }
}
