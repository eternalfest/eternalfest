import {AuthService} from "@eternalfest/core/auth/service";
import {BlobService} from "@eternalfest/core/blob/service";
import {Game2Service} from "@eternalfest/core/game2/service";
import {UserService} from "@eternalfest/core/user/service";
import Router from "@koa/router";
import Koa from "koa";

import {LocalProjectService} from "../api/project/local-service.mjs";
import {InMemoryRunService} from "../api/run/in-memory-service.mjs";
import {createBlobsRouter} from "./blobs.mjs";
import {createDevkitRouter} from "./devkit.mjs";
import {createGamesRouter} from "./games.mjs";
import {KoaAuth} from "./koa-auth.mjs";
import {createProjectsRouter} from "./projects.mjs";
import {createRunsRouter} from "./runs.mjs";
import {createSelfRouter} from "./self.mjs";
import {createUploadSessionsRouter} from "./upload-sessions.mjs";
import {createUsersRouter} from "./users.mjs";

export interface Api {
  auth: AuthService;
  blob: BlobService;
  game2: Game2Service;
  koaAuth: KoaAuth;
  project: LocalProjectService;
  run: InMemoryRunService;
  user: UserService;
}

export function createApiRouter(api: Api): Router {
  const router: Router = new Router();

  const blobs = createBlobsRouter(api);
  router.use("/blobs", blobs.routes(), blobs.allowedMethods());
  const devkit = createDevkitRouter();
  router.use("/devkit", devkit.routes(), devkit.allowedMethods());
  const projects = createProjectsRouter(api);
  router.use("/projects", projects.routes(), projects.allowedMethods());
  const games = createGamesRouter(api);
  router.use("/games", games.routes(), games.allowedMethods());
  const runs = createRunsRouter(api);
  router.use("/runs", runs.routes(), runs.allowedMethods());
  const self = createSelfRouter(api);
  router.use("/self", self.routes(), self.allowedMethods());
  const uploadSessions = createUploadSessionsRouter(api);
  router.use("/upload-sessions", uploadSessions.routes(), uploadSessions.allowedMethods());
  const users = createUsersRouter(api);
  router.use("/users", users.routes(), users.allowedMethods());
  router.use((cx: Koa.Context) => {
    cx.response.status = 404;
    cx.body = {error: "ResourceNotFound"};
  });

  return router;
}
