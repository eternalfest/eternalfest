import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {$ActiveGameChannel, ActiveGameChannel} from "@eternalfest/core/game2/active-game-channel";
import {CreateGameBuildOptions} from "@eternalfest/core/game2/create-game-build-options";
import {$CreateGameOptions, CreateGameOptions} from "@eternalfest/core/game2/create-game-options";
import {$Game, Game} from "@eternalfest/core/game2/game";
import {$GameBuild, GameBuild} from "@eternalfest/core/game2/game-build";
import {$GameChannelKey, GameChannelKey} from "@eternalfest/core/game2/game-channel-key";
import {$GameChannelPatch, GameChannelPatch} from "@eternalfest/core/game2/game-channel-patch";
import {$GameId, GameId} from "@eternalfest/core/game2/game-id";
import {$GameKey} from "@eternalfest/core/game2/game-key";
import {GameRef} from "@eternalfest/core/game2/game-ref";
import {$GetGamesOptions, GetGamesOptions} from "@eternalfest/core/game2/get-games-options";
import {$InputGameBuild, InputGameBuild} from "@eternalfest/core/game2/input-game-build";
import {Game2Service} from "@eternalfest/core/game2/service";
import {SetGameFavoriteOptions} from "@eternalfest/core/game2/set-game-favorite-options";
import {$ShortGameListing, ShortGameListing} from "@eternalfest/core/game2/short-game-listing";
import {UpdateGameChannelOptions} from "@eternalfest/core/game2/update-game-channel-options";
import {$Leaderboard, Leaderboard} from "@eternalfest/core/leaderboard/leaderboard";
import {RunService} from "@eternalfest/core/run/service";
import {$NullableUserIdRef, NullableUserIdRef} from "@eternalfest/core/user/user-id-ref";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import {CaseStyle, readOrThrow} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_READER} from "kryo-search-params/search-params-reader";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";

import {KoaAuth} from "./koa-auth.mjs";

export interface Api {
  koaAuth: KoaAuth;
  game2: Game2Service;
  run: RunService;
}

export interface UpdateGameChannelBody {
  actor?: NullableUserIdRef;
  patches: GameChannelPatch[];
}

export const $UpdateGameChannelBody: RecordIoType<UpdateGameChannelBody> = new RecordType<UpdateGameChannelBody>({
  properties: {
    actor: {type: $NullableUserIdRef, optional: true},
    patches: {type: new ArrayType({itemType: $GameChannelPatch, maxLength: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});

export function createGamesRouter(api: Api): Router {
  const router: Router = new Router();

  router.get("/", getGames);

  async function getGames(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let query: GetGamesOptions;
    try {
      query = readOrThrow($GetGamesOptions, SEARCH_PARAMS_READER, cx.request.querystring);
    } catch (_err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidQueryParameters"};
      return;
    }
    if (query.offset === undefined) {
      query.offset = 0;
    }
    if (query.limit === undefined) {
      query.limit = 30;
    }
    if (query.favorite === undefined) {
      query.favorite = false;
    }
    const games: ShortGameListing = await api.game2.getGames(auth, query);
    cx.response.body = $ShortGameListing.write(JSON_VALUE_WRITER, games);
  }

  router.post("/", koaCompose([koaBodyParser(), createGame]));

  async function createGame(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let options: CreateGameOptions;
    try {
      options = readOrThrow($CreateGameOptions, JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    let game: Game;
    try {
      game = await api.game2.createGame(auth, options);
    } catch (err) {
      switch (err instanceof Error ? err.name : undefined) {
        case "Unauthenticated":
          cx.response.status = 401;
          cx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          cx.response.status = 403;
          cx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("createGameError");
          console.error(err);
          cx.response.status = 500;
          break;
      }
      return;
    }
    const rawGame2 = $Game.write(JSON_VALUE_WRITER, game);
    cx.response.body = rawGame2;
  }

  router.get("/:game_ref", getGameByIdOrKey);

  async function getGameByIdOrKey(cx: Koa.Context): Promise<void> {
    const rawGameIdOrKey: string = cx.params["game_ref"];
    const auth: AuthContext = await api.koaAuth.auth(cx as any as Koa.Context);
    if (!$GameId.test(null, rawGameIdOrKey).ok && !$GameKey.test(null, rawGameIdOrKey).ok) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidGameRef"};
      return;
    }
    const gameRef: GameRef = $GameId.test(null, rawGameIdOrKey).ok ? {id: rawGameIdOrKey} : {key: rawGameIdOrKey};
    let query: GetGamesOptions;
    try {
      query = readOrThrow($GetGamesOptions, SEARCH_PARAMS_READER, cx.request.querystring);
    } catch (_err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidQueryParameters"};
      return;
    }
    const game: Game | null = await api.game2.getGame(auth, {
      game: gameRef,
      channelOffset: query.offset ?? 0,
      channelLimit: query.limit ?? 10,
    });
    if (game === null) {
      cx.response.status = 404;
      cx.response.body = {error: "GameNotFound"};
      return;
    }
    const rawGame2 = $Game.write(JSON_VALUE_WRITER, game);
    cx.response.body = rawGame2;
  }

  router.put("/:game_ref/favorite", koaCompose([koaBodyParser(), setGameFavorite]));

  async function setGameFavorite(cx: Koa.Context): Promise<void> {
    const rawGameIdOrKey: string = cx.params["game_ref"];
    if (!$GameId.test(null, rawGameIdOrKey).ok && !$GameKey.test(null, rawGameIdOrKey).ok) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidGameRef"};
      return;
    }
    const gameRef: GameRef = $GameId.test(null, rawGameIdOrKey).ok ? {id: rawGameIdOrKey} : {key: rawGameIdOrKey};
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const options: SetGameFavoriteOptions = {
      game: gameRef,
      favorite: true,
    };
    try {
      await api.game2.setGameFavorite(auth, options);
    } catch (err) {
      switch (err instanceof Error ? err.name : undefined) {
        case "Unauthenticated":
          cx.response.status = 401;
          cx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          cx.response.status = 403;
          cx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("createGameError");
          console.error(err);
          cx.response.status = 500;
          break;
      }
      return;
    }
    cx.response.body = {favorite: true};
  }

  router.post("/:game_ref/builds", koaCompose([koaBodyParser(), createGameBuild]));

  async function createGameBuild(cx: Koa.Context): Promise<void> {
    const rawGameIdOrKey: string = cx.params["game_ref"];
    if (!$GameId.test(null, rawGameIdOrKey).ok && !$GameKey.test(null, rawGameIdOrKey).ok) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidGameRef"};
      return;
    }
    const gameRef: GameRef = $GameId.test(null, rawGameIdOrKey).ok ? {id: rawGameIdOrKey} : {key: rawGameIdOrKey};
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let body: InputGameBuild;
    try {
      body = readOrThrow($InputGameBuild, JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const options: CreateGameBuildOptions = {
      game: gameRef,
      build: body,
    };
    let gameBuild: GameBuild;
    try {
      gameBuild = await api.game2.createBuild(auth, options);
    } catch (err) {
      switch (err instanceof Error ? err.name : undefined) {
        case "Unauthenticated":
          cx.response.status = 401;
          cx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          cx.response.status = 403;
          cx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("createGameError");
          console.error(err);
          cx.response.status = 500;
          break;
      }
      return;
    }
    cx.response.body = $GameBuild.write(JSON_VALUE_WRITER, gameBuild);
  }

  // TODO: Only keep the plural
  router.patch("/:game_ref/channel/:channel_key", koaCompose([koaBodyParser(), updateGameChannel]));
  router.patch("/:game_ref/channels/:channel_key", koaCompose([koaBodyParser(), updateGameChannel]));

  async function updateGameChannel(cx: Koa.Context): Promise<void> {
    const rawGameIdOrKey: string = cx.params["game_ref"];
    if (!$GameId.test(null, rawGameIdOrKey).ok && !$GameKey.test(null, rawGameIdOrKey).ok) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidGameRef"};
      return;
    }
    const gameRef: GameRef = $GameId.test(null, rawGameIdOrKey).ok ? {id: rawGameIdOrKey} : {key: rawGameIdOrKey};
    const rawChannelKey: string = cx.params["channel_key"];
    if (!$GameChannelKey.test(null, rawChannelKey).ok) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidGameRef"};
      return;
    }
    const channelKey: GameChannelKey = rawChannelKey;
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let body: UpdateGameChannelBody;
    try {
      body = readOrThrow($UpdateGameChannelBody, JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const options: UpdateGameChannelOptions = {
      actor: body.actor,
      game: gameRef,
      channelKey,
      patches: body.patches,
    };
    let gameChannel: ActiveGameChannel;
    try {
      gameChannel = await api.game2.updateChannel(auth, options);
    } catch (err) {
      switch (err instanceof Error ? err.name : undefined) {
        case "Unauthenticated":
          cx.response.status = 401;
          cx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          cx.response.status = 403;
          cx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("createGameError");
          console.error(err);
          cx.response.status = 500;
          break;
      }
      return;
    }
    cx.response.body = $ActiveGameChannel.write(JSON_VALUE_WRITER, gameChannel);
  }

  router.get("/:game_id/leaderboard", getLeaderboardByGameId);

  async function getLeaderboardByGameId(cx: Koa.Context): Promise<void> {
    const rawGameId = cx.params["game_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const gameId: GameId = readOrThrow($GameId, SEARCH_PARAMS_VALUE_READER, rawGameId);
    try {
      const leaderboard: Leaderboard = await api.run.getLeaderboard(auth, gameId, "solo");
      cx.response.body = $Leaderboard.write(JSON_VALUE_WRITER, leaderboard);
    } catch (err) {
      console.error(err);
      // TODO: Some better error?
      cx.response.status = 500;
      cx.response.body = {error: "UnknownError"};
      return;
    }
  }

  return router;
}
