import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {$CreateDebugRunOptions, CreateDebugRunOptions} from "@eternalfest/core/eternaldev/create-debug-run-options";
import {$Run, Run} from "@eternalfest/core/run/run";
import {$RunStart, RunStart} from "@eternalfest/core/run/run-start";
import {$SetRunResultOptions, SetRunResultOptions} from "@eternalfest/core/run/set-run-result-options";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import {readOrThrow} from "kryo";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";

import {InMemoryRunService} from "../api/run/in-memory-service.mjs";
import {KoaAuth} from "./koa-auth.mjs";

export interface Api {
  koaAuth: KoaAuth;
  run: InMemoryRunService;
}

export function createRunsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createRun]));

  async function createRun(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let options: CreateDebugRunOptions;
    try {
      options = readOrThrow($CreateDebugRunOptions, JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await api.run.createDebugRun(auth, options);
    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  router.get("/:run_id", getRunById);

  async function getRunById(cx: Koa.Context): Promise<void> {
    const rawRunId = cx.params["run_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let runId: UuidHex;
    try {
      runId = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawRunId);
    } catch (err) {
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest", message: "Invalid run id"};
      return;
    }

    let run: Run;
    try {
      run = await api.run.getRunById(auth, runId);
    } catch (err) {
      switch (err instanceof Error ? err.name : undefined) {
        case "RunNotFound":
          cx.response.status = 404;
          cx.response.body = {error: "ResourceNotFound"};
          break;
        case "Unauthenticated":
          cx.response.status = 401;
          cx.response.body = {error: "Unauthorized"};
          break;
        case "Forbidden":
          cx.response.status = 403;
          cx.response.body = {error: "Forbidden"};
          break;
        default:
          console.error("getRunById");
          console.error(err);
          cx.response.status = 500;
          break;
      }
      return;
    }

    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  router.post("/:run_id/start", startRun);

  async function startRun(cx: Koa.Context): Promise<void> {
    const rawRunId = cx.params["run_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const runId: UuidHex = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawRunId);
    const runStart: RunStart = await api.run.startRun(auth, runId);
    cx.response.body = $RunStart.write(JSON_VALUE_WRITER, runStart);
  }

  router.post("/:run_id/result", koaCompose([koaBodyParser(), setRunResult]));

  async function setRunResult(cx: Koa.Context): Promise<void> {
    const rawRunId = cx.params["run_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let runId: UuidHex;
    let options: SetRunResultOptions;
    try {
      runId = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawRunId);
      if (Reflect.get(cx.request.body as any, "flash") === "true") {
        for (const [key, value] of Object.entries(cx.request.body as any)) {
          Reflect.set(cx.request.body as any, key, JSON.parse(value as any));
        }
      }
      options = readOrThrow($SetRunResultOptions, JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const run: Run = await api.run.setRunResult(auth, runId, options);
    cx.response.body = $Run.write(JSON_VALUE_WRITER, run);
  }

  return router;
}
