import { AuthContext } from "@eternalfest/core/auth/auth-context";
import {
  $CreateUploadSessionOptions,
  CreateUploadSessionOptions
} from "@eternalfest/core/blob/create-upload-session-options";
import { BlobService } from "@eternalfest/core/blob/service";
import { $UploadBytesOptions, UploadBytesOptions } from "@eternalfest/core/blob/upload-bytes-options";
import { $UploadSession, UploadSession } from "@eternalfest/core/blob/upload-session";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import {readOrThrow} from "kryo";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";

import { KoaAuth } from "./koa-auth.mjs";

export interface Api {
  koaAuth: KoaAuth;
  blob: BlobService;
}

export function createUploadSessionsRouter(api: Api): Router {
  const router: Router = new Router();

  router.post("/", koaCompose([koaBodyParser(), createUploadSession]));

  async function createUploadSession(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    const options: CreateUploadSessionOptions = readOrThrow($CreateUploadSessionOptions, JSON_VALUE_READER, ctx.request.body);
    const uploadSession: UploadSession = await api.blob.createUploadSession(auth, options);
    ctx.response.body = $UploadSession.write(JSON_VALUE_WRITER, uploadSession);
  }

  router.patch("/:upload_session_id", koaCompose([koaBodyParser(), uploadBytes]));

  async function uploadBytes(cx: Koa.Context): Promise<void> {
    const rawUploadSessionId = cx.params["upload_session_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let uploadSessionId: UuidHex;
    let options: UploadBytesOptions;
    try {
      uploadSessionId = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawUploadSessionId);
      options = readOrThrow($UploadBytesOptions, JSON_VALUE_READER, cx.request.body);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const uploadSession: UploadSession = await api.blob.uploadBytes(auth, {uploadSessionId, ...options});
    cx.response.body = $UploadSession.write(JSON_VALUE_WRITER, uploadSession);
  }

  return router;
}
