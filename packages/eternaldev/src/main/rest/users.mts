import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {$RunItems, RunItems} from "@eternalfest/core/run/run-items";
import {RunService} from "@eternalfest/core/run/service";
import {UserService} from "@eternalfest/core/user/service";
import {$UpdateUserOptions, UpdateUserOptions} from "@eternalfest/core/user/update-user-options";
import {$User, User} from "@eternalfest/core/user/user";
import {UserListing} from "@eternalfest/core/user/user-listing";
import Router from "@koa/router";
import Koa from "koa";
import koaBodyParser from "koa-bodyparser";
import koaCompose from "koa-compose";
import {CaseStyle, readOrThrow} from "kryo";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_VALUE_READER} from "kryo-search-params/search-params-value-reader";

import {KoaAuth} from "./koa-auth.mjs";

const $PartialUser: RecordIoType<Partial<User>> = new RecordType<Partial<User>>({
  properties: {
    id: {type: $User.properties.id.type, optional: true},
    type: {type: $User.properties.type.type, optional: true},
    displayName: {type: $User.properties.displayName.type, optional: true},
    createdAt: {type: $User.properties.createdAt.type, optional: true},
    updatedAt: {type: $User.properties.updatedAt.type, optional: true},
    identities: {type: $User.properties.identities!.type, optional: true},
    isAdministrator: {type: $User.properties.isAdministrator.type, optional: true},
    isTester: {type: $User.properties.isTester!.type, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface Api {
  koaAuth: KoaAuth;
  user: UserService;
  run: RunService;
}

export function createUsersRouter(api: Api): Router {
  const router: Router = new Router();

  router.get("/", getUsers);

  async function getUsers(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const userListing: UserListing = await api.user.getUsers(auth);
    const users: ReadonlyArray<Partial<User>> = userListing.items;
    cx.response.body = users.map(user => $PartialUser.write(JSON_VALUE_WRITER, user));
  }

  router.get("/:user_id", getUserById);

  async function getUserById(cx: Koa.Context): Promise<void> {
    const rawUserId = cx.params["user_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    const userId: UuidHex = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawUserId);
    const user: User | undefined = await api.user.getUserById(auth, userId);
    if (user === undefined) {
      cx.response.status = 404;
      return;
    }
    cx.response.body = $User.write(JSON_VALUE_WRITER, user);
  }

  router.patch("/:user_id", koaCompose([koaBodyParser(), updateUserById]));

  async function updateUserById(cx: Koa.Context): Promise<void> {
    const rawUserId = cx.params["user_id"];
    const auth: AuthContext = await api.koaAuth.auth(cx);
    let options: UpdateUserOptions;
    let userId: UuidHex;
    try {
      options = readOrThrow($UpdateUserOptions, JSON_VALUE_READER, cx.request.body);
      userId = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawUserId);
    } catch (err) {
      console.error(err);
      cx.response.status = 422;
      cx.response.body = {error: "InvalidRequest"};
      return;
    }
    const user: User = await api.user.updateUser(auth, {...options, userId});
    cx.response.body = $User.write(JSON_VALUE_WRITER, user);
  }

  router.get("/:user_id/profiles/:game_id/items", getUserItems);

  async function getUserItems(cx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(cx);

    const rawUserId = cx.params["user_id"];
    const rawGameId = cx.params["game_id"];
    const rawUntil = cx.query["until"];
    const userId: UuidHex = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawUserId);
    const gameId: UuidHex = readOrThrow($UuidHex, SEARCH_PARAMS_VALUE_READER, rawGameId);
    const until: Date | null = rawUntil === undefined ? null : readOrThrow($Date, SEARCH_PARAMS_VALUE_READER, rawUntil);

    const items: RunItems | undefined = await api.run.getGameUserItemsById(auth, userId, gameId, until);
    if (items === undefined) {
      cx.response.status = 404;
      return;
    }

    // work-around: ReadonlyMap isn't Map :c
    const items2 = new Map(items);
    cx.response.body = $RunItems.write(JSON_VALUE_WRITER, items2);
  }

  return router;
}
