import {$Project, Project} from "@eternalfest/core/eternaldev/project";
import Router from "@koa/router";
import Koa from "koa";
import {CaseStyle, readOrThrow} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {SEARCH_PARAMS_READER} from "kryo-search-params/search-params-reader";

import {LocalProjectService} from "../api/project/local-service.mjs";
import {$ShortProject, ShortProject} from "../api/project/short-project.mjs";

export interface Api {
  project: LocalProjectService;
}

export function createProjectsRouter(api: Api): Router {
  const router: Router = new Router();

  router.get("/", getProjects);

  async function getProjects(cx: Koa.Context): Promise<void> {
    const projects: ShortProject[] = await api.project.getProjects();
    cx.response.body = projects.map((project) => $ShortProject.write(JSON_VALUE_WRITER, project));
  }

  router.get("/:project_id", getProjectById);

  interface GetProjectByIdQuery {
    projectId: UuidHex,
  }

  const $GetProjectByIdPathParams: RecordIoType<any> = new RecordType({
    properties: {
      projectId: {type: $UuidHex},
    },
    changeCase: CaseStyle.SnakeCase,
  });

  async function getProjectById(cx: Koa.Context): Promise<void> {
    const query: GetProjectByIdQuery = readOrThrow($GetProjectByIdPathParams, SEARCH_PARAMS_READER, cx.params);
    const project: Project | undefined = await api.project.getProjectById(query.projectId);
    if (project === undefined) {
      cx.status = 404;
      return;
    }
    cx.response.body = $Project.write(JSON_VALUE_WRITER, project);
  }

  return router;
}
