import { $AuthContext, AuthContext } from "@eternalfest/core/auth/auth-context";
import { AuthService } from "@eternalfest/core/auth/service";
import Router from "@koa/router";
import Koa from "koa";
import { JSON_VALUE_WRITER } from "kryo-json/json-value-writer";

import { KoaAuth } from "./koa-auth.mjs";

export interface Api {
  auth: AuthService;
  koaAuth: KoaAuth;
  // session: SessionService;
}

export function createSelfRouter(api: Api): Router {
  const router: Router = new Router();

  router.get("/auth", getSelfAuth);

  async function getSelfAuth(ctx: Koa.Context): Promise<void> {
    const auth: AuthContext = await api.koaAuth.auth(ctx);
    ctx.response.body = $AuthContext.write(JSON_VALUE_WRITER, auth);
  }

  return router;
}
