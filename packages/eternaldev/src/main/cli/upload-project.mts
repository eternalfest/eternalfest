import {EternalfestClient, GetGameResponse, RequestType} from "@eternalfest/client/index";
import {Blob} from "@eternalfest/core/blob/blob";
import {BlobIdRef, NullableBlobIdRef} from "@eternalfest/core/blob/blob-id-ref";
import {UploadSession} from "@eternalfest/core/blob/upload-session";
import {LocalFileMap} from "@eternalfest/core/eternaldev/local-file-map";
import {LocalFileRef} from "@eternalfest/core/eternaldev/local-file-ref";
import {Project} from "@eternalfest/core/eternaldev/project";
import {CreateGameOptions} from "@eternalfest/core/game2/create-game-options";
import {Game} from "@eternalfest/core/game2/game";
import {GameBuild} from "@eternalfest/core/game2/game-build";
import {$GameChannelPatch, GameChannelPatch} from "@eternalfest/core/game2/game-channel-patch";
import {GameChannelPermission} from "@eternalfest/core/game2/game-channel-permission";
import {GameEngineType} from "@eternalfest/core/game2/game-engine-type";
import {GameId} from "@eternalfest/core/game2/game-id";
import {InputGameBuild} from "@eternalfest/core/game2/input-game-build";
import {InputGameBuildI18n} from "@eternalfest/core/game2/input-game-build-i18n";
import {InputGameEngine} from "@eternalfest/core/game2/input-game-engine";
import {NullableInputGamePatcher} from "@eternalfest/core/game2/input-game-patcher";
import {InputGameResource} from "@eternalfest/core/game2/input-game-resource";
import {ObjectType} from "@eternalfest/core/types/object-type";
import {Url} from "@eternalfest/core/types/url";
import {unreachable} from "@eternalfest/efc/utils";
import {LocaleId} from "@eternaltwin/core/core/locale-id";
import childProcess from "child_process";
import fs from "fs";
import * as furi from "furi";
import incident from "incident";
import inquirer from "inquirer";
import {IoType, readOrThrow} from "kryo";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {PRETTY_JSON_WRITER} from "kryo-json/json-writer";
import readline from "readline";
import {fileURLToPath} from "url";

import {LocalBlobService} from "../api/blob/local-service.mjs";
import {LocalProjectService} from "../api/project/local-service.mjs";
import {getLocalAuth} from "./login.mjs";

export interface UploadProjectOptions {
  api: string,
  ignoreCommitRef?: boolean,
}

export async function uploadProject(options: UploadProjectOptions): Promise<void> {
  const apiBaseUrl: Url = new Url(options.api);
  console.log(`API endpoint: ${apiBaseUrl.host}`);
  const auth = await getLocalAuth(options.api);
  if (auth === null) {
    throw new Error("Unauthenticated. Please run the `login` subcommand first");
  }
  console.log(`Auth: ${auth.user.display_name} (${auth.user.id}) since ${auth.created_at}`);
  const eternalfest = new EternalfestClient(apiBaseUrl);
  const session = auth.key;

  const cwdUrl: Url = furi.fromSysPath(process.cwd());
  const blobService: LocalBlobService = new LocalBlobService();
  const projectService: LocalProjectService = new LocalProjectService(blobService, {
    projectDirs: [cwdUrl],
    noGitIntegration: options.ignoreCommitRef,
  });

  const project: Project | undefined = await projectService.getProjectByRoot(cwdUrl);
  if (project === undefined) {
    throw new Error(`NonProjectDirectory: ${cwdUrl}`);
  }

  if (project.game.gitCommitRef !== null && await hasGitUncommittedChanges(cwdUrl)) {
    console.error("Uncommitted changes are present in the current git repository");
    console.error("Please commit them and try again.");
    return;
  }

  const fileMap: LocalFileMap = project.files;

  console.log(`Project: ${project.game.displayName}`);
  console.log(`Local URL: ${cwdUrl}`);
  console.log(`Remote id: ${project.remoteId ?? "(none)"}`);
  console.log(`Version: ${project.game.version}`);
  console.log(`Commit: ${project.game.gitCommitRef ?? "(none)"}`);

  let oldGame: Game | null = null;
  if (typeof project.remoteId === "string") {
    console.log("`remote_id` is set: fetching current data...");
    const gameRes: GetGameResponse = await eternalfest.execute({type: RequestType.GetGame, session: auth.key, ref: project.remoteId});
    oldGame = gameRes.value;
    console.log(`Active channel: ${oldGame.channels.active.key}`);
    console.log(`Remote name: ${oldGame.channels.active.build.displayName}`);
    console.log(`Active build: ${oldGame.channels.active.build.version}`);
    console.log(`Publication date: ${oldGame.channels.active.publicationDate?.toISOString()}`);
    console.log(`Last major update: ${oldGame.channels.active.sortUpdateDate.toISOString()}`);
  }

  const ACTION_CREATE = "Create a new game" as const;
  const ACTION_UPLOAD = "Upload a game build (no user-visible change)" as const;
  const ACTION_UPDATE = "Schedule an update (user-visible change)" as const;

  const answer: { action: ((typeof ACTION_CREATE) | (typeof ACTION_UPLOAD) | (typeof ACTION_UPDATE)) } = await inquirer.prompt([
    {
      name: "action",
      type: "list",
      message: "What do you want to do?",
      choices: [
        ACTION_CREATE,
        ACTION_UPLOAD,
        ACTION_UPDATE,
      ],
    },
  ]);

  if (project.remoteId === undefined) {
    if (answer.action === ACTION_CREATE) {
      console.log("Uploading a NEW game");
      await createGameFromProject(eternalfest, session, fileMap, project);
    } else {
      console.log("Specify the remote id (`remote_id` field) in the project manifest then run the upload again.");
      return;
    }
  } else {
    if (answer.action === ACTION_CREATE) {
      console.log("Remove the remote id (`remote_id` field) from the project manifest then run the upload again.");
      return;
    }
    if (answer.action === ACTION_UPLOAD) {
      console.log("UPLOADING a game build, no user-visible changes will be applied");
      await uploadBuild(eternalfest, session, fileMap, project, project.remoteId);
    } else if (answer.action === ACTION_UPDATE) {
      if (oldGame === null) {
        throw new Error("Missing old game data");
      }
      await updateGame(eternalfest, session, oldGame);
    }
  }
}

async function hasGitUncommittedChanges(dir: Url): Promise<boolean> {
  const child = childProcess.spawn("git", ["diff", "--quiet", "HEAD"], {
    // Note: node 14 & 15 don't support URLs here.
    cwd: fileURLToPath(dir.toString()),
    stdio: "ignore",
  });

  return new Promise((resolve, reject) => {
    child.on("exit", (code, _signal) => {
      if (code === 0) {
        resolve(false);
      } else if (code === 1) {
        resolve(true);
      } else {
        reject(new Error(`UnexpectedGitExitCode: ${code}`));
      }
    });
  });
}

async function createGameFromProject(client: EternalfestClient, session: string, fileMap: LocalFileMap, project: Project) {
  console.log("Start Upload");
  const inputBuild = await createBuildFromProject(client, session, fileMap, project);

  console.log("Finalizing");

  const createGame: CreateGameOptions = {
    build: inputBuild,
    channels: [
      {
        key: "main",
        isEnabled: false,
        defaultPermission: GameChannelPermission.None,
        isPinned: false,
        publicationDate: null,
        sortUpdateDate: null,
        version: inputBuild.version,
        patches: [],
      },
      {
        key: "dev",
        isEnabled: true,
        defaultPermission: GameChannelPermission.None,
        isPinned: false,
        publicationDate: null,
        sortUpdateDate: null,
        version: inputBuild.version,
        patches: [],
      }
    ],
  };

  const {value: game} = await client.execute({type: RequestType.CreateGame, session, game: createGame});

  console.log(`Done: ${inputBuild.displayName}: ${game.id}`);
}

async function uploadBuild(client: EternalfestClient, session: string, fileMap: LocalFileMap, project: Project, gameId: GameId) {
  console.log("Start Upload");
  const inputBuild = await createBuildFromProject(client, session, fileMap, project);

  console.log("Finalizing");

  const {value: gameBuild} = await client.execute({type: RequestType.CreateGameBuild, session, game: gameId, build: inputBuild});

  console.log(`Done: ${inputBuild.displayName}: ${gameBuild.version}`);
  console.log("The game build is on the server, but no channel was updated. You must now apply the update explicitly if you want the changes to be visible");
}

async function updateGame(client: EternalfestClient, session: string, game: Game) {
  let patch: GameChannelPatch | null = null;
  let channel: string = "dev";

  const MODE_RAW = "raw" as const;
  const MODE_GUIDED = "guided" as const;

  const answer: { mode: typeof MODE_RAW | typeof MODE_GUIDED } = await inquirer.prompt([
    {
      name: "mode",
      type: "list",
      message: "Do you want to use the guided mode or enter the raw JSON patch manually?",
      choices: [
        MODE_GUIDED,
        MODE_RAW,
      ],
    },
  ]);

  switch (answer.mode) {
    case MODE_GUIDED: {
      if (game.channels.active.key !== "main" || [GameChannelPermission.None, GameChannelPermission.View].includes(game.channels.active.defaultPermission)) {
        const answer1: { publish: boolean } = await inquirer.prompt([
          {
            name: "publish",
            type: "confirm",
            message: "The game is currently private, do you want to make it public?",
          },
        ]);
        if (answer1.publish) {
          const answer2: { startTime: string, version: string } = await inquirer.prompt([
            {
              name: "startTime",
              type: "input",
              message: `When should the game become public? Enter a UTC time in the future (help: the current UTC time is \`${(new Date()).toISOString()}\`)`,
            },
            {
              name: "version",
              type: "input",
              message: "Which version should be used?",
              default: game.channels.active.build.version,
            },
          ]);
          const start: Date = new Date(answer2.startTime);

          channel = "main";
          patch = {
            period: {
              start,
              end: null,
            },
            isEnabled: true,
            defaultPermission: GameChannelPermission.Play,
            isPinned: game.channels.active.isPinned,
            publicationDate: start,
            sortUpdateDate: start,
            version: answer2.version,
          };
        }
      }
      if (patch === null) {
        const defaultDate = new Date(Date.now() + 1000 * 60 * 10); // 10 minutes in the future

        const answer2: { startTime: string, version: string, major: boolean } = await inquirer.prompt([
          {
            name: "version",
            type: "input",
            message: "Which version should be used?",
            default: game.channels.active.build.version,
          },
          {
            name: "startTime",
            type: "input",
            message: `When should the change be applied? Enter a UTC time in the future (help: the current UTC time is \`${(new Date()).toISOString()}\`)`,
            default: "now",
          },
          {
            name: "major",
            type: "confirm",
            message: "Is this a major change (move to the top of the game list)?",
          },
        ]);
        const start: Date = answer2.startTime === "now" ? defaultDate : new Date(answer2.startTime);

        channel = game.channels.active.key;
        patch = {
          period: {
            start: answer2.startTime === "now" ? null : start,
            end: null,
          },
          isEnabled: true,
          defaultPermission: game.channels.active.key === "main" ? GameChannelPermission.Play : GameChannelPermission.None,
          isPinned: game.channels.active.isPinned,
          publicationDate: game.channels.active.publicationDate,
          sortUpdateDate: answer2.major ? start : game.channels.active.sortUpdateDate,
          version: answer2.version,
        };
      }

      const { isPinned }: { isPinned: boolean } = await inquirer.prompt([{
        name: "isPinned",
        type: "confirm",
        message: "Should the game be pinned on top of the game list?",
        default: patch.isPinned,
      }]);
      patch.isPinned = isPinned;

      break;
    }
    case MODE_RAW: {
      const answer1: { channel: string, raw: string } = await inquirer.prompt([
        {
          name: "channel",
          type: "input",
          message: "Channel",
        },
        {
          name: "raw",
          type: "input",
          message: "Raw Patch",
        },
      ]);
      channel = answer1.channel;
      patch = readOrThrow($GameChannelPatch, JSON_READER, answer1.raw);
      break;
    }
    default: {
      throw new Error("InvalidUpdateMode");
    }
  }

  console.log("The following changes will be applied. Send them to @demurgos first so he can check if it's OK.");
  console.log(`Game: ${game.id}`);
  console.log(`Channel: ${channel}`);
  console.log(`Patch: ${$GameChannelPatch.write(PRETTY_JSON_WRITER, patch)}`);

  const answer2: { demuOk: boolean, lieOk: boolean, userOk: boolean } = await inquirer.prompt([
    {
      name: "demuOk",
      type: "confirm",
      message: "Has @demurgos seen and approved the changes?",
    },
    {
      name: "lieOk",
      type: "confirm",
      message: "Did you lie in the previous question?",
    },
    {
      name: "userOk",
      type: "confirm",
      message: "Are you sure you want to apply the changes?",
    },
  ]);

  if (!(answer2.demuOk && !answer2.lieOk && answer2.userOk)) {
    console.info("Aborting");
    return;
  }

  console.log("Sending changes");

  await client.execute({
    type: RequestType.UpdateGameChannel,
    session,
    game: game.id,
    channel,
    patches: [patch],
  });

  console.log("Done");
}

async function createBuildFromProject(client: EternalfestClient, session: string, fileMap: LocalFileMap, project: Project): Promise<InputGameBuild> {
  const localBuild: GameBuild = project.game;
  let icon: NullableBlobIdRef = null;
  if (localBuild.icon !== null) {
    icon = await uploadFile(client, session, fileMap, localBuild.icon);
  }
  let engine: InputGameEngine;
  switch (localBuild.engine.type) {
    case GameEngineType.V96:
      engine = {type: GameEngineType.V96};
      break;
    case GameEngineType.Custom:
      engine = {type: GameEngineType.Custom, blob: await uploadFile(client, session, fileMap, localBuild.engine.blob)};
      break;
    default:
      unreachable(localBuild.engine, "unexpected engine type");
  }
  let patcher: NullableInputGamePatcher = null;
  if (localBuild.patcher !== null) {
    patcher = {
      blob: await uploadFile(client, session, fileMap, localBuild.patcher.blob),
      framework: localBuild.patcher.framework,
      meta: localBuild.patcher.meta,
    };
  }
  let debug: NullableBlobIdRef = null;
  if (localBuild.debug !== null) {
    debug = await uploadFile(client, session, fileMap, localBuild.debug);
  }
  let content: NullableBlobIdRef = null;
  if (localBuild.content !== null) {
    content = await uploadFile(client, session, fileMap, localBuild.content);
  }
  let contentI18n: NullableBlobIdRef = null;
  if (localBuild.contentI18n !== null) {
    contentI18n = await uploadFile(client, session, fileMap, localBuild.contentI18n);
  }
  const musics: InputGameResource[] = [];
  for (const music of localBuild.musics) {
    musics.push({
      blob: await uploadFile(client, session, fileMap, music.blob),
      displayName: music.displayName,
    });
  }

  const i18n: Map<LocaleId, InputGameBuildI18n> = new Map();

  for (const [locale, i18nBuild] of localBuild.i18n) {
    let icon: BlobIdRef | undefined = undefined;
    if (i18nBuild.contentI18n !== null && typeof i18nBuild.icon === "object") {
      icon = await uploadFile(client, session, fileMap, i18nBuild.icon);
    }
    let contentI18n: BlobIdRef | undefined = undefined;
    if (i18nBuild.contentI18n !== null && typeof i18nBuild.contentI18n === "object") {
      contentI18n = await uploadFile(client, session, fileMap, i18nBuild.contentI18n);
    }
    i18n.set(locale as LocaleId, {
      displayName: i18nBuild.displayName,
      description: i18nBuild.description,
      icon,
      contentI18n,
      modes: i18nBuild.modes,
    });
  }

  const inputBuild: InputGameBuild = {
    version: localBuild.version,
    gitCommitRef: localBuild.gitCommitRef,
    mainLocale: localBuild.mainLocale,
    displayName: localBuild.displayName,
    description: localBuild.description,
    icon,
    loader: localBuild.loader,
    engine,
    patcher,
    debug,
    content,
    contentI18n,
    musics,
    modes: localBuild.modes,
    families: localBuild.families,
    category: localBuild.category,
    i18n,
  };

  return inputBuild;
}

export interface Credentials {
  username: string;
  password: string;
}

export async function promptCredentials(): Promise<Credentials> {
  const answer: { username: string, password: string } = await inquirer.prompt([
    {
      name: "username",
      type: "input",
      message: "Eternaltwin username",
    },
    {
      name: "password",
      type: "password",
      message: "Eternaltwin password",
    },
  ]);
  return answer;
}

export async function prompt(message: string): Promise<string> {
  const cliInterface: readline.ReadLine = readline.createInterface(
    process.stdin,
    process.stdout,
  );

  const answer: string = await new Promise<string>(
    (resolve: (res: string) => void, _reject: (err: any) => void): void => {
      cliInterface.question(message, resolve);
    },
  );

  const result: Promise<string> = new Promise(
    (resolve: (res: string) => void, reject: (err: Error) => void): void => {
      cliInterface.once("error", (err: Error): void => {
        reject(err);
      });
      cliInterface.once("close", (): void => {
        resolve(answer);
      });
    },
  );

  cliInterface.close();
  return result;
}

const CHUNK_SIZE: number = 200 * 1024;

async function uploadFile(client: EternalfestClient, session: string, fileMap: LocalFileMap, localBlob: Blob): Promise<BlobIdRef> {
  const fileRef: LocalFileRef | undefined = fileMap.get(localBlob.id);
  if (fileRef === undefined) {
    throw new Error(`blobResolutionError: ${localBlob.id}`);
  }
  const fileUri = furi.asFuri(fileRef.url);
  const name = furi.basename(fileUri);

  console.log(`Start upload: ${name}`);
  const buffer: Buffer = await readFile(fileUri);
  let blob: Blob;
  if (buffer.length <= CHUNK_SIZE) {
    const res = await client.execute({
      type: RequestType.CreateBlob,
      session,
      blob: {mediaType: localBlob.mediaType, data: buffer}
    });
    blob = res.value;
  } else {
    const res = await client.execute({
      type: RequestType.CreateUploadSession,
      session,
      options:{mediaType: localBlob.mediaType, byteSize: buffer.length},
    });
    let uploadSession: UploadSession = res.value;
    while (uploadSession.remainingRange.start < uploadSession.remainingRange.end) {
      console.log(uploadSession.remainingRange.start / buffer.length);
      uploadSession = await reliableUploadNextChunk(client, session, uploadSession, buffer);
    }
    blob = uploadSession.blob!;
  }
  console.log(1);
  return {
    type: ObjectType.Blob,
    id: blob.id
  };
}

async function reliableUploadNextChunk(client: EternalfestClient, session: string, uploadSession: UploadSession, buffer: Buffer) {
  for (; ;) {
    try {
      return await uploadNextChunk(client, session, uploadSession, buffer);
    } catch (err) {
      if (err instanceof Error && Reflect.get(err, "code") === "ECONNRESET") {
        continue;
      }
      console.error(err);
    }
  }
}

async function uploadNextChunk(
  client: EternalfestClient,
  session: string,
  uploadSession: UploadSession,
  buffer: Buffer,
): Promise<UploadSession> {
  const res = await client.execute({
    type: RequestType.UploadBytes,
    session,
    uploadSession: uploadSession.id,
    data: {
      data: buffer.slice(
        uploadSession.remainingRange.start,
        Math.min(uploadSession.remainingRange.start + CHUNK_SIZE, uploadSession.remainingRange.end),
      ),
      offset: uploadSession.remainingRange.start,
    },
  });
  return res.value;
}

async function readFile(filePath: URL): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(filePath, (err, buff) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(buff);
      }
    });
  });
}

export function readJsonResponse<T>(type: IoType<T>, raw: any): T {
  if (typeof raw.error === "string") {
    throw new incident.Incident(raw.error, raw);
  } else {
    return readOrThrow(type, JSON_VALUE_READER, raw);
  }
}
