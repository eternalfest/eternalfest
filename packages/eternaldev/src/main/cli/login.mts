import * as etwin from "@eternalfest/client/etwin";
import {EternalfestClient, LoginWithEternaltwinResponse, OauthReturnResponse, RequestType} from "@eternalfest/client/index";
import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import inquirer from "inquirer";

import {readAppJson, writeAppJson} from "../lib/app-data.mjs";

export interface LoginOptions {
  uri: string,
}

export async function login(options: LoginOptions): Promise<void> {
  const apiBaseUrl: URL = new URL(options.uri);
  console.log(`Starting login on: ${apiBaseUrl.host}`);
  const eternalfest = new EternalfestClient(apiBaseUrl);
  const login: LoginWithEternaltwinResponse = await eternalfest.execute({type: RequestType.LoginWithEternaltwin});
  const etwinBase = new URL(login.getAuthorization.toString());
  etwinBase.pathname = "/";
  etwinBase.search = "";
  const eternaltwin = new etwin.EternaltwinClient(etwinBase);
  console.log(`Auth server: ${etwinBase.host}`);
  const credentials = await promptCredentials();
  const {session: etwinSession} = await eternaltwin.execute({
    type: etwin.RequestType.CreateSession,
    login: credentials.username,
    password: Buffer.from(credentials.password),
  });
  console.log("Credentials verified: OK");
  const token = await eternaltwin.execute({
    type: etwin.RequestType.GetOauthAuthorization,
    session: etwinSession,
    payload: login.getAuthorization,
  });
  console.log("Retrieved authorization token");
  const session: OauthReturnResponse = await eternalfest.execute({type: RequestType.OauthReturn, oauthReturn: token.oauthReturn});
  console.log("Login complete, fetching user details");
  const self = await eternalfest.execute({type: RequestType.GetAuth, session: session.session});
  const acx: AuthContext = self.value;
  switch (acx.type) {
    case ActorType.User: {
      const user = acx.user;
      await touchLocalAuth(options.uri, session.session!, user.id, user.displayName);
      console.log(`Success: You are now logged into ${apiBaseUrl.host} as ${user.displayName} (${user.id})`);
      break;
    }
    default: {
      throw new Error("Unexpected login Error");
    }
  }
}

export interface Credentials {
  username: string;
  password: string;
}

export async function promptCredentials(): Promise<Credentials> {
  const answer: { username: string, password: string } = await inquirer.prompt([
    {
      name: "username",
      type: "input",
      message: "Eternaltwin username",
    },
    {
      name: "password",
      type: "password",
      message: "Eternaltwin password",
    },
  ]);
  return answer;
}

export interface LocalSession {
  created_at: string;
  key: string;
  user: {id: string, display_name: string};
}

export async function touchLocalAuth(uri: string, key: string, id: string, dName: string): Promise<void> {
  let old: Record<string, LocalSession>;
  try {
    old = await readAppJson(["auth.json"]);
  } catch (e) {
    old = {};
  }
  const host: string = new URL(uri).host;
  old[host] = {
    created_at: new Date().toISOString(),
    key,
    user: {
      id, display_name: dName,
    }
  };
  await writeAppJson(["auth.json"], old);
}

export async function getLocalAuth(uri: string): Promise<LocalSession | null> {
  let old: Record<string, LocalSession>;
  try {
    old = await readAppJson(["auth.json"]);
  } catch (e) {
    old = {};
  }
  const host: string = new URL(uri).host;
  return old[host] ?? null;
}
