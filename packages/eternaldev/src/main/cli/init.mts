import child_process from "child_process";
import fs from "fs";
import * as furi from "furi";
import inquirer from "inquirer";
import sysPath from "path";
import url from "url";
import which from "which";

import { mkdirpAsync, outputFileAsync } from "../fs-utils.mjs";

interface InitConfig {
  /**
   * Absolute path to the Git executable.
   */
  git: string,
  /**
   * Absolute path to the Yarn executable.
   */
  yarn: string,
  name: string;
  patcher: Patcher;
  assets: boolean;
  useGit: boolean;
  updateDeps: () => Promise<boolean>;
}

enum Patcher {
  None,
  Patchman,
  External,
}

export async function init(dir: URL): Promise<void> {
  console.info("Detecting environment");
  const git = await detectExe("git");
  const haxe = await detectExe("haxe");
  const node = await detectExe("node");
  const swfmill = await detectExe("swfmill");
  const yarn = await detectExe("yarn");

  if (git === null) {
    console.error("Aborting: git is required");
    return;
  }
  if (node === null) {
    console.error("Aborting: node is required");
    return;
  }
  if (yarn === null) {
    console.error("Aborting: yarn is required");
    return;
  }

  const state: InitDirState = await checkInitDir(dir);
  if (state === InitDirState.NotADirectory) {
    console.error(`Failed to init: Not a directory: ${dir}`);
    return;
  } else if (state === InitDirState.NonEmptyDir) {
    console.error(`Failed to init: Non empty directory: ${dir}`);
    return;
  }

  console.info(`Collecting information to initialize directory: ${dir}`);
  const defaultName: string = sysPath.basename(furi.toSysPath(dir.toString()));

  interface Answer {
    name: string;
    patcher: "Patchman" | "External" | "None";
    assets: boolean;
    git: boolean;
  }

  const answer: Answer = await inquirer.prompt([
    {
      name: "name",
      type: "input",
      message: "What is the project name? (lowercase ascii and dashes)",
      default: defaultName,
    },
    {
      name: "patcher",
      type: "list",
      message: "Which patcher framework do you want to use?",
      choices: [
        "Patchman",
        "External",
        "None",
      ],
    },
    {
      name: "assets",
      type: "confirm",
      message: "Add support for custom assets?",
    },
    {
      name: "git",
      type: "confirm",
      message: "Initialize a Git repository?",
    },
  ]);

  if (answer.patcher === "Patchman" && haxe === null) {
    console.warn("Patchman requires haxe");
  }
  if (answer.assets && swfmill === null) {
    console.warn("Asset support requires swfmill");
  }

  async function updateDeps(): Promise<boolean> {
    const answer: { updateDeps: boolean } = await inquirer.prompt([{
      name: "updateDeps",
      type: "confirm",
      message: "Update and install all dependencies?",
    }]);
    return answer.updateDeps;
  }

  const config: InitConfig = {
    git,
    yarn,
    name: answer.name,
    patcher: answer.patcher === "Patchman"
      ? Patcher.Patchman
      : (answer.patcher === "External" ? Patcher.External : Patcher.None),
    assets: answer.assets,
    useGit: answer.git,
    updateDeps,
  };

  await innerInit(dir, config);
}

enum InitDirState {
  DoesNotExist,
  NotADirectory,
  EmptyDir,
  NonEmptyDir,
}

async function checkInitDir(dir: URL): Promise<InitDirState> {
  let stats: fs.Stats;
  try {
    stats = await statAsync(dir);
  } catch (err) {
    if (err instanceof Error && Reflect.get(err, "code") === "ENOENT") {
      return InitDirState.DoesNotExist;
    } else {
      throw err;
    }
  }
  if (!stats.isDirectory()) {
    return InitDirState.NotADirectory;
  }
  const content: fs.Dirent[] = await readdirAsync(dir);
  return content.length > 0 ? InitDirState.NonEmptyDir : InitDirState.EmptyDir;
}

async function statAsync(path: URL): Promise<fs.Stats> {
  return new Promise<fs.Stats>((resolve, reject) => {
    fs.stat(path as URL, (err: Error | null, stats: fs.Stats) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(stats);
      }
    });
  });
}

async function readdirAsync(path: URL): Promise<fs.Dirent[]> {
  return new Promise<fs.Dirent[]>((resolve, reject) => {
    fs.readdir(path as URL, {withFileTypes: true}, (err: Error | null, ents: fs.Dirent[]) => {
      if (err !== null) {
        reject(err);
      } else {
        resolve(ents);
      }
    });
  });
}

export async function innerInit(dir: URL, config: InitConfig): Promise<void> {
  console.info(`Initializing directory: ${dir}...`);
  await mkdirpAsync(dir as URL);

  // Remember to update `dot-yarnrc.yml` when updating the version
  console.info("Setting up Yarn 3...");
  executeCommandSync(config.yarn, ["set", "version", "3.2.4"], dir, false);

  console.info("Setting up project files...");
  await outputFileAsync(furi.join(dir, "package.json"), Buffer.from(getPackageJson(config)));
  await outputFileAsync(furi.join(dir, "eternalfest.toml"), Buffer.from(getManifestToml(config)));
  await outputFileAsync(furi.join(dir, "hfest-level-editor.xml"), Buffer.from(getLevelEditorConfig(config)));
  await copyProjectTemplateFileAs(dir, ["dot-gitattributes"], [".gitattributes"]);
  await copyProjectTemplateFileAs(dir, ["dot-yarnrc.yml"], [".yarnrc.yml"]);
  await copyProjectTemplateFile(dir, ["musics", "silence.mp3"]);
  await copyProjectTemplateFile(dir, ["src", "dimensions.xml"]);
  await copyProjectTemplateFile(dir, ["src", "score_items.xml"]);
  await copyProjectTemplateFile(dir, ["src", "special_items.xml"]);
  if (config.assets) {
    await copyProjectTemplateFile(dir, ["assets-swf.xml"]);
    await copyProjectTemplateFile(dir, ["tsconfig.json"]);
    await copyProjectTemplateFile(dir, ["src", "assets", "assets.xml"]);
    await copyProjectTemplateFileAs(dir, ["tools", "build-game.ts.template"], ["tools", "build-game.ts"]);
  }
  await copyProjectTemplateFile(dir, ["src", "haxe", "Main.hx"]);
  await copyProjectTemplateFile(dir, ["src", "levels", "adventure", "000.lvl"]);
  await copyProjectTemplateFile(dir, ["src", "levels", "adventure", "001.lvl"]);
  await copyProjectTemplateFile(dir, ["src", "levels", "deepnight", "dim0", "000.lvl"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "items.xml"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "keys.xml"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "lands.xml"]);
  await copyProjectTemplateFile(dir, ["src", "locales", "fr-FR", "statics.xml"]);
  const gitignore: string = await getGitignore(config);
  await outputFileAsync(furi.join(dir, [".gitignore"]), Buffer.from(gitignore));

  if (config.useGit) {
    console.info("Initializing Git repository...");
    executeCommandSync(config.git, ["init"], dir, true);
  }

  console.info("Initialization complete.");
  if (await config.updateDeps()) {
    // Don't update swf-* packages, the latest version isn't
    // compatible with the build-game.ts script.
    executeCommandSync(config.yarn, ["up", "!swf-*", "@*/*"], dir, false);
  }
}

async function getGitignore(config: InitConfig): Promise<string> {
  const common: string = await readProjectTemplateText(["dot-gitignore"]);
  switch (config.patcher) {
    case Patcher.External:
      return await readProjectTemplateText(["dot-gitignore-external"]) + "\n" + common;
    case Patcher.Patchman:
      return await readProjectTemplateText(["dot-gitignore-patchman"]) + "\n" + common;
    case Patcher.None:
      return common;
  }
}

export function getPackageScripts(config: InitConfig): any {
  const scripts: any = {};

  const simpleBuildSteps: string[] = [];
  const buildSteps: string[] = [];

  scripts["build:content"] = "eternalfest build";
  buildSteps.push("build:content");

  switch (config.patcher) {
    case Patcher.External:
      scripts["build:external"] = "eternalfest-external";
      buildSteps.unshift("build:external");
      simpleBuildSteps.unshift("build:external");
      break;
    case Patcher.Patchman:
      scripts["build:patchman"] = "patchman";
      buildSteps.unshift("build:patchman");
      simpleBuildSteps.unshift("build:patchman");
      scripts["idea"] = "patchman --idea";
      scripts["doc"] = "patchman --doc";
      break;
    default:
      break;
  }

  if (config.assets) {
    scripts["build:assets"] = "swfmill simple assets-swf.xml ./build/assets.swf";
    scripts["build:engine"] = "ts-node tools/build-game.ts";
    buildSteps.push("build:assets");
    buildSteps.push("build:engine");
  }

  scripts["build"] = buildSteps.map((x: string) => `yarn run ${x}`).join(" && ");
  if (simpleBuildSteps.length !== buildSteps.length) {
    scripts["simple"] = simpleBuildSteps.map((x: string) => `yarn run ${x}`).join(" && ");
  }
  scripts["prepare"] = "yarn run build";

  return sortByKeys(scripts);
}

export function getPackageDependencies(config: InitConfig): any {
  const dependencies: any = {};
  dependencies["@eternalfest/eternaldev"] = "^0.5.2";
  switch (config.patcher) {
    case Patcher.External:
      dependencies["@eternalfest/external"] = "^3.0.18";
      break;
    case Patcher.Patchman:
      dependencies["@patchman/debug"] = "^0.10.0";
      dependencies["@patchman/merlin"] = "^0.17.1";
      dependencies["@patchman/patchman"] = "^0.10.11";
      break;
    default:
      break;
  }
  if (config.assets) {
    dependencies["swf-emitter"] = "^0.11.0";
    dependencies["swf-merge"] = "^0.11.0";
    dependencies["swf-parser"] = "^0.11.0";
    dependencies["swf-types"] = "^0.11.0";
    dependencies["ts-node"] = "^10.3.0";
    dependencies["typescript"] = "^4.4.4";
  }
  return sortByKeys(dependencies);
}

function sortByKeys<T extends {}>(obj: T): T {
  const entries: [string, any][] = Object.entries(obj);
  entries.sort((left, right) => {
    if (left[0] === right[0]) {
      return 0;
    } else {
      return left[0] < right[0] ? -1 : 1;
    }
  });

  const result: Partial<T> = {};
  for (const [key, value] of entries) {
    Reflect.set(result, key, value);
  }
  return result as T;
}

function getPackageJson(config: InitConfig): string {
  const name: string = config.name;
  const pkg = {
    "name": `@eternalfest-games/${name}`,
    "version": "0.0.1",
    "homepage": `https://gitlab.com/eternalfest/games/${name}`,
    "description": `Project for the game \`${name}\``,
    "repository": {
      type: "git",
      url: `git://gitlab.com:eternalfest/games/${name}.git`,
    },
    "scripts": getPackageScripts(config),
    "private": true,
    "publishConfig": {
      access: "restricted",
      registry: "https://npm.eternal-twin.net/",
    },
    "pre-commit": {
      run: [
        "build",
      ],
    },
    "keywords": ["game"],
    "license": "UNLICENSED",
    "patchman": getPatchmanConfig(),
    "engines": {
      node: ">=14.0.0",
    },
    "dependencies": getPackageDependencies(config),
  };

  function getPatchmanConfig(): object | undefined {
    if (config.patcher === Patcher.Patchman) {
      return {
        "haxe": "src/haxe",
        "haxeMain": "Main",
        "outFile": "./build/patchman.swf",
        "outDocDir": "./build/docs",
      };
    } else {
      return undefined;
    }
  }

  return `${JSON.stringify(pkg, null, 2)}\n`;
}

function getManifestToml(config: InitConfig): string {
  return [
    "manifest = 1",
    `display_name = ${JSON.stringify(config.name)}`,
    `description = ${JSON.stringify(`Description for the game ${config.name}`)}`,
    "# category = \"Other\"",
    "# icon = \"./icon.png\"",
    "# remote_id = \"00000000-0000-0000-0000-000000000000\"",
    `repository = ${JSON.stringify(getRepository(config))}`,
    "families = \"0,1,2,3,4,10,11,12,19,100,101,102,103,104,105,106,107,108,109,110,111,112,113,1000,1001,1002,1003,1004,1005,1006,1007,1008,1009,1010,1011,1012,1013,1014,1015,1016,1017,1018,1023,1025,1026,1030\"",
    "main_locale = \"fr-FR\"",
    `${config.assets ? "" : "# "}hf = "./build/game.swf"`,
    "musics = [",
    "  \"./musics/silence.mp3\",",
    "  \"./musics/silence.mp3\",",
    "  \"./musics/silence.mp3\",",
    "]",
    `${config.assets ? "" : "# "}assets = "./src/assets/assets.xml"`,
    "",
    "[obf]",
    "key = \"TODO: Do not use a hardcoded key\"",
    "",
    "[content]",
    "build = \"./build/game.xml\"",
    "score_items = \"./src/score_items.xml\"",
    "special_items = \"./src/special_items.xml\"",
    "dimensions = \"./src/dimensions.xml\"",
    "data = \"./src/data\"",
    "",
    "[content.levels.0]",
    "levels = \"./src/levels/adventure\"",
    "var = \"xml_adventure\"",
    "did = \"0\"",
    "[content.levels.1]",
    "levels = \"./src/levels/deepnight\"",
    "var = \"xml_deepnight\"",
    "padding = true",
    "did = \"1\"",
    "",
    "[patchman]",
    "build = \"./build/patchman.swf\"",
    "",
    "[modes.solo]",
    "display_name = \"Aventure\"",
    "state = \"enabled\"",
    "[modes.solo.options]",
    "mirror = {display_name = \"Miroir\", state = \"enabled\"}",
    "nightmare = {display_name = \"Cauchemar\", state = \"enabled\"}",
    "ninja = {display_name = \"Ninjutsu\", state = \"enabled\"}",
    "# nojutsu = {display_name = \"Nojutsu\", state = \"enabled\"}",
    "bombexpert = {display_name = \"Explosifs Instables\",state = \"enabled\"}",
    "boost = {display_name = \"Tornade\", state = \"enabled\"}",
    "",
    "[modes.multicoop]",
    "display_name = \"Multi Coopératif\"",
    "state = \"enabled\"",
    "[modes.multicoop.options]",
    "mirrormulti = {display_name = \"Miroir\", state = \"enabled\"}",
    "nightmaremulti = {display_name = \"Cauchemar\", state = \"enabled\"}",
    "bombexpert = {display_name = \"Explosifs Instables\", state = \"enabled\"}",
    "boost = {display_name = \"Tornade\", state = \"enabled\"}",
    "lifesharing = {display_name = \"Partage de vies\", state = \"enabled\"}",
    "",
    "[locales.fr-FR]",
    "build = \"./build/locales.fr-FR.xml\"",
    "[locales.fr-FR.content]",
    "keys = \"./src/locales/fr-FR/keys.xml\"",
    "items = \"./src/locales/fr-FR/items.xml\"",
    "lands = \"./src/locales/fr-FR/lands.xml\"",
    "statics = \"./src/locales/fr-FR/statics.xml\"",
    "",
  ].join("\n");
}

function getLevelEditorConfig(config: InitConfig): string {
  const lines: string[] = [];
  lines.push("<config>");
  if (config.assets) {
    lines.push("  <assets>");
    lines.push("    <asset>src/assets/assets.xml</asset>");
    lines.push("  </assets>");
  }
  lines.push("</config>");
  lines.push("");
  return lines.join("\n");
}

function getRepository(config: InitConfig) {
  return `https://gitlab.com/eternalfest/games/${config.name}`;
}

async function readProjectTemplateText(parts: readonly string[]): Promise<string> {
  const furi: URL = resolveProjectTemplateFuri(parts);
  return fs.promises.readFile(furi as URL, {encoding: "utf-8"});
}

async function copyProjectTemplateFile(outDir: URL, parts: readonly string[]): Promise<void> {
  await copyProjectTemplateFileAs(outDir, parts, parts);
}

async function copyProjectTemplateFileAs(outDir: URL, input: readonly string[], output: readonly string[]): Promise<void> {
  const src: URL = resolveProjectTemplateFuri(input);
  const dest: URL = furi.join(outDir.toString(), output);
  const destDir: URL = furi.join(dest.toString(), "..");
  await fs.promises.mkdir(destDir as URL, {recursive: true});
  await fs.promises.copyFile(src as URL, dest as URL);
}

const RESOURCE_DIR: URL = furi.join(import.meta.url, "../../../main-resources");

function resolveProjectTemplateFuri(parts: readonly string[]): URL {
  return furi.join(RESOURCE_DIR.toString(), "project-template", parts);
}

function executeCommandSync(command: string, args: string[], dir: URL, quiet: boolean): void {
  const stdout = quiet ? "ignore" : "inherit";
  const stderr = quiet ? "ignore" : "inherit";
  const child = child_process.spawnSync(command, args, {
    cwd: url.fileURLToPath(dir as URL),
    stdio: ["pipe", stdout, stderr],
  });

  if (child.status !== 0) {
    const msg = "Failed to execute command: command exited with non-zero status: " + child.status;
    throw new Error(msg);
  }
}

async function detectExe(exeName: string): Promise<string | null> {
  let resolved: string | null;
  try {
    resolved = await which(exeName);
  } catch (e: unknown) {
    if (e instanceof Error && Reflect.get(e, "code") === "ENOENT") {
      resolved = null;
    } else {
      throw e;
    }
  }
  console.info(`${exeName}${ resolved === null ? " not found" : ": " + resolved}`);
  return resolved;
}
