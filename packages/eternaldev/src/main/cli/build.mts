import { LocaleId } from "@eternalfest/core/types/locale-id";
import { CompilerResult as Jcr } from "@eternalfest/efc/compiler-result";
import {
  createTtyReporter,
  DiagnosticCategory,
  DiagnosticReporter,
  DiagnosticReportSummary
} from "@eternalfest/efc/diagnostic/report";
import * as efcConfig from "@eternalfest/efc/java/config";
import { compile as efcCompile, Compiled } from "@eternalfest/efc/project";
import { getObfuMapUri } from "@eternalfest-types/loader";
import { Buffer } from "buffer";
import * as furi from "furi";
import { firstValueFrom } from "rxjs";
import stream from "stream";

import { Manifest } from "../api/manifest/manifest.mjs";
import { ManifestContent } from "../api/manifest/manifest-content.mjs";
import { ManifestLevel } from "../api/manifest/manifest-level.mjs";
import { ManifestLevelKey } from "../api/manifest/manifest-level-key.mjs";
import { ManifestLocale } from "../api/manifest/manifest-locale.mjs";
import { CompilerResult as Cr } from "../api/project/compiler-result.mjs";
import { observeManifestByDir } from "../api/project/local-service.mjs";
import { outputFileAsync } from "../fs-utils.mjs";

export type ExitCode = number;

export async function build(dir: URL, outStream: stream.Writable): Promise<ExitCode> {
  try {
    const manifestCr: Cr<Manifest | undefined> = await firstValueFrom(observeManifestByDir(dir));
    if (!manifestCr.hasValue()) {
      const msg = `Failed to load project ${dir}:\n${JSON.stringify(manifestCr.diagnostics)}`;
      outStream.write(Buffer.from(msg));
      return 1;
    }
    const manifest: Manifest | undefined = manifestCr.unwrap();
    if (manifest === undefined) {
      outStream.write(Buffer.from(`NonProjectDirectory: ${dir}`));
      return 1;
    }

    if (manifest.content === undefined) {
      outStream.write(Buffer.from(`Missing manifest section \`content\`: ${dir}`));
      return 1;
    }

    const content: ManifestContent = manifest.content;

    const assetRoots: URL[] = [];
    if (manifest.assets !== undefined) {
      assetRoots.push(furi.join(dir.toString(), manifest.assets));
    }
    const assets: efcConfig.EfcAssetsConfig = {
      useBaseAssets: true,
      roots: assetRoots,
    };

    const efcConfig: efcConfig.EfcConfig = {
      root: dir,
      levels: resolveSets(dir, content.levels),
      patcherData: furi.join(dir.toString(), content.data !== undefined ? content.data : "datas"),
      scoreItems: furi.join(dir.toString(), content.scoreItems),
      specialItems: furi.join(dir.toString(), content.specialItems),
      dimensions: furi.join(dir.toString(), content.dimensions),
      locales: resolveLocales(dir, manifest.locales),
      assets,
      prettyOutput: false,
      obf: resolveObf(dir, manifest.obf),
      separateLocales: true,
    };

    const compiledCr: Jcr<Compiled> = await firstValueFrom(efcCompile(efcConfig));

    const reporter: DiagnosticReporter = createTtyReporter(outStream);
    const summary: DiagnosticReportSummary = reporter(compiledCr.diagnostics);
    const numErrors: number = summary.byCategory.get(DiagnosticCategory.Error)!;
    const numWarnings: number = summary.byCategory.get(DiagnosticCategory.Warning)!;

    if (numErrors > 0) {
      outStream.write(Buffer.from(
        `Compilation failed with ${numErrors} error(s) and ${numWarnings} warning(s).\n`,
      ));
      return 1;
    } else {
      const data: Compiled = compiledCr.unwrap();
      const contentBuild: URL = furi.join(dir.toString(), content.build);
      await outputFileAsync(contentBuild, Buffer.from(`${data.content}\n`));

      if (manifest.locales !== undefined) {
        for (const [localeId, localeMeta] of manifest.locales) {
          const localeContent = data.locales.get(localeId);
          if (localeContent === undefined) {
            outStream.write(Buffer.from(`Missing efc output for locale: ${localeId}`));
            return 1;
          }
          const localeBuild = furi.join(dir.toString(), localeMeta.build);
          await outputFileAsync(localeBuild, Buffer.from(`${localeContent}\n`));
        }
      }

      if (numWarnings > 0) {
        outStream.write(Buffer.from(`Compilation finished with ${numWarnings} warning(s).\n`));
      } else {
        outStream.write(Buffer.from("Compilation finished.\n"));
      }
      return 0;
    }
  } catch (err: unknown) {
    if (err instanceof Error) {
      outStream.write(Buffer.from(`InternalCompilerError:\n${err.stack}\n`));
    } else {
      outStream.write(Buffer.from(`InternalCompilerError:\n${err}\n`));
    }
    return 1;
  }
}

function resolveSets(dir: URL, levels: ManifestContent["levels"]): efcConfig.EfcConfig["levels"] {
  if (typeof levels === "string") {
    return furi.join(dir.toString(), levels);
  }
  const sets: Map<ManifestLevelKey, efcConfig.EfcLevelSetConfig> = new Map();
  for (const [setKey, levelConfig] of levels) {
    sets.set(setKey, resolveSet(dir, levelConfig));
  }
  return sets;
}

function resolveSet(dir: URL, set: ManifestLevel): efcConfig.EfcLevelSetConfig {
  return {
    dir: furi.join(dir.toString(), set.levels),
    did: set.did,
    varName: set.var,
    padding: set.padding !== undefined ? set.padding : false,
    equalSize: set.sameSize !== undefined ? set.sameSize : false,
  };
}

function resolveLocales(dir: URL, locales: Manifest["locales"]): efcConfig.EfcConfig["locales"] {
  const resolved: Map<LocaleId, efcConfig.EfcLocaleConfig> = new Map();
  if (locales !== undefined) {
    for (const [localeId, locale] of locales) {
      resolved.set(localeId, resolveLocale(dir, locale));
    }
  }
  return resolved;
}

function resolveLocale(dir: URL, locale: ManifestLocale): efcConfig.EfcLocaleConfig {
  if (typeof locale.content === "string") {
    return {
      items: furi.join(dir.toString(), locale.content, "items.xml"),
      keys: furi.join(dir.toString(), locale.content, "keys.xml"),
      lands: furi.join(dir.toString(), locale.content, "lands.xml"),
      statics: furi.join(dir.toString(), locale.content, "statics.xml"),
    };
  } else {
    return {
      items: furi.join(dir.toString(), locale.content.items),
      keys: furi.join(dir.toString(), locale.content.keys),
      lands: furi.join(dir.toString(), locale.content.lands),
      statics: furi.join(dir.toString(), locale.content.statics),
    };
  }
}

function resolveObf(_dir: URL, obf: Manifest["obf"]): efcConfig.EfcConfig["obf"] {
  if (obf === undefined) {
    return undefined;
  }
  const parentMap: URL = getObfuMapUri();
  return {key: obf.key, parentMap: parentMap};
}
