// import {LocaleId} from "@eternal-twin/core/core/locale-id";
// import {$UserAuthContext, UserAuthContext} from "@eternalfest/core/auth/auth-context";
// import {$Blob, Blob} from "@eternalfest/core/blob/blob";
// import {BlobIdRef, NullableBlobIdRef} from "@eternalfest/core/blob/blob-id-ref";
// import {$CreateBlobOptions} from "@eternalfest/core/blob/create-blob-options";
// import {$CreateUploadSessionOptions} from "@eternalfest/core/blob/create-upload-session-options";
// import {$UploadBytesOptions} from "@eternalfest/core/blob/upload-bytes-options";
// import {$UploadSession, UploadSession} from "@eternalfest/core/blob/upload-session";
// import {LocalFileMap} from "@eternalfest/core/eternaldev/local-file-map";
// import {LocalFileRef} from "@eternalfest/core/eternaldev/local-file-ref";
// import {Project} from "@eternalfest/core/eternaldev/project";
// import {$CreateGameOptions, CreateGameOptions} from "@eternalfest/core/game2/create-game-options";
// import {$Game, Game} from "@eternalfest/core/game2/game";
// import {$GameBuild, GameBuild} from "@eternalfest/core/game2/game-build";
// import {$GameChannelPatch, GameChannelPatch} from "@eternalfest/core/game2/game-channel-patch";
// import {GameChannelPermission} from "@eternalfest/core/game2/game-channel-permission";
// import {GameEngineType} from "@eternalfest/core/game2/game-engine-type";
// import {GameId} from "@eternalfest/core/game2/game-id";
// import {$InputGameBuild, InputGameBuild} from "@eternalfest/core/game2/input-game-build";
// import {InputGameBuildI18n} from "@eternalfest/core/game2/input-game-build-i18n";
// import {InputGameEngine} from "@eternalfest/core/game2/input-game-engine";
// import {NullableInputGamePatcher} from "@eternalfest/core/game2/input-game-patcher";
// import {InputGameResource} from "@eternalfest/core/game2/input-game-resource";
// import {ObjectType} from "@eternalfest/core/types/object-type";
// import {Url} from "@eternalfest/core/types/url";
// import {unreachable} from "@eternalfest/efc/utils";
// import {$UpdateGameChannelBody} from "@eternalfest/rest-server/games";
// import childProcess from "child_process";
// import fs from "fs";
// import furi from "furi";
// import incident from "incident";
// import inquirer from "inquirer";
// import {IoType} from "kryo";
// import {$Any} from "kryo/any";
// import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
// import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
// import {PRETTY_JSON_WRITER} from "kryo-json/json-writer";
// import {QS_VALUE_WRITER} from "kryo-qs/qs-value-writer";
// import readline from "readline";
// import superagent from "superagent";
// import urlJoin from "url-join";
//
// import {LocalFileService} from "../api/file/local-service.mjs";
// import {LocalProjectService} from "../api/project/local-service.mjs";
//
// type Agent = superagent.SuperAgent<superagent.SuperAgentRequest>;
//
// class ApiClient {
//   readonly #agent: Agent;
//   readonly #apiBase: Url;
//
//   constructor(apiBase: Url, credentials: Credentials) {
//     this.agent = superagent.agent();
//     this.apiBase = apiBase;
//     this.credentials = credentials;
//   }
//
//   async get<Res>(
//     url: string,
//     resType: IoType<Res>,
//   ): Promise<Res> {
//     const res: superagent.Response = await this.agent
//       .get(this.resolve(url).toString())
//       .auth(this.credentials.username, this.credentials.password, {type: "basic"})
//       .send();
//     try {
//       return readJsonResponse(resType, res.body);
//     } catch (e) {
//       console.error(`Response (${res.status}):`);
//       console.error(JSON.stringify(res.body));
//       throw e;
//     }
//   }
//
//   async patch<Req, Res>(
//     url: string,
//     req: Req,
//     reqType: IoType<Req>,
//     resType: IoType<Res>,
//   ): Promise<Res> {
//     const rawReq: any = reqType.write(JSON_VALUE_WRITER, req);
//     const res: superagent.Response = await this.agent
//       .patch(this.resolve(url).toString())
//       .auth(this.credentials.username, this.credentials.password, {type: "basic"})
//       .send(rawReq);
//     try {
//       return readJsonResponse(resType, res.body);
//     } catch (e) {
//       console.error("Request:");
//       console.error(JSON.stringify(rawReq));
//       console.error(`Response (${res.status}):`);
//       console.error(JSON.stringify(res.body));
//       throw e;
//     }
//   }
//
//   async post<Req, Res>(
//     url: string,
//     req: Req,
//     reqType: IoType<Req>,
//     resType: IoType<Res>,
//   ): Promise<Res> {
//     const rawReq: any = reqType.write(JSON_VALUE_WRITER, req);
//     const res: superagent.Response = await this.agent
//       .post(this.resolve(url).toString())
//       .auth(this.credentials.username, this.credentials.password, {type: "basic"})
//       .send(rawReq);
//     try {
//       return readJsonResponse(resType, res.body);
//     } catch (e) {
//       console.error("Request:");
//       console.error(JSON.stringify(rawReq));
//       console.error(`Response (${res.status}):`);
//       console.error(JSON.stringify(res.body));
//       throw e;
//     }
//   }
//
//   async put<Query, Req, Res>(
//     url: string,
//     query: Query,
//     queryType: IoType<Query>,
//     req: Req,
//     reqType: IoType<Req>,
//     resType: IoType<Res>,
//   ): Promise<Res> {
//     const rawQuery: any = queryType.write(QS_VALUE_WRITER, query);
//     const rawReq: any = reqType.write(JSON_VALUE_WRITER, req);
//     console.log(this.resolve(url).toString());
//     const res: superagent.Response = await this.agent
//       .put(this.resolve(url).toString())
//       .auth(this.credentials.username, this.credentials.password, {type: "basic"})
//       .query(rawQuery)
//       .send(rawReq);
//     try {
//       return readJsonResponse(resType, res.body);
//     } catch (e) {
//       console.error("Request:");
//       console.error(JSON.stringify(rawReq));
//       console.error(`Response (${res.status}):`);
//       console.error(JSON.stringify(res.body));
//       throw e;
//     }
//   }
//
//   private resolve(uri: string): Url {
//     return new Url(urlJoin(this.apiBase.toString(), uri));
//   }
// }
//
// export function readJsonResponse<T>(type: IoType<T>, raw: any): T {
//   if (typeof raw.error === "string") {
//     throw new incident.Incident(raw.error, raw);
//   } else {
//     return type.read(JSON_VALUE_READER, raw);
//   }
// }
