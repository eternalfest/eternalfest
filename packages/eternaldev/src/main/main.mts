#!/usr/bin/env node

import { Url } from "@eternalfest/core/types/url";
import devkit from "@eternalfest/devkit";
import { execCli as execEfcCli } from "@eternalfest/efc/cli";
import * as furi from "furi";
import * as yargs from "yargs";

import { build } from "./cli/build.mjs";
import { init } from "./cli/init.mjs";
import {login} from "./cli/login.mjs";
import { uploadProject } from "./cli/upload-project.mjs";
import { startServer } from "./main.server.mjs";
import { VERSION } from "./version.mjs";

const startCommand: yargs.CommandModule<any, any> = {
  command: ["start"],
  aliases: [],
  builder: {
    throttle: {
      type: "number",
      default: 0,
    }
  },
  describe: "Start Eternaldev in the current directory",
  handler: (args: any) => {
    console.log("Starting development server...");
    const {throttle} = args;
    if (typeof throttle !== "number") {
      throw new Error("InvalidCliArguments: expected --throttle to be a number");
    }

    startServer({
      filesThrottle: throttle,
      // No need for commit refs for local testing.
      noGitIntegration: true,
      // Make all modes and options visible and enabled to so they are usable in eternaldev.
      ignoreModeAndOptionRestrictions: true,
    })
      .catch((err: Error): never => {
        console.error(err);
        process.exit(1);
      });
  },
};

const DEFAULT_API_BASE: string = "https://eternalfest.net/";

const buildCommand: yargs.CommandModule<any, any> = {
  command: ["build"],
  aliases: [],
  describe: "Build the current project",
  handler: async (_args: yargs.Arguments): Promise<never> => {
    const exitCode = await build(furi.fromSysPath(process.cwd()), process.stderr);
    process.exit(exitCode);
  },
};

const initCommand: yargs.CommandModule<any, any> = {
  command: ["init [name]"],
  aliases: [],
  describe: "Initialize the provided directory",
  handler: async (args: yargs.Arguments<{name?: string}>): Promise<never> => {
    try {
      const name: string | undefined = args.name;
      const cwd: Url = furi.fromSysPath(process.cwd());
      const dir: Url = name !== undefined ? furi.join(cwd.toString(), name) : cwd;
      await init(dir);
      process.exit(0);
    } catch (err) {
      if (err instanceof  Error) {
        console.error(err.stack);
      }
      process.exit(1);
    }
  },
};

const loginCommand: yargs.CommandModule<any, any> = {
  command: ["login"],
  aliases: [],
  builder: {
    api: {
      type: "string",
      default: DEFAULT_API_BASE,
    },
  },
  describe: "Create a local session for command line operations",
  handler: async (args: yargs.Arguments): Promise<never> => {
    try {
      const {api} = args;
      if (typeof api !== "string") {
        throw new Error("InvalidCliArguments: expected --api to be a string");
      }
      await login({uri: api});
      process.exit(0);
    } catch (err) {
      if (err instanceof Error) {
        console.error(err.stack);
      }
      process.exit(1);
    }
  },
};

const uploadCommand: yargs.CommandModule<any, any> = {
  command: ["upload"],
  aliases: [],
  builder: {
    api: {
      type: "string",
      default: DEFAULT_API_BASE,
    },
    ignoreCommitRef: {
      boolean: true,
      default: false,
    },
  },
  describe: "Upload the current game",
  handler: async (args: yargs.Arguments): Promise<never> => {
    try {
      const {api, ignoreCommitRef} = args;
      if (typeof api !== "string") {
        throw new Error("InvalidCliArguments: expected --api to be a string");
      }
      if (typeof ignoreCommitRef !== "boolean") {
        throw new Error("InvalidCliArguments: expected --ignore-commit-ref to be a boolean");
      }
      await uploadProject({api, ignoreCommitRef});
      process.exit(0);
    } catch (err) {
      if (err instanceof Error) {
        console.error(err.stack);
      }
      process.exit(1);
    }
  },
};

const levelEditorCommand: yargs.CommandModule<any, {}> = {
  command: ["level-editor"],
  aliases: ["editor"],
  describe: "Start the level editor",
  handler: (_args: {}) => {
    devkit.levelEditor.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const mapperCommand: yargs.CommandModule<any, {}> = {
  command: ["mapper"],
  aliases: [],
  describe: "Start the mapper",
  handler: (_args: {}) => {
    devkit.mapper.exec({args: [], cwd: process.cwd(), stdio: "inherit"});
  },
};

const ARG_PARSER: yargs.Argv = yargs.default(process.argv);

ARG_PARSER
  .command(startCommand)
  .command(initCommand)
  .command(levelEditorCommand)
  .command(loginCommand)
  .command(mapperCommand)
  .command(buildCommand)
  .command(uploadCommand)
  .command("*", false, {}, (): void => {
    ARG_PARSER.showHelp();
  })
  .scriptName("eternalfest")
  .help()
  .locale("en");

async function main() {
  switch (process.argv[2]) {
    case "--version":
      console.log(VERSION);
      break;
    case "level-editor":
    case "editor":
      await devkit.levelEditor.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
      break;
    case "mapper":
      await devkit.mapper.exec({args: process.argv.slice(3), cwd: process.cwd(), stdio: "inherit"}).toPromise();
      break;
    case "efc": {
      const args: string[] = process.argv.slice(3);
      const cwd: string = furi.fromSysPath(process.cwd()).toString();
      const returnCode: number = await execEfcCli(args, cwd, process);
      if (returnCode !== 0) {
        process.exit(returnCode);
      }
      break;
    }
    default:
      ARG_PARSER.parse(process.argv.slice(2));
  }
}

main()
  .then(
    undefined,
    (err: Error): never => {
      console.error(err);
      return process.exit(1) as never;
    });
