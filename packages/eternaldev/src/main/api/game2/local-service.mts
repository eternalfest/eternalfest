import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {PartialProfile} from "@eternalfest/core/eternaldev/partial-profile";
import {ProfileKey} from "@eternalfest/core/eternaldev/profile-key";
import {Project} from "@eternalfest/core/eternaldev/project";
import {ActiveGameChannel} from "@eternalfest/core/game2/active-game-channel";
import {CreateGameBuildOptions} from "@eternalfest/core/game2/create-game-build-options";
import {CreateGameOptions} from "@eternalfest/core/game2/create-game-options";
import {Game, NullableGame} from "@eternalfest/core/game2/game";
import {GameBuild} from "@eternalfest/core/game2/game-build";
import {GameChannelPermission} from "@eternalfest/core/game2/game-channel-permission";
import {GameId} from "@eternalfest/core/game2/game-id";
import {GameIdRef} from "@eternalfest/core/game2/game-ref";
import {GetGameOptions} from "@eternalfest/core/game2/get-game-options";
import {GetGamesOptions} from "@eternalfest/core/game2/get-games-options";
import {Game2Service} from "@eternalfest/core/game2/service";
import {SetGameFavoriteOptions} from "@eternalfest/core/game2/set-game-favorite-options";
import {ShortGameListing} from "@eternalfest/core/game2/short-game-listing";
import {UpdateGameChannelOptions} from "@eternalfest/core/game2/update-game-channel-options";
import {DeepReadonly} from "@eternalfest/core/types/deep-readonly";
import {ObjectType} from "@eternalfest/core/types/object-type";
import incident from "incident";

import {LocalProjectService} from "../project/local-service.mjs";
import {DEFAULT_USER_DISPLAY_NAME, DEFAULT_USER_ID} from "../user/in-memory-service.mjs";

export class LocalGame2Service implements Game2Service {
  readonly #project: LocalProjectService;

  constructor(project: LocalProjectService) {
    this.#project = project;
  }

  setGameFavorite(_acx: AuthContext, _options: DeepReadonly<SetGameFavoriteOptions>): Promise<unknown> {
    throw new Error("LocalGame2Service does not support favorites.");
  }

  async createGame(_acx: AuthContext, _options: DeepReadonly<CreateGameOptions>): Promise<Game> {
    throw new Error("LocalGame2Service does not support game creation.");
  }

  async getGame(_acx: AuthContext, options: DeepReadonly<GetGameOptions>): Promise<NullableGame> {
    const project: Project | undefined = await this.#project.getProjectById((options.game as GameIdRef).id);
    if (project === undefined) {
      return null;
    }
    return this.gameFromProject(project);
  }

  async getGames(_acx: AuthContext, _options: DeepReadonly<GetGamesOptions>): Promise<ShortGameListing> {
    throw new incident.Incident("NotImplemented: LocalGame2Service#getGames");
  }

  async createBuild(_acx: AuthContext, _options: DeepReadonly<CreateGameBuildOptions>): Promise<GameBuild> {
    throw new incident.Incident("NotImplemented: LocalGame2Service#createBuild");
  }

  async updateChannel(_acx: AuthContext, _options: DeepReadonly<UpdateGameChannelOptions>): Promise<ActiveGameChannel> {
    throw new incident.Incident("NotImplemented: LocalGame2Service#updateChannel");
  }

  async getDebugProfile(gameId: GameId, profileKey?: ProfileKey): Promise<PartialProfile> {
    const project: Project | undefined = await this.#project.getProjectById(gameId);
    if (project === undefined) {
      throw new Error("ProjectNotFound");
    }
    const result: PartialProfile | undefined = profileKey !== undefined ? project.profiles.get(profileKey) : undefined;
    return result !== undefined ? result : {};
  }

  private async gameFromProject(project: Project): Promise<Game> {
    const localBuild = project.game;

    const game: Game = {
      type: ObjectType.Game,
      id: project.id,
      createdAt: new Date(),
      key: null,
      owner: {
        type: ObjectType.User,
        id: DEFAULT_USER_ID,
        displayName: DEFAULT_USER_DISPLAY_NAME,
      },
      channels: {
        offset: 0,
        limit: 1,
        count: 1,
        isCountExact: true,
        active: {
          type: ObjectType.GameChannel,
          key: "main",
          isEnabled: true,
          isPinned: false,
          publicationDate: new Date(),
          sortUpdateDate: new Date(),
          defaultPermission: GameChannelPermission.Manage,
          build: localBuild
        },
        items: [],
      }
    };
    return game;
  }
}
