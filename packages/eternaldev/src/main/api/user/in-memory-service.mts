import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { HfestIdentity } from "@eternalfest/core/hfest-identity/hfest-identity";
import {ObjectType} from "@eternalfest/core/types/object-type";
import { CreateUserWithCredentialsOptions } from "@eternalfest/core/user/create-user-with-credentials-options";
import { UserService } from "@eternalfest/core/user/service";
import { UpdateUserOptions } from "@eternalfest/core/user/update-user-options";
import { User } from "@eternalfest/core/user/user";
import { UserDisplayName } from "@eternalfest/core/user/user-display-name";
import { UserListing } from "@eternalfest/core/user/user-listing";
import { UserRef } from "@eternalfest/core/user/user-ref";
import { UuidHex } from "kryo/uuid-hex";

export const DEFAULT_USER_ID: UuidHex = "00000000-0000-0000-0000-000000000000";
export const DEFAULT_USER_DISPLAY_NAME: UserDisplayName = "Eternaldev";

export class InMemoryUserService implements UserService {
  async createUserWithCredentials(
    _auth: AuthContext,
    _options: CreateUserWithCredentialsOptions,
  ): Promise<User> {
    return {
      id: DEFAULT_USER_ID,
      type: ObjectType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async createUserWithHfestIdentity(
    _auth: AuthContext,
    _hfestIdentity: HfestIdentity,
  ): Promise<User> {
    return {
      id: DEFAULT_USER_ID,
      type: ObjectType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async getUserById(_auth: AuthContext, _userId: UuidHex): Promise<User | undefined> {
    return {
      id: DEFAULT_USER_ID,
      type: ObjectType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async getUsers(_auth: AuthContext): Promise<UserListing> {
    const items = [{
      id: DEFAULT_USER_ID,
      type: ObjectType.User as const,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    }];
    return {offset: 0, limit: 1, count: 1, items};
  }

  async updateUser(_auth: AuthContext, _options: UpdateUserOptions): Promise<User> {
    return {
      id: DEFAULT_USER_ID,
      type: ObjectType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }

  async getUserRefById(
    _auth: AuthContext,
    userId: string,
  ): Promise<UserRef | undefined> {
    if (userId !== DEFAULT_USER_ID) {
      return undefined;
    }
    return {
      id: DEFAULT_USER_ID,
      type: ObjectType.User,
      displayName: DEFAULT_USER_DISPLAY_NAME,
    };
  }

  async getUserRefsById(
    auth: AuthContext,
    _userIds: ReadonlySet<UuidHex>,
  ): Promise<Map<UuidHex, UserRef | undefined>> {
    const user: UserRef | undefined = await this.getUserRefById(auth, DEFAULT_USER_ID);
    return new Map([[DEFAULT_USER_ID, user]]);
  }

  async getOrCreateUserWithEtwin(_auth: AuthContext, userId: UuidHex, userDisplay: UserDisplayName): Promise<User> {
    return {
      id: userId,
      type: ObjectType.User,
      displayName: userDisplay,
      identities: [],
      createdAt: new Date(0),
      updatedAt: new Date(0),
      isAdministrator: true,
      isTester: true,
    };
  }
}
