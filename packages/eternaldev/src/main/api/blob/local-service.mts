import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { Blob } from "@eternalfest/core/blob/blob";
import { CreateBlobOptions } from "@eternalfest/core/blob/create-blob-options";
import { CreateUploadSessionOptions } from "@eternalfest/core/blob/create-upload-session-options";
import { GetBlobOptions } from "@eternalfest/core/blob/get-blob-options";
import { MediaType } from "@eternalfest/core/blob/media-type";
import { BlobService } from "@eternalfest/core/blob/service";
import { UploadOptions } from "@eternalfest/core/blob/upload-options";
import { UploadSession } from "@eternalfest/core/blob/upload-session";
import { Url } from "@eternalfest/core/types/url";
import { toUuid } from "@eternalfest/core/utils/helpers";
import fs from "fs";
import incident from "incident";
import { UuidHex } from "kryo/uuid-hex";
import stream from "stream";

function sleep(timeoutMs: number): Promise<void> {
  return new Promise((resolve) => {
    setTimeout(resolve, timeoutMs);
  });
}

class ThrottleGroup {
  #bytesPerSecond: number;
  #maxChunkSize: number;

  constructor(bytesPerSecond: number) {
    this.#bytesPerSecond = bytesPerSecond;
    this.#maxChunkSize = Math.max(Math.floor(bytesPerSecond / 10));
  }

  public createStream(): stream.Duplex {
    // TODO: Throttling should be applied between all elements of the group
    const self = this;
    return stream.Duplex.from(async function*(readable: AsyncIterable<Uint8Array>): AsyncGenerator<Uint8Array, void, undefined> {
      for await (const chunk of await readable) {
        for (let i = 0; i < chunk.length; i += self.#maxChunkSize) {
          const subChunk = chunk.slice(i, i + self.#maxChunkSize);
          const milliseconds = Math.floor(1000 * subChunk.length / self.#bytesPerSecond);
          await sleep(milliseconds);
          yield subChunk;
        }
      }
    });
  }
}

interface LocalFile {
  url: Url;
}

interface LocalBlobServiceOptions {
  throttle?: number;
}

async function blobFromLocalFile(localFile: LocalFile): Promise<Blob> {
  const id: UuidHex = toUuid(localFile.url.toString());
  let mediaType: MediaType;
  if (localFile.url.toString().match(/\.swf$/)) {
    mediaType = "application/x-shockwave-flash";
  } else if (localFile.url.toString().match(/\.xml$/)) {
    mediaType = "application/xml";
  }  else if (localFile.url.toString().match(/\.mp3$/)) {
    mediaType = "audio/mp3";
  }  else if (localFile.url.toString().match(/\.png$/)) {
    mediaType = "image/png";
  } else {
    mediaType = "application/octet-stream";
  }

  const stats: fs.Stats = fs.statSync(localFile.url as URL);

  return {
    id,
    mediaType,
    byteSize: stats.size,
  };
}

export class LocalBlobService implements BlobService {
  readonly #idToLocalFile: Map<UuidHex, LocalFile>;
  readonly #throttle: ThrottleGroup | null;

  constructor(options: LocalBlobServiceOptions = {}) {
    this.#idToLocalFile = new Map();
    if (options.throttle === undefined || options.throttle <= 0) {
      this.#throttle = null;
    } else {
      this.#throttle = new ThrottleGroup(options.throttle);
    }
  }

  hasImmutableBlobs: boolean = false;

  createBlob(_acx: AuthContext, _options: Readonly<CreateBlobOptions>): Promise<Blob> {
    throw new incident.Incident("NotImplemented: LocalBlobService#createBlob");
  }

  async getBlobById(_acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<Blob> {
    const localFile: LocalFile | undefined = this.#idToLocalFile.get(options.id);
    if (localFile === undefined) {
      throw new Error("FileNotFound");
    }
    return blobFromLocalFile(localFile);
  }

  async readBlobData(_acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<stream.Readable> {
    const localFile: LocalFile | undefined = this.#idToLocalFile.get(options.id);
    if (localFile === undefined) {
      throw new Error("FileNotFound");
    }
    const stream = fs.createReadStream(localFile.url as URL);
    if (this.#throttle === null) {
      return stream;
    } else {
      return stream.pipe(this.#throttle.createStream());
    }
  }

  createUploadSession(_acx: AuthContext, _options: Readonly<CreateUploadSessionOptions>): Promise<UploadSession> {
    throw new incident.Incident("NotImplemented: LocalBlobService#createUploadSession");
  }

  uploadBytes(_acx: AuthContext, _options: Readonly<UploadOptions>): Promise<UploadSession> {
    throw new incident.Incident("NotImplemented: LocalBlobService#uploadBytes");
  }

  createBlobUuidFromUrl(fileUrl: Url): UuidHex {
    const id: UuidHex = toUuid(fileUrl.toString());
    this.#idToLocalFile.set(id, {url: fileUrl});
    return id;
  }
}
