import { $GameModeKey, GameModeKey } from "@eternalfest/core/game2/game-mode-key";
import { MapType } from "kryo/map";

import { $ManifestMode, ManifestMode } from "./manifest-mode.mjs";

export type ManifestModeMap = Map<GameModeKey, ManifestMode>;

export const $ManifestModeMap: MapType<GameModeKey, ManifestMode> = new MapType({
  keyType: $GameModeKey,
  valueType: $ManifestMode,
  maxSize: Infinity,
  assumeStringKey: true,
});
