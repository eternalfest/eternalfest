import { $GameOptionDisplayName, GameOptionDisplayName } from "@eternalfest/core/game2/game-option-display-name";
import { DeepReadonly } from "@eternalfest/core/types/deep-readonly";
import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";

import { GameOptionDescription } from "./game-option-description.mjs";
import { $GameOptionState, GameOptionState } from "./game-option-state.mjs";

export interface ManifestOption {
  state: GameOptionState;
  displayName: GameOptionDisplayName;
  description?: GameOptionDescription;
  default?: boolean,
}

export type ReadonlyManifestOption = DeepReadonly<ManifestOption>;

export const $ManifestOption: RecordIoType<ManifestOption> = new RecordType<ManifestOption>({
  properties: {
    displayName: {type: $GameOptionDisplayName},
    description: {type: $GameOptionDisplayName, optional: true},
    state: {type: $GameOptionState},
    default: {type: $Boolean, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
