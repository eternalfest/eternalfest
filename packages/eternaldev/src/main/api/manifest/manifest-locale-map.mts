import { $LocaleId, LocaleId } from "@eternalfest/core/types/locale-id";
import { MapType } from "kryo/map";

import { $ManifestLocale, ManifestLocale } from "./manifest-locale.mjs";

export type ManifestLocaleMap = Map<LocaleId, ManifestLocale>;

export const $ManifestLocaleMap: MapType<LocaleId, ManifestLocale> = new MapType({
  keyType: $LocaleId,
  valueType: $ManifestLocale,
  maxSize: Infinity,
  assumeStringKey: true,
});
