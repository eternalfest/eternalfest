import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $Ucs2String } from "kryo/ucs2-string";

export interface ManifestObf {
  key: string;
}

export const $ManifestObf: RecordIoType<ManifestObf> = new RecordType<ManifestObf>({
  properties: {
    key: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
