import { $UrlFragment, UrlFragment } from "@eternalfest/core/eternaldev/url-fragment";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

export interface ManifestPatchman {
  build: UrlFragment;
}

export const $ManifestPatchman: RecordIoType<ManifestPatchman> = new RecordType<ManifestPatchman>({
  properties: {
    build: {type: $UrlFragment},
  },
  changeCase: CaseStyle.SnakeCase,
});
