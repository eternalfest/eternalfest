import { MapType } from "kryo/map";

import { $ManifestLevel, ManifestLevel } from "./manifest-level.mjs";
import { $ManifestLevelKey, ManifestLevelKey } from "./manifest-level-key.mjs";

export type ManifestLevelMap = Map<ManifestLevelKey, ManifestLevel>;

export const $ManifestLevelMap: MapType<ManifestLevelKey, ManifestLevel> = new MapType({
  keyType: $ManifestLevelKey,
  valueType: $ManifestLevel,
  maxSize: Infinity,
  assumeStringKey: true,
});
