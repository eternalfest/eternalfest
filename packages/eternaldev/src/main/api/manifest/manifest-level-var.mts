import { Ucs2StringType } from "kryo/ucs2-string";

export type ManifestLevelVar = string;

export const $ManifestLevelVar: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
