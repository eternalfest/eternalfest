import { $UrlFragment, UrlFragment } from "@eternalfest/core/eternaldev/url-fragment";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

export interface ManifestLocaleContent {
  lands: UrlFragment;
  keys: UrlFragment;
  items: UrlFragment;
  statics: UrlFragment;
}

export const $ManifestLocaleContent: RecordIoType<ManifestLocaleContent> = new RecordType<ManifestLocaleContent>({
  properties: {
    lands: {type: $UrlFragment},
    keys: {type: $UrlFragment},
    items: {type: $UrlFragment},
    statics: {type: $UrlFragment},
  },
  changeCase: CaseStyle.SnakeCase,
});
