import { $UrlFragment, UrlFragment } from "@eternalfest/core/eternaldev/url-fragment";
import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";
import { $Ucs2String } from "kryo/ucs2-string";

import { $ManifestLevelVar, ManifestLevelVar } from "./manifest-level-var.mjs";

export interface ManifestLevel {
  levels: UrlFragment;
  var: ManifestLevelVar;
  padding?: boolean;
  sameSize?: boolean;
  did?: string;
}

export const $ManifestLevel: RecordIoType<ManifestLevel> = new RecordType<ManifestLevel>({
  properties: {
    levels: {type: $UrlFragment},
    var: {type: $ManifestLevelVar},
    padding: {type: $Boolean, optional: true},
    sameSize: {type: $Boolean, optional: true},
    did: {type: $Ucs2String, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
