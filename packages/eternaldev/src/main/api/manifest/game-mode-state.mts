import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum GameModeState {
  Hidden = "hidden",
  Disabled = "disabled",
  Enabled = "enabled",
}

export const $GameModeState: TsEnumType<GameModeState> = new TsEnumType<GameModeState>({
  enum: GameModeState,
  changeCase: CaseStyle.KebabCase,
});
