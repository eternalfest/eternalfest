import { $UrlFragment, UrlFragment } from "@eternalfest/core/eternaldev/url-fragment";
import { $GameDescription, GameDescription } from "@eternalfest/core/game2/game-description";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/core/game2/game-display-name";
import { $GameModeSpecI18nMap, GameModeSpecI18nMap } from "@eternalfest/core/game2/game-mode-spec-i18n-map";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { TryUnionType } from "kryo/try-union";

import { $ManifestLocaleContent, ManifestLocaleContent } from "./manifest-locale-content.mjs";

export interface ManifestLocale {
  build: UrlFragment;
  displayName?: GameDisplayName;
  description?: GameDescription;
  icon?: UrlFragment;
  // NOTE: we'll need a different type here if we ever have something other than texts.
  modes?: GameModeSpecI18nMap,
  content: UrlFragment | ManifestLocaleContent;
}

export const $ManifestLocale: RecordIoType<ManifestLocale> = new RecordType<ManifestLocale>({
  properties: {
    build: {type: $UrlFragment},
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    icon: {type: $UrlFragment, optional: true},
    modes: {type: $GameModeSpecI18nMap, optional: true},
    content: {type: new TryUnionType({variants: [$UrlFragment, $ManifestLocaleContent]})},
  },
  changeCase: CaseStyle.SnakeCase,
});
