import { $UrlFragment, UrlFragment } from "@eternalfest/core/eternaldev/url-fragment";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { TryUnionType } from "kryo/try-union";

import { $ManifestLevelMap, ManifestLevelMap } from "./manifest-level-map.mjs";

export interface ManifestContent {
  build: UrlFragment;
  scoreItems: UrlFragment;
  specialItems: UrlFragment;
  dimensions: UrlFragment;
  data?: UrlFragment;
  levels: UrlFragment | ManifestLevelMap;
}

export const $ManifestContent: RecordIoType<ManifestContent> = new RecordType<ManifestContent>({
  properties: {
    build: {type: $UrlFragment},
    scoreItems: {type: $UrlFragment},
    specialItems: {type: $UrlFragment},
    dimensions: {type: $UrlFragment},
    data: {type: $UrlFragment, optional: true},
    levels: {type: new TryUnionType({variants: [$UrlFragment, $ManifestLevelMap]})},
  },
  changeCase: CaseStyle.SnakeCase,
});
