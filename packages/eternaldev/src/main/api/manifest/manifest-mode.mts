import { GameDescription } from "@eternalfest/core/game2/game-description";
import { GameDisplayName } from "@eternalfest/core/game2/game-display-name";
import { $GameModeDisplayName } from "@eternalfest/core/game2/game-mode-display-name";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $GameModeDescription } from "./game-mode-description.mjs";
import { $GameModeState, GameModeState } from "./game-mode-state.mjs";
import { $ManifestOptionMap, ManifestOptionMap } from "./manifest-option-map.mjs";

export interface ManifestMode {
  state: GameModeState;
  displayName: GameDisplayName;
  description?: GameDescription;
  options?: ManifestOptionMap;
}

export const $ManifestMode: RecordIoType<ManifestMode> = new RecordType<ManifestMode>({
  properties: {
    state: {type: $GameModeState},
    displayName: {type: $GameModeDisplayName},
    description: {type: $GameModeDescription, optional: true},
    options: {type: $ManifestOptionMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
