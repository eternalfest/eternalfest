import { IoType } from "kryo";
import { IntegerType } from "kryo/integer";

export type ManifestVersion = 1;

export const $ManifestVersion: IoType<ManifestVersion> = new IntegerType({
  min: 1,
  max: 1,
}) as IoType<ManifestVersion>;
