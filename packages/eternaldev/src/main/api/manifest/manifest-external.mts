import { $UrlFragment, UrlFragment } from "@eternalfest/core/eternaldev/url-fragment";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

export interface ManifestExternal {
  build: UrlFragment;
}

export const $ManifestExternal: RecordIoType<ManifestExternal> = new RecordType<ManifestExternal>({
  properties: {
    build: {type: $UrlFragment},
  },
  changeCase: CaseStyle.SnakeCase,
});
