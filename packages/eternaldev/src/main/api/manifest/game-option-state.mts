import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum GameOptionState {
  /**
   * The option is invisible, and cannot be toggled.
   */
  Hidden,
  /**
   * The option is displayed, but cannot be toggled.
   * The option should never be active.
   */
  Disabled,

  /**
   * The option is displayed and can be toggled.
   */
  Enabled,
}

export const $GameOptionState: TsEnumType<GameOptionState> = new TsEnumType<GameOptionState>({
  enum: GameOptionState,
  changeCase: CaseStyle.KebabCase,
});
