import { Blob } from "@eternalfest/core/blob/blob";
import { BlobIdRef } from "@eternalfest/core/blob/blob-id-ref";
import { LocalFileMap } from "@eternalfest/core/eternaldev/local-file-map";
import { ProfileMap } from "@eternalfest/core/eternaldev/profile-map";
import { Project } from "@eternalfest/core/eternaldev/project";
import { FamiliesString } from "@eternalfest/core/game2/families-string";
import { GameBuild } from "@eternalfest/core/game2/game-build";
import { GameBuildI18nMap } from "@eternalfest/core/game2/game-build-i18n-map";
import { GameCategory } from "@eternalfest/core/game2/game-category";
import { GameDisplayName } from "@eternalfest/core/game2/game-display-name";
import { GameEngineType } from "@eternalfest/core/game2/game-engine-type";
import { GameOptionSpecMap } from "@eternalfest/core/game2/game-option-spec-map";
import { ObjectType } from "@eternalfest/core/types/object-type";
import { Url } from "@eternalfest/core/types/url";
import { toUuid } from "@eternalfest/core/utils/helpers";
import { VERSION as LOADER_VERSION } from "@eternalfest/loader";
import childProcess from "child_process";
import * as furi from "furi";
import { Incident } from "incident";
import {readOrThrow} from "kryo";
import { UuidHex } from "kryo/uuid-hex";
import { JsonValueReader } from "kryo-json/json-value-reader";
import { JsonWriter } from "kryo-json/json-writer";
import resolve from "resolve";
import rx, { firstValueFrom } from "rxjs";
import rxOp from "rxjs/operators";
import toml from "toml";
import { fileURLToPath } from "url";

import { readTextAsync } from "../../fs-utils.mjs";
import { LocalBlobService } from "../blob/local-service.mjs";
import { $GameModeState } from "../manifest/game-mode-state.mjs";
import { $GameOptionState, GameOptionState } from "../manifest/game-option-state.mjs";
import { $Manifest, Manifest } from "../manifest/manifest.mjs";
import { ManifestContent } from "../manifest/manifest-content.mjs";
import { ManifestExternal } from "../manifest/manifest-external.mjs";
import { ManifestLevel } from "../manifest/manifest-level.mjs";
import { ManifestLocaleMap } from "../manifest/manifest-locale-map.mjs";
import { ManifestModeMap } from "../manifest/manifest-mode-map.mjs";
import { ManifestPatchman } from "../manifest/manifest-patchman.mjs";
import { CompilerResult as Cr, error as crError, errors as crErrors, of as crOf } from "./compiler-result.mjs";
import { getObservableFileVersion } from "./observable-fs.mjs";
import { ShortProject } from "./short-project.mjs";

const JSON_VALUE_READER: JsonValueReader = new JsonValueReader();
const JSON_WRITER: JsonWriter = new JsonWriter();

const DEFAULT_FAMILIES: FamiliesString = "0,7,1000,1001,1002,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19";

export interface LocalProjectServiceOptions {
  projectDirs: Iterable<Url>,
  noGitIntegration?: boolean,
  ignoreModeAndOptionRestrictions?: boolean,
}

export class LocalProjectService {
  private readonly blobService: LocalBlobService;
  private readonly furiToManifest: Map<string, rx.Observable<Cr<Manifest | undefined>>>;
  private readonly idToFuri: Map<UuidHex, Url>;
  private readonly furiToId: Map<string, UuidHex>;
  private withGitIntegration: boolean;
  private ignoreModeAndOptionRestrictions: boolean;

  public constructor(blobService: LocalBlobService, opts: LocalProjectServiceOptions) {
    const furiToManifest: Map<string, rx.Observable<Cr<Manifest | undefined>>> = new Map();
    const idToFuri: Map<UuidHex, Url> = new Map();
    const furiToId: Map<string, UuidHex> = new Map();

    for (const projectDir of opts.projectDirs) {
      const projectDirStr: string = projectDir.toString();
      if (!furiToManifest.has(projectDirStr)) {
        furiToManifest.set(projectDirStr, observeManifestByDir(projectDir));
        const id: UuidHex = toUuid(projectDirStr);
        idToFuri.set(id, projectDir);
        furiToId.set(projectDirStr, id);
      }
    }

    this.blobService = blobService;
    this.furiToManifest = furiToManifest;
    this.idToFuri = idToFuri;
    this.furiToId = furiToId;
    this.withGitIntegration = !opts.noGitIntegration;
    this.ignoreModeAndOptionRestrictions = opts.ignoreModeAndOptionRestrictions ?? false;
  }

  public disableGitIntegration() {
    this.withGitIntegration = false;
  }

  public async getProjects(): Promise<ShortProject[]> {
    const shortProjects: rx.Observable<(ShortProject | undefined)>[] = [];
    for (const [dir, manifest$] of this.furiToManifest) {
      const shortProject$: rx.Observable<ShortProject | undefined> = manifest$
        .pipe(rxOp.map((manifestCr: Cr<Manifest | undefined>): ShortProject | undefined => {
          if (!manifestCr.hasValue()) {
            const msg = `Failed to load project ${dir}:\n${JSON.stringify(manifestCr.diagnostics)}`;
            console.error(msg);
            return undefined;
          }
          const manifest: Manifest | undefined = manifestCr.unwrap();
          if (manifest === undefined) {
            // Manifest not found
            return undefined;
          }
          const localUrl: string = dir.toString();
          const projectId: UuidHex | undefined = this.furiToId.get(localUrl);
          if (projectId === undefined) {
            throw new Error("AssertionError: Expected known id for project URI");
          }
          const displayName: GameDisplayName = manifest.displayName !== undefined ? manifest.displayName : "Untitled";
          const project: ShortProject = {
            id: projectId,
            localUrl,
            displayName,
            description: manifest.description,
          };
          return project;
        }));
      shortProjects.push(shortProject$);
    }

    const projects: ShortProject[] = await firstValueFrom(rx.combineLatest(shortProjects)
      .pipe(rxOp.map((projects: (ShortProject | undefined)[]): ShortProject[] => {
        const result: ShortProject[] = [];
        for (const p of projects) {
          if (p !== undefined) {
            result.push(p);
          }
        }
        return result;
      })));
    return projects;
  }

  public async getProjectById(projectId: UuidHex): Promise<Project | undefined> {
    const dir: Url | undefined = this.idToFuri.get(projectId);
    if (dir === undefined) {
      return undefined;
    }
    const manifest$: rx.Observable<Cr<Manifest | undefined>> | undefined = this.furiToManifest.get(dir.toString());
    if (manifest$ === undefined) {
      throw new Error("AssertionError: Expected known manifest for project URI");
    }
    const manifestCr: Cr<Manifest | undefined> = await firstValueFrom(manifest$);
    const manifest: Manifest | undefined = manifestCr.unwrap();
    if (manifest === undefined) {
      return undefined;
    }
    const project = await resolveProject(projectId, dir, this.blobService, manifest, this.withGitIntegration);

    if (this.ignoreModeAndOptionRestrictions) {
      for (const mode of project.game.modes.values()) {
        if (!mode.isVisible) {
          mode.displayName += " [hidden]";
          mode.isVisible = true;
        }

        for (const opt of mode.options.values()) {
          if (!opt.isEnabled) {
            opt.displayName += " [disabled]";
            opt.isEnabled = true;
          }
          if (!opt.isVisible) {
            opt.displayName += " [hidden]";
            opt.isVisible = true;
          }
        }
      }
    }

    return project;
  }

  public async getProjectByRoot(root: Url): Promise<Project | undefined> {
    const id: UuidHex | undefined = this.furiToId.get(root.toString());
    if (id === undefined) {
      return undefined;
    }
    return this.getProjectById(id);
  }
}

const ALLOWED_RESOURCE_EXTS: Map<string, string>  = new Map([
  ["swf", "application/x-shockwave-flash"],
  ["xml", "application/xml"],
  ["json", "application/json"],
  ["mp3", "audio/mp3"],
  ["png", "image/png"],
]);

async function resolveProject(id: UuidHex, dir: Url, blobService: LocalBlobService, manifest: Manifest, getGitCommit: boolean): Promise<Project> {
  const files: LocalFileMap = new Map();

  function resolve2(path: string, url: string, mimeType: string): (Blob & BlobIdRef);
  function resolve2(path: string, url: string | undefined, mimeType: string): (Blob & BlobIdRef) | undefined;
  function resolve2(path: string, url: string | undefined, mimeType: string): (Blob & BlobIdRef) | undefined {
    if (url === undefined) {
      return undefined;
    }

    if (!url.startsWith("./") && !url.startsWith("../")) {
      throw new Error(`Invalid ${path}: must start with "./" or "../": ${JSON.stringify(url)}`);
    }

    const mediaType = ALLOWED_RESOURCE_EXTS.get(url.split(".").pop()!);
    if (mediaType === undefined) {
      throw new Error(`Invalid ${path}: unknown file type: ${JSON.stringify(url)}`);
    }
    if (mediaType !== mimeType) {
      throw new Error(`Invalid ${path}: wrong MIME type ${mimeType} for: ${JSON.stringify(url)}`);
    }

    const resolvedUrl = furi.join(dir, url);
    const id = blobService.createBlobUuidFromUrl(resolvedUrl);
    files.set(id, { url: resolvedUrl.toString() });
    return {
      type: ObjectType.Blob,
      id,
      mediaType,
      byteSize: 0,
    };
  }

  const resolveIcon = (path: string, url?: string) => resolve2(path, url, "image/png");
  const resolveXml = (path: string, url?: string) => resolve2(path, url, "application/xml");
  const resolveSwf = (path: string, url: string) => resolve2(path, url, "application/x-shockwave-flash")!;
  const resolveAudio = (path: string, url: string) => resolve2(path, url, "audio/mp3")!;

  function pick<T>(path: string, base: T | undefined, localized: T | undefined, fallback: T): T {
    if (base === undefined) {
      if (localized === undefined) {
        console.warn(`WARN: missing value for ${path}, using fallback`);
        return fallback;
      }
      return localized;
    } else if (localized !== undefined) {
      console.warn(`WARN: duplicated ${path} for main locale`);
      return localized;
    }
    return base;
  }

  const mainLocale = pick("mainLocale", manifest.mainLocale, undefined, "fr-FR");
  const i18n: GameBuildI18nMap = new Map();
  if (manifest.locales !== undefined) {
    for (const [l, locale] of manifest.locales) {
      const lPrefix = "locales." + l;
      let contentI18n = resolveXml(lPrefix + ".build", locale.build);
      if (contentI18n !== undefined && typeof locale.content === "string") {
        contentI18n = resolveXml(lPrefix + ".content", locale.content);
      }

      i18n.set(l, {
        contentI18n,
        displayName: locale.displayName,
        description: locale.description,
        icon: resolveIcon(lPrefix + ".icon", locale.icon),
        // TODO: check that there's no "extra" modes and options not present in main metadata.
        modes: locale.modes,
      });
    }
  }
  // Extract and remove i18n for main locale; the contents will be moved to the fields in GameBuild.
  const mainI18n = i18n.get(mainLocale);
  i18n.delete(mainLocale);

  const game: GameBuild = {
    version: manifest.version ?? await readVersionFromPackageJson(dir),
    gitCommitRef: getGitCommit ? await readGitCommitRef(dir) : null,
    createdAt: new Date(),
    mainLocale: mainLocale,
    displayName: pick("displayName", manifest.displayName, mainI18n?.displayName, "Untitled"),
    description: pick("description", manifest.description, mainI18n?.description, ""),
    icon: pick("icon", resolveIcon("icon", manifest.icon), mainI18n?.icon, null),
    loader: LOADER_VERSION,
    engine: {type: GameEngineType.V96}, // will be filled later.
    patcher: null, // will be filled later.
    debug: null, // TODO
    content: resolveXml("content.build", manifest.content?.build) ?? null,
    contentI18n: mainI18n?.contentI18n ?? null,
    musics: manifest.musics === undefined ? [] : manifest.musics.map((music, i) => {
      const blob = resolveAudio(`musics[${i}]`, music);
      return { displayName: music, blob };
    }),
    modes: new Map(), // will be filled later.
    families: pick("families", manifest.families, undefined, DEFAULT_FAMILIES),
    category: pick("category", manifest.category, undefined, GameCategory.Other),
    i18n,
  };

  if (manifest.hf !== undefined) {
    game.engine = {
      type: GameEngineType.Custom,
      blob: resolveSwf("hf", manifest.hf),
    };
  }

  if (manifest.patchman !== undefined) {
    if (manifest.external !== undefined) {
      throw new Error("Cannot have both patchman and external patchers");
    }

    game.patcher = {
      blob: resolveSwf("patchman.build", manifest.patchman.build),
      framework: {
        name: "patchman",
        version: await resolvePackageVersion(dir, "@patchman/patchman"),
      }
    };
  } else if (manifest.external !== undefined) {
    game.patcher = {
      blob: resolveSwf("external.build", manifest.external.build),
      framework: {
        name: "external",
        version: await resolvePackageVersion(dir, "@eternalfest/external"),
      }
    };
  }

  if (manifest.modes !== undefined) {
    for (const [modeId, mode] of manifest.modes) {
      const mMainI18n = mainI18n?.modes?.get(modeId);
      const mPrefix = "modes." + modeId;

      const options: GameOptionSpecMap = new Map();
      if (mode.options !== undefined) {
        for (const [optionId, option] of mode.options) {
          const oMainI18n = mMainI18n?.options?.get(optionId);
          const path = mPrefix + ".options." + optionId + ".displayName";

          options.set(optionId, {
            displayName: pick(path, option.displayName, oMainI18n?.displayName, `<${optionId}>`),
            isVisible: option.state !== GameOptionState.Hidden,
            isEnabled: option.state === GameOptionState.Enabled,
            defaultValue: option.default ?? false,
          });
        }
      }

      const path = mPrefix + ".displayName";
      if (mode.state === "disabled") {
        console.warn(`WARN: ${mPrefix}.state = "disabled" is deprecated, use "hidden" instead`);
      }

      game.modes.set(modeId, {
        displayName: pick(path, mode.displayName, mMainI18n?.displayName, `<${modeId}>`),
        isVisible: mode.state === "enabled",
        options,
      });
    }
  }

  return {
    id,
    remoteId: manifest.remoteId,
    localUrl: dir.toString(),
    game,
    files,
    profiles: manifest.profiles !== undefined ? manifest.profiles : new Map(),
  };
}

async function readVersionFromPackageJson(dir: Url): Promise<string> {
  try {
    const text = await readTextAsync(furi.join(dir, "package.json"));
    const raw: any = JSON.parse(text);
    if (typeof raw === "object" && typeof raw.version === "string") {
      return raw.version;
    } else {
      throw new Error("Invalid package.json file");
    }
  } catch(e) {
    throw new Error("Invalid package.json file", { cause: e });
  }
}

function resolvePackageVersion(dir: Url, packName: string): Promise<string> {
  return new Promise((resolveCb, rejectCb) => {
    const basedir: string = fileURLToPath(dir);
    resolve(packName + "/package.json", { basedir }, (err, _resolved, pkgMeta) => {
      if (pkgMeta === undefined) {
        rejectCb(err);
      } else {
        resolveCb(pkgMeta.version);
      }
    });
  });
}

function readGitCommitRef(dir: Url): Promise<string | null> {
  const child = childProcess.spawn("git", ["rev-parse", "--verify", "HEAD"], {
    // Note: node 14 & 15 don't support URLs here.
    cwd: fileURLToPath(dir),
    stdio: ["ignore", "pipe", "ignore"],
  });

  let gitOutput: string = "";
  child.stdout.setEncoding("utf-8");
  child.stdout.on("data", data => {
    gitOutput += data.toString();
  });

  return new Promise((resolve, reject) => {
    child.on("exit", (code, _signal) => {
      if (code === 0) {
        const commit = gitOutput.trim();
        if (commit === "") {
          reject(new Incident("UnexpectedGitOutput: empty commit ref"));
        } else {
          resolve(commit);
        }
      } else {
        // Not in a git repository.
        resolve(null);
      }
    });
  });
}

export function observeManifestByDir(path: Url): rx.Observable<Cr<Manifest | undefined>> {
  return rx.combineLatest([
    observeTomlManifest(furi.join(path.toString(), ["eternalfest.toml"])),
    observeJsonManifest(furi.join(path.toString(), ["eternalfest.json"])),
  ])
    .pipe(rxOp.map(([tomlCr, jsonCr]): Cr<Manifest | undefined> => {
      if (tomlCr.hasValue()) {
        const tomlManifest: Manifest | undefined = tomlCr.unwrap();
        if (tomlManifest !== undefined) {
          return tomlCr;
        }
      } else if (jsonCr.hasValue()) {
        const jsonManifest: Manifest | undefined = jsonCr.unwrap();
        if (jsonManifest !== undefined) {
          return jsonCr;
        }
      }

      const diags = [...tomlCr.diagnostics, ...jsonCr.diagnostics];
      if (diags.length === 0) {
        diags.push(new Incident("ManifestNotFound", {path: path.toString()}));
      }

      return crErrors(diags);
    }));
}

function observeJsonManifest(path: Url): rx.Observable<Cr<Manifest | undefined>> {
  return getObservableFileVersion(path).pipe(rxOp.share(), rxOp.switchMap(() => getJsonManifest(path)));
}

async function getJsonManifest(path: Url): Promise<Cr<Manifest | undefined>> {
  let text: string;
  try {
    text = await readTextAsync(path as URL);
  } catch (err) {
    if ((err as NodeJS.ErrnoException).code === "ENOENT") {
      return crOf(undefined);
    } else {
      throw err;
    }
  }
  let manifest: Manifest;
  try {
    const raw: any = JSON.parse(text);
    manifest = readOrThrow($Manifest, JSON_VALUE_READER, raw);
  } catch (err) {
    return crError(err);
  }
  return crOf(manifest);
}

function observeTomlManifest(path: Url): rx.Observable<Cr<Manifest | undefined>> {
  return getObservableFileVersion(path).pipe(rxOp.switchMap(() => getTomlManifest(path)));
}

async function getTomlManifest(path: Url): Promise<Cr<Manifest | undefined>> {
  let text: string;
  try {
    text = await readTextAsync(path as URL);
  } catch (err) {
    if ((err as NodeJS.ErrnoException).code === "ENOENT") {
      return crOf(undefined);
    } else {
      throw err;
    }
  }
  let manifest: Manifest;
  try {
    const raw: any = toml.parse(text);
    manifest = readOrThrow($Manifest, JSON_VALUE_READER, raw);
  } catch (err) {
    return crError(err);
  }
  return crOf(manifest);
}

export function emitTomlManifest(manifest: Manifest): string {
  const lines: string[] = [];
  if (manifest.displayName !== undefined) {
    lines.push(`display_name = ${JSON.stringify(manifest.displayName)}`);
  }
  if (manifest.description !== undefined) {
    lines.push(`description = ${JSON.stringify(manifest.description)}`);
  }
  if (manifest.icon !== undefined) {
    lines.push(`icon = ${JSON.stringify(manifest.icon)}`);
  }
  if (manifest.remoteId !== undefined) {
    lines.push(`remote_id = ${JSON.stringify(manifest.remoteId)}`);
  }
  if (manifest.repository !== undefined) {
    lines.push(`repository = ${JSON.stringify(manifest.repository)}`);
  }
  if (manifest.mainLocale !== undefined) {
    lines.push(`main_locale = ${JSON.stringify(manifest.mainLocale)}`);
  }
  if (manifest.families !== undefined) {
    lines.push(`families = ${JSON.stringify(manifest.families)}`);
  }
  if (manifest.hf !== undefined) {
    lines.push(`hf = ${JSON.stringify(manifest.hf)}`);
  }
  if (manifest.musics !== undefined) {
    lines.push(`musics = ${JSON.stringify(manifest.musics, null, 2)}`);
  }
  if (manifest.content !== undefined) {
    lines.push("");
    lines.push("[content]");
    lines.push(emitTomlManifestContent(manifest.content));
  }
  if (manifest.patchman !== undefined) {
    lines.push("");
    lines.push("[patchman]");
    lines.push(emitTomlManifestPatchman(manifest.patchman));
  }
  if (manifest.external !== undefined) {
    lines.push("");
    lines.push("[external]");
    lines.push(emitTomlManifestExternal(manifest.external));
  }
  if (manifest.modes !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestModes(manifest.modes));
  }
  if (manifest.profiles !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestProfiles(manifest.profiles));
  }
  if (manifest.locales !== undefined) {
    lines.push("");
    lines.push(emitTomlManifestLocales(manifest.locales));
  }
  lines.push("");
  return lines.join("\n");
}

function emitTomlManifestContent(content: ManifestContent): string {
  const lines: string[] = [];
  if (content.build !== undefined) {
    lines.push(`build = ${JSON.stringify(content.build)}`);
  }
  if (content.scoreItems !== undefined) {
    lines.push(`score_items = ${JSON.stringify(content.scoreItems)}`);
  }
  if (content.specialItems !== undefined) {
    lines.push(`special_items = ${JSON.stringify(content.specialItems)}`);
  }
  if (content.dimensions !== undefined) {
    lines.push(`dimensions = ${JSON.stringify(content.dimensions)}`);
  }
  if (content.data !== undefined) {
    lines.push(`data = ${JSON.stringify(content.data)}`);
  }
  if (content.levels !== undefined) {
    if (typeof content.levels === "string") {
      lines.push(`levels = ${JSON.stringify(content.levels)}`);
    } else {
      for (const [key, level] of content.levels) {
        lines.push(`[content.levels.${key}]`);
        lines.push(emitTomlManifestLevel(level));
      }
    }
  }
  return lines.join("\n");
}

function emitTomlManifestLevel(level: ManifestLevel): string {
  const lines: string[] = [];
  if (level.levels !== undefined) {
    lines.push(`levels = ${JSON.stringify(level.levels)}`);
  }
  if (level.var !== undefined) {
    lines.push(`var = ${JSON.stringify(level.var)}`);
  }
  if (level.padding !== undefined) {
    lines.push(`padding = ${JSON.stringify(level.padding)}`);
  }
  if (level.sameSize !== undefined) {
    lines.push(`same_size = ${JSON.stringify(level.sameSize)}`);
  }
  if (level.did !== undefined) {
    lines.push(`did = ${JSON.stringify(level.did)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestPatchman(pm: ManifestPatchman): string {
  const lines: string[] = [];
  if (pm.build !== undefined) {
    lines.push(`build = ${JSON.stringify(pm.build)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestExternal(external: ManifestExternal): string {
  const lines: string[] = [];
  if (external.build !== undefined) {
    lines.push(`build = ${JSON.stringify(external.build)}`);
  }
  return lines.join("\n");
}

function emitTomlManifestModes(modes: ManifestModeMap): string {
  const lines: string[] = [];
  for (const [key, mode] of modes) {
    lines.push(`[modes.${key}]`);
    lines.push(`display_name = ${JSON.stringify(mode.displayName)}`);
    lines.push(`state = ${$GameModeState.write(JSON_WRITER, mode.state)}`);
    if (mode.options !== undefined) {
      lines.push(`[modes.${key}.options]`);
      for (const [okey, opt] of mode.options) {
        lines.push(`${okey} = {display_name = ${JSON.stringify(opt.displayName)}, state = ${$GameOptionState.write(JSON_WRITER, opt.state)}}`);
      }
    }
  }
  return lines.join("\n");
}

function emitTomlManifestProfiles(profiles: ProfileMap): string {
  const lines: string[] = [];
  for (const [key, profile] of profiles) {
    lines.push(`[profiles.${key}]`);
    if (profile.user !== undefined) {
      lines.push(`user = {id = ${JSON.stringify(profile.user.id)}, display_name = ${JSON.stringify(profile.user.displayName)}}`);
    }
    if (profile.items !== undefined) {
      const itemDict: string[] = [];
      for (const [id, count] of profile.items) {
        itemDict.push(`${id} = ${count}`);
      }
      lines.push(`items = {${itemDict.join(", ")}}`);
    }
  }
  return lines.join("\n");
}

function emitTomlManifestLocales(locales: ManifestLocaleMap): string {
  const lines: string[] = [];
  for (const [key, locale] of locales) {
    lines.push(`[locales.${key}]`);
    if (locale.build !== undefined) {
      lines.push(`build = ${JSON.stringify(locale.build)}`);
    }
    if (typeof locale.content === "string") {
      lines.push(`content = ${JSON.stringify(locale.content)}`);
    } else {
      lines.push(`[locales.${key}.content]`);
      lines.push(`keys = ${JSON.stringify(locale.content.keys)}`);
      lines.push(`items = ${JSON.stringify(locale.content.items)}`);
      lines.push(`lands = ${JSON.stringify(locale.content.lands)}`);
      lines.push(`statics = ${JSON.stringify(locale.content.statics)}`);
    }
  }
  return lines.join("\n");
}
