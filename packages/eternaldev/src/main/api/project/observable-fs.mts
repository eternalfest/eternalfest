import { Url } from "@eternalfest/core/types/url";
import chokidar from "chokidar";
import {Minimatch} from "minimatch";
import sysPath from "path";
import rx from "rxjs";
import rxOp from "rxjs/operators";

const DEFAULT_VERSION: number = 1;

export interface ObserveFsOptions {
  readonly cwd: string;
}

export interface FileVersion {
  uri: Url;
  version: number;
}

interface ChokidarWatchedPaths {
    [directory: string]: string[];
}

/**
 * Creates an observable for a given file URI.
 *
 * When subscribing to this observable, starts a watcher for the corresponding URI.
 * It emits a notification:
 * - Once at the start
 * - At every change to the file
 *
 * Note that the initial notification is emitted whether the file exists or not
 * and notifications are emitted when the file changes.
 *
 * Each subscription to this observable will start a new watcher. It is recommended
 * to use the `share` operator to avoid creating multiple watchers for the same file.
 *
 * @param furi File URI to observe.
 */
export function getObservableFileVersion(furi: Url): rx.Observable<FileVersion> {
  const uri: Url = Object.freeze(new Url(furi.toString()));
  return new rx.Observable((observer: rx.Observer<FileVersion>): rx.Unsubscribable => {
    let isReady: boolean = false;
    let version: number = DEFAULT_VERSION;
    const watcher: chokidar.FSWatcher = new chokidar.FSWatcher({disableGlobbing: true, ignoreInitial: true});
    watcher.on("all", (_event, _path) => {
      if (isReady) {
        version += 1;
        observer.next({uri, version});
      } else {
        unsubscribe();
        observer.error(new Error("AssertionError: received change event before `ready`"));
      }
    });
    watcher.on("ready", () => {
      if (!isReady) {
        isReady = true;
      } else {
        unsubscribe();
        observer.error(new Error("AssertionError: received multiple `ready` events"));
      }
    });
    watcher.on("error", (err: Error): void => {
      unsubscribe();
      observer.error(err);
    });

    observer.next({uri, version});

    function unsubscribe(): void {
      watcher.close();
    }

    return {unsubscribe};
  });
}

// export function observeFileUri(fileUri$: rx.Observable<Url>): rx.Observable<FileUriState> {
//   let fileUri: Url | undefined;
//   return new rx.Observable((observer: rx.Observer<FileUriState>): rx.Unsubscribable => {
//     let version: number = DEFAULT_VERSION;
//     // Polling is required on Linux, see: https://github.com/paulmillr/chokidar/issues/237
//     const watcher: chokidar.FSWatcher = new chokidar.FSWatcher({disableGlobbing: true, usePolling: true});
//     watcher.on("all", (_event, _path) => {
//       version += 1;
//       observer.next({uri: fileUri!, version});
//     });
//     const fileUriSubscription: rx.Subscription = fileUri$.subscribe({
//       next(nextFileUri: Url): void {
//         if (fileUri === undefined || nextFileUri.toString() !== fileUri.toString()) {
//           if (fileUri !== undefined) {
//             watcher.unwatch(toSysPath(fileUri.toString()));
//           }
//           fileUri = nextFileUri;
//           watcher.add(toSysPath(fileUri.toString()));
//           version += 1;
//           observer.next({uri: fileUri!, version});
//         }
//       },
//       error(error: any): void {
//         teardown();
//         observer.error(error);
//       },
//     });
//
//     function teardown() {
//       fileUriSubscription.unsubscribe();
//       watcher.close();
//     }
//
//     return {
//       unsubscribe(): void {
//         teardown();
//       },
//     };
//   });
// }

export function observeTree(pattern: string, options: ObserveFsOptions): rx.Observable<ReadonlyFsTree> {
  const fsTreeWatcher: FsTreeWatcher = new FsTreeWatcher(pattern, options);
  return new rx.Observable((observer: rx.Observer<ReadonlyFsTree>) => fsTreeWatcher.subscribe(observer));
}

export function observeNodes(pattern: string, options: ObserveFsOptions): rx.Observable<ReadonlyMap<string, number>> {
  let compiled: Minimatch;
  if (pattern[0] === "!") {
    compiled = new Minimatch(`!${sysPath.posix.join(options.cwd, pattern.substring(1))}`);
  } else {
    compiled = new Minimatch(sysPath.posix.join(options.cwd, pattern));
  }

  return observeTree(pattern, options)
    .pipe(
      rxOp.map((tree: ReadonlyFsTree): ReadonlyMap<string, number> => {
        const result: Map<string, number> = new Map();
        for (const [path, version] of tree.getChildren()) {
          if (compiled.match(path)) {
            result.set(path, version);
          }
        }
        return result;
      }),
      rxOp.distinctUntilChanged((prev: ReadonlyMap<string, number>, next: ReadonlyMap<string, number>): boolean => {
        let prevSum: number = 0;
        for (const [path, version] of prev) {
          if (!next.has(path)) {
            return false;
          }
          prevSum += version;
        }
        let nextSum: number = 0;
        for (const [path, version] of next) {
          if (!prev.has(path)) {
            return false;
          }
          nextSum += version;
        }
        return prevSum === nextSum;
      }),
    );
}

export interface ReadonlyFsTree {
  readonly pattern: string;
  readonly directories: ReadonlyMap<string, ReadonlyMap<string, number>>;

  getChildren(): Iterable<[string, number]>;
}

class WatchedFsTree implements ReadonlyFsTree {
  readonly pattern: string;
  readonly directories: ReadonlyMap<string, ReadonlyMap<string, number>>;

  constructor(pattern: string, directories: ReadonlyMap<string, ReadonlyMap<string, number>>) {
    this.pattern = pattern;
    this.directories = directories;
  }

  * getChildren(): Iterable<[string, number]> {
    for (const [dir, children] of this.directories) {
      for (const [child, version] of children) {
        yield [sysPath.join(dir, child), version];
      }
    }
  }
}

class FsTreeWatcher {
  private readonly pattern: string;
  private readonly options: ObserveFsOptions;
  private readonly observers: Set<rx.Observer<ReadonlyFsTree>>;
  private readonly versions: Map<string, number>;
  private watcher: chokidar.FSWatcher | undefined;
  private isReady: boolean;

  constructor(pattern: string, options: ObserveFsOptions) {
    this.pattern = pattern;
    this.options = {cwd: options.cwd};
    this.observers = new Set();
    this.versions = new Map();
    this.watcher = undefined;
    this.isReady = false;
  }

  subscribe(observer: rx.Observer<ReadonlyFsTree>): rx.Unsubscribable {
    if (!this.observers.has(observer)) {
      this.observers.add(observer);
      if (this.watcher === undefined) {
        this.startWatcher();
      } else if (this.isReady) {
        observer.next(new WatchedFsTree(
          this.pattern,
          this.getVersionedDirectories(this.watcher.getWatched()),
        ));
      }
    }
    // tslint:disable-next-line:no-void-expression
    return {unsubscribe: () => this.unsubscribe(observer)};
  }

  private unsubscribe(observer: rx.Observer<ReadonlyFsTree>): void {
    this.observers.delete(observer);
    if (this.observers.size === 0) {
      this.stopWatcher();
    }
  }

  private startWatcher(): void {
    if (this.watcher !== undefined) {
      return;
    }
    this.isReady = false;
    const watcher: chokidar.FSWatcher = chokidar.watch(this.pattern, {ignoreInitial: true, ...this.options});
    this.watcher = watcher;
    watcher.once("ready", () => {
      this.isReady = true;
      this.broadcast(watcher.getWatched());
    });
    watcher.on("all", (_event: string, path: string) => {
      this.updateVersion(sysPath.join(this.options.cwd, path));
      this.broadcast(watcher.getWatched());
    });
  }

  private stopWatcher(): void {
    if (this.watcher === undefined) {
      return;
    }
    this.watcher.removeAllListeners();
    this.watcher.close();
    this.watcher = undefined;
  }

  private getVersionedDirectories(
    watchedPaths: ChokidarWatchedPaths,
  ): ReadonlyMap<string, ReadonlyMap<string, number>> {
    const directories: Map<string, ReadonlyMap<string, number>> = new Map();
    for (const relDir of Object.keys(watchedPaths)) {
      const absDir: string = sysPath.join(this.options.cwd, relDir);
      const nodes: Map<string, number> = new Map();
      for (const relNode of watchedPaths[relDir]) {
        const absNode: string = sysPath.join(absDir, relNode);
        nodes.set(relNode, this.getVersion(absNode));
      }
      directories.set(absDir, nodes);
    }
    return directories;
  }

  private broadcast(watchedPaths: ChokidarWatchedPaths): void {
    const directories: ReadonlyMap<string, ReadonlyMap<string, number>> = this.getVersionedDirectories(watchedPaths);
    const fsTree: WatchedFsTree = new WatchedFsTree(this.pattern, directories);
    for (const observer of this.observers) {
      observer.next(fsTree);
    }
  }

  private updateVersion(path: string): void {
    const old: number | undefined = this.versions.get(path);
    this.versions.set(path, old === undefined ? DEFAULT_VERSION : old + 1);
  }

  private getVersion(path: string): number {
    const result: number | undefined = this.versions.get(path);
    if (result !== undefined) {
      return result;
    }
    this.versions.set(path, DEFAULT_VERSION);
    return DEFAULT_VERSION;
  }
}
