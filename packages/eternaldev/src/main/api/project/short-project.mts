import { $GameDescription, GameDescription } from "@eternalfest/core/game2/game-description";
import { $GameDisplayName, GameDisplayName } from "@eternalfest/core/game2/game-display-name";
import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export interface ShortProject {
  id: UuidHex;
  localUrl: string;
  displayName: GameDisplayName;
  description?: GameDescription;
}

export const $ShortProject: RecordIoType<ShortProject> = new RecordType<ShortProject>({
  properties: {
    id: {type: $UuidHex},
    localUrl: {type: new Ucs2StringType({maxLength: Infinity})},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
