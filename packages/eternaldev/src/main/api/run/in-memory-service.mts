import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {CreateDebugRunOptions} from "@eternalfest/core/eternaldev/create-debug-run-options";
import {PartialProfile} from "@eternalfest/core/eternaldev/partial-profile";
import {Profile} from "@eternalfest/core/eternaldev/profile";
import {ProfileKey} from "@eternalfest/core/eternaldev/profile-key";
import {Game} from "@eternalfest/core/game2/game";
import {Leaderboard} from "@eternalfest/core/leaderboard/leaderboard";
import {CreateRunOptions} from "@eternalfest/core/run/create-run-options";
import {Run} from "@eternalfest/core/run/run";
import {RunItems} from "@eternalfest/core/run/run-items";
import {RunStart} from "@eternalfest/core/run/run-start";
import {RunService} from "@eternalfest/core/run/service";
import {SetRunResultOptions} from "@eternalfest/core/run/set-run-result-options";
import {ObjectType} from "@eternalfest/core/types/object-type";
import {UserService} from "@eternalfest/core/user/service";
import {User} from "@eternalfest/core/user/user";
import {UuidHex} from "kryo/uuid-hex";
import { UUID } from "uuidjs";

import {LocalGame2Service} from "../game2/local-service.mjs";
import {DEFAULT_USER_DISPLAY_NAME, DEFAULT_USER_ID} from "../user/in-memory-service.mjs";

export class InMemoryRunService implements RunService {
  private readonly game: LocalGame2Service;
  private readonly user: UserService;
  private readonly idToRun: Map<UuidHex, Run>;
  private readonly runIdToProfile: Map<UuidHex, Profile>;

  constructor(game: LocalGame2Service, user: UserService) {
    this.game = game;
    this.user = user;
    this.idToRun = new Map();
    this.runIdToProfile = new Map();
  }

  getGameUserItemsById(_auth: AuthContext, _userId: string, _gameId: string, _until: Date | null): Promise<RunItems | undefined> {
    throw new Error("Method not implemented: getGameUserItemsById.");
  }

  getLeaderboard(_auth: AuthContext, _gameId: string, _gameMode: string): Promise<Leaderboard> {
    throw new Error("Method not implemented: getLeaderboard.");
  }

  async createDebugRun(auth: AuthContext, options: CreateDebugRunOptions): Promise<Run> {
    const id: UuidHex = UUID.genV4().toString();
    const game: Game | null = await this.game.getGame(auth, {
      game: {id: options.gameId},
      channelOffset: 0,
      channelLimit: 1
    });
    if (game === null) {
      throw new Error("GameNotFound");
    }
    const profile: Profile = await this.getDebugProfile(game, options.profile);

    const user: User | undefined = await this.user.getUserById(auth, options.userId);
    if (user === undefined) {
      throw new Error("UserNotFound");
    }
    const run: Run = {
      id,
      game: {id: game.id},
      channel: {key: options.channel},
      build: game.channels.active.build,
      user: profile.user,
      gameOptions: options.gameOptions,
      gameMode: options.gameMode,
      createdAt: new Date(),
      startedAt: null,
      result: null,
      settings: options.settings,
    };
    this.idToRun.set(id, run);
    this.runIdToProfile.set(id, profile);
    return run;
  }

  async createRun(auth: AuthContext, options: CreateRunOptions): Promise<Run> {
    return this.createDebugRun(auth, {...options, userId: options.user.id, gameId: options.game.id});
  }

  async getRunById(_auth: AuthContext, runId: UuidHex): Promise<Run> {
    return this.idToRun.get(runId)!;
  }

  async startRun(_auth: AuthContext, runId: UuidHex): Promise<RunStart> {
    const run: Run | undefined = this.idToRun.get(runId);
    if (run === undefined) {
      throw new Error("RunNotFound");
    }
    const key: UuidHex = UUID.genV4().toString();

    const profile: Profile = this.runIdToProfile.get(runId)!;

    const runStart: RunStart = {
      families: profile.families,
      key,
      run: {id: runId},
      items: profile.items,
    };

    const newRun: Run = {...run, startedAt: new Date()};
    this.idToRun.set(runId, newRun);
    return runStart;
  }

  async setRunResult(
    _auth: AuthContext,
    runId: UuidHex,
    options: SetRunResultOptions,
  ): Promise<Run> {
    console.log("Run result:");
    console.log(options);
    return this.idToRun.get(runId)!;
  }

  private async getDebugProfile(game: Game, profileKey?: ProfileKey): Promise<Profile> {
    const partial: PartialProfile = await this.game.getDebugProfile(game.id, profileKey);

    return {
      user: {
        type: ObjectType.User,
        id: partial.user?.id ?? DEFAULT_USER_ID,
        displayName: partial.user?.displayName ?? DEFAULT_USER_DISPLAY_NAME,
      },
      families: partial.families ?? game.channels.active.build.families,
      items: partial.items ?? new Map(),
    };
  }
}
