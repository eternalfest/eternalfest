import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { AuthScope } from "@eternalfest/core/auth/auth-scope";
import { AuthenticateHttpOptions } from "@eternalfest/core/auth/authenticate-http-options";
import { AuthService } from "@eternalfest/core/auth/service";
import {ObjectType} from "@eternalfest/core/types/object-type";
import { Session } from "@eternalfest/core/types/session";
import { ActorType } from "@eternalfest/core/types/user/actor-type";
import { UserDisplayName } from "@eternalfest/core/user/user-display-name";
import { UserId } from "@eternalfest/core/user/user-id";
import incident from "incident";

export class InMemoryAuthService implements AuthService {
  async etwinOauth(_userId: UserId, _userDisplayName: UserDisplayName): Promise<AuthContext> {
    throw new incident.Incident("NotImplemented: InMemoryAuthService#etwinOauth");
  }

  async session(_sessionId: string): Promise<AuthContext> {
    throw new incident.Incident("NotImplemented: InMemoryAuthService#session");
  }

  async authenticateHttp(_options: AuthenticateHttpOptions): Promise<AuthContext> {
    return {
      type: ActorType.User,
      scope: AuthScope.Default,
      user: {
        type: ObjectType.User,
        id: "00000000-0000-0000-0000-000000000000",
        displayName: "Eternaldev",
      },
      isAdministrator: true,
      isTester: true,
    };
  }

  async createSession(_acx: AuthContext, _userId: UserId): Promise<Session> {
    return {
      id: "00000000-0000-0000-0000-000000000000",
      user: {
        id: "00000000-0000-0000-0000-000000000000",
      },
      createdAt: new Date(),
      updatedAt: new Date(),
    };
  }
}
