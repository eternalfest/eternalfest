import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { GameModeKey } from "@eternalfest/core/game2/game-mode-key";
import { Leaderboard } from "@eternalfest/core/leaderboard/leaderboard";
import { LeaderboardService } from "@eternalfest/core/leaderboard/service";
import incident from "incident";
import { UuidHex } from "kryo/uuid-hex";

export class InMemoryLeaderboardService implements LeaderboardService {
  getLeaderboard(_auth: AuthContext, _gameId: UuidHex, _gameMode: GameModeKey): Promise<Leaderboard> {
    throw new incident.Incident("NotImplemented: InMemoryLeaderboardService#getGameResults");
  }
}
