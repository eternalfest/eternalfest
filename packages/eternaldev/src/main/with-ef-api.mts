import fs from "fs";
import * as furi from "furi";

import {InMemoryAuthService} from "./api/auth/in-memory-service.mjs";
import {LocalBlobService} from "./api/blob/local-service.mjs";
import {LocalGame2Service} from "./api/game2/local-service.mjs";
import {LocalProjectService} from "./api/project/local-service.mjs";
import {InMemoryRunService} from "./api/run/in-memory-service.mjs";
import {InMemoryUserService} from "./api/user/in-memory-service.mjs";
import {getRecentProjects, touchRecentProject} from "./lib/recent-projects.mjs";
import {Api as EfApi} from "./rest/index.mjs";
import {KoaAuth} from "./rest/koa-auth.mjs";

export interface LocalEfApi extends EfApi {
  project: LocalProjectService;
  run: InMemoryRunService;
}

export interface LocalEfApiOptions {
  filesThrottle?: number;
  noGitIntegration?: boolean;
  ignoreModeAndOptionRestrictions?: boolean;
}

export async function withEfApi<R>(options: LocalEfApiOptions, handler: (efApi: LocalEfApi) => Promise<R>): Promise<R> {
  const localProjects: readonly URL[] = await getLocalProjects();
  const blob: LocalBlobService = new LocalBlobService({
    throttle: options.filesThrottle,
  });
  const project: LocalProjectService = new LocalProjectService(blob, {
    projectDirs: localProjects,
    noGitIntegration: options.noGitIntegration,
    ignoreModeAndOptionRestrictions: options.ignoreModeAndOptionRestrictions,
  });
  const user: InMemoryUserService = new InMemoryUserService();
  const game2: LocalGame2Service = new LocalGame2Service(project);
  const run: InMemoryRunService = new InMemoryRunService(game2, user);
  const auth: InMemoryAuthService = new InMemoryAuthService();
  const koaAuth: KoaAuth = new KoaAuth(auth);
  const efApi: LocalEfApi = {auth, blob, game2, koaAuth, run, user, project};
  return handler(efApi);
}

async function getLocalProjects(): Promise<URL[]> {
  const cwdProject: URL | undefined = await tryGetCwdProject();
  if (cwdProject !== undefined) {
    await touchRecentProject(cwdProject);
  }
  return getRecentProjects();
}

export async function tryGetCwdProject(): Promise<URL | undefined> {
  const cwdUrl: URL = furi.fromSysPath(process.cwd());
  if (fs.existsSync(furi.join(cwdUrl.toString(), "eternalfest.toml"))) {
    return cwdUrl;
  }
  return undefined;
}
