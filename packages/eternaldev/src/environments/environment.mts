import { Environment } from "./environment-type.mjs";

let apiBase: string = "http://localhost:50317/api/v1";

if (typeof document !== "undefined" && typeof document.URL !== "undefined") {
  apiBase = new URL(document.URL).origin + "/api/v1";
}

export const environment: Environment = {
  production: false,
  apiBase,
};
