import { Environment } from "./environment-type.mjs";

export const environment: Environment = {
  production: false,
  apiBase: "http://localhost:50317/api/v1",
};
