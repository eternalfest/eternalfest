export interface ServerAppConfig {
  externalUri?: URL;
  isProduction: boolean;
  backendPort: number;
}
