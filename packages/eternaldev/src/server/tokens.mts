import { InjectionToken } from "@angular/core";
import type { Request } from "koa";

export const BACKEND_URI: InjectionToken<string> = new InjectionToken("BACKEND_URI");

export const REQUEST: InjectionToken<Request> = new InjectionToken("REQUEST");
