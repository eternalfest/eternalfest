import {isPlatformBrowser} from "@angular/common";
import {Component, ElementRef, Inject, Input, NgZone, OnInit, PLATFORM_ID, ViewChild} from "@angular/core";
import {$Run, Run} from "@eternalfest/core/run/run";
import {RunSettings} from "@eternalfest/core/run/run-settings";
import {LocaleId} from "@eternalfest/core/types/locale-id";
import {Incident} from "incident";
import {JsonWriter} from "kryo-json/json-writer";
import {BehaviorSubject, combineLatest} from "rxjs";

import {ConsoleService} from "../../modules/console/console.service.mjs";
import {ConsoleRow} from "../../modules/console/console-row.mjs";
import {Devtools} from "../../modules/devtools-client/devtools.mjs";
import {ConsoleAPICalledEvent} from "../../modules/devtools-client/runtime.mjs";
import {RemoteObject} from "../../modules/devtools-client/types.mjs";
import {SwfSocketMessage} from "../../modules/swf-socket/message.mjs";
import {SwfSocketRemoteClient} from "../../modules/swf-socket/remote-client.mjs";
import {createSwfSocketServer, SwfSocketServer} from "../../modules/swf-socket/server.mjs";

const JSON_WRITER: JsonWriter = new JsonWriter();

function getFlashVars(
  elementId: string,
  run: Run | undefined,
  locale: LocaleId | undefined,
  swfSocketServer: SwfSocketServer | undefined,
): string {
  if (run !== undefined) {
    const mode: string = run.gameMode;
    const options: string[] = Array.from(run.gameOptions);
    const settings: RunSettings = run.settings;
    const loaderParameters = new URLSearchParams();
    loaderParameters.append("object_id", elementId);
    loaderParameters.append("run", $Run.write(JSON_WRITER, run));
    loaderParameters.append("game", run.game.id);
    loaderParameters.append("options", JSON.stringify({mode, options, settings: {...settings, volume: 1}, locale: locale ?? "fr-FR"}));
    if (swfSocketServer !== undefined) {
      loaderParameters.append("server", swfSocketServer.methodName);
    }
    return loaderParameters.toString();
  }
  return "";
}

@Component({
  selector: "efd-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"],
  standalone: false,
})
export class GameComponent implements OnInit {
  flashVars: string | undefined;
  swfSocketServer: SwfSocketServer | undefined;

  @ViewChild("container", {static: true})
    containerElement!: ElementRef;

  @Input("run")
  set run(run: Run | undefined) {
    this.run$.next(run);
  }

  get run(): Run | undefined {
    return this.run$.getValue();
  }

  @Input("locale")
  set locale(locale: LocaleId | undefined) {
    this.locale$.next(locale);
  }

  get locale(): LocaleId | undefined {
    return this.locale$.getValue();
  }

  public elementId: string;
  private run$: BehaviorSubject<Run | undefined>;
  private locale$: BehaviorSubject<LocaleId | undefined>;

  private readonly isBrowser: boolean;
  private readonly consoleService: ConsoleService;
  private readonly ngZone: NgZone;

  constructor(@Inject(PLATFORM_ID) platformId: Object, consoleService: ConsoleService, ngZone: NgZone) {
    this.isBrowser = isPlatformBrowser(platformId);
    this.consoleService = consoleService;
    this.ngZone = ngZone;

    this.elementId = `swf${Math.floor((1 << 16) * Math.random())}`;

    this.run$ = new BehaviorSubject<Run | undefined>(undefined);
    this.locale$ = new BehaviorSubject<LocaleId | undefined>(undefined);
    this.flashVars = undefined;
    if (!this.isBrowser) {
      this.swfSocketServer = undefined;
    } else {
      this.swfSocketServer = createSwfSocketServer();
      this.swfSocketServer.connection.subscribe((client: SwfSocketRemoteClient) => {
        const devtools: Devtools = new Devtools(client);
        devtools.runtime.consoleAPICalled.subscribe((event: ConsoleAPICalledEvent): void => {
          let row: ConsoleRow | undefined;
          switch (event.type) {
            case "log":
              row = {type: "flash-object", runtime: devtools.runtime, level: "log", object: event.args[0]};
              break;
            case "warning":
              row = {type: "flash-object", runtime: devtools.runtime, level: "warning", object: event.args[0]};
              break;
            case "error":
              row = {type: "flash-object", runtime: devtools.runtime, level: "error", object: event.args[0]};
              break;
            case "set-print": {
              const message: RemoteObject = event.args[0];
              const text: string = message.type === "string" ? message.value : JSON.stringify(message);
              this.ngZone.run(() => this.consoleService.setPrint({type: "text" as "text", text, level: "log"}));
              break;
            }
            case "clear-print":
              this.ngZone.run(() => this.consoleService.clearPrint());
              break;
            default:
              throw new Incident("UnreachableCode");
          }
          if (row !== undefined) {
            this.ngZone.run(() => this.consoleService.addRow(row!));
          }
        });

        client.message.subscribe(({name, data}: SwfSocketMessage) => {
          switch (name) {
            case "log":
              console.log(data.value);
              break;
            case "warn":
              console.warn(data.value);
              break;
            case "error":
              console.error(data.value);
              break;
            default:
              console.warn("Unknown message name: " + name);
          }
        });
      });
    }
  }

  ngOnInit(): void {
    combineLatest([this.run$, this.locale$]).subscribe(([run, locale]: [Run | undefined, LocaleId | undefined]): void => {
      this.flashVars = getFlashVars(this.elementId, run, locale, this.swfSocketServer);
      this.refreshDom();
    });
  }

  refreshDom() {
    if (this.isBrowser) {
      const container: HTMLDivElement = this.containerElement.nativeElement;
      const children: Node[] = Array.from(container.children);
      for (const child of children) {
        container.removeChild(child);
      }

      const object: HTMLObjectElement = document.createElement("object");
      object.setAttribute("id", this.elementId);
      object.setAttribute("type", "application/x-shockwave-flash");
      object.setAttribute("data", "/assets/loader.swf");
      object.setAttribute("name", "game");
      object.setAttribute("quality", "high");
      object.setAttribute("allowscriptaccess", "always");
      if (this.flashVars !== undefined) {
        object.setAttribute("flashvars", this.flashVars);
      }
      object.setAttribute("width", "420");
      object.setAttribute("height", "520");
      container.appendChild(object);
    }
  }
}
