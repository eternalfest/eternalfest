import { CommonModule } from "@angular/common";
import {HttpClientModule, provideHttpClient} from "@angular/common/http";
import { NgModule } from "@angular/core";
import { BrowserModule } from "@angular/platform-browser";

import { ConsoleModule } from "../modules/console/console.module.mjs";
import { AppComponent } from "./app.component.mjs";
import { AppRoutingModule } from "./app-routing.module.mjs";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    ConsoleModule,
  ],
  declarations: [AppComponent],
  providers: [provideHttpClient()],
  exports: [AppComponent],
})
export class AppModule {
}
