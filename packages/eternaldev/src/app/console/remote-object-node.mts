import { Incident } from "incident";

import { GetPropertiesResult, Runtime } from "../../modules/devtools-client/runtime.mjs";
import { RemoteObject } from "../../modules/devtools-client/types.mjs";
import { TreeNode } from "./tree-node.mjs";

function isPrimitive(ro: RemoteObject): boolean {
  switch (ro.type) {
    case "boolean":
    case "number":
    case "string":
    case "undefined":
      return true;
    case "function":
      return false;
    case "object":
      return ro.subtype === "null";
    default:
      throw new Incident("UnreachableCode");
  }
}

export class RemoteObjectNode implements TreeNode<RemoteObject, PropertyDescriptor> {
  runtime: Runtime;
  data: RemoteObject;
  children?: PropertyDescriptorNode[];
  isLeaf: boolean;

  constructor(runtime: Runtime, value: RemoteObject) {
    this.runtime = runtime;
    this.data = value;
    this.children = undefined;
    this.isLeaf = isPrimitive(value);
  }

  async getChildren(): Promise<PropertyDescriptorNode[]> {
    if (this.isLeaf) {
      throw new Incident("Cannot get children of leaf node");
    }
    if (this.data.objectId === undefined) {
      throw new Incident("Undefined objectId");
    }

    const result: GetPropertiesResult = await this.runtime.getProperties({
      objectId: this.data.objectId,
    });

    return result.result.map((descriptor: PropertyDescriptor) => new PropertyDescriptorNode(this.runtime, descriptor));
  }
}

export class PropertyDescriptorNode implements TreeNode<PropertyDescriptor> {
  runtime: Runtime;
  data: PropertyDescriptor;
  children?: PropertyDescriptorNode[];
  isLeaf: boolean;

  constructor(runtime: Runtime, data: PropertyDescriptor) {
    this.runtime = runtime;
    this.data = data;
    this.children = undefined;
    this.isLeaf = isPrimitive(data.value);
  }

  async getChildren(): Promise<PropertyDescriptorNode[]> {
    if (this.isLeaf) {
      throw new Incident("Cannot get children of leaf node");
    }
    if (this.data.value.objectId === undefined) {
      throw new Incident("Undefined objectId");
    }

    const result: GetPropertiesResult = await this.runtime.getProperties({
      objectId: this.data.value.objectId,
    });

    return result.result.map((descriptor: PropertyDescriptor) => new PropertyDescriptorNode(this.runtime, descriptor));
  }
}
