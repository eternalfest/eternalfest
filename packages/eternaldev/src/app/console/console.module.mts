import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module.mjs";
import { ConsoleComponent } from "./console.component.mjs";
import { ConsoleRowComponent } from "./console-row.component.mjs";
import { PrintConsoleComponent } from "./print-console.component.mjs";
import { PropertyDescriptorComponent } from "./property-descriptor.component.mjs";
import { RemoteObjectComponent } from "./remote-object.component.mjs";
import { TreeNodeComponent } from "./tree-node.component.mjs";
import { TreeViewComponent } from "./tree-view.component.mjs";

@NgModule({
  declarations: [
    ConsoleComponent,
    ConsoleRowComponent,
    PrintConsoleComponent,
    PropertyDescriptorComponent,
    RemoteObjectComponent,
    TreeNodeComponent,
    TreeViewComponent,
  ],
  exports: [ConsoleComponent, PrintConsoleComponent, TreeViewComponent],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
  ],
})
export class ConsoleModule {
}
