import { Component, ContentChild, Input, TemplateRef } from "@angular/core";

import { TreeNode } from "./tree-node.mjs";

@Component({
  selector: "efd-tree-view",
  templateUrl: "./tree-view.component.html",
  styleUrls: [],
  standalone: false,
})
export class TreeViewComponent<Root = unknown, Child = Root> {
  @ContentChild("rootNode", {static: true})
    rootNodeTemplate!: TemplateRef<any>;
  @ContentChild("childNode", {static: true})
    childNodeTemplate!: TemplateRef<any>;

  @Input("node")
    node!: TreeNode<Root, Child>;

  // set node(value: TreeNode<T>) {
  //   this._node = value;
  //   this.data = value.data;
  //   this.loadChildren();
  // }
  // get node(): TreeNode<T> {
  //   return this._node;
  // }
  //
  // data: T;
  //
  // children?: TreeNode<T>[];
  //
  // isExpanded: boolean;
  //
  // private _node: TreeNode<T>;

  constructor() {
    // this.isExpanded = true;
  }

  // toggleIsExpanded() {
  //   this.isExpanded = !this.isExpanded;
  // }
  //
  // private loadChildren() {
  //   if (this._node.children !== undefined) {
  //     this.children = this._node.children;
  //     return;
  //   }
  //   if (this._node.isLeaf || this._node.getChildren === undefined) {
  //     return;
  //   }
  //   this._node.getChildren().then((children) => this.children = children);
  // }
}
