import { Component, Input, OnInit } from "@angular/core";

@Component({
  selector: "efd-property-descriptor",
  templateUrl: "./property-descriptor.component.html",
  styleUrls: ["./property-descriptor.component.scss"],
  standalone: false,
})
export class PropertyDescriptorComponent implements OnInit {
  @Input("descriptor")
    descriptor!: PropertyDescriptor & {name?: string};

  constructor() {
  }

  ngOnInit(): void {
  }
}
