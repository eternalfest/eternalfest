import { Component, OnDestroy, OnInit } from "@angular/core";
import { Subscription } from "rxjs";

import { ConsoleService } from "../../modules/console/console.service.mjs";
import { ConsoleRow } from "../../modules/console/console-row.mjs";

@Component({
  selector: "efd-console",
  templateUrl: "./console.component.html",
  styleUrls: ["./console.component.scss"],
  standalone: false,
})
export class ConsoleComponent implements OnInit, OnDestroy {
  rows: ReadonlyArray<ConsoleRow>;

  private readonly consoleService: ConsoleService;
  private rowsSubscription?: Subscription;

  constructor(consoleService: ConsoleService) {
    this.consoleService = consoleService;
    this.rows = [];
  }

  ngOnInit(): void {
    this.rowsSubscription = this.consoleService.observeRows()
      .subscribe((rows) => {
        this.rows = rows;
      });
  }

  ngOnDestroy(): void {
    if (this.rowsSubscription !== undefined) {
      this.rowsSubscription.unsubscribe();
    }
  }
}
