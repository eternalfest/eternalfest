import { Component, Input, OnInit } from "@angular/core";
import { Incident } from "incident";

import { RemoteObject } from "../../modules/devtools-client/types.mjs";

function getText(ro: RemoteObject): string {
  switch (ro.type) {
    case "boolean":
      return JSON.stringify(ro.value);
    case "object":
      switch (ro.subtype) {
        case "array":
          return "Array";
        case "null":
          return "null";
        default:
          return "Object";
      }
    case "function":
      return "Function";
    case "number":
      if (ro.unserializableValue !== undefined) {
        return ro.unserializableValue;
      } else {
        return JSON.stringify(ro.value);
      }
    case "string":
    // TODO(demurgos): This is a dirty hack because flash fails to serialize the empty string ""
      return ro.value === "null" ? "\"\"" : JSON.stringify(ro.value);
    case "symbol":
      return "symbol(...)";
    case "undefined":
      return "undefined";
    default:
      throw new Incident("UnreachableCode");
  }
}

@Component({
  selector: "efd-remote-object",
  templateUrl: "./remote-object.component.html",
  styleUrls: [],
  standalone: false,
})
export class RemoteObjectComponent implements OnInit {
  @Input("object")
    object!: RemoteObject;

  text!: string;

  constructor() {}

  ngOnInit(): void {
    this.text = getText(this.object);
  }
}
