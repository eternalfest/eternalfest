import { Component, Input, TemplateRef } from "@angular/core";

import { TreeNode } from "./tree-node.mjs";

export interface TreeNodeTemplates {
  rootNode?: TemplateRef<any>;
  childNode?: TemplateRef<any>;
}

@Component({
  selector: "efd-tree-node",
  templateUrl: "./tree-node.component.html",
  styleUrls: ["./tree-node.component.scss"],
  standalone: false,
})
export class TreeNodeComponent<Root = any, Child = Root> {
  @Input("is-root")
    isRoot!: boolean;

  @Input("node")
    node!: TreeNode<Root, Child>;

  @Input("templates")
    templates!: TreeNodeTemplates;

  isExpanded: boolean;

  children: TreeNode<Child>[] | undefined;

  constructor() {
    this.isExpanded = false;
    this.children = undefined;
  }

  toggleIsExpanded(): void {
    if (this.node.isLeaf) {
      return;
    }

    this.isExpanded = !this.isExpanded;
    if (this.isExpanded && !this.node.isLeaf && this.children === undefined) {
      if (this.node.children !== undefined) {
        this.children = this.node.children;
      } else if (this.node.getChildren !== undefined) {
        // TODO(demurgos): Avoid parallel promise
        this.node.getChildren().then((children: TreeNode<Child>[]) => {
          this.children = children;
        });
      }
    }
  }
}
