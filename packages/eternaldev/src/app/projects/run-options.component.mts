import {ChangeDetectorRef, Component, EventEmitter, Input, Output} from "@angular/core";
import {CreateDebugRunOptions} from "@eternalfest/core/eternaldev/create-debug-run-options";
import {ProfileKey} from "@eternalfest/core/eternaldev/profile-key";
import {GameModeKey} from "@eternalfest/core/game2/game-mode-key";
import {GameModeSpec} from "@eternalfest/core/game2/game-mode-spec";
import {RunSettings} from "@eternalfest/core/run/run-settings";
import {LocaleId} from "@eternalfest/core/types/locale-id";

@Component({
  selector: "efd-run-options",
  templateUrl: "./run-options.component.html",
  styleUrls: ["./run-options.component.scss"],
  standalone: false,
})
export class RunOptionsComponent {
  @Input("locales")
    locales: ReadonlyArray<LocaleId> | undefined;

  @Input("modes")
    modes: Map<GameModeKey, GameModeSpec> | undefined;

  @Input("profiles")
    profiles: ReadonlyArray<ProfileKey> | undefined;

  @Output()
    efdSubmit: EventEmitter<CreateDebugRunOptions>;

  set modeKey(modeKey: string | undefined) {
    if (modeKey !== undefined && (this.modes === undefined || !this.modes.has(modeKey))) {
      modeKey = undefined;
    }

    this.optionIds.clear();
    this.#modeKey = modeKey;
    this.#changeDetectorRef.detectChanges();
  }

  get modeKey(): string | undefined {
    return this.#modeKey;
  }

  get mode(): GameModeSpec | undefined {
    if (this.#modeKey === undefined || this.modes === undefined) {
      return undefined;
    }
    return this.modes.get(this.#modeKey);
  }

  optionIds: Set<string>;

  detail: boolean;
  shake: boolean;
  sound: boolean;
  music: boolean;

  profileKey: string;
  locale: LocaleId;

  #changeDetectorRef: ChangeDetectorRef;
  #modeKey: string | undefined;

  constructor(changeDetectorRef: ChangeDetectorRef) {
    this.#changeDetectorRef = changeDetectorRef;
    this.#modeKey = undefined;

    this.optionIds = new Set();
    this.efdSubmit = new EventEmitter();
    this.detail = false;
    this.shake = false;
    this.sound = false;
    this.music = false;
    this.profileKey = "default";
    this.locale = "fr-FR";
  }

  readOptions(): CreateDebugRunOptions {
    const modeKey: string | undefined = this.modeKey;
    const mode: GameModeSpec | undefined = this.mode;
    if (modeKey === undefined || mode === undefined) {
      throw new Error("No mode");
    }

    const settings: RunSettings = {
      sound: this.sound,
      music: this.music,
      shake: this.shake,
      detail: this.detail,
      volume: 100,
      locale: this.locale,
    };

    const gameOptions: ReadonlyArray<string> = [...this.optionIds];

    return {
      gameId: "",
      userId: "",
      channel: "main",
      version: "0.0.0",
      gameMode: modeKey,
      gameOptions,
      settings,
      profile: this.profileKey,
      locale: this.locale,
    };
  }

  onSubmit(event: Event): void {
    event.preventDefault();
    const result: CreateDebugRunOptions = this.readOptions();
    this.efdSubmit.next(result);
  }

  onOptionChange(event: Event): void {
    event.preventDefault();
    const target: HTMLInputElement = event.target as HTMLInputElement;
    if (target.checked) {
      this.optionIds.add(target.value);
    } else {
      this.optionIds.delete(target.value);
    }
  }
}
