import {NgModule} from "@angular/core";
import {RouterModule, Routes} from "@angular/router";

import {ProjectComponent} from "./project.component.mjs";
import {ProjectsListComponent} from "./projects-list.component.mjs";

const routes: Routes = [
  {path: "", component: ProjectsListComponent, pathMatch: "full"},
  {path: ":project_id", component: ProjectComponent, pathMatch: "full"},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsRoutingModule {
}
