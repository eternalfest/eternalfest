import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module.mjs";
import { ConsoleModule } from "../console/console.module.mjs";
import { GameModule } from "../game/game.module.mjs";
import { ProjectComponent } from "./project.component.mjs";
import { ProjectsListComponent } from "./projects-list.component.mjs";
import { ProjectsRoutingModule } from "./projects-routing.module.mjs";
import { RunOptionsComponent } from "./run-options.component.mjs";

@NgModule({
  declarations: [ProjectComponent, ProjectsListComponent, RunOptionsComponent],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
    GameModule,
    ConsoleModule,
    ProjectsRoutingModule,
  ],
})
export class ProjectsModule {
}
