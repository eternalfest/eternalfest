import {ChangeDetectorRef, Component, OnInit} from "@angular/core";
import {ActivatedRoute, Params} from "@angular/router";
import {CreateDebugRunOptions} from "@eternalfest/core/eternaldev/create-debug-run-options";
import {ProfileKey} from "@eternalfest/core/eternaldev/profile-key";
import {Project} from "@eternalfest/core/eternaldev/project";
import {Game} from "@eternalfest/core/game2/game";
import {CreateRunOptions} from "@eternalfest/core/run/create-run-options";
import {Run} from "@eternalfest/core/run/run";
import {LocaleId} from "@eternalfest/core/types/locale-id";
import {ObjectType} from "@eternalfest/core/types/object-type";
import {firstValueFrom} from "rxjs";

import {GameService} from "../../modules/api/game.service.mjs";
import {ProjectService} from "../../modules/api/project.service.mjs";
import {RunService} from "../../modules/api/run.service.mjs";

@Component({
  selector: "efd-project",
  templateUrl: "./project.component.html",
  styleUrls: ["./project.component.scss"],
  standalone: false,
})
export class ProjectComponent implements OnInit {
  project: Project | undefined;
  projectProfileKeys: ReadonlyArray<ProfileKey>;
  projectLocales: ReadonlyArray<LocaleId>;
  game: Game | null;
  runOptions: CreateRunOptions | undefined;
  run: Run | undefined;
  locale: LocaleId | undefined;

  private readonly activatedRoute: ActivatedRoute;
  private readonly changeDetectorRef: ChangeDetectorRef;
  private readonly gameService: GameService;
  private readonly projectService: ProjectService;
  private readonly runService: RunService;

  constructor(
    activatedRoute: ActivatedRoute,
    changeDetectorRef: ChangeDetectorRef,
    gameService: GameService,
    projectService: ProjectService,
    runService: RunService,
  ) {
    this.activatedRoute = activatedRoute;
    this.gameService = gameService;
    this.changeDetectorRef = changeDetectorRef;
    this.projectService = projectService;
    this.runService = runService;

    this.project = undefined;
    this.projectProfileKeys = [];
    this.projectLocales = [];
    this.game = null;
    this.runOptions = undefined;
  }

  ngOnInit(): void {
    this.activatedRoute.params
      .subscribe(async (params: Params) => {
        const {project_id: projectId} = params;
        this.game = await firstValueFrom(this.gameService.getGameByRef(projectId));
        this.project = await firstValueFrom(this.projectService.getProjectById(projectId));
        this.projectProfileKeys = this.project !== undefined ? [...this.project.profiles.keys()] : [];
        this.projectLocales = this.project !== undefined ? [this.project.game.mainLocale, ...this.project.game.i18n.keys()] : [];
        this.changeDetectorRef.detectChanges();
      });
  }

  async onRunOptions(event: CreateDebugRunOptions): Promise<void> {
    this.runOptions = {...event, user: {type: ObjectType.User, id: event.userId}, game: {id: event.gameId}};
    this.locale = event.locale;
    console.log(this.locale);
    const createRunOptions: CreateDebugRunOptions = {
      ...event,
      userId: "00000000-0000-0000-0000-000000000000",
      gameId: this.game!.id,
    };
    this.run = await firstValueFrom(this.runService.createDebugRun(createRunOptions));
    // console.log("CREATED");
    // console.log(this.run);
    this.changeDetectorRef.detectChanges();
  }
}
