import { ChangeDetectorRef, Component, OnInit } from "@angular/core";
import {firstValueFrom} from "rxjs";

import { ShortProject } from "../../main/api/project/short-project.mjs";
import { ProjectService } from "../../modules/api/project.service.mjs";

@Component({
  selector: "efd-projects-list",
  templateUrl: "./projects-list.component.html",
  styleUrls: ["./projects-list.component.scss"],
  standalone: false,
})
export class ProjectsListComponent implements OnInit {
  projects: readonly ShortProject[] | undefined;

  private projectService: ProjectService;
  private changeDetectorRef: ChangeDetectorRef;

  constructor(changeDetectorRef: ChangeDetectorRef, projectService: ProjectService) {
    this.changeDetectorRef = changeDetectorRef;
    this.projectService = projectService;
    this.projects = undefined;
  }

  async ngOnInit(): Promise<void> {
    this.projects = await firstValueFrom(this.projectService.getProjects(true));
    this.changeDetectorRef.detectChanges();
  }
}
