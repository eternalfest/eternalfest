import {APP_BASE_HREF} from "@angular/common";
import {NgModule} from "@angular/core";

import {ApiModule} from "../modules/api/api.module.mjs";
import {BrowserRestModule} from "../modules/rest/rest.module.browser.mjs";
import {AppComponent} from "./app.component.mjs";
import {AppModule} from "./app.module.mjs";

@NgModule({
  imports: [
    AppModule,
    ApiModule,
    BrowserRestModule,
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: "/"},
  ],
  bootstrap: [AppComponent],
})
export class AppBrowserModule {
}
