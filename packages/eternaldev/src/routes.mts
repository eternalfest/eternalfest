export const ROUTES: string[] = [
  "/",
  "/projects",
  "/projects/:project_id",
];
