// import {Injectable, NgZone} from "@angular/core";
// import {ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot} from "@angular/router";
// import {Game} from "@eternalfest/core/game2/game";
//
// import {GameService} from "./game.service.mjs";
//
// @Injectable()
// export class GameResolverService implements Resolve<Game> {
//   private readonly game: GameService;
//   private readonly ngZone: NgZone;
//   private readonly router: Router;
//
//   constructor(game: GameService, ngZone: NgZone, router: Router) {
//     this.game = game;
//     this.ngZone = ngZone;
//     this.router = router;
//   }
//
//   async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Game | never> {
//     const gameId: string | null = route.paramMap.get("game_id");
//     if (gameId !== null) {
//       const game: Game | undefined = await this.game.getGameById(gameId);
//       if (game !== undefined) {
//         return game;
//       }
//     }
//
//     await this.ngZone.run(() => this.router.navigateByUrl("/games"));
//     return undefined as never;
//   }
// }
