import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from "@angular/router";
import { ObjectType } from "@eternalfest/core/types/object-type";
import { User } from "@eternalfest/core/user/user";
import { Observable, of as rxOf } from "rxjs";

export const SELF_USER: User = {
  id: "00000000-0000-0000-0000-000000000000",
  type: ObjectType.User as ObjectType.User,
  displayName: "EternalDev",
  identities: [],
  createdAt: new Date(0),
  updatedAt: new Date(0),
  isAdministrator: true,
  isTester: true,
};

@Injectable()
export class SelfUserResolverService implements Resolve<User> {
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User | never> {
    return rxOf(SELF_USER);
  }
}
