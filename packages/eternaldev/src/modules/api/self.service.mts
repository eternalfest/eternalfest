import {Injectable} from "@angular/core";
import {AuthContext, UserAuthContext} from "@eternalfest/core/auth/auth-context";
import {AuthScope} from "@eternalfest/core/auth/auth-scope";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {Observable, of as rxOf} from "rxjs";

import {SELF_USER} from "./self-user-resolver.service.mjs";

const SELF_ACX: UserAuthContext = {
  type: ActorType.User,
  scope: AuthScope.Default,
  user: SELF_USER,
  isAdministrator: SELF_USER.isAdministrator,
  isTester: SELF_USER.isTester,
};

@Injectable()
export class SelfService {
  constructor() {}

  getAuth(): Observable<AuthContext> {
    return rxOf(SELF_ACX);
  }
}
