import {Injectable} from "@angular/core";
import {$Game, Game} from "@eternalfest/core/game2/game";
import {GameId} from "@eternalfest/core/game2/game-id";
import {$GameModeKey, GameModeKey} from "@eternalfest/core/game2/game-mode-key";
import {GameIdRef} from "@eternalfest/core/game2/game-ref";
import {RawGameRef} from "@eternalfest/core/game2/raw-game-ref";
import {$ShortGameListing, ShortGameListing} from "@eternalfest/core/game2/short-game-listing";
import {$Leaderboard, Leaderboard} from "@eternalfest/core/leaderboard/leaderboard";
import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {$Boolean} from "kryo/boolean";
import {$Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";

import {BodyRequestOptions, RestService} from "../rest/rest.service.mjs";

export interface GetGamesQuery {
  favorite: boolean;
  offset: number;
  limit: number;
}

const $GetGamesQuery: RecordIoType<GetGamesQuery> = new RecordType<GetGamesQuery>({
  properties: {
    favorite: {type: $Boolean},
    offset: {type: $Uint32},
    limit: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
});

@Injectable()
export class GameService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getGames(page0: number, favorite: boolean): Observable<ShortGameListing> {
    return this.#rest
      .get(["games"], {
        queryType: $GetGamesQuery,
        query: {
          favorite,
          offset: page0 * 100,
          limit: 100
        },
        resType: $ShortGameListing});
  }

  getGameByRef(gameRef: RawGameRef): Observable<Game | null> {
    return this.#rest
      .get(["games", gameRef], {resType: $Game})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }
}
