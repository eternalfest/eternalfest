import { environment } from "../../../environments/environment.mjs";

export function apiUri(...components: string[]): string {
  return `${environment.apiBase}/${components.map(encodeURIComponent).join("/")}`;
}
