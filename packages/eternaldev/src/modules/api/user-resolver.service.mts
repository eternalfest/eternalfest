import { Injectable, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
import { User } from "@eternalfest/core/user/user";

import { UserService } from "./user.service.mjs";

@Injectable()
export class UserResolverService implements Resolve<User> {
  private readonly ngZone: NgZone;
  private readonly router: Router;
  private readonly user: UserService;

  constructor(ngZone: NgZone, router: Router, user: UserService) {
    this.ngZone = ngZone;
    this.router = router;
    this.user = user;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<User | never> {
    const userId: string | null = route.paramMap.get("user_id");
    if (userId !== null) {
      const user: User | undefined = await this.user.getUserById(userId);
      if (user !== undefined) {
        return user;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/"));
    return undefined as never;
  }
}
