import {Injectable} from "@angular/core";
import {$User, User} from "@eternalfest/core/user/user";
import {UserId} from "@eternalfest/core/user/user-id";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";

import {RestService} from "../rest/rest.service.mjs";

@Injectable()
export class UserService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getUserById(userId: UserId): Observable<User | null> {
    return this.#rest
      .get(["users", userId], {resType: $User})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }
}
