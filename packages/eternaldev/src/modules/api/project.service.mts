import {Injectable} from "@angular/core";
import {$Project, Project} from "@eternalfest/core/eternaldev/project";
import {Game} from "@eternalfest/core/game2/game";
import {ArrayIoType, ArrayType} from "kryo/array";
import {UuidHex} from "kryo/uuid-hex";
import {Observable} from "rxjs";

import {$ShortProject, ShortProject} from "../../main/api/project/short-project.mjs";
import {RestService} from "../rest/rest.service.mjs";


const $ShortProjectArray: ArrayIoType<ShortProject> = new ArrayType({itemType: $ShortProject, maxLength: Infinity});

@Injectable()
export class ProjectService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getProjects(acceptCached: boolean): Observable<ShortProject[]> {
    return this.#rest
      .get(["projects"], {resType: $ShortProjectArray});
  }

  getProjectById(projectId: UuidHex): Observable<Project> {
    return this.#rest
      .get(["projects", projectId], {resType: $Project});
  }

  getGameByProjectId(projectId: UuidHex): Observable<Game> {
    throw new Error("NotImplemented: ProjectService#getGameByProjectId");
  }
}
