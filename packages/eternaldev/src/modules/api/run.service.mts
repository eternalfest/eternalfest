import {Injectable} from "@angular/core";
import {$CreateDebugRunOptions, CreateDebugRunOptions} from "@eternalfest/core/eternaldev/create-debug-run-options";
import {CreateRunOptions} from "@eternalfest/core/run/create-run-options";
import {$Run, Run} from "@eternalfest/core/run/run";
import {RunId} from "@eternalfest/core/run/run-id";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";

import {RestService} from "../rest/rest.service.mjs";

@Injectable()
export class RunService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  createRun(options: Readonly<CreateRunOptions>): Observable<Run> {
    return this.createDebugRun({...options, gameId: options.game.id, userId: options.user.id});
  }

  createDebugRun(options: Readonly<CreateDebugRunOptions>): Observable<Run> {
    return this.#rest.post(["runs"], {
      reqType: $CreateDebugRunOptions,
      req: options,
      resType: $Run,
    });
  }

  getRunById(runId: RunId): Observable<Run | null> {
    return this.#rest
      .get(["runs", runId], {resType: $Run})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }
}
