// import { Injectable, NgZone } from "@angular/core";
// import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from "@angular/router";
// import { Run } from "@eternalfest/core/run/run";
//
// import { RunService } from "./run.service.mjs";
//
// @Injectable()
// export class RunResolverService implements Resolve<Run> {
//   private readonly ngZone: NgZone;
//   private readonly router: Router;
//   private readonly run: RunService;
//
//   constructor(ngZone: NgZone, router: Router, run: RunService) {
//     this.ngZone = ngZone;
//     this.router = router;
//     this.run = run;
//   }
//
//   async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Run | never> {
//     const runId: string | null = route.paramMap.get("run_id");
//     if (runId !== null) {
//       const run: Run | undefined = await this.run.getRunById(runId);
//       if (run !== undefined) {
//         return run;
//       }
//     }
//
//     await this.ngZone.run(() => this.router.navigateByUrl("/games"));
//     return undefined as never;
//   }
// }
