import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "hfestUri",
  pure: true,
  standalone: false,
})
export class HfestUri implements PipeTransform {
  transform(type: "login"): string {
    return "/login";
  }
}
