import { NgModule } from "@angular/core";

import { RestService } from "./rest.service.mjs";
import { ServerRestService } from "./rest.service.server.mjs";

@NgModule({
  imports: [],
  providers: [
    {provide: RestService, useClass: ServerRestService},
  ],
})
export class ServerRestModule {
}
