import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {Inject, Injectable,TransferState} from "@angular/core";
import {readOrThrow} from "kryo";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_WRITER} from "kryo-json/json-writer";
import {SEARCH_PARAMS_WRITER} from "kryo-search-params/search-params-writer";
import {from as rxFrom, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";
import urljoin from "url-join";

import {BACKEND_URI} from "../../server/tokens.mjs";
import {
  GetInput,
  QueryRequestOptions,
  RequestOptions,
  RestService,
  SimpleRequestOptions,
  toTransferStateKey
} from "./rest.service.mjs";

@Injectable()
export class ServerRestService extends RestService {
  readonly #backendUri: string;
  readonly #httpClient: HttpClient;
  readonly #transferState: TransferState;

  constructor(@Inject(BACKEND_URI) backendUri: string, httpClient: HttpClient, transferState: TransferState) {
    super();
    this.#backendUri = backendUri;
    this.#httpClient = httpClient;
    this.#transferState = transferState;

  }

  public delete<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("delete", route, options);
  }

  public get<Query, Res>(route: readonly string[], options: SimpleRequestOptions<Res> | QueryRequestOptions<Query, Res>): Observable<Res> {
    return this.request("get", route, options);
  }

  public patch<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("patch", route, options);
  }

  public post<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("post", route, options);
  }

  public put<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.request("put", route, options);
  }

  private request<Query, Req, Res>(method: string, route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    const path = ["api", "v1", ...route].map(encodeURIComponent).join("/");
    const url = urljoin(this.#backendUri, path);
    const rawBody: string | undefined = options.reqType !== undefined ? options.reqType.write(JSON_WRITER, options.req) : undefined;
    const rawQuery: string | undefined = options.queryType !== undefined ? options.queryType.write(SEARCH_PARAMS_WRITER, options.query) : undefined;

    return rxFrom(this.#httpClient.request(method.toUpperCase(), url, {
      headers: new HttpHeaders({
        "Content-Type": "application/json",
      }),
      params: new HttpParams({fromString: rawQuery ?? ""}),
      body: method === "get" ? undefined : Buffer.from(rawBody ?? ""),
      observe: "response",
      responseType: "json",
    })).pipe(rxMap((res: HttpResponse<unknown>) => {
      const resRaw: unknown = res.body;
      let resObj: Res;
      try {
        resObj = readOrThrow(options.resType, JSON_VALUE_READER, resRaw);
      } catch (err) {
        console.error(`API Error: ${path}`);
        console.error(err);
        throw err;
      }
      if (method === "get" && res.status === 200) {
        this.#transferState.set<unknown>(toTransferStateKey({
          route,
          queryType: options.queryType,
          query: options.query
        } as GetInput<Query>), resRaw);
      }
      return resObj;
    }));
  }
}
