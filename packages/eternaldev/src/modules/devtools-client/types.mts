export type ScriptId = string;

export type RemoteObjectId = string;

export type ExecutionContextId = number;

/**
 * Milliseconds since the UNIX epoch
 */
export type Timestamp = number;

export type UnserializableValue = "Infinity" | "-Infinity" | "NaN" | "-0";

export interface ExceptionDetails {
  exceptionId: number;
  text: string;
  lineNumber: number;
  columnNumber: number;
  scriptId?: ScriptId;
  url?: string;
  stackTrace?: any;
  exceptions?: RemoteObject;
  executionContextId?: any;
}

export interface RemoteObject {
  type: "boolean" | "function" | "object" | "number" | "string" | "symbol" | "undefined";
  subtype?: "array" | "date" | "error" | "generator" | "iterator" | "node" | "map" | "promise" | "proxy" | "null"
    | "regexp" | "set" | "weakmap" | "weakset";
  className?: string;
  value?: any;
  unserializableValue?: UnserializableValue;
  description?: string;
  objectId?: RemoteObjectId;
}

export interface PropertyDescriptor {
  name: string;
  value?: RemoteObject;
  configurable: boolean;
  enumerable: boolean;
}

export interface InternalPropertyDescriptor {
  name: string;
  value?: RemoteObject;
}
