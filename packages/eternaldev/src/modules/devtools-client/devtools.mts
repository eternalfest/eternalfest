import { SwfSocketRemoteClient } from "../swf-socket/remote-client.mjs";
import { Runtime } from "./runtime.mjs";

export class Devtools {
  public readonly runtime: Runtime;

  private readonly client: SwfSocketRemoteClient;

  constructor(client: SwfSocketRemoteClient) {
    this.client = client;
    this.runtime = new Runtime(client);
  }
}
