import {EventEmitter} from "@angular/core";

import {SwfSocketRemoteClient} from "../swf-socket/remote-client.mjs";
import {
  ExceptionDetails,
  ExecutionContextId,
  InternalPropertyDescriptor,
  PropertyDescriptor,
  RemoteObject,
  RemoteObjectId,
  Timestamp,
} from "./types.mjs";

export interface EvaluateOptions {
  expression: string;
  objectGroup?: string;
  includeCommandLineAPI?: string;
  silent?: boolean;
  contextId?: any;
  returnByValue?: boolean;
  generatePreview?: boolean;
  userGesture?: boolean;
  awaitPromise?: boolean;
}

export interface EvaluateResult {
  result: RemoteObject;
  exceptionDetails?: ExceptionDetails;
}

export interface GetPropertiesOptions {
  objectId: RemoteObjectId;
  ownProperties?: boolean;
  accessorPropertiesOnly?: boolean;
  generatePreview?: boolean;
}

export interface GetPropertiesResult {
  result: PropertyDescriptor[];
  internalProperties?: InternalPropertyDescriptor[];
  exceptionDetails?: ExceptionDetails;
}

export interface ConsoleAPICalledEvent {
  type: "clear-print" | "error" | "log" | "set-print" | "warning";
  args: RemoteObject[];
  executionContextId: ExecutionContextId;
  timestamp: Timestamp;
  stackTrace?: any;
  context?: string;
}

export class Runtime {
  readonly consoleAPICalled: EventEmitter<ConsoleAPICalledEvent>;

  private client: SwfSocketRemoteClient;

  constructor(client: SwfSocketRemoteClient) {
    this.client = client;
    this.consoleAPICalled = new EventEmitter;
    this.client.use("runtime/consoleAPICalled", this.handleConsoleAPICalled.bind(this));
  }

  handleConsoleAPICalled(event: ConsoleAPICalledEvent): void {
    // console.log(event);
    this.consoleAPICalled.next(event);
  }

  evaluate(options: EvaluateOptions): EvaluateResult {
    throw new Error("NotImplemented: Runtime#evaluate");
  }

  getProperties(options: GetPropertiesOptions): GetPropertiesResult {
    // TODO: Copy options (to avoid noise due to other properties)
    return this.client.emitMessage("runtime/getProperties", options);
  }
}
