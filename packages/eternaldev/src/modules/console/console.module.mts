import { NgModule } from "@angular/core";

import { ConsoleService } from "./console.service.mjs";

@NgModule({
  providers: [
    {provide: ConsoleService, useClass: ConsoleService},
  ],
})
export class ConsoleModule {
}
