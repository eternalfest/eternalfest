import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

import { ConsoleRow, TextRow } from "./console-row.mjs";

@Injectable()
export class ConsoleService {
  private readonly rows$: BehaviorSubject<readonly ConsoleRow[]>;
  private readonly printRows$: BehaviorSubject<readonly TextRow[]>;

  constructor() {
    this.rows$ = new BehaviorSubject<readonly ConsoleRow[]>([]);
    this.printRows$ = new BehaviorSubject<readonly TextRow[]>([]);
  }

  addRow(row: ConsoleRow): void {
    const rows: ReadonlyArray<ConsoleRow> = this.rows$.getValue();
    this.rows$.next([...rows, row]);
  }

  setPrint(row: TextRow): void {
    this.printRows$.next([row]);
  }

  clearPrint(): void {
    this.printRows$.next([]);
  }

  observeRows(): Observable<ReadonlyArray<ConsoleRow>> {
    return this.rows$;
  }

  observePrintRows(): Observable<ReadonlyArray<TextRow>> {
    return this.printRows$;
  }
}
