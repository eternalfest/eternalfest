import { Runtime } from "../devtools-client/runtime.mjs";
import { RemoteObject } from "../devtools-client/types.mjs";

/* tslint:disable-next-line:class-name */
export interface _ConsoleRow {
  readonly type: "text" | "flash-object";
  readonly level: "log" | "warning" | "error";
}

export interface TextRow extends _ConsoleRow {
  readonly type: "text";
  readonly text: string;
}

export interface FlashObjectRow extends _ConsoleRow {
  readonly type: "flash-object";
  readonly runtime: Runtime;
  readonly object: RemoteObject;
}

export type ConsoleRow = TextRow | FlashObjectRow;
