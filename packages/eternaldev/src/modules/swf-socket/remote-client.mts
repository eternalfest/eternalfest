import { EventEmitter } from "@angular/core";
import { Incident } from "incident";

import { SwfSocketMessage } from "./message.mjs";
import { SwfSocketServer } from "./server.mjs";

export type RequestHandler<D = any, R = any> = (data: D) => R;

export class SwfSocketRemoteClient {
  /**
   * clientId = `${remoteElementId}:${remoteMethodName}`
   */
  readonly clientId: string;

  /**
   * HTML `id` of the <object> HTML element for this client.
   */
  readonly remoteElementId: string;

  /**
   * Method name for this client.
   */
  readonly remoteMethodName: string;

  /**
   * Name of the handler for this specific client instance (attache to `window`).
   */
  readonly localMethodName: string;

  readonly message: EventEmitter<SwfSocketMessage>;
  private messageHandler: (message: SwfSocketMessage) => {success: boolean, result?: any};
  private readonly router: Map<string, RequestHandler>;

  /**
   * Server: the server for this client.
   * ClientId: Method name of the remote client
   */
  constructor(server: SwfSocketServer, clientId: string) {
    this.clientId = clientId;
    const match: RegExpExecArray | null = /^([^:]+):([^:]+)$/.exec(clientId);
    if (match === null) {
      throw new Incident(`Invalid client: flash://${clientId}`);
    }
    this.remoteElementId = match[1];
    this.remoteMethodName = match[2];

    this.localMethodName = "swfSocketClient" + Math.floor(Math.random() * (1 << 16));
    this.messageHandler = this.onMessage.bind(this);
    this.message = new EventEmitter();
    this.router = new Map();
    this.attach();
  }

  emitMessage(name: string, data: any): any {
    if (typeof document !== "object") {
      throw new Incident("Cannot user the SWF socket server because `document` is not an object");
    }
    const element: HTMLElement | null = document.getElementById(this.remoteElementId);
    if (element === null) {
      throw new Incident("ConnectionClosed", `Remote element not found for the id: ${this.remoteElementId}`);
    }
    const remoteMethod: ((req: any) => any) | undefined = Reflect.get(element, this.remoteMethodName);
    if (remoteMethod === undefined) {
      throw new Incident("ConnectionClosed", `Remote element does not expose its method: flash://${this.clientId}`);
    }
    let response: {success: boolean, result?: any, error?: any};
    try {
      response = remoteMethod.call(element, {name, data});
    } catch (err) {
      console.error(err);
      throw err;
    }
    if (!response.success) {
      throw new Incident("Unknown Flash Error");
    }
    return response.result;
  }

  use(route: string, handler: RequestHandler): void {
    if (this.router.has(route)) {
      throw new Incident("DuplicateRouteDefinition");
    }
    this.router.set(route, handler);
  }

  private onMessage(message: SwfSocketMessage): {success: boolean, result?: any} {
    try {
      const handler: RequestHandler | undefined = this.router.get(message.name);
      if (handler !== undefined) {
        const result: any = handler(message.data);
        return {success: true, result};
      } else {
        this.message.next(message);
        return {success: true};
      }
    } catch (err) {
      console.warn(err);
      return {success: false};
    }
  }

  private attach(): void {
    if (typeof window !== "object") {
      throw new Error("Cannot create the SWF socket server because `window` is not an object");
    }
    if (Reflect.get(window, this.localMethodName) !== undefined) {
      throw new Error(`Cannot attach the SWF socket server, method already in use: ${this.localMethodName}`);
    }
    Reflect.set(window, this.localMethodName, this.messageHandler);
  }
}
