import { EventEmitter } from "@angular/core";

import { SwfSocketRemoteClient } from "./remote-client.mjs";

export function createSwfSocketServer(serverMethodName?: string): SwfSocketServer {
  if (typeof serverMethodName !== "string") {
    serverMethodName = "swfSocketServer" + Math.floor(Math.random() * (1 << 16));
  }
  return new SwfSocketServer(serverMethodName);
}

export class SwfSocketServer {
  readonly methodName: string;
  readonly connection: EventEmitter<SwfSocketRemoteClient>;
  private readonly connectionHandler: (clientId: string) => {success: boolean, result?: string, error?: string};

  /**
   * Method name for this server (this should be random):
   * This is the method called by Flash to interact with JS.
   * @param methodName
   */
  constructor(methodName: string) {
    this.methodName = methodName;
    this.connectionHandler = this.onConnection.bind(this);
    this.connection = new EventEmitter();
    this.attach();
  }

  private onConnection(clientId: string): {success: boolean, result?: string, error?: string} {
    try {
      const client: SwfSocketRemoteClient = this.createRemoteClient(clientId);
      this.connection.next(client);
      return {success: true, result: client.localMethodName};
    } catch (err) {
      console.warn(err);
      return {success: false};
    }
  }

  private createRemoteClient(clientId: string): SwfSocketRemoteClient {
    return new SwfSocketRemoteClient(this, clientId);
  }

  private attach(): void {
    if (typeof window !== "object") {
      throw new Error("Cannot create the SWF socket server because `window` is not an object");
    }
    if (Reflect.get(window, this.methodName) !== undefined) {
      throw new Error(`Cannot attach the SWF socket server, method already in use: ${this.methodName}`);
    }
    Reflect.set(window, this.methodName, this.connectionHandler);
  }
}
