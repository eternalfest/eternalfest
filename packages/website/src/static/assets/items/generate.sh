#!/usr/bin/env bash
# Mogrify is part of ImageMagick
mogrify -transparent "#d4eafa" -transparent "#d4eafb" -transparent "#d5eafa" -transparent "#d4eafc" -transparent "#d5e8f8" -transparent "#d4e9fb" -transparent "#d4eaff" -transparent "#d6e9f8" -transparent "#d4e9fa" -format png -path . gif/*.gif
