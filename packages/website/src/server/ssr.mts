import {APP_BASE_HREF} from "@angular/common";
import {enableProdMode, isDevMode} from "@angular/core";
import {CommonEngine} from "@angular/ssr/node";
import {basename, join, parent} from "furi";

import {ROUTES} from "../routes.mjs";
import AppServerModule from "./main.js";
import {BACKEND_URI, INTERNAL_AUTH_KEY, REQUEST} from "./tokens.mjs";

const SERVER_DIR = parent(import.meta.url);
const ngLocaleId = basename(SERVER_DIR);
const SERVER_INDEX = join(SERVER_DIR, "index.server.html");
const BROWSER_DIR = join(SERVER_DIR, "../../browser", [ngLocaleId]);

export {
  APP_BASE_HREF,
  AppServerModule,
  BACKEND_URI,
  BROWSER_DIR,
  CommonEngine,
  enableProdMode,
  INTERNAL_AUTH_KEY,
  isDevMode,
  REQUEST,
  ROUTES,
  SERVER_DIR,
  SERVER_INDEX,
};
