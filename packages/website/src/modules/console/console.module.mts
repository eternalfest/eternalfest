import { NgModule } from "@angular/core";

import { ConsoleService } from "./console.service.mjs";

@NgModule({
  providers: [
    ConsoleService,
  ],
})
export class ConsoleModule {
}
