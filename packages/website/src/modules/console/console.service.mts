import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";

import { ConsoleRow } from "./console-row.mjs";

@Injectable()
export class ConsoleService {
  private readonly rows: BehaviorSubject<ConsoleRow[]>;
  private readonly _rows: ConsoleRow[];

  constructor() {
    this._rows = [];
    this.rows = new BehaviorSubject<ConsoleRow[]>([]);
  }

  addRow(row: ConsoleRow): void {
    this._rows.push(row);
    this.rows.next([...this._rows]);
  }

  getRows(): Observable<ConsoleRow[]> {
    return this.rows;
  }
}
