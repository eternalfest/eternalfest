import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "score",
  pure: true,
  standalone: false,
})
export class ScorePipe implements PipeTransform {
  transform(score: number): string {
    let decimal: string = score.toString(10);
    const parts: string[] = [];
    while (decimal.length > 3) {
      parts.unshift(decimal.substring(decimal.length - 3));
      decimal = decimal.substring(0, decimal.length - 3);
    }
    if (decimal.length > 0) {
      parts.unshift(decimal);
    }
    return parts.join(".");
  }
}
