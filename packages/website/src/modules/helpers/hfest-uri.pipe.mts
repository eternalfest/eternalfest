import { Pipe, PipeTransform } from "@angular/core";
import { HfestServer } from "@eternalfest/core/hfest-identity/hfest-server";

export const SERVER_EN: string = "http://www.hfest.net/";
export const SERVER_ES: string = "http://www.hammerfest.es/";
export const SERVER_FR: string = "http://www.hammerfest.fr/";

export function hfestServerFromUrl(hfestServerUrl: string): HfestServer {
  switch (hfestServerUrl) {
    case SERVER_EN:
      return HfestServer.En;
    case SERVER_ES:
      return HfestServer.Es;
    case SERVER_FR:
      return HfestServer.Fr;
    default:
      throw new Error(`UnknownEnumVariant: ${hfestServerUrl}`);
  }
}

export function hfestServerToUrl(hfestServer: HfestServer): string {
  switch (hfestServer) {
    case HfestServer.En:
      return SERVER_EN;
    case HfestServer.Es:
      return SERVER_ES;
    case HfestServer.Fr:
      return SERVER_FR;
    default:
      throw new Error(`UnknownEnumVariant: ${hfestServer}`);
  }
}


@Pipe({
  name: "hfestUri",
  pure: true,
  standalone: false,
})
export class HfestUriPipe implements PipeTransform {
  transform(type: "public-profile", server: HfestServer, userId: string): string;
  transform(type: "login", server: HfestServer): string;
  transform(type: "host", server: HfestServer): string;
  transform(type: "root", server: HfestServer): string;
  transform(type: any, server: any, ...args: any[]): any {
    switch (type) {
      case "login":
        return getHfestLoginUri(server);
      case "host":
        return getHfestHostUri(server);
      case "public-profile":
        return getHfestPublicProfileUri(server, args[0]);
      case "root":
        return getHfestRootUri(server);
      default:
        console.error(`Unexpected type: ${type}`);
        return getHfestRootUri(server);
    }
  }
}

function getHfestRootUri(server: HfestServer): string {
  return hfestServerToUrl(server);
}

function getHfestHostUri(server: HfestServer): string {
  switch (server) {
    case HfestServer.En:
      return "hfest.net";
    case HfestServer.Es:
      return "hammerfest.es";
    case HfestServer.Fr:
      return "hammerfest.fr";
  }
}

function getHfestLoginUri(server: HfestServer): string {
  return `${hfestServerToUrl(server)}login.html`;
}

function getHfestPublicProfileUri(server: HfestServer, userId: string): string {
  return `${hfestServerToUrl(server)}user.html/${userId}`;
}
