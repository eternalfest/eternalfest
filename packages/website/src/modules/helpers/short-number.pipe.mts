import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "shortNumber",
  pure: true,
  standalone: false,
})
export class ShortNumberPipe implements PipeTransform {
  transform(num: number): string {
    for (const [unity, postfix] of POSTFIXES) {
      if (num >= 10 * unity) {
        return Math.floor(num / unity).toString(10) + postfix;
      }
    }

    return num.toString(10);
  }
}

const POSTFIXES: [number, string][] = [
  [1_000_000_000, "G"], [1_000_000, "M"], [1_000, "k"],
];
