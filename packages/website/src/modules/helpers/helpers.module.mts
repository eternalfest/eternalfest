import { NgModule } from "@angular/core";

import { HfestUriPipe } from "./hfest-uri.pipe.mjs";
import { ScorePipe } from "./score.pipe.mjs";
import { ShortNumberPipe } from "./short-number.pipe.mjs";
import { UnsortedKeyValue } from "./unsorted-keyvalue.pipe.mjs";

@NgModule({
  declarations: [
    HfestUriPipe,
    ScorePipe,
    ShortNumberPipe,
    UnsortedKeyValue,
  ],
  exports: [
    HfestUriPipe,
    ScorePipe,
    ShortNumberPipe,
    UnsortedKeyValue,
  ],
})
export class HelpersModule {
}
