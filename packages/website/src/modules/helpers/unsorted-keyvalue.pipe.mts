import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "unsortedKeyValue",
  pure: true,
  standalone: false,
})
export class UnsortedKeyValue implements PipeTransform {
  transform<K, V>(map: Map<K, V>): { key: K, value: V }[] {
    const results = [];
    for (const [key, value] of map) {
      results.push({key, value});
    }
    return results;
  }
}
