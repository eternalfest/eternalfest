import { NgModule } from "@angular/core";

import { GameService } from "./game.service.mjs";
import { RunService } from "./run.service.mjs";
import { SelfService } from "./self.service.mjs";
import { UserService } from "./user.service.mjs";

@NgModule({
  providers: [
    {provide: GameService, useClass: GameService},
    {provide: RunService, useClass: RunService},
    {provide: SelfService, useClass: SelfService},
    {provide: UserService, useClass: UserService},
  ],
})
export class ApiModule {
}
