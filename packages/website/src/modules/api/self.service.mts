import { Injectable } from "@angular/core";
import { $AuthContext,AuthContext } from "@eternalfest/core/auth/auth-context";
import { Observable } from "rxjs";

import { RestService } from "../rest/rest.service.mjs";

@Injectable()
export class SelfService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getAuth(): Observable<AuthContext> {
    return this.#rest
      .get(["self", "auth"], {resType: $AuthContext});
  }
}
