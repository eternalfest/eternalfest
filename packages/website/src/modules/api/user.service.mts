import {Injectable} from "@angular/core";
import {GameId} from "@eternalfest/core/game2/game-id";
import {$RunItems, RunItems} from "@eternalfest/core/run/run-items";
import {$UpdateUserOptions, UpdateUserOptions} from "@eternalfest/core/user/update-user-options";
import {$User, User} from "@eternalfest/core/user/user";
import {UserId} from "@eternalfest/core/user/user-id";
import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError} from "rxjs/internal/operators/catchError";

import {BodyRequestOptions, RestService} from "../rest/rest.service.mjs";

interface GetProfileQuery {
  until?: Date,
}

const $GetProfileQuery: RecordIoType<GetProfileQuery> = new RecordType<GetProfileQuery>({
  properties: {
    until: {type: $Date, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});

@Injectable()
export class UserService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getUserById(userId: UserId): Observable<User | null> {
    return this.#rest
      .get(["users", userId], {resType: $User})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }

  getProfile(userId: UserId, gameId: GameId, until?: Date): Observable<RunItems | null> {
    return this.#rest
      .get(["users", userId, "profiles", gameId, "items"], {
        query: {until},
        queryType: $GetProfileQuery,
        resType: $RunItems})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }


  updateUser(userId: UserId, patch: Readonly<UpdateUserOptions>): Observable<User> {
    return this.#rest.patch(["users", userId], {
      reqType: $UpdateUserOptions,
      req: patch,
      resType: $User,
    });
  }
}
