import { NgZone } from "@angular/core";

// TODO: Remove this global declaration
declare const Zone: any;

function noOp(): void {
}

/**
 * Run `fn` outside of the Angular Zone and ensure that the renderer awaits its result before stabilizing.
 */
export async function runUnzoned<T>(ngZone: NgZone, source: string, fn: () => Promise<T>): Promise<T> {

  interface OkResult {
    success: true;
    value: T;
  }

  interface ErrorResult {
    success: false;
    error: Error;
  }

  type Result = OkResult | ErrorResult;

  let result: Result | undefined = undefined;

  return new Promise<T>((resolve: (value: T) => void, reject: (err: Error) => void) => {
    const task: any = Zone.current.scheduleMacroTask(
      source,
      () => {
        if (result === undefined) {
          throw new Error("PreConditionError: Task callback called before promise resolution");
        }
        if (result.success) {
          resolve(result.value);
        } else {
          reject((result as ErrorResult).error);
        }
      },
      {},
      noOp,
      noOp,
    );
    ngZone.runOutsideAngular(fn)
      .then(
        (value: T) => {
          result = {success: true, value};
          task.invoke();
        },
        (error: Error) => {
          result = {success: false, error};
          task.invoke();
        },
      );
  });
}
