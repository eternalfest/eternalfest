import {Injectable} from "@angular/core";
import {Blob as ApiBlob,NullableBlob} from "@eternalfest/core/blob/blob";
import {BlobId} from "@eternalfest/core/blob/blob-id";
import {$Game, Game} from "@eternalfest/core/game2/game";
import {GameBuild, LocalizedBlob,resolveContentI18n} from "@eternalfest/core/game2/game-build";
import {GameId} from "@eternalfest/core/game2/game-id";
import {$GameModeKey, GameModeKey} from "@eternalfest/core/game2/game-mode-key";
import {GameIdRef} from "@eternalfest/core/game2/game-ref";
import {RawGameRef} from "@eternalfest/core/game2/raw-game-ref";
import {$ShortGameListing, ShortGameListing} from "@eternalfest/core/game2/short-game-listing";
import {$Leaderboard, Leaderboard} from "@eternalfest/core/leaderboard/leaderboard";
import {LocaleId} from "@eternalfest/core/types/locale-id";
import {HammerfestItemId} from "@eternaltwin/core/hammerfest/hammerfest-item-id";
import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {$Boolean} from "kryo/boolean";
import {$Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";
import {Observable, of as rxOf} from "rxjs";
import {catchError as rxCatchError, map as rxMap} from "rxjs/operators";

import {RestService} from "../rest/rest.service.mjs";

export interface GetGamesQuery {
  favorite: boolean;
  offset: number;
  limit: number;
}

const $GetGamesQuery: RecordIoType<GetGamesQuery> = new RecordType<GetGamesQuery>({
  properties: {
    favorite: {type: $Boolean},
    offset: {type: $Uint32},
    limit: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface SetFavorite {
  favorite: boolean;
}

const $SetFavorite: RecordIoType<SetFavorite> = new RecordType<SetFavorite>({
  properties: {
    favorite: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface GetLeaderboardQuery {
  gameMode: GameModeKey;
}

const $GetLeaderboardQuery: RecordIoType<GetLeaderboardQuery> = new RecordType<GetLeaderboardQuery>({
  properties: {
    gameMode: {type: $GameModeKey},
  },
  changeCase: CaseStyle.SnakeCase,
});

export class GameLocale {
  readonly #items: Map<HammerfestItemId, string>;

  private constructor() {
    this.#items = new Map();
  }

  public static fromXml(localeNode: Element): GameLocale {
    const locale = new GameLocale();
    const itemNodes = localeNode.querySelectorAll("lang > items > item");
    for (let i = 0; i < itemNodes.length; i++) {
      const itemNode = itemNodes[i];
      const id: string | null = itemNode.getAttribute("id");
      const name: string | null = itemNode.getAttribute("name");
      if (id !== null && name !== null) {
        locale.#items.set(id, name);
      }
    }
    return locale;
  }

  public static empty(): GameLocale {
    return new GameLocale();
  }

  public getItems(): ReadonlyMap<HammerfestItemId, string> {
    return this.#items;
  }
}

@Injectable()
export class GameService {
  readonly #rest: RestService;

  constructor(rest: RestService) {
    this.#rest = rest;
  }

  getGames(page0: number, favorite: boolean): Observable<ShortGameListing> {
    return this.#rest
      .get(["games"], {
        queryType: $GetGamesQuery,
        query: {
          favorite,
          offset: page0 * 20,
          limit: 20
        },
        resType: $ShortGameListing});
  }

  getGameByRef(gameRef: RawGameRef): Observable<Game | null> {
    return this.#rest
      .get(["games", gameRef], {resType: $Game})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }

  getGameLocale(build: GameBuild, locale: LocaleId): Observable<GameLocale> {
    const localizedContentI18n = resolveContentI18n(build, locale);
    if (localizedContentI18n !== null) {
      return this.#getGameLocaleFromContentI18n(localizedContentI18n);
    } else {
      const content = build.content;
      if (content !== null) {
        return this.#getGameLocaleFromContent(content, locale);
      }
      return rxOf(GameLocale.empty());
    }
  }

  getLeaderboardByGameId(gameId: GameId, gameMode: GameModeKey): Observable<Leaderboard | null> {
    return this.#rest
      .get(["games", gameId, "leaderboard"], {queryType: $GetLeaderboardQuery, query: {gameMode}, resType: $Leaderboard})
      .pipe(
        rxCatchError((err: Error): Observable<null> => {
          return rxOf(null);
        }),
      );
  }

  setFavorite(gameIdRef: GameIdRef, options: Readonly<SetFavorite>): Observable<unknown> {
    return this.#rest.put(["games", gameIdRef.id, "favorite"], {
      reqType: $SetFavorite,
      req: options,
      resType: $Any,
    });
  }

  #getGameLocaleFromContentI18n(lblob: LocalizedBlob): Observable<GameLocale> {
    return this.#rest.getXml(["blobs", lblob.blob.id, "raw"]).pipe(rxMap((doc: XMLDocument) => {
      return GameLocale.fromXml(doc.documentElement);
    }));
  }

  #getGameLocaleFromContent(blob: ApiBlob, preferredLocale: LocaleId): Observable<GameLocale> {
    return this.#rest.getXml(["blobs", blob.id, "raw"]).pipe(rxMap((doc: XMLDocument) => {
      const root = doc.documentElement;
      const langNodes = root.querySelectorAll("game > lang");
      if (langNodes.length > 0) {
        return GameLocale.fromXml(langNodes[0]);
      } else {
        return GameLocale.empty();
      }
    }));
  }
}
