import { NgModule } from "@angular/core";

import { LocalStorageService } from "./local-storage.service.mjs";

@NgModule({
  providers: [
    {provide: LocalStorageService},
  ],
})
export class StorageModule {
}
