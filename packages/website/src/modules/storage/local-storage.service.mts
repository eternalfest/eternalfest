import {isPlatformBrowser} from "@angular/common";
import {Inject, Injectable, PLATFORM_ID} from "@angular/core";
import {IoType, readOrThrow} from "kryo";
import {JSON_READER} from "kryo-json/json-reader";
import {JSON_WRITER} from "kryo-json/json-writer";

export class LocalStorageKey<T> {
  public readonly name: string;
  public readonly type: IoType<T>;

  constructor(name: string, type: IoType<T>) {
    this.name = name;
    this.type = type;
  }
}

@Injectable({
  providedIn: "root",
})
export class LocalStorageService {
  private readonly isBrowser: boolean;

  constructor(@Inject(PLATFORM_ID) platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  public read<T>(key: LocalStorageKey<T>): T | null {
    if (!this.isBrowser) {
      return null;
    }

    const value = window.localStorage.getItem(key.name);
    if (value === null) {
      return null;
    }
    try {
      return readOrThrow(key.type, JSON_READER, value);
    } catch (err) {
      console.error(`Couldn't retrieve '${key.name}' from local storage: ${err}`);
      window.localStorage.removeItem(key.name);
      return null;
    }
  }

  public store<T>(key: LocalStorageKey<T>, data: T) {
    if (!this.isBrowser) {
      return;
    }

    const value: string = key.type.write(JSON_WRITER, data);
    window.localStorage.setItem(key.name, value);
  }
}
