import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from "@angular/common/http";
import {Inject, Injectable, TransferState} from "@angular/core";
import {$AuthContext, AuthContext} from "@eternalfest/core/auth/auth-context";
import {readOrThrow} from "kryo";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {JSON_WRITER} from "kryo-json/json-writer";
import {SEARCH_PARAMS_WRITER} from "kryo-search-params/search-params-writer";
import {Observable, throwError as rxThrowError} from "rxjs";
import {first as rxFirst, map as rxMap, switchMap as rxSwitchMap} from "rxjs/operators";
import urljoin from "url-join";

import {BACKEND_URI, INTERNAL_AUTH_KEY} from "../../server/tokens.mjs";
import {AuthService} from "../auth/auth.service.mjs";
import {
  BlobRequestOptions,
  GetInput,
  QueryRequestOptions,
  RequestOptions,
  RestService,
  SimpleRequestOptions,
  toTransferStateKey
} from "./rest.service.mjs";

@Injectable()
export class ServerRestService extends RestService {
  readonly #auth: AuthService;
  readonly #backendUri: string;
  readonly #internalAuthKey: string;
  readonly #httpClient: HttpClient;
  readonly #transferState: TransferState;

  constructor(auth: AuthService, @Inject(BACKEND_URI) backendUri: string, @Inject(INTERNAL_AUTH_KEY) internalAuthKey: string, httpClient: HttpClient, transferState: TransferState) {
    super();
    this.#auth = auth;
    this.#backendUri = backendUri;
    this.#internalAuthKey = internalAuthKey;
    this.#httpClient = httpClient;
    this.#transferState = transferState;

  }

  public delete<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.#request("delete", route, options);
  }

  public get<Query, Res>(route: readonly string[], options: SimpleRequestOptions<Res> | QueryRequestOptions<Query, Res>): Observable<Res> {
    return this.#request("get", route, options);
  }

  public getBlob<Query>(route: readonly string[], options: BlobRequestOptions<Query>): Observable<Blob> {
    return this.#auth.auth().pipe(rxFirst(), rxSwitchMap((acx): Observable<Blob> => {
      return this.#authGetBlob(route, acx, options);
    }));
  }

  public getXml<Query>(route: readonly string[], options: BlobRequestOptions<Query>): Observable<XMLDocument> {
    return rxThrowError(() => new Error("not implemented ServerRestService#getXml"));
  }

  #authGetBlob<Query>(route: readonly string[], acx: AuthContext, options: BlobRequestOptions<Query>): Observable<Blob> {
    const token = JSON.stringify([this.#internalAuthKey, $AuthContext.write(JSON_VALUE_WRITER, acx)]);
    const path = ["api", "v1", ...route].map(encodeURIComponent).join("/");
    const url = urljoin(this.#backendUri, path);
    const rawQuery: string | undefined = options.queryType !== undefined ? options.queryType.write(SEARCH_PARAMS_WRITER, options.query) : undefined;

    return this.#httpClient
      .request("GET", url, {
        headers: new HttpHeaders({
          "Authorization": `Bearer ${token}`,
        }),
        params: new HttpParams({fromString: rawQuery ?? ""}),
        body: undefined,
        observe: "response",
        responseType: "blob",
      })
      .pipe(rxMap((res: HttpResponse<unknown>) => {
        return res.body as Blob;
      }));
  }

  public patch<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.#request("patch", route, options);
  }

  public post<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.#request("post", route, options);
  }

  public put<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.#request("put", route, options);
  }

  #request<Query, Req, Res>(method: string, route: readonly string[], options: RequestOptions<Query, Req, Res>): Observable<Res> {
    return this.#auth.auth().pipe(rxFirst(), rxSwitchMap((acx): Observable<Res> => {
      return this.#authRequest(method, route, acx, options);
    }));
  }

  #authRequest<Query, Req, Res>(method: string, route: readonly string[], acx: AuthContext, options: RequestOptions<Query, Req, Res>): Observable<Res> {
    const token = JSON.stringify([this.#internalAuthKey, $AuthContext.write(JSON_VALUE_WRITER, acx)]);
    const path = ["api", "v1", ...route].map(encodeURIComponent).join("/");
    const url = urljoin(this.#backendUri, path);
    const rawBody: string | undefined = options.reqType !== undefined ? options.reqType.write(JSON_WRITER, options.req) : undefined;
    const rawQuery: string | undefined = options.queryType !== undefined ? options.queryType.write(SEARCH_PARAMS_WRITER, options.query) : undefined;

    return this.#httpClient
      .request(method.toUpperCase(), url, {
        headers: new HttpHeaders({
          "Content-Type": "application/json",
          "Authorization": `Bearer ${token}`,
        }),
        params: new HttpParams({fromString: rawQuery ?? ""}),
        body: method === "get" ? undefined : Buffer.from(rawBody ?? ""),
        observe: "response",
        responseType: "json",
      })
      .pipe(rxMap((res: HttpResponse<unknown>) => {
        const resRaw: unknown = res.body;
        let resObj: Res;
        try {
          resObj = readOrThrow(options.resType, JSON_VALUE_READER, resRaw);
        } catch (err) {
          console.error(`API Error: ${path}`);
          console.error(err);
          throw err;
        }
        if (method === "get" && res.status === 200) {
          this.#transferState.set<unknown>(toTransferStateKey({
            route,
            queryType: options.queryType,
            query: options.query
          } as GetInput<Query>), resRaw);
        }
        return resObj;
      }));
  }
}
