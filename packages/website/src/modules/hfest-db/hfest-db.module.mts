import { NgModule } from "@angular/core";

import { HfestItemNamePipe } from "./hfest-item-name.pipe.mjs";
import { HfestItemsDbService } from "./hfest-items-db.service.mjs";

@NgModule({
  declarations: [
    HfestItemNamePipe,
  ],
  exports: [
    HfestItemNamePipe,
  ],
  providers: [
    HfestItemsDbService,
  ],
})
export class HfestDbModule {
}
