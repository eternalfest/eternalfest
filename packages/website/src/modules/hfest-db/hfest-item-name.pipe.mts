import { Pipe, PipeTransform } from "@angular/core";

import { HfestItemsDbService } from "./hfest-items-db.service.mjs";

@Pipe({
  name: "hfestItemName",
  pure: true,
  standalone: false,
})
export class HfestItemNamePipe implements PipeTransform {
  private hfestItemsDbService: HfestItemsDbService;

  constructor(hfestItemsDbService: HfestItemsDbService) {
    this.hfestItemsDbService = hfestItemsDbService;
  }

  transform(itemId: number): string {
    const name: string | undefined = this.hfestItemsDbService.getItemName("" + itemId);
    return name ?? `Item #${itemId}`;
  }
}
