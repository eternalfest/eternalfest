export type DbLocale = "fr" | "en" | "es";

export type DbItem = Readonly<{
  id: string;
  name: Record<DbLocale, string>;
  // TODO(demurgos): Rename to unlock
  required?: number;
}>;

export type DbQuest = Readonly<{
  id: string;
  title: Record<DbLocale, string>;
  description: Record<DbLocale, string>;
  require: ReadonlyArray<DbQuestRequire>;
}>;

export type DbQuestRequire = Readonly<{
  id: string;
  qty: number;
}>;

declare function require(id: string): any;

// tslint:disable-next-line:no-var-requires
export const ITEM_DB = require("./items-db.json") as ReadonlyArray<DbItem>;

export const ITEM_DEFAULT_UNLOCK: number = 10;

// tslint:disable-next-line:no-var-requires
export const QUEST_DB = require("./quests-db.json") as ReadonlyArray<DbQuest>;

export function localeIdToDbLocale(localeId: string): DbLocale {
  if (/^fr(?:-|$)/.test(localeId)) {
    return "fr";
  } else if (/^es(?:-|$)/.test(localeId)) {
    return "es";
  } else {
    return "en";
  }
}
