import { Inject, Injectable, LOCALE_ID } from "@angular/core";

import { DbItem, DbLocale, ITEM_DB, ITEM_DEFAULT_UNLOCK, localeIdToDbLocale } from "./hfest-db.mjs";

// TODO(moulins): remove all usages of this; use GameContent instead
@Injectable()
export class HfestItemsDbService {
  readonly #localeId: DbLocale;
  readonly #items: Map<string, DbItem>;

  constructor(@Inject(LOCALE_ID) localeId: string) {
    console.log(localeId);

    this.#localeId = localeIdToDbLocale(localeId);
    this.#items = new Map();
    for (const item of ITEM_DB) {
      this.#items.set(item.id, item);
    }
  }

  hasItem(itemId: string): boolean {
    return this.#items.has(itemId);
  }

  getItemName(itemId: string): string | undefined {
    const dbItem: DbItem | undefined = this.#items.get(itemId);
    if (dbItem === undefined) {
      return undefined;
    }
    return dbItem.name[this.#localeId];
  }

  getItemUnlock(itemId: number): number {
    const dbItem: DbItem | undefined = this.#items.get("" + itemId);
    if (dbItem === undefined) {
      console.warn("Item not found: " + itemId);
      return ITEM_DEFAULT_UNLOCK;
    }
    return dbItem.required ?? ITEM_DEFAULT_UNLOCK;
  }
}
