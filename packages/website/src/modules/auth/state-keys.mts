import { makeStateKey, StateKey } from "@angular/core";

export type RawAuthContext = unknown;

export const AUTH_CONTEXT_KEY: StateKey<RawAuthContext | undefined> = makeStateKey("auth_context");
