import { HttpClient, HttpHeaders } from "@angular/common/http";
import {Inject, Injectable, OnDestroy, TransferState} from "@angular/core";
import { $AuthContext, AuthContext } from "@eternalfest/core/auth/auth-context";
import { GUEST_AUTH } from "@eternalfest/core/auth/guest-auth-context";
import type { Request } from "koa";
import {readOrThrow} from "kryo";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import { concat as rxConcat, Observable, ReplaySubject, Subscription } from "rxjs";
import { NEVER as RX_NEVER } from "rxjs/internal/observable/never";
import { map as rxMap } from "rxjs/operators";
import urljoin from "url-join";

import { BACKEND_URI, REQUEST } from "../../server/tokens.mjs";
import { AuthService } from "./auth.service.mjs";
import { AUTH_CONTEXT_KEY } from "./state-keys.mjs";

@Injectable()
export class ServerAuthService extends AuthService implements OnDestroy {
  // private readonly auth$: Observable<AuthContext>;
  readonly #auth$: ReplaySubject<AuthContext>;
  readonly #authSubscription: Subscription;
  readonly #transferState: TransferState;

  constructor(
    @Inject(BACKEND_URI) backendUri: string,
    @Inject(REQUEST) request: Request,
      httpClient: HttpClient,
      transferState: TransferState,
  ) {
    super();
    this.#transferState = transferState;

    const headers: Record<string, string | string[]> = {};
    for (const [key, value] of Object.entries(request.headers)) {
      if (value !== undefined && key === "cookie") {
        headers[key] = value;
      }
    }

    const firstAuth$: Observable<AuthContext> = httpClient.request(
      "GET",
      urljoin(backendUri, "api/v1/auth/self"),
      {
        headers: new HttpHeaders(headers),
        responseType: "json",
      }
    ).pipe(rxMap((raw): AuthContext => {
      let acx: AuthContext;
      try {
        acx = readOrThrow($AuthContext, JSON_VALUE_READER, raw);
        this.#transferState.set<unknown>(AUTH_CONTEXT_KEY, raw);
      } catch (err) {
        console.error("AuthError:");
        console.error(err);
        acx = GUEST_AUTH;
      }
      return acx;
    }));

    // Prevent the `complete` event.
    const infFirstAuth$: Observable<AuthContext> = rxConcat(firstAuth$, RX_NEVER);

    this.#auth$ = new ReplaySubject(1);
    this.#authSubscription = infFirstAuth$.subscribe(this.#auth$);
  }

  auth(): Observable<AuthContext> {
    return this.#auth$;
  }

  logout(): Observable<null> {
    throw new Error("NotImplemented");
  }

  ngOnDestroy() {
    this.#authSubscription.unsubscribe();
  }
}
