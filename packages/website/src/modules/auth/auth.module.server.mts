import { NgModule } from "@angular/core";

import { ApiModule } from "../api/api.module.mjs";
import { AuthService } from "./auth.service.mjs";
import { ServerAuthService } from "./auth.service.server.mjs";
import { AuthGuard } from "./auth-guard.service.mjs";
import { Authorization } from "./authorization.service.mjs";

@NgModule({
  providers: [
    AuthGuard,
    {provide: AuthService, useClass: ServerAuthService},
    Authorization,
  ],
  imports: [
    ApiModule,
  ],
})
export class ServerAuthModule {
}
