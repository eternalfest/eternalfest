import {Injectable, OnDestroy, TransferState} from "@angular/core";
import { $AuthContext, AuthContext } from "@eternalfest/core/auth/auth-context";
import { GUEST_AUTH } from "@eternalfest/core/auth/guest-auth-context";
import {readOrThrow} from "kryo";
import { JSON_VALUE_READER } from "kryo-json/json-value-reader";
import {concat as rxConcat, NEVER as RX_NEVER, Observable, of as rxOf, ReplaySubject, Subscription} from "rxjs";
import { map as rxMap, tap as rxTap } from "rxjs/operators";

import { RestService } from "../rest/rest.service.mjs";
import { AuthService } from "./auth.service.mjs";
import { AUTH_CONTEXT_KEY, RawAuthContext } from "./state-keys.mjs";

@Injectable()
export class BrowserAuthService extends AuthService implements OnDestroy {
  readonly #rest: RestService;
  readonly #auth$: ReplaySubject<AuthContext>;
  readonly #authSubscription: Subscription;

  constructor(transferState: TransferState, rest: RestService) {
    super();
    this.#rest = rest;

    let firstAuth$: Observable<AuthContext>;

    const transferredAuth: RawAuthContext | undefined = transferState.get(AUTH_CONTEXT_KEY, undefined);
    if (transferredAuth !== undefined) {
      let auth: AuthContext = GUEST_AUTH;
      try {
        auth = readOrThrow($AuthContext, JSON_VALUE_READER, transferredAuth);
      } catch (err) {
        console.error(err);
      }
      firstAuth$ = rxOf(auth);
    } else {
      firstAuth$ = this.getSelf();
    }

    // Prevent the `complete` event.
    const infFirstAuth$: Observable<AuthContext> = rxConcat(firstAuth$, RX_NEVER);

    this.#auth$ = new ReplaySubject(1);
    this.#authSubscription = infFirstAuth$.subscribe(this.#auth$);
  }

  public auth(): Observable<AuthContext> {
    return this.#auth$;
  }

  logout(): Observable<null> {
    return this.#rest.delete(["auth", "self"], {resType: $AuthContext})
      .pipe(
        rxMap((): null => {
          this.#auth$.next(GUEST_AUTH);
          return null;
        }),
      );
  }

  private getSelf(): Observable<AuthContext> {
    return this.#rest.get(["auth", "self"], {resType: $AuthContext});
  }

  ngOnDestroy() {
    this.#authSubscription.unsubscribe();
  }
}
