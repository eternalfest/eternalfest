import { NgModule } from "@angular/core";

import { ApiModule } from "../api/api.module.mjs";
import { BrowserAuthService } from "./auth.service.browser.mjs";
import { AuthService } from "./auth.service.mjs";
import { AuthGuard } from "./auth-guard.service.mjs";
import { Authorization } from "./authorization.service.mjs";

@NgModule({
  providers: [
    AuthGuard,
    {provide: AuthService, useClass: BrowserAuthService},
    Authorization,
  ],
  imports: [
    ApiModule,
  ],
})
export class BrowserAuthModule {
}
