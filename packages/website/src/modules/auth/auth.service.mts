import { Injectable } from "@angular/core";
import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { Observable } from "rxjs";

@Injectable()
export abstract class AuthService {
  abstract auth(): Observable<AuthContext>;

  abstract logout(): Observable<null>;
}
