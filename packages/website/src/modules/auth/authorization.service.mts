import { Injectable } from "@angular/core";
import { ActorType } from "@eternalfest/core/types/user/actor-type";
import { Observable } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import { AuthService } from "./auth.service.mjs";

@Injectable()
export class Authorization {
  private authentication: AuthService;

  constructor(authenticationService: AuthService) {
    this.authentication = authenticationService;
  }

  editUserSettings(): Observable<boolean> {
    return this.authentication.auth().pipe(rxMap((auth) => auth.type === ActorType.User));
  }
}
