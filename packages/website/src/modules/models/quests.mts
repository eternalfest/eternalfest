import {RunItems} from "@eternalfest/core/run/run-items";

import {Item, ItemId, Quest, QuestRequire} from "./game-content.mjs";

export type UserProfileStats = Readonly<{
  items: RunItems;
}>;


export const EMPTY_USER_PROFILE: UserProfileStats = {
  items: new Map(),
};

export type QuestState = "started" | "partial" | "completed";

export function questStateFor(
  stats: UserProfileStats,
  statsDelta: UserProfileStats | null,
  reqs: ReadonlyArray<QuestRequire>,
): QuestState | null {
  let reqCount = 0; // The total number of required items.
  let curCount = 0; // The total number of relevant, currently-held items.
  let prevCount = 0; // The total number of relevant, previously-held items.

  // This assumes that no delta is ever negative, which implies
  // that cur/prevCount are monotonically increasing across runs.
  for (const { qty, item: { id }} of reqs) {
    const cur = stats.items.get(id) ?? 0;
    const delta = statsDelta?.items.get(id) ?? 0;

    reqCount += qty;
    curCount += Math.min(cur, qty);
    prevCount += Math.min(cur - delta, qty);
  }

  const state = curCount >= reqCount ? "completed" : curCount > 0 ? "partial" : null;

  if (statsDelta !== null) {
    if (curCount === prevCount) {
      return null;
    } else if (prevCount <= 0 && state === "partial") {
      return "started";
    }
  }

  return state;
}

const QUEST_STATE_SORT: Record<QuestState, number> = {
  started: 0,
  partial: 1,
  completed: 2,
};

export interface QuestWithState {
  quest: Quest,
  state: QuestState;
}

export function computeAndSortQuests(
  stats: UserProfileStats,
  delta: UserProfileStats | null,
  quests: Iterable<Quest>,
): Array<QuestWithState> {
  const result: Array<QuestWithState & { sortIdx: number }> = [];

  for (const quest of quests) {
    const state = questStateFor(stats, delta, quest.require);
    if (state !== null) {
      result.push({ quest, state, sortIdx: result.length });
    }
  }

  result.sort((a, b) => {
    if (a.state === b.state) {
      return a.sortIdx - b.sortIdx;
    } else {
      return QUEST_STATE_SORT[a.state] - QUEST_STATE_SORT[b.state];
    }
  });

  return result;
}

export function computeItemUnlocks(
  stats: UserProfileStats,
  delta: UserProfileStats,
  items: ReadonlyMap<ItemId, Item>,
): Array<Item> {
  const unlocked: Array<Item> = [];

  for (const itemId of delta.items.keys()) {
    const item = items.get(itemId);
    if (item !== undefined) {
      const cur = stats.items.get(itemId) ?? 0;
      if (cur >= item.unlock) {
        const prev = cur - (delta.items.get(itemId) ?? 0);
        if (prev < item.unlock) {
          unlocked.push(item);
        }
      }
    }
  }

  return unlocked;
}
