import {GameModeDisplayName} from "@eternalfest/core/game2/game-mode-display-name";
import {GameModeKey} from "@eternalfest/core/game2/game-mode-key";
import {GameModeSpec} from "@eternalfest/core/game2/game-mode-spec";
import {GameModeSpecI18n} from "@eternalfest/core/game2/game-mode-spec-i18n";
import {GameOptionDisplayName} from "@eternalfest/core/game2/game-option-display-name";
import {GameOptionKey} from "@eternalfest/core/game2/game-option-key";

export class GameModeModel {
  key: GameModeKey;
  displayName: GameModeDisplayName;

  // A map of all defined options, including hidden ones.
  allOptions: Map<GameOptionKey, CheckboxOptionModel>;

  // Available options, ready to be displayed on the UI.
  get options(): GameOptionsModel {
    if (this.#lazyOptions instanceof Map) {
      this.#lazyOptions = computeVisibleOptions(this.#lazyOptions);
    }
    return this.#lazyOptions;
  }

  #lazyOptions: ComputeOptionsArg | GameOptionsModel;

  constructor(key: GameModeKey, mode: GameModeSpec, i18n?: GameModeSpecI18n) {
    this.key = key;
    this.displayName = i18n?.displayName ?? mode.displayName;

    const allOptions: ComputeOptionsArg = new Map();
    for (const [key, optSpec] of mode.options) {
      const optI18n = i18n?.options?.get(key);
      allOptions.set(key, {
        key: key,
        displayName: optI18n?.displayName ?? optSpec.displayName,
        disabled: !optSpec.isEnabled,
        checked: optSpec.defaultValue,
        visible: optSpec.isVisible,
      });
    }

    this.allOptions = allOptions;
    this.#lazyOptions = allOptions;
  }

  selectedOptions(): Array<GameOptionKey> {
    const options = this.options;
    const allSelected = options.required.slice();
    for (const option of options.checkbox.values()) {
      if (option.checked) {
        allSelected.push(option.key);
      }
    }
    for (const option of options.dropdown.values()) {
      if (option.selected !== null) {
        allSelected.push(option.selected);
      }
    }
    return allSelected;
  }
}

export type GameOptionsModel = Readonly<{
  // Checkbox-style options; can be toggled independently.
  checkbox: ReadonlyArray<CheckboxOptionModel>,
  // Dropdown-style options; at most one selected per group.
  // These are auto-detected from the option keys: options of
  // the form `set_<group>_*` will be grouped together.
  dropdown: ReadonlyArray<DropdownOptionModel>,
  // Hidden options that should always be enabled.
  required: ReadonlyArray<GameOptionKey>,
}>;

export type GameOptionModel = Readonly<{
  key: GameOptionKey,
  displayName: GameOptionDisplayName,
  disabled: boolean,
}>;

export type CheckboxOptionModel = GameOptionModel & {
  // UI state for the option checkbox.
  checked: boolean,
};

export type DropdownOptionModel = {
  readonly choices: ReadonlyArray<GameOptionModel>,

  // UI state for the option dropdown selection.
  selected: GameOptionKey | null,
};

type ComputeOptionsArg = Map<string, CheckboxOptionModel & { visible: boolean }>;

function computeVisibleOptions(options: ComputeOptionsArg): GameOptionsModel {
  type DropdownBuilder = DropdownOptionModel & { choices: Array<GameOptionModel> };

  const required: Array<GameOptionKey> = [];
  const checkbox: Array<CheckboxOptionModel> = [];
  const dropdownGroups = new Map<string, DropdownBuilder>();

  for (const opt of options.values()) {
    const key = opt.key;

    // Skip hidden options.
    if (!opt.visible) {
      if (opt.checked) {
        required.push(key);
      }
      continue;
    }

    let group: DropdownBuilder | undefined = undefined;

    // Put the option inside a group if it's of the form `set_word_*`
    {
      const PREFIX: string = "set_";
      const groupEnd = +key.startsWith(PREFIX) && key.indexOf("_", PREFIX.length);

      if (groupEnd > 0) {
        const groupKey = key.substring(0, groupEnd) + "_*";

        group = dropdownGroups.get(groupKey);
        if (group === undefined) {
          group = {
            choices: [],
            selected: null,
          };
          dropdownGroups.set(groupKey, group);
        }
      }
    }

    if (group === undefined) {
      // No group: this option is a simple checkbox.
      checkbox.push(opt);
    } else {
      group.choices.push(opt);

      // Find which dropdown choice to pre-select (prioritizing explicitely
      // selected choices over implicitely available ones).
      // Note: we don't detect unsatisfiable cases (e.g. multiple required
      // options in a group), but these will be rejected down the line anyways.
      if (opt.checked || (!opt.disabled && group.selected === null)) {
        group.selected = key;
      }
    }
  }

  const dropdown = [...dropdownGroups.values()];
  return { required, checkbox, dropdown };
}
