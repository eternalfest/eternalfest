import {Component, Input} from "@angular/core";

import {Quest, QuestRequire} from "./game-content.mjs";
import {QuestState, UserProfileStats} from "./quests.mjs";

@Component({
  selector: "ef-quest",
  templateUrl: "./quest.component.html",
  styleUrls: ["./quest.component.scss"],
  standalone: true,
})
export class QuestComponent {
  @Input("profile")
  public profile!: UserProfileStats;

  @Input("quest")
  public quest!: Quest;

  @Input("state")
  public state: QuestState | null = null;

  constructor() {}

  public displayCount(req: QuestRequire): number {
    const count = this.profile.items.get(req.item.id) ?? 0;
    // Never display an item count higher than the quest's requirement.
    return Math.min(count, req.qty);
  }
}
