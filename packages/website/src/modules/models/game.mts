import {Blob} from "@eternalfest/core/blob/blob";
import {Game} from "@eternalfest/core/game2/game";
import {GameChannelKey} from "@eternalfest/core/game2/game-channel-key";
import {GameChannelPermission} from "@eternalfest/core/game2/game-channel-permission";
import {GameDescription} from "@eternalfest/core/game2/game-description";
import {GameDisplayName} from "@eternalfest/core/game2/game-display-name";
import {GameId} from "@eternalfest/core/game2/game-id";
import { GameKey } from "@eternalfest/core/game2/game-key";
import {GameModeKey} from "@eternalfest/core/game2/game-mode-key";
import {ShortGame} from "@eternalfest/core/game2/short-game";
import {ShortGameBuildI18n} from "@eternalfest/core/game2/short-game-build-i18n";
import {ShortGameChannel} from "@eternalfest/core/game2/short-game-channel";
import {LocaleId} from "@eternalfest/core/types/locale-id";
import {VersionString} from "@eternalfest/core/types/version-string";
import {ShortUser} from "@eternalfest/core/user/short-user";

import {GameModeModel} from "./game-mode.mjs";

export class ShortGameModel {
  readonly id: GameId;
  readonly key: GameKey | null;

  readonly owner: ShortUser;
  readonly channel: GameChannelKey;
  readonly version: VersionString;
  readonly private: boolean;
  readonly publicationDate: Date | null;

  readonly displayName: GameDisplayName;
  readonly description: GameDescription;
  readonly icon: Blob | null;

  protected constructor(game: ShortGame | Game, channel: ShortGameChannel, i18n?: ShortGameBuildI18n) {
    this.id = game.id;
    this.key = game.key;
    this.owner = game.owner;
    this.channel = channel.key;
    this.version = channel.build.version;
    this.private = channel.defaultPermission === GameChannelPermission.None;
    this.publicationDate = channel.publicationDate;
    this.displayName = i18n?.displayName ?? channel.build.displayName;
    this.description = i18n?.description ?? channel.build.description;
    this.icon = i18n?.icon ?? channel.build.icon;
  }

  static of(game: ShortGame, locale: LocaleId, channel?: GameChannelKey): ShortGameModel | null {
    const chanItems = game.channels.items;
    let chan: ShortGameChannel | null = null;
    if (channel === undefined && chanItems.length > 0) {
      chan = chanItems[0];
    } else {
      for (const c of chanItems) {
        if (c.key === channel) {
          chan = c;
          break;
        }
      }
    }

    return chan && new ShortGameModel(game, chan, chan.build.i18n.get(locale));
  }
}

export class GameModel extends ShortGameModel {
  // All defined modes, including hidden ones.
  readonly allModes: ReadonlyMap<GameModeKey, Omit<GameModeModel, "options">>;

  // All available modes, ready to be displayed on the UI.
  readonly modes: ReadonlyArray<GameModeModel>;

  get hasQuests(): boolean {
    // For now, only Hammerfest has hardcoded support for front-end quests.
    return this.key === "hammerfest";
  }

  constructor(game: Game, locale: LocaleId) {
    const channel = game.channels.active;
    const i18n = channel.build.i18n?.get(locale);

    const allModes: Map<GameModeKey, GameModeModel> = new Map();
    const modes: Array<GameModeModel> = [];
    for (const [modeKey, modeSpec] of channel.build.modes) {
      const mode = new GameModeModel(modeKey, modeSpec, i18n?.modes?.get(modeKey));

      allModes.set(mode.key, mode);
      if (modeSpec.isVisible) {
        modes.push(mode);
      }
    }

    super(game, channel, i18n);
    this.allModes = allModes;
    this.modes = modes;
  }
}
