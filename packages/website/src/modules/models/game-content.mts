import {GameBuild} from "@eternalfest/core/game2/game-build";
import {HfestItemId} from "@eternalfest/core/hfest-api/hfest-item-id";
import {LocaleId} from "@eternalfest/core/types/locale-id";

import {GameLocale} from "../api/game.service.mjs";
import {DbLocale, ITEM_DB, ITEM_DEFAULT_UNLOCK, localeIdToDbLocale, QUEST_DB} from "../hfest-db/hfest-db.mjs";

export type ItemId = HfestItemId;

export type Item = Readonly<{
  id: ItemId,
  displayName: string,
  iconUri: string | null,
  unlock: number,
}>;

export type QuestId = string;

export type Quest = Readonly<{
  id: QuestId,
  displayName: string,
  description: string,
  require: ReadonlyArray<QuestRequire>,
}>;

export type QuestRequire = Readonly<{
  type: "item",
  item: Item,
  qty: number,
}>;

export class GameContent {
  readonly items: ReadonlyMap<ItemId, Item>;
  readonly quests: ReadonlyArray<Quest>;

  constructor(useHardcodedHfestQuests: boolean, build: GameBuild, gameLocale: GameLocale, locale: LocaleId) {
    const dbLocale: DbLocale = localeIdToDbLocale(locale);
    const useHfestQuests: boolean = useHardcodedHfestQuests;
    const customNames: Map<ItemId, string> = new Map([...gameLocale.getItems()]);

    const items: Map<ItemId, Item> = new Map();

    // The default Hammerfest items.
    for (const item of ITEM_DB) {
      const id = item.id;
      items.set(id, {
        id,
        displayName: customNames.get(id) ?? item.name[dbLocale],
        iconUri: `/assets/items/${id}.png`,
        unlock: item.required ?? ITEM_DEFAULT_UNLOCK,
      });

      customNames.delete(id);
    }

    // Add any remaining custom items, with default metadata and no icon.
    for (const [id, name] of customNames) {
      items.set(id, {
        id,
        displayName: name,
        iconUri: null,
        unlock: ITEM_DEFAULT_UNLOCK,
      });
    }

    // Add hardcoded quests if requested.
    const quests: Array<Quest> = [];
    for (const quest of useHfestQuests ? QUEST_DB : []) {
      const require: Array<QuestRequire> = [];
      for (const r of quest.require) {
        const item = items.get(r.id);
        if (item === undefined) {
          console.error(`Unknown item #${r.id} in quest #${quest.id}; skipping`);
          continue;
        }
        require.push({
          type: "item",
          item,
          qty: r.qty,
        });
      }

      quests.push({
        id: quest.id,
        displayName: quest.title[dbLocale],
        description: quest.description[dbLocale],
        require,
      });
    }

    this.items = items;
    this.quests = quests;
  }
}
