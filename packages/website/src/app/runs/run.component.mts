import {ChangeDetectorRef, Component, Inject, LOCALE_ID, NgZone, OnDestroy, OnInit, PLATFORM_ID} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {CreateRunOptions} from "@eternalfest/core/run/create-run-options";
import {Run} from "@eternalfest/core/run/run";
import {ObjectType} from "@eternalfest/core/types/object-type";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {firstValueFrom, Observable, Subscription} from "rxjs";

import {GameService} from "../../modules/api/game.service.mjs";
import {RunService} from "../../modules/api/run.service.mjs";
import {UserService} from "../../modules/api/user.service.mjs";
import {AuthService} from "../../modules/auth/auth.service.mjs";
import {GameModel} from "../../modules/models/game.mjs";
import {GameContent, Item} from "../../modules/models/game-content.mjs";
import {computeAndSortQuests, computeItemUnlocks, QuestWithState, UserProfileStats} from "../../modules/models/quests.mjs";

interface RouteData {
  run: Run;
}

// TODO(moulins): make proper `RunModel` type in `modules/models`.
interface RunResult {
  readonly profile: UserProfileStats,
  readonly quests: ReadonlyArray<QuestWithState>,
  readonly unlocks: ReadonlyArray<Item>,
}

@Component({
  selector: "ef-run",
  templateUrl: "./run.component.html",
  styleUrls: ["./run.component.scss"],
  standalone: false,
})
export class RunComponent implements OnInit, OnDestroy {
  public run: Run | undefined;
  public runResult: RunResult | undefined;
  public pending: boolean;
  public readonly isEtwinApp: boolean;

  readonly #changeDetectorRef: ChangeDetectorRef;
  private readonly localeId: string;
  private readonly authentication: AuthService;
  private readonly ngZone: NgZone;
  private readonly route: ActivatedRoute;
  private readonly router: Router;
  private readonly gameService: GameService;
  private readonly runService: RunService;
  private readonly userService: UserService;

  private readonly subscriptions: Subscription[];

  constructor(
    authentication: AuthService,
    ngZone: NgZone,
    route: ActivatedRoute,
    router: Router,
    gameService: GameService,
    runService: RunService,
    userService: UserService,
    @Inject(LOCALE_ID) localeId: string,
    @Inject(PLATFORM_ID) platformId: Object,
    changeDetectorRef: ChangeDetectorRef,
  ) {
    this.localeId = localeId;
    // `isPlatformBrowser(PLATFORM_ID)` returns false in the etwin app for some reason, so we can't use it.
    this.isEtwinApp = navigator?.userAgent !== undefined && /etwin|eternaltwin/.test(navigator.userAgent);
    this.authentication = authentication;
    this.ngZone = ngZone;
    this.route = route;
    this.router = router;
    this.gameService = gameService;
    this.runService = runService;
    this.userService = userService;
    this.subscriptions = [];
    this.#changeDetectorRef = changeDetectorRef;

    this.run = undefined;
    this.pending = false;
  }

  ngOnInit(): void {
    const routeData$ = this.route.data as Observable<RouteData>;
    this.subscriptions.push(routeData$
      .subscribe((data: RouteData) => {
        // if (data.run.game.displayName === "Goodbye") {
        //   this.themeService.enableGoodbyeTheme();
        // } else if (this.run !== undefined) {
        //   this.themeService.disableGoodbyeTheme();
        // }
        this.run = data.run;
        this.runResult = undefined;
      }));

    this.subscriptions.push(routeData$
      .subscribe(async (data: RouteData) => {
        const result = data.run.result;
        if (result === null) {
          return;
        }

        // The `run` object doesn't give us enough infos so we need to get it ourselves :c
        // TODO: improve backend to reduce the need for extra requests?
        const rawGame = await firstValueFrom(this.gameService.getGameByRef(data.run.game.id));
        if (rawGame === null || rawGame?.channels.active.key !== data.run.channel.key) {
          console.warn("Missing game or invalid game");
          return;
        }

        const gameModel = new GameModel(rawGame, this.localeId);
        const runItemsPromise = firstValueFrom(this.userService.getProfile(data.run.user.id, gameModel.id, result.createdAt));
        const gameLocalePromise = firstValueFrom(this.gameService.getGameLocale(rawGame.channels.active.build, this.localeId));
        const [runItems, gameLocale] = await Promise.all([runItemsPromise, gameLocalePromise]);
        if (runItems === null) {
          return;
        }
        const content = new GameContent(gameModel.hasQuests, rawGame.channels.active.build, gameLocale, this.localeId);

        const profile = { items: runItems };
        this.runResult = {
          profile,
          quests: computeAndSortQuests(profile, result, content.quests),
          unlocks: computeItemUnlocks(profile, result, content.items),
        };
        this.#changeDetectorRef.markForCheck();
      }));
  }

  ngOnDestroy(): void {
    // if (this.run !== undefined && this.run.game.displayName === "Goodbye") {
    //   this.themeService.disableGoodbyeTheme();
    // }
    for (const subscription of this.subscriptions) {
      subscription.unsubscribe();
    }
    this.subscriptions.length = 0;
  }

  async onSubmit(event: Event): Promise<void> {
    event.preventDefault();
    const createRunOptions: CreateRunOptions = await this.read();
    if (this.pending) {
      return;
    }
    this.pending = true;
    try {
      const run: Run = await firstValueFrom(this.runService.createRun(createRunOptions));
      await this.ngZone.run(() => this.router.navigateByUrl(`/runs/${run.id}`));
    } catch (err) {
      // this.errorMessage = err.message;
      console.error(err);
    }
    this.pending = false;
  }

  public sumScores(scores: readonly number[]): number {
    let result: number = 0;
    for (const score of scores) {
      result += score;
    }
    return result;
  }

  private async read(): Promise<CreateRunOptions> {
    const auth: AuthContext = await firstValueFrom(this.authentication.auth());
    if (auth.type !== ActorType.User) {
      throw new Error("NotAuthenticated");
    }
    return {
      user: {type: ObjectType.User, id: auth.user.id},
      game: {id: this.run!.game.id},
      gameMode: this.run!.gameMode,
      gameOptions: [...this.run!.gameOptions],
      channel: this.run!.channel.key,
      version: this.run!.build.version,
      settings: {
        detail: this.run!.settings.detail,
        music: this.run!.settings.music,
        shake: this.run!.settings.shake,
        sound: this.run!.settings.sound,
        volume: this.run!.settings.volume,
        locale: this.run!.settings.locale,
      },
    };
  }
}
