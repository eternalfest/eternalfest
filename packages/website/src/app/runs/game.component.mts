import { isPlatformBrowser } from "@angular/common";
import {Component, ElementRef, Inject, Input, NgZone, OnDestroy, OnInit, PLATFORM_ID, ViewChild} from "@angular/core";
import { $Run, Run } from "@eternalfest/core/run/run";
import { RunSettings } from "@eternalfest/core/run/run-settings";
import { JSON_WRITER } from "kryo-json/json-writer";
import { BehaviorSubject, Subscription } from "rxjs";

import { ConsoleService } from "../../modules/console/console.service.mjs";

interface SwfSocketServer {
  methodName: string;
}

function getFlashVars(
  elementId: string,
  run: Run | undefined,
  swfSocketServer: SwfSocketServer | undefined,
): string {
  if (run !== undefined) {
    const mode: string = run.gameMode;
    const options: string[] = Array.from(run.gameOptions);
    const settings: RunSettings = run.settings;
    const locale: string = run.settings.locale;
    const loaderParameters = new URLSearchParams();
    loaderParameters.append("object_id", elementId);
    loaderParameters.append("run", $Run.write(JSON_WRITER, run));
    loaderParameters.append("game", run.game.id);
    loaderParameters.append("options", JSON.stringify({mode, options, settings: {...settings, volume: 100}, locale}));
    if (swfSocketServer !== undefined) {
      loaderParameters.append("server", swfSocketServer.methodName);
    }
    return loaderParameters.toString();
  }
  return "";
}

@Component({
  selector: "ef-game",
  templateUrl: "./game.component.html",
  styleUrls: ["./game.component.scss"],
  standalone: false,
})
export class GameComponent implements OnInit, OnDestroy {
  flashVars: string | undefined;
  swfSocketServer: SwfSocketServer | undefined;

  @ViewChild("container", {static: true})
    containerElement!: ElementRef;

  @Input("run")
  set gameUri(run: Run | undefined) {
    this.run$.next(run);
  }

  get run(): Run | undefined {
    return this.run$.getValue();
  }

  public elementId: string;
  private run$: BehaviorSubject<Run | undefined>;

  private readonly isBrowser: boolean;
  private readonly consoleService: ConsoleService;
  private readonly ngZone: NgZone;
  readonly #subscriptions: Subscription[];

  constructor(@Inject(PLATFORM_ID) platformId: Object, consoleService: ConsoleService, ngZone: NgZone) {
    this.isBrowser = isPlatformBrowser(platformId);
    this.consoleService = consoleService;
    this.ngZone = ngZone;
    this.#subscriptions = [];

    this.elementId = `swf${Math.floor((1 << 16) * Math.random())}`;

    this.run$ = new BehaviorSubject<Run | undefined>(undefined);
    this.flashVars = undefined;
    if (!this.isBrowser) {
      this.swfSocketServer = undefined;
    } else {
      // this.swfSocketServer = ...;
    }
  }

  ngOnInit(): void {
    this.#subscriptions.push(this.run$.subscribe((run: Run | undefined): void => {
      this.flashVars = getFlashVars(this.elementId, run!, this.swfSocketServer);
      this.refreshDom();
    }));
  }

  ngOnDestroy(): void {
    for (const sub of this.#subscriptions) {
      sub.unsubscribe();
    }
    this.#subscriptions.length = 0;
  }

  refreshDom() {
    if (!this.isBrowser) {
      return;
    }
    const container: HTMLDivElement = this.containerElement.nativeElement;
    const children: Node[] = Array.from(container.children);
    for (const child of children) {
      container.removeChild(child);
    }
    const embed: HTMLEmbedElement = document.createElement("embed");
    embed.setAttribute("id", this.elementId);
    embed.setAttribute("type", "application/x-shockwave-flash");
    embed.setAttribute("src", "/assets/loader.swf");
    embed.setAttribute("name", "game");
    embed.setAttribute("quality", "high");
    embed.setAttribute("allowscriptaccess", "always");
    if (this.flashVars !== undefined) {
      embed.setAttribute("flashvars", this.flashVars);
    }
    embed.setAttribute("width", "420");
    embed.setAttribute("height", "520");
    container.appendChild(embed);
  }
}
