import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module.mjs";
import { ModelsModule } from "../../modules/models/models.module.mjs";
import {QuestComponent} from "../../modules/models/quest.component.mjs";
import { GameComponent } from "./game.component.mjs";
import { RunComponent } from "./run.component.mjs";
import { RunsRoutingModule } from "./runs-routing.module.mjs";

@NgModule({
  declarations: [
    GameComponent,
    RunComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    HelpersModule,
    ModelsModule,
    QuestComponent,
    RunsRoutingModule,
  ],
})
export class RunsModule {
}
