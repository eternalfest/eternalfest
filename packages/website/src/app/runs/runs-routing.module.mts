import { Injectable, NgModule, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { Run } from "@eternalfest/core/run/run";
import {firstValueFrom} from "rxjs";

import { RunService } from "../../modules/api/run.service.mjs";
import { RunComponent } from "./run.component.mjs";

@Injectable()
export class RunResolverService implements Resolve<Run> {
  private readonly ngZone: NgZone;
  private readonly router: Router;
  private readonly run: RunService;

  constructor(ngZone: NgZone, router: Router, run: RunService) {
    this.ngZone = ngZone;
    this.router = router;
    this.run = run;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Run | never> {
    const runId: string | null = route.paramMap.get("run_id");
    if (runId !== null) {
      const run: Run | null = await firstValueFrom(this.run.getRunById(runId));
      if (run !== null) {
        return run;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games"));
    return undefined as never;
  }
}

const routes: Routes = [
  {
    path: ":run_id",
    component: RunComponent,
    pathMatch: "full",
    resolve: {
      run: RunResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [RunResolverService],
})
export class RunsRoutingModule {
}
