import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ShortGameListing} from "@eternalfest/core/game2/short-game-listing";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {NEVER as RX_NEVER, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {AuthService} from "../../modules/auth/auth.service.mjs";

@Component({
  selector: "ef-game-list",
  templateUrl: "./game-list.component.html",
  standalone: false,
})
export class GameListComponent {
  readonly #route: ActivatedRoute;
  readonly #authentication: AuthService;

  public games$: Observable<ShortGameListing>;
  public isAuthenticated$: Observable<boolean>;

  constructor(
    authentication: AuthService,
    route: ActivatedRoute,
  ) {
    this.#authentication = authentication;
    this.#route = route;
    this.games$ = RX_NEVER;
    this.isAuthenticated$ = RX_NEVER;
  }

  ngOnInit(): void {
    interface RouteData {
      games: ShortGameListing;
    }

    const routeData$: Observable<RouteData> = this.#route.data as any;
    this.games$ = routeData$.pipe(rxMap(({games}: RouteData) => games));
    this.isAuthenticated$ = this.#authentication.auth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }
}
