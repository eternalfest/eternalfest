import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {FormsModule} from "@angular/forms";

import {HelpersModule} from "../../modules/helpers/helpers.module.mjs";
import {HfestDbModule} from "../../modules/hfest-db/hfest-db.module.mjs";
import {ModelsModule} from "../../modules/models/models.module.mjs";
import {QuestComponent} from "../../modules/models/quest.component.mjs";
import {GameIconUriPipe} from "./game-icon-uri.pipe.mjs";
import {GameLeaderboardComponent} from "./game-leaderboard.component.mjs";
import {GameListComponent} from "./game-list.component.mjs";
import {GameListItemComponent} from "./game-list-item.component.mjs";
import {GameOptionNameComponent} from "./game-option-name.component.mjs";
import {GamePaginationComponent} from "./game-pagination.component.mjs";
import {GameProfileItemsComponent} from "./game-profile-items.component.mjs";
import {GameProfileQuestsComponent} from "./game-profile-quests.component.mjs";
import {GameViewComponent} from "./game-view.component.mjs";
import {GamesRoutingModule} from "./games-routing.module.mjs";
import {ItemTableComponent} from "./item-table.component.mjs";

@NgModule({
  declarations: [
    GameIconUriPipe,
    GameLeaderboardComponent,
    GameProfileItemsComponent,
    GameProfileQuestsComponent,
    GameListComponent,
    GameListItemComponent,
    GameOptionNameComponent,
    GamePaginationComponent,
    GameViewComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    GamesRoutingModule,
    HelpersModule,
    HfestDbModule,
    ItemTableComponent,
    ModelsModule,
    QuestComponent,
  ],
})
export class GamesModule {
}
