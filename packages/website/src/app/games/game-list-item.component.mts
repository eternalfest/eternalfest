import {Component, Inject, Input, LOCALE_ID} from "@angular/core";
import {ShortGame} from "@eternalfest/core/game2/short-game";

import {ShortGameModel} from "../../modules/models/game.mjs";

@Component({
  selector: "ef-game-list-item",
  templateUrl: "./game-list-item.component.html",
  standalone: false,
})
export class GameListItemComponent {
  #localeId: string;
  game: ShortGameModel | null = null;

  @Input("game")
  set _game(game: ShortGame | null | undefined) {
    game = game ?? null;
    if (game !== null) {
      this.game = ShortGameModel.of(game, this.#localeId);
    }
  }

  constructor(@Inject(LOCALE_ID) localeId: string) {
    this.#localeId = localeId;
  }
}
