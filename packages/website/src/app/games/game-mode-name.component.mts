import {Component, Input} from "@angular/core";

@Component({
  selector: "ef-game-mode-name",
  template: "{{mode}}",
  standalone: true,
})
export class GameModeNameComponent {
  @Input("mode")
    mode: string | undefined;
}
