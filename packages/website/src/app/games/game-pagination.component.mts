import { Component, Input } from "@angular/core";

@Component({
  selector: "ef-game-pagination",
  templateUrl: "./game-pagination.component.html",
  styleUrls: [],
  standalone: false,
})
export class GamePaginationComponent {
  #offset: number | undefined;
  #limit: number | undefined;
  #count: number | undefined;

  public page1: number;
  public maxPage: number;
  public pages: number[];

  @Input("offset")
  set offset(offset: number | undefined) {
    this.#offset = offset;
    this.#refresh();
  }

  get offset(): number | undefined {
    return this.#offset;
  }

  @Input("limit")
  set limit(limit: number | undefined) {
    this.#limit = limit;
    this.#refresh();
  }

  get limit(): number | undefined {
    return this.#limit;
  }

  @Input("count")
  set count(count: number | undefined) {
    this.#count = count;
    this.#refresh();
  }

  get count(): number | undefined {
    return this.#count;
  }

  constructor() {
    this.page1 = 1;
    this.maxPage = 0;
    this.pages = [];
    this.#refresh();
  }

  #refresh() {
    const offset = this.#offset;
    const limit = this.#limit;
    const count = this.#count;
    if (offset === undefined || limit === undefined || limit <= 0 || count === undefined) {
      return;
    }
    this.page1 = 1 + Math.floor(offset / limit);
    this.maxPage = Math.ceil(count / limit);
    this.pages = [];
    for (let i = 1; i <= this.maxPage; i++) {
      this.pages.push(i);
    }
  }
}
