import {Component, Inject, LOCALE_ID, NgZone, OnDestroy, OnInit} from "@angular/core";
import {ActivatedRoute, Router} from "@angular/router";
import {AuthContext} from "@eternalfest/core/auth/auth-context";
import {Game} from "@eternalfest/core/game2/game";
import {CreateRunOptions} from "@eternalfest/core/run/create-run-options";
import {Run} from "@eternalfest/core/run/run";
import {$RunSettings, RunSettings} from "@eternalfest/core/run/run-settings";
import {ObjectType} from "@eternalfest/core/types/object-type";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {firstValueFrom, Observable, Subscription} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {GameService} from "../../modules/api/game.service.mjs";
import {RunService} from "../../modules/api/run.service.mjs";
import {AuthService} from "../../modules/auth/auth.service.mjs";
import {GameModel} from "../../modules/models/game.mjs";
import {GameModeModel} from "../../modules/models/game-mode.mjs";
import {LocalStorageKey, LocalStorageService} from "../../modules/storage/local-storage.service.mjs";

const GAME_SETTINGS_KEY: LocalStorageKey<Omit<RunSettings, "locale">> = new LocalStorageKey(
  "user/game-settings",
  $RunSettings.omit(["locale"]),
);

@Component({
  selector: "ef-game-view",
  templateUrl: "./game-view.component.html",
  styleUrls: ["./game-view.component.scss"],
  standalone: false,
})
export class GameViewComponent implements OnInit, OnDestroy {
  public game: GameModel | undefined;
  public gameMode: GameModeModel | undefined;

  public detail: boolean;
  public shake: boolean;
  public sound: boolean;
  public music: boolean;
  public pending: boolean;
  public newPublicationDate: string;
  public favorite: boolean;

  public readonly isAdmin$: Observable<boolean>;
  public readonly isAuthenticated$: Observable<boolean>;

  readonly #localeId: string;
  readonly #authentication: AuthService;
  readonly #gameService: GameService;
  readonly #ngZone: NgZone;
  readonly #route: ActivatedRoute;
  readonly #router: Router;
  readonly #runService: RunService;
  readonly #localStorageService: LocalStorageService;
  readonly #subscriptions: Subscription[];

  constructor(
    @Inject(LOCALE_ID) localeId: string,
      authentication: AuthService,
      gameService: GameService,
      ngZone: NgZone,
      route: ActivatedRoute,
      router: Router,
      runService: RunService,
      localStorageService: LocalStorageService,
  ) {
    this.#localeId = localeId;
    this.#authentication = authentication;
    this.#gameService = gameService;
    this.#ngZone = ngZone;
    this.#route = route;
    this.#router = router;
    this.#runService = runService;
    this.#localStorageService = localStorageService;
    this.isAdmin$ = authentication.auth()
      .pipe(rxMap((auth) => auth.type === ActorType.User && auth.isAdministrator));
    this.isAuthenticated$ = authentication.auth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
    this.#subscriptions = [];

    this.game = undefined;
    this.gameMode = undefined;
    this.pending = false;
    this.detail = true;
    this.shake = true;
    this.sound = true;
    this.music = true;
    this.favorite = false;
    this.newPublicationDate = (new Date()).toISOString();

    const settings = this.#localStorageService.read(GAME_SETTINGS_KEY);
    if (settings !== null) {
      this.detail = settings.detail;
      this.shake = settings.shake;
      this.sound = settings.sound;
      this.music = settings.music;
    }
  }

  ngOnInit(): void {
    this.#subscriptions.push(this.#route.data.subscribe((data: any & { data: Game }) => {
      this.game = new GameModel(data.game as Game, this.#localeId);
      // Select the first available game mode, if it exists.
      this.gameMode = this.game.modes[0];
    }));
  }

  ngOnDestroy(): void {
    for (const sub of this.#subscriptions) {
      sub.unsubscribe();
    }
    this.#subscriptions.length = 0;
  }

  async addFavorite(event: Event): Promise<void> {
    event.preventDefault();
    try {
      await firstValueFrom(this.#gameService.setFavorite(this.game!, {favorite: true}));
    } catch (err) {
      console.error(err);
    }
    // todo: don't ignore errors
    this.favorite = true;
  }

  async removeFavorite(event: Event): Promise<void> {
    event.preventDefault();
    try {
      await firstValueFrom(this.#gameService.setFavorite(this.game!, {favorite: false}));
    } catch (err) {
      console.error(err);
    }
    // todo: don't ignore errors
    this.favorite = false;
  }

  async onSubmit(event: Event): Promise<void> {
    event.preventDefault();
    if (this.pending) {
      return;
    }

    try {
      this.pending = true;

      const createRunOptions: CreateRunOptions = await this.#read();
      this.#localStorageService.store(GAME_SETTINGS_KEY, createRunOptions.settings);
      const run: Run = await firstValueFrom(this.#runService.createRun(createRunOptions));

      await this.#ngZone.run(() => this.#router.navigateByUrl(`/runs/${run.id}`));
    } catch (err) {
      console.error(err);
    } finally {
      this.pending = false;
    }
  }

  onOptionChange(event: Event): void {
    event.preventDefault();
    const target = event.target;
    const mode = this.gameMode;
    if (mode === undefined) {
      return;
    }

    if (target instanceof HTMLInputElement) {
      mode.options.checkbox[+target.name].checked = target.checked;
    } else if (target instanceof HTMLSelectElement) {
      mode.options.dropdown[+target.name].selected = target.value;
    }
  }

  async #read(): Promise<CreateRunOptions> {
    const auth: AuthContext = await firstValueFrom(this.#authentication.auth());
    if (auth.type !== ActorType.User) {
      throw new Error("NotAuthenticated");
    }

    if (this.game === undefined || this.gameMode === undefined) {
      throw new Error("NoModeAvailable");
    }

    return {
      user: {type: ObjectType.User, id: auth.user.id},
      game: {id: this.game.id},
      gameMode: this.gameMode.key,
      gameOptions: this.gameMode.selectedOptions(),
      channel: this.game.channel,
      version: this.game.version,
      settings: {
        detail: this.detail,
        music: this.music,
        shake: this.shake,
        sound: this.sound,
        volume: 100,
        locale: this.#localeId,
      },
    };
  }
}
