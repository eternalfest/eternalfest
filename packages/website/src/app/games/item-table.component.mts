import {Component, Input} from "@angular/core";
import {RunItems} from "@eternalfest/core/run/run-items";

import {GameContent, Item} from "../../modules/models/game-content.mjs";

export interface ItemCount {
  item: Item;
  count: number;
}

@Component({
  selector: "ef-item-table",
  template: `
    <table>
      <thead>
      <tr>
        <th i18n="Profile item images header@@games/profile.header-image">Image</th>
        <th i18n="Profile item count header@@games/profile.header-count">Quantity</th>
        <th i18n="Profile item names header@@games/profile.header-name">Name</th>
      </tr>
      </thead>
      <tbody>
        @for (ic of itemCounts; track ic.item.id) {
          <tr>
            <td>
              @if (ic.item.iconUri !== null) {
                <img [src]="ic.item.iconUri" />
              }
            </td>
            <td>{{ic.count}}</td>
            <td>{{ic.item.displayName}}</td>
          </tr>
        }
      </tbody>
    </table>`,
  standalone: true,
})
export class ItemTableComponent {
  #items: RunItems | undefined;
  #gameContent: GameContent | undefined;

  itemCounts: ItemCount[] | undefined;

  @Input("items")
  set items(items: RunItems | undefined) {
    this.#items = items;
    this.#refreshViewData();
  }

  @Input("content")
  set content(content: GameContent | undefined) {
    this.#gameContent = content;
    this.#refreshViewData();
  }

  constructor() {}

  #refreshViewData() {
    if (this.#items !== undefined && this.#gameContent !== undefined) {
      this.itemCounts = getItemCounts(this.#items, this.#gameContent);
    } else {
      this.itemCounts = undefined;
    }
  }
}

function getItemCounts(runItems: RunItems, content: GameContent): ItemCount[] {
  const result: ItemCount[] = [];
  for (const [id, count] of runItems) {
    const item = content.items.get(id);
    if (item !== undefined) {
      result.push({ item, count });
    } else {
      console.warn(`Unknown item id #${id} (count: ${count})`);
    }
  }
  result.sort(compareItemIds);
  return result;
}

function compareItemIds(a: ItemCount, b: ItemCount): number {
  const aNum = parseInt(a.item.id, 10);
  const bNum = parseInt(b.item.id, 10);
  if (!isNaN(aNum)) {
    if (!isNaN(bNum)) {
      const aHf = (0 <= aNum && aNum <= 117) || (1000 <= aNum && aNum <= 1238);
      const bHf = (0 <= bNum && bNum <= 117) || (1000 <= bNum && bNum <= 1238);
      if (aHf !== bHf) {
        return aHf ? 1 : -1;
      } else {
        return a < b ? -1 : 1;
      }
    } else {
      return -1;
    }
  } else {
    if (!isNaN(bNum)) {
      return 1;
    } else {
      return a < b ? -1 : 1;
    }
  }
}
