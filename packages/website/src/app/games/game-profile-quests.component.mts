import {Component, Inject, LOCALE_ID} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {Observable} from "rxjs";
import {map as rxMap, switchMap as rxSwitchMap} from "rxjs/operators";

import {GameLocale, GameService} from "../../modules/api/game.service.mjs";
import {AuthService} from "../../modules/auth/auth.service.mjs";
import {GameModel} from "../../modules/models/game.mjs";
import {GameContent} from "../../modules/models/game-content.mjs";
import {computeAndSortQuests, QuestWithState, UserProfileStats} from "../../modules/models/quests.mjs";
import {GameProfile} from "./games-routing.module.mjs";

interface RouteData {
  profile: GameProfile;
}

interface ViewData {
  game: GameModel;
  quests: readonly QuestWithState[];
  profile: UserProfileStats;
}

@Component({
  selector: "ef-profile",
  templateUrl: "./game-profile-quests.component.html",
  styles: [`
    ef-quest { margin-bottom: 5px; }

    .no-quests {
      font-style: italic;
      margin: 7px;
    }
  `],
  standalone: false,
})
export class GameProfileQuestsComponent {
  readonly #localeId: string;
  readonly #route: ActivatedRoute;

  public data$: Observable<ViewData>;
  public isAuthenticated$: Observable<boolean>;

  // public profile: UserProfileStats = EMPTY_USER_PROFILE;

  constructor(
    @Inject(LOCALE_ID) localeId: string,
      authentication: AuthService,
      route: ActivatedRoute,
      game: GameService,
  ) {
    this.#localeId = localeId;
    this.#route = route;

    const routeData$ = route.data as Observable<RouteData>;
    this.data$ = routeData$.pipe(
      rxSwitchMap((data: RouteData): Observable<ViewData> => {
        const gameModel = new GameModel(data.profile.game, this.#localeId);
        const userItems = data.profile.items;
        return game
          .getGameLocale(data.profile.game.channels.active.build, localeId)
          .pipe(rxMap((locale: GameLocale): ViewData => {
            const content = new GameContent(gameModel.hasQuests, data.profile.game.channels.active.build, locale, localeId);
            const quests = computeAndSortQuests(data.profile, null, content.quests);
            const profile = { items: data.profile.items };
            return {game: gameModel, quests, profile};
          }));
      })
    );

    this.isAuthenticated$ = authentication.auth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }
}
