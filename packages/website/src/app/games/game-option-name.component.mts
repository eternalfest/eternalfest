import { Component, Input } from "@angular/core";

@Component({
  selector: "ef-game-option-name",
  templateUrl: "./game-option-name.component.html",
  styleUrls: ["./game-option-name.component.scss"],
  standalone: false,
})
export class GameOptionNameComponent {
  @Input("option")
    option!: string;
}
