import {Component, Inject, LOCALE_ID} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {Leaderboard} from "@eternalfest/core/leaderboard/leaderboard";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {AuthService} from "../../modules/auth/auth.service.mjs";
import {GameModel} from "../../modules/models/game.mjs";
import {GameLeaderboard} from "./games-routing.module.mjs";

interface RouteData {
  leaderboard: GameLeaderboard;
}

interface ViewData {
  leaderboard: Leaderboard;
  game: GameModel;
}

@Component({
  selector: "ef-leaderboard",
  templateUrl: "./game-leaderboard.component.html",
  styleUrls: ["./game-leaderboard.component.scss"],
  standalone: false,
})
export class GameLeaderboardComponent {
  public data$: Observable<ViewData>;
  public isAuthenticated$: Observable<boolean>;

  constructor(@Inject(LOCALE_ID) localeId: string, authentication: AuthService, route: ActivatedRoute) {
    const routeData$ = route.data as Observable<RouteData>;
    this.data$ = routeData$.pipe(rxMap((data: RouteData): ViewData => {
      const leaderboard: Leaderboard = data.leaderboard.leaderboard;
      const game = new GameModel(data.leaderboard.game, localeId);
      return {leaderboard, game};
    }));
    this.isAuthenticated$ = authentication.auth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }
}
