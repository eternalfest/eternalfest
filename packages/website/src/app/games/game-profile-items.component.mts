import {Component, Inject, LOCALE_ID} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {RunItems} from "@eternalfest/core/run/run-items";
import {LocaleId} from "@eternalfest/core/types/locale-id";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {Observable} from "rxjs";
import {map as rxMap, switchMap as rxSwitchMap} from "rxjs/operators";

import {GameLocale, GameService} from "../../modules/api/game.service.mjs";
import {AuthService} from "../../modules/auth/auth.service.mjs";
import {GameModel} from "../../modules/models/game.mjs";
import {GameContent, Item} from "../../modules/models/game-content.mjs";
import {GameProfile} from "./games-routing.module.mjs";

interface RouteData {
  profile: GameProfile;
}

interface ViewData {
  game: GameModel;
  userItems: RunItems;
  locale: GameLocale;
  content: GameContent;
}

@Component({
  selector: "ef-profile",
  templateUrl: "./game-profile-items.component.html",
  standalone: false,
})
export class GameProfileItemsComponent {
  readonly #localeId: string;
  readonly #authentication: AuthService;
  readonly #route: ActivatedRoute;

  public data$: Observable<ViewData>;
  public isAuthenticated$: Observable<boolean>;

  constructor(
    @Inject(LOCALE_ID) localeId: LocaleId,
      authentication: AuthService,
      game: GameService,
      route: ActivatedRoute,
  ) {
    this.#localeId = localeId;
    this.#authentication = authentication;
    this.#route = route;

    const routeData$ = route.data as Observable<RouteData>;
    this.data$ = routeData$.pipe(
      rxSwitchMap((data: RouteData): Observable<ViewData> => {
        const gameModel = new GameModel(data.profile.game, this.#localeId);
        const userItems = data.profile.items;
        return game
          .getGameLocale(data.profile.game.channels.active.build, localeId)
          .pipe(rxMap((locale: GameLocale): ViewData => {
            const content = new GameContent(gameModel.hasQuests, data.profile.game.channels.active.build, locale, localeId);
            return {game: gameModel, userItems, locale, content};
          }));
      })
    );

    this.isAuthenticated$ = authentication.auth().pipe(
      rxMap((auth) => auth.type === ActorType.User),
    );
  }
}
