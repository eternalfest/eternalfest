import {ComponentRef, DebugElement} from "@angular/core";
import {ComponentFixture, TestBed,} from "@angular/core/testing";
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from "@angular/platform-browser-dynamic/testing";

import {GameModeNameComponent} from "./game-mode-name.component.mjs";

describe("GameMondeNameComponent", () => {
  it("should create the app", async () => {
    const testBed = new TestBed();
    testBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting(), {
      errorOnUnknownElements: true,
      errorOnUnknownProperties: true
    });
    const fixture: ComponentFixture<GameModeNameComponent> = testBed.createComponent(GameModeNameComponent);
    const componentRef: ComponentRef<GameModeNameComponent> = fixture.componentRef;
    const de: DebugElement = fixture.debugElement;
    componentRef.setInput("mode", "Aventure");
    fixture.detectChanges(false);
    await fixture.whenRenderingDone();
    expect(de.nativeElement.textContent).toEqual("Aventure");
  });
});
