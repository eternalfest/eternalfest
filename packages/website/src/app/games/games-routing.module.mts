import {Injectable, NgModule, NgZone} from "@angular/core";
import {ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes} from "@angular/router";
import {Game} from "@eternalfest/core/game2/game";
import {GameModeKey} from "@eternalfest/core/game2/game-mode-key";
import {ShortGameListing} from "@eternalfest/core/game2/short-game-listing";
import {Leaderboard} from "@eternalfest/core/leaderboard/leaderboard";
import {RunItems} from "@eternalfest/core/run/run-items";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {UserId} from "@eternaltwin/core/user/user-id";
import {firstValueFrom, Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

import {GameService} from "../../modules/api/game.service.mjs";
import {UserService} from "../../modules/api/user.service.mjs";
import {AuthService} from "../../modules/auth/auth.service.mjs";
import {GameLeaderboardComponent} from "./game-leaderboard.component.mjs";
import {GameListComponent} from "./game-list.component.mjs";
import {GameProfileItemsComponent} from "./game-profile-items.component.mjs";
import {GameProfileQuestsComponent} from "./game-profile-quests.component.mjs";
import {GameViewComponent} from "./game-view.component.mjs";

@Injectable()
export class GameResolverService implements Resolve<Game> {
  private readonly game: GameService;
  private readonly ngZone: NgZone;
  private readonly router: Router;

  constructor(game: GameService, ngZone: NgZone, router: Router) {
    this.game = game;
    this.ngZone = ngZone;
    this.router = router;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<Game | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId !== null) {
      const game: Game | null = await firstValueFrom(this.game.getGameByRef(gameId));
      if (game !== null) {
        return game;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games"));
    return undefined as never;
  }
}

export interface GameProfile {
  game: Game;
  items: RunItems;
}

@Injectable()
export class GameProfileResolverService implements Resolve<GameProfile> {
  readonly #authentication: AuthService;
  readonly #game: GameService;
  readonly #user: UserService;
  readonly #ngZone: NgZone;
  readonly #router: Router;

  constructor(authentication: AuthService, game: GameService, user: UserService, ngZone: NgZone, router: Router) {
    this.#authentication = authentication;
    this.#game = game;
    this.#user = user;
    this.#ngZone = ngZone;
    this.#router = router;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<GameProfile | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId === null) {
      await this.#ngZone.run(() => this.#router.navigateByUrl("/games"));
      return undefined as never;
    }
    const userId$: Observable<UserId | null> = this.#authentication.auth().pipe(
      rxMap((auth) => auth.type === ActorType.User ? auth.user.id : null),
    );
    const userP: Promise<UserId | null> = firstValueFrom(userId$);
    const gameP: Promise<Game | null> = firstValueFrom(this.#game.getGameByRef(gameId));
    const [user, game] = await Promise.all([userP, gameP]);
    if (user === null || game === null) {
      await this.#ngZone.run(() => this.#router.navigateByUrl("/games"));
      return undefined as never;
    }
    const items = await firstValueFrom(this.#user.getProfile(user, gameId));
    if (items === null) {
      await this.#ngZone.run(() => this.#router.navigateByUrl("/games"));
      return undefined as never;
    }
    return {game, items};
  }
}

@Injectable()
export class GamesResolverService implements Resolve<ShortGameListing> {
  readonly #game: GameService;
  readonly #router: Router;

  constructor(router: Router, game: GameService) {
    this.#router = router;
    this.#game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<ShortGameListing> {
    const favorite = route.queryParamMap.get("favorite");
    const pageStr = route.queryParamMap.get("p");
    const page: number = pageStr !== null ? parseInt(pageStr, 10) : 1;
    return firstValueFrom(this.#game.getGames(Math.max(0, page - 1), favorite === "true"));
  }
}

export interface GameLeaderboard {
  game: Game;
  leaderboard: Leaderboard;
}

@Injectable()
export class GameLeaderboardResolverService implements Resolve<GameLeaderboard> {
  private readonly game: GameService;
  private readonly ngZone: NgZone;
  private readonly router: Router;

  constructor(game: GameService, ngZone: NgZone, router: Router) {
    this.ngZone = ngZone;
    this.router = router;
    this.game = game;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<GameLeaderboard | never> {
    const gameId: string | null = route.paramMap.get("game_id");
    if (gameId !== null) {
      const game: Game | null = await firstValueFrom(this.game.getGameByRef(gameId));
      if (game !== null) {
        // TODO: How to choose a different mode?
        let gameMode: GameModeKey = "solo";

        // If a game doesn't have a `solo` mode, pick the first existing mode.
        if (!game.channels.active.build.modes.has(gameMode)) {
          const firstMode = game.channels.active.build.modes.keys().next().value;
          if (typeof firstMode === "string") {
            gameMode ??= firstMode;
          }
        }

        const leaderboard: Leaderboard | null = await firstValueFrom(this.game.getLeaderboardByGameId(gameId, gameMode));
        if (leaderboard !== null) {
          return { game, leaderboard };
        }
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/games/" + gameId + "/leaderboard"));
    return undefined as never;
  }
}

const routes: Routes = [
  {
    path: "",
    component: GameListComponent,
    pathMatch: "full",
    runGuardsAndResolvers: "paramsOrQueryParamsChange",
    resolve: {
      games: GamesResolverService,
    },
  },
  {
    path: ":game_id",
    component: GameViewComponent,
    pathMatch: "full",
    resolve: {
      game: GameResolverService,
    },
  },
  {
    path: ":game_id/leaderboard",
    component: GameLeaderboardComponent,
    pathMatch: "full",
    resolve: {
      leaderboard: GameLeaderboardResolverService,
    },
  },
  {
    path: ":game_id/profile/items",
    component: GameProfileItemsComponent,
    pathMatch: "full",
    resolve: {
      profile: GameProfileResolverService,
    },
  },
  {
    path: ":game_id/profile/quests",
    component: GameProfileQuestsComponent,
    pathMatch: "full",
    resolve: {
      profile: GameProfileResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [GameResolverService, GamesResolverService, GameProfileResolverService, GameLeaderboardResolverService],
})
export class GamesRoutingModule {
}
