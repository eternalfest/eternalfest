import {Pipe, PipeTransform} from "@angular/core";
import {NullableBlob} from "@eternalfest/core/blob/blob";

const DEFAULT_ICON_URI: string = "/assets/accounts/ef.x64.png";

@Pipe({
  name: "gameIconUri",
  pure: true,
  standalone: false,
})
export class GameIconUriPipe implements PipeTransform {
  transform(iconBlob: NullableBlob): string {
    if (iconBlob === null || iconBlob === undefined) {
      return DEFAULT_ICON_URI;
    }
    return `/api/v1/blobs/${iconBlob.id}/raw`;
  }
}
