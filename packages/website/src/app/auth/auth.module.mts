import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FormsModule } from "@angular/forms";

import { HelpersModule } from "../../modules/helpers/helpers.module.mjs";
import { AuthRoutingModule } from "./auth-routing.module.mjs";
import { LoginComponent } from "./login.component.mjs";

@NgModule({
  declarations: [LoginComponent],
  imports: [
    HelpersModule,
    CommonModule,
    FormsModule,
    AuthRoutingModule,
  ],
})
export class AuthModule {
}
