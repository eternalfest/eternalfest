import { Component, HostBinding } from "@angular/core";
import {AuthContext} from "@eternalfest/core/auth/auth-context";
import { ActorType } from "@eternalfest/core/types/user/actor-type";
import {UserId} from "@eternaltwin/core/user/user-id";
import { $Boolean } from "kryo/boolean";
import { Observable } from "rxjs";
import { map as rxMap } from "rxjs/operators";

import {AuthService} from "../modules/auth/auth.service.mjs";
import { LocalStorageKey, LocalStorageService } from "../modules/storage/local-storage.service.mjs";

const DISABLE_SNOW_KEY: LocalStorageKey<boolean> = new LocalStorageKey("app/snow", $Boolean);

@Component({
  selector: "ef-app",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
  standalone: false,
})
export class AppComponent {
  public readonly localStorageService: LocalStorageService;
  public isAuthenticated$: Observable<boolean>;
  public userId$: Observable<UserId | undefined>;
  public userDisplayName$: Observable<string | undefined>;
  public title = "Eternalfest";

  @HostBinding("class.theme-goodbye")
  public isGoodbye: boolean = false;

  @HostBinding("class.theme-xmas")
  public isXmas: boolean = false;

  @HostBinding("class.theme-xmas-snow")
  public isXmasSnow: boolean;

  constructor(
    authentication: AuthService,
    localStorageService: LocalStorageService,
  ) {
    this.localStorageService = localStorageService;
    const storedDisableSnow: boolean | null = localStorageService.read(DISABLE_SNOW_KEY);
    this.isXmasSnow = storedDisableSnow !== null ? !storedDisableSnow : true;

    this.isAuthenticated$ = authentication.auth().pipe(
      rxMap((auth: AuthContext) => auth.type === ActorType.User),
    );
    this.userId$ = authentication.auth().pipe(
      rxMap((auth: AuthContext) => auth.type === ActorType.User ? auth.user.id : undefined),
    );
    this.userDisplayName$ = authentication.auth().pipe(
      rxMap((auth: AuthContext) => auth.type === ActorType.User ? auth.user.displayName : undefined),
    );
  }

  public toggleSnow(): void {
    this.isXmasSnow = !this.isXmasSnow;
    this.localStorageService.store(DISABLE_SNOW_KEY, !this.isXmasSnow);
  }
}
