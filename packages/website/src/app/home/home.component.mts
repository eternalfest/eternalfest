import { Component } from "@angular/core";

@Component({
  selector: "home-view",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"],
  standalone: false,
})
export class HomeView {
}
