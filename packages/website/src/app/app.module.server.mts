import {XhrFactory} from "@angular/common";
import {provideHttpClient, withFetch} from "@angular/common/http";
import {NgModule} from "@angular/core";
import {provideClientHydration} from "@angular/platform-browser";
import {provideServerRendering, ServerModule} from "@angular/platform-server";
import {provideRouter} from "@angular/router";
import xhr2 from "xhr2";

import { ApiModule } from "../modules/api/api.module.mjs";
import { ServerAuthModule } from "../modules/auth/auth.module.server.mjs";
import {ServerRestModule} from "../modules/rest/rest.module.server.mjs";
import { AppComponent } from "./app.component.mjs";
import { AppModule } from "./app.module.mjs";
import {routes} from "./app-routing.module.mjs";

/**
 * Workaround for https://github.com/angular/angular/issues/15730
 *
 * (allow sending cookies)
 */
class ServerXhr implements XhrFactory {
  build(): XMLHttpRequest {
    const xhr = new xhr2.XMLHttpRequest();
    xhr._restrictedHeaders.cookie = false;
    return xhr;
  }
}

@NgModule({
  imports: [
    AppModule,
    ApiModule,
    ServerAuthModule,
    ServerRestModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {provide: XhrFactory, useClass: ServerXhr, deps: []},
    provideHttpClient(withFetch()),
    provideServerRendering(),
    provideRouter(routes),
    provideClientHydration()
  ],
})
export class AppServerModule {
}
