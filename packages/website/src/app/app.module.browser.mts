import {APP_BASE_HREF} from "@angular/common";
import {provideHttpClient} from "@angular/common/http";
import {NgModule} from "@angular/core";

import {ApiModule} from "../modules/api/api.module.mjs";
import {BrowserAuthModule} from "../modules/auth/auth.module.browser.mjs";
import {BrowserRestModule} from "../modules/rest/rest.module.browser.mjs";
import {AppComponent} from "./app.component.mjs";
import {AppModule} from "./app.module.mjs";

@NgModule({
  imports: [
    AppModule,
    ApiModule,
    BrowserAuthModule,
    BrowserRestModule,
  ],
  providers: [
    {provide: APP_BASE_HREF, useValue: "/"},
    provideHttpClient(),
  ],
  bootstrap: [AppComponent],
})
export class AppBrowserModule {
}
