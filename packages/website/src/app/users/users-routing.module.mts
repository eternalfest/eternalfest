import { Injectable, NgModule, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { User } from "@eternalfest/core/user/user";
import {firstValueFrom} from "rxjs";

import { UserService } from "../../modules/api/user.service.mjs";
import { UserComponent } from "./user.component.mjs";

@Injectable()
export class UserResolverService implements Resolve<User> {
  private readonly ngZone: NgZone;
  private readonly router: Router;
  private readonly user: UserService;

  constructor(ngZone: NgZone, router: Router, user: UserService) {
    this.ngZone = ngZone;
    this.router = router;
    this.user = user;
  }

  async resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<User | never> {
    const userId: string | null = route.paramMap.get("user_id");
    if (userId !== null) {
      const user: User | null = await firstValueFrom(this.user.getUserById(userId));
      if (user !== null) {
        return user;
      }
    }

    await this.ngZone.run(() => this.router.navigateByUrl("/"));
    return undefined as never;
  }
}

const routes: Routes = [
  {
    path: ":user_id",
    component: UserComponent,
    pathMatch: "full",
    resolve: {
      user: UserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [UserResolverService],
})
export class UsersRoutingModule {
}
