import { Component, Input, OnInit } from "@angular/core";

import { HfestItemsDbService } from "../../modules/hfest-db/hfest-items-db.service.mjs";

@Component({
  selector: "ef-user-item",
  templateUrl: "./item.component.html",
  styleUrls: ["./item.component.scss"],
  standalone: false,
})
export class ItemComponent implements OnInit {
  @Input("id")
    itemId!: number;
  @Input("count")
    count!: number;
  @Input("max-count")
    maxCount!: number;

  isLocked: boolean;
  isUnknown: boolean;

  private hfestItemsDbService: HfestItemsDbService;

  constructor(hfestItemsDbService: HfestItemsDbService) {
    this.hfestItemsDbService = hfestItemsDbService;
    this.isUnknown = true;
    this.isLocked = true;
  }

  ngOnInit(): void {
    if (this.count === 0) {
      this.isLocked = false;
      this.isUnknown = true;
    } else {
      this.isLocked = this.count < this.hfestItemsDbService.getItemUnlock(this.itemId);
      this.isUnknown = false;
    }
  }
}
