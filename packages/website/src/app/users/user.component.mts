import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { IdentityType } from "@eternalfest/core/types/user/identity-type";
import { User } from "@eternalfest/core/user/user";
import {Observable} from "rxjs";

interface ViewData {
  user: User;
}

@Component({
  selector: "ef-user",
  templateUrl: "./user.component.html",
  styleUrls: ["./user.component.scss"],
  standalone: false,
})
export class UserComponent {
  data$: Observable<ViewData>;
  IdentityType: typeof IdentityType = IdentityType;

  constructor(activatedRoute: ActivatedRoute) {
    this.data$ = activatedRoute.data as Observable<ViewData>;
  }
}
