import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { HelpersModule } from "../../modules/helpers/helpers.module.mjs";
import { HfestDbModule } from "../../modules/hfest-db/hfest-db.module.mjs";
import { HfestIdentityComponent } from "./hfest-identity.component.mjs";
import { ItemComponent } from "./item.component.mjs";
import { UserComponent } from "./user.component.mjs";
import { UsersRoutingModule } from "./users-routing.module.mjs";

@NgModule({
  declarations: [HfestIdentityComponent, ItemComponent, UserComponent],
  imports: [
    CommonModule,
    HelpersModule,
    HfestDbModule,
    UsersRoutingModule,
  ],
})
export class UsersModule {
}
