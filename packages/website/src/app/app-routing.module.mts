import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { AuthGuard } from "../modules/auth/auth-guard.service.mjs";
import { HomeView } from "./home/home.component.mjs";

export const routes: Routes = [
  {path: "", component: HomeView, pathMatch: "full"},
  {path: "login", loadChildren: () => import("./auth/auth.module.mjs").then((m) => m.AuthModule)},
  {path: "games", loadChildren: () => import("./games/games.module.mjs").then((m) => m.GamesModule)},
  {path: "runs", loadChildren: () => import("./runs/runs.module.mjs").then((m) => m.RunsModule)},
  {
    path: "settings",
    canActivate: [AuthGuard],
    loadChildren: () => import("./settings/settings.module.mjs").then((m) => m.SettingsModule),
  },
  {path: "users", loadChildren: () => import("./users/users.module.mjs").then((m) => m.UsersModule)},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
