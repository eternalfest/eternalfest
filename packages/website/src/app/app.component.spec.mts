import {provideHttpClient} from "@angular/common/http";
import {HttpTestingController, provideHttpClientTesting} from "@angular/common/http/testing";
import {ComponentRef, DebugElement} from "@angular/core";
import {ComponentFixture, TestBed,} from "@angular/core/testing";
import {By} from "@angular/platform-browser";
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from "@angular/platform-browser-dynamic/testing";
import {$AuthContext, AuthContext} from "@eternalfest/core/auth/auth-context";
import {AuthScope} from "@eternalfest/core/auth/auth-scope";
import {ObjectType} from "@eternalfest/core/types/object-type";
import {ActorType} from "@eternalfest/core/types/user/actor-type";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import {Observable} from "rxjs";
import {LeakDetector} from "rxjs-leak-detector";

import {ApiModule} from "../modules/api/api.module.mjs";
import {BrowserAuthModule} from "../modules/auth/auth.module.browser.mjs";
import {BrowserRestModule} from "../modules/rest/rest.module.browser.mjs";
import {AppComponent} from "./app.component.mjs";
import {AppModule} from "./app.module.mjs";

describe("AppComponent", () => {
  it("should create the app", async () => {
    const leakDetecter = new LeakDetector(Observable);
    using spy = leakDetecter.zoneSpy();
    await spy.run(async () => {
      const testBed = new TestBed();
      testBed.initTestEnvironment(BrowserDynamicTestingModule, platformBrowserDynamicTesting(), {
        errorOnUnknownElements: true,
        errorOnUnknownProperties: true,
        teardown: {
          rethrowErrors: true,
          destroyAfterEach: true,
        }
      });
      testBed.configureTestingModule({
        imports: [
          AppModule,
          ApiModule,
          BrowserAuthModule,
          BrowserRestModule,
        ],
        providers: [
          provideHttpClient(),
          provideHttpClientTesting(),
        ]
      });
      const fixture: ComponentFixture<AppComponent> = testBed.createComponent(AppComponent);
      const componentRef: ComponentRef<AppComponent> = fixture.componentRef;
      expect(componentRef).toBeDefined();
      const de: DebugElement = fixture.debugElement;
      const httpMock: HttpTestingController = de.injector.get(HttpTestingController);
      expect(httpMock).toBeDefined();
      const req = httpMock.expectOne((r) => {
        const url = new URL(r.url);
        return url.pathname === "/api/v1/auth/self";
      });
      req.flush($AuthContext.write(JSON_VALUE_WRITER, {
        type: ActorType.User,
        user: {type: ObjectType.User, id: "9f310484-963b-446b-af69-797feec6813f", displayName: "Alice"},
        scope: AuthScope.Default,
        isTester: true,
        isAdministrator: true
      } satisfies AuthContext));
      fixture.detectChanges(false);
      await fixture.whenStable();
      httpMock.expectNone(() => true);
      const userLink: DebugElement = de.query(By.css(".main-bar a"));
      expect(userLink.nativeElement.textContent).toEqual("Alice");
      testBed.resetTestEnvironment();
    });
    leakDetecter.snapshot().assertEmptyOrPrintReportToConsole();
  });
});
