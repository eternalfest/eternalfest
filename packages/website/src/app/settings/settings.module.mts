import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";

import { HelpersModule } from "../../modules/helpers/helpers.module.mjs";
import { SettingsComponent } from "./settings.component.mjs";
import { SettingsRoutingModule } from "./settings-routing.module.mjs";

@NgModule({
  declarations: [
    SettingsComponent,
  ],
  imports: [
    CommonModule,
    HelpersModule,
    SettingsRoutingModule,
  ],
})
export class SettingsModule {
}
