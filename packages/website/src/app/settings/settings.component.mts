import {Component} from "@angular/core";
import {ActivatedRoute} from "@angular/router";
import {User} from "@eternalfest/core/user/user";
import {Observable} from "rxjs";
import {map as rxMap} from "rxjs/operators";

interface RouteData {
  user: User;
}

@Component({
  selector: "ef-settings",
  templateUrl: "./settings.component.html",
  styleUrls: ["./settings.component.scss"],
  standalone: false,
})
export class SettingsComponent {
  public user$: Observable<User>;

  constructor(route: ActivatedRoute) {
    const routeData$ = route.data as Observable<RouteData>;
    this.user$ = routeData$.pipe(rxMap((data: RouteData): User => data.user));
  }
}
