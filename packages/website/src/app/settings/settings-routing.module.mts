import { Injectable, NgModule, NgZone } from "@angular/core";
import { ActivatedRouteSnapshot, Resolve, Router, RouterModule, RouterStateSnapshot, Routes } from "@angular/router";
import { AuthContext } from "@eternalfest/core/auth/auth-context";
import { ActorType } from "@eternalfest/core/types/user/actor-type";
import { User } from "@eternalfest/core/user/user";
import { EMPTY as RX_EMPTY, from as rxFrom, Observable, of as rxOf } from "rxjs";
import { first as rxFirst, flatMap as rxFlatMap } from "rxjs/operators";

import { UserService } from "../../modules/api/user.service.mjs";
import { AuthService } from "../../modules/auth/auth.service.mjs";
import { SettingsComponent } from "./settings.component.mjs";

@Injectable()
export class SelfUserResolverService implements Resolve<User> {
  private readonly auth: AuthService;
  private readonly ngZone: NgZone;
  private readonly router: Router;
  private readonly user: UserService;

  constructor(auth: AuthService, ngZone: NgZone, router: Router, user: UserService) {
    this.auth = auth;
    this.ngZone = ngZone;
    this.router = router;
    this.user = user;
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<User | never> {
    return this.auth.auth().pipe(
      rxFirst(),
      rxFlatMap((auth: AuthContext): Observable<User | null> => {
        if (auth.type === ActorType.User) {
          return rxFrom(this.user.getUserById(auth.user.id));
        } else {
          return rxOf(null);
        }
      }),
      rxFlatMap((user: User | null): Observable<User | never> => {
        if (user !== null) {
          return rxOf(user);
        } else {
          return rxFrom(this.ngZone.run(() => this.router.navigateByUrl("/")))
            .pipe(rxFlatMap(() => RX_EMPTY));
        }
      }),
    );
  }
}

const routes: Routes = [
  {
    path: "",
    component: SettingsComponent,
    pathMatch: "full",
    resolve: {
      user: SelfUserResolverService,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [SelfUserResolverService],
})
export class SettingsRoutingModule {
}
