import {CommonModule} from "@angular/common";
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";

import {ConsoleModule} from "../modules/console/console.module.mjs";
import {HfestDbModule} from "../modules/hfest-db/hfest-db.module.mjs";
import {AppComponent} from "./app.component.mjs";
import {AppRoutingModule} from "./app-routing.module.mjs";
import {HomeView} from "./home/home.component.mjs";

@NgModule({
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    ConsoleModule,
    HfestDbModule,
  ],
  declarations: [AppComponent, HomeView],
  exports: [AppComponent],
})
export class AppModule {
}
