/**
 * List of routes corresponding to pages (not assets).
 */
export const ROUTES: string[] = [
  "/",
  "/games",
  "/games/:game_id",
  "/games/:game_id/leaderboard",
  "/games/:game_id/profile/items",
  "/games/:game_id/profile/quests",
  "/login",
  "/runs/:run_id",
  "/settings",
  "/users/:user_id",
];
