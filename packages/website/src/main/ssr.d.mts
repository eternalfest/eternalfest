declare module "#ssr-en-us" {
  import {InjectionToken, Type} from "@angular/core";
  import {Config} from "@eternaltwin/core/config/config";
  import type {Request} from "koa";

  export const CommonEngine: any;
  export const AppServerModule: Type<{}>;
  export const SERVER_DIR: URL;
  export const SERVER_INDEX: URL;
  export const BROWSER_DIR: URL;
  export const APP_BASE_HREF: InjectionToken<string>;
  export const REQUEST: InjectionToken<Request>;
  export const BACKEND_URI: InjectionToken<string>;
  export const INTERNAL_AUTH_KEY: InjectionToken<string>;
  export const ROUTES: readonly string[];

  export function isDevMode(): boolean;
  export function enableProdMode(): void;
}

declare module "#ssr-de-de" {
  export * from "#ssr-en-us";
}

declare module "#ssr-fr-fr" {
  export * from "#ssr-en-us";
}

declare module "#ssr-es-sp" {
  export * from "#ssr-en-us";
}
