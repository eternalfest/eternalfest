/**
 * Moves `./app/browser/<name>/index.html` to `./app/server/<name>/index.html`
 */

import fs from "fs";
import furi from "furi";
import sysPath from "path";

const PROJECT_ROOT = sysPath.join(furi.toSysPath(import.meta.url), "..", "..");
const BROWSER_DIR = sysPath.join(PROJECT_ROOT, "app", "browser");
const SERVER_DIR = sysPath.join(PROJECT_ROOT, "app", "server");

for (const dirEnt of fs.readdirSync(SERVER_DIR, {withFileTypes: true})) {
  if (!dirEnt.isDirectory()) {
    continue;
  }
  const browserIndex = sysPath.join(BROWSER_DIR, dirEnt.name, "index.html");
  const serverIndex = sysPath.join(SERVER_DIR, dirEnt.name, "index.html");

  let browserStats = null;
  try {
    browserStats = fs.statSync(browserIndex);
  } catch (e) {
    if (e.code !== "ENOENT") {
      throw e;
    }
  }

  let serverStats = null;
  try {
    serverStats = fs.statSync(serverIndex);
  } catch (e) {
    if (e.code !== "ENOENT") {
      throw e;
    }
  }

  let needsCopy = false;
  if (serverStats === null) {
    if (browserStats === null) {
      throw new Error(`index file missing at both ${browserIndex} and ${serverIndex}`);
    }
    needsCopy = true;
  } else if (browserStats !== null && serverStats.mtime < browserStats.mtime) {
    needsCopy = true;
  }

  if (needsCopy) {
    fs.copyFileSync(browserIndex, serverIndex);
    fs.unlinkSync(browserIndex);
  } else {
    console.warn("skipping copy: server index exists and is recent enough")
  }
}
