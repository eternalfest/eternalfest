# API Core

[![build status](https://gitlab.com/eternalfest/core/badges/master/build.svg)](https://gitlab.com/eternalfest/core/commits/master)

Core the Eternalfest API

## Changelog

See [CHANGELOG.md](./CHANGELOG.md).

## License

[MIT License © 2018 Eternalfest](./LICENSE.md)
