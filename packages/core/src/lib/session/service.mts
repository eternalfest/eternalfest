import { UuidHex } from "kryo/uuid-hex";

import { AuthContext } from "../auth/auth-context.mjs";
import { Session } from "../types/session.mjs";

export interface SessionService {
  createSession(authContext: AuthContext, userId: UuidHex): Promise<Session>;

  getSessionById(authContext: AuthContext, sessionId: UuidHex): Promise<Session | undefined>;

  getAndTouchSessionById(authContext: AuthContext, sessionId: UuidHex): Promise<Session | undefined>;

  deleteSessionById(authContext: AuthContext, sessionId: UuidHex): Promise<void>;
}
