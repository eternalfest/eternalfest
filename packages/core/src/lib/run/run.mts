import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Date} from "kryo/date";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$GameBuild, GameBuild} from "../game2/game-build.mjs";
import {$GameChannelRef, GameChannelKeyRef} from "../game2/game-channel-ref.mjs";
import {$GameModeKey, GameModeKey} from "../game2/game-mode-key.mjs";
import {$GameOptionKey, GameOptionKey} from "../game2/game-option-key.mjs";
import {$GameIdRef, GameIdRef} from "../game2/game-ref.mjs";
import {$NullableDate, NullableDate} from "../types/nullable-date.mjs";
import {$ShortUser, ShortUser} from "../user/short-user.mjs";
import {$RunResult, RunResult} from "./run-result.mjs";
import {$RunSettings, RunSettings} from "./run-settings.mjs";

export interface Run {
  id: UuidHex;
  createdAt: Date;
  startedAt: NullableDate;
  result: null | RunResult;
  game: GameIdRef;
  channel: GameChannelKeyRef;
  build: GameBuild;
  user: ShortUser;
  gameMode: GameModeKey;
  gameOptions: ReadonlyArray<GameOptionKey>;
  settings: RunSettings;
}

export const $Run: RecordIoType<Run> = new RecordType<Run>({
  properties: {
    id: {type: $UuidHex},
    createdAt: {type: $Date},
    startedAt: {type: $NullableDate},
    result: {type: new TryUnionType({variants: [$Null, $RunResult]})},
    game: {type: $GameIdRef},
    channel: {type: $GameChannelRef},
    build: {type: $GameBuild},
    user: {type: $ShortUser},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 20})},
    settings: {type: $RunSettings},
  },
  changeCase: CaseStyle.SnakeCase,
});
