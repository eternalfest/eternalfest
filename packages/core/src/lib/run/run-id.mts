import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type RunId = UuidHex;

export const $RunId: Ucs2StringType = $UuidHex;
