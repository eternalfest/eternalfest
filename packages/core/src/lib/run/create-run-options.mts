import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$GameChannelKey, GameChannelKey} from "../game2/game-channel-key.mjs";
import {$GameModeKey, GameModeKey} from "../game2/game-mode-key.mjs";
import {$GameOptionKey, GameOptionKey} from "../game2/game-option-key.mjs";
import {$GameIdRef, GameIdRef} from "../game2/game-ref.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$UserIdRef, UserIdRef} from "../user/user-id-ref.mjs";
import {$RunSettings, RunSettings} from "./run-settings.mjs";

export interface CreateRunOptions {
  readonly game: GameIdRef;
  readonly channel: GameChannelKey;
  readonly version: VersionString;
  readonly user: UserIdRef;
  readonly gameMode: GameModeKey;
  readonly gameOptions: ReadonlyArray<GameOptionKey>;
  readonly settings: RunSettings;
}

export const $CreateRunOptions: RecordIoType<CreateRunOptions> = new RecordType<CreateRunOptions>({
  properties: {
    game: {type: $GameIdRef},
    channel: {type: $GameChannelKey},
    version: {type: $VersionString},
    user: {type: $UserIdRef},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 20})},
    settings: {type: $RunSettings},
  },
  changeCase: CaseStyle.SnakeCase,
});
