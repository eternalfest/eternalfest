import { $Uint32 } from "kryo/integer";
import { MapType } from "kryo/map";

import { $HfestItemId, HfestItemId } from "../hfest-api/hfest-item-id.mjs";

export type RunItems = ReadonlyMap<HfestItemId, number>;

export const $RunItems: MapType<HfestItemId, number> = new MapType({
  keyType: $HfestItemId,
  valueType: $Uint32,
  maxSize: 500,
  assumeStringKey: true,
});
