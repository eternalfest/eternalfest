import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {IntegerType} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

import {$LocaleId, LocaleId} from "../types/locale-id.mjs";

export interface RunSettings {
  readonly detail: boolean;
  readonly shake: boolean;
  readonly sound: boolean;
  readonly music: boolean;
  readonly volume: number;
  readonly locale: LocaleId;
}

export const $RunSettings: RecordIoType<RunSettings> = new RecordType<RunSettings>({
  properties: {
    detail: {type: $Boolean},
    shake: {type: $Boolean},
    sound: {type: $Boolean},
    music: {type: $Boolean},
    volume: {type: new IntegerType({min: 0, max: 100})},
    locale: {type: $LocaleId},
  },
  changeCase: CaseStyle.SnakeCase,
});
