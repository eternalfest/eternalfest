import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $FamiliesString, FamiliesString } from "../game2/families-string.mjs";
import { $IdRef, IdRef } from "../types/id-ref.mjs";
import { $RunItems, RunItems } from "./run-items.mjs";

export interface RunStart {
  readonly run: IdRef;
  readonly key: UuidHex;
  readonly families: FamiliesString;
  readonly items: RunItems;
}

export const $RunStart: RecordIoType<RunStart> = new RecordType<RunStart>({
  properties: {
    run: {type: $IdRef},
    key: {type: $UuidHex},
    families: {type: $FamiliesString},
    items: {type: $RunItems},
  },
  changeCase: CaseStyle.SnakeCase,
});
