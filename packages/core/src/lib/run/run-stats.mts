import { AnyType } from "kryo/any";

export type RunStats = unknown;

export const $RunStats: AnyType = new AnyType();
