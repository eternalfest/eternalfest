import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/boolean";
import { $Date } from "kryo/date";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

import { $RunItems, RunItems } from "./run-items.mjs";
import { $RunScores, RunScores } from "./run-scores.mjs";
import { $RunStats, RunStats } from "./run-stats.mjs";

export interface RunResult {
  readonly createdAt: Date;
  readonly isVictory: boolean;
  readonly maxLevel: number;
  readonly scores: RunScores;
  readonly items: RunItems;
  readonly stats: RunStats;
}

export const $RunResult: RecordIoType<RunResult> = new RecordType<RunResult>({
  properties: {
    createdAt: {type: $Date},
    isVictory: {type: $Boolean},
    maxLevel: {type: $Uint32},
    scores: {type: $RunScores},
    items: {type: $RunItems},
    stats: {type: $RunStats},
  },
  changeCase: CaseStyle.SnakeCase,
});
