import { ArrayIoType, ArrayType } from "kryo/array";
import { $Sint32 } from "kryo/integer";

export type RunScores = ReadonlyArray<number>;

export const $RunScores: ArrayIoType<number> = new ArrayType({
  itemType: $Sint32,
  minLength: 1,
  maxLength: 100,
});
