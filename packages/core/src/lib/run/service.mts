import {UuidHex} from "kryo/uuid-hex";

import {AuthContext} from "../auth/auth-context.mjs";
import {LeaderboardService} from "../leaderboard/service.mjs";
import {CreateRunOptions} from "./create-run-options.mjs";
import {Run} from "./run.mjs";
import {RunItems} from "./run-items.mjs";
import {RunStart} from "./run-start.mjs";
import {SetRunResultOptions} from "./set-run-result-options.mjs";

export interface RunService extends LeaderboardService {
  createRun(acx: AuthContext, options: CreateRunOptions): Promise<Run>;

  getRunById(acx: AuthContext, runId: UuidHex): Promise<Run>;

  startRun(acx: AuthContext, runId: UuidHex): Promise<RunStart>;

  setRunResult(acx: AuthContext, runId: UuidHex, result: SetRunResultOptions): Promise<Run>;

  getGameUserItemsById(auth: AuthContext, userId: UuidHex, gameId: UuidHex, until: Date | null): Promise<RunItems | undefined>;
}
