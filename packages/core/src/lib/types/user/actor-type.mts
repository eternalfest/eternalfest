import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

export enum ActorType {
  Guest,
  User,
  System,
}

export const $ActorType: TsEnumType<ActorType> = new TsEnumType<ActorType>({
  enum: ActorType,
  changeCase: CaseStyle.PascalCase,
});
