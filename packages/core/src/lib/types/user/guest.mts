import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $ActorType, ActorType } from "./actor-type.mjs";

/**
 * Represents an unauthenticated actor
 */
export interface Guest {
  type: ActorType.Guest;
}

export const $Guest: RecordIoType<Guest> = new RecordType<Guest>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.Guest as ActorType.Guest})},
  },
  changeCase: CaseStyle.SnakeCase,
});
