import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $AbstractIdentity, IdentityBase } from "./identity-base.mjs";
import { $IdentityType, IdentityType } from "./identity-type.mjs";

export interface EternalfestIdentity extends IdentityBase {
  type: IdentityType.Ef;
}

export const $EternalfestIdentity: RecordIoType<EternalfestIdentity> = new RecordType<EternalfestIdentity>({
  properties: {
    ...$AbstractIdentity.properties,
    type: {type: new LiteralType({type: $IdentityType, value: IdentityType.Ef as IdentityType.Ef})},
  },
  changeCase: CaseStyle.SnakeCase,
});
