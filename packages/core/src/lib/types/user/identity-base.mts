import { CaseStyle } from "kryo";
import { $Date } from "kryo/date";
import { MapType } from "kryo/map";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $HfestItemId, HfestItemId } from "../../hfest-api/hfest-item-id.mjs";
import { $IdentityType, IdentityType } from "./identity-type.mjs";
import { $ItemCount, ItemCount } from "./item-count.mjs";

export interface IdentityBase {
  /**
   * UUID of this identity
   */
  id: UuidHex;

  type: IdentityType;

  createdAt: Date;
  updatedAt: Date;

  /**
   * User owning this identity, null if no user is associated to this identity
   */
  userId?: UuidHex | null;

  items?: Map<HfestItemId, ItemCount>;
}

export const $AbstractIdentity: RecordIoType<IdentityBase> = new RecordType<IdentityBase>({
  properties: {
    type: {type: $IdentityType},
    id: {type: $UuidHex},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
    items: {
      type: new MapType({keyType: $HfestItemId, valueType: $ItemCount, maxSize: 500}),
      optional: true,
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
