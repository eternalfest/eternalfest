import { TaggedUnionType } from "kryo/tagged-union";

import { $HfestIdentity, HfestIdentity } from "../../hfest-identity/hfest-identity.mjs";
import { $EternalfestIdentity, EternalfestIdentity } from "./eternalfest-identity.mjs";

export type Identity = EternalfestIdentity | HfestIdentity;

export const $Identity: TaggedUnionType<Identity> = new TaggedUnionType<Identity>({
  variants: [$EternalfestIdentity, $HfestIdentity],
  tag: "type",
});
