import { CaseStyle } from "kryo";
import { $Date } from "kryo/date";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $IdRef, IdRef } from "./id-ref.mjs";

export interface Session {
  id: UuidHex;

  user: IdRef;

  createdAt: Date;

  updatedAt: Date;
}

export const $Session: RecordIoType<Session> = new RecordType<Session>({
  properties: {
    id: {type: $UuidHex},
    user: {type: $IdRef},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
  },
  changeCase: CaseStyle.SnakeCase,
});
