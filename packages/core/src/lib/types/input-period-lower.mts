import {RecordIoType, RecordType} from "kryo/record";

import {$NullableDate, NullableDate} from "./nullable-date.mjs";

/**
 * Represents the time period `[start, end[.
 */
export interface InputPeriodLower {
  start: NullableDate;
  end: NullableDate;
}

export const $InputPeriodLower: RecordIoType<InputPeriodLower> = new RecordType<InputPeriodLower>(() => ({
  properties: {
    start: {type: $NullableDate},
    end: {type: $NullableDate},
  },
}));
