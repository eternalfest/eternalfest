export type AsyncResourceManager<C, R> = (handler: (context: C) => Promise<R>) => Promise<R>;

export type StreamResourceManager<C, R> = (handler: (context: C) => AsyncIterable<R>) => AsyncIterable<R>;
