import { $Date } from "kryo/date";
import { $Null } from "kryo/null";
import { TryUnionType } from "kryo/try-union";

export type NullableDate = null | Date;

export const $NullableDate: TryUnionType<NullableDate> = new TryUnionType({variants: [$Null, $Date]});
