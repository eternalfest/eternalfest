import { $Null } from "kryo/null";
import { TryUnionType } from "kryo/try-union";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type NullableUuidHex = null | UuidHex;

export const $NullableUuidHex: TryUnionType<NullableUuidHex> = new TryUnionType({variants: [$Null, $UuidHex]});
