import {CaseStyle, IoType} from "kryo";
import {GenericIoType, GenericType} from "kryo/generic";
import {MapType} from "kryo/map";
import {RecordIoType, RecordType} from "kryo/record";

import {$LocaleId, LocaleId} from "./locale-id.mjs";

export interface Localized<T> {
  value: T;
  mainLocale: LocaleId;
  i18n: Map<LocaleId, T>;
}

export const $Localized: GenericIoType<<T>(t: T) => Localized<T>> = new GenericType({
  apply: <T, >(t: IoType<T>): RecordIoType<Localized<T>> => new RecordType({
    properties: {
      value: {type: t},
      mainLocale: {type: $LocaleId},
      i18n: {
        type: new MapType({
          keyType: $LocaleId,
          valueType: t as any,
          maxSize: 500,
          assumeStringKey: true,
        })
      },
    },
    changeCase: CaseStyle.SnakeCase,
  }),
});
