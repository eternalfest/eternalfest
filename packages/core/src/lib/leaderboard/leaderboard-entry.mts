import { CaseStyle } from "kryo";
import { $Float64 } from "kryo/float64";
import { RecordIoType, RecordType } from "kryo/record";

import { $UserRef, UserRef } from "../user/user-ref.mjs";
import { $LeaderboardEntryRun, LeaderboardEntryRun } from "./leaderboard-entry-run.mjs";

export interface LeaderboardEntry {
  score: number;
  user: UserRef;
  run: LeaderboardEntryRun;
}

export const $LeaderboardEntry: RecordIoType<LeaderboardEntry> = new RecordType<LeaderboardEntry>({
  properties: {
    score: {type: $Float64},
    user: {type: $UserRef},
    run: {type: $LeaderboardEntryRun},
  },
  changeCase: CaseStyle.SnakeCase,
});
