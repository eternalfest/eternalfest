import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$GameChannelKey, GameChannelKey} from "../game2/game-channel-key.mjs";
import {$GameModeKey, GameModeKey} from "../game2/game-mode-key.mjs";
import {$GameIdRef, GameIdRef} from "../game2/game-ref.mjs";
import {$LeaderboardEntry, LeaderboardEntry} from "./leaderboard-entry.mjs";

export interface Leaderboard {
  game: GameIdRef;
  channel: GameChannelKey;
  mode: GameModeKey;
  results: LeaderboardEntry[];
}

export const $Leaderboard: RecordIoType<Leaderboard> = new RecordType<Leaderboard>({
  properties: {
    game: {type: $GameIdRef},
    channel: {type: $GameChannelKey},
    mode: {type: $GameModeKey},
    results: {type: new ArrayType({itemType: $LeaderboardEntry, maxLength: Infinity})},
  },
  changeCase: CaseStyle.SnakeCase,
});
