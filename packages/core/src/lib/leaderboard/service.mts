import { UuidHex } from "kryo/uuid-hex";

import { AuthContext } from "../auth/auth-context.mjs";
import { GameModeKey } from "../game2/game-mode-key.mjs";
import { Leaderboard } from "./leaderboard.mjs";

export interface LeaderboardService {
  getLeaderboard(auth: AuthContext, gameId: UuidHex, gameMode: GameModeKey): Promise<Leaderboard>;
}
