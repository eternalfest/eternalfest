import { MapType } from "kryo/map";

import { $PartialProfile, PartialProfile } from "./partial-profile.mjs";
import { $ProfileKey, ProfileKey } from "./profile-key.mjs";

export type ProfileMap = Map<ProfileKey, PartialProfile>;

export const $ProfileMap: MapType<ProfileKey, PartialProfile> = new MapType({
  keyType: $ProfileKey,
  valueType: $PartialProfile,
  maxSize: Infinity,
  assumeStringKey: true,
});
