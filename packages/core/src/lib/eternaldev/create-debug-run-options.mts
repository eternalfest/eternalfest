import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$GameChannelKey, GameChannelKey} from "../game2/game-channel-key.mjs";
import {$GameModeKey, GameModeKey} from "../game2/game-mode-key.mjs";
import {$GameOptionKey, GameOptionKey} from "../game2/game-option-key.mjs";
import {$RunSettings, RunSettings} from "../run/run-settings.mjs";
import {$LocaleId, LocaleId} from "../types/locale-id.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$ProfileKey, ProfileKey} from "./profile-key.mjs";

export interface CreateDebugRunOptions {
  readonly gameId: UuidHex;
  readonly channel: GameChannelKey;
  readonly version: VersionString;
  readonly userId: UuidHex;
  readonly gameMode: GameModeKey;
  readonly gameOptions: ReadonlyArray<GameOptionKey>;
  readonly settings: RunSettings;
  readonly profile?: ProfileKey;
  readonly locale?: LocaleId;
}

export const $CreateDebugRunOptions: RecordIoType<CreateDebugRunOptions> = new RecordType<CreateDebugRunOptions>({
  properties: {
    gameId: {type: $UuidHex},
    userId: {type: $UuidHex},
    channel: {type: $GameChannelKey},
    version: {type: $VersionString},
    gameMode: {type: $GameModeKey},
    gameOptions: {type: new ArrayType({itemType: $GameOptionKey, maxLength: 20})},
    settings: {type: $RunSettings},
    profile: {type: $ProfileKey, optional: true},
    locale: {type: $LocaleId, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
