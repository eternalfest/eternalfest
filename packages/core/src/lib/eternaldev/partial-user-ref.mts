import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $UserDisplayName, UserDisplayName } from "../user/user-display-name.mjs";

export interface PartialUserRef {
  id?: UuidHex;

  displayName?: UserDisplayName;
}

export const $PartialUserRef: RecordIoType<PartialUserRef> = new RecordType<PartialUserRef>({
  properties: {
    id: {type: $UuidHex, optional: true},
    displayName: {type: $UserDisplayName, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
