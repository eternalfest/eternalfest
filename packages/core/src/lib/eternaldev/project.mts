import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $GameBuild, GameBuild } from "../game2/game-build.mjs";
import { $LocalFileMap, LocalFileMap } from "./local-file-map.mjs";
import { $ProfileMap, ProfileMap } from "./profile-map.mjs";

export interface Project {
  id: UuidHex,
  remoteId?: UuidHex,
  localUrl: string;
  game: GameBuild;
  files: LocalFileMap;
  profiles: ProfileMap;
}

export const $Project: RecordIoType<Project> = new RecordType<Project>({
  properties: {
    id: {type: $UuidHex},
    remoteId: {type: $UuidHex, optional: true},
    localUrl: {type: new Ucs2StringType({maxLength: Infinity})},
    game: {type: $GameBuild},
    files: {type: $LocalFileMap},
    profiles: {type: $ProfileMap},
  },
  changeCase: CaseStyle.SnakeCase,
});
