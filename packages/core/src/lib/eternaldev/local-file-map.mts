import { MapType } from "kryo/map";

import { $BlobId, BlobId } from "../blob/blob-id.mjs";
import { $LocalFileRef, LocalFileRef } from "./local-file-ref.mjs";

export type LocalFileMap = Map<BlobId, LocalFileRef>;

export const $LocalFileMap: MapType<BlobId, LocalFileRef> = new MapType({
  keyType: $BlobId,
  valueType: $LocalFileRef,
  maxSize: Infinity,
  assumeStringKey: true,
});
