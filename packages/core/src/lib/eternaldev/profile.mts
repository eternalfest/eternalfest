import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $FamiliesString, FamiliesString } from "../game2/families-string.mjs";
import { $RunItems, RunItems } from "../run/run-items.mjs";
import { $UserRef, UserRef } from "../user/user-ref.mjs";

export interface Profile {
  readonly user: UserRef;
  readonly items: RunItems;
  readonly families: FamiliesString;
}

export const $Profile: RecordIoType<Profile> = new RecordType<Profile>({
  properties: {
    user: {type: $UserRef},
    items: {type: $RunItems},
    families: {type: $FamiliesString},
  },
  changeCase: CaseStyle.SnakeCase,
});
