import { Ucs2StringType } from "kryo/ucs2-string";

export type ProfileKey = string;

export const $ProfileKey: Ucs2StringType = new Ucs2StringType({maxLength: 50});
