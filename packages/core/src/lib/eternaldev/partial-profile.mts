import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $FamiliesString, FamiliesString } from "../game2/families-string.mjs";
import { $RunItems, RunItems } from "../run/run-items.mjs";
import { $PartialUserRef, PartialUserRef } from "./partial-user-ref.mjs";

export interface PartialProfile {
  readonly user?: PartialUserRef;
  readonly items?: RunItems;
  readonly families?: FamiliesString;
}

export const $PartialProfile: RecordIoType<PartialProfile> = new RecordType<PartialProfile>({
  properties: {
    user: {type: $PartialUserRef, optional: true},
    items: {type: $RunItems, optional: true},
    families: {type: $FamiliesString, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
