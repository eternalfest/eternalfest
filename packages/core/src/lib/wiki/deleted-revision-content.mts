import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $WikiContentType, WikiContentType } from "./wiki-content-type.mjs";

export interface DeletedRevisionContent {
  type: WikiContentType.Deleted;
}

// tslint:disable-next-line:max-line-length
export const $DeletedRevisionContent: RecordIoType<DeletedRevisionContent> = new RecordType<DeletedRevisionContent>({
  properties: {
    type: {type: new LiteralType({type: $WikiContentType, value: WikiContentType.Deleted as const})},
  },
  changeCase: CaseStyle.SnakeCase,
});
