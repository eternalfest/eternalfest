import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $MarkdownText, MarkdownText } from "./markdown-text.mjs";
import { $WikiContentType, WikiContentType } from "./wiki-content-type.mjs";
import { $WikiPageTitle, WikiPageTitle } from "./wiki-page-title.mjs";

/**
 * A text revision content provided by the user.
 */
export interface InputTextRevisionContent {
  type: WikiContentType.Text;
  title: WikiPageTitle;
  body: MarkdownText;
}

// tslint:disable-next-line:max-line-length
export const $InputTextRevisionContent: RecordIoType<InputTextRevisionContent> = new RecordType<InputTextRevisionContent>({
  properties: {
    type: {type: new LiteralType({type: $WikiContentType, value: WikiContentType.Text as const})},
    title: {type: $WikiPageTitle},
    body: {type: $MarkdownText},
  },
  changeCase: CaseStyle.SnakeCase,
});
