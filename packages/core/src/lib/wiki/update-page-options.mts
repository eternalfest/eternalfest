import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $InputRevisionContent, InputRevisionContent } from "./input-revision-content.mjs";
import { $RevisionComment, RevisionComment } from "./revision-comment.mjs";

export interface UpdatePageOptions {
  /**
   * Id the old revision.
   *
   * Used to detect edit conflicts.
   */
  revisionId: UuidHex;

  content: InputRevisionContent;

  /**
   * Comment describing the changes in this revision.
   */
  comment: RevisionComment;
}

export const $UpdatePageOptions: RecordIoType<UpdatePageOptions> = new RecordType<UpdatePageOptions>({
  properties: {
    revisionId: {type: $UuidHex},
    content: {type: $InputRevisionContent},
    comment: {type: $RevisionComment},
  },
  changeCase: CaseStyle.SnakeCase,
});
