import { UuidHex } from "kryo/uuid-hex";

import { AuthContext } from "../auth/auth-context.mjs";
import { CreatePageOptions } from "./create-page-options.mjs";
import { GetPageByIdOptions } from "./get-page-by-id-options.mjs";
import { GetPageBySlugOptions } from "./get-page-by-slug-options.mjs";
import { UpdatePageOptions } from "./update-page-options.mjs";
import { WikiPage } from "./wiki-page.mjs";

export interface ReadonlyWikiService {
  getPageById(auth: AuthContext, options: GetPageByIdOptions): Promise<WikiPage | undefined>;

  getPageBySlug(auth: AuthContext, options: GetPageBySlugOptions): Promise<WikiPage | undefined>;
}

export interface WikiService extends ReadonlyWikiService {
  createPage(auth: AuthContext, options: CreatePageOptions): Promise<WikiPage>;

  updatePage(auth: AuthContext, gameId: UuidHex, options: UpdatePageOptions): Promise<WikiPage>;
}
