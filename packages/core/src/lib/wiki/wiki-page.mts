import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $Revision, Revision } from "./revision.mjs";
import { $WikiSlug, WikiSlug } from "./wiki-slug.mjs";

export interface WikiPage {
  id: UuidHex;
  slug: WikiSlug;
  revision: Revision;
}

export const $WikiPage: RecordIoType<WikiPage> = new RecordType<WikiPage>({
  properties: {
    id: {type: $UuidHex},
    slug: {type: $WikiSlug},
    revision: {type: $Revision},
  },
  changeCase: CaseStyle.SnakeCase,
});
