import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $InputTextRevisionContent, InputTextRevisionContent } from "./input-text-revision-content.mjs";
import { $RevisionComment, RevisionComment } from "./revision-comment.mjs";
import { $WikiSlug, WikiSlug } from "./wiki-slug.mjs";

export interface CreatePageOptions {
  /**
   * Slug of the page to create.
   */
  slug: WikiSlug;

  /**
   * Initial content of the page.
   */
  content: InputTextRevisionContent;

  /**
   * Comment describing the initial content of the page.
   */
  comment: RevisionComment;
}

export const $CreatePageOptions: RecordIoType<CreatePageOptions> = new RecordType<CreatePageOptions>({
  properties: {
    slug: {type: $WikiSlug},
    content: {type: $InputTextRevisionContent},
    comment: {type: $RevisionComment},
  },
  changeCase: CaseStyle.SnakeCase,
});
