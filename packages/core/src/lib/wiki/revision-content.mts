import { TaggedUnionType } from "kryo/tagged-union";

import { $DeletedRevisionContent, DeletedRevisionContent } from "./deleted-revision-content.mjs";
import { $TextRevisionContent, TextRevisionContent } from "./text-revision-content.mjs";

export type RevisionContent = DeletedRevisionContent | TextRevisionContent;

export const $RevisionContent: TaggedUnionType<RevisionContent> = new TaggedUnionType<RevisionContent>({
  variants: [$DeletedRevisionContent, $TextRevisionContent],
  tag: "type",
});
