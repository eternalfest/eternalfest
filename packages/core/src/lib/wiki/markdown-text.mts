import { Ucs2StringType } from "kryo/ucs2-string";

/**
 * Raw markdown content.
 */
export type MarkdownText = string;

export const $MarkdownText: Ucs2StringType = new Ucs2StringType({maxLength: Infinity});
