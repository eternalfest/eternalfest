import { TaggedUnionType } from "kryo/tagged-union";

import { $DeletedRevisionContent, DeletedRevisionContent } from "./deleted-revision-content.mjs";
import { $InputTextRevisionContent, InputTextRevisionContent } from "./input-text-revision-content.mjs";

/**
 * Revision content provided by the user.
 */
export type InputRevisionContent = DeletedRevisionContent | InputTextRevisionContent;

export const $InputRevisionContent: TaggedUnionType<InputRevisionContent> = new TaggedUnionType<InputRevisionContent>({
  variants: [$DeletedRevisionContent, $InputTextRevisionContent],
  tag: "type",
});
