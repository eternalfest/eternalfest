import { CaseStyle } from "kryo";
import { TsEnumType } from "kryo/ts-enum";

/**
 * Indicates the type of the content of a wiki page.
 */
export enum WikiContentType {
  /**
   * Indicates that there is no content
   */
  Deleted,

  /**
   * Indicates that the page contains text content
   */
  Text,
}

export const $WikiContentType: TsEnumType<WikiContentType> = new TsEnumType<WikiContentType>({
  enum: WikiContentType,
  changeCase: CaseStyle.KebabCase,
});
