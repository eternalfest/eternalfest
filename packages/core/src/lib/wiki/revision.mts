import { CaseStyle } from "kryo";
import { $Date } from "kryo/date";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $NullableUserRef, UserRef } from "../user/user-ref.mjs";
import { $RevisionComment, RevisionComment } from "./revision-comment.mjs";
import { $RevisionContent, RevisionContent } from "./revision-content.mjs";
import { $NullableRevisionRef, RevisionRef } from "./revision-ref.mjs";

export interface Revision {
  /**
   * Revision id
   *
   * The revision id unambiguously represents a given version of a wiki page.
   */
  id: UuidHex;

  /**
   * Date of the revision.
   */
  date: Date;

  /**
   * User who did the revision.
   *
   * `null` if the revision was created by the system.
   */
  user: UserRef | null;

  /**
   * Comment describing the changes in this revision.
   */
  comment: RevisionComment;

  /**
   * Reference to the next (more recent) revision, if any.
   *
   * `null` indicates that this revision is the most recent one.
   */
  nextRevision?: RevisionRef | null;

  /**
   * Reference to the previous (older) revision, if any.
   *
   * `null` indicates that this revision is the first one.
   */
  previousRevision?: RevisionRef | null;

  /**
   * Content for this revision.
   */
  content: RevisionContent;
}

export const $Revision: RecordIoType<Revision> = new RecordType<Revision>({
  properties: {
    id: {type: $UuidHex},
    date: {type: $Date},
    user: {type: $NullableUserRef},
    comment: {type: $RevisionComment},
    nextRevision: {type: $NullableRevisionRef, optional: true},
    previousRevision: {type: $NullableRevisionRef, optional: true},
    content: {type: $RevisionContent},
  },
  changeCase: CaseStyle.SnakeCase,
});
