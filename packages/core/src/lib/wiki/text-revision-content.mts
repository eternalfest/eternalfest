import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $WikiContentType, WikiContentType } from "./wiki-content-type.mjs";
import { $WikiPageTitle, WikiPageTitle } from "./wiki-page-title.mjs";
import { $WikiText, WikiText } from "./wiki-text.mjs";

export interface TextRevisionContent {
  type: WikiContentType.Text;
  title: WikiPageTitle;
  body: WikiText;
}

export const $TextRevisionContent: RecordIoType<TextRevisionContent> = new RecordType<TextRevisionContent>({
  properties: {
    type: {type: new LiteralType({type: $WikiContentType, value: WikiContentType.Text as const})},
    title: {type: $WikiPageTitle},
    body: {type: $WikiText},
  },
  changeCase: CaseStyle.SnakeCase,
});
