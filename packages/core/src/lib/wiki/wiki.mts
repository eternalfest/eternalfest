import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$NullableGameIdRef, NullableGameIdRef} from "../game2/game-ref.mjs";
import {$WikiPage, WikiPage} from "./wiki-page.mjs";

export interface Wiki {
  id: UuidHex;
  game: NullableGameIdRef;
  home: WikiPage;
}

export const $Wiki: RecordIoType<Wiki> = new RecordType<Wiki>({
  properties: {
    id: {type: $UuidHex},
    game: {type: $NullableGameIdRef},
    home: {type: $WikiPage},
  },
  changeCase: CaseStyle.SnakeCase,
});
