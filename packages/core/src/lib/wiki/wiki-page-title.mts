import { Ucs2StringType } from "kryo/ucs2-string";

export type WikiPageTitle = string;

export const $WikiPageTitle: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 1,
  maxLength: 100,
});
