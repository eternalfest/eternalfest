import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $Ucs2String } from "kryo/ucs2-string";

import { $MarkdownText, MarkdownText } from "./markdown-text.mjs";

export interface WikiText {
  markdown: MarkdownText;
  html: string;
}

export const $WikiText: RecordIoType<WikiText> = new RecordType<WikiText>({
  properties: {
    markdown: {type: $MarkdownText},
    html: {type: $Ucs2String},
  },
  changeCase: CaseStyle.SnakeCase,
});
