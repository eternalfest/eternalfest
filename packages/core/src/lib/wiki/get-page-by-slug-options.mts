import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $NullableUuidHex, NullableUuidHex } from "../types/nullable-uuid-hex.mjs";
import { $WikiSlug, WikiSlug } from "./wiki-slug.mjs";

export interface GetPageBySlugOptions {
  gameId: NullableUuidHex;
  slug: WikiSlug;
  revision?: UuidHex;
}

export const $GetPageBySlugOptions: RecordIoType<GetPageBySlugOptions> = new RecordType<GetPageBySlugOptions>({
  properties: {
    gameId: {type: $NullableUuidHex},
    slug: {type: $WikiSlug},
    revision: {type: $UuidHex, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
