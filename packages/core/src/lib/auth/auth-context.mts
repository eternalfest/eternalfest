import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";
import {TaggedUnionType} from "kryo/tagged-union";

import {$ActorType, ActorType} from "../types/user/actor-type.mjs";
import {$ShortUser, ShortUser} from "../user/short-user.mjs";
import {$AuthScope, AuthScope} from "./auth-scope.mjs";
import {$GuestAuthContext, GuestAuthContext} from "./guest-auth-context.mjs";

export interface UserAuthContext {
  readonly type: ActorType.User;
  readonly scope: AuthScope;
  readonly user: ShortUser;
  readonly isAdministrator: boolean;
  readonly isTester: boolean;
}

export const $UserAuthContext: RecordIoType<UserAuthContext> = new RecordType<UserAuthContext>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.User as ActorType.User})},
    scope: {type: $AuthScope},
    user: {type: $ShortUser},
    isAdministrator: {type: $Boolean},
    isTester: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface SystemAuthContext {
  readonly type: ActorType.System;
}

export const $SystemAuthContext: RecordIoType<SystemAuthContext> = new RecordType<SystemAuthContext>({
  properties: {
    type: {type: new LiteralType({type: $ActorType, value: ActorType.System as ActorType.System})},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type AuthContext = GuestAuthContext | UserAuthContext | SystemAuthContext;

export const $AuthContext: TaggedUnionType<AuthContext> = new TaggedUnionType<AuthContext>({
  variants: [$GuestAuthContext, $UserAuthContext, $SystemAuthContext],
  tag: "type",
});
