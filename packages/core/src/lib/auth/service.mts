import { Session } from "../types/session.mjs";
import { UserDisplayName } from "../user/user-display-name.mjs";
import { UserId } from "../user/user-id.mjs";
import { AuthContext } from "./auth-context.mjs";
import { AuthenticateHttpOptions } from "./authenticate-http-options.mjs";

export interface AuthService {
  authenticateHttp(options: AuthenticateHttpOptions): Promise<AuthContext>;

  session(sessionId: string): Promise<AuthContext>;

  etwinOauth(userId: UserId, userDisplayName: UserDisplayName): Promise<AuthContext>;

  createSession(_acx: AuthContext, _userId: UserId): Promise<Session>;
}
