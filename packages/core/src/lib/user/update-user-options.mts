import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";

import { $UserId, UserId } from "./user-id.mjs";

export interface UpdateUserOptions {
  userId: UserId;
  /**
   * Only administrators are allowed to change the `isTester` flag.
   */
  isTester: boolean;
}

export const $UpdateUserOptions: RecordIoType<UpdateUserOptions> = new RecordType<UpdateUserOptions>({
  properties: {
    userId: {type: $UserId},
    isTester: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});
