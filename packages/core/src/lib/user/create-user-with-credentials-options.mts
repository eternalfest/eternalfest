import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/bytes";
import { RecordIoType, RecordType } from "kryo/record";

import { $Email, Email } from "../types/email.mjs";
import { $Login, Login } from "./login.mjs";
import { $UserDisplayName, UserDisplayName } from "./user-display-name.mjs";

export interface CreateUserWithCredentialsOptions {
  readonly displayName: UserDisplayName;
  readonly email?: Email;
  readonly login: Login;
  readonly password: Uint8Array;
}

// tslint:disable-next-line:max-line-length
export const $CreateUserWithCredentialsOptions: RecordIoType<CreateUserWithCredentialsOptions> = new RecordType<CreateUserWithCredentialsOptions>({
  properties: {
    displayName: {type: $UserDisplayName},
    email: {type: $Email, optional: true},
    login: {type: $Login},
    password: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
