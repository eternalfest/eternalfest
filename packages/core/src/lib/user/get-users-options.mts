import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";
import { SetType } from "kryo/set";

import { $UserId, UserId } from "./user-id.mjs";

export interface GetUsersOptions {
  id: Set<UserId>;
}

export const $GetUsersOptions: RecordIoType<GetUsersOptions> = new RecordType<GetUsersOptions>({
  properties: {
    id: {type: new SetType({itemType: $UserId, maxSize: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});
