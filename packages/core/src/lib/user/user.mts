import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Boolean} from "kryo/boolean";
import {$Date} from "kryo/date";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$ObjectType, ObjectType} from "../types/object-type.mjs";
import {$Identity, Identity} from "../types/user/identity.mjs";
import {$UserDisplayName, UserDisplayName} from "./user-display-name.mjs";

export interface User {
  /**
   * UUID of this user
   */
  id: UuidHex;

  type: ObjectType.User;

  displayName: UserDisplayName;
  createdAt: Date;
  updatedAt: Date;
  identities?: Identity[];
  isAdministrator: boolean;

  /**
   * Indicates that this user has access to the closed beta of Eternalfest
   */
  isTester: boolean;
}

export const $User: RecordIoType<User> = new RecordType<User>({
  properties: {
    id: {type: $UuidHex},
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.User as ObjectType.User})},
    displayName: {type: $UserDisplayName},
    createdAt: {type: $Date},
    updatedAt: {type: $Date},
    // TODO: fully remove this in a future version? (it's already gone from the Rust side)
    identities: {type: new ArrayType({itemType: $Identity, maxLength: 5}), optional: true},
    isAdministrator: {type: $Boolean},
    isTester: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});
