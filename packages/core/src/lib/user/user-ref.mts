import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$ObjectType, ObjectType} from "../types/object-type.mjs";
import {$UserDisplayName, UserDisplayName} from "./user-display-name.mjs";

export interface UserRef {
  id: UuidHex;

  type: ObjectType.User;

  displayName: UserDisplayName;
}

export const $UserRef: RecordIoType<UserRef> = new RecordType<UserRef>({
  properties: {
    id: {type: $UuidHex},
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.User as ObjectType.User})},
    displayName: {type: $UserDisplayName},
  },
  changeCase: CaseStyle.SnakeCase,
});

export const $NullableUserRef: TryUnionType<null | UserRef> = new TryUnionType({variants: [$Null, $UserRef]});
