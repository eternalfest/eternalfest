import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";

import {$UserId, UserId} from "./user-id.mjs";

export interface GetUserOptions {
  id: UserId;
  now: Date,
  time?: Date,
}

export const $GetUserOptions: RecordIoType<GetUserOptions> = new RecordType<GetUserOptions>({
  properties: {
    id: {type: $UserId},
    now: {type: $Date},
    time: {type: $Date, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
