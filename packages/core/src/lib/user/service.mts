import { UuidHex } from "kryo/uuid-hex";

import { AuthContext } from "../auth/auth-context.mjs";
import { UpdateUserOptions } from "./update-user-options.mjs";
import { User } from "./user.mjs";
import { UserDisplayName } from "./user-display-name.mjs";
import { UserListing } from "./user-listing.mjs";
import { UserRef } from "./user-ref.mjs";

export interface UserService {
  getOrCreateUserWithEtwin(auth: AuthContext, userId: UuidHex, userDisplay: UserDisplayName): Promise<User>;

  getUserById(auth: AuthContext, userId: UuidHex): Promise<User | undefined>;

  getUserRefById(auth: AuthContext, userId: UuidHex): Promise<UserRef | undefined>;

  getUserRefsById(auth: AuthContext, userIds: ReadonlySet<UuidHex>): Promise<Map<UuidHex, UserRef | undefined>>;

  getUsers(auth: AuthContext): Promise<UserListing>;

  updateUser(auth: AuthContext, options: UpdateUserOptions): Promise<User>;
}
