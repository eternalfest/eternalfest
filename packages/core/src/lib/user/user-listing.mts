import { CaseStyle } from "kryo";
import { ArrayType } from "kryo/array";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

import { $User, User } from "./user.mjs";

export interface UserListing {
  offset: number;
  limit: number;
  count: number;
  items: User[];
}

export const $UserListing: RecordIoType<UserListing> = new RecordType<UserListing>({
  properties: {
    offset: {type: $Uint32},
    limit: {type: $Uint32},
    count: {type: $Uint32},
    items: {type: new ArrayType({itemType: $User, maxLength: 10000})},
  },
  changeCase: CaseStyle.SnakeCase,
});
