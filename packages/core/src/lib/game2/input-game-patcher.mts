import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";
import {JsonValue} from "kryo-json/json-value";

import {$BlobIdRef, BlobIdRef} from "../blob/blob-id-ref.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$PatcherFramework, PatcherFramework} from "./patcher-framework.mjs";

export interface InputGamePatcher {
  blob: BlobIdRef;
  framework: PatcherFramework;
  meta?: JsonValue;
}

export type ReadonlyInputGamePatcher = DeepReadonly<InputGamePatcher>;

export const $InputGamePatcher: RecordIoType<InputGamePatcher> = new RecordType<InputGamePatcher>({
  properties: {
    blob: {type: $BlobIdRef},
    framework: {type: $PatcherFramework},
    meta: {type: $Any, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableInputGamePatcher = null | InputGamePatcher;

export const $NullableInputGamePatcher: TryUnionType<NullableInputGamePatcher> = new TryUnionType({variants: [$Null, $InputGamePatcher]});
