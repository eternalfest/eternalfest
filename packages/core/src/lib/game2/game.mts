import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$ObjectType, ObjectType} from "../types/object-type.mjs";
import {$ShortUser, ShortUser} from "../user/short-user.mjs";
import {$GameChannelListing, GameChannelListing} from "./game-channel-listing.mjs";
import {$GameId, GameId} from "./game-id.mjs";
import {$NullableGameKey, NullableGameKey} from "./game-key.mjs";

export interface Game {
  type: ObjectType.Game;
  id: GameId;
  createdAt: Date;
  key: NullableGameKey;
  owner: ShortUser;
  channels: GameChannelListing;
}

export type ReadonlyGame = DeepReadonly<Game>;

export const $Game: RecordIoType<Game> = new RecordType<Game>({
  properties: {
    type: new LiteralType({type: $ObjectType, value: ObjectType.GameChannel as ObjectType.GameChannel}),
    id: {type: $GameId},
    createdAt: {type: $Date},
    key: {type: $NullableGameKey},
    owner: {type: $ShortUser},
    channels: {type: $GameChannelListing},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableGame = null | Game;

export const $NullableGame: TryUnionType<NullableGame> = new TryUnionType({variants: [$Null, $Game]});
