import {MapType} from "kryo/map";

import {$LocaleId, LocaleId} from "../types/locale-id.mjs";
import {$InputGameBuildI18n, InputGameBuildI18n} from "./input-game-build-i18n.mjs";

export type InputGameBuildI18nMap = Map<LocaleId, InputGameBuildI18n>;

export const $InputGameBuildI18nMap: MapType<LocaleId, InputGameBuildI18n> = new MapType({
  keyType: $LocaleId,
  valueType: $InputGameBuildI18n,
  maxSize: 500,
  assumeStringKey: true,
});
