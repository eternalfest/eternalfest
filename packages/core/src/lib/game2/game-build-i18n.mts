import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$Blob, Blob} from "../blob/blob.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$GameDescription, GameDescription} from "./game-description.mjs";
import {$GameDisplayName, GameDisplayName} from "./game-display-name.mjs";
import {$GameModeSpecI18nMap, GameModeSpecI18nMap} from "./game-mode-spec-i18n-map.mjs";

export interface GameBuildI18n {
  displayName?: GameDisplayName;
  description?: GameDescription;
  icon?: Blob;
  contentI18n?: Blob;
  modes?: GameModeSpecI18nMap;
}

export type ReadonlyGameBuildI18n = DeepReadonly<GameBuildI18n>;

export const $GameBuildI18n: RecordIoType<GameBuildI18n> = new RecordType<GameBuildI18n>({
  properties: {
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    icon: {type: $Blob, optional: true},
    contentI18n: {type: $Blob, optional: true},
    modes: {type: $GameModeSpecI18nMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
