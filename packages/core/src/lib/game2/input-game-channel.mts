import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Boolean} from "kryo/boolean";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$NullableDate, NullableDate} from "../types/nullable-date.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$GameChannelKey, GameChannelKey} from "./game-channel-key.mjs";
import {$GameChannelPatch, GameChannelPatch} from "./game-channel-patch.mjs";
import {$GameChannelPermission, GameChannelPermission} from "./game-channel-permission.mjs";

export interface InputGameChannel {
  key: GameChannelKey;
  isEnabled: boolean;
  defaultPermission: GameChannelPermission;
  isPinned: boolean;
  publicationDate: NullableDate;
  sortUpdateDate: NullableDate;
  version: VersionString;
  patches: GameChannelPatch[];
}

export type ReadonlyInputGameChannel = DeepReadonly<InputGameChannel>;

export const $InputGameChannel: RecordIoType<InputGameChannel> = new RecordType<InputGameChannel>({
  properties: {
    key: {type: $GameChannelKey},
    isEnabled: {type: $Boolean},
    defaultPermission: {type: $GameChannelPermission},
    isPinned: {type: $Boolean},
    publicationDate: {type: $NullableDate},
    sortUpdateDate: {type: $NullableDate},
    version: {type: $VersionString},
    patches: {type: new ArrayType({itemType: $GameChannelPatch, maxLength: 10})},
  },
  changeCase: CaseStyle.SnakeCase,
});
