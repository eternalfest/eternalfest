import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { DeepReadonly } from "../types/deep-readonly.mjs";
import { $GameOptionDisplayName, GameOptionDisplayName } from "./game-option-display-name.mjs";

export interface GameOptionSpecI18n {
  displayName?: GameOptionDisplayName;
}

export type ReadonlyGameOptionSpecI18n = DeepReadonly<GameOptionSpecI18n>;

export const $GameOptionSpecI18n: RecordIoType<GameOptionSpecI18n> = new RecordType<GameOptionSpecI18n>({
  properties: {
    displayName: {type: $GameOptionDisplayName, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
