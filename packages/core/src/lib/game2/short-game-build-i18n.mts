import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$Blob, Blob} from "../blob/blob.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$GameDescription, GameDescription} from "./game-description.mjs";
import {$GameDisplayName, GameDisplayName} from "./game-display-name.mjs";

export interface ShortGameBuildI18n {
  displayName?: GameDisplayName,
  description?: GameDescription,
  icon?: Blob,
}

export type ReadonlyShortGameBuildI18n = DeepReadonly<ShortGameBuildI18n>;

export const $ShortGameBuildI18n: RecordIoType<ShortGameBuildI18n> = new RecordType<ShortGameBuildI18n>({
  properties: {
    displayName: {type: $GameDisplayName, optional: true},
    description: {type: $GameDescription, optional: true},
    icon: {type: $Blob, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
