import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$BlobIdRef, BlobIdRef} from "../blob/blob-id-ref.mjs";
import {$NullableGameResourceDisplayName, NullableGameResourceDisplayName} from "./game-resource-display-name.mjs";

export interface InputGameResource {
  readonly displayName: NullableGameResourceDisplayName;
  readonly blob: BlobIdRef;
}

export const $InputGameResource: RecordIoType<InputGameResource> = new RecordType<InputGameResource>({
  properties: {
    displayName: {type: $NullableGameResourceDisplayName},
    blob: {type: $BlobIdRef},
  },
  changeCase: CaseStyle.SnakeCase,
});
