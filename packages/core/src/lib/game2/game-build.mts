import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableBlob, Blob,NullableBlob} from "../blob/blob.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$NullableGitCommitRef, NullableGitCommitRef} from "../types/git-commit-ref.mjs";
import {$LocaleId, LocaleId} from "../types/locale-id.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$FamiliesString, FamiliesString} from "./families-string.mjs";
import {GameBuildI18n} from "./game-build-i18n.mjs";
import {$GameBuildI18nMap, GameBuildI18nMap} from "./game-build-i18n-map.mjs";
import {$GameCategory, GameCategory} from "./game-category.mjs";
import {$GameDescription, GameDescription} from "./game-description.mjs";
import {$GameDisplayName, GameDisplayName} from "./game-display-name.mjs";
import {$GameEngine, GameEngine} from "./game-engine.mjs";
import {$GameModeSpecMap, GameModeSpecMap} from "./game-mode-spec-map.mjs";
import {$NullableGamePatcher, NullableGamePatcher} from "./game-patcher.mjs";
import {$GameResource, GameResource} from "./game-resource.mjs";

export interface GameBuild {
  version: VersionString;
  createdAt: Date,
  gitCommitRef: NullableGitCommitRef;
  mainLocale: LocaleId;
  displayName: GameDisplayName;
  description: GameDescription;
  // Blob id ref
  icon: NullableBlob;
  loader: VersionString;
  engine: GameEngine;
  patcher: NullableGamePatcher;
  debug: NullableBlob;
  content: NullableBlob;
  contentI18n: NullableBlob;
  musics: GameResource[];
  modes: GameModeSpecMap;
  families: FamiliesString;
  category: GameCategory;
  i18n: GameBuildI18nMap;
}

export type ReadonlyGameBuild = DeepReadonly<GameBuild>;

export const $GameBuild: RecordIoType<GameBuild> = new RecordType<GameBuild>({
  properties: {
    version: {type: $VersionString},
    createdAt: {type: $Date},
    gitCommitRef: {type: $NullableGitCommitRef},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    icon: {type: $NullableBlob},
    loader: {type: $VersionString},
    engine: {type: $GameEngine},
    patcher: {type: $NullableGamePatcher},
    debug: {type: $NullableBlob},
    content: {type: $NullableBlob},
    contentI18n: {type: $NullableBlob},
    musics: {type: new ArrayType({itemType: $GameResource, maxLength: 50})},
    modes: {type: $GameModeSpecMap},
    families: {type: $FamiliesString},
    category: {type: $GameCategory},
    i18n: {type: $GameBuildI18nMap},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface LocalizedBlob {
  locale: LocaleId;
  blob: Blob;
}

export function resolveContentI18n(build: GameBuild, locale: LocaleId): LocalizedBlob | null {
  const i18n: GameBuildI18n | undefined = build.i18n.get(locale);
  if (i18n !== undefined) {
    const blob = i18n.contentI18n;
    if (blob !== undefined) {
      return {locale, blob};
    }
  }
  const blob = build.contentI18n;
  return blob !== null ? {locale: build.mainLocale, blob} : null;
}
