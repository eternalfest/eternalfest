import { Ucs2StringType } from "kryo/ucs2-string";

export type FamiliesString = string;

export const $FamiliesString: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 0,
  maxLength: 500,
  pattern: /^(?:(?:0|[1-9]\d*)(?:,(?:0|[1-9]\d*))*)?$/,
});
