import {MapType} from "kryo/map";

import {$GameOptionKey, GameOptionKey} from "./game-option-key.mjs";
import {$GameOptionSpec, GameOptionSpec} from "./game-option-spec.mjs";

export type GameOptionSpecMap = Map<GameOptionKey, GameOptionSpec>;

export const $GameOptionSpecMap: MapType<GameOptionKey, GameOptionSpec> = new MapType({
  keyType: $GameOptionKey,
  valueType: $GameOptionSpec,
  maxSize: 500,
  assumeStringKey: true,
});
