import {CaseStyle} from "kryo";
import {MapType} from "kryo/map";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableBlob, NullableBlob} from "../blob/blob.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$NullableGitCommitRef, NullableGitCommitRef} from "../types/git-commit-ref.mjs";
import {$LocaleId, LocaleId} from "../types/locale-id.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$GameDescription, GameDescription} from "./game-description.mjs";
import {$GameDisplayName, GameDisplayName} from "./game-display-name.mjs";
import {$ShortGameBuildI18n, ShortGameBuildI18n} from "./short-game-build-i18n.mjs";

export interface ShortGameBuild {
  version: VersionString;
  gitCommitRef: NullableGitCommitRef;
  mainLocale: LocaleId;
  displayName: GameDisplayName,
  description: GameDescription,
  icon: NullableBlob,
  i18n: Map<LocaleId, ShortGameBuildI18n>,
}

export type ReadonlyShortGameBuild = DeepReadonly<ShortGameBuild>;

export const $ShortGameBuild: RecordIoType<ShortGameBuild> = new RecordType<ShortGameBuild>({
  properties: {
    version: {type: $VersionString},
    gitCommitRef: {type: $NullableGitCommitRef},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    icon: {type: $NullableBlob},
    i18n: {
      type: new MapType({
        keyType: $LocaleId,
        valueType: $ShortGameBuildI18n,
        maxSize: 500,
        assumeStringKey: true,
      }),
    },
  },
  changeCase: CaseStyle.SnakeCase,
});
