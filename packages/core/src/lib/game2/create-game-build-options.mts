import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$GameRef, GameRef} from "./game-ref.mjs";
import {$InputGameBuild, InputGameBuild} from "./input-game-build.mjs";

export interface CreateGameBuildOptions {
  game: GameRef;
  build: InputGameBuild;
}

export type ReadonlyCreateGameBuildOptions = DeepReadonly<CreateGameBuildOptions>;

export const $CreateGameBuildOptions: RecordIoType<CreateGameBuildOptions> = new RecordType<CreateGameBuildOptions>({
  properties: {
    game: {type: $GameRef},
    build: {type: $InputGameBuild},
  },
  changeCase: CaseStyle.SnakeCase,
});
