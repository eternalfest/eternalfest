import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {$Boolean} from "kryo/boolean";
import {$Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

import {$ActiveGameChannel, ActiveGameChannel} from "./active-game-channel.mjs";
import {$NullableShortGameChannel, NullableShortGameChannel} from "./short-game-channel.mjs";

export interface GameChannelListing {
  offset: number;
  limit: number;
  count: number;
  isCountExact: boolean;
  active: ActiveGameChannel;
  items: NullableShortGameChannel[];
}

export const $GameChannelListing: RecordIoType<GameChannelListing> = new RecordType<GameChannelListing>({
  properties: {
    offset: {type: $Uint32},
    limit: {type: $Uint32},
    count: {type: $Uint32},
    isCountExact: {type: $Boolean},
    active: {type: $ActiveGameChannel},
    items: {type: new ArrayType({itemType: $NullableShortGameChannel, maxLength: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});
