import {CaseStyle} from "kryo";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {$GameId, GameId} from "./game-id.mjs";
import {$GameKey, GameKey} from "./game-key.mjs";

export interface GameIdRef {
  id: GameId;
}

export const $GameIdRef: RecordIoType<GameIdRef> = new RecordType<GameIdRef>({
  properties: {
    id: {type: $GameId},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableGameIdRef = null | GameIdRef;

export const $NullableGameIdRef: TryUnionType<NullableGameIdRef> = new TryUnionType({variants: [$Null, $GameIdRef]});

export interface GameKeyRef {
  key: GameKey;
}

export const $GameKeyRef: RecordIoType<GameKeyRef> = new RecordType<GameKeyRef>({
  properties: {
    key: {type: $GameKey},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type GameRef = GameIdRef | GameKeyRef;

export const $GameRef: TryUnionType<GameRef> = new TryUnionType<GameRef>({
  variants: [$GameIdRef, $GameKeyRef],
});
