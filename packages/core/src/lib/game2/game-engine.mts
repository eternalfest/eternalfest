import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";
import {TaggedUnionType} from "kryo/tagged-union";

import {$Blob, Blob} from "../blob/blob.mjs";
import {$GameEngineType, GameEngineType} from "./game-engine-type.mjs";

export interface V96GameEngine {
  readonly type: GameEngineType.V96;
}

export const $V96GameEngine: RecordIoType<V96GameEngine> = new RecordType<V96GameEngine>({
  properties: {
    type: {type: new LiteralType({type: $GameEngineType, value: GameEngineType.V96 as const})},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface CustomGameEngine {
  readonly type: GameEngineType.Custom;
  readonly blob: Blob;
}

export const $CustomGameEngine: RecordIoType<CustomGameEngine> = new RecordType<CustomGameEngine>({
  properties: {
    type: {type: new LiteralType({type: $GameEngineType, value: GameEngineType.Custom as const})},
    blob: {type: $Blob},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type GameEngine = V96GameEngine | CustomGameEngine;

export const $GameEngine: TaggedUnionType<GameEngine> = new TaggedUnionType<GameEngine>({
  variants: [$V96GameEngine, $CustomGameEngine],
  tag: "type",
});
