import {CaseStyle} from "kryo";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {$GameChannelKey,GameChannelKey} from "./game-channel-key.mjs";

export interface GameChannelKeyRef {
  key: GameChannelKey;
}

export const $GameChannelKeyRef: RecordIoType<GameChannelKeyRef> = new RecordType<GameChannelKeyRef>({
  properties: {
    key: {type: $GameChannelKey},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableGameChannelKeyRef = null | GameChannelKeyRef;

export const $NullableGameChannelKeyRef: TryUnionType<NullableGameChannelKeyRef> = new TryUnionType({variants: [$Null, $GameChannelKeyRef]});

export type GameChannelRef = GameChannelKeyRef;

export const $GameChannelRef = $GameChannelKeyRef;
