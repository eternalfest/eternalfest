import {CaseStyle} from "kryo";
import {$Date} from "kryo/date";
import {$Uint32} from "kryo/integer";
import {RecordIoType, RecordType} from "kryo/record";

import {$GameRef, GameRef} from "./game-ref.mjs";

export interface GetGameOptions {
  game: GameRef,
  channelOffset: number,
  channelLimit: number,
  time?: Date,
}

export const $GetGameOptions: RecordIoType<GetGameOptions> = new RecordType<GetGameOptions>({
  properties: {
    game: {type: $GameRef},
    channelOffset: {type: $Uint32},
    channelLimit: {type: $Uint32},
    time: {type: $Date, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
