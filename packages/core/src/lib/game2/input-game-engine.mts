import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {RecordIoType, RecordType} from "kryo/record";
import {TaggedUnionType} from "kryo/tagged-union";

import {$BlobIdRef, BlobIdRef} from "../blob/blob-id-ref.mjs";
import {$GameEngineType, GameEngineType} from "./game-engine-type.mjs";

export interface V96InputGameEngine {
  readonly type: GameEngineType.V96;
}

export const $V96InputGameEngine: RecordIoType<V96InputGameEngine> = new RecordType<V96InputGameEngine>({
  properties: {
    type: {type: new LiteralType({type: $GameEngineType, value: GameEngineType.V96 as const})},
  },
  changeCase: CaseStyle.SnakeCase,
});

export interface CustomInputGameEngine {
  readonly type: GameEngineType.Custom;
  readonly blob: BlobIdRef;
}

export const $CustomInputGameEngine: RecordIoType<CustomInputGameEngine> = new RecordType<CustomInputGameEngine>({
  properties: {
    type: {type: new LiteralType({type: $GameEngineType, value: GameEngineType.Custom as const})},
    blob: {type: $BlobIdRef},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type InputGameEngine = V96InputGameEngine | CustomInputGameEngine;

export const $InputGameEngine: TaggedUnionType<InputGameEngine> = new TaggedUnionType<InputGameEngine>({
  variants: [$V96InputGameEngine, $CustomInputGameEngine],
  tag: "type",
});
