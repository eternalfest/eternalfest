import { Ucs2StringType } from "kryo/ucs2-string";

export type GameOptionKey = string;

export const $GameOptionKey: Ucs2StringType = new Ucs2StringType({maxLength: 50});
