import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";
import {JsonValue} from "kryo-json/json-value";

import {$Blob, Blob} from "../blob/blob.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$PatcherFramework, PatcherFramework} from "./patcher-framework.mjs";

export interface GamePatcher {
  blob: Blob;
  framework: PatcherFramework;
  meta?: JsonValue;
}

export type ReadonlyGamePatcher = DeepReadonly<GamePatcher>;

export const $GamePatcher: RecordIoType<GamePatcher> = new RecordType<GamePatcher>({
  properties: {
    blob: {type: $Blob},
    framework: {type: $PatcherFramework},
    meta: {type: $Any, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableGamePatcher = null | GamePatcher;

export const $NullableGamePatcher: TryUnionType<NullableGamePatcher> = new TryUnionType({variants: [$Null, $GamePatcher]});
