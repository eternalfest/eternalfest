import { CaseStyle } from "kryo";
import {$Boolean} from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";

import { DeepReadonly } from "../types/deep-readonly.mjs";
import { $GameModeDisplayName, GameModeDisplayName } from "./game-mode-display-name.mjs";
import {$GameOptionSpecMap, GameOptionSpecMap} from "./game-option-spec-map.mjs";

export interface GameModeSpec {
  displayName: GameModeDisplayName;
  isVisible: boolean;
  options: GameOptionSpecMap;
}

export type ReadonlyGameModeSpec = DeepReadonly<GameModeSpec>;

export const $GameModeSpec: RecordIoType<GameModeSpec> = new RecordType<GameModeSpec>({
  properties: {
    displayName: {type: $GameModeDisplayName},
    isVisible: {type: $Boolean},
    options: {type: $GameOptionSpecMap},
  },
  changeCase: CaseStyle.SnakeCase,
});
