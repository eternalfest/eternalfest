import {MapType} from "kryo/map";

import {$GameOptionKey, GameOptionKey} from "./game-option-key.mjs";
import {$GameOptionSpecI18n, GameOptionSpecI18n} from "./game-option-spec-i18n.mjs";

export type GameOptionSpecI18nMap = Map<GameOptionKey, GameOptionSpecI18n>;

export const $GameOptionSpecI18nMap: MapType<GameOptionKey, GameOptionSpecI18n> = new MapType({
  keyType: $GameOptionKey,
  valueType: $GameOptionSpecI18n,
  maxSize: 500,
  assumeStringKey: true,
});
