import {AuthContext} from "../auth/auth-context.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {ActiveGameChannel} from "./active-game-channel.mjs";
import {CreateGameBuildOptions} from "./create-game-build-options.mjs";
import {CreateGameOptions} from "./create-game-options.mjs";
import {Game, NullableGame} from "./game.mjs";
import {GameBuild} from "./game-build.mjs";
import {GetGameOptions} from "./get-game-options.mjs";
import {GetGamesOptions} from "./get-games-options.mjs";
import {SetGameFavoriteOptions} from "./set-game-favorite-options.mjs";
import {ShortGameListing} from "./short-game-listing.mjs";
import {UpdateGameChannelOptions} from "./update-game-channel-options.mjs";

export interface ReadonlyGame2Service {
  createGame(acx: AuthContext, options: DeepReadonly<CreateGameOptions>): Promise<Game>;

  getGame(acx: AuthContext, options: DeepReadonly<GetGameOptions>): Promise<NullableGame>;

  getGames(acx: AuthContext, options: DeepReadonly<GetGamesOptions>): Promise<ShortGameListing>;

  setGameFavorite(acx: AuthContext, options: DeepReadonly<SetGameFavoriteOptions>): Promise<unknown>;

  createBuild(acx: AuthContext, options: DeepReadonly<CreateGameBuildOptions>): Promise<GameBuild>;

  updateChannel(acx: AuthContext, options: DeepReadonly<UpdateGameChannelOptions>): Promise<ActiveGameChannel>;
}

export interface Game2Service extends ReadonlyGame2Service {
}
