import {TryUnionType} from "kryo/try-union";

import {$GameId, GameId} from "./game-id.mjs";
import {$GameKey, GameKey} from "./game-key.mjs";

export type RawGameRef = GameId | GameKey;

export const $RawGameRef: TryUnionType<RawGameRef> = new TryUnionType<RawGameRef>({
  variants: [$GameId, $GameKey],
});
