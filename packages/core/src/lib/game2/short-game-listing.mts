import {RecordIoType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$Listing, Listing} from "../types/listing.mjs";
import {$NullableShortGame, NullableShortGame} from "./short-game.mjs";

export type ShortGameListing = Listing<NullableShortGame>;

export type ReadonlyShortGameListing = DeepReadonly<ShortGameListing>;

export const $ShortGameListing: RecordIoType<ShortGameListing> = $Listing.apply($NullableShortGame) as RecordIoType<ShortGameListing>;
