import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";

import {$Blob, Blob} from "../blob/blob.mjs";
import {$NullableGameResourceDisplayName, NullableGameResourceDisplayName} from "./game-resource-display-name.mjs";

export interface GameResource {
  readonly displayName: NullableGameResourceDisplayName;
  readonly blob: Blob;
}

export const $GameResource: RecordIoType<GameResource> = new RecordType<GameResource>({
  properties: {
    displayName: {type: $NullableGameResourceDisplayName},
    blob: {type: $Blob},
  },
  changeCase: CaseStyle.SnakeCase,
});
