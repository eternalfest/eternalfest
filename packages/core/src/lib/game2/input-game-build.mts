import {CaseStyle} from "kryo";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {$NullableBlobIdRef, NullableBlobIdRef} from "../blob/blob-id-ref.mjs";
import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$NullableGitCommitRef, NullableGitCommitRef} from "../types/git-commit-ref.mjs";
import {$LocaleId, LocaleId} from "../types/locale-id.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$FamiliesString, FamiliesString} from "./families-string.mjs";
import {$GameCategory, GameCategory} from "./game-category.mjs";
import {$GameDescription, GameDescription} from "./game-description.mjs";
import {$GameDisplayName, GameDisplayName} from "./game-display-name.mjs";
import {$GameModeSpecMap, GameModeSpecMap} from "./game-mode-spec-map.mjs";
import {$InputGameBuildI18nMap, InputGameBuildI18nMap} from "./input-game-build-i18n-map.mjs";
import {$InputGameEngine, InputGameEngine} from "./input-game-engine.mjs";
import {$NullableInputGamePatcher, NullableInputGamePatcher} from "./input-game-patcher.mjs";
import {$InputGameResource, InputGameResource} from "./input-game-resource.mjs";

export interface InputGameBuild {
  version: VersionString;
  gitCommitRef?: NullableGitCommitRef;
  mainLocale: LocaleId;
  displayName: GameDisplayName;
  description: GameDescription;
  // Blob id ref
  icon: NullableBlobIdRef;
  loader: VersionString;
  engine: InputGameEngine;
  patcher: NullableInputGamePatcher;
  debug: NullableBlobIdRef;
  content: NullableBlobIdRef;
  contentI18n?: NullableBlobIdRef;
  musics: InputGameResource[];
  modes: GameModeSpecMap;
  families: FamiliesString;
  category: GameCategory;
  i18n?: InputGameBuildI18nMap;
}

export type ReadonlyInputGameBuild = DeepReadonly<InputGameBuild>;

export const $InputGameBuild: RecordIoType<InputGameBuild> = new RecordType<InputGameBuild>({
  properties: {
    version: {type: $VersionString},
    gitCommitRef: {type: $NullableGitCommitRef, optional: true},
    mainLocale: {type: $LocaleId},
    displayName: {type: $GameDisplayName},
    description: {type: $GameDescription},
    icon: {type: $NullableBlobIdRef},
    loader: {type: $VersionString},
    engine: {type: $InputGameEngine},
    patcher: {type: $NullableInputGamePatcher, optional: true},
    debug: {type: $NullableBlobIdRef},
    content: {type: $NullableBlobIdRef},
    contentI18n: {type: $NullableBlobIdRef},
    musics: {type: new ArrayType({itemType: $InputGameResource, maxLength: 50})},
    modes: {type: $GameModeSpecMap},
    families: {type: $FamiliesString},
    category: {type: $GameCategory},
    i18n: {type: $InputGameBuildI18nMap, optional: true},
  },
  changeCase: CaseStyle.SnakeCase,
});
