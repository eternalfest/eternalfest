import {CaseStyle} from "kryo";
import {RecordIoType, RecordType} from "kryo/record";
import {$Ucs2String} from "kryo/ucs2-string";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";

export interface PatcherFramework {
  name: string;
  version: VersionString;
}

export type ReadonlyPatcherFramework = DeepReadonly<PatcherFramework>;

export const $PatcherFramework: RecordIoType<PatcherFramework> = new RecordType<PatcherFramework>({
  properties: {
    name: {type: $Ucs2String},
    version: {type: $VersionString},
  },
  changeCase: CaseStyle.SnakeCase,
});
