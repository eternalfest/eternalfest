import {CaseStyle} from "kryo";
import { ArrayType } from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$NullableUserIdRef, NullableUserIdRef} from "../user/user-id-ref.mjs";
import {$NullableGameKey, NullableGameKey} from "./game-key.mjs";
import {$InputGameBuild, InputGameBuild} from "./input-game-build.mjs";
import {$InputGameChannel, InputGameChannel} from "./input-game-channel.mjs";

export interface CreateGameOptions {
  owner?: NullableUserIdRef;
  key?: NullableGameKey;
  build: InputGameBuild;
  channels: InputGameChannel[];
}

export type ReadonlyCreateGameOptions = DeepReadonly<CreateGameOptions>;

export const $CreateGameOptions: RecordIoType<CreateGameOptions> = new RecordType<CreateGameOptions>({
  properties: {
    owner: {type: $NullableUserIdRef, optional: true},
    key: {type: $NullableGameKey, optional: true},
    build: {type: $InputGameBuild },
    channels: {type: new ArrayType({itemType: $InputGameChannel, minLength: 1, maxLength: 10}) },
  },
  changeCase: CaseStyle.SnakeCase,
});
