import { CaseStyle } from "kryo";
import {$Boolean} from "kryo/boolean";
import { RecordIoType, RecordType } from "kryo/record";

import { DeepReadonly } from "../types/deep-readonly.mjs";
import { $GameOptionDisplayName, GameOptionDisplayName } from "./game-option-display-name.mjs";

export interface GameOptionSpec {
  displayName: GameOptionDisplayName;
  isVisible: boolean;
  isEnabled: boolean;
  defaultValue: boolean;
}

export type ReadonlyGameOptionSpec = DeepReadonly<GameOptionSpec>;

export const $GameOptionSpec: RecordIoType<GameOptionSpec> = new RecordType<GameOptionSpec>({
  properties: {
    displayName: {type: $GameOptionDisplayName},
    isVisible: {type: $Boolean},
    isEnabled: {type: $Boolean},
    defaultValue: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});
