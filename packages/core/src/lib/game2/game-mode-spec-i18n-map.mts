import {MapType} from "kryo/map";

import {$GameModeKey, GameModeKey} from "./game-mode-key.mjs";
import {$GameModeSpecI18n, GameModeSpecI18n} from "./game-mode-spec-i18n.mjs";

export type GameModeSpecI18nMap = Map<GameModeKey, GameModeSpecI18n>;

export const $GameModeSpecI18nMap: MapType<GameModeKey, GameModeSpecI18n> = new MapType({
  keyType: $GameModeKey,
  valueType: $GameModeSpecI18n,
  maxSize: 500,
  assumeStringKey: true,
});
