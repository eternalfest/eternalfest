import {MapType} from "kryo/map";

import {$GameModeKey, GameModeKey} from "./game-mode-key.mjs";
import {$GameModeSpec, GameModeSpec} from "./game-mode-spec.mjs";

export type GameModeSpecMap = Map<GameModeKey, GameModeSpec>;

export const $GameModeSpecMap: MapType<GameModeKey, GameModeSpec> = new MapType({
  keyType: $GameModeKey,
  valueType: $GameModeSpec,
  maxSize: 500,
  assumeStringKey: true,
});
