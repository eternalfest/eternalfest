import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { DeepReadonly } from "../types/deep-readonly.mjs";
import { $GameModeDisplayName, GameModeDisplayName } from "./game-mode-display-name.mjs";
import {$GameOptionSpecI18nMap, GameOptionSpecI18nMap} from "./game-option-spec-i18n-map.mjs";

export interface GameModeSpecI18n {
  displayName?: GameModeDisplayName;
  options?: GameOptionSpecI18nMap;
}

export type ReadonlyGameModeSpecI18n = DeepReadonly<GameModeSpecI18n>;

export const $GameModeSpecI18n: RecordIoType<GameModeSpecI18n> = new RecordType<GameModeSpecI18n>({
  properties: {
    displayName: {type: $GameModeDisplayName, optional: true},
    options: {type: $GameOptionSpecI18nMap},
  },
  changeCase: CaseStyle.SnakeCase,
});
