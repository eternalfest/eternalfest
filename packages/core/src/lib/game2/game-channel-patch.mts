import {CaseStyle} from "kryo";
import {$Boolean} from "kryo/boolean";
import {$Date} from "kryo/date";
import {RecordIoType, RecordType} from "kryo/record";

import {DeepReadonly} from "../types/deep-readonly.mjs";
import {$InputPeriodLower, InputPeriodLower} from "../types/input-period-lower.mjs";
import {$NullableDate, NullableDate} from "../types/nullable-date.mjs";
import {$VersionString, VersionString} from "../types/version-string.mjs";
import {$GameChannelPermission, GameChannelPermission} from "./game-channel-permission.mjs";

export interface GameChannelPatch {
  period: InputPeriodLower;
  isEnabled: boolean;
  defaultPermission: GameChannelPermission;
  isPinned: boolean;
  publicationDate: NullableDate;
  sortUpdateDate: Date;
  version: VersionString;
}

export type ReadonlyGameChannelPatch = DeepReadonly<GameChannelPatch>;

export const $GameChannelPatch: RecordIoType<GameChannelPatch> = new RecordType<GameChannelPatch>({
  properties: {
    period: {type: $InputPeriodLower},
    isEnabled: {type: $Boolean},
    defaultPermission: {type: $GameChannelPermission},
    isPinned: {type: $Boolean},
    publicationDate: {type: $NullableDate},
    sortUpdateDate: {type: $Date},
    version: {type: $VersionString},
  },
  changeCase: CaseStyle.SnakeCase,
});
