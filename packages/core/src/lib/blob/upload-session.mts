import { CaseStyle } from "kryo";
import { DateType } from "kryo/date";
import { RecordIoType, RecordType } from "kryo/record";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

import { $Range, Range } from "../types/range.mjs";
import { $NullableBlob, NullableBlob } from "./blob.mjs";

export interface UploadSession {
  id: UuidHex;
  expiresAt: Date;
  remainingRange: Range;
  blob: NullableBlob;
}

export const $UploadSession: RecordIoType<UploadSession> = new RecordType<UploadSession>(() => ({
  properties: {
    id: {type: $UuidHex},
    expiresAt: {type: new DateType()},
    remainingRange: {type: $Range},
    blob: {type: $NullableBlob},
  },
  changeCase: CaseStyle.SnakeCase,
}));
