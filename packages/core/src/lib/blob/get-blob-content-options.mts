import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $BlobId, BlobId } from "./blob-id.mjs";

export interface GetBlobContentOptions {
  id: BlobId;
}

export const $GetBlobContentOptions: RecordIoType<GetBlobContentOptions> = new RecordType<GetBlobContentOptions>({
  properties: {
    id: {type: $BlobId},
  },
  changeCase: CaseStyle.SnakeCase,
});
