import {CaseStyle} from "kryo";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";

import {$ObjectType, ObjectType} from "../types/object-type.mjs";
import {$BlobId, BlobId} from "./blob-id.mjs";

/**
 * Wrapper object for a blob id.
 */
export interface BlobIdRef {
  type: ObjectType.Blob;
  id: BlobId;
}

export const $BlobIdRef: RecordIoType<BlobIdRef> = new RecordType<BlobIdRef>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.Blob})},
    id: {type: $BlobId},
  },
  changeCase: CaseStyle.SnakeCase,
});

export type NullableBlobIdRef = null | BlobIdRef;

export const $NullableBlobIdRef: TryUnionType<NullableBlobIdRef> = new TryUnionType({variants: [$Null, $BlobIdRef]});
