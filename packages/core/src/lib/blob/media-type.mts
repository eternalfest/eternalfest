import { Ucs2StringType } from "kryo/ucs2-string";

export type MediaType = string;

export const $MediaType: Ucs2StringType = new Ucs2StringType({
  trimmed: true,
  minLength: 3,
  maxLength: 120,
  pattern: /^[0-9a-z.-]+\/[0-9a-z.-]+$/,
});
