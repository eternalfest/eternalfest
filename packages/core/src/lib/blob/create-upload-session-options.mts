import { CaseStyle } from "kryo";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

import { $MediaType, MediaType } from "./media-type.mjs";

export interface CreateUploadSessionOptions {
  mediaType: MediaType;
  byteSize: number;
}

// tslint:disable-next-line:max-line-length
export const $CreateUploadSessionOptions: RecordIoType<CreateUploadSessionOptions> = new RecordType<CreateUploadSessionOptions>({
  properties: {
    mediaType: {type: $MediaType},
    byteSize: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
});
