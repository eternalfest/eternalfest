import {CaseStyle} from "kryo";
import {$Uint32} from "kryo/integer";
import {LiteralType} from "kryo/literal";
import {$Null} from "kryo/null";
import {RecordIoType, RecordType} from "kryo/record";
import {TryUnionType} from "kryo/try-union";
import {$UuidHex, UuidHex} from "kryo/uuid-hex";

import {$ObjectType, ObjectType} from "../types/object-type.mjs";
import {$MediaType, MediaType} from "./media-type.mjs";

/**
 * Placing the mediatype here (instead of the file interface) is consistent with the web Blob
 * object.
 *
 * - https://developer.mozilla.org/en-US/docs/Web/API/Blob
 */
export interface Blob {
  type?: ObjectType.Blob;
  id: UuidHex;
  mediaType: MediaType;
  byteSize: number;
  // dataUrl?: string;
}

export const $Blob: RecordIoType<Blob> = new RecordType<Blob>(() => ({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.Blob}), optional: true},
    id: {type: $UuidHex},
    mediaType: {type: $MediaType},
    byteSize: {type: $Uint32},
  },
  changeCase: CaseStyle.SnakeCase,
}));

export type NullableBlob = null | Blob;

export const $NullableBlob: TryUnionType<NullableBlob> = new TryUnionType({
  variants: [$Null, $Blob],
});
