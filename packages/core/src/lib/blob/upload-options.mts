import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/bytes";
import { $Uint32 } from "kryo/integer";
import { RecordIoType, RecordType } from "kryo/record";

import { $UploadSessionId, UploadSessionId } from "./upload-session-id.mjs";

export interface UploadOptions {
  uploadSessionId: UploadSessionId;
  offset: number;
  data: Uint8Array;
}

export const $UploadOptions: RecordIoType<UploadOptions> = new RecordType<UploadOptions>({
  properties: {
    uploadSessionId: {type: $UploadSessionId},
    offset: {type: $Uint32},
    data: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
