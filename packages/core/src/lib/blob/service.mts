import stream from "stream";

import { AuthContext } from "../auth/auth-context.mjs";
import { Blob } from "./blob.mjs";
import { CreateBlobOptions } from "./create-blob-options.mjs";
import { CreateUploadSessionOptions } from "./create-upload-session-options.mjs";
import { GetBlobOptions } from "./get-blob-options.mjs";
import { UploadOptions } from "./upload-options.mjs";
import { UploadSession } from "./upload-session.mjs";

export interface BlobService {
  readonly hasImmutableBlobs: boolean;

  createBlob(acx: AuthContext, options: Readonly<CreateBlobOptions>): Promise<Blob>;

  getBlobById(acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<Blob>;

  readBlobData(acx: AuthContext, options: Readonly<GetBlobOptions>): Promise<stream.Readable>;

  createUploadSession(acx: AuthContext, options: Readonly<CreateUploadSessionOptions>): Promise<UploadSession>;

  uploadBytes(acx: AuthContext, options: Readonly<UploadOptions>): Promise<UploadSession>;
}
