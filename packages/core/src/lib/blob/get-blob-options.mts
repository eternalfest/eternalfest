import { CaseStyle } from "kryo";
import { RecordIoType, RecordType } from "kryo/record";

import { $BlobId, BlobId } from "./blob-id.mjs";

export interface GetBlobOptions {
  id: BlobId;
}

export const $GetBlobOptions: RecordIoType<GetBlobOptions> = new RecordType<GetBlobOptions>({
  properties: {
    id: {type: $BlobId},
  },
  changeCase: CaseStyle.SnakeCase,
});
