import { CaseStyle } from "kryo";
import { $Bytes } from "kryo/bytes";
import { RecordIoType, RecordType } from "kryo/record";

import { $MediaType, MediaType } from "./media-type.mjs";

export interface CreateBlobOptions {
  mediaType: MediaType;
  data: Uint8Array;
}

export const $CreateBlobOptions: RecordIoType<CreateBlobOptions> = new RecordType<CreateBlobOptions>({
  properties: {
    mediaType: {type: $MediaType},
    data: {type: $Bytes},
  },
  changeCase: CaseStyle.SnakeCase,
});
