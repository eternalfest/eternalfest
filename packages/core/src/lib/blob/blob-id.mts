import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type BlobId = UuidHex;

export const $BlobId: Ucs2StringType = $UuidHex;
