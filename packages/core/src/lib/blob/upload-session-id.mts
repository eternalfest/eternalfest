import { Ucs2StringType } from "kryo/ucs2-string";
import { $UuidHex, UuidHex } from "kryo/uuid-hex";

export type UploadSessionId = UuidHex;

export const $UploadSessionId: Ucs2StringType = $UuidHex;
