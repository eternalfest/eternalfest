import { CaseStyle } from "kryo";
import { $Boolean } from "kryo/boolean";
import { $Uint32 } from "kryo/integer";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";
import { Ucs2StringType } from "kryo/ucs2-string";

import { $Email, Email } from "../types/email.mjs";
import { $AbstractIdentity, IdentityBase } from "../types/user/identity-base.mjs";
import { $IdentityType, IdentityType } from "../types/user/identity-type.mjs";
import { $HfestLogin, HfestLogin } from "./hfest-login.mjs";
import { $HfestServer, HfestServer } from "./hfest-server.mjs";

export interface HfestIdentity extends IdentityBase {
  type: IdentityType.Hfest;
  server: HfestServer;
  hfestId: string;
  username: HfestLogin;
  email?: Email;
  bestScore: number;
  bestLevel: number;
  gameCompleted: boolean;
}

export const $HfestIdentity: RecordIoType<HfestIdentity> = new RecordType<HfestIdentity>({
  properties: {
    ...$AbstractIdentity.properties,
    type: {type: new LiteralType({type: $IdentityType, value: IdentityType.Hfest as IdentityType.Hfest})},
    server: {type: $HfestServer},
    hfestId: {type: new Ucs2StringType({maxLength: 10, trimmed: true, pattern: /^\d+$/})},
    username: {type: $HfestLogin},
    email: {type: $Email, optional: true},
    bestScore: {type: $Uint32},
    bestLevel: {type: $Uint32},
    gameCompleted: {type: $Boolean},
  },
  changeCase: CaseStyle.SnakeCase,
});
