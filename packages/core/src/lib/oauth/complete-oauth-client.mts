import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $ObjectType, ObjectType } from "../types/object-type.mjs";
import { $Url, Url } from "../types/url.mjs";
import { $OauthClientDisplayName, OauthClientDisplayName } from "./oauth-client-display-name.mjs";
import { $OauthClientId, OauthClientId } from "./oauth-client-id.mjs";
import { $OauthClientSecret, OauthClientSecret } from "./oauth-client-secret.mjs";

export interface CompleteOauthClient {
  type: ObjectType.OauthClient;
  id: OauthClientId;
  displayName: OauthClientDisplayName;
  appUri: Url;
  callbackUri: Url;
  secret: OauthClientSecret;
}

export const $CompleteOauthClient: RecordIoType<CompleteOauthClient> = new RecordType<CompleteOauthClient>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.OauthClient})},
    id: {type: $OauthClientId},
    displayName: {type: $OauthClientDisplayName},
    appUri: {type: $Url},
    callbackUri: {type: $Url},
    secret: {type: $OauthClientSecret},
  },
  changeCase: CaseStyle.SnakeCase,
});
