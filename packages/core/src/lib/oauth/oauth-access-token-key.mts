import { $Ucs2String, Ucs2StringType } from "kryo/ucs2-string";

export type OauthAccessTokenKey = string;

export const $OauthAccessTokenKey: Ucs2StringType = $Ucs2String;
