import { Ucs2StringType } from "kryo/ucs2-string";

export type OauthClientId = string;

export const $OauthClientId: Ucs2StringType = new Ucs2StringType({maxLength: 256});
