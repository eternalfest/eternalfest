import { CaseStyle } from "kryo";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $ObjectType, ObjectType } from "../types/object-type.mjs";
import { $Url, Url } from "../types/url.mjs";
import { $OauthClientDisplayName, OauthClientDisplayName } from "./oauth-client-display-name.mjs";
import { $OauthClientId, OauthClientId } from "./oauth-client-id.mjs";

export interface OauthClient {
  type: ObjectType.OauthClient;
  id: OauthClientId;
  displayName: OauthClientDisplayName;
  appUrl: Url;
  callbackUrl: Url;
}

export const $OauthClient: RecordIoType<OauthClient> = new RecordType<OauthClient>({
  properties: {
    type: {type: new LiteralType({type: $ObjectType, value: ObjectType.OauthClient})},
    id: {type: $OauthClientId},
    displayName: {type: $OauthClientDisplayName},
    appUrl: {type: $Url},
    callbackUrl: {type: $Url},
  },
  changeCase: CaseStyle.SnakeCase,
});
