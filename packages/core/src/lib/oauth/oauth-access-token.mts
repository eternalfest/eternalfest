import { CaseStyle } from "kryo";
import { $Uint53 } from "kryo/integer";
import { LiteralType } from "kryo/literal";
import { RecordIoType, RecordType } from "kryo/record";

import { $OauthAccessTokenKey, OauthAccessTokenKey } from "./oauth-access-token-key.mjs";
import { $OauthTokenType, OauthTokenType } from "./oauth-token-type.mjs";

export interface OauthAccessToken {
  accessToken: OauthAccessTokenKey;
  expiresIn: number;
  tokenType: OauthTokenType;
}

export const $OauthAccessToken: RecordIoType<OauthAccessToken> = new RecordType<OauthAccessToken>({
  properties: {
    tokenType: {type: new LiteralType({type: $OauthTokenType, value: OauthTokenType.Bearer})},
    accessToken: {type: $OauthAccessTokenKey},
    expiresIn: {type: $Uint53},
  },
  changeCase: CaseStyle.SnakeCase,
});
