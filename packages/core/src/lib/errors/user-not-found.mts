import incident, { Incident } from "incident";
import { UuidHex } from "kryo/uuid-hex";

export type Name = "UserNotFound";
export const name: Name = "UserNotFound";

export interface Data {
  id: UuidHex;
}

export type Cause = undefined;
export type UserNotFoundError = Incident<Data, Name, Cause>;

export function format({id}: Data): string {
  return `User not found for the ID: ${id}`;
}

export function createUserNotFoundError(id: UuidHex): UserNotFoundError {
  return incident.Incident(name, {id}, format);
}
