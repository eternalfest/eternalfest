import incident, { Incident } from "incident";

export type Name = "Forbidden";
export const name: Name = "Forbidden";

export type Cause = undefined;
export type ForbiddenError<D extends object> = Incident<D, Name, Cause>;

export function createForbiddenError<D extends object>(data: D): ForbiddenError<D> {
  return incident.Incident(name, data);
}
