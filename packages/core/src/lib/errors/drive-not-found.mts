import { Incident } from "incident";
import { UuidHex } from "kryo/uuid-hex";

export type Name = "DriveNotFound";
export const name: Name = "DriveNotFound";

export interface Data {
  id: UuidHex;
}

export type Cause = undefined;
export type UserNotFoundError = Incident<Data, Name, Cause>;

export function format({id}: Data): string {
  return `Drive not found for the ID: ${id}`;
}

export function createDriveNotFoundError(id: UuidHex): UserNotFoundError {
  return Incident(name, {id}, format);
}
