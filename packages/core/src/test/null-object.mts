export function nullObject<T>(obj: T): T {
  const out: object = Object.create(null);
  for (const key of Object.keys(obj as object)) {
    Reflect.set(out, key, Reflect.get(obj as object, key));
  }
  return out as T;
}
