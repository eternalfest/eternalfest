import {describe} from "node:test";

import { $RunSettings } from "../../lib/run/run-settings.mjs";
import { testKryoType } from "../test-kryo.mjs";

describe("RunSettings", function () {
  testKryoType({
    type: $RunSettings,
    valid: [
      {value: {detail: false, shake: false, sound: false, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: true, shake: true, sound: true, music: true, volume: 100, locale: "fr-FR"}},
      {value: {detail: true, shake: false, sound: false, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: true, sound: false, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: true, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: false, music: true, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: false, music: false, volume: 100, locale: "fr-FR"}},
    ],
    invalid: [
      {value: {detail: false, shake: false, sound: false, music: true, volume: -1, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: false, music: true, volume: 101, locale: "fr-FR"}},
      {value: {shake: false, sound: false, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, sound: false, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: false, music: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: false, volume: 0, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: false, music: false, locale: "fr-FR"}},
      {value: {detail: false, shake: false, sound: false, music: false, volume: 0, locale: ""}},
    ],
  });
});
