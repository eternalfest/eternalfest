import {describe} from "node:test";

import { $SetRunResultOptions } from "../../lib/run/set-run-result-options.mjs";
import { testKryoType } from "../test-kryo.mjs";

describe("SetRunResultOptions", function () {
  testKryoType({
    type: $SetRunResultOptions,
    valid: [
      {value: {isVictory: false, maxLevel: 0, scores: [0], items: new Map(), stats: {jumps: 10}}},
    ],
    invalid: [
      {value: {maxLevel: 0, scores: [0], items: new Map()}},
      {value: {isVictory: false, scores: [0], items: new Map()}},
      {value: {isVictory: false, maxLevel: 0, items: new Map()}},
      {value: {isVictory: false, maxLevel: 0, scores: [0]}},
    ],
  });
});
