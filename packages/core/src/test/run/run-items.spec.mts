import {describe} from "node:test";

import { $RunItems } from "../../lib/run/run-items.mjs";
import { testKryoType } from "../test-kryo.mjs";

describe("RunItems", function () {
  testKryoType({
    type: $RunItems,
    valid: [
      // {value: new Map(), rawJson: "{}"},
      // {value: new Map([["1000", 1]]), rawJson: "{\"1000\": 1}"},
    ],
    invalid: [
      {value: new Map([["1000", -1]])},
    ],
  });
});
