import {describe} from "node:test";

import {$CreateGameOptions, CreateGameOptions} from "../../lib/game2/create-game-options.mjs";
import {GameCategory} from "../../lib/game2/game-category.mjs";
import {GameChannelPermission} from "../../lib/game2/game-channel-permission.mjs";
import {GameEngineType} from "../../lib/game2/game-engine-type.mjs";
import {ObjectType} from "../../lib/types/object-type.mjs";
import {nullObject} from "../null-object.mjs";
import {registerJsonIoTests} from "../test-kryo.mjs";

describe("CreateGame", function () {
  registerJsonIoTests<CreateGameOptions>(
    $CreateGameOptions,
    "game/create-game",
    new Map([
      ["sous-la-colline", nullObject({
        owner: nullObject({
          type: ObjectType.User,
          id: "00000000-0000-0000-0001-000000000001"
        }),
        key: "hill",
        build: nullObject({
          version: "1.2.3",
          gitCommitRef: "0123456789abcdef0123456789abcdef01234567",
          mainLocale: "fr-FR",
          displayName: "Sous la colline",
          description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques\u202f!",
          icon: nullObject({
            type: ObjectType.Blob,
            id: "00000000-0000-0000-0002-000000000001"
          }),
          loader: "4.1.0",
          engine: nullObject({
            type: GameEngineType.V96,
          }),
          musics: [],
          contentI18n: nullObject({
            type: ObjectType.Blob,
            id: "00000000-0000-0000-0002-000000000002"
          }),
          patcher: null,
          debug: null,
          content: null,
          modes: new Map([
            ["solo", nullObject({
              displayName: "Aventure",
              isVisible: true,
              options: new Map([
                ["boost", nullObject({
                  displayName: "Tornade",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                })]
              ]),
            })],
            ["multi", nullObject({
              displayName: "Multicoopératif",
              isVisible: true,
              options: new Map([
                ["lifesharing", nullObject({
                  displayName: "Partage de vies",
                  isVisible: true,
                  isEnabled: true,
                  defaultValue: false
                })]
              ]),
            })],
          ]),
          families: "10",
          category: GameCategory.Small,
          i18n: new Map([
            ["en-US", nullObject({
              displayName: "Under the hill",
              description: "Hallmark Eternalfest game for beginners and nostalgics\u202f!",
              contentI18n: nullObject({
                type: ObjectType.Blob,
                id: "00000000-0000-0000-0002-000000000002"
              }),
              modes: new Map([
                ["multi", nullObject({
                  displayName: "Multiplayer",
                  options: new Map([
                    ["lifesharing", nullObject({displayName: "Life sharing"})]
                  ])
                })],
                ["solo", nullObject({
                  displayName: "Adventure",
                  options: new Map([
                    ["boost", nullObject({displayName: "Tornado"})]
                  ])
                })],
              ])
            })]
          ]),
        }),
        channels: [
          nullObject({
            key: "main",
            isEnabled: false,
            defaultPermission: GameChannelPermission.None,
            isPinned: false,
            publicationDate: null,
            sortUpdateDate: null,
            version: "1.2.3",
            patches: [],
          })
        ],
      })],
    ])
  );
});
