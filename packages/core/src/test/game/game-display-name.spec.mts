import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import {readOrThrow} from "kryo";
import { JSON_READER } from "kryo-json/json-reader";

import { $GameDisplayName, GameDisplayName } from "../../lib/game2/game-display-name.mjs";

describe("GameDisplayName", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: GameDisplayName;
    }

    const items: Item[] = [
      {name: "empty string", value: ""},
      {name: "1 space character", value: " "},
      {name: "2 space characters", value: "  "},
      {name: "65 (max + 1) letters", value: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"},
      {
        name: "Non-trimmed name",
        value: "  \n \t Hackfest ",
      },
      {
        name: "64 letters and whitespace",
        value: "  Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa  ",
      },
    ];

    for (const item of items) {
      test(`should reject ${JSON.stringify(item.value)}: ${item.name}`, function () {
        assert.throws(() => readOrThrow($GameDisplayName, JSON_READER, item.value));
      });
    }
  });
  describe("valid", function () {
    interface Item {
      name?: string;
      value: GameDisplayName;
      rawJson: string;
    }

    const items: Item[] = [
      {
        name: "1 letter",
        value: "A",
        rawJson: "\"A\"",
      },
      {
        name: "1 special character",
        value: "?",
        rawJson: "\"?\"",
      },
      {
        name: "64 letters",
        value: "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
        rawJson: "\"Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\"",
      },
    ];

    for (const item of items) {
      test(`should accept ${JSON.stringify(item.value)}: ${item.name}`, function () {
        const actualValue: string = readOrThrow($GameDisplayName, JSON_READER, item.rawJson);
        assert.ok($GameDisplayName.equals(actualValue, item.value));
      });
    }
  });
});
