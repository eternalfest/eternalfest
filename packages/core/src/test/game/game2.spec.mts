import {describe} from "node:test";

import {$Game, Game} from "../../lib/game2/game.mjs";
import {GameCategory} from "../../lib/game2/game-category.mjs";
import {GameChannelPermission} from "../../lib/game2/game-channel-permission.mjs";
import {GameEngineType} from "../../lib/game2/game-engine-type.mjs";
import {ObjectType} from "../../lib/types/object-type.mjs";
import {nullObject} from "../null-object.mjs";
import {registerJsonIoTests} from "../test-kryo.mjs";

describe("Game", function () {
  registerJsonIoTests<Game>(
    $Game,
    "game/game",
    new Map([
      ["beta", nullObject({
        type: ObjectType.Game,
        id: "00000000-0000-0000-0003-000000000002",
        createdAt: new Date("2022-01-01T00:00:02.000Z"),
        key: null,
        owner: nullObject({
          type: ObjectType.User,
          id: "00000000-0000-0000-0001-000000000001",
          displayName: "Alice",
        }),
        channels: nullObject({
          offset: 0,
          limit: 10,
          count: 3,
          isCountExact: false,
          active: nullObject({
            type: ObjectType.GameChannel,
            key: "beta",
            isEnabled: true,
            isPinned: false,
            publicationDate: null,
            sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
            defaultPermission: GameChannelPermission.None,
            build: nullObject({
              createdAt: new Date("2022-01-01T00:00:02.000Z"),
              version: "0.1.2",
              gitCommitRef: null,
              mainLocale: "fr-FR",
              displayName: "Beta",
              description: "Beta game",
              icon: null,
              loader: "4.1.0",
              engine: nullObject({
                type: GameEngineType.V96,
              }),
              patcher: null,
              debug: null,
              content: null,
              contentI18n: null,
              musics: [],
              modes: new Map(),
              families: "1,2,3",
              category: GameCategory.Big,
              i18n: new Map(),
            })
          }),
          items: [
            null,
            nullObject({
              type: ObjectType.GameChannel,
              key: "beta",
              isEnabled: true,
              isPinned: false,
              publicationDate: null,
              sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
              defaultPermission: GameChannelPermission.None,
              build: nullObject({
                version: "0.1.2",
                gitCommitRef: null,
                mainLocale: "fr-FR",
                displayName: "Beta",
                description: "Beta game",
                icon: null,
                i18n: new Map(),
              }),
            }),
            nullObject({
              type: ObjectType.GameChannel,
              key: "dev",
              isEnabled: true,
              isPinned: false,
              publicationDate: null,
              sortUpdateDate: new Date("2022-01-01T00:00:02.000Z"),
              defaultPermission: GameChannelPermission.None,
              build: nullObject({
                version: "0.1.2",
                gitCommitRef: null,
                mainLocale: "fr-FR",
                displayName: "Beta",
                description: "Beta game",
                icon: null,
                i18n: new Map(),
              }),
            }),
          ],
        }),
      })],
    ])
  );
});
