import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import {readOrThrow} from "kryo";
import {JSON_READER} from "kryo-json/json-reader";

import {$GameDisplayName} from "../../lib/game2/game-display-name.mjs";
import {$WikiSlug, WikiSlug} from "../../lib/wiki/wiki-slug.mjs";

describe("WikiSlug", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: WikiSlug;
    }

    const items: Item[] = [
      {value: "/"},
      {value: "/a"},
      {value: "a/"},
      {value: "/a/"},
      {value: "A"},
      {value: "A/B"},
      {value: "a/B"},
      {value: "@"},
      {value: "%"},
      {name: "101 (max + 1) letters", value: new Array(101).fill("a").join("")},
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      test(`should reject ${valueStr}: ${item.name ?? valueStr}`, function () {
        assert.throws(() => readOrThrow($GameDisplayName, JSON_READER, item.value));
      });
    }
  });
  describe("valid", function () {
    interface Item {
      name?: string;
      value: WikiSlug;
      rawJson: string;
    }

    const items: Item[] = [
      {
        name: "empty string",
        value: "",
        rawJson: "\"\"",
      },
      {
        name: "1 letter",
        value: "a",
        rawJson: "\"a\"",
      },
      {
        name: "1 digit",
        value: "0",
        rawJson: "\"0\"",
      },
      {
        name: "1 dash",
        value: "-",
        rawJson: "\"-\"",
      },
      {
        name: "1 dot",
        value: ".",
        rawJson: "\".\"",
      },
      {
        value: "levels/10.8",
        rawJson: "\"levels/10.8\"",
      },
      {
        value: "dimensions/les-landes-violettes",
        rawJson: "\"dimensions/les-landes-violettes\"",
      },
      {
        name: "100 letters",
        value: new Array(100).fill("a").join(""),
        rawJson: `"${new Array(100).fill("a").join("")}"`,
      },
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      test(`should accept ${valueStr}: ${item.name ?? valueStr}`, function () {
        const actualValue: WikiSlug = readOrThrow($WikiSlug, JSON_READER, item.rawJson);
        assert.ok($WikiSlug.equals(actualValue, item.value));
      });
    }
  });
});
