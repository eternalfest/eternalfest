import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import {readOrThrow} from "kryo";
import {JSON_READER} from "kryo-json/json-reader";

import {$GameDisplayName} from "../../lib/game2/game-display-name.mjs";
import {$InputTextRevisionContent, InputTextRevisionContent} from "../../lib/wiki/input-text-revision-content.mjs";
import {WikiContentType} from "../../lib/wiki/wiki-content-type.mjs";

describe("InputTextRevisionContent", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: InputTextRevisionContent;
    }

    const items: Item[] = [
      {
        name: "Eternoel description",
        value: {
          type: WikiContentType.Text,
          title: "",
          body: "Eternoël est la première contrée publiée sur Eternalfest.",
        },
      },
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      test(`should reject ${valueStr}: ${item.name ?? valueStr}`, function () {
        assert.throws(() => readOrThrow($GameDisplayName, JSON_READER, item.value));
      });
    }
  });
  describe("valid", function () {
    interface Item {
      name?: string;
      value: InputTextRevisionContent;
      rawJson: string;
    }

    const items: Item[] = [
      {
        name: "Eternoel description",
        value: {
          type: WikiContentType.Text,
          title: "Contrée - Eternoël",
          body: "Eternoël est la première contrée publiée sur Eternalfest.",
        },
        rawJson: "{\"type\":\"text\",\"title\":\"Contrée - Eternoël\",\"body\":\"Eternoël est la première contrée publiée sur Eternalfest.\"}",
      },
    ];

    for (const item of items) {
      const valueStr: string = JSON.stringify(item.value);
      test(`should accept ${valueStr}: ${item.name ?? valueStr}`, function () {
        const actualValue: InputTextRevisionContent = readOrThrow($InputTextRevisionContent, JSON_READER, item.rawJson);
        assert.ok($InputTextRevisionContent.equals(actualValue, item.value));
      });
    }
  });
});
