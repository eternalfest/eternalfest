import {describe} from "node:test";

import {$LeaderboardEntry} from "../../lib/leaderboard/leaderboard-entry.mjs";
import {ObjectType} from "../../lib/types/object-type.mjs";
import {testKryoType} from "../test-kryo.mjs";

describe("LeaderboardEntry", function () {
  testKryoType({
    type: $LeaderboardEntry,
    valid: [
      {
        value: {
          score: 514233,
          user: {
            type: ObjectType.User,
            id: "00000000-0000-0000-0000-000000000000",
            displayName: "SkyWasHere",
          },
          run: {
            id: "00000000-0000-0000-0000-000000000000",
            maxLevel: 0,
            gameOptions: ["[0]"],
          },
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"score\":514233,\"user\":{\"type\":\"User\",\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"SkyWasHere\"},\"run\":{\"id\":\"00000000-0000-0000-0000-000000000000\",\"max_level\":0,\"game_options\":[\"[0]\"]}}",
      },
    ],
    invalid: [],
  });
});
