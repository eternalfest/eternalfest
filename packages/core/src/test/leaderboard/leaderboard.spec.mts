import {describe} from "node:test";

import {$Leaderboard} from "../../lib/leaderboard/leaderboard.mjs";
import {ObjectType} from "../../lib/types/object-type.mjs";
import {testKryoType} from "../test-kryo.mjs";

describe("Leaderboard", function () {
  testKryoType({
    type: $Leaderboard,
    valid: [
      {
        value: {
          game: {
            id: "00000000-0000-0000-0000-000000000000",
          },
          channel: "main",
          mode: "solo",
          results: [
            {
              score: 500,
              user: {
                type: ObjectType.User,
                id: "00000000-0000-0000-0000-000000000000",
                displayName: "Demurgos",
              },
              run: {
                id: "00000000-0000-0000-0000-000000000000",
                maxLevel: 2,
                gameOptions: [],
              },
            },
            {
              score: 1580250,
              user: {
                type: ObjectType.User,
                id: "00000000-0000-0000-0000-000000000001",
                displayName: "Elagea",
              },
              run: {
                id: "00000000-0000-0000-0000-000000000001",
                maxLevel: 374,
                gameOptions: ["nightmare"],
              },
            },
          ]
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"game\":{\"id\":\"00000000-0000-0000-0000-000000000000\"},\"channel\":\"main\",\"mode\":\"solo\",\"results\":[{\"score\":500,\"user\":{\"type\":\"User\",\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"Demurgos\"},\"run\":{\"id\":\"00000000-0000-0000-0000-000000000000\",\"max_level\":2,\"game_options\":[]}}, {\"score\":1580250,\"user\":{\"type\":\"User\",\"id\":\"00000000-0000-0000-0000-000000000001\",\"display_name\":\"Elagea\"},\"run\":{\"id\":\"00000000-0000-0000-0000-000000000001\",\"max_level\":374,\"game_options\":[\"nightmare\"]}}]}",
      },
    ],
    invalid: [],
  });
});
