import * as assert from "node:assert/strict";
import {describe, test} from "node:test";

import {readOrThrow} from "kryo";
import {JSON_READER} from "kryo-json/json-reader";

import {$UserDisplayName,UserDisplayName} from "../../lib/user/user-display-name.mjs";

describe("UserDisplayName", function () {
  describe("invalid", function () {
    interface Item {
      name?: string;
      value: UserDisplayName;
    }

    const items: Item[] = [
      {name: "empty string", value: ""},
      {name: "4 whites spaces", value: "    "},
      {name: "1 letter", value: "A"},
      {name: "2 letters (ASCII)", value: "Ab"},
      {name: "2 letters (Unicode in BMP)", value: "∑∑"},
      {name: "2 letters (Unicode outside of BMP)", value: "𝄞𝄞"},
      {name: "4 letters (Forbidden ASCII)", value: "!!!!"},
      {name: "4 letters (Unicode in BMP)", value: "éééé"},
      {name: "4 letters (Unicode outside of BMP)", value: "𝄞𝄞𝄞𝄞"},
      {name: "Non-trimmed string with 2 letters", value: " Ab "},
      {name: "65 (max + 1) letters", value: "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"},
      {
        name: "Non-trimmed username",
        value: "  \n \t Demurgos ",
      },
      {
        name: "Only spaces",
        value: "   ",
      },
      {
        name: "64 letters and whitespace",
        value: "  Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa  ",
      },
      {
        name: "Only parenthesis",
        value: "(((",
      },
    ];

    for (const item of items) {
      test(`should reject ${JSON.stringify(item.value)}: ${item.name}`, function () {
        assert.throws(() => readOrThrow($UserDisplayName, JSON_READER, item.value));
      });
    }
  });

  describe("valid", function () {
    interface Item {
      name?: string;
      value: string;
      rawJson: string;
    }

    const items: Item[] = [
      // {
      //   name: "3 letters",
      //   value: "Tom",
      //   rawJson: "\"Tom\"",
      // },
      // {
      //   name: "With spaces and parenthesis",
      //   value: "Lokapi (EternalCool)",
      //   rawJson: "\"Lokapi (EternalCool)\"",
      // },
      {
        name: "Short with parenthesis",
        value: "a((",
        rawJson: "\"a((\"",
      },
      // {
      //   name: "59 letters",
      //   value: "Llanfairpwllgwyngyllgogerychwyrndrobwyllllantysiliogogogoch",
      //   rawJson: "\"Llanfairpwllgwyngyllgogerychwyrndrobwyllllantysiliogogogoch\"",
      // },
      // {
      //   name: "64 letters",
      //   value: "Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa",
      //   rawJson: "\"Aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\"",
      // },
    ];

    for (const item of items) {
      test(`should accept ${JSON.stringify(item.value)}: ${item.name}`, function () {
        const actualValue: string = readOrThrow($UserDisplayName, JSON_READER, item.rawJson);
        assert.ok($UserDisplayName.equals(actualValue, item.value));
      });
    }
  });
});
