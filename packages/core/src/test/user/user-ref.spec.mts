import {describe} from "node:test";

import {ObjectType} from "../../lib/types/object-type.mjs";
import {$UserRef} from "../../lib/user/user-ref.mjs";
import {testKryoType} from "../test-kryo.mjs";

describe("UserRef", function () {
  testKryoType({
    type: $UserRef,
    valid: [
      {
        value: {
          type: ObjectType.User,
          id: "00000000-0000-0000-0000-000000000000",
          displayName: "NotDemurgosForOnce",
        },
        // tslint:disable-next-line:max-line-length
        rawJson: "{\"type\":\"User\",\"id\":\"00000000-0000-0000-0000-000000000000\",\"display_name\":\"NotDemurgosForOnce\"}",
      },
    ],
    invalid: [],
  });
});
