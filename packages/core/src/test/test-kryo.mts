import * as assert from "node:assert/strict";
import {test} from "node:test";

import fs from "fs";
import * as furi from "furi";
import {IoType, readOrThrow, testOrThrow, Type} from "kryo";
import { JSON_READER } from "kryo-json/json-reader";
import { PRETTY_JSON_WRITER } from "kryo-json/json-writer";
import objectInspect from "object-inspect";

const TEST_ROOT = furi.join(import.meta.url, "../../../../test-resources/core");

export interface InvalidValue {
  readonly value: any;
  readonly name?: string;
}

export interface ValidValue<T> {
  readonly value: T;
  readonly rawJson?: undefined;
  readonly name?: string;
}

export interface ValidIoValue<T> {
  readonly value: T;
  readonly rawJson: string;
  readonly name?: string;
}

export interface TestKryoTypeOptions<T> {
  readonly type: Type<T>;
  readonly valid: ReadonlyArray<ValidValue<T>>;
  readonly invalid: ReadonlyArray<InvalidValue>;
}

export interface TestKryoIoTypeOptions<T> {
  readonly type: Type<T>;
  readonly valid: ReadonlyArray<ValidIoValue<T>>;
  readonly invalid: ReadonlyArray<InvalidValue>;
}

export function testKryoType<T = unknown>(options: TestKryoTypeOptions<T> | TestKryoIoTypeOptions<T>): void {
  for (const valid of options.valid) {
    const name: string = valid.name !== undefined ? valid.name : objectInspect(valid.value);
    test(`accepts: ${name}`, () => {
      testOrThrow(options.type, valid.value);
    });

    if (valid.rawJson !== undefined) {
      test(`reads raw JSON: ${name}`, () => {
        const actual: T = readOrThrow(options.type as IoType<T>, JSON_READER, valid.rawJson);
        assert.ok(options.type.equals(actual, valid.value));
      });
    }
  }

  for (const invalid of options.invalid) {
    const name: string = invalid.name !== undefined ? invalid.name : objectInspect(invalid.value);
    test(`rejects: ${name}`, () => {
      assert.ok(!options.type.test(null, invalid.value).ok);
    });
  }
}

export function registerJsonIoTests<T>(type: IoType<T>, group: string, items: ReadonlyMap<string, T>): void {
  const groupeUri = furi.join(TEST_ROOT, group);
  const actualItems: Set<string> = new Set();
  for (const ent of fs.readdirSync(groupeUri, {withFileTypes: true})) {
    if (!ent.isDirectory() || ent.name.startsWith(".")) {
      continue;
    }
    const name = ent.name;
    actualItems.add(name);
    const value: T | undefined = items.get(name);
    if (value === undefined) {
      throw new Error(`ValueNotFound: ${group} -> ${name}`);
    }
    const valuePath = furi.join(groupeUri, name, "value.json");
    test (`Reads ${name}`, () => {
      const valueJson: string = fs.readFileSync(valuePath, {encoding: "utf-8"});
      const actual = readOrThrow(type, JSON_READER, valueJson);
      assert.deepStrictEqual(actual, value);
    });

    test (`Writes ${name}`, () => {
      const expected: string = fs.readFileSync(valuePath, {encoding: "utf-8"});
      const actual = type.write(PRETTY_JSON_WRITER, value);
      assert.deepStrictEqual(actual, expected);
    });
  }
  const extraValues: Set<string> = new Set();
  for (const name of items.keys()) {
    if (!actualItems.has(name)) {
      extraValues.add(name);
    }
  }
  if (extraValues.size > 0) {
    throw new Error(`ExtraValues: ${group} -> ${[...extraValues].join(", ")}`);
  }
}
