import * as etwin from "../lib/etwin.mjs";
import {EternalfestClient, LoginWithEternaltwinResponse, OauthReturnResponse, RequestType} from "../lib/index.mjs";

async function main() {
  // const client = new EternalfestClient(new URL("https://eternalfest.net/"));
  const eternalfest = new EternalfestClient(new URL("http://localhost:50313/"));
  const eternaltwin = new etwin.EternaltwinClient(new URL("http://localhost:50320/"));
  // const user = await eternaltwin.execute({
  //   type: etwin.RequestType.CreateEtwinUser,
  //   username: "alice",
  //   password: Buffer.from("aaaaaaaaaa"),
  //   displayName: "Alice"
  // });
  const {session: etwinSession} = await eternaltwin.execute({
    type: etwin.RequestType.CreateSession,
    login: "alice",
    password: Buffer.from("aaaaaaaaaa"),
  });
  const login: LoginWithEternaltwinResponse = await eternalfest.execute({type: RequestType.LoginWithEternaltwin});
  const token = await eternaltwin.execute({
    type: etwin.RequestType.GetOauthAuthorization,
    session: etwinSession,
    payload: login.getAuthorization,
  });
  const session: OauthReturnResponse = await eternalfest.execute({type: RequestType.OauthReturn, oauthReturn: token.oauthReturn});
  const self = await eternalfest.execute({type: RequestType.GetAuth, session: session.session});
  console.log(self);
}

await main();
