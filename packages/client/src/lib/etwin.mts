import {$RawUserCredentials} from "@eternaltwin/core/auth/raw-user-credentials";
import {
  $RegisterWithUsernameOptions
} from "@eternaltwin/core/auth/register-with-username-options";
import {$User,User} from "@eternaltwin/core/user/user";
import {Axios, AxiosResponse, Method} from "axios";
import {Cookie, CookieAccessInfo, CookieJar} from "cookiejar";
import {IoType, readOrThrow} from "kryo";
import {JSON_VALUE_READER} from "kryo-json/json-value-reader";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import urlJoin from "url-join";

import {RequestOptions} from "./client.mjs";

// export type Agent = superagent.SuperAgent<superagent.SuperAgentRequest>;

export enum RequestType {
  CreateSession,
  CreateUser,
  GetOauthAuthorization,
}

export interface BaseRequest {
  readonly type: RequestType,
  readonly session?: string,
}

export type Request = CreateEternaltwinSession | CreateEternaltwinUser | GetOauthAuthorization;

export interface CreateEternaltwinSession extends BaseRequest {
  readonly type: RequestType.CreateSession,
  readonly login: string;
  readonly password: Uint8Array;
}

export interface CreateEternaltwinSessionResponse {
  readonly session: string;
  readonly user: User;
}

export interface CreateEternaltwinUser extends BaseRequest {
  readonly type: RequestType.CreateUser,
  readonly username: string;
  readonly password: Uint8Array;
  readonly displayName: string;
}

export interface CreateEternaltwinUserResponse {
  readonly session: string;
  readonly user: User;
}

export interface GetOauthAuthorization extends BaseRequest {
  readonly type: RequestType.GetOauthAuthorization,
  readonly payload: URL;
}

export interface GetOauthAuthorizationResponse {
  readonly session: string | undefined;
  readonly oauthReturn: URL;
}

export interface RequestToResponse {
  readonly [RequestType.CreateUser]: CreateEternaltwinUserResponse;
  readonly [RequestType.CreateSession]: CreateEternaltwinSessionResponse;
  readonly [RequestType.GetOauthAuthorization]: GetOauthAuthorizationResponse;
}

export class EternaltwinClient {
  readonly #agent: Axios;
  readonly #apiBase: URL;

  public constructor(apiBase: URL) {
    this.#agent = new Axios({});
    this.#apiBase = apiBase;
  }

  public async execute(req: CreateEternaltwinSession): Promise<CreateEternaltwinSessionResponse>;
  public async execute(req: CreateEternaltwinUser): Promise<CreateEternaltwinUserResponse>;
  public async execute(req: GetOauthAuthorization): Promise<GetOauthAuthorizationResponse>;
  public async execute(req: Request): Promise<unknown> {
    switch (req.type) {
      case RequestType.CreateSession:
        return this.#createSessionWithCredentials(req);
      case RequestType.CreateUser:
        return this.#createEtwinUser(req);
      case RequestType.GetOauthAuthorization:
        return this.#getOauthAuthorization(req);
      default:
        throw new Error("unexpected request type");
    }
  }

  async #createEtwinUser(req: CreateEternaltwinUser): Promise<CreateEternaltwinUserResponse> {
    const {
      session,
      value
    } = await this.#post(["api", "v1", "users"], {
      req: {
        username: req.username,
        password: req.password,
        displayName: req.displayName
      },
      reqType: $RegisterWithUsernameOptions, resType: $User
    });
    if (session === undefined) {
      throw new Error("MissingSessionCookie");
    }
    return {session: session.toValueString(), user: value};
  }

  async #createSessionWithCredentials(req: CreateEternaltwinSession): Promise<CreateEternaltwinSessionResponse> {
    const {
      session,
      value
    }: any = await this.#put(["api", "v1", "auth", "self"], {
      req: {
        login: req.login,
        password: req.password,
      }, reqType: $RawUserCredentials, resType: $User
    });
    if (session === undefined) {
      throw new Error("MissingSessionCookie");
    }
    return {session: session.toString(), user: value};
  }

  async #getOauthAuthorization(req: GetOauthAuthorization): Promise<GetOauthAuthorizationResponse> {
    if (req.payload.hostname !== this.#apiBase.hostname) {
      throw new Error("Invalid hostname");
    }
    const headers = {
      cookie: req.session
    };
    const res: AxiosResponse = await this.#agent.request({
      method: "get",
      url: req.payload.toString(),
      headers,
      maxRedirects: 0,
    });
    if (res.status !== 303) {
      throw new Error(`expected status 303 ("See Other"), got ${res.status}`);
    }
    const redirectUri: string | undefined = res.headers["location"];
    if (redirectUri === undefined) {
      throw new Error("missing `location` header");
    }
    const jar = readCookies(res, req.payload);
    const session: Cookie | undefined = jar.getCookie("sid", new CookieAccessInfo(req.payload.host));
    const oauthReturn = new URL(redirectUri, req.payload);
    return {session: session?.toString(), oauthReturn};
  }

  // async #delete<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Promise<{session: Cookie | undefined, value: Res}> {
  //   return this.#request("delete", route, options);
  // }

  // async #get<Query, Res>(route: readonly string[], options: SimpleRequestOptions<Res> | QueryRequestOptions<Query, Res>): Promise<{session: Cookie | undefined, value: Res}> {
  //   return this.#request("get", route, options);
  // }

  // async #patch<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Promise<{session: Cookie | undefined, value: Res}> {
  //   return this.#request("patch", route, options);
  // }

  async #post<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Promise<{session: Cookie | undefined, value: Res}> {
    return this.#request("post", route, options);
  }

  async #put<Query, Req, Res>(route: readonly string[], options: RequestOptions<Query, Req, Res>): Promise<{session: Cookie | undefined, value: Res}> {
    return this.#request("put", route, options);
  }

  async #request<Query, Req, Res>(method: Method, route: readonly string[], options: RequestOptions<Query, Req, Res>): Promise<{session: Cookie | undefined, value: Res}> {
    const uri: URL = this.#resolve(route);
    const rawReq: object | undefined = options.reqType !== undefined ? options.reqType.write(JSON_VALUE_WRITER, options.req) : undefined;
    const rawQuery: Record<string, string> | undefined = options.queryType !== undefined ? options.queryType.write(JSON_VALUE_WRITER, options.query) : undefined;

    const res: AxiosResponse = await this.#agent.request({
      method,
      url: uri.toString(),
      params: rawQuery,
      headers: {
        "Content-Type": "application/json"
      },
      data: rawReq !== undefined ? JSON.stringify(rawReq) : undefined,
      responseType: "json",
    });

    const jar = readCookies(res, uri);
    const session: Cookie | undefined = jar.getCookie("sid", new CookieAccessInfo(this.#apiBase.host));

    const value = readJsonResponse(options. resType, JSON.parse(res.data));

    return {session, value};
  }

  #resolve(route: URL | readonly string[]): URL {
    if (route instanceof URL) {
      return route;
    } else {
      return new URL(urlJoin(this.#apiBase.toString(), ...route));
    }
  }
}

function readCookies(res: AxiosResponse, url: URL): CookieJar {
  const cookies = res.headers["set-cookie"];
  const jar = new CookieJar();
  if (Array.isArray(cookies)) {
    jar.setCookies(cookies, url.host, url.pathname);
  }
  return jar;
}

export function readJsonResponse<T>(type: IoType<T>, raw: any): T {
  if (typeof raw.error === "string") {
    throw new Error(`server replied with error: ${JSON.stringify(raw)}`);
  } else {
    return readOrThrow(type, JSON_VALUE_READER, raw);
  }
}
