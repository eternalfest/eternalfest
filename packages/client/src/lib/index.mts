import {$AuthContext, AuthContext} from "@eternalfest/core/auth/auth-context";
import {$Blob, Blob} from "@eternalfest/core/blob/blob";
import {$CreateBlobOptions, CreateBlobOptions} from "@eternalfest/core/blob/create-blob-options";
import {$CreateUploadSessionOptions, CreateUploadSessionOptions} from "@eternalfest/core/blob/create-upload-session-options";
import {$UploadBytesOptions, UploadBytesOptions} from "@eternalfest/core/blob/upload-bytes-options";
import {$UploadSession, UploadSession} from "@eternalfest/core/blob/upload-session";
import {$CreateGameOptions, CreateGameOptions} from "@eternalfest/core/game2/create-game-options";
import {$Game, Game} from "@eternalfest/core/game2/game";
import {$GameBuild, GameBuild} from "@eternalfest/core/game2/game-build";
import {$GameChannelPatch, GameChannelPatch} from "@eternalfest/core/game2/game-channel-patch";
import {$InputGameBuild, InputGameBuild} from "@eternalfest/core/game2/input-game-build";
import {$NullableUserIdRef, NullableUserIdRef} from "@eternaltwin/core/user/user-id-ref";
import {Axios, AxiosResponse, Method} from "axios";
import {Cookie, CookieAccessInfo, CookieJar} from "cookiejar";
import {CaseStyle} from "kryo";
import {$Any} from "kryo/any";
import {ArrayType} from "kryo/array";
import {RecordIoType, RecordType} from "kryo/record";
import {JSON_VALUE_WRITER} from "kryo-json/json-value-writer";
import urlJoin from "url-join";

import {QueryRequestOptions, RequestOptions, SimpleRequestOptions} from "./client.mjs";
import {readJsonResponse} from "./etwin.mjs";

export enum RequestType {
  CreateBlob,
  CreateGame,
  CreateGameBuild,
  CreateUploadSession,
  GetAuth,
  GetGame,
  LoginWithEternaltwin,
  OauthReturn,
  UpdateGameChannel,
  UploadBytes,
}

export type Request =
  CreateBlob
  | CreateGame
  | CreateGameBuild
  | CreateUploadSession
  | GetAuth
  | GetGame
  | LoginWithEternaltwin
  | OauthReturn
  | UpdateGameChannel
  | UploadBytes;

export interface BaseRequest {
  readonly type: RequestType,
}

export interface LoginWithEternaltwin extends BaseRequest {
  readonly type: RequestType.LoginWithEternaltwin,
}

export interface LoginWithEternaltwinResponse {
  /**
   * URL for the Eternaltwin OAuth authorization.
   *
   * This is something like `https://eternaltwin.org/oauth/authorize?...`, and expected to be fetched through `GET`.
   */
  readonly getAuthorization: URL;
}

export interface CreateBlob extends BaseRequest {
  readonly type: RequestType.CreateBlob;
  readonly session: string | undefined;
  readonly blob: CreateBlobOptions;
}

export interface CreateBlobResponse {
  readonly session: string | undefined;
  readonly value: Blob;
}

export interface CreateGame extends BaseRequest {
  readonly type: RequestType.CreateGame;
  readonly session: string | undefined;
  readonly game: CreateGameOptions;
}

export interface CreateGameResponse {
  readonly session: string | undefined;
  readonly value: Game;
}

export interface CreateGameBuild extends BaseRequest {
  readonly type: RequestType.CreateGameBuild;
  readonly session: string | undefined;
  readonly game: string;
  readonly build: InputGameBuild;
}

export interface CreateGameBuildResponse {
  readonly session: string | undefined;
  readonly value: GameBuild;
}

export interface CreateUploadSession extends BaseRequest {
  readonly type: RequestType.CreateUploadSession;
  readonly session: string | undefined;
  readonly options: CreateUploadSessionOptions;
}

export interface CreateUploadSessionResponse {
  readonly session: string | undefined;
  readonly value: UploadSession;
}

export interface GetAuth extends BaseRequest {
  readonly type: RequestType.GetAuth;
  readonly session: string | undefined;
}

export interface GetAuthResponse {
  readonly session: string | undefined;
  readonly value: AuthContext;
}

export interface GetGame extends BaseRequest {
  readonly type: RequestType.GetGame;
  readonly session: string | undefined;
  readonly ref: string;
}

export interface GetGameResponse {
  readonly value: Game;
}

export interface OauthReturn extends BaseRequest {
  readonly type: RequestType.OauthReturn;
  readonly oauthReturn: URL;
}

export interface OauthReturnResponse {
  readonly session: string | undefined;
}

export interface UpdateGameChannel extends BaseRequest {
  readonly type: RequestType.UpdateGameChannel;
  readonly session: string | undefined;
  readonly game: string;
  readonly channel: string;
  readonly patches: GameChannelPatch[];
}

export interface UpdateGameChannelResponse {
  readonly session: string | undefined;
}

export interface UploadBytes extends BaseRequest {
  readonly type: RequestType.UploadBytes;
  readonly session: string | undefined;
  readonly uploadSession: string;
  readonly data: UploadBytesOptions;
}

export interface UploadBytesResponse {
  readonly session: string | undefined;
  readonly value: UploadSession;
}

export interface RequestToResponse {
  readonly [RequestType.CreateBlob]: CreateBlobResponse;
  readonly [RequestType.CreateGame]: CreateGameResponse;
  readonly [RequestType.CreateGameBuild]: CreateGameBuildResponse;
  readonly [RequestType.CreateUploadSession]: CreateUploadSessionResponse;
  readonly [RequestType.GetAuth]: GetAuthResponse;
  readonly [RequestType.GetGame]: GetGameResponse;
  readonly [RequestType.LoginWithEternaltwin]: LoginWithEternaltwinResponse;
  readonly [RequestType.OauthReturn]: OauthReturnResponse;
  readonly [RequestType.UpdateGameChannel]: UpdateGameChannelResponse;
  readonly [RequestType.UploadBytes]: UploadBytesResponse;
}

export class EternalfestClient {
  readonly #agent: Axios;
  readonly #apiBase: URL;

  public constructor(apiBase: URL) {
    this.#agent = new Axios({});
    this.#apiBase = apiBase;
  }

  public async execute(req: CreateBlob): Promise<CreateBlobResponse>;
  public async execute(req: CreateGame): Promise<CreateGameResponse>;
  public async execute(req: CreateGameBuild): Promise<CreateGameBuildResponse>;
  public async execute(req: CreateUploadSession): Promise<CreateUploadSessionResponse>;
  public async execute(req: GetAuth): Promise<GetAuthResponse>;
  public async execute(req: GetGame): Promise<GetGameResponse>;
  public async execute(req: LoginWithEternaltwin): Promise<LoginWithEternaltwinResponse>;
  public async execute(req: OauthReturn): Promise<OauthReturnResponse>;
  public async execute(req: UpdateGameChannel): Promise<UpdateGameChannelResponse>;
  public async execute(req: UploadBytes): Promise<UploadBytesResponse>;
  public async execute(req: Request): Promise<unknown> {
    switch (req.type) {
      case RequestType.CreateBlob:
        return this.#createBlob(req);
      case RequestType.CreateGame:
        return this.#createGame(req);
      case RequestType.CreateGameBuild:
        return this.#createGameBuild(req);
      case RequestType.CreateUploadSession:
        return this.#createUploadSession(req);
      case RequestType.GetAuth:
        return this.#getAuth(req);
      case RequestType.GetGame:
        return this.#getGame(req);
      case RequestType.LoginWithEternaltwin:
        return this.#loginWithEternaltwin(req);
      case RequestType.OauthReturn:
        return this.#oauthReturn(req);
      case RequestType.UpdateGameChannel:
        return this.#updateGameChannel(req);
      case RequestType.UploadBytes:
        return this.#uploadBytes(req);
      default:
        throw new Error("unexpected request type");
    }
  }

  async #createBlob(req: CreateBlob): Promise<CreateBlobResponse> {
    const res = await this.#post<unknown, CreateBlobOptions, Blob>(["api", "v1", "blobs"], req.session, {
      req: req.blob,
      reqType: $CreateBlobOptions,
      resType: $Blob,
    });
    return {session: res.session?.toValueString() ?? req.session, value: res.value};
  }

  async #createGame(req: CreateGame): Promise<CreateGameResponse> {
    const res = await this.#post<unknown, CreateGameOptions, Game>(["api", "v1", "games"], req.session, {
      req: req.game,
      reqType: $CreateGameOptions,
      resType: $Game,
    });
    return {session: res.session?.toValueString() ?? req.session, value: res.value};
  }

  async #createGameBuild(req: CreateGameBuild): Promise<CreateGameBuildResponse> {
    const res = await this.#post<unknown, InputGameBuild, GameBuild>(["api", "v1", "games", req.game, "builds"], req.session, {
      req: req.build,
      reqType: $InputGameBuild,
      resType: $GameBuild,
    });
    return {session: res.session?.toValueString() ?? req.session, value: res.value};
  }

  async #createUploadSession(req: CreateUploadSession): Promise<CreateUploadSessionResponse> {
    const res = await this.#post<unknown, CreateUploadSessionOptions, UploadSession>(["api", "v1", "upload-sessions"], req.session, {
      req: req.options,
      reqType: $CreateUploadSessionOptions,
      resType: $UploadSession,
    });
    return {session: res.session?.toValueString() ?? req.session, value: res.value};
  }

  async #getAuth(req: GetAuth): Promise<GetAuthResponse> {
    const res = await this.#get(["api", "v1", "self", "auth"], req.session, {resType: $AuthContext});
    return {session: res.session?.toValueString() ?? req.session, value: res.value};
  }

  async #getGame(req: GetGame): Promise<GetGameResponse> {
    const res = await this.#get(["api", "v1", "games", req.ref], req.session, {resType: $Game});
    return {value: res.value};
  }

  async #updateGameChannel(req: UpdateGameChannel): Promise<UpdateGameChannelResponse> {
    const res = await this.#patch<unknown, UpdateGameChannelBody, unknown>(["api", "v1", "games", req.game, "channels", req.channel], req.session, {
      req: {patches: req.patches},
      reqType: $UpdateGameChannelBody,
      resType: $Any,
    });
    return {session: res.session?.toValueString() ?? req.session};
  }

  async #uploadBytes(req: UploadBytes): Promise<UploadBytesResponse> {
    const res = await this.#patch<unknown, UploadBytesOptions, UploadSession>(["api", "v1", "upload-sessions", req.uploadSession], req.session, {
      req: req.data,
      reqType: $UploadBytesOptions,
      resType: $UploadSession,
    });
    return {session: res.session?.toValueString() ?? req.session, value: res.value};
  }

  async #loginWithEternaltwin(_req: LoginWithEternaltwin): Promise<LoginWithEternaltwinResponse> {
    const getAuthorization: URL = await this.#postForm(["actions", "login", "etwin"]);
    return {getAuthorization};
  }

  async #oauthReturn(req: OauthReturn): Promise<OauthReturnResponse> {
    if (req.oauthReturn.hostname !== this.#apiBase.hostname) {
      throw new Error("Invalid hostname");
    }
    const res: AxiosResponse = await this.#agent.request({
      method: "get",
      url: req.oauthReturn.toString(),
      maxRedirects: 0,
    });
    if (res.status !== 302) {
      throw new Error(`expected status 302 ("Found"), got ${res.status}`);
    }
    const redirectUri: string | undefined = res.headers["location"];
    if (redirectUri === undefined) {
      throw new Error("missing `location` header");
    }
    const jar = readCookies(res, req.oauthReturn);
    const session: Cookie | undefined = jar.getCookie("ef_sid", new CookieAccessInfo(req.oauthReturn.host));
    return {session: session?.toValueString()};
  }

  async #postForm(
    url: readonly string[],
  ): Promise<URL> {
    const res: AxiosResponse = await this.#agent
      .request({
        method: "post",
        url: this.#resolve(url).toString(),
        maxRedirects: 0,
      });
    if (res.status !== 302) {
      throw new Error(`expected status 302 ("Found"), got ${res.status}`);
    }
    const redirectUri: string | undefined = res.headers["location"];
    if (redirectUri === undefined) {
      throw new Error("missing `location` header");
    }
    return new URL(redirectUri);
  }

  async #get<Query, Res>(route: readonly string[], auth: string | undefined, options: SimpleRequestOptions<Res> | QueryRequestOptions<Query, Res>): Promise<{ session: Cookie | undefined, value: Res }> {
    return this.#request("get", route, auth, options);
  }

  async #patch<Query, Req, Res>(route: readonly string[], auth: string | undefined, options: RequestOptions<Query, Req, Res>): Promise<{ session: Cookie | undefined, value: Res }> {
    return this.#request("patch", route, auth, options);
  }

  async #post<Query, Req, Res>(route: readonly string[], auth: string | undefined, options: RequestOptions<Query, Req, Res>): Promise<{ session: Cookie | undefined, value: Res }> {
    return this.#request("post", route, auth, options);
  }

  async #request<Query, Req, Res>(method: Method, route: readonly string[], auth: string | undefined, options: RequestOptions<Query, Req, Res>): Promise<{ session: Cookie | undefined, value: Res }> {
    const uri: URL = this.#resolve(route);
    const rawReq: object | undefined = options.reqType !== undefined ? options.reqType.write(JSON_VALUE_WRITER, options.req) : undefined;
    const rawQuery: Record<string, string> | undefined = options.queryType !== undefined ? options.queryType.write(JSON_VALUE_WRITER, options.query) : undefined;

    const res: AxiosResponse = await this.#agent.request({
      method,
      url: uri.toString(),
      params: rawQuery,
      headers: {
        "Content-Type": rawReq !== undefined ? "application/json" : undefined,
        cookie: auth,
      },
      data: rawReq !== undefined ? JSON.stringify(rawReq) : undefined,
      responseType: "json",
    });

    const jar = readCookies(res, uri);
    const session: Cookie | undefined = jar.getCookie("ef_sid", new CookieAccessInfo(this.#apiBase.host));

    const value = readJsonResponse(options.resType, JSON.parse(res.data));

    return {session, value};
  }

  #resolve(route: URL | readonly string[]): URL {
    if (route instanceof URL) {
      return route;
    } else {
      return new URL(urlJoin(this.#apiBase.toString(), ...route));
    }
  }
}

function readCookies(res: AxiosResponse, url: URL): CookieJar {
  const cookies = res.headers["set-cookie"];
  const jar = new CookieJar();
  if (Array.isArray(cookies)) {
    jar.setCookies(cookies, url.host, url.pathname);
  }
  return jar;
}

export interface UpdateGameChannelBody {
  actor?: NullableUserIdRef;
  patches: GameChannelPatch[];
}

export const $UpdateGameChannelBody: RecordIoType<UpdateGameChannelBody> = new RecordType<UpdateGameChannelBody>({
  properties: {
    actor: {type: $NullableUserIdRef, optional: true},
    patches: {type: new ArrayType({itemType: $GameChannelPatch, maxLength: 100})},
  },
  changeCase: CaseStyle.SnakeCase,
});

