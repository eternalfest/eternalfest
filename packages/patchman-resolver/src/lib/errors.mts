import { Url } from "@eternalfest/core/types/url";

export class PackageNotFound extends Error {
  /**
   * Full name of the missing package
   */
  packageName: string;

  /**
   * Base URI used to import the package
   */
  importerUri: Url;

  cause: Error;

  constructor(packageName: string, importerUri: Url, cause: Error) {
    super();
    this.packageName = packageName;
    this.importerUri = importerUri;
    this.cause = cause;
    this.message = `Failed to resolve ${packageName} from ${importerUri}`;
  }
}

/**
 * The package was found but failed validation
 */
export class InvalidPackage extends Error {
  packageName: string;

  uri: Url;

  packageJsonUri: Url;

  importerUri: Url;

  constructor(packageName: string, uri: Url, packageJsonUri: Url, importerUri: Url) {
    super();
    this.packageName = packageName;
    this.uri = uri;
    this.packageJsonUri = packageJsonUri;
    this.importerUri = importerUri;
  }
}

export class DependencyResolutionMaxDepth extends Error {
  constructor() {
    super();
    this.message = "Reached maximum dependency resolution depth";
  }
}

export class DependencyResolutionMaxIterLimit extends Error {
  constructor() {
    super();
    this.message = "Reached maximum dependency resolution iteration limit";
  }
}

export class PackageRootNotFound extends Error {
  uri: Url;

  constructor(uri: Url) {
    super();
    this.uri = uri;
    this.message = `Failed to resolve package root from ${uri}`;
  }
}
