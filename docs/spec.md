# Spécification

Le but de la première version du site `play.eternalfest.net` est de permettre aux joueurs de lier
leur compte Hammerfest avec Eternalfest afin de faire des sauvegardes de leur progression.
Sur la fin de la première version, nous devons aussi avoir un système de gestion des droits
permettant de jouer à Eternalfest sur `play.eternalfest.net` (sans avoir besoin d'installer de
script) si on est membre d'Eternalfest.

## Design

L'idée est d'avoir un site qui puisse correspondre à une "mise à jour" du site officiel.
Au niveau du design, il s'agirait de s'assurer que le site est au moins un peu responsive pour
fonctionner sur mobile sans dégradation majeure (par exemple pour pouvoir utiliser le forum,
dans une version future).

Pour les couleurs, on utilisera les tons d'Eternalfest:
- Rouge clair: `#EF3D23`
- Rouge: `#A81E22`
- Rouge sombre: `#6A100E`
- Gris sombre: `rgba(16, 16, 16, 0.9)`

Pour l'arrière plan, l'idée serait de s'inspirer de la transition qui a eu lieu sur Muxxu
(passage de Muxxu classique à la version intégrée à Twinoid: le fond "ciel ensoleillé" est devenu
un fond "ciel étoilé). Pour ça, on pense s'inspirer du [niveau 0 de la contrée
"Eternalfest"](./media/lvl-0-eternalfest.png).

Si aucun compte Hammerfest n'est lié, les icônes Frigo et quêtes ont un cadenas et redirigent
vers la page de liaison des comptes.

### Page d'accueil 

la page d'acceuil d'Eternalfest sera constuitué à la manière de hammerfest avec un bandeau faisant passer Eternalfest 
pour un style de mçj de hammerfest, avec :
- Un bloc descriptif expliquant ce qu'est Eternalfest et son but.
- Un bouton inscription et un bouton connexion. (définir une maquette)

### Page profil
```TODO
à définir  
  
Objectif à long-terme: ne pas avoir plusieurs sites, mettre en place une gestion des droits.
```

## Comptes

L'idée principale est que les joueurs se sont _déjà_ inscrits sur _hammerfest.fr_ donc il n'ont
pas besoin de se réinscrire pour accéder à Eternalfest. Le but est d'avoir un système similaire
à StackExchange où il est possible d'avoir un compte "natif" ou bien se connecter grâce à un
service externe. Stack Exchange permet soit d'utiliser un couple email / mot de passe, soit se
connecter par Google, Github, etc. Pour nous, ce serait soit une connexion par identifiant / mot
de passe Eternalfest, soit en utilisant un compte déjà existant sur _hammerfest.fr_, _hfest.net_
ou _hammerfest.es_ (dans une version future, nous aimerions supporter les comptes _twinoid.com_).

D'autres exemples de sites permettant ce type de connexion:
- Twinoid: Google, Facebook, Microsoft, Amazon. Natif: email / mot de passe (email non vérifier à
  l'inscription, possibilité d'utiliser un nom d'utilisateur à la place de l'email).
- Gitlab
- Mozilla Developper Network

### Exemples

### Joueur d'Hammerfest, première connexion à Eternalfest

Sur la page principale, un bouton `Connexion` permet d'ouvrir une fenêtre pop-in (cf. Twinoid).
Cette fenêtre est composée de deux onglets: `Eternalfest` et `Hammerfest` pour utiliser
respectivement un compte Eternalfest ou Hammerfest. L'onglet Hammerfest est ouvert par défaut.

Dans l'onglet Hammerfest, on a trois champs:
- Serveur: _hammerfest.fr_, _hfest.net_ ou _hammerfest.es_. _hammerfest.fr_ pré-sélectionné
- Identifiant
- Mot de passe: Petit bouton `i` (informations), au survol: explique que le mot de passe n'est pas
  sauvegardé sur nos serveurs, au clic: page avec plus d'infos ?

Et un bouton "Connexion".

En bas, on a deux liens: "Mot de passe oublié" et "Inscription", les deux redirigent vers la page
correspondante du site officiel (serveur en fonction du champs "serveur" actuellement sélectionné).

Une fois le formulaire soumis, le serveur utilise l'API Hammerfest pour tenter de se connecter
avec les identifiants du joueur sur le serveur choisi. En cas d'échec, un message d'erreur est
retourné expliquant le problème.
En cas de succès, le serveur Eternalfest valide l'authentification:

1. Il vérifie si le couple serveur / identifiant Hammerfest a déjà été utilisé (lié à un compte
   Eternalfest). Si le compte Hammerfest est déjà lié à un compte Eternalfest, l'authentification
   se poursuit simplement pour ce compte Eternalfest. Si le compte Hammerfest n'est pas connu
   (première utilisation), un nouveau compte Eternalfest créé et le compte Hammerfest y est
   automatiquement lié. Ce nouveau compte est initialisé sans identiants Eternalfest, un compte
   Hammerfest lié (on stocke l'identifiant / ID et le serveur) et comme nom affiché, on utilise
   l'identifiant Hammerfest.
2. Les données relatives aux compte Hammerfest sont mises à jour: inventaire du joueur.

Notes:
- On ne garantit pas l'unicité du nom affiché car des joueurs différents peuvent utiliser des noms
  identiques sur des serveurs différents.
- Si le joueur supprime son compte Hammerfest, on affiche simplement une erreur (l'API ne pourra
  pas se connecter de toute manière).
- Si le joueur change de mot de passe Hammerfest, il peut simplement utiliser son nouveau mot de
  passe pour se connecter (pas de problème de ce point de vue là: le mdp est renvoyé à chaque fois
  vers le serveur d'Hammerfest).

Si la seule méthode de connexion au compte Eternalfest est le compte Hammerfest, une notification
en haut de l'écran (cf. la librairie de notification Toastr) invite le joueur à ajoute un couple
identifiant / mot de passe Eternalfest, permettant une connexion indépendante du serveur
Hammerfest.

###  Inscription sur Eternalfest

Il est possible de créer un compte Eternalfest "natif" utilisant un couple identifiant / mot de
passe propre au serveur Eternalfest. Ce type de compte a l'avantage de ne pas nécessiter de
requête vers des serveur externes.

Sur la page principale, nous avons un bouton `Inscription` qui ouvre une fenêtre pop-in.

Cette fenêtre demande de renseigner les champs suivants:

- Identifiant (nom d'utilisateur)
- Mot de passe
- Confirmation du mot de passe
- Email: optionnel, non vérifié par le serveur (similaire à Hammerfest ou Twinoid)
  (Dans le futur on pourra ajouter une confirmation de l'email, mais ce n'est pas quelque chose
  de prévu actuellement). Une icône information explique que renseigner l'email permet de retrouver
  son accès à Eternalfest en cas d'oubli de mot de passe.

Si l'identifiant est disponible, un compte est créé. Ce compte a un couple identifiant / mot de
passe Eternalfest mais aucun compte Hammerfest lié. Le nom affiché prend la valeur de l'identifiant
Eternalfest.

_Idée_: permettre de lier son compte lors de l'inscription ? (Dans un premier temps, non)

### Connexion avec un compte Eternalfest

L'utilisateur choisi le bouton connexion de la page principale, prend l'onglet "Eternalfest" et
renseigne ses identifiants. La procédure de connexion est celle d'un site web classique.

En cas d'erreur (compte non trouvé, mauvais mot de passe), demander à l'utilisateur de s'assurer
qu'il ne devrait pas utiliser une connexion par Hammerfest car il n'a pas défini de couple
identifiant / mot de passe Eternalfest.

## Lier des comptes

L'utilisateur dispose d'une page pour gérer les paramètres de son compte. Cette page contient
une section dédiée à la gestion des comptes. Parmi les autres options, il peut changer son nom
affiché ou son adresse email.

### Eternalfest

- Si l'utilisateur n'a pas défini de couple identifiant / mot de passe pour se connecter de manière
directe par Eternalfest, la section "Eternalfest" propose d'effectuer cette liaison. Cette section
contient un message insistant sur l'importance de configurer cette méthode de connexion.

- Sinon, si l'utilisateur a déjà défini ses identifiants "natifs", cette section propose de mettre
  à jour le mot de passe.

L'identifiant utiliser pour la connextion "Eternalfest" n'est pas modifiable (_il peut le devenir
dans le futur pour imiter Twinoid, ce n'est pas un objectif actuellement mais il faut éviter de
dépendre sur sa valeur et plutôt préférer utiliser l'ID de l'utilisateur).
Une fois configurée, il est impossible de supprimer la méthode de connexion natives.

### Hammerfest

Prévoir trois boutons, un par serveur (ne pas encourager le multi-compte), ils permettent
de configurer le compte pour un serveur donner (fonctionnement par onglet ou accordion, pas de
fenêtre pop-in).

- Si le compte est non lié, la liaison du compte est similaire à une procédure de connexion par
  compte Hammerfest mais au lieu de créer un nouveau compte Eternalfest, il va lier le compte
  Hammerfest à l'utilisateur actif. Si le compte Hammerfest est déjà lié à un compte Eternalfest,
  l'opération échoue (le compte n'est pas délié puis relié automatiquement).

- Si le compte est déjà lié, le bouton propose de délier le compte Hammerfest. Il n'est pas
  possible de délier un compte Hammerfest si c'est la dernière méthode de connexion au compte.
  Quand le compte est délié, il est également supprimé de notre base de donnée (informations
  privées et progression), on ne veut pas garder les informations sur un joueur qui choisi de
  délier son compte (droit à l'oubli).

**Scénario d'exemple**:
- Feraz a un compte Hammerfest `feraz@hfest`.
- Demurgos obtient le mot de passe du compte `feraz@hfest`.
- Demurgos lie le compte `feraz@hfest` à Eternalfest à l'insu de Feraz.
- Demurgos ajoute un identifiant / mot de passe Eternalfest à ce compte: `demurgos@efest`.
- Feraz tente de se connecter sur Eternalfest grâce à son compte Hammerfest `feraz@hfest`.
- Feraz arrive sur le compte avec l'identifiant Eternalfest déjà défini (`demurgos@efest`).
- Feraz peut alors délier le son compte Hammerfest puis se reconnecter avec ce compte pour créer
  un nouveau compte Eternalfest personnel et configurer les identifiants "natifs" `feraz@efest`.

Cette résolution possible car il est possible de délier les comptes sans demander de mot de passe.

### Résolution des conflits

| Utilisateur | Identifiant Eternalfest | Identifiant hammerfest.fr | Idenfiant hfest.net |
|-------------|-------------------------|---------------------------|---------------------|
| A           | a_ef                    |                           | a_net               |
| B           |                         | b_fr                      |                     |
| C           |                         | c_fr                      | c_net               |

Si **A** essaie de lier le compte **b_fr**, il obtient une erreur car le compte est déjà lié à
Eternalfest. Pour régler cette situation, il doit se connecter à **B** par **b_fr** supprimer
son compte Eternalfest et ainsi libérer les comptes liés puis se connecter à **A** et effectuer
la liaison.

Il doit donc être possible de supprimer son compte Eternalfest. Il est important que l'écran
de désinscription précise que les comptes Hammerfest seront déliés (et pourront donc être liés à
un autre compte).

### Restreindre les comptes permettant l'authentification

Dans le futur, il pourrait être intéressant de différencier les comptes liés permettant de
s'authentifier et ceux qui ne sont là que pour sauvegarder les données ou référencer son profil.
Il serait alors d'avoir lier des comptes Hammerfest, mais forcer la connexion par un compte
Eternalfest pour des raisons de sécurité (si on considère que le compte Hammerfest a plus de
chances d'être volé). Les bénéfices de ce type de restriction dépendent de si le hack du compte
Hammerfest arrive avant ou après avoir utilisé Eternalfest.

## Développment et déploiement

Le site sera développé et testé en local, par Feraz et Demurgos.

Quand le site sera prêt à être mis en ligne, il passera d'abord par une phase _beta_ pour les
membres de la Cachette des Carotteux. Le site complet sera protégé par une simple authentification
HTTP avec un nom de compte et mot de passe partagé par tous les joueurs.

Une fois la bêta finie, l'authentification HTTP sera retirée (accès public).

## Profil public

Bien qu'Hammerfest ne permette pas d'avoir accès aux détails du frigo, nous prévoyons d'ajouter
des informations au profil public des joueurs: le frigo et l'avancée des quêtes.

Le profil aurait donc au moins trois "pages" ou sections: résumé (profil officiel), détail des
quêtes, détails du frigo.

Point d'attention : un gros effort devras être fait sur l'ergonomie en effet un joueur poura avoir 3 profils différent (fr en es) lors de la v1.

## Droits

Ici il est question de définir un certain nombres de droit afin de restreindre l'accès au information en fonction de la personne connecté.
Afin de construire le site dans un perspective d'évolution il sera aussi mentionné les différents droit qui arriverons avec les versions superieure d'Eternalfest.

v1 Sois celle lors de l'ouverture (a minima de la beta) 
v1.1 corespond a la v1 + la possibilité de jouer a EternalFest


| V1                                 | V1.1                                 | V2                        | 
|------------------------------------|--------------------------------------|---------------------------|
| Accèder à ca page gérer son compte | Jouer à Eternalfest (priorité basse) | Bannir un joueur du forum |
| Consulter son "frigo"              |                                      | Consulter le forum        |
| Consulter les "frigo "             |                                      |                           |
| Consulter son "profil"             |                                      |                           |
| Consulter les "profil"             |                                      |                           |


## Rôles

- personne nom connecté qui arrive sur Eternalfest (anonyme /guest) 
- Personne connecté sur Eternalfest (user)
- Personne connecté avec des droits supérieur (carroteux) 
- Personne possédant tout les droits (admin)

## Base de donnée

La base de donné utilisé seras : PostreSQL
Voici un début de schéma de base de donnée :
- Item(**id**, ...)
- EternalAccount(_userId_, login, password)
- HammerfestAccount(**id**, _userId_ (null ou défini), fr, 128, feraz, pseudo_hammerfest, best_level, pantheon_message) : 0-3 ligne par joueur qui a lié ces compte hammerfest
- Inventory(hfest_id, item_id, qty) Table de jointure entre un site_hammerfest lié et les objets afin de savoir les objet obtenu par le joueur sur ce site_hammerfest
- User(**id**, displayName) : une ligne par utilisateur dans la base de donnée on y stockera ici les infomrations propre a un joueur (indépendament des compte lié)

A gérer:

- Utilisateur: Profil + comptes
- Frigo = items pour la backup
- Quêtes




