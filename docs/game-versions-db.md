# Game versions (DB)

How to represent game versions in the DB.

`GameVersion`: Immutable, game at a given point
`GameChannel`: An independent environment/namespace for runs and progression in a game

A game channel always has an active version. You can schedule versions.

<https://en.wikipedia.org/wiki/Temporal_database>

We need a "bitemporal table":
- system/transaction time: when the scheduling request was handled by the server
- application/valid time: when the scheduled changes runs


```


```
