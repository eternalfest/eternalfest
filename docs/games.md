# Games model

A `Game` is modeled with:
- general metadata: display name, thumbnail, description, creator, owner
- channels: channels control access permissions and schedule version changes
- versions: game content at a given point in time: game-engine, patcher, schema for end-result data

## Channel

Channels are identified by their key.

~~When a game is created, it has two channels:
- `stable`: initially empty, publicly available
- `dev`: enabled, only available to the owner~~

When a game is created, it has a single channel `main`, only available to the owner.

Owners may create extra channels.

Channels can be enabled/disabled. This is a global switch
for permissions, it does delete the channel.
Channels can be archived but never deleted.

They are ordered by priority, the first available one is
picked by default.

Inventories and stats are displayed/tracked per-channel.

### Permissions

Two levels: game or channel.
At the start, the game-level permissions are not configurable: the owners and admins have full permission, nothing else.

Does a channel always need an active game revision? Probably? Let's say yes.

## Game version

A game version defines:
- game content (levels, musics, patcher), content may be localized
- items, quests, stats schema
- migration rules to import data from older versions
- aggregation rules for stats
- policy to allow/deny concurrent runs

A version must define a default locale, it must be complete.
Extra locales may be defined, they are allowed to be partial (fallback to default).

# State

Eternalfest enables each game to store custom per-user state as a JSON-like
document.

The state is processed using an **event sourcing model**: all the events are stored
and can be replayed: no information is lost. This allows games to submit updates
and fix issues with older results (invalidate a run, replace an item id, etc.)

Runs can be seen as the following non-atomic programs:
```
1. Read initial state
2. Play
3. Update state
```

## Wanted properties

### Run result page

- The run result page should agree with the run.
  - If a run detects that a quest will be complete at the end, the quest should
    be displayed as complete in the result.
  - In general, a run result page should be close to what happened during the
    game and act as a summary. In the limit, imagine that the run result
    provides a replay for the run.
- A run result page should not change over time.
  - When returning to a result page, after any time, it should display the same
    result. This allows to use result pages as proofs for results.
  - In particular, it allows to generate notifications when unlocking items
    or completing quests with a link to the run.
  - The result **page** should not change, but the contribution of the run
    towards the progress in the latest version may change over time (e.g. to
    support migrations).
- A user should not miss when he gets a reward (item unlock / quest complete).
  - Quests should only be completed when a run ends (and be displayed in the
    run result).
  - We may eventually a reward screen before starting a run to deal with
    updates (lower priority).

### Quests

(Note: quests and achievements are treated as synonyms)

- Users should have an interface to see their quest progression.
- Quests are not only "completed or not", they can also be "in progress"
  (or other states)
- Support hidden/internal quests? (Configure display)
- Progressive reveal: quests should be revealed as the user plays (not all at
  once).
- Quests can progress independently by default.
- Quest progress should not regress during normal play (no game updates).

### Migrations

- When updating games, it should be possible to configure how older runs
  contribute to the state in the final version.
  - Support item id changes
  - Convert hidden item ids to stats (e.g. for Hackfest)
  - Ignore
- Changes are fully supported, they should not require manually cleaning the DB.

### Miscellaneous

- Some top-secret games may want to experiment with complex state handling that
  requires exclusive access.
  - Example: consumables (prevent double-spend)
- Everything should fit nicely with leaderboards (requires cross-player
  interactions).
- Server-wide quests? Multiple players can contribute.
- Every text should be translatable (even in images).
- The server can't go into an infinite loop or exhaust RAM when handling state.

## Conclusions

### Run results

- Once a run is _started_, it behaves the same whether there is a concurrent
  update or not.
  - In particular, it uses the config (quests, items) for version used to start
    the run, even if a new update is available.

### Quests

- Quests have a list of requirements that the server can understand and display.
  - A requirement has the form `max(stat) >= target` (with special cases for
    `min(stat) <= target` and `is_true(stat)`). This allows to display progress
    such as "7/15 items".
  - A quest progresses whenever the best value changes.
- If requirements are more complex, they can always be handled with a single
  "complete the quest" requirement and you compute the state dynamically. It's
  worse for the UI but is not blocking.
- Quests have 4 states:
  - None: No progress at all
  - Pending: Quest started, some progress
  - Ready: All requirements are met, the quest will be complete on the next run.
  - Complete: The quest is done.

### Migrations

When a run ends, the server stores the raw result sent by Flash as-is, without
any modification. Before being processed, these raw run results must be
converted to a normalized form corresponding to the latest version.

- Games must provide a normalization function to convert raw run results.
- This function can handle migration from old results (example: rename ids).
- This is a total function (always returns a deterministic value).
- The format is not fixed yet. Some propositions:
  - Custom declarative format: arrays of transformations (filter + operation)
  - Dhall script (most likely requires a custom impl for our use case)
  - JS function executed by the QuickJS VM

## Scenarios

### Concurrent runs

#### Example

1. Version 1 is released
2. Run A1 starts
3. Run B1 starts
4. Run A1 ends
5. Run B1 ends

Note that the official game allows submitting results at a later time to handle
internet issues.

#### Expected

1. The runs are both started from the initial state.
2. The results are then applied as A1 followed by B1.

#### Conclusion 1

**Runs do not lock the state.** Non-careful access may cause race conditions.
However, the state may be locked punctually to perform atomic updates.

#### Conclusion 2

**It is safer to submit updates rather than writes.** Suppose we want to count
the total number of items. During each run, the player finds 10 items. The
initial state corresponds to 0 items.
- With writes, each run issues: "write 10 to the total item count" and the
  final state is "10 items" instead of the expected "20".
- With updates, each run issues: "add 10 to the total item count" and the
  updates are applied atomically. The final state is "20" as expected.

#### Conclusion 3

**Runs can't assume they are unique.**

The following gameplay is not possible:
> The very first run of the player should play a cinematic. In the following
> runs, there is is no cinematic. This cinematic is only played once.

It is not possible to uniquely identify the first run.

#### Workaround 1

Allow multiple updates per run: e.g. at the start of the run, while the run
is playing, at the end. This provides additional synchronization points.

It is possible to run an update at the run start when player clicks to start
the run. Introducing synchronization during gameplay and at the end requires
internet access.

#### Workaround 2

Add opt-in to disable concurrent runs. When starting a new run check that there
is no pending runs. Add an option to force-start the run (the old run will
be discarded if it ever completes).

This does not fit well for Eternaltwin stats but may be OK for Eternalfest.

### Out-of-order runs

1. Version 1 is released
2. Run A1 starts
3. Run B1 starts
4. Run B1 ends
5. Run C1 starts
6. Run C1 ends
7. Run A1 ends

Expected:
1. The runs A1 and B1 are started from the initial state, C1 starts from B1.
2. The results are then applied as B1, C1, A1.

### Cross-version run

1. Version 1 is released
2. Run A1 starts
3. Version 2 is released
4. Run A1 ends

Expected:
1. The run is started from the initial state.
2. The results are then applied as B1 followed by A1.

### Cross-version out-of-order runs

1. Version 1 is released
2. Run A1 starts
3. Version 2 is released
4. Run B2 starts
5. Run B2 ends
6. Run A1 ends

Expected:
1. The runs are both started from the initial state.
2. The results are then applied as B1 followed by A1.

### Run creation / Run start

1. Version 1 is relased
2. The players picks its game mode and options and clicks "play" in the form
3. The run is created in the database
4. The player loads the game
5. Version 2 is released
6. The player clicks to _start_ the game.

#### Expected

Failed to start the run: this version is no longer available. Please start a
new run.

## Changing requirements (progress loss)

```
1. Version 1: 10 items for the quest
2. Run A1 starts
3. Run A1 ends: 15 items => quest complete
4. Screenshot/share on discord
5. Version 2: the quest now requires 20 items
6. Return to the A1 run result page
```

### Expected

During `3-5`, the quest is completed at `A1`. From `6`, the quest is marked as
incomplete.

## Changing requirements (no progress loss)

```
1. Version 1: 10 items for the quest
2. Run A1 starts
3. Run A1 ends: 15 items => quest complete
4. Run B1 starts
5. Run B1 ends: 15 more items items (30 in total)
6. Version 2: the quest now requires 20 items
```

### Expected

During `3-5`, the quest is completed at `A1`. From `6`, the quest is marked as
completed at `B1`.

## Changing requirements (progress loss and broken causality)

```
1. Version 1: 10 items for the quest
2. Run A1 starts
3. Run A1 ends: 15 items => quest complete, unlocks rare items
4. Run B1 starts
5. Run B1 ends: 1 rare item found
6. Version 2: the quest now requires 20 items
```

#### Expected

The player keeps his rare item, even if the quest is no longer complete.

Note: migrations are free to chose to remove the rare item from the run `B1`,
but the server has no say in this.

### Update changes quests for concurrent run

```
1. Version 1 with the quests:
   - alpha requires itemA × 10
   - beta requires itemB × 20
2. Run A1 starts
1. Version 2 with the quests:
   - alpha requires itemA × 20
   - beta requires itemB × 10
4. Run A1 ends: itemA × 15, itemB × 15
```

#### Expected

```
5. A1 result: the quest alpha is completed, the quest beta progressed (15/20)
6. In the inventory, the user has 15 of each item
7. In his profile
   - the quest alpha is incomplete at 15/20 itemA
   - the quest beta is ready (not complete) at 10/10 itemB
8. As soon as the player starts **end ends** a run in version 2, the quest
   beta goas from "ready" to "completed"
```

##### Conclusion

If players can only end quests at the end of runs, then updates can place quests
in a critical state where it is ready but not completed yet: the player has to
end a run to complete the run.

### Requirements change, with progress loss

```
1. v1: quest requires 15 items
2. run A1: 10 items
3. run B1: 10 items
4. run C1: 10 items
5. v2: quest requires 25 items
6. run D2: 10 items
```

```
completed_by: C1
notified_for: B1
```

# Progression

Progression describes how a player advances **through multiple runs for a single
fixed game release**. The interaction between progression and new releases
is described in the `Migrations` section, but it is a separate independent
layer.

The player progression at a given point is described by a set of named monotonic
values such as:

```
# Total number of items
items.1 = 0
items.2 = 7
items.3 = 16
# Custom stats
has_found_secret1 = false
has_found_secret2 = true
shortest_time_to_beat_boss = 3min15s
longest_survival_time_in_challenge_room = 42s
shortest_path_through_labyrinth = 16
```

A progression value can either be an integer, a boolean or a duration. Values
are monotonic, they can only change in one direction: always increase or always
decrease.

This means that a player progression can only move forward.

Progress diagram:

```mermaid
flowchart TB
    subgraph v3
    subgraph v2
    subgraph v1
    a1
    b1
    end
    c2
    d2
    end
    e3
    end
    a3*
    b3*
    c3*
    d3*
    e3*
    a1-->a3*
    b1-->b3*
    c2-->c3*
    d2-->d3*
    e3-->e3*

    totalProgress

    a3*-->runA
    b3*-->runB
    c3*-->runC
    d3*-->runD
    e3*-->runE

    a3*-->progA
    b3*-->progB
    c3*-->progC
    d3*-->progD
    e3*-->progE


    progA-->totalProgress
    progB-->totalProgress
    progC-->totalProgress
    progD-->totalProgress
    progE-->totalProgress

    totalProgress-->quests
    quests-->userStats
    totalProgress-->userStats

```

The first step is migration and normalization: convert raw run result from
older versions to the latest version. Then normalize them: add missing fields
using default values. The result corresponds to user stats as if they had played
this single run. Each normalized record is then merged. Finally, quest
requirements are computed from the stats.




Progression is handled for a fixed game version / channel version pair, over
multiple runs.

Progression corresponds to how a player advances through a given version.

Maxsum is non-commutative
Associativity:

```
interface Item {
  max: number;
  cur: number;
}

function fromRun(score: number): Item {
  return {
    max: Math.max(0, score),
    cur: score
  }
}

function merge(left: Item, right: Item): Item {
  return {
    max: Math.max(left.max, left.cur + right.max),
    cur: left.cur + right.cur
  }
}
```
