use async_trait::async_trait;
use eternalfest_core::run::{
  GetUserItems, GetUserItemsError, RunStore, StoreCreateRun, StoreCreateRunError, StoreGetLeaderboard,
  StoreGetLeaderboardError, StoreGetRun, StoreGetRunError, StoreLeaderboard, StoreRun, StoreRunStarted,
  StoreSetRunResult, StoreSetRunResultError, StoreStartRun, StoreStartRunError,
};
use eternaltwin_core::hammerfest::HammerfestItemId;
use std::collections::BTreeMap;

pub struct MemRunStore {}

impl MemRunStore {
  #[expect(clippy::new_without_default, reason = "this is just a stub")]
  pub fn new() -> Self {
    Self {}
  }
}

#[async_trait]
impl RunStore for MemRunStore {
  async fn get_user_items(&self, _req: &GetUserItems) -> Result<BTreeMap<HammerfestItemId, u32>, GetUserItemsError> {
    todo!()
  }

  async fn create_run(&self, _req: &StoreCreateRun) -> Result<StoreRun, StoreCreateRunError> {
    todo!()
  }

  async fn get_run(&self, _req: &StoreGetRun) -> Result<StoreRun, StoreGetRunError> {
    todo!()
  }

  async fn start_run(&self, _req: &StoreStartRun) -> Result<StoreRunStarted, StoreStartRunError> {
    todo!()
  }

  async fn set_run_result(&self, _req: &StoreSetRunResult) -> Result<StoreRun, StoreSetRunResultError> {
    todo!()
  }

  async fn get_leaderboard(&self, _req: &StoreGetLeaderboard) -> Result<StoreLeaderboard, StoreGetLeaderboardError> {
    todo!()
  }
}
