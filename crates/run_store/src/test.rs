use eternalfest_core::clock::VirtualClock;
use eternalfest_core::core::Instant;
use eternalfest_core::run::RunStore;
use eternalfest_core::run::{RunId, RunIdRef, RunStoreRef, StoreGetRun, StoreGetRunError};
use eternalfest_core::SyncRef;

#[macro_export]
macro_rules! test_run_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_get_missing_run);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyClock, TyRunStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyRunStore: RunStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) run_store: TyRunStore,
}

pub(crate) async fn test_get_missing_run<TyClock, TyRunStore>(api: TestApi<TyClock, TyRunStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyRunStore: RunStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .run_store
    .run_store()
    .get_run(&StoreGetRun {
      run: RunIdRef::new(RunId::from_u128(123)),
    })
    .await;
  let expected = Err(StoreGetRunError::NotFound);
  assert_eq!(actual, expected);
}
