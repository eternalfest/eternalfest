use eternalfest_core::blob::{
  Blob, BlobDigest, BlobStore, BlobStoreRef, CreateBlobOptions, CreateUploadSessionOptions, FullBlob,
  GetBlobDataOptions, GetBlobOptions, UploadOptions, UploadSession,
};
use eternalfest_core::clock::VirtualClock;
use eternalfest_core::core::{Duration, Instant};
use eternalfest_core::digest::{DigestSha2, DigestSha3};
use eternalfest_core::SyncRef;

#[macro_export]
macro_rules! test_blob_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_create_text_plain);
    register_test!($(#[$meta])*, $api, test_create_empty);
    register_test!($(#[$meta])*, $api, test_create_and_get);
    register_test!($(#[$meta])*, $api, test_create_and_read);
    register_test!($(#[$meta])*, $api, test_create_upload_session);
    register_test!($(#[$meta])*, $api, test_create_upload_session_and_partial_upload);
    register_test!($(#[$meta])*, $api, test_create_upload_session_and_upload_all);
    register_test!($(#[$meta])*, $api, test_upload_all_and_read);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyBlobStore, TyClock>
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  pub(crate) blob_store: TyBlobStore,
  pub(crate) clock: TyClock,
}

pub(crate) async fn test_create_text_plain<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  let data = "Hello, World!".as_bytes();
  let actual = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "text/plain".parse().unwrap(),
      data: data.to_vec(),
    })
    .await
    .unwrap();
  let expected = Blob {
    id: actual.id,
    media_type: "text/plain".parse().unwrap(),
    byte_size: 13,
    digest: BlobDigest {
      sha2_256: "dffd6021bb2bd5b0af676290809ec3a53191dd81c7f70a4b28688a362182986f"
        .parse()
        .unwrap(),
      sha3_256: "1af17a664e3fa8e419b8ba05c2a173169df76162a5a286e0c405b460d478f7ef"
        .parse()
        .unwrap(),
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_empty<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  let actual = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "application/octet-stream".parse().unwrap(),
      data: Vec::new(),
    })
    .await
    .unwrap();
  let expected = Blob {
    id: actual.id,
    media_type: "application/octet-stream".parse().unwrap(),
    byte_size: 0,
    digest: BlobDigest {
      sha2_256: "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855"
        .parse()
        .unwrap(),
      sha3_256: "a7ffc6f8bf1ed76651c14756a061d662f580ff4de43b49fa82d80a4b80f8434a"
        .parse()
        .unwrap(),
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_and_get<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  let data = "Hello, World!".as_bytes();
  let blob = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "text/plain".parse().unwrap(),
      data: data.to_vec(),
    })
    .await
    .unwrap();
  let actual = api
    .blob_store
    .blob_store()
    .get_blob(&GetBlobOptions {
      id: blob.id,
      time: None,
    })
    .await
    .unwrap();
  let expected = Blob {
    id: blob.id,
    media_type: "text/plain".parse().unwrap(),
    byte_size: 13,
    digest: BlobDigest {
      sha2_256: "dffd6021bb2bd5b0af676290809ec3a53191dd81c7f70a4b28688a362182986f"
        .parse()
        .unwrap(),
      sha3_256: "1af17a664e3fa8e419b8ba05c2a173169df76162a5a286e0c405b460d478f7ef"
        .parse()
        .unwrap(),
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_and_read<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  let data = "Hello, World!".as_bytes();
  let blob: Blob = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "text/plain".parse().unwrap(),
      data: data.to_vec(),
    })
    .await
    .unwrap();
  let actual: FullBlob = api
    .blob_store
    .blob_store()
    .get_blob_data(&GetBlobDataOptions {
      id: blob.id,
      time: None,
    })
    .await
    .unwrap();
  let expected: FullBlob = FullBlob {
    id: blob.id,
    media_type: "text/plain".parse().unwrap(),
    byte_size: 13,
    digest: BlobDigest {
      sha2_256: DigestSha2::from_hex("dffd6021bb2bd5b0af676290809ec3a53191dd81c7f70a4b28688a362182986f")
        .expect("constant digest is valid"),
      sha3_256: DigestSha3::from_hex("1af17a664e3fa8e419b8ba05c2a173169df76162a5a286e0c405b460d478f7ef")
        .expect("constant digest is valid"),
    },
    offset: 0,
    data: Vec::from(data),
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_upload_session<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .blob_store
    .blob_store()
    .create_upload_session(&CreateUploadSessionOptions {
      media_type: "text/plain".parse().unwrap(),
      byte_size: 13,
    })
    .await
    .unwrap();
  let expected = UploadSession {
    id: actual.id,
    expires_at: Instant::ymd_hms(2021, 1, 2, 0, 0, 0),
    remaining_range: 0..13,
    blob: None,
  };

  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_upload_session_and_partial_upload<TyBlobStore, TyClock>(
  api: TestApi<TyBlobStore, TyClock>,
) where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let upload_session = api
    .blob_store
    .blob_store()
    .create_upload_session(&CreateUploadSessionOptions {
      media_type: "text/plain".parse().unwrap(),
      byte_size: 13,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .blob_store
    .blob_store()
    .upload(&UploadOptions {
      upload_session_id: upload_session.id,
      offset: 0,
      data: Vec::from("Hello".as_bytes()),
    })
    .await
    .unwrap();
  let expected = UploadSession {
    id: upload_session.id,
    expires_at: Instant::ymd_hms(2021, 1, 2, 0, 0, 0),
    remaining_range: 5..13,
    blob: None,
  };

  assert_eq!(actual, expected);
}

pub(crate) async fn test_create_upload_session_and_upload_all<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let upload_session = api
    .blob_store
    .blob_store()
    .create_upload_session(&CreateUploadSessionOptions {
      media_type: "text/plain".parse().unwrap(),
      byte_size: 13,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let upload_session = api
    .blob_store
    .blob_store()
    .upload(&UploadOptions {
      upload_session_id: upload_session.id,
      offset: 0,
      data: Vec::from("Hello".as_bytes()),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let actual = api
    .blob_store
    .blob_store()
    .upload(&UploadOptions {
      upload_session_id: upload_session.id,
      offset: 5,
      data: Vec::from(", World!".as_bytes()),
    })
    .await
    .unwrap();
  let expected = UploadSession {
    id: upload_session.id,
    expires_at: Instant::ymd_hms(2021, 1, 2, 0, 0, 0),
    remaining_range: 13..13,
    blob: Some(Blob {
      id: actual.blob.as_ref().unwrap().id,
      byte_size: 13,
      media_type: "text/plain".parse().unwrap(),
      digest: BlobDigest {
        sha2_256: "dffd6021bb2bd5b0af676290809ec3a53191dd81c7f70a4b28688a362182986f"
          .parse()
          .unwrap(),
        sha3_256: "1af17a664e3fa8e419b8ba05c2a173169df76162a5a286e0c405b460d478f7ef"
          .parse()
          .unwrap(),
      },
    }),
  };

  assert_eq!(actual, expected);
}

pub(crate) async fn test_upload_all_and_read<TyBlobStore, TyClock>(api: TestApi<TyBlobStore, TyClock>)
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let upload_session = api
    .blob_store
    .blob_store()
    .create_upload_session(&CreateUploadSessionOptions {
      media_type: "text/plain".parse().unwrap(),
      byte_size: 13,
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let upload_session = api
    .blob_store
    .blob_store()
    .upload(&UploadOptions {
      upload_session_id: upload_session.id,
      offset: 0,
      data: Vec::from("Hello".as_bytes()),
    })
    .await
    .unwrap();
  api.clock.advance_by(Duration::from_seconds(1));
  let upload_session = api
    .blob_store
    .blob_store()
    .upload(&UploadOptions {
      upload_session_id: upload_session.id,
      offset: 5,
      data: Vec::from(", World!".as_bytes()),
    })
    .await
    .unwrap();
  let blob = upload_session
    .blob
    .expect("blob should be defined on complete upload session");
  let actual = api
    .blob_store
    .blob_store()
    .get_blob_data(&GetBlobDataOptions {
      id: blob.id,
      time: None,
    })
    .await
    .unwrap();
  let expected = FullBlob {
    id: blob.id,
    media_type: "text/plain".parse().unwrap(),
    byte_size: 13,
    digest: BlobDigest {
      sha2_256: DigestSha2::from_hex("dffd6021bb2bd5b0af676290809ec3a53191dd81c7f70a4b28688a362182986f")
        .expect("constant digest is valid"),
      sha3_256: DigestSha3::from_hex("1af17a664e3fa8e419b8ba05c2a173169df76162a5a286e0c405b460d478f7ef")
        .expect("constant digest is valid"),
    },
    offset: 0,
    data: Vec::from("Hello, World!".as_bytes()),
  };
  assert_eq!(actual, expected);
}
