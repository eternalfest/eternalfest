use crate::{MAX_BLOB_SIZE, UPLOAD_SESSION_MAX_AGE};
use async_trait::async_trait;
use eternalfest_core::blob::{
  Blob, BlobDigest, BlobId, BlobStore, CreateBlobOptions, CreateStoreBlobError, CreateStoreUploadSessionError,
  CreateUploadSessionOptions, FullBlob, GetBlobDataOptions, GetBlobOptions, GetBlobsError, GetBlobsOptions,
  GetStoreBlobDataError, GetStoreBlobError, MediaType, UploadError, UploadOptions, UploadSession, UploadSessionId,
};
use eternalfest_core::buffer::{BufferId, BufferStore, BufferStoreRef};
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::Instant;
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::{BTreeMap, HashMap};
use std::convert::TryFrom;
use tokio::sync::RwLock;

struct MemBlobStoreState {
  blobs: BTreeMap<BlobId, MemBlob>,
  upload_sessions: BTreeMap<UploadSessionId, MemUploadSession>,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct MemBlob {
  id: BlobId,
  created_at: Instant,
  media_type: MediaType,
  byte_size: u32,
  digest: BlobDigest,
  buffer: BufferId,
}

#[derive(Debug, Clone, PartialEq, Eq)]
struct MemUploadSession {
  id: UploadSessionId,
  created_at: Instant,
  updated_at: Instant,
  media_type: MediaType,
  byte_size: u32,
  buffer: BufferId,
  written_bytes: u32,
  blob: Option<BlobId>,
}

impl MemBlobStoreState {
  pub fn new() -> Self {
    Self {
      blobs: BTreeMap::new(),
      upload_sessions: BTreeMap::new(),
    }
  }
}

pub struct MemBlobStore<TyBufferStore, TyClock, TyUuidGenerator>
where
  TyBufferStore: BufferStoreRef,
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  buffer_store: TyBufferStore,
  clock: TyClock,
  uuid_generator: TyUuidGenerator,
  state: RwLock<MemBlobStoreState>,
}

impl<TyBufferStore, TyClock, TyUuidGenerator> MemBlobStore<TyBufferStore, TyClock, TyUuidGenerator>
where
  TyBufferStore: BufferStoreRef,
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(buffer_store: TyBufferStore, clock: TyClock, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      buffer_store,
      clock,
      uuid_generator,
      state: RwLock::new(MemBlobStoreState::new()),
    }
  }
}

#[async_trait]
impl<TyBufferStore, TyClock, TyUuidGenerator> BlobStore for MemBlobStore<TyBufferStore, TyClock, TyUuidGenerator>
where
  TyBufferStore: BufferStoreRef,
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  /// TODO: Remove this function, it should _always_ be true.
  fn has_immutable_blobs(&self) -> bool {
    true
  }

  async fn create_blob(&self, options: &CreateBlobOptions) -> Result<Blob, CreateStoreBlobError> {
    let input_len = u32::try_from(options.data.len()).map_err(|_| CreateStoreBlobError::MaxSize)?;
    if input_len > MAX_BLOB_SIZE {
      return Err(CreateStoreBlobError::MaxSize);
    }

    let mut state = self.state.write().await;
    let id = BlobId::from_uuid(self.uuid_generator.uuid_generator().next());
    let now = self.clock.clock().now();
    let digest = BlobDigest::digest(options.data.as_slice());
    let buffer_id = self
      .buffer_store
      .buffer_store()
      .create_buffer(input_len)
      .await
      .map_err(CreateStoreBlobError::other)?;
    self
      .buffer_store
      .buffer_store()
      .write_bytes(buffer_id, 0, options.data.as_slice())
      .await
      .map_err(CreateStoreBlobError::other)?;

    let blob = MemBlob {
      id,
      buffer: buffer_id,
      created_at: now,
      media_type: options.media_type.clone(),
      byte_size: input_len,
      digest: BlobDigest::digest(&options.data),
    };
    state.blobs.insert(id, blob);

    Ok(Blob {
      id,
      media_type: options.media_type.clone(),
      byte_size: input_len,
      digest,
    })
  }

  async fn get_blob(&self, options: &GetBlobOptions) -> Result<Blob, GetStoreBlobError> {
    let state = self.state.read().await;
    let Some(blob) = state.blobs.get(&options.id) else {
      return Err(GetStoreBlobError::NotFound(options.id));
    };
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());
    if time < blob.created_at {
      return Err(GetStoreBlobError::NotFound(options.id));
    }
    Ok(Blob {
      id: blob.id,
      media_type: blob.media_type.clone(),
      byte_size: blob.byte_size,
      digest: blob.digest,
    })
  }

  async fn get_blobs(
    &self,
    options: &GetBlobsOptions,
  ) -> Result<HashMap<BlobId, Result<Blob, GetStoreBlobError>>, GetBlobsError> {
    let state = self.state.read().await;
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());
    let mut out: HashMap<BlobId, Result<Blob, GetStoreBlobError>> = HashMap::new();
    for blob_id in &options.id {
      let res = match state.blobs.get(blob_id) {
        None => Err(GetStoreBlobError::NotFound(*blob_id)),
        Some(blob) if time < blob.created_at => Err(GetStoreBlobError::NotFound(*blob_id)),
        Some(blob) => Ok(Blob {
          id: blob.id,
          media_type: blob.media_type.clone(),
          byte_size: blob.byte_size,
          digest: blob.digest,
        }),
      };
      out.insert(*blob_id, res);
    }
    Ok(out)
  }

  async fn get_blob_data(&self, options: &GetBlobDataOptions) -> Result<FullBlob, GetStoreBlobDataError> {
    let state = self.state.read().await;
    let Some(blob) = state.blobs.get(&options.id) else {
      return Err(GetStoreBlobDataError::NotFound(options.id));
    };
    let time = options.time.unwrap_or_else(|| self.clock.clock().now());
    if blob.created_at < time {
      return Err(GetStoreBlobDataError::NotFound(options.id));
    }
    let buffer_id = blob.buffer;

    let buffer_content = self
      .buffer_store
      .buffer_store()
      .read_stream(buffer_id)
      .await
      .map_err(GetStoreBlobDataError::other)?;

    Ok(FullBlob {
      id: options.id,
      media_type: blob.media_type.clone(),
      byte_size: blob.byte_size,
      digest: blob.digest,
      offset: 0,
      data: buffer_content,
    })
  }

  async fn create_upload_session(
    &self,
    options: &CreateUploadSessionOptions,
  ) -> Result<UploadSession, CreateStoreUploadSessionError> {
    let mut state = self.state.write().await;

    let input_len = options.byte_size;
    if input_len > MAX_BLOB_SIZE {
      return Err(CreateStoreUploadSessionError::MaxSize);
    }
    let now = self.clock.clock().now();
    let upload_session_id = UploadSessionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let buffer_id = self
      .buffer_store
      .buffer_store()
      .create_buffer(input_len)
      .await
      .map_err(CreateStoreUploadSessionError::other)?;

    let upload_session = MemUploadSession {
      id: upload_session_id,
      created_at: now,
      updated_at: now,
      media_type: options.media_type.clone(),
      byte_size: input_len,
      buffer: buffer_id,
      written_bytes: 0,
      blob: None,
    };
    state.upload_sessions.insert(upload_session_id, upload_session);

    Ok(UploadSession {
      id: upload_session_id,
      expires_at: now + UPLOAD_SESSION_MAX_AGE,
      remaining_range: 0..input_len,
      blob: None,
    })
  }

  async fn upload(&self, options: &UploadOptions) -> Result<UploadSession, UploadError> {
    let mut state = self.state.write().await;
    let state: &mut MemBlobStoreState = &mut state;
    if options.data.is_empty() {
      return Err(UploadError::EmptyInputData);
    }
    let input_len = u32::try_from(options.data.len()).map_err(|_| UploadError::Overflow)?;
    let now = self.clock.clock().now();
    let Some(upload_session) = state.upload_sessions.get_mut(&options.upload_session_id) else {
      return Err(UploadError::NotFound(options.upload_session_id));
    };
    if now < upload_session.created_at {
      return Err(UploadError::NotFound(options.upload_session_id));
    }

    let buffer_id: BufferId = upload_session.buffer;

    let expires_at = upload_session.created_at + UPLOAD_SESSION_MAX_AGE;
    if expires_at <= now {
      return Err(UploadError::Expired(options.upload_session_id));
    }
    let written_bytes: u32 = upload_session.written_bytes;
    let byte_size: u32 = upload_session.byte_size;
    let new_written_bytes: u32 = written_bytes.checked_add(input_len).ok_or(UploadError::Overflow)?;
    if new_written_bytes > byte_size {
      return Err(UploadError::Overflow);
    }
    if options.offset != written_bytes {
      return Err(UploadError::BadOffset {
        actual: options.offset,
        expected: written_bytes,
      });
    }
    self
      .buffer_store
      .buffer_store()
      .write_bytes(buffer_id, options.offset, &options.data)
      .await
      .map_err(UploadError::other)?;

    let blob = if new_written_bytes == byte_size {
      let data = self
        .buffer_store
        .buffer_store()
        .read_stream(buffer_id)
        .await
        .expect("buffer is readable");

      let digest = BlobDigest::digest(data.as_slice());

      let blob_id = BlobId::from_uuid(self.uuid_generator.uuid_generator().next());

      {
        let blob = MemBlob {
          id: blob_id,
          buffer: buffer_id,
          created_at: now,
          media_type: upload_session.media_type.clone(),
          byte_size,
          digest,
        };
        state.blobs.insert(blob_id, blob);
      }
      Some(Blob {
        id: blob_id,
        byte_size,
        media_type: upload_session.media_type.clone(),
        digest,
      })
    } else {
      None
    };
    upload_session.updated_at = now;
    upload_session.written_bytes = new_written_bytes;
    upload_session.blob = blob.as_ref().map(|b| b.id);

    Ok(UploadSession {
      id: options.upload_session_id,
      expires_at,
      remaining_range: new_written_bytes..byte_size,
      blob,
    })
  }
}

#[cfg(test)]
mod test {
  use super::MemBlobStore;
  use crate::test::TestApi;
  use eternalfest_buffer_store::mem::MemBufferStore;
  use eternalfest_core::blob::BlobStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::uuid::Uuid4Generator;
  use eternalfest_db_schema::force_create_latest;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn BlobStore>, Arc<VirtualClock>> {
    let config = eternalfest_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let buffer_store = Arc::new(MemBufferStore::new(Arc::clone(&uuid_generator)));
    let blob_store: Arc<dyn BlobStore> = Arc::new(MemBlobStore::new(buffer_store, clock.clone(), uuid_generator));

    TestApi { blob_store, clock }
  }

  test_blob_store!(|| make_test_api().await);
}
