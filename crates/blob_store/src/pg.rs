use crate::{MAX_BLOB_SIZE, UPLOAD_SESSION_MAX_AGE};
use async_trait::async_trait;
use eternalfest_core::blob::{
  Blob, BlobDigest, BlobId, BlobStore, CreateBlobOptions, CreateStoreBlobError, CreateStoreUploadSessionError,
  CreateUploadSessionOptions, FullBlob, GetBlobDataOptions, GetBlobOptions, GetBlobsError, GetBlobsOptions,
  GetStoreBlobDataError, GetStoreBlobError, MediaType, UploadError, UploadOptions, UploadSession, UploadSessionId,
};
use eternalfest_core::buffer::{BufferId, BufferStore, BufferStoreRef};
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::{Instant, PeriodLower};
use eternalfest_core::digest::{DigestSha2, DigestSha3};
use eternalfest_core::pg_num::PgU32;
use eternalfest_core::types::WeakError;
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternalfest_core::SyncRef;
use futures::StreamExt;
use futures::TryStreamExt;
use sqlx::postgres::{PgPool, PgQueryResult};
use sqlx::{Postgres, Transaction};
use std::collections::HashMap;
use std::convert::TryFrom;
use std::str::FromStr;
use uuid::Uuid;

pub struct PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStoreRef,
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  buffer_store: TyBufferStore,
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
}

impl<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
  PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStoreRef,
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub async fn new(
    buffer_store: TyBufferStore,
    clock: TyClock,
    database: TyDatabase,
    uuid_generator: TyUuidGenerator,
  ) -> Result<Self, WeakError> {
    let store = Self {
      buffer_store,
      clock,
      database,
      uuid_generator,
    };
    store.migrate_data().await?;
    Ok(store)
  }

  pub async fn mark_unused_blobs_for_deletion(&self) -> Result<(), WeakError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    let queued_rows = {
      // `ON CONFLICT` makes the insertion idempotent (should not be needed in practice)
      // language=PostgreSQL
      let res: PgQueryResult = sqlx::query(
        r"
        WITH unused_blobs AS (
          SELECT blob_id, media_type, byte_size, buffer_id, created_at, updated_at
          FROM blobs
          WHERE NOT EXISTS (
            SELECT 1 FROM files WHERE files.blob_id = blobs.blob_id
          )
        )
        INSERT INTO blob_deletion_queue(blob_id, media_type, byte_size, buffer_id, created_at, updated_at, deleted_at)
        SELECT blob_id, media_type, byte_size, buffer_id, created_at, updated_at, NOW() AS deleted_at FROM unused_blobs
        ON CONFLICT (blob_id) DO NOTHING;
      ",
      )
      .execute(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      res.rows_affected()
    };
    let deleted_rows = {
      // language=PostgreSQL
      let res: PgQueryResult = sqlx::query(
        r"
        DELETE FROM blobs
      WHERE blob_id IN (SELECT blob_id FROM blob_deletion_queue);
      ",
      )
      .execute(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      res.rows_affected()
    };
    assert_eq!(deleted_rows, queued_rows);
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }

  pub async fn exec_blob_deletions(&self) -> Result<(), WeakError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      buffer_id: String,
    }
    // language=PostgreSQL
    let rows: Vec<Row> = sqlx::query_as(
      r"
        SELECT buffer_id FROM blob_deletion_queue;
      ",
    )
    .fetch_all(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    let deleted_buffers = u64::try_from(rows.len()).expect("OverflowOnDeletedBuffers");

    for row in rows {
      self
        .buffer_store
        .buffer_store()
        .delete_buffer_if_exists(row.buffer_id.parse().unwrap())
        .await
        .map_err(WeakError::wrap)?;
    }

    let deleted_rows = {
      // language=PostgreSQL
      let res: PgQueryResult = sqlx::query(
        r"
        DELETE FROM blob_deletion_queue;
      ",
      )
      .execute(&mut *tx)
      .await
      .map_err(WeakError::wrap)?;
      res.rows_affected()
    };
    assert_eq!(deleted_rows, deleted_buffers);
    tx.commit().await.map_err(WeakError::wrap)?;
    Ok(())
  }

  async fn migrate_data(&self) -> Result<(), WeakError> {
    let mut buffers = self.buffer_store.buffer_store().list().await?;
    while let Some(buffer) = buffers.next().await {
      let buffer = buffer?;
      let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;
      if !self.buffer_is_alive(&mut tx, buffer.id).await? {
        eprintln!("scheduling deletion for: {:?}", buffer);
      }
      tx.commit().await.map_err(WeakError::wrap)?;
    }
    Ok(())
  }

  async fn touch_buffer(
    &self,
    tx: &mut Transaction<'_, Postgres>,
    now: Instant,
    buffer_id: BufferId,
    data: &[u8],
  ) -> Result<(BufferId, PeriodLower), WeakError> {
    let digest = BlobDigest::digest(data);

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      buffer_id: String,
      period: PeriodLower,
    }
    // language=PostgreSQL
    let row: Option<Row> = sqlx::query_as::<_, Row>(
      r"
      SELECT buffer_id, period
      FROM buffer
      WHERE _alive AND size = $1::U32 AND digest_sha2_256 = $2::BYTEA AND digest_sha3_256 = $3::BYTEA
      LIMIT 1;
    ",
    )
    .bind(PgU32::from(u32::try_from(data.len()).map_err(WeakError::wrap)?))
    .bind(digest.sha2_256.as_slice())
    .bind(digest.sha3_256.as_slice())
    .fetch_optional(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    if let Some(row) = row {
      let buffer_id = BufferId::from_str(&row.buffer_id).map_err(WeakError::wrap)?;
      return Ok((buffer_id, row.period));
    }

    // language=PostgreSQL
    let result = sqlx::query(
      r"
      INSERT INTO buffer(
        period,
        size,
        digest_sha2_256,
        digest_sha3_256,
        buffer_id,
        _alive,
        freed_at
      )
      VALUES (PERIOD($1::INSTANT, NULL), $2::U32, $3::BYTEA, $4::BYTEA, $5::BUFFER_ID, true, NULL);
    ",
    )
    .bind(now)
    .bind(PgU32::from(u32::try_from(data.len()).map_err(WeakError::wrap)?))
    .bind(digest.sha2_256.as_slice())
    .bind(digest.sha3_256.as_slice())
    .bind(buffer_id.to_hex().as_str())
    .execute(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    if result.rows_affected() != 1 {
      return Err(WeakError::new("invalid affected row count"));
    }

    Ok((buffer_id, PeriodLower::new(now, None)))
  }

  /// Check if the provided buffer id exists and is alive
  async fn buffer_is_alive(&self, tx: &mut Transaction<'_, Postgres>, buffer_id: BufferId) -> Result<bool, WeakError> {
    // language=PostgreSQL
    let row: Option<(i32,)> = sqlx::query_as::<_, (i32,)>(
      r"
      SELECT 1
      FROM buffer
      WHERE buffer_id = $1::BUFFER_ID AND _alive
      LIMIT 1;
    ",
    )
    .bind(buffer_id.to_hex().as_str())
    .fetch_optional(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;

    Ok(row.is_some())
  }
}

#[async_trait]
impl<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator> BlobStore
  for PgBlobStore<TyBufferStore, TyClock, TyDatabase, TyUuidGenerator>
where
  TyBufferStore: BufferStoreRef,
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  /// TODO: Remove this function, it should _always_ be true.
  fn has_immutable_blobs(&self) -> bool {
    true
  }

  async fn create_blob(&self, options: &CreateBlobOptions) -> Result<Blob, CreateStoreBlobError> {
    let input_len = u32::try_from(options.data.len()).map_err(|_| CreateStoreBlobError::MaxSize)?;
    if input_len > MAX_BLOB_SIZE {
      return Err(CreateStoreBlobError::MaxSize);
    }
    let now = self.clock.clock().now();

    let digest = BlobDigest::digest(options.data.as_slice());

    let buffer_id = self
      .buffer_store
      .buffer_store()
      .create_buffer(input_len)
      .await
      .map_err(CreateStoreBlobError::other)?;
    self
      .buffer_store
      .buffer_store()
      .write_bytes(buffer_id, 0, options.data.as_slice())
      .await
      .map_err(CreateStoreBlobError::other)?;

    let mut tx = self.database.begin().await.map_err(CreateStoreBlobError::other)?;
    let (buffer_id, buffer_period) = self
      .touch_buffer(&mut tx, now, buffer_id, options.data.as_slice())
      .await?;

    let blob_id = BlobId::from_uuid(self.uuid_generator.uuid_generator().next());
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      blob_id: BlobId,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as(
      r"
        INSERT
        INTO blob(blob_id, period, buffer_id, _buffer_period, media_type)
        VALUES ($1::BLOB_ID, PERIOD($2::INSTANT, NULL), $3::BUFFER_ID, $4::PERIOD_LOWER, $5::MEDIA_TYPE)
        RETURNING blob_id;
      ",
    )
    .bind(blob_id)
    .bind(now)
    .bind(buffer_id.to_hex().as_str())
    .bind(buffer_period)
    .bind(&options.media_type)
    .fetch_one(&mut *tx)
    .await
    .map_err(CreateStoreBlobError::other)?;
    assert_eq!(row.blob_id, blob_id);

    tx.commit().await.map_err(CreateStoreBlobError::other)?;

    Ok(Blob {
      id: blob_id,
      media_type: options.media_type.clone(),
      byte_size: input_len,
      digest,
    })
  }

  async fn get_blob(&self, options: &GetBlobOptions) -> Result<Blob, GetStoreBlobError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      byte_size: PgU32,
      media_type: MediaType,
      digest_sha2_256: Option<Vec<u8>>,
      digest_sha3_256: Option<Vec<u8>>,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      SELECT size AS byte_size, media_type, digest_sha2_256, digest_sha3_256
      FROM blob INNER JOIN buffer USING (buffer_id)
      WHERE blob.blob_id = $1::BLOB_ID;
    ",
    )
    .bind(options.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(GetStoreBlobError::other)?
    .ok_or(GetStoreBlobError::NotFound(options.id))?;

    Ok(Blob {
      id: options.id,
      media_type: row.media_type,
      byte_size: row.byte_size.into(),
      digest: BlobDigest {
        sha2_256: DigestSha2::from_bytes(row.digest_sha2_256.as_deref().unwrap_or(&[0; 32]))
          .expect("Digest2 from database is valid"),
        sha3_256: DigestSha3::from_bytes(row.digest_sha3_256.as_deref().unwrap_or(&[0; 32]))
          .expect("Digest3 from database is valid"),
      },
    })
  }

  async fn get_blobs(
    &self,
    options: &GetBlobsOptions,
  ) -> Result<HashMap<BlobId, Result<Blob, GetStoreBlobError>>, GetBlobsError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      blob_id: BlobId,
      byte_size: PgU32,
      media_type: MediaType,
      digest_sha2_256: Option<Vec<u8>>,
      digest_sha3_256: Option<Vec<u8>>,
    }
    let ids: Vec<Uuid> = options.id.iter().map(|id| id.into_uuid()).collect();
    let mut blobs: HashMap<BlobId, Result<Blob, GetStoreBlobError>> = HashMap::new();

    // language=PostgreSQL
    let mut rows = sqlx::query_as::<_, Row>(
      r"
      SELECT blob_id, size AS byte_size, media_type, digest_sha2_256, digest_sha3_256
      FROM blob INNER JOIN buffer USING (buffer_id)
      WHERE blob.blob_id = ANY($1::BLOB_ID[]);
      ",
    )
    .bind(ids.as_slice())
    .fetch(&*self.database);

    while let Some(row) = rows
      .try_next()
      .await
      .map_err(|e| GetBlobsError::Other(WeakError::wrap(e)))?
    {
      blobs.insert(
        row.blob_id,
        Ok(Blob {
          id: row.blob_id,
          media_type: row.media_type,
          byte_size: row.byte_size.into(),
          digest: BlobDigest {
            sha2_256: DigestSha2::from_bytes(row.digest_sha2_256.as_deref().unwrap_or(&[0; 32]))
              .expect("Digest2 from database is valid"),
            sha3_256: DigestSha3::from_bytes(row.digest_sha3_256.as_deref().unwrap_or(&[0; 32]))
              .expect("Digest3 from database is valid"),
          },
        }),
      );
    }

    for id in &options.id {
      if !blobs.contains_key(id) {
        blobs.insert(*id, Err(GetStoreBlobError::NotFound(*id)));
      }
    }

    Ok(blobs)
  }

  async fn get_blob_data(&self, options: &GetBlobDataOptions) -> Result<FullBlob, GetStoreBlobDataError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      byte_size: PgU32,
      media_type: MediaType,
      digest_sha2_256: Option<Vec<u8>>,
      digest_sha3_256: Option<Vec<u8>>,
      buffer_id: String,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
      SELECT size AS byte_size, media_type, digest_sha2_256, digest_sha3_256, buffer_id
      FROM blob INNER JOIN buffer USING (buffer_id)
      WHERE blob.blob_id = $1::BLOB_ID;
    ",
    )
    .bind(options.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(GetStoreBlobDataError::other)?
    .ok_or(GetStoreBlobDataError::NotFound(options.id))?;

    let buffer_id = BufferId::from_uuid(row.buffer_id.parse().unwrap());

    let buffer_content = self
      .buffer_store
      .buffer_store()
      .read_stream(buffer_id)
      .await
      .map_err(GetStoreBlobDataError::other)?;

    Ok(FullBlob {
      id: options.id,
      media_type: row.media_type,
      byte_size: row.byte_size.into(),
      digest: BlobDigest {
        sha2_256: DigestSha2::from_bytes(row.digest_sha2_256.as_deref().unwrap_or(&[0; 32]))
          .expect("Digest2 from database is valid"),
        sha3_256: DigestSha3::from_bytes(row.digest_sha3_256.as_deref().unwrap_or(&[0; 32]))
          .expect("Digest3 from database is valid"),
      },
      offset: 0,
      data: buffer_content,
    })
  }

  async fn create_upload_session(
    &self,
    options: &CreateUploadSessionOptions,
  ) -> Result<UploadSession, CreateStoreUploadSessionError> {
    let input_len = options.byte_size;
    if input_len > MAX_BLOB_SIZE {
      return Err(CreateStoreUploadSessionError::MaxSize);
    }
    let now = self.clock.clock().now();
    let upload_session_id = UploadSessionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let buffer_id = self
      .buffer_store
      .buffer_store()
      .create_buffer(input_len)
      .await
      .map_err(CreateStoreUploadSessionError::other)?;

    {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        upload_session_id: UploadSessionId,
      }
      // language=PostgreSQL
      let row: Row = sqlx::query_as(
        r"
        INSERT INTO upload_sessions(
          upload_session_id, media_type, byte_size, written_bytes, created_at, updated_at, buffer_id, blob_id
        )
        VALUES ($1::UPLOAD_SESSION_ID, $2::MEDIA_TYPE, $3::U32, 0, $4::INSTANT, $4::INSTANT, $5::BUFFER_ID, NULL)
        RETURNING upload_session_id;
      ",
      )
      .bind(upload_session_id)
      .bind(options.media_type.as_str())
      .bind(PgU32::from(input_len))
      .bind(now)
      .bind(buffer_id.to_hex().as_str())
      .fetch_one(&*self.database)
      .await
      .map_err(CreateStoreUploadSessionError::other)?;
      assert_eq!(row.upload_session_id, upload_session_id);
    }
    Ok(UploadSession {
      id: upload_session_id,
      expires_at: now + UPLOAD_SESSION_MAX_AGE,
      remaining_range: 0..input_len,
      blob: None,
    })
  }

  async fn upload(&self, options: &UploadOptions) -> Result<UploadSession, UploadError> {
    if options.data.is_empty() {
      return Err(UploadError::EmptyInputData);
    }
    let input_len = u32::try_from(options.data.len()).map_err(|_| UploadError::Overflow)?;
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(UploadError::other)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      byte_size: PgU32,
      written_bytes: PgU32,
      created_at: Instant,
      media_type: MediaType,
      buffer_id: String,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as(
      r"
      SELECT byte_size, written_bytes, created_at, media_type, buffer_id
      FROM upload_sessions
      WHERE upload_session_id = $1::UPLOAD_SESSION_ID;
      ",
    )
    .bind(options.upload_session_id)
    .fetch_optional(&mut *tx)
    .await
    .map_err(UploadError::other)?
    .ok_or(UploadError::NotFound(options.upload_session_id))?;

    let buffer_id = BufferId::from_uuid(row.buffer_id.parse().unwrap());

    let expires_at = row.created_at + UPLOAD_SESSION_MAX_AGE;
    if now >= expires_at {
      return Err(UploadError::Expired(options.upload_session_id));
    }
    let written_bytes = u32::from(row.written_bytes);
    let byte_size = u32::from(row.byte_size);
    let new_written_bytes = written_bytes.checked_add(input_len).ok_or(UploadError::Overflow)?;
    if new_written_bytes > byte_size {
      return Err(UploadError::Overflow);
    }
    if options.offset != written_bytes {
      return Err(UploadError::BadOffset {
        actual: options.offset,
        expected: written_bytes,
      });
    }
    self
      .buffer_store
      .buffer_store()
      .write_bytes(buffer_id, options.offset, &options.data)
      .await
      .map_err(UploadError::other)?;

    let blob = if new_written_bytes == byte_size {
      let data = self
        .buffer_store
        .buffer_store()
        .read_stream(buffer_id)
        .await
        .expect("buffer is readable");

      let (buffer_id, buffer_period) = self
        .touch_buffer(&mut tx, now, buffer_id, &data)
        .await
        .map_err(WeakError::wrap)?;

      let digest = BlobDigest::digest(data.as_slice());

      let blob_id = BlobId::from_uuid(self.uuid_generator.uuid_generator().next());
      {
        let media_type = &row.media_type;
        #[derive(Debug, sqlx::FromRow)]
        struct Row {
          blob_id: BlobId,
        }
        // language=PostgreSQL
        let row: Row = sqlx::query_as(
          r"
        INSERT
        INTO blob(blob_id, period, buffer_id, _buffer_period, media_type)
        VALUES ($1::BLOB_ID, PERIOD($2::INSTANT, NULL), $3::BUFFER_ID, $4::PERIOD_LOWER, $5::MEDIA_TYPE)
        RETURNING blob_id;
      ",
        )
        .bind(blob_id)
        .bind(now)
        .bind(buffer_id.to_hex().as_str())
        .bind(buffer_period)
        .bind(media_type)
        .fetch_one(&mut *tx)
        .await
        .map_err(UploadError::other)?;
        assert_eq!(row.blob_id, blob_id);
      }
      Some(Blob {
        id: blob_id,
        byte_size,
        media_type: row.media_type.clone(),
        digest,
      })
    } else {
      None
    };
    {
      // language=PostgreSQL
      let res: PgQueryResult = sqlx::query(
        r"
        UPDATE upload_sessions
        SET written_bytes = $3::U32, updated_at = $4::INSTANT, blob_id = $5::BLOB_ID
        WHERE upload_session_id = $1::UPLOAD_SESSION_ID AND written_bytes = $2::U32 AND blob_id IS NULL;
      ",
      )
      .bind(options.upload_session_id)
      .bind(PgU32::from(written_bytes))
      .bind(PgU32::from(new_written_bytes))
      .bind(now)
      .bind(blob.as_ref().map(|b| b.id))
      .execute(&mut *tx)
      .await
      .map_err(UploadError::other)?;
      assert_eq!(res.rows_affected(), 1);
    }
    tx.commit().await.map_err(UploadError::other)?;

    Ok(UploadSession {
      id: options.upload_session_id,
      expires_at,
      remaining_range: new_written_bytes..byte_size,
      blob,
    })
  }
}

#[cfg(test)]
mod test {
  use super::PgBlobStore;
  use crate::test::TestApi;
  use eternalfest_buffer_store::fs::FsBufferStore;
  use eternalfest_core::blob::BlobStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::uuid::Uuid4Generator;
  use eternalfest_db_schema::force_create_latest;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn BlobStore>, Arc<VirtualClock>> {
    let config = eternalfest_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let data_root = config.data_root();
    let buffer_store = Arc::new(FsBufferStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator), data_root).await);
    let blob_store: Arc<dyn BlobStore> = Arc::new(
      PgBlobStore::new(buffer_store, clock.clone(), database, uuid_generator)
        .await
        .expect("failed to create `PgBlobStore`"),
    );

    TestApi { blob_store, clock }
  }

  test_blob_store!(
    #[serial]
    || make_test_api().await
  );
}
