use eternalfest_core::core::Duration;

#[cfg(test)]
#[macro_use]
pub(crate) mod test;

pub(crate) const MAX_BLOB_SIZE: u32 = 100 * 1024 * 1024;
pub(crate) const UPLOAD_SESSION_MAX_AGE: Duration = Duration::from_days(1);

#[cfg(feature = "mem")]
pub mod mem;
#[cfg(feature = "pg")]
pub mod pg;
