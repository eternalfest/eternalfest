use crate::{BuiltinEternalfestConfigProfile, ConfigValue};
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct DataConfig<TyMeta> {
  pub root: ConfigValue<String, TyMeta>,
}

impl DataConfig<()> {
  pub(crate) fn patch(mut self, config: DataConfigPatch) -> Self {
    if let SimplePatch::Set(root) = config.root {
      self.root = ConfigValue::new(root);
    }
    self
  }

  pub(crate) fn default_for_profile(_profile: BuiltinEternalfestConfigProfile) -> Self {
    Self {
      root: ConfigValue::new("./data/".to_string()),
      // root: ConfigValue::new(Url::parse("file:///var/data/eternalfest/").expect("default `data.root` is valid")),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct DataConfigPatch {
  /// URL to the root of the data directory
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub root: SimplePatch<String>,
}
