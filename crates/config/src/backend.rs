use crate::{BufferConfig, BuiltinEternalfestConfigProfile, ClockConfig, ConfigValue, StoreConfig};
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct BackendConfig<TyMeta> {
  pub listen: ConfigValue<String, TyMeta>,
  pub port: ConfigValue<Option<u16>, TyMeta>,
  pub secret: ConfigValue<Option<String>, TyMeta>,
  pub cookie_key: ConfigValue<Option<String>, TyMeta>,
  pub clock: ConfigValue<ClockConfig, TyMeta>,
  pub auth_store: ConfigValue<StoreConfig, TyMeta>,
  pub blob_store: ConfigValue<StoreConfig, TyMeta>,
  pub buffer_store: ConfigValue<BufferConfig, TyMeta>,
  pub game_store: ConfigValue<StoreConfig, TyMeta>,
  pub run_store: ConfigValue<StoreConfig, TyMeta>,
  pub user_store: ConfigValue<StoreConfig, TyMeta>,
}

impl BackendConfig<()> {
  pub(crate) fn patch(mut self, config: BackendConfigPatch) -> Self {
    match (config.listen, config.port) {
      (SimplePatch::Set(listen), SimplePatch::Skip) => {
        self.listen = ConfigValue::new(listen);
        self.port = ConfigValue::new(None);
      }
      (SimplePatch::Skip, SimplePatch::Set(port)) => {
        // keep old listen interface
        self.port = ConfigValue::new(Some(port));
      }
      (SimplePatch::Set(listen), SimplePatch::Set(port)) => {
        self.listen = ConfigValue::new(listen);
        self.port = ConfigValue::new(Some(port));
      }
      (SimplePatch::Skip, SimplePatch::Skip) => {
        // nothing to do
      }
    }
    if let SimplePatch::Set(secret) = config.secret {
      self.secret = ConfigValue::new(secret);
    }
    if let SimplePatch::Set(cookie_key) = config.cookie_key {
      self.cookie_key = ConfigValue::new(cookie_key);
    }
    if let SimplePatch::Set(clock) = config.clock {
      self.clock = ConfigValue::new(clock);
    }
    if let SimplePatch::Set(buffer_store) = config.buffer_store {
      self.buffer_store = ConfigValue::new(buffer_store);
    }
    if let SimplePatch::Set(store) = config.store {
      let value = ConfigValue::new(store);
      self.auth_store = value;
      self.blob_store = value;
      self.game_store = value;
      self.run_store = value;
      self.user_store = value;
    }
    if let SimplePatch::Set(patch) = config.auth_store {
      self.auth_store = ConfigValue::new(patch);
    }
    if let SimplePatch::Set(patch) = config.blob_store {
      self.blob_store = ConfigValue::new(patch);
    }
    if let SimplePatch::Set(patch) = config.game_store {
      self.game_store = ConfigValue::new(patch);
    }
    if let SimplePatch::Set(patch) = config.run_store {
      self.run_store = ConfigValue::new(patch);
    }
    if let SimplePatch::Set(patch) = config.user_store {
      self.user_store = ConfigValue::new(patch);
    }
    self
  }

  pub(crate) fn default_for_profile(profile: BuiltinEternalfestConfigProfile) -> Self {
    use BuiltinEternalfestConfigProfile::*;
    let store = match profile {
      Production => ConfigValue::new(StoreConfig::Postgres),
      Dev | Sdk | Test => ConfigValue::new(StoreConfig::Memory),
    };
    Self {
      listen: ConfigValue::new(String::from("[::]:50314")),
      port: ConfigValue::new(None),
      secret: match profile {
        Production => ConfigValue::new(None),
        Dev | Sdk | Test => ConfigValue::new(Some("dev".to_string())),
      },
      cookie_key: match profile {
        Production => ConfigValue::new(None),
        Dev | Sdk | Test => ConfigValue::new(Some("dev".to_string())),
      },
      clock: match profile {
        Dev | Production => ConfigValue::new(ClockConfig::System),
        Sdk | Test => ConfigValue::new(ClockConfig::Virtual),
      },
      buffer_store: ConfigValue::new(match profile {
        Production => BufferConfig::Fs,
        Dev | Sdk | Test => BufferConfig::Memory,
      }),
      auth_store: store,
      blob_store: store,
      game_store: store,
      run_store: store,
      user_store: store,
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct BackendConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub listen: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub port: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub secret: SimplePatch<Option<String>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub cookie_key: SimplePatch<Option<String>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub clock: SimplePatch<ClockConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub buffer_store: SimplePatch<BufferConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub auth_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub blob_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub game_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub run_store: SimplePatch<StoreConfig>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub user_store: SimplePatch<StoreConfig>,
}
