use crate::{BuiltinEternalfestConfigProfile, ConfigValue};
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct FrontendConfig<TyMeta> {
  pub port: ConfigValue<u16, TyMeta>,
  pub uri: ConfigValue<Url, TyMeta>,
}

impl FrontendConfig<()> {
  pub(crate) fn patch(mut self, config: FrontendConfigPatch) -> Self {
    if let SimplePatch::Set(port) = config.port {
      self.port = ConfigValue::new(port);
      self.uri = ConfigValue::new({
        let mut u = Url::parse("http://localhost/").expect("computed `frontend.uri` is valid");
        u.set_port(Some(self.port.value)).expect("setting the port succeeds");
        u
      });
    }
    if let SimplePatch::Set(uri) = config.uri {
      self.uri = ConfigValue::new(uri);
    }
    self
  }

  pub(crate) fn default_for_profile(_profile: BuiltinEternalfestConfigProfile) -> Self {
    Self {
      port: ConfigValue::new(50313),
      uri: ConfigValue::new(Url::parse("http://localhost:50313/").expect("default `frontend.uri` is valid")),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct FrontendConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub port: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub uri: SimplePatch<Url>,
}
