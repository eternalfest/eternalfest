use crate::ConfigValue;
use eternalfest_core::core::Duration;
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use std::collections::BTreeMap;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct ExporterConfig<TyMeta> {
  pub r#type: ConfigValue<String, TyMeta>,
  pub color: ConfigValue<String, TyMeta>,
  pub target: ConfigValue<String, TyMeta>,
  pub endpoint: ConfigValue<Url, TyMeta>,
  pub timeout: ConfigValue<Duration, TyMeta>,
  pub metadata: ConfigValue<BTreeMap<String, ConfigValue<String, TyMeta>>, TyMeta>,
}

impl ExporterConfig<()> {
  pub(crate) fn patch_ref(&mut self, config: PartialExporterConfig) {
    if let SimplePatch::Set(typ) = config.r#type {
      self.r#type = ConfigValue::new(typ);
    }
    if let SimplePatch::Set(color) = config.color {
      self.color = ConfigValue::new(color);
    }
    if let SimplePatch::Set(target) = config.target {
      self.target = ConfigValue::new(target);
    }
    if let SimplePatch::Set(endpoint) = config.endpoint {
      self.endpoint = ConfigValue::new(endpoint);
    }
    if let SimplePatch::Set(timeout) = config.timeout {
      self.timeout = ConfigValue::new(timeout);
    }
    if let SimplePatch::Set(metadata) = config.metadata {
      match metadata {
        None => self.metadata.value.clear(),
        Some(metadata) => {
          for (key, value) in metadata {
            match value {
              None => self.metadata.value.remove(&key),
              Some(value) => self.metadata.value.insert(key, ConfigValue::new(value)),
            };
          }
        }
      }
    }
  }

  pub(crate) fn default_for_key(_key: &str) -> Self {
    Self {
      r#type: ConfigValue::new("OtlpHttp".to_string()),
      color: ConfigValue::new("Auto".to_string()),
      target: ConfigValue::new("eternalfest://stdout".to_string()),
      endpoint: ConfigValue::new(Url::parse("eternalfest://eternaltwin/").expect("default endpoint is valid")),
      timeout: ConfigValue::new(Duration::from_seconds(5)),
      metadata: ConfigValue::new(BTreeMap::new()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PartialExporterConfig {
  /// Exporter type:
  /// - `OtlpHttp`: Opentelemetry Protocol (HTTP variant)
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub r#type: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub color: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub target: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub endpoint: SimplePatch<Url>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub timeout: SimplePatch<Duration>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub metadata: SimplePatch<Option<BTreeMap<String, Option<String>>>>,
}
