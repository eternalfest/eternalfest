mod exporter;

use crate::opentelemetry::exporter::{ExporterConfig, PartialExporterConfig};
use crate::{BuiltinEternalfestConfigProfile, ConfigValue};
use eternalfest_core::core::Duration;
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use std::collections::btree_map::Entry;
use std::collections::BTreeMap;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct OpentelemetryConfig<TyMeta> {
  pub enabled: ConfigValue<bool, TyMeta>,
  pub attributes: ConfigValue<BTreeMap<String, ConfigValue<String, TyMeta>>, TyMeta>,
  pub exporter: ConfigValue<BTreeMap<String, ExporterConfig<TyMeta>>, TyMeta>,
}

impl OpentelemetryConfig<()> {
  pub(crate) fn patch(mut self, config: OpentelemetryConfigPatch) -> Self {
    if let SimplePatch::Set(enabled) = config.enabled {
      self.enabled = ConfigValue::new(enabled);
    }
    if let SimplePatch::Set(attributes) = config.attributes {
      match attributes {
        None => self.attributes.value.clear(),
        Some(attributes) => {
          for (key, value) in attributes {
            match value {
              None => self.attributes.value.remove(&key),
              Some(value) => self.attributes.value.insert(key, ConfigValue::new(value)),
            };
          }
        }
      }
    }
    if let SimplePatch::Set(exporter) = config.exporter {
      match exporter {
        None => self.exporter.value.clear(),
        Some(exporter) => {
          for (key, exporter) in exporter {
            match exporter {
              None => drop(self.exporter.value.remove(&key)),
              Some(exporter) => {
                match self.exporter.value.entry(key) {
                  Entry::Vacant(e) => {
                    let c = ExporterConfig::default_for_key(e.key());
                    e.insert(c).patch_ref(exporter)
                  }
                  Entry::Occupied(mut e) => e.get_mut().patch_ref(exporter),
                };
              }
            }
          }
        }
      }
    }
    self
  }

  pub(crate) fn default_for_profile(profile: BuiltinEternalfestConfigProfile) -> Self {
    use BuiltinEternalfestConfigProfile::*;
    Self {
      enabled: ConfigValue::new(true),
      attributes: ConfigValue::new(
        [
          ("service.name".to_string(), ConfigValue::new("eternalfest".to_string())),
          (
            "deployment.environment".to_string(),
            ConfigValue::new("dev".to_string()),
          ),
          (
            "service.version".to_string(),
            ConfigValue::new(env!("CARGO_PKG_VERSION").to_string()),
          ),
        ]
        .into_iter()
        .collect(),
      ),
      exporter: ConfigValue::new(match profile {
        Production | Test => BTreeMap::new(),
        Dev => [(
          "eternaltwin".to_string(),
          ExporterConfig {
            r#type: ConfigValue::new("OtlpHttp".to_string()),
            color: ConfigValue::new("Auto".to_string()),
            target: ConfigValue::new("eternalfest://stdout".to_string()),
            endpoint: ConfigValue::new(Url::parse("eternalfest://eternaltwin/").expect("default endpoint is valid")),
            timeout: ConfigValue::new(Duration::from_seconds(5)),
            metadata: ConfigValue::new(BTreeMap::new()),
          },
        )]
        .into_iter()
        .collect(),
        Sdk => [(
          "eternaltwin".to_string(),
          ExporterConfig {
            r#type: ConfigValue::new("OtlpHttp".to_string()),
            color: ConfigValue::new("Never".to_string()),
            target: ConfigValue::new("eternalfest://stdout".to_string()),
            endpoint: ConfigValue::new(Url::parse("eternalfest://eternaltwin/").expect("default endpoint is valid")),
            timeout: ConfigValue::new(Duration::from_seconds(5)),
            metadata: ConfigValue::new(BTreeMap::new()),
          },
        )]
        .into_iter()
        .collect(),
      }),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct OpentelemetryConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub enabled: SimplePatch<bool>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub attributes: SimplePatch<Option<BTreeMap<String, Option<String>>>>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub exporter: SimplePatch<Option<BTreeMap<String, Option<PartialExporterConfig>>>>,
}
