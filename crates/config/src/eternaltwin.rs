use crate::{BuiltinEternalfestConfigProfile, ConfigValue};
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct EternaltwinConfig<TyMeta> {
  pub uri: ConfigValue<Url, TyMeta>,
  pub oauth_client_id: ConfigValue<String, TyMeta>,
  pub oauth_client_secret: ConfigValue<String, TyMeta>,
}

impl EternaltwinConfig<()> {
  pub(crate) fn patch(mut self, config: EternaltwinConfigPatch) -> Self {
    if let SimplePatch::Set(uri) = config.uri {
      self.uri = ConfigValue::new(uri);
    }
    if let SimplePatch::Set(oauth_client_id) = config.oauth_client_id {
      self.oauth_client_id = ConfigValue::new(oauth_client_id);
    }
    if let SimplePatch::Set(oauth_client_secret) = config.oauth_client_secret {
      self.oauth_client_secret = ConfigValue::new(oauth_client_secret);
    }
    self
  }

  pub(crate) fn default_for_profile(_profile: BuiltinEternalfestConfigProfile) -> Self {
    Self {
      uri: ConfigValue::new(Url::parse("http://localhost:50320/").expect("default `eternaltwin.uri` is valid")),
      oauth_client_id: ConfigValue::new("eternalfest_dev@clients".to_string()),
      oauth_client_secret: ConfigValue::new("dev".to_string()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct EternaltwinConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub uri: SimplePatch<Url>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub oauth_client_id: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub oauth_client_secret: SimplePatch<String>,
}
