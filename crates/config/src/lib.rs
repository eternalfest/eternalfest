pub mod backend;
pub mod data;
pub mod eternaltwin;
pub mod frontend;
pub mod load;
mod opentelemetry;
pub mod postgres;

use crate::backend::{BackendConfig, BackendConfigPatch};
use crate::data::{DataConfig, DataConfigPatch};
use crate::eternaltwin::{EternaltwinConfig, EternaltwinConfigPatch};
use crate::frontend::{FrontendConfig, FrontendConfigPatch};
use crate::load::{ConfigRoot, ConfigRoots};
use crate::opentelemetry::{OpentelemetryConfig, OpentelemetryConfigPatch};
use crate::postgres::{PostgresConfig, PostgresConfigPatch};
use eternalfest_core::patch::SimplePatch;
use eternalfest_core::types::{DisplayErrorChain, WeakError};
use serde::{Deserialize, Serialize};
use std::borrow::Borrow;
use std::collections::hash_map::Entry as HashMapEntry;
use std::collections::{HashMap, HashSet};
use std::convert::Infallible;
use std::fmt::Debug;
use std::fs;
use std::io;
use std::path::{Path, PathBuf};
use std::sync::RwLock;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(untagged)]
enum ConfigRefOrList {
  One(ConfigRef),
  Many(Vec<ConfigRef>),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
#[serde(untagged)]
enum ConfigRef {
  Url(String),
  Typed { url: String, format: ConfigFormat },
}

struct FsCache {
  state: RwLock<HashMap<PathBuf, io::Result<String>>>,
}

impl FsCache {
  pub fn new() -> Self {
    Self {
      state: RwLock::new(HashMap::new()),
    }
  }

  pub fn read_to_string<F, R>(&self, p: &Path, f: F) -> R
  where
    F: for<'a> Fn(&'a io::Result<String>) -> R,
  {
    {
      if let Some(r) = self.state.read().expect("lock is poisoned").get(p) {
        return f(r);
      }
    }
    let mut s = self.state.write().expect("lock is poisoned");
    match s.entry(p.to_path_buf()) {
      HashMapEntry::Vacant(e) => {
        let p = e.key();
        let r = fs::read_to_string(p);
        let r = e.insert(r);
        f(r)
      }
      HashMapEntry::Occupied(e) => f(e.get()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ConfigChain {
  pub items: Vec<ResolvedConfig>,
  pub default: BuiltinEternalfestConfigProfile,
}

impl ConfigChain {
  pub fn merge(self) -> Config {
    let mut cur = Config::default_for_profile(self.default);
    for item in self.items.into_iter().rev() {
      cur = cur.patch(item.config);
    }
    cur
  }

  #[cfg(test)]
  pub(crate) fn one(resolved: ResolvedConfig, default: BuiltinEternalfestConfigProfile) -> Self {
    Self {
      items: vec![resolved],
      default,
    }
  }

  pub fn resolve_from_roots(roots: &[ConfigRoot], working_dir: &Path, profile: &EternalfestConfigProfile) -> Self {
    let mut resolver = ConfigResolver::new();
    for root in roots.iter().rev() {
      resolver.add_root(root, working_dir)
    }
    Self {
      items: resolver.resolved,
      default: profile.as_builtin_or_dev(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
enum TryResolveUrlError {
  #[error("resource not found")]
  NotFound,
  #[error("unexpected URL scheme {0:?}")]
  Scheme(String),
  #[error("unexpected io error")]
  Io(#[source] WeakError),
}

struct ConfigResolver {
  resolved: Vec<ResolvedConfig>,
  fs: FsCache,
}

impl ConfigResolver {
  pub(crate) fn new() -> Self {
    Self {
      resolved: Vec::new(),
      fs: FsCache::new(),
    }
  }

  pub(crate) fn add_root(&mut self, root: &ConfigRoot, working_dir: &Path) {
    let working_dir_url = Url::from_directory_path(working_dir).expect("invalid working dir");
    let resolved = match root {
      ConfigRoot::Data { data, format } => Some(self.add_data(data, format)),
      ConfigRoot::Search { patterns, format } => self.add_search(patterns, format, working_dir.to_path_buf()),
      ConfigRoot::Url { url, format } => Some(self.add_url(&working_dir_url, url, format)),
    };
    let resolved: &ResolvedConfig = match resolved {
      None => return,
      Some(resolved) => resolved,
    };
    let mut stack = resolved.extends(&working_dir_url);
    while let Some((url, format)) = stack.pop() {
      let dependency = match self.try_resolve_url(&url, format) {
        Ok(dep) => dep,
        Err(e) => {
          panic!("failed to resolve base config: {e:?}");
        }
      };
      stack.extend_from_slice(&dependency.extends(&working_dir_url));
      self.resolved.push(dependency);
    }
  }

  fn add_data(
    &mut self,
    data: &ConfigValue<String, LoadConfigSource>,
    format: &ConfigValue<ConfigFormat, LoadConfigSource>,
  ) -> &ResolvedConfig {
    let resolved_source: ConfigSource = match data.meta {
      LoadConfigSource::Args => ConfigSource::Cli,
      LoadConfigSource::Env => ConfigSource::Env,
      LoadConfigSource::Default(_) => ConfigSource::Default,
    };
    let resolved = ResolvedConfig::parse(&data.value, format.value, true, resolved_source).expect("invalid config");
    self.resolved.push(resolved);
    self.resolved.last().expect("`self.resolved` is non-empty")
  }

  fn add_search(
    &mut self,
    patterns: &ConfigValue<Vec<String>, LoadConfigSource>,
    format: &ConfigValue<ConfigFormat, LoadConfigSource>,
    start_dir: PathBuf,
  ) -> Option<&ResolvedConfig> {
    let mut visited: HashSet<PathBuf> = HashSet::new();
    let mut cur: PathBuf = start_dir;
    loop {
      let cur_url = Url::from_directory_path(&cur).expect("invalid cur search dir");
      for pat in &patterns.value {
        let is_relative_path = pat.starts_with("./") || pat.starts_with("../");
        if !is_relative_path {
          panic!(r#"search pattern must start with "./" or "../", received {pat}"#);
        }
        let target = Url::options().base_url(Some(&cur_url)).parse(pat);
        let target = match target {
          Ok(t) => t,
          Err(e) => panic!("failed to build file url from pattern {pat:?} and base {cur_url}: {e:?}"),
        };
        match self.try_resolve_url(&target, format.value) {
          Ok(r) => {
            self.resolved.push(r);
            return Some(self.resolved.last().expect("`self.resolved` is non-empty"));
          }
          Err(TryResolveUrlError::NotFound) => continue,
          Err(e) => panic!("read error for {target}: {}", DisplayErrorChain(&e)),
        };
      }
      let parent: Option<PathBuf> = cur.parent().map(|p| p.to_path_buf());
      if let Some(parent) = parent {
        visited.insert(cur);
        if visited.contains(parent.as_path()) {
          // Found cycle when iterating through parents; we assume that we
          // reached the root on an OS where Rust has trouble noticing it.
          return None;
        } else {
          cur = parent;
        }
      } else {
        // Reached root without finding any match
        return None;
      }
    }
  }

  fn add_url(
    &mut self,
    working_dir: &Url,
    url: &ConfigValue<String, LoadConfigSource>,
    format: &ConfigValue<ConfigFormat, LoadConfigSource>,
  ) -> &ResolvedConfig {
    let url = resolve_config_ref(working_dir, &url.value);
    match self.try_resolve_url(&url, format.value) {
      Ok(r) => {
        self.resolved.push(r);
        self.resolved.last().expect("`self.resolved` is non-empty")
      }
      Err(e) => panic!("read error for {url}: {}", DisplayErrorChain(&e)),
    }
  }

  fn try_resolve_url(&self, url: &Url, format: ConfigFormat) -> Result<ResolvedConfig, TryResolveUrlError> {
    match url.scheme() {
      "file" => {
        let path = url.to_file_path().expect("must be a file url");
        self.fs.read_to_string(&path, |r| match r.as_deref() {
          Ok(s) => {
            let resolved_source = ConfigSource::File(Some(path.clone()));
            let resolved = ResolvedConfig::parse(s, format, false, resolved_source).expect("config format error");
            Ok(resolved)
          }
          Err(e) => Err(match e.kind() {
            io::ErrorKind::NotFound => TryResolveUrlError::NotFound,
            _ => TryResolveUrlError::Io(WeakError::wrap(e)),
          }),
        })
      }
      s => Err(TryResolveUrlError::Scheme(s.to_string())),
    }
  }
}

fn resolve_config_ref(base: &Url, url: &str) -> Url {
  let parsed = if url.starts_with("./") || url.starts_with("../") {
    Url::options().base_url(Some(base)).parse(url)
  } else {
    Url::parse(url)
  };
  match parsed {
    Ok(u) => u,
    Err(e) => panic!("invalid url {url}: {}", DisplayErrorChain(&e)),
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub struct ResolvedConfig {
  pub source: ConfigSource,
  format: ConfigFormat,
  config: ConfigPatch,
}

impl ResolvedConfig {
  pub fn parse(s: &str, format: ConfigFormat, prefer_json: bool, source: ConfigSource) -> Result<Self, WeakError> {
    let (format, partial) = match format {
      ConfigFormat::Auto => {
        if prefer_json {
          match serde_json::from_str(s) {
            Ok(p) => (ConfigFormat::Json, p),
            Err(e) => toml::de::from_str(s)
              .map(|p| (ConfigFormat::Toml, p))
              .map_err(|_| WeakError::wrap(e))?,
          }
        } else {
          match toml::de::from_str(s) {
            Ok(p) => (ConfigFormat::Toml, p),
            Err(e) => serde_json::from_str(s)
              .map(|p| (ConfigFormat::Json, p))
              .map_err(|_| WeakError::wrap(e))?,
          }
        }
      }
      ConfigFormat::Json => (ConfigFormat::Json, serde_json::from_str(s).map_err(WeakError::wrap)?),
      ConfigFormat::Toml => (ConfigFormat::Toml, toml::de::from_str(s).map_err(WeakError::wrap)?),
    };
    Ok(Self {
      source,
      format,
      config: partial,
    })
  }

  pub fn extends(&self, working_dir: &Url) -> Vec<(Url, ConfigFormat)> {
    let extends = match self.config.extends.as_ref() {
      Some(extends) => extends,
      None => return Vec::new(),
    };

    let base_url = match &self.source {
      ConfigSource::File(Some(file)) => Url::from_file_path(file).expect("invalid file path"),
      _ => working_dir.clone(),
    };

    match extends {
      ConfigRefOrList::One(e) => Self::extends_inner(&base_url, &[e]),
      ConfigRefOrList::Many(e) => Self::extends_inner(&base_url, e),
    }
  }

  fn extends_inner<Cr: Borrow<ConfigRef>>(base_url: &Url, extends: &[Cr]) -> Vec<(Url, ConfigFormat)> {
    let mut result: Vec<(Url, ConfigFormat)> = Vec::new();
    for e in extends {
      let (url, format) = match e.borrow() {
        ConfigRef::Url(u) => (u, ConfigFormat::Auto),
        ConfigRef::Typed { url, format } => (url, *format),
      };
      let url = resolve_config_ref(base_url, url);
      result.push((url, format));
    }
    result
  }
}

/// Trait representing a config field.
///
/// A config field is path to a value. This is the main way to extract values
/// from a config object.
pub trait ConfigField<TyContainer, TyValue> {
  /// Path, for debug purposes
  fn path(&self) -> &[&str];

  fn get(&self, container: TyContainer) -> TyValue;
}

/// Value extracted from config.
///
/// The value is always tagged with metadata.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct ConfigValue<T, M = ()> {
  pub value: T,
  pub meta: M,
}

impl<T, M> ConfigValue<T, M>
where
  M: Default,
{
  pub fn new(value: T) -> Self {
    Self {
      value,
      meta: M::default(),
    }
  }
}

/// Entry from config: field and corresponding value.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ConfigEntry<F, V> {
  pub field: F,
  pub value: V,
}

pub type DebugConfigValue<T> = ConfigValue<T, ConfigMeta<T, LoadConfigSource>>;

pub type DebugConfigEntry<'a> = ConfigEntry<&'a [&'a str], DebugConfigValue<&'a dyn Debug>>;

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum LoadConfigSource {
  Args,
  Env,
  Default(BuiltinEternalfestConfigProfile),
}

/// Metadata for a config value
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ConfigMeta<V, S> {
  pub default: V,
  pub source: S,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum BuiltinEternalfestConfigProfile {
  Production,
  Dev,
  Test,
  Sdk,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum EternalfestConfigProfile {
  Production,
  Dev,
  Test,
  Sdk,
  Custom(String),
}

impl EternalfestConfigProfile {
  pub fn default_sources(&self) -> Vec<ConfigSource> {
    todo!()
  }

  pub fn as_str(&self) -> &str {
    match &self {
      Self::Production => "production",
      Self::Dev => "dev",
      Self::Test => "test",
      Self::Sdk => "sdk",
      Self::Custom(ref profile) => profile.as_str(),
    }
  }

  pub fn parse(s: &str) -> Self {
    match s {
      "production" => Self::Production,
      "dev" => Self::Dev,
      "test" => Self::Test,
      "sdk" => Self::Sdk,
      s => Self::Custom(s.to_string()),
    }
  }

  pub fn as_builtin_or_dev(&self) -> BuiltinEternalfestConfigProfile {
    match &self {
      Self::Production => BuiltinEternalfestConfigProfile::Production,
      Self::Dev => BuiltinEternalfestConfigProfile::Dev,
      Self::Test => BuiltinEternalfestConfigProfile::Test,
      Self::Sdk => BuiltinEternalfestConfigProfile::Sdk,
      Self::Custom(_) => BuiltinEternalfestConfigProfile::Dev,
    }
  }
}

impl core::str::FromStr for EternalfestConfigProfile {
  type Err = Infallible;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    Ok(Self::parse(s))
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub enum ConfigFormat {
  Auto,
  Json,
  Toml,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[error("invalid config format: possible values are `auto`, `json`, `toml`")]
pub struct InvalidConfigFormat;

impl ConfigFormat {
  pub fn as_str(&self) -> &str {
    match &self {
      Self::Auto => "auto",
      Self::Json => "json",
      Self::Toml => "toml",
    }
  }

  pub fn parse(s: &str) -> Result<Self, InvalidConfigFormat> {
    match s {
      "auto" => Ok(Self::Auto),
      "json" => Ok(Self::Json),
      "toml" => Ok(Self::Toml),
      _ => Err(InvalidConfigFormat),
    }
  }
}

impl core::str::FromStr for ConfigFormat {
  type Err = InvalidConfigFormat;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    Self::parse(s)
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum ConfigSource {
  Default,
  File(Option<PathBuf>),
  Cli,
  Env,
}

pub enum InputConfigSource {
  Default,
  Cwd(PathBuf),
  Cli,
  Env,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum ClockConfig {
  System,
  Virtual,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum BufferConfig {
  Memory,
  Fs,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub enum StoreConfig {
  Memory,
  Postgres,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
pub struct Config<TyMeta = ()> {
  pub backend: BackendConfig<TyMeta>,
  pub frontend: FrontendConfig<TyMeta>,
  pub postgres: PostgresConfig<TyMeta>,
  pub data: DataConfig<TyMeta>,
  pub eternaltwin: EternaltwinConfig<TyMeta>,
  pub opentelemetry: OpentelemetryConfig<TyMeta>,
}

impl Config<()> {
  pub fn for_test() -> Self {
    let cwd = std::env::current_dir().expect("failed to resolve cwd");
    let profile = EternalfestConfigProfile::Test;
    let roots = ConfigRoots::default_value(&profile);
    let chain = ConfigChain::resolve_from_roots(&roots, cwd.as_path(), &profile);
    chain.merge()
  }

  fn patch(mut self, config: ConfigPatch) -> Self {
    if let SimplePatch::Set(backend) = config.backend {
      self.backend = self.backend.patch(backend);
    }
    if let SimplePatch::Set(frontend) = config.frontend {
      self.frontend = self.frontend.patch(frontend);
    }
    if let SimplePatch::Set(postgres) = config.postgres {
      self.postgres = self.postgres.patch(postgres);
    }
    if let SimplePatch::Set(data) = config.data {
      self.data = self.data.patch(data);
    }
    if let SimplePatch::Set(eternaltwin) = config.eternaltwin {
      self.eternaltwin = self.eternaltwin.patch(eternaltwin);
    }
    if let SimplePatch::Set(opentelemetry) = config.opentelemetry {
      self.opentelemetry = self.opentelemetry.patch(opentelemetry);
    }
    // if let SimplePatch::Set(seed) = config.seed {
    //   let patch = match seed {
    //     // An explicit `seed = null` is propagated to clear all the seed maps
    //     None => SeedConfigPatch {
    //       user: SimplePatch::Set(None),
    //       app: SimplePatch::Set(None),
    //       forum_section: SimplePatch::Set(None),
    //     },
    //     Some(patch) => patch,
    //   };
    //   self.seed = self.seed.patch(patch);
    // }
    self
  }

  fn default_for_profile(profile: BuiltinEternalfestConfigProfile) -> Self {
    Self {
      backend: BackendConfig::default_for_profile(profile),
      frontend: FrontendConfig::default_for_profile(profile),
      postgres: PostgresConfig::default_for_profile(profile),
      data: DataConfig::default_for_profile(profile),
      eternaltwin: EternaltwinConfig::default_for_profile(profile),
      opentelemetry: OpentelemetryConfig::default_for_profile(profile),
    }
  }
}

impl<TyMeta> Config<TyMeta> {
  pub fn data_root(&self) -> PathBuf {
    let root = self.data.root.value.as_str();
    if root.starts_with("./") || root.starts_with("../") {
      std::env::current_dir().expect("cwd is valid").join(root)
    } else {
      Url::parse(root)
        .expect("data root is valid")
        .to_file_path()
        .expect("data root is a valid file uri")
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
struct ConfigPatch {
  extends: Option<ConfigRefOrList>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  backend: SimplePatch<BackendConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  frontend: SimplePatch<FrontendConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  postgres: SimplePatch<PostgresConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  data: SimplePatch<DataConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  eternaltwin: SimplePatch<EternaltwinConfigPatch>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  opentelemetry: SimplePatch<OpentelemetryConfigPatch>,
}

#[derive(Debug)]
pub enum FindConfigFileError {
  NotFound(PathBuf),
  Other(PathBuf, io::Error),
}

#[derive(Debug, thiserror::Error)]
pub enum FindConfigError {
  #[error("config file not found, searching from {:?}", .0)]
  NotFound(PathBuf),
  #[error("config file parse error")]
  ParseError(#[source] toml::de::Error),
  #[error("I/O config file error, searching from {:?}", .0)]
  Other(PathBuf, #[source] io::Error),
}

#[cfg(test)]
mod test {
  use super::*;

  #[test]
  fn test_default_config() {
    // language=toml
    const INPUT: &str = r#"
[backend]
listen = "[::]:50314"
clock = "System"
auth_store = "Memory"
blob_store = "Memory"
buffer_store = "Memory"
game_store = "Memory"
run_store = "Memory"
user_store = "Memory"
secret = "dev"
cookie_key = "dev"

[frontend]
port = 50313
uri = "http://localhost:50313/"

[postgres]
host = "localhost"
port = 5432
name = "eternalfest.dev"
user = "eternalfest.dev.main"
password = "dev"

[data]
root = "./data/"

[eternaltwin]
uri = "http://localhost:50320/"
oauth_client_id = "eternalfest_dev@clients"
secret = "dev"

[opentelemetry]
enabled = true
attributes = {"deployment.environment" = "dev"}
[opentelemetry.exporter.eternaltwin]
type = "OtlpHttp"
color = "Auto"
target = "eternalfest://stdout"
endpoint = "eternalfest://eternaltwin/"
timeout = "5s"
metadata = {}
    "#;
    let actual = ResolvedConfig::parse(INPUT, ConfigFormat::Toml, false, ConfigSource::Default).unwrap();
    let actual = ConfigChain::one(actual, BuiltinEternalfestConfigProfile::Production).merge();
    let expected = Config::default_for_profile(BuiltinEternalfestConfigProfile::Dev);
    assert_eq!(actual, expected);
  }
}
