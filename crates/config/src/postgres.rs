use crate::{BuiltinEternalfestConfigProfile, ConfigValue};
use eternalfest_core::patch::SimplePatch;
use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PostgresConfig<TyMeta> {
  pub host: ConfigValue<String, TyMeta>,
  pub port: ConfigValue<u16, TyMeta>,
  pub name: ConfigValue<String, TyMeta>,
  pub user: ConfigValue<String, TyMeta>,
  pub password: ConfigValue<String, TyMeta>,
  pub admin_user: ConfigValue<String, TyMeta>,
  pub admin_password: ConfigValue<String, TyMeta>,
  pub max_connections: ConfigValue<u32, TyMeta>,
}

impl PostgresConfig<()> {
  pub(crate) fn patch(mut self, config: PostgresConfigPatch) -> Self {
    if let SimplePatch::Set(host) = config.host {
      self.host = ConfigValue::new(host);
    }
    if let SimplePatch::Set(port) = config.port {
      self.port = ConfigValue::new(port);
    }
    if let SimplePatch::Set(name) = config.name {
      self.name = ConfigValue::new(name);
    }
    if let SimplePatch::Set(user) = config.user {
      self.user = ConfigValue::new(user.clone());
      self.admin_user = ConfigValue::new(user);
    }
    if let SimplePatch::Set(password) = config.password {
      self.password = ConfigValue::new(password.clone());
      self.admin_password = ConfigValue::new(password);
    }
    if let SimplePatch::Set(admin_user) = config.admin_user {
      self.admin_user = ConfigValue::new(admin_user);
    }
    if let SimplePatch::Set(admin_password) = config.admin_password {
      self.admin_password = ConfigValue::new(admin_password);
    }
    if let SimplePatch::Set(max_connections) = config.max_connections {
      self.max_connections = ConfigValue::new(max_connections);
    }
    self
  }

  pub(crate) fn default_for_profile(profile: BuiltinEternalfestConfigProfile) -> Self {
    use BuiltinEternalfestConfigProfile::*;
    Self {
      host: ConfigValue::new("localhost".to_string()),
      port: ConfigValue::new(5432),
      name: ConfigValue::new(match profile {
        Production => "eternalfest.production".to_string(),
        Dev | Sdk | Test => "eternalfest.dev".to_string(),
      }),
      user: ConfigValue::new(match profile {
        Production => "eternalfest.production.main".to_string(),
        Dev | Sdk | Test => "eternalfest.dev.main".to_string(),
      }),
      password: ConfigValue::new("dev".to_string()),
      admin_user: ConfigValue::new(match profile {
        Production => "eternalfest.production.admin".to_string(),
        Dev | Sdk | Test => "eternalfest.dev.main".to_string(),
      }),
      admin_password: ConfigValue::new("dev".to_string()),
      max_connections: ConfigValue::new(50),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Serialize, Deserialize)]
#[non_exhaustive]
pub struct PostgresConfigPatch {
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub host: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub port: SimplePatch<u16>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub name: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub user: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub password: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub admin_user: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub admin_password: SimplePatch<String>,
  #[serde(default, skip_serializing_if = "SimplePatch::is_skip")]
  pub max_connections: SimplePatch<u32>,
}
