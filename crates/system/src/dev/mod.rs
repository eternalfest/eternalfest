use eternalfest_core::clock::VirtualClock;
use std::sync::Arc;

#[derive(Clone)]
pub struct DevApi {
  pub clock: Option<Arc<VirtualClock>>,
}

impl DevApi {
  pub fn new() -> Self {
    Self { clock: None }
  }
}

impl Default for DevApi {
  fn default() -> Self {
    Self::new()
  }
}
