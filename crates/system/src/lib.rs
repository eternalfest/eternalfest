extern crate core;

mod dev;
mod opentelemetry;

use eternalfest_config::{BufferConfig, ClockConfig, Config, StoreConfig};
use std::collections::HashMap;

pub use crate::dev::DevApi;
use crate::opentelemetry::hyper_client::HyperClient;
use ::opentelemetry::KeyValue;
use ::opentelemetry_otlp::WithExportConfig;
use ::opentelemetry_sdk::resource::TelemetryResourceDetector;
use ::opentelemetry_sdk::Resource;
use eternalfest_auth_store::mem::MemAuthStore;
use eternalfest_auth_store::pg::PgAuthStore;
use eternalfest_blob_store::mem::MemBlobStore;
use eternalfest_blob_store::pg::PgBlobStore;
use eternalfest_buffer_store::fs::FsBufferStore;
use eternalfest_buffer_store::mem::MemBufferStore;
use eternalfest_config::postgres::PostgresConfig;
use eternalfest_core::auth::AuthStore;
use eternalfest_core::blob::BlobStore;
use eternalfest_core::buffer::BufferStore;
use eternalfest_core::clock::{Clock, DynScheduler, ErasedScheduler, Scheduler, SystemClock, VirtualClock};
use eternalfest_core::core::{Instant, SecretBytes};
use eternalfest_core::game::GameStore;
use eternalfest_core::opentelemetry::{ArcDynLoggerProvider, ArcDynTracerProvider};
use eternalfest_core::run::RunStore;
use eternalfest_core::types::WeakError;
use eternalfest_core::user::UserStore;
use eternalfest_core::uuid::{Uuid4Generator, UuidGenerator};
use eternalfest_core::OwnRef;
use eternalfest_game_store::mem::MemGameStore;
use eternalfest_game_store::pg::PgGameStore;
use eternalfest_run_store::mem::MemRunStore;
use eternalfest_run_store::pg::PgRunStore;
use eternalfest_services::auth::{AuthService, DynAuthService};
use eternalfest_services::blob::{BlobService, DynBlobService};
use eternalfest_services::game::{DynGameService, GameService};
use eternalfest_services::run::{DynRunService, RunService};
use eternalfest_services::user::{DynUserService, UserService};
use eternalfest_user_store::mem::MemUserStore;
use eternalfest_user_store::pg::PgUserStore;
use eternaltwin_client::http::HttpEternaltwinClient;
use eternaltwin_client::EtwinClient;
use eternaltwin_oauth_client::http::{HttpRfcOauthClient, HttpRfcOauthClientOptions};
use eternaltwin_oauth_client::RfcOauthClient;
use http::header::AUTHORIZATION;
use opentelemetry_sdk::logs::BatchLogProcessor;
use opentelemetry_sdk::resource::ResourceDetector;
use opentelemetry_sdk::trace::BatchSpanProcessor;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;
use std::future::Future;
use std::path::PathBuf;
use std::pin::Pin;
use std::sync::Arc;
use std::time::Duration;
use url::Url;

/// On-demand and cached pg pool factory
struct LazyPgPool<'a, ConfigMeta> {
  config: &'a PostgresConfig<ConfigMeta>,
  pool: Option<PgPool>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
pub enum GetPgPoolError {
  #[error("missing database configuration")]
  MissingConfig,
  #[error("failed to connect to the database")]
  Connect(#[from] WeakError),
}

impl<'a, ConfigMeta> LazyPgPool<'a, ConfigMeta> {
  pub fn new(config: &'a PostgresConfig<ConfigMeta>) -> Self {
    Self { config, pool: None }
  }

  pub async fn get(&mut self) -> Result<PgPool, GetPgPoolError> {
    let pool = match &self.pool {
      Some(pool) => pool,
      None => {
        let pool: PgPool = PgPoolOptions::new()
          .max_connections(self.config.max_connections.value)
          .connect_with(
            PgConnectOptions::new()
              .host(&self.config.host.value)
              .port(self.config.port.value)
              .database(&self.config.name.value)
              .username(&self.config.user.value)
              .password(&self.config.password.value),
          )
          .await
          .map_err(WeakError::wrap)?;

        self.pool.insert(pool)
      }
    };
    Ok(pool.clone())
  }
}

#[derive(Clone)]
pub struct EternalfestSystem {
  pub dev: DevApi,
  pub auth: Arc<DynAuthService>,
  pub blob: Arc<DynBlobService>,
  pub clock: Arc<dyn Scheduler<Timer = Pin<Box<dyn Future<Output = ()> + Send>>>>,
  pub game: Arc<DynGameService>,
  pub run: Arc<DynRunService>,
  pub user: Arc<DynUserService>,
  pub logger_provider: ArcDynLoggerProvider,
  pub tracer_provider: ArcDynTracerProvider,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error)]
#[allow(clippy::enum_variant_names)]
pub enum CreateSystemError {
  #[error("failed to acquire database pool")]
  PgPool(#[from] GetPgPoolError),
  #[error("missing backend.secret")]
  MissingBackendSecret,
  #[error("invalid OTLP exporter endpoint")]
  OtlpEndpoint(#[source] WeakError),
  #[error("invalid eternaltwin credentials")]
  EternaltwinCredentials(#[source] WeakError),
  #[error("multiple Opentelemetry exporters try to acquire stdout")]
  StdoutContention,
  #[error("unknown Opentelemetry exporter type {0:?}")]
  UnknownExporterType(String),
}

impl EternalfestSystem {
  pub async fn create_dyn<ConfigMeta, TyStdout>(
    config: &Config<ConfigMeta>,
    _stdout: TyStdout,
  ) -> Result<Self, CreateSystemError>
  where
    TyStdout: std::io::Write + Send + Sync + 'static,
  {
    let eternaltwin_authorization_header = typed_headers::Credentials::basic(
      &config.eternaltwin.oauth_client_id.value,
      &config.eternaltwin.oauth_client_secret.value,
    )
    .map_err(|e| CreateSystemError::EternaltwinCredentials(WeakError::wrap(e)))?
    .to_string();

    // TODO: Seeded RNG (use clock + seed)
    let uuid_generator: Arc<dyn UuidGenerator> = Arc::new(Uuid4Generator);

    let (logger_provider, tracer_provider): (ArcDynLoggerProvider, ArcDynTracerProvider) =
      if config.opentelemetry.enabled.value {
        let system_resource = {
          let process = Resource::new(
            config
              .opentelemetry
              .attributes
              .value
              .iter()
              .map(|(key, value)| KeyValue::new(key.to_string(), value.value.to_string())),
          );
          let telemetry: Resource = TelemetryResourceDetector.detect(Duration::from_secs(0));
          Resource::merge(&telemetry, &process)
        };

        let mut logger_provider_builder =
          opentelemetry_sdk::logs::LoggerProvider::builder().with_resource(system_resource.clone());

        let mut tracer_provider_builder = opentelemetry_sdk::trace::TracerProvider::builder().with_config(
          opentelemetry_sdk::trace::Config::default()
            .with_id_generator(opentelemetry::IdGenerator::new(Arc::clone(&uuid_generator)))
            .with_resource(system_resource.clone()),
        );

        for exporter_config in config.opentelemetry.exporter.value.values() {
          let (log_processor, span_processor): (_, _) = match exporter_config.r#type.value.as_str() {
            "OtlpHttp" => {
              let exporter = opentelemetry_otlp::new_exporter()
                .http()
                .with_endpoint(
                  otlp_url(
                    &exporter_config.endpoint.value,
                    ["v1", "logs"],
                    &config.eternaltwin.uri.value,
                  )
                  .map_err(CreateSystemError::OtlpEndpoint)?
                  .to_string(),
                )
                .with_timeout(exporter_config.timeout.value.into_std())
                .with_http_client(HyperClient::default())
                .with_headers({
                  let mut headers = HashMap::<String, String>::with_capacity(exporter_config.metadata.value.len());
                  headers.insert(AUTHORIZATION.to_string(), eternaltwin_authorization_header.clone());
                  for (key, value) in exporter_config.metadata.value.iter() {
                    headers.insert(key.clone(), value.value.clone());
                  }
                  headers
                });
              let log_processor = BatchLogProcessor::builder(
                exporter.build_log_exporter().expect("grpc exporter is valid"),
                opentelemetry_sdk::runtime::Tokio,
              )
              .build();
              let exporter = opentelemetry_otlp::new_exporter()
                .http()
                .with_endpoint(
                  otlp_url(
                    &exporter_config.endpoint.value,
                    ["v1", "traces"],
                    &config.eternaltwin.uri.value,
                  )
                  .map_err(CreateSystemError::OtlpEndpoint)?
                  .to_string(),
                )
                .with_timeout(exporter_config.timeout.value.into_std())
                .with_http_client(HyperClient::default())
                .with_headers({
                  let mut headers = HashMap::<String, String>::with_capacity(exporter_config.metadata.value.len());
                  headers.insert(AUTHORIZATION.to_string(), eternaltwin_authorization_header.clone());
                  for (key, value) in exporter_config.metadata.value.iter() {
                    headers.insert(key.clone(), value.value.clone());
                  }
                  headers
                });
              let span_processor = BatchSpanProcessor::builder(
                exporter.build_span_exporter().expect("grpc exporter is valid"),
                opentelemetry_sdk::runtime::Tokio,
              )
              .build();
              (log_processor, span_processor)
            }
            ty => return Err(CreateSystemError::UnknownExporterType(ty.to_string())),
          };
          logger_provider_builder = logger_provider_builder.with_log_processor(log_processor);
          tracer_provider_builder = tracer_provider_builder.with_span_processor(span_processor);
        }
        let logger_provider = logger_provider_builder.build();
        let tracer_provider = tracer_provider_builder.build();
        (
          ArcDynLoggerProvider::regular(logger_provider),
          ArcDynTracerProvider::regular(tracer_provider),
        )
      } else {
        (ArcDynLoggerProvider::noop(), ArcDynTracerProvider::noop())
      };

    let eternaltwin_url = config.eternaltwin.uri.value.as_str();

    let dev = DevApi::new();
    let (clock, scheduler): (Arc<dyn Clock>, Arc<DynScheduler>) = match config.backend.clock.value {
      ClockConfig::System => {
        let clock = Arc::new(ErasedScheduler::new(SystemClock));
        let scheduler = Arc::clone(&clock);
        (clock, scheduler)
      }
      ClockConfig::Virtual => {
        let clock = Arc::new(ErasedScheduler::new(VirtualClock::new(Instant::ymd_hms(
          2020, 1, 1, 0, 0, 0,
        ))));
        let scheduler = Arc::clone(&clock);
        (clock, scheduler)
      }
    };
    let mut db = LazyPgPool::new(&config.postgres);
    let secret_str: String = match config.backend.secret.value.clone() {
      Some(secret) => secret,
      None => return Err(CreateSystemError::MissingBackendSecret),
    };
    let secret_bytes = secret_str.as_bytes().to_vec();

    let auth_store: Arc<dyn AuthStore> = match config.backend.auth_store.value {
      StoreConfig::Postgres => Arc::new(PgAuthStore::new(
        Arc::clone(&clock),
        OwnRef(db.get().await?),
        Arc::clone(&uuid_generator),
      )),
      StoreConfig::Memory => Arc::new(MemAuthStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator))),
    };
    let buffer_store: Arc<dyn BufferStore> = match config.backend.buffer_store.value {
      BufferConfig::Memory => Arc::new(MemBufferStore::new(Arc::clone(&uuid_generator))),
      BufferConfig::Fs => {
        let root: PathBuf = config.data_root();
        Arc::new(FsBufferStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator), root).await)
      }
    };
    let blob_store: Arc<dyn BlobStore> = match config.backend.blob_store.value {
      StoreConfig::Memory => Arc::new(MemBlobStore::new(
        Arc::clone(&buffer_store),
        Arc::clone(&clock),
        Arc::clone(&uuid_generator),
      )),
      StoreConfig::Postgres => Arc::new(
        PgBlobStore::new(
          Arc::clone(&buffer_store),
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          Arc::clone(&uuid_generator),
        )
        .await
        .expect("initializing the blob store succeeds"),
      ),
    };
    let game_store: Arc<dyn GameStore> = match config.backend.game_store.value {
      StoreConfig::Memory => Arc::new(MemGameStore::new()),
      StoreConfig::Postgres => {
        Arc::new(PgGameStore::new(Arc::clone(&clock), OwnRef(db.get().await?), Arc::clone(&uuid_generator)).await)
      }
    };
    let run_store: Arc<dyn RunStore> = match config.backend.run_store.value {
      StoreConfig::Memory => Arc::new(MemRunStore::new()),
      StoreConfig::Postgres => Arc::new(
        PgRunStore::new(
          Arc::clone(&clock),
          OwnRef(db.get().await?),
          Arc::clone(&uuid_generator),
          eternaltwin_url.parse().expect("invalid eternaltwin uri"),
        )
        .expect("building the run store succeeds"),
      ),
    };
    let user_store: Arc<dyn UserStore> = match config.backend.user_store.value {
      StoreConfig::Postgres => Arc::new(PgUserStore::new(Arc::clone(&clock), OwnRef(db.get().await?))),
      StoreConfig::Memory => Arc::new(MemUserStore::new(Arc::clone(&clock))),
    };

    let eternaltwin_client: Arc<dyn EtwinClient> = Arc::new(HttpEternaltwinClient::new(
      Arc::clone(&clock),
      eternaltwin_url.parse().expect("eternaltwin url is valid"),
    ));
    let eternaltwin_oauth_client: Arc<dyn RfcOauthClient> =
      Arc::new(HttpRfcOauthClient::new(HttpRfcOauthClientOptions {
        authorization_endpoint: {
          let mut url: Url = eternaltwin_url.parse().expect("eternaltwin url is valid");
          url
            .path_segments_mut()
            .expect("eternaltwin url has path segments")
            .extend(["oauth", "authorize"]);
          url
        },
        token_endpoint: {
          let mut url: Url = eternaltwin_url.parse().expect("eternaltwin url is valid");
          url
            .path_segments_mut()
            .expect("eternaltwin url has path segments")
            .extend(["oauth", "token"]);
          url
        },
        callback_endpoint: {
          let mut url: Url = config.frontend.uri.value.clone();
          url
            .path_segments_mut()
            .expect("eternalfest url has path segments")
            .extend(["oauth", "callback"]);
          url
        },
        client_id: config.eternaltwin.oauth_client_id.value.clone(),
        client_secret: config.eternaltwin.oauth_client_secret.value.clone(),
      }));

    let auth = Arc::new(AuthService::new(
      Arc::clone(&auth_store),
      Arc::clone(&clock),
      Arc::clone(&eternaltwin_client),
      Arc::clone(&eternaltwin_oauth_client),
      Arc::clone(&user_store),
      SecretBytes::new(secret_bytes.clone()),
      SecretBytes::new(secret_bytes.clone()),
    ));

    let user = Arc::new(UserService::new(Arc::clone(&clock), Arc::clone(&user_store)));

    let blob = Arc::new(BlobService::new(Arc::clone(&blob_store), Arc::clone(&clock)));

    let game = Arc::new(GameService::new(
      Arc::clone(&blob_store),
      Arc::clone(&clock),
      Arc::clone(&game_store),
      Arc::clone(&run_store),
      Arc::clone(&user_store),
    ));

    let run = Arc::new(RunService::new(
      Arc::clone(&blob_store),
      Arc::clone(&clock),
      Arc::clone(&game_store),
      Arc::clone(&run_store),
      Arc::clone(&user_store),
    ));

    Ok(EternalfestSystem {
      dev,
      auth,
      blob,
      clock: scheduler,
      game,
      run,
      user,
      logger_provider,
      tracer_provider,
    })
  }

  pub async fn for_test() -> Self {
    let config = Config::for_test();
    let stdout = Vec::<u8>::new();
    Self::create_dyn(&config, stdout).await.expect("test config is valid")
  }
}

fn otlp_url(endpoint: &Url, path: [&str; 2], eternaltwin_url: &Url) -> Result<Url, WeakError> {
  let mut http_endpoint: Url = match endpoint.scheme() {
    "eternalfest" => {
      if endpoint.authority() == "eternaltwin" && endpoint.path() == "/" {
        if matches!(eternaltwin_url.scheme(), "http" | "https") {
          eternaltwin_url.clone()
        } else {
          return Err(WeakError::new(
            "Eternaltwin URL must use `http` or `https` scheme when OTLP exporter is `eternalfest://eternaltwin/`",
          ));
        }
      } else {
        return Err(WeakError::new(
          "invalid OTLP exporter endpoint with `eternalfest` scheme, expected `eternalfest://eternaltwin/`",
        ));
      }
    }
    "http" | "https" => endpoint.clone(),
    _ => {
      return Err(WeakError::new(
        "invalid OTLP exporter endpoint scheme, expected `http`, `https` or `eternalfest`",
      ))
    }
  };
  http_endpoint
    .path_segments_mut()
    .expect("http(s) url is hierarchical")
    .extend(path);
  Ok(http_endpoint)
}
