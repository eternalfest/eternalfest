use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Version {
  pub eternalfest: Cow<'static, str>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Start {
  pub version: Version,
  pub exe: Url,
}
