use crate::cmd::config::{resolve_config, ConfigArgsRef};
use crate::cmd::CliContext;
use crate::output::OutFormat;
use clap::Parser;
use eternalfest_blob_store::pg::PgBlobStore;
use eternalfest_buffer_store::fs::FsBufferStore;
use eternalfest_core::clock::VirtualClock;
use eternalfest_core::uuid::Uuid4Generator;
use eternaltwin_core::core::Instant;
use eternaltwin_core::types::WeakError;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;
use std::ops::Deref;

/// Arguments to the `clean` task.
#[derive(Debug, Parser)]
pub struct Args {
  #[clap(flatten)]
  config: crate::cmd::config::Args,
  #[arg(long, value_enum, default_value_t)]
  format: OutFormat,
}

impl ConfigArgsRef for Args {
  fn config_args(&self) -> &crate::cmd::config::Args {
    &self.config
  }
}

pub async fn run(cx: CliContext<'_, Args>) -> Result<(), WeakError> {
  let config = resolve_config(&cx, true).await?;
  let database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.postgres.host.value)
        .port(config.postgres.port.value)
        .database(&config.postgres.name.value)
        .username(&config.postgres.admin_user.value)
        .password(&config.postgres.admin_password.value),
    )
    .await
    .unwrap();

  {
    let database = PgPoolRef(&database);
    let clock = VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0));
    let uuid_generator = Uuid4Generator;
    let data_root = config.data_root();
    let buffer_store = FsBufferStore::new(&clock, &uuid_generator, data_root).await;
    let blob_store = PgBlobStore::new(&buffer_store, &clock, database, &uuid_generator)
      .await
      .expect("failed to create `PgBlobStore`");

    eprintln!("Starting clean-up");

    blob_store.mark_unused_blobs_for_deletion().await?;
    blob_store.exec_blob_deletions().await?;

    eprintln!("Done");
  }

  // await blob.markUnusedBlobsForDeletion({type: actorType.ActorType.System});
  // await blob.execBlobDeletions({type: actorType.ActorType.System});

  database.close().await;

  Ok(())
}

struct PgPoolRef<'a>(&'a PgPool);

impl<'a> Deref for PgPoolRef<'a> {
  type Target = PgPool;

  fn deref(&self) -> &Self::Target {
    self.0
  }
}
