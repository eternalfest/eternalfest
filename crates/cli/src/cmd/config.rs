use crate::cmd::CliContext;
use clap::ArgGroup;
use clap::{value_parser, Arg, ArgAction, ArgMatches, Command, Error, FromArgMatches};
use eternalfest_config::load::{ConfigRoot, ConfigRoots, EnvConfig, MetaLoadConfig, Profile, WorkingDir};
use eternalfest_config::{
  BuiltinEternalfestConfigProfile, Config, ConfigChain, ConfigFormat, ConfigSource, ConfigValue,
  EternalfestConfigProfile, LoadConfigSource,
};
use eternalfest_system::EternalfestSystem;
use eternaltwin_core::types::WeakError;
use std::collections::BTreeMap;
use url::Url;

/// Arguments to the `config` task.
///
/// The config task loads the configuration, prints it and exits. Its purpose
/// is to help ensuring that the config is properly set-up.
///
/// The config is loaded in a few steps:
/// 1. Parse the command line arguments
/// 2. Resolve the environment. Real environment variables always win, then the
///    `.env` files are used.
///
/// <https://symfony.com/doc/current/configuration.html#overriding-environment-values-via-env-local>
#[derive(Debug)]
pub struct Args {
  profile: Option<EternalfestConfigProfile>,
  env_config: Option<bool>,
  config_roots: Vec<ConfigCommand>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
enum ConfigCommand {
  Format(ConfigFormat),
  Url(String),
  Data(String),
  Search(String),
}

impl FromArgMatches for Args {
  fn from_arg_matches(matches: &ArgMatches) -> Result<Self, Error> {
    Self::from_arg_matches_mut(&mut matches.clone())
  }

  fn from_arg_matches_mut(matches: &mut ArgMatches) -> Result<Self, Error> {
    let profile = matches.remove_one::<EternalfestConfigProfile>("profile");

    let env_config = match (
      matches.get_flag("env_config_true"),
      matches.get_flag("env_config_false"),
    ) {
      (true, false) => Some(true),
      (false, true) => Some(false),
      (false, false) => None,
      (true, true) => unreachable!(),
    };

    let mut config_commands: BTreeMap<usize, ConfigCommand> = BTreeMap::new();

    match (
      matches.indices_of("config_format"),
      matches.get_many::<ConfigFormat>("config_format"),
    ) {
      (None, None) => {}
      (Some(indices), Some(values)) => {
        for (i, v) in Iterator::zip(indices, values) {
          config_commands.insert(i, ConfigCommand::Format(*v));
        }
      }
      _ => unreachable!(),
    }

    match (matches.indices_of("config"), matches.get_many::<String>("config")) {
      (None, None) => {}
      (Some(indices), Some(values)) => {
        for (i, v) in Iterator::zip(indices, values) {
          config_commands.insert(i, ConfigCommand::Url(v.clone()));
        }
      }
      _ => unreachable!(),
    }

    match (
      matches.indices_of("config_data"),
      matches.get_many::<String>("config_data"),
    ) {
      (None, None) => {}
      (Some(indices), Some(values)) => {
        for (i, v) in Iterator::zip(indices, values) {
          config_commands.insert(i, ConfigCommand::Data(v.clone()));
        }
      }
      _ => unreachable!(),
    }

    match (
      matches.indices_of("config_search"),
      matches.get_many::<String>("config_search"),
    ) {
      (None, None) => {}
      (Some(indices), Some(values)) => {
        for (i, v) in Iterator::zip(indices, values) {
          config_commands.insert(i, ConfigCommand::Search(v.clone()));
        }
      }
      _ => unreachable!(),
    }

    Ok(Args {
      profile,
      env_config,
      config_roots: config_commands.into_values().collect(),
    })
  }

  fn update_from_arg_matches(&mut self, matches: &ArgMatches) -> Result<(), Error> {
    self.update_from_arg_matches_mut(&mut matches.clone())
  }

  fn update_from_arg_matches_mut(&mut self, matches: &mut ArgMatches) -> Result<(), Error> {
    self.profile = matches.remove_one::<EternalfestConfigProfile>("profile");
    self.env_config = None;
    Ok(())
  }
}

impl clap::Args for Args {
  fn augment_args(cmd: Command) -> Command {
    cmd
      .arg(
        Arg::new("profile")
          .short('p')
          .long("profile")
          .value_name("PROFILE")
          .value_parser(value_parser!(EternalfestConfigProfile))
          .action(ArgAction::Set)
          .help(r#"Eternalfest configuration profile, for defaults and customization"#)
          .long_help(r#"Eternalfest profile. Profiles control which config to load. Builtin profiles are "dev", "production", "sdk", "test". They control default values. Custom profiles use "dev" for default values. [env: ETERNALFEST_PROFILE]"#),
      )
      .arg(
        Arg::new("env_config_true")
          .long("env-config")
          .overrides_with("env_config_false")
          .overrides_with("env_config_true")
          .action(ArgAction::SetTrue)
          .help("Allow reading configuration from environment variables")
          .long_help(r#"Allow reading configuration from environment variables. Negate it using "--no-env-config" [default: true]."#),
      )
      .arg(
        Arg::new("env_config_false")
          .long("no-env-config")
          .overrides_with("env_config_false")
          .overrides_with("env_config_true")
          .action(ArgAction::SetTrue)
          .hide(true)
      )
      .group(ArgGroup::new("env_config").args(["env_config_true", "env_config_false"]))
      .arg(Arg::new("config").short('c').long("config").value_name("CONFIG_URL").action(ArgAction::Append).help("Configuration file location").long_help(r#"Configuration file location. The format can be a file:// url, a relative path starting with "./" or "../". In the future more options may be provided"#))
      .arg(Arg::new("config_data").long("config-data").value_name("CONFIG_DATA").action(ArgAction::Append).help("Inline configuration").long_help(r"Inline configuration"))
      .arg(Arg::new("config_search").long("config-search").value_name("PATTERN").action(ArgAction::Append).help("Search config file in parent directories").long_help(r#"Search config file in parent directories. This works as if appending more and more `../` in front of the pattern. Stops at the first match or when the root is reached."#))
      .arg(
        Arg::new("config_format")
          .long("config-format")
          .value_name("FORMAT")
          .value_parser(value_parser!(ConfigFormat))
          .action(ArgAction::Append)
          .help(r#"Configuration format for the next config argument"#)
          .long_help(r#"Configuration format for the next config argument. Possibles values are "auto", "json", "toml"."#),
      )
  }

  fn augment_args_for_update(cmd: Command) -> Command {
    Self::augment_args(cmd)
  }
}

/// Like [`Deref`], but the target is [`crate::cmd::config::Args`].
pub trait ConfigArgsRef: Send + Sync {
  fn config_args(&self) -> &Args;
}

impl ConfigArgsRef for Args {
  fn config_args(&self) -> &crate::cmd::config::Args {
    self
  }
}

pub async fn resolve_config<A: ConfigArgsRef, Out>(
  cx: &CliContext<'_, A, Out>,
  verbose: bool,
) -> Result<Config, WeakError> {
  if verbose {
    eprintln!("start early initialization");
  }
  let config = resolve_load_config(cx).await?;
  if verbose {
    eprintln!("early initialization complete");
    print_load_config(&config);
    eprintln!("configuration resolution: start");
  }
  let config = ConfigChain::resolve_from_roots(
    config.get(&ConfigRoots).value,
    config.get(&WorkingDir).value,
    config.get(&Profile).value,
  );
  if verbose {
    eprintln!("configuration resolution: done");
    print_config_chain(&config);
  }
  let config = config.merge();
  if verbose {
    let mut safe_config = config.clone();
    safe_config.backend.secret.value = None;
    eprintln!("resolved config:");
    println!(
      "{}",
      serde_json::to_string_pretty(&safe_config).expect("serialization succeeds")
    );
  }
  Ok(config)
}

pub async fn resolve_load_config<A: ConfigArgsRef, Out>(
  cx: &CliContext<'_, A, Out>,
) -> Result<MetaLoadConfig, WeakError> {
  let args = cx.args.config_args();
  let config = MetaLoadConfig::new(cx.working_dir.to_path_buf());
  let config = match args.env_config {
    Some(env_config) => config.arg_env_config(env_config),
    None => config,
  };
  let config = match &args.profile {
    Some(profile) => config.arg_profile(profile.clone()),
    None => {
      if config.get(&EnvConfig).value {
        match cx.env.get("ETERNALFEST_PROFILE") {
          Some(profile) => config.env_profile(EternalfestConfigProfile::parse(profile)),
          None => config,
        }
      } else {
        config
      }
    }
  };
  let config = config.refresh_env_config_from_profile();
  let profile: BuiltinEternalfestConfigProfile = config.get(&Profile).value.as_builtin_or_dev();
  let config = if args.config_roots.is_empty() {
    config
  } else {
    let mut config_roots: Vec<ConfigRoot> = Vec::new();
    let mut format = ConfigValue {
      value: ConfigFormat::Auto,
      meta: LoadConfigSource::Default(profile),
    };
    for cmd in &args.config_roots {
      match cmd {
        ConfigCommand::Format(ref new_format) => {
          if matches!(format.meta, LoadConfigSource::Default(_)) {
            format = ConfigValue {
              value: *new_format,
              meta: LoadConfigSource::Args,
            }
          } else {
            panic!("duplicate `--config-format`");
          }
        }
        ConfigCommand::Url(c) => {
          config_roots.push(ConfigRoot::Url {
            url: ConfigValue {
              value: c.clone(),
              meta: LoadConfigSource::Args,
            },
            format,
          });
          format = ConfigValue {
            value: ConfigFormat::Auto,
            meta: LoadConfigSource::Default(profile),
          };
        }
        ConfigCommand::Data(c) => {
          config_roots.push(ConfigRoot::Data {
            data: ConfigValue {
              value: c.clone(),
              meta: LoadConfigSource::Args,
            },
            format,
          });
          format = ConfigValue {
            value: ConfigFormat::Auto,
            meta: LoadConfigSource::Default(profile),
          };
        }
        ConfigCommand::Search(c) => {
          config_roots.push(ConfigRoot::Search {
            patterns: ConfigValue {
              value: vec![c.clone()],
              meta: LoadConfigSource::Args,
            },
            format,
          });
          format = ConfigValue {
            value: ConfigFormat::Auto,
            meta: LoadConfigSource::Default(profile),
          };
        }
      }
    }
    config.set_config_roots(config_roots, LoadConfigSource::Args)
  };
  Ok(config)
}

pub fn print_load_config(config: &MetaLoadConfig) {
  {
    let profile = config.get(&Profile);
    eprintln!(
      "- profile = {:?} [source = {:?}]",
      profile.value.as_str(),
      profile.meta.source
    );
  }
  {
    let env_config = config.get(&EnvConfig);
    eprintln!(
      "- env_config = {:?} [source = {:?}]",
      env_config.value, env_config.meta.source
    );
  }
  {
    let working_dir = config.get(&WorkingDir);
    eprintln!(
      "- working_dir = {:?} [source = {:?}]",
      working_dir.value, working_dir.meta.source
    );
  }
  {
    let config_roots = config.get(&ConfigRoots);
    eprintln!("- config_roots [source = {:?}]", config_roots.meta.source);
    eprintln!("  - Default");
    for root in config_roots.value {
      match root {
        ConfigRoot::Data { data, format } => {
          eprintln!("  - Data");
          eprintln!("    - format = {:?} [source = {:?}]", &format.value, format.meta);
          eprintln!("    - data = (hidden) [source = {:?}]", data.meta);
        }
        ConfigRoot::Url { url, format } => {
          eprintln!("  - Url");
          eprintln!("    - format = {:?} [source = {:?}]", &format.value, format.meta);
          eprintln!("    - url = {:?} [source = {:?}]", &url.value, url.meta);
        }
        ConfigRoot::Search { patterns, format } => {
          eprintln!("  - Search");
          eprintln!("    - format = {:?} [source = {:?}]", &format.value, format.meta);
          let empty_warning = if patterns.value.is_empty() { " (empty)" } else { "" };
          eprintln!("    - patterns{} [source = {:?}]", empty_warning, patterns.meta);
          for pattern in &patterns.value {
            eprintln!("      - {pattern:?}");
          }
        }
      }
    }
  }
}

pub fn print_config_chain(config: &ConfigChain) {
  eprintln!("loaded config (from highest priority to lowest):");
  for item in &config.items {
    match &item.source {
      ConfigSource::File(Some(file)) => eprintln!("- {}", Url::from_file_path(file).unwrap()),
      ConfigSource::File(None) => eprintln!("- (anonymous file)"),
      ConfigSource::Cli => eprintln!("- Args"),
      ConfigSource::Env => eprintln!("- Env"),
      ConfigSource::Default => eprintln!("- Default"),
    }
  }
  eprintln!("- Default");
}

pub async fn populate_system(_system: &EternalfestSystem, _config: &Config, _verbose: bool) -> Result<(), WeakError> {
  // TODO: Support seeding the DB with users, games and runs
  Ok(())
}

pub async fn run(cx: CliContext<'_, Args>) -> Result<(), WeakError> {
  resolve_config(&cx, true).await?;
  Ok(())
}

#[cfg(test)]
mod test {
  use super::*;
  use clap::Args as _;

  #[test]
  fn check_args() {
    Args::augment_args(Command::new("eternalfest-config")).debug_assert()
  }
}
