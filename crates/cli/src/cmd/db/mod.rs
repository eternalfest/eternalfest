use crate::cmd::CliContext;
use clap::Parser;
use eternaltwin_core::types::WeakError;

pub mod check;
pub mod reset;
pub mod sync;

#[derive(Debug, Parser)]
pub struct Args {
  #[clap(subcommand)]
  command: Command,
}

#[derive(Debug, Parser)]
pub enum Command {
  /// Check the database state
  #[clap(name = "check")]
  Check(check::Args),
  /// Reset the database to the empty state
  #[clap(name = "reset")]
  Reset(reset::Args),
  /// Synchronize the DB schema to the latest version
  #[clap(name = "sync")]
  Sync(sync::Args),
}

pub async fn run(cx: CliContext<'_, Args>) -> Result<(), WeakError> {
  match &cx.args.command {
    Command::Check(ref args) => {
      check::run(CliContext {
        args,
        env: cx.env,
        working_dir: cx.working_dir,
        out: cx.out,
      })
      .await
    }
    Command::Reset(ref args) => {
      reset::run(CliContext {
        args,
        env: cx.env,
        working_dir: cx.working_dir,
        out: cx.out,
      })
      .await
    }
    Command::Sync(ref args) => {
      sync::run(CliContext {
        args,
        env: cx.env,
        working_dir: cx.working_dir,
        out: cx.out,
      })
      .await
    }
  }
}
