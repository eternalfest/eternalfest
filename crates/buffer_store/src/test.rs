use eternalfest_core::buffer::{BufferStore, BufferStoreRef, DeleteBufferError, ReadStreamError};
use std::convert::TryInto;

macro_rules! assert_ok {
  ($result:expr $(,)?) => {{
    match &$result {
      Err(_) => {
        panic!("assertion failed: `result.is_ok()`: {:?}", &$result)
      }
      Ok(()) => {}
    }
  }};
}

#[macro_export]
macro_rules! test_buffer_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_create_and_read);
    register_test!($(#[$meta])*, $api, create_buffer_and_delete_it);
    register_test!($(#[$meta])*, $api, write_twice);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyBufferStore>
where
  TyBufferStore: BufferStoreRef,
{
  pub(crate) buffer_store: TyBufferStore,
}

pub(crate) async fn test_create_and_read<TyBufferStore>(api: TestApi<TyBufferStore>)
where
  TyBufferStore: BufferStoreRef,
{
  let buffer_id = api.buffer_store.buffer_store().create_buffer(2).await.unwrap();
  let data = &[0x01u8, 0xffu8];
  assert_ok!(api.buffer_store.buffer_store().write_bytes(buffer_id, 0, data).await);
  let actual = api.buffer_store.buffer_store().read_stream(buffer_id).await.unwrap();
  assert_eq!(actual.as_slice(), data);
}

pub(crate) async fn create_buffer_and_delete_it<TyBufferStore>(api: TestApi<TyBufferStore>)
where
  TyBufferStore: BufferStoreRef,
{
  let buffer_id = api.buffer_store.buffer_store().create_buffer(2).await.unwrap();
  assert_ok!(api.buffer_store.buffer_store().delete_buffer(buffer_id).await);
  {
    let actual = api.buffer_store.buffer_store().read_stream(buffer_id).await;
    assert!(
      matches!(actual, Err(ReadStreamError::NotFound(_))),
      "should not read after deletion"
    );
  }
  {
    let actual = api.buffer_store.buffer_store().delete_buffer(buffer_id).await;
    assert!(
      matches!(actual, Err(DeleteBufferError::NotFound(_))),
      "should not double-delete"
    );
  }
  assert_ok!(api.buffer_store.buffer_store().delete_buffer_if_exists(buffer_id).await);
}

pub(crate) async fn write_twice<TyBufferStore>(api: TestApi<TyBufferStore>)
where
  TyBufferStore: BufferStoreRef,
{
  let first = b"Hello";
  let second = b", World!";
  let full = b"Hello, World!";
  let buffer_id = api
    .buffer_store
    .buffer_store()
    .create_buffer(full.len().try_into().unwrap())
    .await
    .unwrap();
  assert_ok!(api.buffer_store.buffer_store().write_bytes(buffer_id, 0, first).await);
  assert_ok!(
    api
      .buffer_store
      .buffer_store()
      .write_bytes(buffer_id, first.len().try_into().unwrap(), second)
      .await
  );
  let actual = api.buffer_store.buffer_store().read_stream(buffer_id).await.unwrap();
  assert_eq!(actual.as_slice(), full);
}
