use async_stream::try_stream;
use async_trait::async_trait;
use core::str::FromStr;
use eternalfest_core::buffer::{
  Buffer, BufferId, BufferStore, CreateBufferError, DeleteBufferError, ReadStreamError, WriteBytesError,
};
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::types::WeakError;
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use futures::{Stream, TryStreamExt};
use std::convert::TryFrom;
use std::ffi::OsStr;
use std::io::ErrorKind;
use std::path::PathBuf;
use std::pin::Pin;
use thiserror::Error;
use tokio::fs;
use tokio::fs::OpenOptions;
use tokio::io::{AsyncSeekExt, AsyncWriteExt, SeekFrom};
use tokio_stream::wrappers::ReadDirStream;

pub struct FsBufferStore<TyClock: ClockRef, TyUuidGenerator: UuidGeneratorRef> {
  pub(crate) clock: TyClock,
  pub(crate) uuid_generator: TyUuidGenerator,
  pub(crate) root: PathBuf,
  pub(crate) tmp_dir: PathBuf,
}

const TMP_DIR_NAME: &str = ".tmp";

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum ListError {
  #[error("failed to read root directory {0:?}: {1:?}")]
  ReadRoot(PathBuf, String),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum ListItemError {
  #[error("failed to dir entry from {0:?}: {1:?}")]
  DirEntry(PathBuf, String),
  #[error("failed to read intermediate dir {0:?}: {1:?}")]
  ReadDir(PathBuf, String),
  #[error("buffer has invalid id at path: {0:?}")]
  InvalidId(PathBuf),
}

impl<TyClock, TyUuidGenerator> FsBufferStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub async fn new(clock: TyClock, uuid_generator: TyUuidGenerator, root: PathBuf) -> Self {
    let tmp_dir = root.join(TMP_DIR_NAME);
    fs::create_dir_all(root.as_path())
      .await
      .unwrap_or_else(|_| panic!("failed to create FsBufferStore root: {}", root.display()));
    fs::create_dir_all(tmp_dir.as_path())
      .await
      .unwrap_or_else(|_| panic!("failed to create FsBufferStore tmp dir: {}", root.display()));
    Self {
      clock,
      uuid_generator,
      root,
      tmp_dir,
    }
  }

  fn get_alloc_path(&self, id: BufferId) -> PathBuf {
    self.tmp_dir.join(id.to_hex().as_str())
  }

  fn get_writable_path(&self, id: BufferId) -> PathBuf {
    let id = id.to_hex();
    let mut path = self.root.clone();
    path.push(&id[0..2]);
    path.push(&id[2..4]);
    path.push(id.as_str()); // TODO: Prefix with dot because it's a temporary file
    path
  }

  fn get_readable_path(&self, id: BufferId) -> PathBuf {
    let id = id.to_hex();
    let mut path = self.root.clone();
    path.push(&id[0..2]);
    path.push(&id[2..4]);
    path.push(id.as_str());
    path
  }

  async fn inner_list(&self) -> Result<impl Stream<Item = Result<Buffer, ListItemError>> + '_, ListError> {
    let _ = self.clock.clock().now();
    let root_entries = fs::read_dir(self.root.as_path())
      .await
      .map_err(|e| ListError::ReadRoot(self.root.clone(), format!("{e:?}")))?;
    let root_entries = ReadDirStream::new(root_entries);
    Ok(try_stream! {
      for await entry in root_entries {
        // Prefix [0..2]
        let entry = entry.map_err(|e| ListItemError::DirEntry(self.root.clone(), format!("{e:?}")))?;
        if entry.file_name() == OsStr::new(TMP_DIR_NAME) {
          continue;
        }
        let prefix_dir = entry.path();
        let prefix_entries = fs::read_dir(prefix_dir.as_path())
          .await
          .map_err(|e| ListItemError::ReadDir(prefix_dir.clone(), format!("{e:?}")))?;
        let prefix_entries = ReadDirStream::new(prefix_entries);
        for await entry in prefix_entries {
          // Prefix [2..4]
          let entry = entry.map_err(|e| ListItemError::DirEntry(prefix_dir.clone(), format!("{e:?}")))?;
          let prefix_dir = entry.path();
          let buffer_entries = fs::read_dir(prefix_dir.as_path())
            .await
            .map_err(|e| ListItemError::ReadDir(prefix_dir.clone(), format!("{e:?}")))?;
          let buffer_entries = ReadDirStream::new(buffer_entries);
          for await entry in buffer_entries {
            let entry: fs::DirEntry = entry.map_err(|e| ListItemError::DirEntry(prefix_dir.clone(), format!("{e:?}")))?;
            let id = BufferId::from_str(entry.file_name().to_str().ok_or_else(|| ListItemError::InvalidId(entry.path()))?)
              .map_err(|_| ListItemError::InvalidId(entry.path()))?;
            yield Buffer { id };
          }
        }
      }
    })
  }
}

#[async_trait]
impl<TyClock, TyUuidGenerator> BufferStore for FsBufferStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_buffer(&self, size: u32) -> Result<BufferId, CreateBufferError> {
    let id = BufferId::from_uuid(self.uuid_generator.uuid_generator().next());

    let alloc_file_path = self.get_alloc_path(id);
    let mut alloc_file = OpenOptions::new()
      .create_new(true)
      .append(true)
      .open(alloc_file_path.as_path())
      .await
      .map_err(CreateBufferError::other)?;
    alloc_file
      .set_len(size.into())
      .await
      .map_err(CreateBufferError::other)?;
    alloc_file.flush().await.map_err(CreateBufferError::other)?;
    alloc_file.sync_all().await.map_err(CreateBufferError::other)?;

    let writable_file_path = self.get_writable_path(id);

    fs::create_dir_all(writable_file_path.parent().unwrap())
      .await
      .map_err(CreateBufferError::other)?;
    fs::hard_link(alloc_file_path.as_path(), writable_file_path.as_path())
      .await
      .map_err(CreateBufferError::other)?;
    fs::remove_file(alloc_file_path.as_path())
      .await
      .map_err(CreateBufferError::other)?;

    Ok(id)
  }

  async fn delete_buffer(&self, buffer_id: BufferId) -> Result<(), DeleteBufferError> {
    // The order (readable then writable) is important to prevent race conditions with `upload`
    let writable_file = self.get_writable_path(buffer_id);
    let found = match fs::remove_file(writable_file).await {
      Err(e) if e.kind() != ErrorKind::NotFound => return Err(DeleteBufferError::Other(WeakError::wrap(e))),
      r => r.is_ok(),
    };
    let readable_file = self.get_readable_path(buffer_id);
    let found = match fs::remove_file(readable_file).await {
      Err(e) if e.kind() != ErrorKind::NotFound => return Err(DeleteBufferError::Other(WeakError::wrap(e))),
      r => found || r.is_ok(),
    };
    if found {
      Ok(())
    } else {
      Err(DeleteBufferError::NotFound(buffer_id))
    }
  }

  async fn delete_buffer_if_exists(&self, buffer_id: BufferId) -> Result<(), WeakError> {
    let res = self.delete_buffer(buffer_id).await;
    match res {
      Ok(()) => Ok(()),
      Err(DeleteBufferError::NotFound(_)) => Ok(()),
      Err(DeleteBufferError::Other(e)) => Err(e),
    }
  }

  async fn write_bytes(&self, buffer_id: BufferId, offset: u32, bytes: &[u8]) -> Result<(), WriteBytesError> {
    // TODO: Hashmap from buffer id to file locks? What about multiple server processes?
    let writable_file_path = self.get_writable_path(buffer_id);
    let mut writable_file = OpenOptions::new()
      .write(true)
      .open(writable_file_path.as_path())
      .await
      .map_err(|e| match e.kind() {
        ErrorKind::NotFound => WriteBytesError::NotFound(buffer_id),
        _ => WriteBytesError::Other(WeakError::wrap(e)),
      })?;
    let buf_len = writable_file.metadata().await.map_err(WriteBytesError::other)?.len();
    let input_len = u32::try_from(bytes.len()).map_err(|_| WriteBytesError::MaxSize)?;
    let end_offset = offset.checked_add(input_len).ok_or(WriteBytesError::BufferOverflow)?;
    if u64::from(end_offset) > buf_len {
      return Err(WriteBytesError::BufferOverflow);
    }
    writable_file
      .seek(SeekFrom::Start(offset.into()))
      .await
      .map_err(WriteBytesError::other)?;
    writable_file.write_all(bytes).await.map_err(WriteBytesError::other)?;
    writable_file.flush().await.map_err(WriteBytesError::other)?;
    writable_file.sync_all().await.map_err(WriteBytesError::other)?;
    Ok(())
  }

  async fn read_stream(&self, buffer_id: BufferId) -> Result<Vec<u8>, ReadStreamError> {
    let readable_file_path = self.get_readable_path(buffer_id);
    match fs::read(readable_file_path.as_path()).await {
      Ok(buf) => Ok(buf),
      Err(e) if e.kind() == ErrorKind::NotFound => return Err(ReadStreamError::NotFound(buffer_id)),
      Err(e) => return Err(ReadStreamError::Other(WeakError::wrap(e))),
    }
  }

  async fn list(&self) -> Result<Pin<Box<dyn Send + Sync + Stream<Item = Result<Buffer, WeakError>> + '_>>, WeakError> {
    self
      .inner_list()
      .await
      .map(|stream| {
        Box::pin(stream.map_err(WeakError::wrap))
          as Pin<Box<dyn Send + Sync + Stream<Item = Result<Buffer, WeakError>> + '_>>
      })
      .map_err(WeakError::wrap)
  }
}

#[cfg(test)]
mod test {
  use super::FsBufferStore;
  use crate::test::TestApi;
  use eternalfest_core::buffer::BufferStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::uuid::Uuid4Generator;
  use serial_test::serial;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn BufferStore>> {
    let config = eternalfest_config::Config::for_test();
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let data_root = config.data_root();
    let buffer_store: Arc<dyn BufferStore> = Arc::new(FsBufferStore::new(clock, uuid_generator, data_root).await);

    TestApi { buffer_store }
  }

  test_buffer_store!(
    #[serial]
    || make_test_api().await
  );
}
