use async_trait::async_trait;
use eternalfest_core::buffer::{
  Buffer, BufferId, BufferStore, CreateBufferError, DeleteBufferError, ReadStreamError, WriteBytesError,
};
use eternalfest_core::types::WeakError;
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use futures::Stream;
use std::collections::BTreeMap;
use std::convert::TryFrom;
use std::pin::Pin;
use tokio::sync::RwLock;

struct MemBufferStoreState {
  buffers: BTreeMap<BufferId, MemBuffer>,
}

struct MemBuffer {
  #[expect(dead_code, reason = "`list` is not implemented yet")]
  id: BufferId,
  data: Vec<u8>,
}

impl MemBufferStoreState {
  pub fn new() -> Self {
    Self {
      buffers: BTreeMap::new(),
    }
  }
}

pub struct MemBufferStore<TyUuidGenerator: UuidGeneratorRef> {
  uuid_generator: TyUuidGenerator,
  state: RwLock<MemBufferStoreState>,
}

impl<TyUuidGenerator> MemBufferStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(uuid_generator: TyUuidGenerator) -> Self {
    Self {
      uuid_generator,
      state: RwLock::new(MemBufferStoreState::new()),
    }
  }
}

#[async_trait]
impl<TyUuidGenerator> BufferStore for MemBufferStore<TyUuidGenerator>
where
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_buffer(&self, size: u32) -> Result<BufferId, CreateBufferError> {
    let mut state = self.state.write().await;
    let id = BufferId::from_uuid(self.uuid_generator.uuid_generator().next());
    let size: usize = usize::try_from(size).map_err(CreateBufferError::other)?;
    let buffer = MemBuffer {
      id,
      data: vec![0; size],
    };
    state.buffers.insert(id, buffer);
    Ok(id)
  }

  async fn delete_buffer(&self, buffer_id: BufferId) -> Result<(), DeleteBufferError> {
    let mut state = self.state.write().await;
    let old = state.buffers.remove(&buffer_id);
    if old.is_some() {
      Ok(())
    } else {
      Err(DeleteBufferError::NotFound(buffer_id))
    }
  }

  async fn delete_buffer_if_exists(&self, buffer_id: BufferId) -> Result<(), WeakError> {
    let mut state = self.state.write().await;
    let _ = state.buffers.remove(&buffer_id);
    Ok(())
  }

  async fn write_bytes(&self, buffer_id: BufferId, offset: u32, bytes: &[u8]) -> Result<(), WriteBytesError> {
    let mut state = self.state.write().await;
    let buffer: &mut MemBuffer = state
      .buffers
      .get_mut(&buffer_id)
      .ok_or(WriteBytesError::NotFound(buffer_id))?;
    let start_offset: usize = usize::try_from(offset).map_err(WriteBytesError::other)?;
    let end_offset: usize = start_offset.checked_add(bytes.len()).ok_or(WriteBytesError::MaxSize)?;
    if end_offset <= buffer.data.len() {
      buffer.data[start_offset..end_offset].copy_from_slice(bytes);
    } else {
      return Err(WriteBytesError::BufferOverflow);
    }
    Ok(())
  }

  async fn read_stream(&self, buffer_id: BufferId) -> Result<Vec<u8>, ReadStreamError> {
    let state = self.state.read().await;
    match state.buffers.get(&buffer_id) {
      None => Err(ReadStreamError::NotFound(buffer_id)),
      Some(buffer) => Ok(buffer.data.clone()),
    }
  }

  async fn list(&self) -> Result<Pin<Box<dyn Send + Sync + Stream<Item = Result<Buffer, WeakError>> + '_>>, WeakError> {
    eprintln!("using stub implementation for MemBufferStore::list");
    Ok(Box::pin(futures::stream::empty()))
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::test::TestApi;
  use eternalfest_core::buffer::BufferStore;
  use eternalfest_core::uuid::Uuid4Generator;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn BufferStore>> {
    let uuid_generator = Arc::new(Uuid4Generator);
    let buffer_store: Arc<dyn BufferStore> = Arc::new(MemBufferStore::new(uuid_generator));

    TestApi { buffer_store }
  }

  test_buffer_store!(|| make_test_api().await);
}
