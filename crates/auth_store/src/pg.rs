use async_trait::async_trait;
use eternalfest_core::auth::{AuthStore, CreateSessionOptions, RawSession, SessionId};
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::Instant;
use eternalfest_core::types::WeakError;
use eternalfest_core::user::UserId;
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternalfest_core::SyncRef;
use sqlx::PgPool;

pub struct PgAuthStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgAuthStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(clock: TyClock, database: TyDatabase, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      database,
      uuid_generator,
    }
  }
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> AuthStore for PgAuthStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_session(&self, options: &CreateSessionOptions) -> Result<RawSession, WeakError> {
    let session_id = SessionId::from_uuid(self.uuid_generator.uuid_generator().next());
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      created_at: Instant,
    }

    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
          INSERT INTO sessions(
            session_id, user_id, created_at, updated_at, data
          )
          VALUES (
            $1::SESSION_ID, $2::USER_ID, $3::INSTANT, $3::INSTANT, '{}'
          )
          RETURNING created_at;
          ",
    )
    .bind(session_id)
    .bind(options.user.id)
    .bind(now)
    .fetch_one(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    Ok(RawSession {
      id: session_id,
      user: options.user,
      created_at: row.created_at,
      updated_at: row.created_at,
    })
  }

  async fn get_and_touch_session(&self, session: SessionId) -> Result<Option<RawSession>, WeakError> {
    let now = self.clock.clock().now();

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      created_at: Instant,
      updated_at: Instant,
      user_id: UserId,
    }

    // language=PostgreSQL
    let row = sqlx::query_as::<_, Row>(
      r"
      UPDATE sessions
      SET updated_at = $2::INSTANT
      WHERE session_id = $1::SESSION_ID
      RETURNING sessions.created_at, sessions.updated_at, sessions.user_id;
      ",
    )
    .bind(session)
    .bind(now)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    Ok(row.map(|row| RawSession {
      id: session,
      user: row.user_id.into(),
      created_at: row.created_at,
      updated_at: row.updated_at,
    }))
  }
}

#[cfg(test)]
mod test {
  use super::PgAuthStore;
  use crate::test::TestApi;
  use eternalfest_core::auth::AuthStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::user::UserStore;
  use eternalfest_core::uuid::Uuid4Generator;
  use eternalfest_db_schema::force_create_latest;
  use eternalfest_user_store::pg::PgUserStore;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<dyn AuthStore>, Arc<VirtualClock>, Arc<dyn UserStore>> {
    let config = eternalfest_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2021, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let auth_store: Arc<dyn AuthStore> = Arc::new(PgAuthStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      uuid_generator,
    ));

    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(Arc::clone(&clock), Arc::clone(&database)));

    TestApi {
      auth_store,
      clock,
      user_store,
    }
  }

  test_dinoparc_store!(
    #[serial]
    || make_test_api().await
  );
}
