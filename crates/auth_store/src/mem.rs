use async_trait::async_trait;
use eternalfest_core::auth::{AuthStore, CreateSessionOptions, RawSession, SessionId};
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::Instant;
use eternalfest_core::types::WeakError;
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use std::collections::HashMap;
use std::sync::RwLock;

struct StoreState {
  sessions: HashMap<SessionId, RawSession>,
}

impl StoreState {
  fn new() -> Self {
    Self {
      sessions: HashMap::new(),
    }
  }

  pub(crate) fn create_session(
    &mut self,
    now: Instant,
    uuid_generator: &(impl ?Sized + UuidGenerator),
    options: &CreateSessionOptions,
  ) -> Result<RawSession, WeakError> {
    let session_id = SessionId::from_uuid(uuid_generator.next());
    let session = RawSession {
      id: session_id,
      user: options.user,
      created_at: now,
      updated_at: now,
    };
    self.sessions.insert(session_id, session.clone());
    Ok(session)
  }

  pub(crate) fn get_and_touch_session(
    &mut self,
    now: Instant,
    session_id: SessionId,
  ) -> Result<Option<RawSession>, WeakError> {
    let session = self.sessions.get_mut(&session_id);
    match session {
      None => Ok(None),
      Some(session) => {
        session.updated_at = now;
        Ok(Some(session.clone()))
      }
    }
  }
}

pub struct MemAuthStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  uuid_generator: TyUuidGenerator,
  state: RwLock<StoreState>,
}

impl<TyClock, TyUuidGenerator> MemAuthStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub fn new(clock: TyClock, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      uuid_generator,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock, TyUuidGenerator> AuthStore for MemAuthStore<TyClock, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_session(&self, options: &CreateSessionOptions) -> Result<RawSession, WeakError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    state.create_session(now, self.uuid_generator.uuid_generator(), options)
  }

  async fn get_and_touch_session(&self, session: SessionId) -> Result<Option<RawSession>, WeakError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    state.get_and_touch_session(now, session)
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemAuthStore;
  use crate::test::TestApi;
  use eternalfest_core::auth::AuthStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::user::UserStore;
  use eternalfest_core::uuid::Uuid4Generator;
  use eternalfest_user_store::mem::MemUserStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<dyn AuthStore>, Arc<VirtualClock>, Arc<dyn UserStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2021, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let auth_store: Arc<dyn AuthStore> = Arc::new(MemAuthStore::new(Arc::clone(&clock), uuid_generator));
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(Arc::clone(&clock)));

    TestApi {
      auth_store,
      clock,
      user_store,
    }
  }

  test_dinoparc_store!(|| make_test_api());
}
