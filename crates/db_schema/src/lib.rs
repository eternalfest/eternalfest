use eternaltwin_squirrel::{EmptyError, ForceCreateError, GetStateError, MigrationDirection, SchemaResolver};
pub use eternaltwin_squirrel::{SchemaState, SchemaStateRef, SchemaVersion, SchemaVersionRef};
use include_dir::{include_dir, Dir};
use lazy_static::lazy_static;
use sqlx::PgPool;
use std::error::Error;

const DB_SCRIPTS: Dir = include_dir!("$CARGO_MANIFEST_DIR/scripts");

lazy_static! {
  static ref SQUIRREL: SchemaResolver = SchemaResolver::new(&DB_SCRIPTS);
}

pub async fn get_state(db: &PgPool) -> Result<SchemaStateRef<'static>, GetStateError> {
  SQUIRREL.get_state(db).await
}

pub async fn empty(db: &PgPool) -> Result<(), EmptyError> {
  SQUIRREL.empty(db).await
}

pub async fn sync(db: &PgPool) -> Result<(), Box<dyn Error>> {
  let current: SchemaStateRef<'_> = SQUIRREL.get_state(db).await?;
  let latest: SchemaVersionRef<'_> = SQUIRREL.get_latest();
  if current != SchemaStateRef::from(latest) {
    match SQUIRREL.create_migration(current, latest, MigrationDirection::UpgradeOnly) {
      Some(migration) => SQUIRREL.apply_migration(db, &migration).await,
      None => Err("failed to plan migration".into()),
    }
  } else {
    Ok(())
  }
}

pub async fn force_create_latest(db: &PgPool, void: bool) -> Result<(), ForceCreateError> {
  SQUIRREL.force_create_latest(db, void).await
}

pub async fn force_create(db: &PgPool, state: SchemaStateRef<'static>, void: bool) -> Result<(), ForceCreateError> {
  SQUIRREL.force_create(db, state, void).await
}
