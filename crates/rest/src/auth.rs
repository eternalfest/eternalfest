use crate::extract::{Extractor, SESSION_COOKIE};
use axum::routing::get;
use axum::{Json, Router};
use axum_extra::extract::cookie::Cookie;
use axum_extra::extract::CookieJar;
use cookie::time::OffsetDateTime;
use cookie::Expiration;
use eternalfest_core::auth::AuthContext;

pub fn router() -> Router<()> {
  Router::new().route("/self", get(get_self).delete(delete_self))
}

/// legacy router for compat with Eternaldev 0.5.7
// TODO: remove it once Eternaldev is updated
pub fn legacy_router() -> Router<()> {
  Router::new().route("/auth", get(get_self).delete(delete_self))
}

async fn get_self(acx: Extractor<AuthContext>) -> Json<AuthContext> {
  Json(acx.value())
}

async fn delete_self(jar: CookieJar) -> (CookieJar, Json<AuthContext>) {
  let cookie = Cookie::build((SESSION_COOKIE, "".to_string()))
    .path("/")
    .http_only(true)
    .expires(Expiration::DateTime(OffsetDateTime::UNIX_EPOCH))
    .build();
  (jar.add(cookie), Json(AuthContext::guest()))
}

// #[cfg(test)]
// mod tests {
//   use crate::clock::RestClock;
//   use crate::{app, Client, RouterExt};
//   use axum::http::StatusCode;
//   use axum::Router;
//   use eternalfest_core::core::Instant;
//   use eternalfest_system::EternalfestSystem;
//
//   #[tokio::test]
//   async fn test_register_a_user_and_authenticate_back() {
//     let system = EternalfestSystem::for_test();
//     let router: Router = app(system);
//     let mut client: Client = router.client();
//
//     {
//       let req = axum::http::Request::builder()
//         .method("PUT")
//         .uri("/api/v1/clock")
//         .header("Content-Type", "application/json")
//         .body(axum::body::Body::from(
//           serde_json::to_string(&RestClock {
//             time: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
//           })
//           .unwrap(),
//         ))
//         .unwrap();
//
//       let res = client.send(req).await;
//       let actual_status = res.status();
//       assert_eq!(actual_status, StatusCode::OK);
//     }
//     let alice = {
//       let req = axum::http::Request::builder()
//         .method("POST")
//         .uri("/api/v1/users")
//         .header("Content-Type", "application/json")
//         .body(axum::body::Body::from(
//           serde_json::to_string(&RegisterWithUsernameOptions {
//             username: "alice".parse().unwrap(),
//             display_name: "Alice".parse().unwrap(),
//             password: Password::from("aaaaaaaaaa"),
//           })
//           .unwrap(),
//         ))
//         .unwrap();
//
//       let res = client.send(req).await;
//       assert_eq!(res.status(), StatusCode::OK);
//       let body = axum::body::to_bytes(res.into_body(), 1024 * 1024)
//         .await
//         .expect("read body")
//         .to_vec();
//
//       let body: &str = std::str::from_utf8(body.as_slice()).unwrap();
//       serde_json::from_str::<User>(body).unwrap()
//     };
//     let mut client = router.client();
//     {
//       let req = axum::http::Request::builder()
//         .method("GET")
//         .uri("/api/v1/auth/self")
//         .header("Content-Type", "application/json")
//         .body(axum::body::Body::empty())
//         .unwrap();
//
//       let res = client.send(req).await;
//       assert_eq!(res.status(), StatusCode::OK);
//       let body = axum::body::to_bytes(res.into_body(), 1024 * 1024)
//         .await
//         .expect("read body")
//         .to_vec();
//
//       let body: &str = std::str::from_utf8(body.as_slice()).unwrap();
//       let actual: AuthContext = serde_json::from_str(body).unwrap();
//       let expected = AuthContext::guest();
//       assert_eq!(actual, expected);
//     }
//     {
//       let req = axum::http::Request::builder()
//         .method("PUT")
//         .uri("/api/v1/auth/self?method=Etwin")
//         .header("Content-Type", "application/json")
//         .body(axum::body::Body::from(
//           serde_json::to_string(&RawUserCredentials {
//             login: "alice".to_string(),
//             password: b"aaaaaaaaaa".as_slice().into(),
//           })
//           .unwrap(),
//         ))
//         .unwrap();
//
//       let res = client.send(req).await;
//       assert_eq!(res.status(), StatusCode::OK);
//       let body = axum::body::to_bytes(res.into_body(), 1024 * 1024)
//         .await
//         .expect("read body")
//         .to_vec();
//
//       let body: &str = std::str::from_utf8(body.as_slice()).unwrap();
//       let actual: User = serde_json::from_str(body).unwrap();
//       let expected = User {
//         id: alice.id,
//         created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 0),
//         deleted_at: None,
//         display_name: UserDisplayNameVersions {
//           current: UserDisplayNameVersion {
//             value: "Alice".parse().unwrap(),
//           },
//         },
//         is_administrator: true,
//         links: VersionedLinks::default(),
//       };
//       assert_eq!(actual, expected);
//     }
//   }
// }
