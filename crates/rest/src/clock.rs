use axum::routing::get;
use axum::{Extension, Json, Router};
use eternalfest_core::core::Instant;
use eternalfest_system::EternalfestSystem;
use serde::{Deserialize, Serialize};

pub fn router() -> Router<()> {
  Router::new().route("/", get(get_clock))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct RestClock {
  pub time: Instant,
}

async fn get_clock(Extension(api): Extension<EternalfestSystem>) -> Json<RestClock> {
  Json(RestClock { time: api.clock.now() })
}
