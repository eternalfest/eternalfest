mod auth;
mod bff;
mod blobs;
mod clock;
mod config;
mod extract;
mod games;
mod runs;
mod upload_sessions;
mod users;
mod utils;

pub use crate::extract::InternalAuthKey;
use crate::utils::trace::TraceLayer;
use axum::extract::Extension;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::get;
use axum::{Json, Router};
use eternalfest_system::EternalfestSystem;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ErrorResponse {
  pub http_code: StatusCode,
  pub error: ErrorInfo,
}

impl IntoResponse for ErrorResponse {
  fn into_response(self) -> Response {
    (self.http_code, Json(self.error)).into_response()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ErrorInfo {
  pub code: String,
  pub message: String,
}

pub fn router() -> Router<()> {
  Router::new()
    .route("/", get(get_api_home))
    .nest("/auth", auth::router())
    .nest("/blobs", blobs::router())
    .nest("/clock", clock::router())
    .nest("/config", config::router())
    .nest("/games", games::router())
    .nest("/runs", runs::router())
    .nest("/self", auth::legacy_router())
    .nest("/upload-sessions", upload_sessions::router())
    .nest("/users", users::router())
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct ApiHome {
  pub eternalfest: EternalfestMeta,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, serde::Serialize, serde::Deserialize)]
pub struct EternalfestMeta {
  pub version: String,
}

async fn get_api_home() -> Json<ApiHome> {
  Json(ApiHome {
    eternalfest: EternalfestMeta {
      version: String::from(env!("CARGO_PKG_VERSION")),
    },
  })
}

pub fn app(system: EternalfestSystem) -> Router<()> {
  let tracer_provider = system.tracer_provider.clone();
  Router::new()
    .nest("/api/v1", router())
    .nest("/oauth", bff::oauth::router())
    .nest("/actions", bff::actions::router())
    // .layer(Extension(system.dev.clone()))
    .layer(Extension(system))
    .layer(TraceLayer::new(tracer_provider))
}

#[cfg(test)]
pub(crate) struct Client {
  router: Router,
  cookies: cookie_store::CookieStore,
}

#[cfg(test)]
impl Client {
  pub fn new(router: Router) -> Self {
    Self {
      router,
      cookies: cookie_store::CookieStore::default(),
    }
  }

  pub async fn send(&mut self, mut req: axum::extract::Request<axum::body::Body>) -> Response<axum::body::Body> {
    use axum::http::header::{HeaderValue, COOKIE, SET_COOKIE};
    use cookie_store::Cookie;
    use tower::ServiceExt;

    let mut req_url = url::Url::parse("http://eternaltwin.localhost/").expect("base uri");
    req_url.set_path(req.uri().path());
    let cookies = self
      .cookies
      .get_request_values(&req_url)
      .map(|(name, value)| format!("{}={}", name, value))
      .collect::<Vec<_>>()
      .join("; ");
    if !cookies.is_empty() {
      req
        .headers_mut()
        .insert(COOKIE, HeaderValue::from_str(&cookies).expect("invalid cookies"));
    }
    let res: Result<_, core::convert::Infallible> = self.router.clone().with_state(()).oneshot(req).await;
    let res = match res {
      Ok(r) => r,
      Err(_) => unreachable!("`Infaillible` error"),
    };
    for cookie in res.headers().get_all(SET_COOKIE) {
      let cookie: Cookie = Cookie::parse(cookie.to_str().expect("invalid cookie"), &req_url).expect("parse cookie");
      self.cookies.insert_raw(&cookie, &req_url).expect("cookie insertion");
    }
    res
  }
}

#[cfg(test)]
pub(crate) trait RouterExt {
  fn client(&self) -> Client;
}

#[cfg(test)]
impl RouterExt for Router {
  fn client(&self) -> Client {
    Client::new(self.clone())
  }
}
