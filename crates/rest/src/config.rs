use axum::routing::get;
use axum::{Extension, Json, Router};
use eternalfest_system::EternalfestSystem;
use serde::{Deserialize, Serialize};

pub fn router() -> Router<()> {
  Router::new().route("/", get(get_config))
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
pub struct Config {}

async fn get_config(_: Extension<EternalfestSystem>) -> Json<Config> {
  Json(Config {})
}
