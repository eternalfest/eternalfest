use crate::utils::otel_attributes::{normalize_http_method, normalize_ip_addr};
use axum::extract::MatchedPath;
use axum::http::{Request, Response};
use opentelemetry::trace::{SpanKind, Status, TraceContextExt, Tracer, TracerProvider};
use opentelemetry::{Context as OtelContext, KeyValue};
use opentelemetry_semantic_conventions::trace as otel_keys;
use pin_project_lite::pin_project;
use std::borrow::Cow;
use std::future::Future;
use std::net::SocketAddr;
use std::pin::Pin;
use std::task::{Context, Poll};
use tower::{Layer, Service};

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Trace<S, TyTracerProvider> {
  inner: S,
  tracer_provider: TyTracerProvider,
}

impl<S, ReqBody, ResBody, TyTracerProvider> Service<Request<ReqBody>> for Trace<S, TyTracerProvider>
where
  S: Service<Request<ReqBody>, Response = Response<ResBody>>,
  TyTracerProvider: TracerProvider,
  <TyTracerProvider::Tracer as Tracer>::Span: Send + Sync + 'static,
{
  type Response = Response<ResBody>;
  type Error = S::Error;
  type Future = TraceFuture<S::Future>;

  fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.inner.poll_ready(cx)
  }

  fn call(&mut self, req: Request<ReqBody>) -> Self::Future {
    let tracer = self.tracer_provider.tracer("eternalfest_rest");
    let http_route = req.extensions().get::<MatchedPath>().map(MatchedPath::as_str);
    let (method, method_orig) = normalize_http_method(req.method());
    // > HTTP server span names SHOULD be `{method} {http.route}` if there is a (low-cardinality) `http.route`
    // > available [...].
    // >
    // > If there is no (low-cardinality) `http.route` available, HTTP server span names SHOULD be `{method}`.
    // <https://opentelemetry.io/docs/specs/semconv/http/http-spans/#name>
    let span_name = match http_route {
      Some(http_route) => format!("{method} {http_route}"),
      None => method.to_string(),
    };
    // let http_request_method_original = req.method().as_str();
    let mut span_builder = tracer.span_builder(span_name);
    span_builder.span_kind = Some(SpanKind::Server);
    span_builder.attributes = Some({
      let mut attributes = Vec::with_capacity(7);
      if let Some(http_route) = http_route {
        attributes.push(KeyValue::new(otel_keys::HTTP_ROUTE, http_route.to_string()))
      };
      attributes.push(KeyValue::new(otel_keys::HTTP_REQUEST_METHOD, method));
      if let Some(method_orig) = method_orig {
        attributes.push(KeyValue::new(
          otel_keys::HTTP_REQUEST_METHOD_ORIGINAL,
          method_orig.to_string(),
        ));
      }
      if let Some(socket_addr) = req.extensions().get::<axum::extract::ConnectInfo<SocketAddr>>() {
        attributes.push(KeyValue::new(
          otel_keys::CLIENT_ADDRESS,
          normalize_ip_addr(socket_addr.ip()).to_string(),
        ));
        attributes.push(KeyValue::new(otel_keys::CLIENT_PORT, i64::from(socket_addr.port())));
      };
      let uri = req.uri();
      attributes.push(KeyValue::new(otel_keys::URL_PATH, uri.path().to_string()));
      if let Some(query) = uri.query() {
        attributes.push(KeyValue::new(otel_keys::URL_QUERY, query.to_string()));
      }
      if let Some(scheme) = uri.scheme_str() {
        attributes.push(KeyValue::new(otel_keys::URL_SCHEME, scheme.to_string()));
      }
      attributes
    });

    let span = tracer.build(span_builder);
    let ocx = OtelContext::current_with_span(span);

    let inner = self.inner.call(req);

    TraceFuture { inner, ocx }
  }
}

pin_project! {
  pub struct TraceFuture<Fut> {
    #[pin]
    inner: Fut,
    ocx: OtelContext,
  }
}

impl<Fut, ResBody, Err> Future for TraceFuture<Fut>
where
  Fut: Future<Output = Result<Response<ResBody>, Err>>,
{
  type Output = Result<Response<ResBody>, Err>;

  fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
    let this = self.project();
    let out = {
      let guard = this.ocx.clone().attach();
      let out = this.inner.poll(cx);
      drop(guard);
      out
    };
    let out = match out {
      Poll::Pending => return Poll::Pending,
      Poll::Ready(out) => out,
    };
    let span = this.ocx.span();
    match out.as_ref() {
      Ok(out) => {
        let status = out.status();
        span.set_attribute(KeyValue::new(
          otel_keys::HTTP_RESPONSE_STATUS_CODE,
          i64::from(status.as_u16()),
        ));
        if status.is_success() {
          span.set_status(Status::Ok)
        } else if matches!(status.as_u16(), 400..=599) {
          span.set_status(Status::Error {
            description: Cow::Borrowed(status.canonical_reason().unwrap_or("unkHttpError")),
          })
        }
        // else leave status unset
      }
      Err(_) => span.set_status(Status::Error {
        description: Cow::Borrowed("unkHttpError"),
      }),
    }
    span.end();
    Poll::Ready(out)
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct TraceLayer<TyTracerProvider> {
  tracer_provider: TyTracerProvider,
}

impl<TyTracerProvider> TraceLayer<TyTracerProvider> {
  pub fn new(tracer_provider: TyTracerProvider) -> Self {
    Self { tracer_provider }
  }
}

impl<S, TyTracerProvider> Layer<S> for TraceLayer<TyTracerProvider>
where
  TyTracerProvider: Clone,
{
  type Service = Trace<S, TyTracerProvider>;

  fn layer(&self, inner: S) -> Self::Service {
    Trace {
      inner,
      tracer_provider: self.tracer_provider.clone(),
    }
  }
}
