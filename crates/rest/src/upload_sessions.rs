use crate::extract::{Extractor, ParamFromStr};
use axum::extract::Path;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::{patch, post};
use axum::{Extension, Json, Router};
use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{CreateUploadSessionOptions, UploadOptions, UploadSession, UploadSessionId};
use eternalfest_system::EternalfestSystem;
use serde::{Deserialize, Serialize};
use serde_json::json;
use thiserror::Error;

pub fn router() -> Router<()> {
  Router::new()
    .route("/", post(create_upload_session))
    .route("/:upload_session_id", patch(update_upload_session))
}

#[derive(Debug, Error)]
enum CreateUploadSessionError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::blob::CreateUploadSessionError> for CreateUploadSessionError {
  fn from(inner: eternalfest_services::blob::CreateUploadSessionError) -> Self {
    use eternalfest_services::blob::CreateUploadSessionError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalCreateStoreUploadSession(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for CreateUploadSessionError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn create_upload_session(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Json(body): Json<CreateUploadSessionOptions>,
) -> Result<Json<UploadSession>, CreateUploadSessionError> {
  Ok(Json(api.blob.create_upload_session(&acx.value(), &body).await?))
}

#[derive(Debug, Error)]
enum UpdateUploadSessionError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::blob::UpdateUploadSessionError> for UpdateUploadSessionError {
  fn from(inner: eternalfest_services::blob::UpdateUploadSessionError) -> Self {
    use eternalfest_services::blob::UpdateUploadSessionError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalUpload(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for UpdateUploadSessionError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpdateUploadSessionBody {
  pub offset: u32,
  #[serde(
    serialize_with = "eternalfest_core::serde_buffer::buffer_to_hex",
    deserialize_with = "eternalfest_core::serde_buffer::hex_to_buffer"
  )]
  pub data: Vec<u8>,
}

async fn update_upload_session(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(upload_session_id)): Path<ParamFromStr<UploadSessionId>>,
  Json(body): Json<UpdateUploadSessionBody>,
) -> Result<Json<UploadSession>, UpdateUploadSessionError> {
  Ok(Json(
    api
      .blob
      .update_upload_session(
        &acx.value(),
        &UploadOptions {
          upload_session_id,
          offset: body.offset,
          data: body.data,
        },
      )
      .await?,
  ))
}
