use crate::extract::Extractor;
use axum::extract::Path;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::get;
use axum::{Extension, Json, Router};
use eternalfest_core::auth::AuthContext;
use eternalfest_core::game::GameId;
use eternalfest_core::run::HammerfestItemId;
use eternalfest_core::user::{User, UserId};
use eternalfest_services::user::GetUser;
use eternalfest_system::EternalfestSystem;
use serde_json::json;
use std::collections::BTreeMap;
use thiserror::Error;

pub fn router() -> Router<()> {
  Router::new()
    .route("/:user_id", get(get_user))
    .route("/:user_id/profiles/:game_id/items", get(get_user_inventory))
}

#[derive(Debug, Error)]
enum GetUserError {
  #[error("user not found")]
  NotFound,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::user::GetUserError> for GetUserError {
  fn from(e: eternalfest_services::user::GetUserError) -> Self {
    use eternalfest_services::user::GetUserError::*;
    match dbg!(e) {
      NotFound => Self::NotFound,
      Other(_) => Self::Internal,
    }
  }
}

impl IntoResponse for GetUserError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::NotFound => StatusCode::NOT_FOUND,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_user(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(user_id): Path<UserId>,
) -> Result<(StatusCode, Json<User>), GetUserError> {
  let user: User = api
    .user
    .get_user(
      &acx.value(),
      &GetUser {
        id: user_id,
        time: None,
      },
    )
    .await?;
  let status = StatusCode::OK;
  Ok((status, Json(user)))
}

#[derive(Debug, Error)]
enum GetUserInventoryError {
  #[error("user not found")]
  NotFound,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::run::GetUserInventoryError> for GetUserInventoryError {
  fn from(e: eternalfest_services::run::GetUserInventoryError) -> Self {
    use eternalfest_services::run::GetUserInventoryError::*;
    match dbg!(e) {
      GameNotFound => Self::NotFound,
      _ => Self::Internal,
    }
  }
}

impl IntoResponse for GetUserInventoryError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::NotFound => StatusCode::NOT_FOUND,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_user_inventory(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path((user_id, game_id)): Path<(UserId, GameId)>,
) -> Result<(StatusCode, Json<BTreeMap<HammerfestItemId, u32>>), GetUserInventoryError> {
  let inventory: BTreeMap<HammerfestItemId, u32> = api.run.get_user_items(&acx.value(), user_id, game_id, None).await?;
  let status = StatusCode::OK;
  Ok((status, Json(inventory)))
}
