use crate::extract::{Extractor, ParamFromStr};
use axum::extract::{Path, Query};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::{get, post};
use axum::{Extension, Form, Json, Router};
use eternalfest_core::auth::AuthContext;
use eternalfest_core::core::{BoundedVec, Instant, Listing};
use eternalfest_core::run::{CreateRunOptions, HammerfestItemId, JsonValue, Run, RunId, RunStart, SetRunResultOptions};
use eternalfest_system::EternalfestSystem;
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::collections::BTreeMap;

pub fn router() -> Router<()> {
  Router::new()
    .route("/", get(get_runs).post(create_run))
    .route("/:run_id", get(get_run))
    .route("/:run_id/start", post(start_run))
    .route("/:run_id/result", post(set_run_result).put(set_run_result))
}

#[derive(Debug, thiserror::Error)]
enum GetRunsError {
  #[error("forbidden")]
  Forbidden,
}

impl IntoResponse for GetRunsError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_runs(
  Extension(_api): Extension<EternalfestSystem>,
  _acx: Extractor<AuthContext>,
) -> Result<Json<Listing<String>>, GetRunsError> {
  Err(GetRunsError::Forbidden)
}

#[derive(Debug, thiserror::Error)]
enum CreateRunError {
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::run::CreateRunError> for CreateRunError {
  fn from(inner: eternalfest_services::run::CreateRunError) -> Self {
    use eternalfest_services::run::CreateRunError::*;
    match inner {
      GameNotFound => Self::GameNotFound,
      ChannelNotFound => Self::ChannelNotFound,
      Forbidden => Self::Forbidden,
      InternalStoreCreateRun(..) => Self::Internal,
      InternalStoreGetShortUser(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for CreateRunError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::GameNotFound => StatusCode::UNPROCESSABLE_ENTITY,
      Self::ChannelNotFound => StatusCode::UNPROCESSABLE_ENTITY,
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn create_run(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Json(body): Json<CreateRunOptions>,
) -> Result<Json<Run>, CreateRunError> {
  Ok(Json(api.run.create_run(&acx.value(), &body).await?))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct GetRunQuery {
  #[serde(rename = "t")]
  time: Option<Instant>,
}

#[derive(Debug, thiserror::Error)]
enum GetRunError {
  #[error("run not found")]
  RunNotFound,
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::run::GetRunError> for GetRunError {
  fn from(inner: eternalfest_services::run::GetRunError) -> Self {
    use eternalfest_services::run::GetRunError::*;
    match inner {
      RunNotFound => Self::RunNotFound,
      Forbidden => Self::Forbidden,
      GameNotFound => {
        eprintln!("GetRunError: GameNotFound");
        Self::Internal
      }
      ChannelNotFound => {
        eprintln!("GetRunError: ChannelNotFound");
        Self::Internal
      }
      InternalStoreGetShortUser(e) => {
        eprintln!("GetRunError: StoreUser: {e:?}");
        Self::Internal
      }
      Other(e) => {
        eprintln!("GetRunError: {e:?}");
        Self::Internal
      }
    }
  }
}

impl IntoResponse for GetRunError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::RunNotFound => StatusCode::NOT_FOUND,
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_run(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(run_id)): Path<ParamFromStr<RunId>>,
  Query(_query): Query<GetRunQuery>,
) -> Result<Json<Run>, GetRunError> {
  Ok(Json(api.run.get_run(&acx.value(), run_id).await?))
}

#[derive(Debug, thiserror::Error)]
enum StartRunError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::run::StartRunError> for StartRunError {
  fn from(inner: eternalfest_services::run::StartRunError) -> Self {
    use eternalfest_services::run::StartRunError::*;
    match inner {
      Forbidden => Self::Forbidden,
      GameNotFound => Self::Internal,
      ChannelNotFound => Self::Internal,
      InternalStoreStartRun(..) => Self::Internal,
      InternalGetUserItems(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for StartRunError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct StartRunBody {
  // deliberately empty at this moment
}

async fn start_run(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(run_id)): Path<ParamFromStr<RunId>>,
  Form(_body): Form<StartRunBody>,
) -> Result<Json<RunStart>, StartRunError> {
  Ok(Json(api.run.start_run(&acx.value(), run_id).await?))
}

#[derive(Debug, thiserror::Error)]
enum SetRunResultError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
  #[error("failed to read run result")]
  Read(#[source] ReadSetRunResultBodyError),
}

impl From<eternalfest_services::run::SetRunResultError> for SetRunResultError {
  fn from(inner: eternalfest_services::run::SetRunResultError) -> Self {
    use eternalfest_services::run::SetRunResultError::*;
    match inner {
      Forbidden => Self::Forbidden,
      GameNotFound => Self::Internal,
      ChannelNotFound => Self::Internal,
      InternalStoreSetRunResult(..) => Self::Internal,
      InternalStoreGetShortUser(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for SetRunResultError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
      Self::Read(..) => StatusCode::UNPROCESSABLE_ENTITY,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
struct SetRunResultBody {
  pub flash: bool,
  pub is_victory: bool,
  pub max_level: u32,
  pub scores: String,
  pub items: String,
  pub stats: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize, thiserror::Error)]
enum ReadSetRunResultBodyError {
  #[error("invalid `scores` field")]
  Scores,
  #[error("invalid `items` field")]
  Items,
  #[error("invalid `stats` field")]
  Stats,
}

impl TryFrom<SetRunResultBody> for SetRunResultOptions {
  type Error = ReadSetRunResultBodyError;

  fn try_from(value: SetRunResultBody) -> Result<Self, Self::Error> {
    let scores: BoundedVec<i32, 1, 100> =
      serde_json::from_str(&value.scores).map_err(|_| ReadSetRunResultBodyError::Scores)?;
    let items: BTreeMap<HammerfestItemId, u32> =
      serde_json::from_str(&value.items).map_err(|_| ReadSetRunResultBodyError::Items)?;
    let stats: JsonValue = serde_json::from_str(&value.stats).map_err(|_| ReadSetRunResultBodyError::Stats)?;
    Ok(Self {
      is_victory: value.is_victory,
      max_level: value.max_level,
      scores,
      items,
      stats,
    })
  }
}

async fn set_run_result(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(run_id)): Path<ParamFromStr<RunId>>,
  Form(body): Form<SetRunResultBody>,
) -> Result<Json<Run>, SetRunResultError> {
  let options = SetRunResultOptions::try_from(body).map_err(SetRunResultError::Read)?;
  Ok(Json(api.run.set_run_result(&acx.value(), run_id, options).await?))
}
