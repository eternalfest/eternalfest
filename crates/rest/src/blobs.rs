use crate::extract::{Extractor, ParamFromStr};
use axum::body::Body;
use axum::extract::{Path, Query};
use axum::http::header::{CACHE_CONTROL, CONTENT_LENGTH, CONTENT_TYPE};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::{get, post};
use axum::{Extension, Json, Router};
use axum_extra::TypedHeader;
use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{Blob, BlobId, CreateBlobOptions, FullBlob, GetBlobDataOptions, GetBlobOptions};
use eternalfest_core::core::Instant;
use eternalfest_core::digest::DigestSha2;
use eternalfest_core::types::WeakError;
use eternalfest_system::EternalfestSystem;
use headers::HeaderMapExt;
use headers::{ETag, IfNoneMatch};
use serde::{Deserialize, Serialize};
use serde_json::json;
use std::str::FromStr;
use thiserror::Error;

pub fn router() -> Router<()> {
  Router::new()
    .route("/", post(create_blob))
    .route("/:blob_id", get(get_blob))
    .route("/:blob_id/raw", get(get_blob_data))
}

#[derive(Debug, Error)]
enum CreateBlobError {
  #[error("unauthorized")]
  Unauthorized,
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::blob::CreateBlobError> for CreateBlobError {
  fn from(inner: eternalfest_services::blob::CreateBlobError) -> Self {
    use eternalfest_services::blob::CreateBlobError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalCreateStoreBlob(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for CreateBlobError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Unauthorized => StatusCode::UNAUTHORIZED,
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn create_blob(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Json(body): Json<CreateBlobOptions>,
) -> Result<Json<Blob>, CreateBlobError> {
  let acx = &acx.value();
  if matches!(acx, AuthContext::Guest(_)) {
    return Err(CreateBlobError::Unauthorized);
  }
  Ok(Json(api.blob.create_blob(acx, &body).await?))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct GetBlobQuery {
  #[serde(rename = "t")]
  time: Option<Instant>,
}

#[derive(Debug, Error)]
enum GetBlobError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::blob::GetBlobError> for GetBlobError {
  fn from(inner: eternalfest_services::blob::GetBlobError) -> Self {
    use eternalfest_services::blob::GetBlobError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalGetStoreBlob(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for GetBlobError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_blob(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(blob_id)): Path<ParamFromStr<BlobId>>,
  Query(query): Query<GetBlobQuery>,
) -> Result<Json<Blob>, GetBlobError> {
  Ok(Json(
    api
      .blob
      .get_blob(
        &acx.value(),
        &GetBlobOptions {
          id: blob_id,
          time: query.time,
        },
      )
      .await?,
  ))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct GetBlobDataQuery {
  #[serde(rename = "t")]
  time: Option<Instant>,
}

#[derive(Debug, Error)]
enum GetBlobDataError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
  #[error("failed to compute entity tag")]
  InvalidETag(#[source] WeakError),
  #[error("failed to build http response")]
  BuildResponse(#[source] axum::http::Error),
}

impl From<eternalfest_services::blob::GetBlobDataError> for GetBlobDataError {
  fn from(inner: eternalfest_services::blob::GetBlobDataError) -> Self {
    use eternalfest_services::blob::GetBlobDataError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalGetStoreBlobData(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for GetBlobDataError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal | Self::InvalidETag(..) | Self::BuildResponse(..) => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_blob_data(
  Extension(api): Extension<EternalfestSystem>,
  if_none_match: Option<TypedHeader<IfNoneMatch>>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(blob_id)): Path<ParamFromStr<BlobId>>,
  Query(query): Query<GetBlobDataQuery>,
) -> Result<Response, GetBlobDataError> {
  let blob: FullBlob = api
    .blob
    .get_blob_data(
      &acx.value(),
      &GetBlobDataOptions {
        id: blob_id,
        time: query.time,
      },
    )
    .await?;

  debug_assert_eq!(Ok(blob.byte_size), u32::try_from(blob.data.len()));

  let mut etag = Vec::new();
  etag.extend(blob.digest.sha2_256.as_slice());
  etag.extend(blob.digest.sha3_256.as_slice());
  etag.extend(blob.media_type.as_str().as_bytes());
  let etag = DigestSha2::digest(etag.as_slice()).hex();
  // entity tag must be quoted, see <https://datatracker.ietf.org/doc/html/rfc7232#section-2.3>
  // it is safe to just add the tags as below since we know that `etag` is a hex string
  let etag = format!(r#""{etag}""#);
  let etag = ETag::from_str(etag.as_str()).map_err(|e| GetBlobDataError::InvalidETag(WeakError::wrap(e)))?;

  let should_do_work = match if_none_match {
    Some(TypedHeader(if_none_match)) => if_none_match.precondition_passes(&etag),
    _ => true,
  };

  // content-length is provided and corresponds to the body size for 200 OK, this matches the spec:
  // <https://datatracker.ietf.org/doc/html/rfc7230#section-3.3.2>

  let mut res = Response::builder()
    .header(CONTENT_LENGTH, blob.byte_size)
    .header(CONTENT_TYPE, blob.media_type.as_str())
    .header(CACHE_CONTROL, "public,max-age=31536000,immutable");
  res.headers_mut().expect("headers can be retrieved").typed_insert(etag);

  if !should_do_work {
    return res
      .status(StatusCode::NOT_MODIFIED)
      .body(Body::empty())
      .map_err(GetBlobDataError::BuildResponse);
  }

  res
    .status(StatusCode::OK)
    .body(Body::from(blob.data))
    .map_err(GetBlobDataError::BuildResponse)
}

#[cfg(test)]
mod tests {
  use crate::{app, Client, RouterExt};
  use axum::body::Body;
  use axum::http::StatusCode;
  use axum::response::Response;
  use axum::Router;
  use eternalfest_core::blob::{CreateBlobOptions, MediaType};
  use eternalfest_system::EternalfestSystem;
  use http_body_util::BodyExt;
  use std::str::FromStr;

  #[tokio::test]
  async fn test_create_blob_as_guest() {
    let system = EternalfestSystem::for_test().await;
    let router: Router = app(system);
    let mut client: Client = router.client();

    {
      let req = axum::http::Request::builder()
        .method("POST")
        .uri("/api/v1/blobs")
        .header("Content-Type", "application/json")
        .body(Body::from(
          serde_json::to_vec(&CreateBlobOptions {
            media_type: MediaType::from_str("text/plain").expect("test mime type is valid"),
            data: b"Hello, World!".to_vec(),
          })
          .expect("test body is valid"),
        ))
        .unwrap();

      let res: Response<Body> = client.send(req).await;
      assert_eq!(res.status(), StatusCode::UNAUTHORIZED);
      let body: Vec<u8> = res.into_body().collect().await.expect("read body").to_bytes().to_vec();

      let body: &str = std::str::from_utf8(body.as_slice()).unwrap();
      let expected = r#"{"error":"unauthorized"}"#;
      assert_eq!(body, expected);
    }
  }
}
