//! Backend-for-frontend
//! These modules contains frontend-specific code.

pub mod actions;
pub mod oauth;
