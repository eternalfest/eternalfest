use crate::extract::SESSION_COOKIE;
use axum::http::header::LOCATION;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::post;
use axum::Router;
use axum_extra::extract::CookieJar;
use cookie::time::OffsetDateTime;
use cookie::{Cookie, Expiration};
use std::convert::Infallible;

pub mod login;

pub fn router() -> Router<()> {
  Router::new()
    .route("/logout", post(logout))
    .nest("/login", login::router())
}

async fn logout(jar: CookieJar) -> Result<Response, Infallible> {
  let cookie = Cookie::build((SESSION_COOKIE, "".to_string()))
    .path("/")
    .http_only(true)
    .expires(Expiration::DateTime(OffsetDateTime::UNIX_EPOCH))
    .build();
  Ok((StatusCode::FOUND, jar.add(cookie), [(LOCATION, "/")]).into_response())
}
