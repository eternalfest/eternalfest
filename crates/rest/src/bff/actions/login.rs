use axum::http::header::LOCATION;
use axum::http::StatusCode;
use axum::response::{IntoResponse, Redirect, Response};
use axum::routing::post;
use axum::{Extension, Router};
use eternalfest_core::oauth::EternalfestOauthStateAction;
use eternalfest_core::types::WeakError;
use eternalfest_system::EternalfestSystem;

pub fn router() -> Router<()> {
  Router::new()
    .route("/eternaltwin", post(login_with_eternaltwin))
    .route("/etwin", post(login_with_etwin))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, thiserror::Error)]
enum LoginWithEternaltwinError {
  #[error("inner error")]
  Inner(#[from] WeakError),
}

impl IntoResponse for LoginWithEternaltwinError {
  fn into_response(self) -> Response {
    let (code, msg) = match self {
      Self::Inner(_) => (StatusCode::INTERNAL_SERVER_ERROR, "internal server error".to_string()),
    };
    (code, msg).into_response()
  }
}

async fn login_with_eternaltwin(
  Extension(api): Extension<EternalfestSystem>,
) -> Result<Redirect, LoginWithEternaltwinError> {
  let authorization_uri = api
    .auth
    .as_ref()
    .request_eternaltwin_auth(EternalfestOauthStateAction::Login)
    .await?;
  Ok(Redirect::to(authorization_uri.as_str()))
}

// the `/etwin` route is provided for compat with older Eternaldev CLI versions
// in particular, it uses a 302 redirect instead of 303 (303 is better, but compat must be preserved)
async fn login_with_etwin(Extension(api): Extension<EternalfestSystem>) -> Result<Response, LoginWithEternaltwinError> {
  let authorization_uri = api
    .auth
    .as_ref()
    .request_eternaltwin_auth(EternalfestOauthStateAction::Login)
    .await?;
  Ok((StatusCode::FOUND, [(LOCATION, authorization_uri.to_string())]).into_response())
}
