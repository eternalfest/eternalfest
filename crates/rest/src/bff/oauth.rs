use crate::extract::{Extractor, SESSION_COOKIE};
use crate::EternalfestSystem;
use axum::extract::Query;
use axum::http::header::{HeaderName, LOCATION};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::get;
use axum::{Extension, Json, Router};
use axum_extra::extract::CookieJar;
use cookie::{Cookie, Expiration};
use eternalfest_core::auth::{AuthContext, UserAndSession};
use eternalfest_core::oauth::{EternalfestOauthStateAction, EternalfestOauthStateClaims};
use eternalfest_core::types::WeakError;
use eternalfest_services::auth::ReadOauthStateError;
use serde::{Deserialize, Serialize};

pub fn router() -> Router<()> {
  Router::new().route("/callback", get(on_oauth_callback))
}

#[derive(Debug, thiserror::Error)]
enum CallbackError {
  #[error("failed to use oauth code to retrieve user data")]
  UseOauthCode(#[source] WeakError),
  #[error("invalid state {1:?}")]
  InvalidState(#[source] ReadOauthStateError, String),
}

impl IntoResponse for CallbackError {
  fn into_response(self) -> Response {
    let (code, msg) = match dbg!(self) {
      // Self::Inner(e @ Inner::MissingCode) => (StatusCode::UNPROCESSABLE_ENTITY, e.to_string()),
      e @ Self::InvalidState(_, _) => (StatusCode::UNPROCESSABLE_ENTITY, e.to_string()),
      _ => (StatusCode::INTERNAL_SERVER_ERROR, "internal server error".to_string()),
    };
    (code, msg).into_response()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct CallbackQuery {
  code: String,
  state: String,
}

// Called when returning from Eternaltwin for example.
async fn on_oauth_callback(
  Extension(api): Extension<EternalfestSystem>,
  _acx: Extractor<AuthContext>,
  Query(query): Query<CallbackQuery>,
  jar: CookieJar,
) -> Result<
  (
    StatusCode,
    CookieJar,
    [(HeaderName, &'static str); 1],
    Json<serde_json::Value>,
  ),
  CallbackError,
> {
  // let acx = acx.value();
  let state = query.state;
  let state: EternalfestOauthStateClaims = api
    .auth
    .read_oauth_state(state.as_str())
    .map_err(|e| CallbackError::InvalidState(e, state))?;

  match state.action {
    EternalfestOauthStateAction::Login => {
      let user_and_session: UserAndSession = api
        .auth
        .as_ref()
        .eternaltwin_oauth_code(&query.code)
        .await
        .map_err(CallbackError::UseOauthCode)?;

      let cookie = Cookie::build((SESSION_COOKIE, user_and_session.session.id.to_string()))
        .path("/")
        .http_only(true)
        .expires(Expiration::Session)
        .build();

      // TODO: Use `StatusCode::SEE_OTHER` instead of `FOUND` once compat with Eternaldev 0.5.7 is no longer needed.
      Ok((
        StatusCode::FOUND,
        jar.add(cookie),
        [(LOCATION, "/")],
        Json(serde_json::to_value(user_and_session.to_auth_context()).expect("converting to a value always succeeds")),
      ))
    }
  }
}
