use crate::extract::{Extractor, ParamFromStr};
use axum::extract::{Path, Query};
use axum::http::StatusCode;
use axum::response::{IntoResponse, Response};
use axum::routing::{get, patch, post, put};
use axum::{Extension, Json, Router};
use core::num::NonZeroU32;
use eternalfest_core::auth::AuthContext;
use eternalfest_core::core::{Instant, Listing};
use eternalfest_core::game::requests::{
  CreateGame, CreateGameBuild, GetGame, GetGames, SetGameFavorite, UpdateGameChannel,
};
use eternalfest_core::game::{
  ActiveGameChannel, Game, GameBuild, GameChannelKey, GameChannelPatch, GameListItem, GameModeKey, GameRef,
  InputGameBuild, Resolved,
};
use eternalfest_core::run::Leaderboard;
use eternalfest_core::user::UserIdRef;
use eternalfest_system::EternalfestSystem;
use serde::{Deserialize, Serialize};
use serde_json::json;
use thiserror::Error;

pub fn router() -> Router<()> {
  Router::new()
    .route("/", get(get_games).post(create_game))
    .route("/:game_ref", get(get_game))
    .route("/:game_ref/favorite", put(set_game_favorite))
    .route("/:game_ref/builds", post(create_game_build))
    .route("/:game_ref/channels/:channel_key", patch(update_game_channel))
    .route("/:game_ref/leaderboard", get(get_game_leaderboard))
}

#[derive(Debug, Error)]
enum GetGamesError {
  #[error("internal error")]
  Internal(#[from] eternalfest_services::game::GetGamesError),
}

impl IntoResponse for GetGamesError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Internal(_) => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct GetGamesQuery {
  offset: Option<u32>,
  limit: Option<NonZeroU32>,
  #[serde(default)]
  favorite: bool,
  #[serde(rename = "t")]
  time: Option<Instant>,
}

async fn get_games(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Query(query): Query<GetGamesQuery>,
) -> Result<Json<Listing<GameListItem>>, GetGamesError> {
  Ok(Json(
    api
      .game
      .get_games(
        &acx.value(),
        &GetGames {
          offset: query.offset.unwrap_or(0),
          limit: query.limit.unwrap_or(NonZeroU32::new(20).expect("20 != 0")),
          favorite: query.favorite,
          time: query.time,
        },
      )
      .await?,
  ))
}

#[derive(Debug, Error)]
enum CreateGameError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::game::CreateGameError> for CreateGameError {
  fn from(inner: eternalfest_services::game::CreateGameError) -> Self {
    use eternalfest_services::game::CreateGameError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalCreateStoreGame(..) => Self::Internal,
      InternalGetBlobs(..) => Self::Internal,
      InternalGetShortUsers(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for CreateGameError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn create_game(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Json(body): Json<CreateGame>,
) -> Result<Json<Game<Resolved>>, CreateGameError> {
  Ok(Json(api.game.create_game(&acx.value(), &body).await?))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct GetGameQuery {
  // offset: Option<u32>,
  // limit: Option<u32>,
  channel: Option<GameChannelKey>,
  #[serde(rename = "t")]
  time: Option<Instant>,
}

#[derive(Debug, Error)]
enum GetGameError {
  #[error("game not found")]
  GameNotFound,
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::game::GetGameError> for GetGameError {
  fn from(inner: eternalfest_services::game::GetGameError) -> Self {
    use eternalfest_services::game::GetGameError::*;
    match inner {
      NotFound => Self::GameNotFound,
      PermissionRevoked => Self::Forbidden,
      InternalGetGame(..) => Self::Internal,
      InternalGetBlobs(..) => Self::Internal,
      InternalGetShortUsers(..) => Self::Internal,
      InternalGetUserItems(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for GetGameError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::GameNotFound => StatusCode::NOT_FOUND,
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_game(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(game_ref)): Path<ParamFromStr<GameRef>>,
  Query(query): Query<GetGameQuery>,
) -> Result<Json<Game<Resolved>>, GetGameError> {
  Ok(Json(
    api
      .game
      .get_game(
        &acx.value(),
        &GetGame {
          game: game_ref,
          channel: query.channel,
          time: query.time,
        },
      )
      .await?,
  ))
}

#[derive(Debug, Error)]
enum SetGameFavoriteError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::game::SetGameFavoriteError> for SetGameFavoriteError {
  fn from(inner: eternalfest_services::game::SetGameFavoriteError) -> Self {
    use eternalfest_services::game::SetGameFavoriteError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalSetGameFavorite(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for SetGameFavoriteError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Default, Serialize, Deserialize)]
struct SetGameFavoriteBody {
  favorite: bool,
}

async fn set_game_favorite(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(game_ref)): Path<ParamFromStr<GameRef>>,
  Json(body): Json<SetGameFavoriteBody>,
) -> Result<Json<bool>, SetGameFavoriteError> {
  Ok(Json(
    api
      .game
      .set_game_favorite(
        &acx.value(),
        &SetGameFavorite {
          game: game_ref,
          favorite: body.favorite,
        },
      )
      .await?,
  ))
}

#[derive(Debug, Error)]
enum CreateGameBuildError {
  #[error("forbidden")]
  Forbidden,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::game::CreateBuildError> for CreateGameBuildError {
  fn from(inner: eternalfest_services::game::CreateBuildError) -> Self {
    use eternalfest_services::game::CreateBuildError::*;
    match inner {
      Forbidden => Self::Forbidden,
      InternalGetBlobs(..) => Self::Internal,
      InternalCreateBuild(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for CreateGameBuildError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn create_game_build(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(game_ref)): Path<ParamFromStr<GameRef>>,
  Json(body): Json<InputGameBuild>,
) -> Result<Json<GameBuild<Resolved>>, CreateGameBuildError> {
  Ok(Json(
    api
      .game
      .create_build(
        &acx.value(),
        &CreateGameBuild {
          game: game_ref,
          build: body,
        },
      )
      .await?,
  ))
}

#[derive(Debug, Error)]
enum UpdateGameChannelError {
  #[error("forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("game channel not found")]
  GameChannelNotFound,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::game::UpdateGameChannelError> for UpdateGameChannelError {
  fn from(inner: eternalfest_services::game::UpdateGameChannelError) -> Self {
    use eternalfest_services::game::UpdateGameChannelError::*;
    match inner {
      Forbidden => Self::Forbidden,
      GameNotFound(..) => Self::GameNotFound,
      GameChannelNotFound(..) => Self::GameChannelNotFound,
      GetBlobs(..) => Self::Internal,
      ResolveGameBuild(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for UpdateGameChannelError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::GameNotFound => StatusCode::NOT_FOUND,
      Self::GameChannelNotFound => StatusCode::NOT_FOUND,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Default, Serialize, Deserialize)]
struct UpdateGameChannelBody {
  actor: Option<UserIdRef>,
  patches: Vec<GameChannelPatch>,
}

async fn update_game_channel(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path((ParamFromStr(game_ref), ParamFromStr(channel_key))): Path<(
    ParamFromStr<GameRef>,
    ParamFromStr<GameChannelKey>,
  )>,
  Json(body): Json<UpdateGameChannelBody>,
) -> Result<Json<ActiveGameChannel<Resolved>>, UpdateGameChannelError> {
  Ok(Json(
    api
      .game
      .update_channel(
        &acx.value(),
        &UpdateGameChannel {
          actor: body.actor,
          game: game_ref,
          channel_key,
          patches: body.patches,
        },
      )
      .await?,
  ))
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Serialize, Deserialize)]
struct GetGameLeaderboardQuery {
  game_mode: GameModeKey,
  #[serde(rename = "t")]
  time: Option<Instant>,
}

#[derive(Debug, Error)]
enum GetGameLeaderboardError {
  #[error("forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("game channel not found")]
  GameChannelNotFound,
  #[error("internal error")]
  Internal,
}

impl From<eternalfest_services::run::GetLeaderboardError> for GetGameLeaderboardError {
  fn from(inner: eternalfest_services::run::GetLeaderboardError) -> Self {
    use eternalfest_services::run::GetLeaderboardError::*;
    match inner {
      Forbidden => Self::Forbidden,
      GameNotFound => Self::GameNotFound,
      ChannelNotFound => Self::GameChannelNotFound,
      InternalGetGame(..) => Self::Internal,
      InternalGetLeaderboard(..) => Self::Internal,
      InternalStoreGetShortUsers(..) => Self::Internal,
      Other(..) => Self::Internal,
    }
  }
}

impl IntoResponse for GetGameLeaderboardError {
  fn into_response(self) -> Response {
    let status = match &self {
      Self::GameNotFound => StatusCode::NOT_FOUND,
      Self::GameChannelNotFound => StatusCode::NOT_FOUND,
      Self::Forbidden => StatusCode::FORBIDDEN,
      Self::Internal => StatusCode::INTERNAL_SERVER_ERROR,
    };
    (status, Json(json!({ "error": self.to_string() }))).into_response()
  }
}

async fn get_game_leaderboard(
  Extension(api): Extension<EternalfestSystem>,
  acx: Extractor<AuthContext>,
  Path(ParamFromStr(game_ref)): Path<ParamFromStr<GameRef>>,
  Query(query): Query<GetGameLeaderboardQuery>,
) -> Result<Json<Leaderboard>, GetGameLeaderboardError> {
  Ok(Json(
    api.run.get_leaderboard(&acx.value(), game_ref, query.game_mode).await?,
  ))
}
