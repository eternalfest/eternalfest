use async_trait::async_trait;
use eternalfest_core::blob::{BlobId, BlobIdRef};
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::{Instant, Listing, Localized, PeriodLower, SimpleSemVer};
use eternalfest_core::game::requests::CreateGameChannel;
use eternalfest_core::game::store::{
  CreateBuild, CreateBuildError, CreateGameChannelError, CreateStoreGame, CreateStoreGameError, GetStoreGame,
  GetStoreGameError, GetStoreShortGames, GetStoreShortGamesError, StoreSetGameFavorite, StoreSetGameFavoriteError,
  UpdateGameChannelError, UpdateStoreGameChannel,
};
use eternalfest_core::game::{
  ActiveGameChannel, CustomGameEngine, FamilyList, Game, GameBuild, GameBuildI18n, GameCategory, GameChannelKey,
  GameChannelListing, GameChannelPatch, GameChannelPermission, GameDescription, GameDisplayName, GameEngine, GameId,
  GameIdRef, GameKey, GameModeDisplayName, GameModeKey, GameModeSpec, GameOptionDisplayName, GameOptionKey,
  GameOptionSpec, GamePatcher, GameQuests, GameRef, GameResource, GameResourceDisplayName, GameStore, GitCommitRefSha1,
  InputGameBuild, InputGameChannel, PatcherFramework, RefsOnly, ShortGame, ShortGameBuild, ShortGameBuildI18n,
  ShortGameChannel,
};
use eternalfest_core::indexmap::IndexMap;
use eternalfest_core::pg_num::PgU32;
use eternalfest_core::types::WeakError;
use eternalfest_core::user::{UserId, UserIdRef};
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternalfest_core::LocaleId;
use eternalfest_core::SyncRef;
use serde_json::Value as JsonValue;
use sha3::Digest;
use sqlx::postgres::{PgHasArrayType, PgPool, PgQueryResult, PgTypeInfo};
use sqlx::{postgres, Postgres, Transaction};
use std::collections::BTreeMap;
use std::fmt::Debug;
use std::num::NonZeroU32;
use thiserror::Error;
use uuid::Uuid;

pub struct PgGameStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  clock: TyClock,
  database: TyDatabase,
  uuid_generator: TyUuidGenerator,
  quests: &'static eternalfest_core::inventory::QuestDb,
}

impl<TyClock, TyDatabase, TyUuidGenerator> PgGameStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub async fn new(clock: TyClock, database: TyDatabase, uuid_generator: TyUuidGenerator) -> Self {
    Self {
      clock,
      database,
      uuid_generator,
      quests: eternalfest_core::inventory::QuestDb::hardcoded(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StoreGameResource {
  blob: Localized<BlobIdRef>,
  display_name: Option<Localized<GameResourceDisplayName>>,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum StoreGameEngine {
  /// Use Motion-Twin's latest game engine (version 96).
  V96,
  /// Use a custom game engine.
  Custom(BlobIdRef),
}

impl StoreGameEngine {
  pub fn from_input_engine(engine: &GameEngine<RefsOnly>) -> Self {
    match engine {
      GameEngine::V96 => Self::V96,
      GameEngine::Custom(engine) => Self::Custom(engine.blob),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StoreGamePatcher {
  blob: BlobIdRef,
  framework: PatcherFramework,
  meta: Option<JsonValue>,
}

impl StoreGamePatcher {
  pub fn from_input_patcher(patcher: &GamePatcher<RefsOnly>) -> Self {
    Self {
      blob: patcher.blob,
      framework: patcher.framework.clone(),
      meta: patcher.meta.clone(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StoreGameMode {
  display_name: Localized<GameModeDisplayName>,
  is_visible: bool,
  options: IndexMap<GameOptionKey, StoreGameOption>,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct StoreGameOption {
  display_name: Localized<GameOptionDisplayName>,
  is_visible: bool,
  is_enabled: bool,
  default_value: bool,
}

async fn create_game_channel(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  game: GameIdRef,
  channel: &InputGameChannel,
  actor: UserId,
  build: ShortGameBuild<RefsOnly>,
) -> Result<ShortGameChannel<RefsOnly>, WeakError> {
  if channel.default_permission >= GameChannelPermission::Play && channel.version.major == 0 {
    return Err(WeakError::new("major version can't be `0` for public games"));
  }
  {
    // language=PostgreSQL
    let res: PgQueryResult = sqlx::query(
      r"
      INSERT INTO game_channel(game_id, game_channel_key, created_at, first_history_tt_period, first_history_vt_period)
      VALUES ($1::GAME_ID, $2::GAME_CHANNEL_KEY, $3::INSTANT, $4::PERIOD_LOWER, $5::PERIOD_LOWER)
    ",
    )
    .bind(game.id)
    .bind(&channel.key)
    .bind(now)
    .bind(PeriodLower::new(now, None))
    .bind(PeriodLower::new(now, None))
    .execute(&mut **tx)
    .await
    .map_err(WeakError::wrap)?;
    debug_assert_eq!(res.rows_affected(), 1);
  }
  let sort_update_date = channel.sort_update_date.unwrap_or(now);
  {
    // language=PostgreSQL
    let res: PgQueryResult = sqlx::query(
      r"
      INSERT INTO game_channel_history(
        game_id, game_channel_key, tt_period, vt_period, _game_channel_created_at,
        _tt_next_tt_period, _tt_next_vt_period, _vt_next_tt_period, _vt_next_vt_period,
        created_by, version_major, version_minor, version_patch, rank, is_enabled, default_permission,
        is_pinned, publication_date, sort_update_date
      )
      VALUES (
        $1::GAME_ID, $2::GAME_CHANNEL_KEY, $3::PERIOD_LOWER, $4::PERIOD_LOWER, $5::INSTANT,
        NULL, NULL, NULL, NULL,
        $6::USER_ID, $7::U32, $8::U32, $9::U32, (SELECT (10 * COUNT(*)) AS rank FROM game_channel WHERE game_id = $1::GAME_ID), $10::BOOLEAN, $11::GAME_CHANNEL_PERMISSION,
        $12::BOOLEAN, $13::INSTANT, $14::INSTANT
      )
    ",
    )
    .bind(game.id)
    .bind(&channel.key)
    .bind(PeriodLower::new(now, None))
    .bind(PeriodLower::new(now, None))
    .bind(now)
    .bind(actor)
    .bind(PgU32::from(channel.version.major))
    .bind(PgU32::from(channel.version.minor))
    .bind(PgU32::from(channel.version.patch))
    .bind(channel.is_enabled)
    .bind(channel.default_permission)
    .bind(channel.is_pinned)
    .bind(channel.publication_date)
    .bind(sort_update_date)
    .execute(&mut **tx)
    .await.map_err(WeakError::wrap)?;
    debug_assert_eq!(res.rows_affected(), 1);
  }
  Ok(ShortGameChannel {
    key: channel.key.clone(),
    is_enabled: channel.is_enabled,
    is_pinned: channel.is_pinned,
    publication_date: channel.publication_date,
    sort_update_date,
    default_permission: channel.default_permission,
    build,
  })
}

async fn update_game_channel(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  game_id: GameId,
  game_channel_key: &GameChannelKey,
  patch: &GameChannelPatch,
  actor: UserId,
) -> Result<(), WeakError> {
  let vt_period = patch.period.resolve_from(now);
  // TODO: Rewrite with `merge` once available
  // language=PostgreSQL
  let res: PgQueryResult = sqlx::query(
      r"
        WITH
        input_row AS (
          SELECT
            $3::GAME_ID AS game_id, $4::GAME_CHANNEL_KEY AS game_channel_key, PERIOD($1::INSTANT, NULL, '[)') AS tt_period, $2::PERIOD_LOWER AS vt_period, (SELECT created_at FROM game_channel WHERE game_id = $3::GAME_ID AND game_channel.game_channel_key = $4::GAME_CHANNEL_KEY) AS _game_channel_created_at,
            NULL::PERIOD_LOWER AS _tt_next_tt_period, NULL::PERIOD_LOWER AS _tt_next_vt_period, NULL::PERIOD_LOWER AS _vt_next_tt_period, NULL::PERIOD_LOWER AS _vt_next_vt_period,
            $5::USER_ID AS created_by, $6::U32 AS version_major, $7::U32 AS version_minor, $8::U32 AS version_patch,
            $9::BOOLEAN AS is_enabled,
            (SELECT rank FROM game_channel_history WHERE game_id = $3::GAME_ID AND game_channel_key = $4::GAME_CHANNEL_KEY AND vt_period @> $1::INSTANT AND tt_period @> $1::INSTANT) AS rank,
            $10::GAME_CHANNEL_PERMISSION AS default_permission, $11::BOOLEAN AS is_pinned, $12::INSTANT AS publication_date, $13::INSTANT AS sort_update_date
        ),
        input_region AS (SELECT PERIOD($1::INSTANT, NULL, '[)') AS tt, multirange(vt_period) AS vt FROM input_row),
        parents AS (
          SELECT * FROM game_channel_history WHERE game_id = $3::GAME_ID AND game_channel_key = $4::GAME_CHANNEL_KEY AND tt_period @> $1::INSTANT AND vt_period && (SELECT vt FROM input_region)
        ),
        parent_continues AS (
          -- Neighbor slices with a tt still ending at +inf
          SELECT game_id, game_channel_key, tt_period, unnest(multirange(vt_period) - (SELECT vt FROM input_region)) AS vt_period, _game_channel_created_at,
            _tt_next_tt_period, _tt_next_vt_period, _vt_next_tt_period, _vt_next_vt_period,
            created_by, version_major, version_minor, version_patch, is_enabled, rank, default_permission, is_pinned, publication_date, sort_update_date
          FROM parents
        ),
        parent_breaks AS (
          -- Neighbor slices with a tt hitting the cutoff
          SELECT game_id, game_channel_key, (tt_period - (SELECT tt FROM input_region)) AS tt_period, unnest(multirange(vt_period) * (SELECT vt FROM input_region)) AS vt_period, _game_channel_created_at,
            _tt_next_tt_period, _tt_next_vt_period, _vt_next_tt_period, _vt_next_vt_period,
            created_by, version_major, version_minor, version_patch, is_enabled, rank, default_permission, is_pinned, publication_date, sort_update_date
          FROM parents
        ),
        grand_parents AS (
            SELECT gp.game_id, gp.game_channel_key, gp.tt_period, gp.vt_period, gp._game_channel_created_at,
            gp._tt_next_tt_period, gp._tt_next_vt_period, gp._vt_next_tt_period, gp._vt_next_vt_period,
            gp.created_by, gp.version_major, gp.version_minor, gp.version_patch, gp.is_enabled, gp.rank, gp.default_permission, gp.is_pinned, gp.publication_date, gp.sort_update_date
            FROM game_channel_history AS gp INNER JOIN parents AS p
                ON ((gp._tt_next_tt_period = p.tt_period AND gp._tt_next_vt_period = p.vt_period) OR (p._vt_next_tt_period = gp.tt_period AND p._vt_next_vt_period = gp.vt_period))
            WHERE gp.game_id = $3::GAME_ID AND gp.game_channel_key = $4::GAME_CHANNEL_KEY
        ),
        blocks AS (
          -- All blocks updated or inserted
          SELECT * FROM input_row UNION ALL SELECT * FROM parent_continues UNION ALL SELECT * FROM parent_breaks UNION ALL SELECT * FROM grand_parents
        ),
        linked_blocks AS (
            SELECT
            prev.game_id, prev.game_channel_key, prev.tt_period, prev.vt_period, prev._game_channel_created_at,
            tt_next.tt_period AS _tt_next_tt_period, tt_next.vt_period AS _tt_next_vt_period,
            vt_next.tt_period AS _vt_next_tt_period, vt_next.vt_period AS _vt_next_vt_period,
            prev.created_by, prev.version_major, prev.version_minor, prev.version_patch, prev.is_enabled, prev.rank, prev.default_permission, prev.is_pinned, prev.publication_date, prev.sort_update_date
            FROM blocks AS prev
                LEFT OUTER JOIN blocks AS tt_next ON (lower(tt_next.tt_period) = upper(prev.tt_period) AND tt_next.vt_period @> lower(prev.vt_period))
                LEFT OUTER JOIN blocks AS vt_next ON (lower(vt_next.vt_period) = upper(prev.vt_period) AND vt_next.tt_period @> lower(prev.tt_period))
        ),
        updated_rows AS (
          UPDATE game_channel_history
          SET
              tt_period = linked_blocks.tt_period,
              vt_period = linked_blocks.vt_period,
              _tt_next_tt_period = linked_blocks._tt_next_tt_period,
              _tt_next_vt_period = linked_blocks._tt_next_vt_period,
              _vt_next_tt_period = linked_blocks._vt_next_tt_period,
              _vt_next_vt_period = linked_blocks._vt_next_vt_period
          FROM linked_blocks
          WHERE
              game_channel_history.game_id = linked_blocks.game_id
              AND game_channel_history.game_channel_key = linked_blocks.game_channel_key
              AND lower(game_channel_history.tt_period) = lower(linked_blocks.tt_period)
              AND lower(game_channel_history.vt_period) = lower(linked_blocks.vt_period)
          RETURNING game_channel_history.tt_period, game_channel_history.vt_period
        ),
        inserted_rows AS (
          INSERT INTO game_channel_history(
            game_id, game_channel_key, tt_period, vt_period, _game_channel_created_at,
            _tt_next_tt_period, _tt_next_vt_period, _vt_next_tt_period, _vt_next_vt_period,
            created_by, version_major, version_minor, version_patch, is_enabled, rank, default_permission, is_pinned, publication_date, sort_update_date
          )
          SELECT * FROM linked_blocks WHERE
          ROW(tt_period, vt_period) NOT IN (SELECT tt_period, vt_period FROM updated_rows)
          RETURNING tt_period, vt_period
        )
        SELECT * FROM inserted_rows
        UNION ALL
        SELECT * FROM updated_rows;
    ",
    )
    .bind(now)
    .bind(vt_period)
    .bind(game_id)
    .bind(game_channel_key)
    .bind(actor)
    .bind(PgU32::from(patch.version.major))
    .bind(PgU32::from(patch.version.minor))
    .bind(PgU32::from(patch.version.patch))
    .bind(patch.is_enabled)
    .bind(patch.default_permission)
    .bind(patch.is_pinned)
    .bind(patch.publication_date)
    .bind(patch.sort_update_date)
    .execute(&mut **tx)
    .await.map_err(WeakError::wrap)?;
  debug_assert!(res.rows_affected() > 0);
  Ok(())
}

async fn upsert_game_revision(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  game_id: GameId,
  revision: &InputGameBuild,
) -> Result<GameBuild<RefsOnly>, WeakError> {
  let version = revision.version;
  let git_commit_ref = revision.git_commit_ref;

  let (build_id, build) = upsert_game_build(tx, uuid_generator, revision, now).await?;

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    #[allow(unused)]
    game_id: GameId,
  }

  // language=PostgreSQL
  let _row: Row = sqlx::query_as::<_, Row>(
      r"
      INSERT INTO game_version(game_id, version_major, version_minor, version_patch, git_commit_ref, created_at, game_build_id)
      VALUES ($1::GAME_ID, $2::U32, $3::U32, $4::U32, $5::GIT_COMMIT_REF, $6::INSTANT, $7::GAME_BUILD_ID)
      RETURNING game_id;
    ",
    )
      .bind(game_id)
      .bind(PgU32::from(version.major))
      .bind(PgU32::from(version.minor))
      .bind(PgU32::from(version.patch))
      .bind(git_commit_ref)
      .bind(now)
      .bind(build_id)
      .fetch_one(&mut **tx)
      .await.map_err(WeakError::wrap)?;

  Ok(build)
}

async fn upsert_game_build(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  game_build: &InputGameBuild,
  created_at: Instant,
) -> Result<(Uuid, GameBuild<RefsOnly>), WeakError> {
  let main_locale = game_build.main_locale;
  let display_name = upsert_game_display_name(
    tx,
    uuid_generator,
    Localized {
      value: &game_build.display_name,
      main_locale,
      i18n: game_build
        .i18n
        .iter()
        .flat_map(|(l, v)| v.display_name.as_ref().map(|v| (*l, v)))
        .collect(),
    },
  )
  .await
  .map_err(WeakError::wrap)?;
  let description = upsert_game_description(
    tx,
    uuid_generator,
    Localized {
      value: &game_build.description,
      main_locale,
      i18n: game_build
        .i18n
        .iter()
        .flat_map(|(l, v)| v.description.as_ref().map(|v| (*l, v)))
        .collect(),
    },
  )
  .await
  .map_err(WeakError::wrap)?;
  let icon = match &game_build.icon {
    None => None,
    Some(icon) => Some(
      upsert_blob_i18n(
        tx,
        uuid_generator,
        Localized {
          value: *icon,
          main_locale,
          i18n: game_build
            .i18n
            .iter()
            .flat_map(|(l, v)| v.icon.map(|v| (*l, v)))
            .collect(),
        },
      )
      .await?,
    ),
  };

  let mut store_modes: Vec<(GameModeKey, Uuid)> = Vec::new();
  for (mode_key, mode) in &game_build.modes {
    let mode_id = upsert_game_mode(tx, uuid_generator, mode_key, mode, main_locale, &game_build.i18n).await?;
    store_modes.push((mode_key.clone(), mode_id));
  }
  let modes = serde_json::to_string(&store_modes).map_err(WeakError::wrap)?;
  let modes = sha3::Sha3_256::digest(modes);

  let new_uuid = uuid_generator.next();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_build_id: Uuid,
  }

  let (engine_type, custom_engine) = match &game_build.engine {
    GameEngine::V96 => ("V96", None),
    GameEngine::Custom(ref engine) => ("Custom", Some(engine.blob.id)),
  };

  let patcher = match &game_build.patcher {
    None => None,
    Some(ref p) => Some(
      upsert_game_patcher(tx, uuid_generator, p)
        .await
        .map_err(WeakError::wrap)?,
    ),
  };

  let content_i18n = match &game_build.content_i18n {
    None => None,
    Some(bundle) => Some(
      upsert_blob_i18n(
        tx,
        uuid_generator,
        Localized {
          value: *bundle,
          main_locale,
          i18n: game_build
            .i18n
            .iter()
            .flat_map(|(l, v)| v.content_i18n.map(|v| (*l, v)))
            .collect(),
        },
      )
      .await?,
    ),
  };

  let music_list = {
    let store_musics: Vec<_> = game_build
      .musics
      .iter()
      .map(|music| StoreGameResource {
        blob: Localized::one(main_locale, music.blob),
        display_name: music
          .display_name
          .as_ref()
          .map(|dn| Localized::one(main_locale, dn.clone())),
      })
      .collect();
    upsert_game_resource_list(tx, uuid_generator, &store_musics, main_locale).await?
  };

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
      r"
      INSERT INTO game_build(
        game_build_id, main_locale,
        game_display_name_id, game_description_id, icon,
        loader_version_major, loader_version_minor, loader_version_patch, engine_type, custom_engine, patcher, debug,
        content, content_i18n, music_list, families, category, _mode_list_sha3_256)
      VALUES (
        $1::GAME_BUILD_ID, $2::LOCALE_ID,
        $3::GAME_DISPLAY_NAME_I18N_ID, $4::GAME_DESCRIPTION_I18N_ID, $5::BLOB_I18N_ID,
        $6::U32, $7::U32, $8::U32, $9::GAME_ENGINE_TYPE, $10::BLOB_ID, $11::GAME_PATCHER_ID, $12::BLOB_ID,
        $13::BLOB_ID, $14::BLOB_I18N_ID, $15::GAME_RESOURCE_LIST_ID, $16::FAMILIES_STRING, $17::GAME_CATEGORY2, $18::BYTEA)
      ON CONFLICT(main_locale, game_display_name_id, game_description_id, icon, loader_version_major, loader_version_minor, loader_version_patch, engine_type, custom_engine, patcher, debug, content, content_i18n, music_list, families, category, _mode_list_sha3_256) DO UPDATE SET game_build_id = game_build.game_build_id
      RETURNING game_build_id;
    ",
    )
      .bind(new_uuid)
      .bind(main_locale)
      .bind(display_name)
      .bind(description)
      .bind(icon)
      .bind(PgU32::from(game_build.loader.major))
      .bind(PgU32::from(game_build.loader.minor))
      .bind(PgU32::from(game_build.loader.patch))
      .bind(engine_type)
      .bind(custom_engine)
      .bind(patcher)
      .bind(game_build.debug.map(|b| b.id))
      .bind(game_build.content.map(|b| b.id))
      .bind(content_i18n)
      .bind(music_list)
      .bind(&game_build.families)
      .bind(game_build.category)
      .bind(modes.as_slice())
      .fetch_one(&mut **tx)
      .await.map_err(WeakError::wrap)?;

  let game_build_id = row.game_build_id;

  if game_build_id == new_uuid {
    // First insertion, populate modes
    let mut prev_rank: Option<u32> = None;
    for (rank, (key, id)) in store_modes.into_iter().enumerate() {
      let rank = u32::try_from(rank).map_err(WeakError::wrap)?;
      // language=PostgreSQL
      let row: PgQueryResult = sqlx::query(
        r"
      INSERT INTO game_build_mode(game_build_id, main_locale, rank, game_mode_key, game_mode_id, _prev)
      VALUES ($1::GAME_BUILD_ID, $2::LOCALE_ID, $3::U32, $4::GAME_MODE_KEY, $5::GAME_MODE_ID, $6::U32);
    ",
      )
      .bind(game_build_id)
      .bind(main_locale)
      .bind(PgU32::from(rank))
      .bind(key)
      .bind(id)
      .bind(prev_rank.map(PgU32::from))
      .execute(&mut **tx)
      .await
      .map_err(WeakError::wrap)?;
      debug_assert_eq!(row.rows_affected(), 1);
      prev_rank = Some(rank);
    }
  }

  let build = GameBuild {
    version: game_build.version,
    git_commit_ref: game_build.git_commit_ref,
    created_at,
    main_locale,
    display_name: game_build.display_name.clone(),
    description: game_build.description.clone(),
    icon: game_build.icon,
    loader: game_build.loader,
    engine: game_build.engine.clone(),
    patcher: game_build.patcher.clone(),
    debug: game_build.debug,
    content: game_build.content,
    musics: game_build.musics.clone(),
    content_i18n: game_build.content_i18n,
    modes: game_build.modes.clone(),
    families: game_build.families.clone(),
    category: game_build.category,
    i18n: game_build.i18n.clone(),
  };

  Ok((game_build_id, build))
}

async fn upsert_game_resource_list(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  resources: &[StoreGameResource],
  main_locale: LocaleId,
) -> Result<Uuid, WeakError> {
  let mut item_ids: Vec<Uuid> = Vec::new();
  for resource in resources {
    let resource_id = upsert_game_resource(tx, uuid_generator, resource, main_locale).await?;
    item_ids.push(resource_id);
  }
  let items = serde_json::to_string(&item_ids).map_err(WeakError::wrap)?;
  let items = sha3::Sha3_256::digest(items);

  let new_uuid = uuid_generator.next();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_resource_list_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_resource_list(game_resource_list_id, main_locale, size, _sha3_256)
    VALUES ($1::GAME_RESOURCE_LIST_ID, $2::LOCALE_ID, $3::U32, $4::BYTEA)
    ON CONFLICT(main_locale, size, _sha3_256) DO UPDATE SET game_resource_list_id = game_resource_list.game_resource_list_id
    RETURNING game_resource_list_id;
  ",
  )
  .bind(new_uuid)
  .bind(main_locale)
  .bind(PgU32::from(u32::try_from(item_ids.len()).map_err(WeakError::wrap)?))
  .bind(items.as_slice())
  .fetch_one(&mut **tx)
  .await.map_err(WeakError::wrap)?;

  let list_id = row.game_resource_list_id;

  if list_id == new_uuid {
    // First insertion, populate options
    let mut prev_rank: Option<u32> = None;
    for (rank, resource_id) in item_ids.into_iter().enumerate() {
      let rank = u32::try_from(rank).map_err(WeakError::wrap)?;
      // language=PostgreSQL
      let row: PgQueryResult = sqlx::query(
        r"
    INSERT INTO game_resource_list_item(game_resource_list_id, main_locale, rank, game_resource_id, _prev)
    VALUES ($1::GAME_RESOURCE_LIST_ID, $2::LOCALE_ID, $3::U32, $4::GAME_RESOURCE_ID, $5::U32);
  ",
      )
      .bind(list_id)
      .bind(main_locale)
      .bind(PgU32::from(rank))
      .bind(resource_id)
      .bind(prev_rank.map(PgU32::from))
      .execute(&mut **tx)
      .await
      .map_err(WeakError::wrap)?;
      debug_assert_eq!(row.rows_affected(), 1);
      prev_rank = Some(rank);
    }
  }

  Ok(list_id)
}

async fn upsert_game_resource(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  resource: &StoreGameResource,
  main_locale: LocaleId,
) -> Result<Uuid, WeakError> {
  let blob_i18n_id = upsert_blob_i18n(tx, uuid_generator, resource.blob.clone()).await?;
  let display_name = match &resource.display_name {
    None => None,
    Some(display_name) => Some(
      upsert_game_resource_display_name(tx, uuid_generator, display_name.clone())
        .await
        .map_err(WeakError::wrap)?,
    ),
  };

  let new_uuid = uuid_generator.next();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_resource_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_resource(game_resource_id, main_locale, blob_i18n_id, display_name)
    VALUES ($1::GAME_RESOURCE_ID, $2::LOCALE_ID, $3::BLOB_I18N_ID, $4::GAME_RESOURCE_DISPLAY_NAME_I18N_ID)
    ON CONFLICT(main_locale, blob_i18n_id, display_name) DO UPDATE SET game_resource_id = game_resource.game_resource_id
    RETURNING game_resource_id;
  ",
  )
  .bind(new_uuid)
  .bind(main_locale)
  .bind(blob_i18n_id)
  .bind(display_name)
  .fetch_one(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;

  Ok(row.game_resource_id)
}

async fn upsert_game_resource_display_name(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  display_name: Localized<GameResourceDisplayName>,
) -> Result<Uuid, sqlx::Error> {
  let new_uuid = uuid_generator.next();
  let mut items: Vec<GameResourceDisplayNameI18nItem> = vec![GameResourceDisplayNameI18nItem {
    locale: display_name.main_locale,
    value: display_name.value.clone(),
  }];
  items.extend(
    display_name
      .i18n
      .into_iter()
      .map(|(locale, value)| GameResourceDisplayNameI18nItem { locale, value }),
  );
  items.sort_unstable();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_resource_display_name_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_resource_display_name_i18n(game_resource_display_name_id, main_locale, items)
    VALUES ($1::GAME_RESOURCE_DISPLAY_NAME_I18N_ID, $2::LOCALE_ID, $3::game_resource_display_name_i18n_item[])
    ON CONFLICT(main_locale, items) DO UPDATE SET game_resource_display_name_id = game_resource_display_name_i18n.game_resource_display_name_id
    RETURNING game_resource_display_name_id;
  ",
  )
    .bind(new_uuid)
    .bind(display_name.main_locale)
    .bind(items)
    .fetch_one(&mut **tx)
    .await?;

  Ok(row.game_resource_display_name_id)
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameResourceDisplayNameI18nItem {
  pub locale: LocaleId,
  pub value: GameResourceDisplayName,
}

impl sqlx::Type<Postgres> for GameResourceDisplayNameI18nItem {
  fn type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("game_resource_display_name_i18n_item")
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == PgTypeInfo::with_name("game_resource_display_name_i18n_item")
  }
}

impl<'q> sqlx::Encode<'q, Postgres> for GameResourceDisplayNameI18nItem {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(self.locale);
    encoder.encode(&self.value);
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}

impl PgHasArrayType for GameResourceDisplayNameI18nItem {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_game_resource_display_name_i18n_item")
  }
}

async fn upsert_game_patcher(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  patcher: &GamePatcher<RefsOnly>,
) -> Result<Uuid, sqlx::Error> {
  let new_uuid = uuid_generator.next();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_patcher_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_patcher(
      game_patcher_id, blob_id, patcher_framework_name,
      patcher_framework_version_major, patcher_framework_version_minor, patcher_framework_version_patch, meta)
    VALUES (
      $1::GAME_PATCHER_ID, $2::BLOB_ID, $3::PATCHER_FRAMEWORK_NAME,
      $4::U32, $5::U32, $6::U32, $7::JSONB)
    ON CONFLICT(blob_id, patcher_framework_name, patcher_framework_version_major, patcher_framework_version_minor, patcher_framework_version_patch, meta) DO UPDATE SET game_patcher_id = game_patcher.game_patcher_id
    RETURNING game_patcher_id;
  ",
  )
  .bind(new_uuid)
  .bind(patcher.blob.id)
  .bind(&patcher.framework.name)
  .bind(PgU32::from(patcher.framework.version.major))
  .bind(PgU32::from(patcher.framework.version.minor))
  .bind(PgU32::from(patcher.framework.version.patch))
  .bind(patcher.meta.as_ref())
  .fetch_one(&mut **tx)
  .await?;

  Ok(row.game_patcher_id)
}

async fn upsert_game_display_name(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  display_name: Localized<&GameDisplayName>,
) -> Result<Uuid, sqlx::Error> {
  let new_uuid = uuid_generator.next();
  let mut items: Vec<GameDisplayNameI18nItem> = vec![GameDisplayNameI18nItem {
    locale: display_name.main_locale,
    value: display_name.value.clone(),
  }];
  items.extend(
    display_name
      .i18n
      .into_iter()
      .map(|(locale, value)| GameDisplayNameI18nItem {
        locale,
        value: value.clone(),
      }),
  );
  items.sort_unstable();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_display_name_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_display_name_i18n(game_display_name_id, main_locale, items)
    VALUES ($1::GAME_DISPLAY_NAME_I18N_ID, $2::LOCALE_ID, $3::game_display_name_i18n_item[])
    ON CONFLICT(main_locale, items) DO UPDATE SET game_display_name_id = game_display_name_i18n.game_display_name_id
    RETURNING game_display_name_id;
  ",
  )
  .bind(new_uuid)
  .bind(display_name.main_locale)
  .bind(items)
  .fetch_one(&mut **tx)
  .await?;

  Ok(row.game_display_name_id)
}

async fn upsert_game_description(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  description: Localized<&GameDescription>,
) -> Result<Uuid, sqlx::Error> {
  let new_uuid = uuid_generator.next();
  let mut items: Vec<GameDescriptionI18nItem> = vec![GameDescriptionI18nItem {
    locale: description.main_locale,
    value: description.value.clone(),
  }];
  items.extend(
    description
      .i18n
      .into_iter()
      .map(|(locale, value)| GameDescriptionI18nItem {
        locale,
        value: value.clone(),
      }),
  );
  items.sort_unstable();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_description_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_description_i18n(game_description_id, main_locale, items)
    VALUES ($1::GAME_DESCRIPTION_I18N_ID, $2::LOCALE_ID, $3::game_description_i18n_item[])
    ON CONFLICT(main_locale, items) DO UPDATE SET game_description_id = game_description_i18n.game_description_id
    RETURNING game_description_id;
  ",
  )
  .bind(new_uuid)
  .bind(description.main_locale)
  .bind(items)
  .fetch_one(&mut **tx)
  .await?;

  Ok(row.game_description_id)
}

async fn upsert_blob_i18n(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  blob: Localized<BlobIdRef>,
) -> Result<Uuid, WeakError> {
  let new_uuid = uuid_generator.next();
  let mut store_items: BTreeMap<LocaleId, BlobId> = BTreeMap::new();
  store_items.insert(blob.main_locale, blob.value.id);
  for (l, v) in &blob.i18n {
    store_items.insert(*l, v.id);
  }
  let items = serde_json::to_string(&store_items).map_err(WeakError::wrap)?;
  let items = sha3::Sha3_256::digest(items);

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    blob_i18n_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO blob_i18n(blob_i18n_id, main_locale, _sha3_256)
    VALUES ($1::BLOB_I18N_ID, $2::LOCALE_ID, $3::BYTEA)
    ON CONFLICT(main_locale, _sha3_256) DO UPDATE SET blob_i18n_id = blob_i18n.blob_i18n_id
    RETURNING blob_i18n_id;
  ",
  )
  .bind(new_uuid)
  .bind(blob.main_locale)
  .bind(items.as_slice())
  .fetch_one(&mut **tx)
  .await
  .map_err(WeakError::wrap)?;

  let blob_i18n_id = row.blob_i18n_id;

  if blob_i18n_id == new_uuid {
    // First insert, populate
    for (locale, blob_id) in store_items {
      // language=PostgreSQL
      let row: PgQueryResult = sqlx::query(
        r"
        INSERT INTO blob_i18n_item(blob_i18n_id, locale, blob_id)
        VALUES ($1::BLOB_I18N_ID, $2::LOCALE_ID, $3::BLOB_ID)
        ",
      )
      .bind(blob_i18n_id)
      .bind(locale)
      .bind(blob_id)
      .execute(&mut **tx)
      .await
      .map_err(WeakError::wrap)?;
      debug_assert_eq!(row.rows_affected(), 1);
    }
  }

  Ok(blob_i18n_id)
}

async fn upsert_game_mode(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  mode_key: &GameModeKey,
  mode: &GameModeSpec,
  main_locale: LocaleId,
  i18n: &BTreeMap<LocaleId, GameBuildI18n<RefsOnly>>,
) -> Result<Uuid, WeakError> {
  let display_name = upsert_game_mode_display_name(
    tx,
    uuid_generator,
    Localized {
      value: &mode.display_name,
      main_locale,
      i18n: i18n
        .iter()
        .flat_map(|(l, v)| {
          v.modes
            .get(mode_key)
            .and_then(|v| v.display_name.as_ref().map(|v| (*l, v)))
        })
        .collect(),
    },
  )
  .await
  .map_err(WeakError::wrap)?;

  let mut store_options: Vec<(GameOptionKey, Uuid)> = Vec::new();
  for (option_key, option) in &mode.options {
    let option_id = upsert_game_option(tx, uuid_generator, (mode_key, option_key), option, main_locale, i18n)
      .await
      .map_err(WeakError::wrap)?;
    store_options.push((option_key.clone(), option_id));
  }
  let options = serde_json::to_string(&store_options).map_err(WeakError::wrap)?;
  let options = sha3::Sha3_256::digest(options);

  let new_uuid = uuid_generator.next();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_mode_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_mode(game_mode_id, main_locale, display_name, is_visible, _option_list_sha3_256)
    VALUES ($1::GAME_MODE_ID, $2::LOCALE_ID, $3::GAME_MODE_DISPLAY_NAME_I18N_ID, $4::BOOLEAN, $5::BYTEA)
    ON CONFLICT(main_locale, display_name, is_visible, _option_list_sha3_256) DO UPDATE SET game_mode_id = game_mode.game_mode_id
    RETURNING game_mode_id;
  ",
  )
    .bind(new_uuid)
    .bind(main_locale)
    .bind(display_name)
    .bind(mode.is_visible)
    .bind(options.as_slice())
    .fetch_one(&mut **tx)
    .await.map_err(WeakError::wrap)?;

  let game_mode_id = row.game_mode_id;

  if game_mode_id == new_uuid {
    // First insertion, populate options
    let mut prev_rank: Option<u32> = None;
    for (rank, (key, id)) in store_options.into_iter().enumerate() {
      let rank = u32::try_from(rank).map_err(WeakError::wrap)?;
      // language=PostgreSQL
      let row: PgQueryResult = sqlx::query(
        r"
    INSERT INTO game_mode_option(game_mode_id, main_locale, rank, game_option_key, game_option_id, _prev)
    VALUES ($1::GAME_MODE_ID, $2::LOCALE_ID, $3::U32, $4::GAME_OPTION_KEY, $5::GAME_OPTION_ID, $6::U32)
--     ON CONFLICT(main_locale, display_name, is_visible, _option_list_sha3_256) DO UPDATE SET game_mode_id = game_mode.game_mode_id
--     RETURNING game_mode_id;
  ",
      )
        .bind(game_mode_id)
        .bind(main_locale)
        .bind(PgU32::from(rank))
        .bind(key)
        .bind(id)
        .bind(prev_rank.map(PgU32::from))
        .execute(&mut **tx)
        .await.map_err(WeakError::wrap)?;
      debug_assert_eq!(row.rows_affected(), 1);
      prev_rank = Some(rank);
    }
  }

  Ok(game_mode_id)
}

async fn upsert_game_mode_display_name(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  display_name: Localized<&GameModeDisplayName>,
) -> Result<Uuid, sqlx::Error> {
  let new_uuid = uuid_generator.next();
  let mut items: Vec<GameModeDisplayNameI18nItem> = vec![GameModeDisplayNameI18nItem {
    locale: display_name.main_locale,
    value: display_name.value.clone(),
  }];
  items.extend(
    display_name
      .i18n
      .into_iter()
      .map(|(locale, value)| GameModeDisplayNameI18nItem {
        locale,
        value: value.clone(),
      }),
  );
  items.sort_unstable();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_mode_display_name_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_mode_display_name_i18n(game_mode_display_name_id, main_locale, items)
    VALUES ($1::GAME_MODE_DISPLAY_NAME_I18N_ID, $2::LOCALE_ID, $3::game_mode_display_name_i18n_item[])
    ON CONFLICT(main_locale, items) DO UPDATE SET game_mode_display_name_id = game_mode_display_name_i18n.game_mode_display_name_id
    RETURNING game_mode_display_name_id;
  ",
  )
    .bind(new_uuid)
    .bind(display_name.main_locale)
    .bind(items)
    .fetch_one(&mut **tx)
    .await?;

  Ok(row.game_mode_display_name_id)
}

async fn upsert_game_option(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  (mode_key, option_key): (&GameModeKey, &GameOptionKey),
  option: &GameOptionSpec,
  main_locale: LocaleId,
  i18n: &BTreeMap<LocaleId, GameBuildI18n<RefsOnly>>,
) -> Result<Uuid, sqlx::Error> {
  let display_name = upsert_game_option_display_name(
    tx,
    uuid_generator,
    Localized {
      value: &option.display_name,
      main_locale,
      i18n: i18n
        .iter()
        .flat_map(|(l, v)| {
          v.modes.get(mode_key).and_then(|v| {
            v.options
              .get(option_key)
              .and_then(|v| v.display_name.as_ref().map(|v| (*l, v)))
          })
        })
        .collect(),
    },
  )
  .await?;
  let new_uuid = uuid_generator.next();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_option_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_option(game_option_id, main_locale, display_name, is_visible, is_enabled, default_value)
    VALUES ($1::GAME_OPTION_ID, $2::LOCALE_ID, $3::GAME_OPTION_DISPLAY_NAME_I18N_ID, $4::BOOLEAN, $5::BOOLEAN, $6::BOOLEAN)
    ON CONFLICT(main_locale, display_name, is_visible, is_enabled, default_value) DO UPDATE SET game_option_id = game_option.game_option_id
    RETURNING game_option_id;
  ",
  )
    .bind(new_uuid)
    .bind(main_locale)
    .bind(display_name)
    .bind(option.is_visible)
    .bind(option.is_enabled)
    .bind(option.default_value)
    .fetch_one(&mut **tx)
    .await?;

  Ok(row.game_option_id)
}

async fn upsert_game_option_display_name(
  tx: &mut Transaction<'_, Postgres>,
  uuid_generator: &(impl ?Sized + UuidGenerator),
  display_name: Localized<&GameOptionDisplayName>,
) -> Result<Uuid, sqlx::Error> {
  let new_uuid = uuid_generator.next();
  let mut items: Vec<GameOptionDisplayNameI18nItem> = vec![GameOptionDisplayNameI18nItem {
    locale: display_name.main_locale,
    value: display_name.value.clone(),
  }];
  items.extend(
    display_name
      .i18n
      .into_iter()
      .map(|(locale, value)| GameOptionDisplayNameI18nItem {
        locale,
        value: value.clone(),
      }),
  );
  items.sort_unstable();

  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_option_display_name_id: Uuid,
  }

  // language=PostgreSQL
  let row: Row = sqlx::query_as::<_, Row>(
    r"
    INSERT INTO game_option_display_name_i18n(game_option_display_name_id, main_locale, items)
    VALUES ($1::GAME_OPTION_DISPLAY_NAME_I18N_ID, $2::LOCALE_ID, $3::game_option_display_name_i18n_item[])
    ON CONFLICT(main_locale, items) DO UPDATE SET game_option_display_name_id = game_option_display_name_i18n.game_option_display_name_id
    RETURNING game_option_display_name_id;
  ",
  )
  .bind(new_uuid)
  .bind(display_name.main_locale)
  .bind(items)
  .fetch_one(&mut **tx)
  .await?;

  Ok(row.game_option_display_name_id)
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameDisplayNameI18nItem {
  pub locale: LocaleId,
  pub value: GameDisplayName,
}

impl sqlx::Type<Postgres> for GameDisplayNameI18nItem {
  fn type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("game_display_name_i18n_item")
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == PgTypeInfo::with_name("game_display_name_i18n_item")
  }
}

impl<'q> sqlx::Encode<'q, Postgres> for GameDisplayNameI18nItem {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(self.locale);
    encoder.encode(&self.value);
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}

impl PgHasArrayType for GameDisplayNameI18nItem {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_game_display_name_i18n_item")
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameDescriptionI18nItem {
  pub locale: LocaleId,
  pub value: GameDescription,
}

impl sqlx::Type<Postgres> for GameDescriptionI18nItem {
  fn type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("game_description_i18n_item")
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == PgTypeInfo::with_name("game_description_i18n_item")
  }
}

impl<'q> sqlx::Encode<'q, Postgres> for GameDescriptionI18nItem {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(self.locale);
    encoder.encode(&self.value);
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}

impl PgHasArrayType for GameDescriptionI18nItem {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_game_description_i18n_item")
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameModeDisplayNameI18nItem {
  pub locale: LocaleId,
  pub value: GameModeDisplayName,
}

impl sqlx::Type<Postgres> for GameModeDisplayNameI18nItem {
  fn type_info() -> postgres::PgTypeInfo {
    postgres::PgTypeInfo::with_name("game_mode_display_name_i18n_item")
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == postgres::PgTypeInfo::with_name("game_mode_display_name_i18n_item")
  }
}

impl<'q> sqlx::Encode<'q, Postgres> for GameModeDisplayNameI18nItem {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(self.locale);
    encoder.encode(&self.value);
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}

impl PgHasArrayType for GameModeDisplayNameI18nItem {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_game_mode_display_name_i18n_item")
  }
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameOptionDisplayNameI18nItem {
  pub locale: LocaleId,
  pub value: GameOptionDisplayName,
}

impl sqlx::Type<Postgres> for GameOptionDisplayNameI18nItem {
  fn type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("game_option_display_name_i18n_item")
  }

  fn compatible(ty: &PgTypeInfo) -> bool {
    *ty == Self::type_info() || *ty == PgTypeInfo::with_name("game_option_display_name_i18n_item")
  }
}

impl<'q> sqlx::Encode<'q, Postgres> for GameOptionDisplayNameI18nItem {
  fn encode_by_ref(&self, buf: &mut postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    let mut encoder = postgres::types::PgRecordEncoder::new(buf);
    encoder.encode(self.locale);
    encoder.encode(&self.value);
    encoder.finish();
    sqlx::encode::IsNull::No
  }
}

impl PgHasArrayType for GameOptionDisplayNameI18nItem {
  fn array_type_info() -> PgTypeInfo {
    PgTypeInfo::with_name("_game_option_display_name_i18n_item")
  }
}

#[derive(Debug, Error)]
enum ResolveGameRefError {
  #[error("game not found")]
  GameNotFound,
  #[error("database error")]
  Database(#[from] sqlx::Error),
}

async fn resolve_game_ref(
  tx: &mut Transaction<'_, Postgres>,
  time: Instant,
  game_ref: &GameRef,
) -> Result<GameIdRef, ResolveGameRefError> {
  match game_ref {
    GameRef::Id(id) => Ok(*id),
    GameRef::Key(key) => {
      #[derive(Debug, sqlx::FromRow)]
      struct Row {
        game_id: GameId,
      }

      // language=PostgreSQL
      let row: Option<Row> = sqlx::query_as::<_, Row>(
        r"
        SELECT game_id FROM game_key_history WHERE period @> $1::INSTANT AND game_key = $2::GAME_KEY;
      ",
      )
      .bind(time)
      .bind(&key.key)
      .fetch_optional(&mut **tx)
      .await?;
      let row = row.ok_or(ResolveGameRefError::GameNotFound)?;
      Ok(GameIdRef::new(row.game_id))
    }
  }
}

// pub struct RawGame {
//   game_id: GameId,
//   created_at: Instant,
//   owner: UserId,
//   game_channel_key: GameChannelKey,
//   is_enabled: bool,
//   is_visible_now: bool,
// }

#[allow(clippy::too_many_arguments)]
async fn get_games(
  tx: &mut Transaction<'_, Postgres>,
  time: Instant,
  now: Instant,
  offset: u32,
  limit: NonZeroU32,
  actor: Option<Option<UserIdRef>>,
  require_enabled: bool,
  favorite: bool,
  is_tester: bool,
) -> Result<Listing<(ShortGame<RefsOnly>, bool)>, WeakError> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_count: i64,
    game_id: GameId,
    game_key: Option<GameKey>,
    created_at: Instant,
    owner: UserId,
    game_channel_key: GameChannelKey,
    is_enabled: bool,
    is_pinned: bool,
    publication_date: Option<Instant>,
    sort_update_date: Instant,
    default_permission: GameChannelPermission,
    version_major: PgU32,
    version_minor: PgU32,
    version_patch: PgU32,
    channel_rank: i64,
    channel_count: i64,
    is_visible_now: bool,
    main_locale: LocaleId,
    display_name: Vec<(LocaleId, GameDisplayName)>,
    description: Vec<(LocaleId, GameDescription)>,
    icon: Option<Vec<(LocaleId, BlobId)>>,
  }

  let (view_all, user_id, user_favorite) = match actor {
    None => (true, None, None),
    Some(user) => {
      let u = user.map(|u| u.id);
      (false, u, if favorite { u } else { None })
    }
  };

  // language=PostgreSQL
  let rows: Vec<Row> = sqlx::query_as::<_, Row>(
    r"
      WITH channels AS (
        SELECT game_id, g.created_at, owner,
          game_channel_key, is_enabled, is_pinned, publication_date, sort_update_date, default_permission, version_major, version_minor, version_patch,
          ROW_NUMBER() OVER w AS channel_rank,
          COUNT(*) OVER w AS channel_count,
          (
            $4::BOOLEAN
            OR $2::INSTANT = $1::INSTANT
            OR g.owner = $5::USER_ID
            OR EXISTS (
              SELECT 1
              FROM game_channel_history AS now_gch
              WHERE
                now_gch.game_id = gch.game_id
                AND now_gch.game_channel_key = gch.game_channel_key
                AND now_gch.tt_period @> $2::INSTANT
                AND now_gch.vt_period @> $2::INSTANT
                AND now_gch.default_permission IN ('View', 'Play', 'Debug', 'Manage')
          )) AS is_visible_now
        FROM game_channel_history AS gch
          INNER JOIN game_channel AS gc USING (game_id, game_channel_key)
          INNER JOIN game AS g USING (game_id)
        WHERE g.created_at <= $1::INSTANT
          AND gc.created_at <= $1::INSTANT
          AND tt_period @> $1::INSTANT
          AND vt_period @> $1::INSTANT
          AND gch.is_enabled >= $3::BOOLEAN
          AND (
            $4::BOOLEAN
            OR g.owner = $5::USER_ID
            OR gch.default_permission IN ('View', 'Play', 'Debug', 'Manage')
          )
        WINDOW w AS (PARTITION BY (game_id) ORDER BY (NOT is_enabled, gch.rank, gc.created_at) ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
        ORDER BY game_id, NOT is_enabled, gch.rank, gc.created_at
      ),
      games AS (
        SELECT DISTINCT ON (game_id) game_id, owner, game_channel_key, is_enabled, is_pinned, publication_date, sort_update_date, default_permission, version_major, version_minor, version_patch, created_at, channel_rank, channel_count, is_visible_now
        FROM channels AS c
        WHERE $8::USER_ID IS NULL OR ($8::USER_ID IS NOT NULL AND EXISTS(SELECT 1 FROM user_favorite_game AS ufg WHERE ufg.game_id = c.game_id AND ufg.user_id = $8::USER_ID AND ufg.period @> $1::INSTANT))
        ORDER BY game_id, is_visible_now DESC, channel_rank
      ),
      game_count AS (
        SELECT COUNT(*) AS count FROM games
      )
      SELECT (SELECT count FROM game_count) AS game_count,
        game_id, game_key, g.created_at, owner,
        game_channel_key, is_enabled, is_pinned, publication_date, sort_update_date, default_permission, version_major, version_minor, version_patch,
        channel_rank, channel_count, is_visible_now,
        game_build.main_locale,
        -- Cast away the item type (SQLx workaround)
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(display_name.items) AS GAME_DISPLAY_NAME_I18N_ITEM) AS i) AS display_name,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(description.items) AS GAME_DESCRIPTION_I18N_ITEM) AS i) AS description,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = icon.blob_i18n_id) AS icon
      FROM games AS g
        LEFT OUTER JOIN game_key_history AS gkh USING(game_id)
        INNER JOIN game_version USING (game_id, version_major, version_minor, version_patch)
        INNER JOIN game_build USING (game_build_id)
        INNER JOIN game_display_name_i18n AS display_name USING (game_display_name_id, main_locale)
        INNER JOIN game_description_i18n AS description USING (game_description_id, main_locale)
        LEFT OUTER JOIN blob_i18n AS icon ON (icon.main_locale = game_build.main_locale AND icon.blob_i18n_id = game_build.icon)
      WHERE gkh.period IS NULL OR gkh.period @> $1::INSTANT
      ORDER BY is_pinned DESC, sort_update_date DESC
      LIMIT $6::U32 OFFSET $7::U32;
  ",
  )
  .bind(time)
  .bind(now)
  .bind(require_enabled)
  .bind(view_all || is_tester)
  .bind(user_id)
  .bind(PgU32::from(limit.get()))
  .bind(PgU32::from(offset))
  .bind(user_favorite)
  .fetch_all(&mut **tx)
  .await.map_err(WeakError::wrap)?;

  let mut items = Vec::<(ShortGame<_>, bool)>::new();
  let mut count: u32 = 0;

  for r in rows {
    count = u32::try_from(r.game_count).map_err(WeakError::wrap)?;
    items.push((
      ShortGame {
        id: r.game_id,
        created_at: r.created_at,
        key: r.game_key,
        owner: UserIdRef::new(r.owner),
        channels: Listing {
          offset: r
            .channel_rank
            .checked_sub(1)
            .and_then(|r| u32::try_from(r).ok())
            .expect("invalid game channel rank"),
          limit: NonZeroU32::new(1).expect("1 != 0"),
          count: u32::try_from(r.channel_count).expect("invalid game channel count"),
          is_count_exact: false,
          items: vec![ShortGameChannel {
            key: r.game_channel_key,
            is_enabled: r.is_enabled,
            is_pinned: r.is_pinned,
            publication_date: r.publication_date,
            sort_update_date: r.sort_update_date,
            default_permission: r.default_permission,
            build: {
              let mut display_name: Option<GameDisplayName> = None;
              let mut description: Option<GameDescription> = None;
              let mut icon: Option<BlobIdRef> = None;
              let mut i18n = BTreeMap::<LocaleId, ShortGameBuildI18n<_>>::new();
              for (l, v) in r.display_name {
                if l == r.main_locale {
                  display_name = Some(v)
                } else {
                  i18n.entry(l).or_default().display_name = Some(v);
                }
              }
              for (l, v) in r.description {
                if l == r.main_locale {
                  description = Some(v)
                } else {
                  i18n.entry(l).or_default().description = Some(v);
                }
              }
              for (l, v) in r.icon.into_iter().flatten() {
                if l == r.main_locale {
                  icon = Some(BlobIdRef::new(v))
                } else {
                  i18n.entry(l).or_default().icon = Some(BlobIdRef::new(v));
                }
              }
              ShortGameBuild {
                version: SimpleSemVer {
                  major: r.version_major.into(),
                  minor: r.version_minor.into(),
                  patch: r.version_patch.into(),
                },
                git_commit_ref: None,
                main_locale: r.main_locale,
                display_name: display_name.expect("display_name should exist for the main locale"),
                description: description.expect("description should exist for the main locale"),
                icon,
                i18n,
              }
            },
          }],
        },
      },
      r.is_visible_now,
    ))
  }

  Ok(Listing {
    offset,
    limit,
    count,
    is_count_exact: false,
    items,
  })
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
enum InnerGetGameError {
  #[error("game not found")]
  GameNotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[allow(clippy::too_many_arguments, clippy::type_complexity)]
async fn get_game(
  tx: &mut Transaction<'_, Postgres>,
  time: Instant,
  now: Instant,
  game: GameIdRef,
  channel: Option<GameChannelKey>,
  actor: Option<Option<UserIdRef>>,
  require_enabled: bool,
  is_tester: bool,
) -> Result<(Game<RefsOnly>, bool), InnerGetGameError> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    game_id: GameId,
    game_key: Option<GameKey>,
    created_at: Instant,
    owner: UserId,
    game_channel_key: GameChannelKey,
    is_enabled: bool,
    default_permission: GameChannelPermission,
    is_pinned: bool,
    publication_date: Option<Instant>,
    sort_update_date: Instant,
    version_major: PgU32,
    version_minor: PgU32,
    version_patch: PgU32,
    git_commit_ref: Option<GitCommitRefSha1>,
    channel_rank: i64,
    channel_count: i64,
    is_visible_now: bool,
    version_created_at: Instant,
    main_locale: LocaleId,
    display_name: Vec<(LocaleId, GameDisplayName)>,
    description: Vec<(LocaleId, GameDescription)>,
    icon: Option<Vec<(LocaleId, BlobId)>>,
    loader_version_major: PgU32,
    loader_version_minor: PgU32,
    loader_version_patch: PgU32,
    custom_engine: Option<BlobId>,
    patcher: Option<(Uuid, String, PgU32, PgU32, PgU32, Option<JsonValue>)>,
    debug: Option<BlobId>,
    content: Option<BlobId>,
    musics: Vec<(
      Option<Vec<(LocaleId, BlobId)>>,
      Option<Vec<(LocaleId, GameResourceDisplayName)>>,
    )>,
    content_i18n: Option<Vec<(LocaleId, BlobId)>>,
    modes: Vec<(
      GameModeKey,
      Vec<(LocaleId, GameModeDisplayName)>,
      bool,
      Vec<(GameOptionKey, Vec<(LocaleId, GameOptionDisplayName)>, bool, bool, bool)>,
    )>,
    families: FamilyList,
    category: GameCategory,
  }

  let (view_all, user_id) = match actor {
    None => (true, None),
    Some(user) => (false, user),
  };

  // language=PostgreSQL
  let row: Option<Row> = sqlx::query_as::<_, Row>(
    r#"
      WITH channels AS (
        SELECT game_id, g.created_at, owner,
          game_channel_key, is_enabled, default_permission, is_pinned, publication_date, sort_update_date,
          version_major, version_minor, version_patch,
          ROW_NUMBER() OVER w AS channel_rank,
          COUNT(*) OVER w AS channel_count,
          (
            $6::BOOLEAN
            OR $2::INSTANT = $1::INSTANT
            OR g.owner = $7::USER_ID
            OR EXISTS (
              SELECT 1
              FROM game_channel_history AS now_gch
              WHERE
                now_gch.game_id = gch.game_id
                AND now_gch.game_channel_key = gch.game_channel_key
                AND now_gch.tt_period @> $2::INSTANT
                AND now_gch.vt_period @> $2::INSTANT
                AND now_gch.default_permission IN ('View', 'Play', 'Debug', 'Manage')
          )) AS is_visible_now
        FROM game_channel_history AS gch
          INNER JOIN game_channel AS gc USING (game_id, game_channel_key)
          INNER JOIN game AS g USING (game_id)
        WHERE g.game_id = $3::GAME_ID
          AND g.created_at <= $1::INSTANT
          AND gc.created_at <= $1::INSTANT
          AND tt_period @> $1::INSTANT
          AND vt_period @> $1::INSTANT
          AND gch.is_enabled >= $5::BOOLEAN
          AND (
            $6::BOOLEAN
            OR g.owner = $7::USER_ID
            OR gch.default_permission IN ('View', 'Play', 'Debug', 'Manage')
          )
        WINDOW w AS (PARTITION BY (game_id) ORDER BY (NOT is_enabled, gch.rank, gc.created_at) ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING)
        ORDER BY game_id, NOT is_enabled, gch.rank, gc.created_at
      ),
      active_channel AS (
        SELECT game_id, owner, game_channel_key, is_enabled, default_permission, is_pinned, publication_date, sort_update_date, version_major, version_minor, version_patch, created_at, channel_rank, channel_count, is_visible_now
        FROM channels
        ORDER BY (game_channel_key = $4::GAME_CHANNEL_KEY) DESC, is_visible_now DESC, channel_rank
        LIMIT 1
      )
      SELECT
        game_id, game_key, g.created_at, owner,
        game_channel_key, is_enabled, default_permission, is_pinned, publication_date, sort_update_date,
        version_major, version_minor, version_patch, git_commit_ref,
        game_version.created_at AS version_created_at,
        channel_rank, channel_count, is_visible_now,
        gb.main_locale,
        -- Cast away the item type (SQLx workaround)
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(display_name.items) AS GAME_DISPLAY_NAME_I18N_ITEM) AS i) AS display_name,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(description.items) AS GAME_DESCRIPTION_I18N_ITEM) AS i) AS description,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = icon.blob_i18n_id) AS icon,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = content_i18n.blob_i18n_id) AS content_i18n,
        gb.loader_version_major, gb.loader_version_minor, gb.loader_version_patch,
        gb.custom_engine, gb.debug, gb.content,
        (
          CASE gb.patcher IS NOT NULL
          WHEN TRUE THEN
            ROW(
              patcher.blob_id::UUID,
              patcher.patcher_framework_name::VARCHAR,
              patcher.patcher_framework_version_major::INT8,
              patcher.patcher_framework_version_minor::INT8,
              patcher.patcher_framework_version_patch::INT8,
              patcher.meta::JSONB
            )
        END) AS patcher,
        COALESCE((
          SELECT ARRAY_AGG(ROW(
            (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = blob.blob_i18n_id),
            (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(grdn.items) AS GAME_RESOURCE_DISPLAY_NAME_I18N_ITEM) AS i)
          ) ORDER BY rank)
          FROM game_resource_list_item AS gri
            INNER JOIN game_resource AS gr USING (game_resource_id)
            INNER JOIN blob_i18n AS blob ON (blob.main_locale = gr.main_locale AND blob.blob_i18n_id = gr.blob_i18n_id)
            LEFT OUTER JOIN game_resource_display_name_i18n AS grdn ON (grdn.game_resource_display_name_id = gr.display_name)
          WHERE gri.game_resource_list_id = gb.music_list
        ), '{}') AS musics,
        gb.families, gb.category,
        COALESCE((
          SELECT ARRAY_AGG(ROW(
            gbm.game_mode_key::VARCHAR,
            (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(gmdn.items) AS GAME_MODE_DISPLAY_NAME_I18N_ITEM) AS i),
            gm.is_visible,
            COALESCE((
            SELECT ARRAY_AGG(ROW(
                gmo.game_option_key::VARCHAR,
                (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(godn.items) AS GAME_OPTION_DISPLAY_NAME_I18N_ITEM) AS i),
                go.is_visible,
                go.is_enabled,
                go.default_value
              ) ORDER BY gmo.rank)
              FROM game_option AS go
                INNER JOIN game_mode_option AS gmo USING (game_option_id)
                INNER JOIN game_option_display_name_i18n AS godn ON (godn.main_locale = go.main_locale AND godn.game_option_display_name_id = go.display_name)
              WHERE gmo.game_mode_id = gm.game_mode_id
            ), '{}')
          ) ORDER BY gbm.rank)
          FROM game_mode AS gm
            INNER JOIN game_build_mode AS gbm USING (game_mode_id)
            INNER JOIN game_mode_display_name_i18n AS gmdn ON (gmdn.main_locale = gm.main_locale AND gmdn.game_mode_display_name_id = gm.display_name)
          WHERE gbm.game_build_id = gb.game_build_id
        ), '{}') AS modes
      FROM active_channel AS g
        LEFT OUTER JOIN game_key_history AS gkh USING(game_id)
        INNER JOIN game_version USING (game_id, version_major, version_minor, version_patch)
        INNER JOIN game_build AS gb USING (game_build_id)
        INNER JOIN game_display_name_i18n AS display_name USING (game_display_name_id, main_locale)
        INNER JOIN game_description_i18n AS description USING (game_description_id, main_locale)
        LEFT OUTER JOIN blob_i18n AS icon ON (icon.main_locale = gb.main_locale AND icon.blob_i18n_id = gb.icon)
        LEFT OUTER JOIN blob_i18n AS content_i18n ON (content_i18n.main_locale = gb.main_locale AND content_i18n.blob_i18n_id = gb.content_i18n)
        LEFT OUTER JOIN game_patcher AS patcher ON (patcher.game_patcher_id = gb.patcher)
      WHERE gkh.period IS NULL OR gkh.period @> $1::INSTANT;
  "#,
  )
  .bind(time)
  .bind(now)
  .bind(game.id)
  .bind(channel)
  .bind(require_enabled)
  .bind(view_all || is_tester)
  .bind(user_id.map(|u| u.id))
  .fetch_optional(&mut **tx)
  .await.map_err(WeakError::wrap)?;

  let r = row.ok_or(InnerGetGameError::GameNotFound)?;

  let game = Game {
    id: r.game_id,
    created_at: r.created_at,
    key: r.game_key,
    owner: UserIdRef::new(r.owner),
    channels: GameChannelListing {
      offset: r
        .channel_rank
        .checked_sub(1)
        .and_then(|r| u32::try_from(r).ok())
        .expect("invalid game channel rank"),
      limit: 1,
      count: u32::try_from(r.channel_count).expect("invalid game channel count"),
      is_count_exact: actor.is_none() || is_tester || view_all,
      active: ActiveGameChannel {
        key: r.game_channel_key.clone(),
        is_enabled: r.is_enabled,
        default_permission: r.default_permission,
        is_pinned: r.is_pinned,
        publication_date: r.publication_date,
        sort_update_date: r.sort_update_date,
        build: {
          let mut display_name: Option<GameDisplayName> = None;
          let mut description: Option<GameDescription> = None;
          let mut icon: Option<BlobIdRef> = None;
          let mut content_i18n: Option<BlobIdRef> = None;
          let mut i18n = BTreeMap::<LocaleId, GameBuildI18n<_>>::new();
          let mut modes = IndexMap::<GameModeKey, GameModeSpec>::new();
          for (l, v) in r.display_name.clone() {
            if l == r.main_locale {
              display_name = Some(v)
            } else {
              i18n.entry(l).or_default().display_name = Some(v);
            }
          }
          for (l, v) in r.description.clone() {
            if l == r.main_locale {
              description = Some(v)
            } else {
              i18n.entry(l).or_default().description = Some(v);
            }
          }
          for (l, v) in r.icon.clone().into_iter().flatten() {
            if l == r.main_locale {
              icon = Some(BlobIdRef::new(v))
            } else {
              i18n.entry(l).or_default().icon = Some(BlobIdRef::new(v));
            }
          }
          for (l, v) in r.content_i18n.clone().into_iter().flatten() {
            if l == r.main_locale {
              content_i18n = Some(BlobIdRef::new(v))
            } else {
              i18n.entry(l).or_default().content_i18n = Some(BlobIdRef::new(v));
            }
          }
          for (mode_key, mode_display_name, is_visible, raw_options) in r.modes.clone().into_iter() {
            let mut display_name: Option<GameModeDisplayName> = None;
            let mut options = IndexMap::<GameOptionKey, GameOptionSpec>::new();
            for (l, v) in mode_display_name {
              if l == r.main_locale {
                display_name = Some(v)
              } else {
                i18n
                  .entry(l)
                  .or_default()
                  .modes
                  .entry(mode_key.clone())
                  .or_default()
                  .display_name = Some(v);
              }
            }
            for (option_key, option_display_name, is_visible, is_enabled, default_value) in raw_options {
              let mut display_name: Option<GameOptionDisplayName> = None;
              for (l, v) in option_display_name {
                if l == r.main_locale {
                  display_name = Some(v)
                } else {
                  i18n
                    .entry(l)
                    .or_default()
                    .modes
                    .entry(mode_key.clone())
                    .or_default()
                    .options
                    .entry(option_key.clone())
                    .or_default()
                    .display_name = Some(v);
                }
              }
              options.insert(
                option_key,
                GameOptionSpec {
                  display_name: display_name.expect("option display_name should exist for the main locale"),
                  is_visible,
                  is_enabled,
                  default_value,
                },
              );
            }
            modes.insert(
              mode_key,
              GameModeSpec {
                display_name: display_name.expect("mode display_name should exist for the main locale"),
                is_visible,
                options,
              },
            );
          }

          GameBuild {
            version: SimpleSemVer {
              major: r.version_major.into(),
              minor: r.version_minor.into(),
              patch: r.version_patch.into(),
            },
            created_at: r.version_created_at,
            git_commit_ref: r.git_commit_ref,
            main_locale: r.main_locale,
            display_name: display_name.expect("display_name should exist for the main locale"),
            description: description.expect("description should exist for the main locale"),
            icon,
            loader: SimpleSemVer {
              major: r.loader_version_major.into(),
              minor: r.loader_version_minor.into(),
              patch: r.loader_version_patch.into(),
            },
            engine: match r.custom_engine {
              None => GameEngine::V96,
              Some(id) => GameEngine::Custom(CustomGameEngine {
                blob: BlobIdRef::from(id),
              }),
            },
            patcher: r.patcher.map(|p| GamePatcher {
              blob: BlobIdRef::from(BlobId::from_uuid(p.0)),
              framework: PatcherFramework {
                name: p.1.parse().expect("valid patcher framework name"),
                version: SimpleSemVer {
                  major: p.2.into(),
                  minor: p.3.into(),
                  patch: p.4.into(),
                },
              },
              meta: p.5,
            }),
            debug: r.debug.map(BlobIdRef::from),
            content: r.content.map(BlobIdRef::from),
            musics: r
              .musics
              .into_iter()
              .map(|(blob_i18n, name_i18n)| {
                let mut blob: Option<BlobIdRef> = None;
                let mut display_name: Option<GameResourceDisplayName> = None;
                for (l, v) in blob_i18n.into_iter().flatten() {
                  if l == r.main_locale {
                    blob = Some(BlobIdRef::new(v))
                  } else {
                    // TODO: Support i18n for game resource blobs
                  }
                }
                for (l, v) in name_i18n.into_iter().flatten() {
                  if l == r.main_locale {
                    display_name = Some(v)
                  } else {
                    // TODO: Support i18n for game resource names
                  }
                }
                GameResource {
                  blob: blob.expect("game resource main blob exists"),
                  display_name,
                }
              })
              .collect(),
            content_i18n,
            modes,
            families: r.families,
            category: r.category,
            i18n,
          }
        },
      },
      items: vec![Some(ShortGameChannel {
        key: r.game_channel_key,
        is_enabled: r.is_enabled,
        is_pinned: r.is_pinned,
        publication_date: r.publication_date,
        sort_update_date: r.sort_update_date,
        default_permission: r.default_permission,
        build: {
          let mut display_name: Option<GameDisplayName> = None;
          let mut description: Option<GameDescription> = None;
          let mut icon: Option<BlobIdRef> = None;
          let mut i18n = BTreeMap::<LocaleId, ShortGameBuildI18n<_>>::new();
          for (l, v) in r.display_name {
            if l == r.main_locale {
              display_name = Some(v)
            } else {
              i18n.entry(l).or_default().display_name = Some(v);
            }
          }
          for (l, v) in r.description {
            if l == r.main_locale {
              description = Some(v)
            } else {
              i18n.entry(l).or_default().description = Some(v);
            }
          }
          for (l, v) in r.icon.into_iter().flatten() {
            if l == r.main_locale {
              icon = Some(BlobIdRef::new(v))
            } else {
              i18n.entry(l).or_default().icon = Some(BlobIdRef::new(v));
            }
          }
          ShortGameBuild {
            version: SimpleSemVer {
              major: r.version_major.into(),
              minor: r.version_minor.into(),
              patch: r.version_patch.into(),
            },
            git_commit_ref: None,
            main_locale: r.main_locale,
            display_name: display_name.expect("display_name should exist for the main locale"),
            description: description.expect("description should exist for the main locale"),
            icon,
            i18n,
          }
        },
      })],
    },
  };

  Ok((game, r.is_visible_now))
}

#[allow(clippy::too_many_arguments, clippy::type_complexity)]
async fn get_short_game_build(
  tx: &mut Transaction<'_, Postgres>,
  now: Instant,
  game: GameIdRef,
  build_version: SimpleSemVer,
) -> Result<(ShortGameBuild<RefsOnly>, bool), WeakError> {
  #[derive(Debug, sqlx::FromRow)]
  struct Row {
    version_major: PgU32,
    version_minor: PgU32,
    version_patch: PgU32,
    git_commit_ref: Option<GitCommitRefSha1>,
    // version_created_at: Instant,
    main_locale: LocaleId,
    display_name: Vec<(LocaleId, GameDisplayName)>,
    description: Vec<(LocaleId, GameDescription)>,
    icon: Option<Vec<(LocaleId, BlobId)>>,
  }

  // language=PostgreSQL
  let row: Option<Row> = sqlx::query_as::<_, Row>(
    r#"
      WITH versions AS (
        SELECT version_major, version_minor, version_patch, created_at, git_commit_ref, game_build_id
        FROM game_version AS gv
        WHERE gv.game_id = $2::GAME_ID
          AND gv.created_at <= $1::INSTANT
          AND gv.version_major = $3::U32
          AND gv.version_minor = $4::U32
          AND gv.version_patch = $5::U32
      )
      SELECT
        version_major, version_minor, version_patch, git_commit_ref,
        gv.created_at AS version_created_at,
        gb.main_locale,
        -- Cast away the item type (SQLx workaround)
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(display_name.items) AS GAME_DISPLAY_NAME_I18N_ITEM) AS i) AS display_name,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(description.items) AS GAME_DESCRIPTION_I18N_ITEM) AS i) AS description,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = icon.blob_i18n_id) AS icon,
        (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = content_i18n.blob_i18n_id) AS content_i18n,
        gb.loader_version_major, gb.loader_version_minor, gb.loader_version_patch,
        gb.custom_engine, gb.debug, gb.content,
        (
          CASE gb.patcher IS NOT NULL
          WHEN TRUE THEN
            ROW(
              patcher.blob_id::UUID,
              patcher.patcher_framework_name::VARCHAR,
              patcher.patcher_framework_version_major::INT8,
              patcher.patcher_framework_version_minor::INT8,
              patcher.patcher_framework_version_patch::INT8,
              patcher.meta::JSONB
            )
        END) AS patcher,
        COALESCE((
          SELECT ARRAY_AGG(ROW(
            (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.blob_id::UUID)) FROM blob_i18n_item AS i WHERE i.blob_i18n_id = blob.blob_i18n_id),
            (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(grdn.items) AS GAME_RESOURCE_DISPLAY_NAME_I18N_ITEM) AS i)
          ) ORDER BY rank)
          FROM game_resource_list_item AS gri
            INNER JOIN game_resource AS gr USING (game_resource_id)
            INNER JOIN blob_i18n AS blob ON (blob.main_locale = gr.main_locale AND blob.blob_i18n_id = gr.blob_i18n_id)
            LEFT OUTER JOIN game_resource_display_name_i18n AS grdn ON (grdn.game_resource_display_name_id = gr.display_name)
          WHERE gri.game_resource_list_id = gb.music_list
        ), '{}') AS musics,
        gb.families, gb.category,
        COALESCE((
          SELECT ARRAY_AGG(ROW(
            gbm.game_mode_key::VARCHAR,
            (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(gmdn.items) AS GAME_MODE_DISPLAY_NAME_I18N_ITEM) AS i),
            gm.is_visible,
            COALESCE((
            SELECT ARRAY_AGG(ROW(
                gmo.game_option_key::VARCHAR,
                (SELECT ARRAY_AGG(ROW(i.locale::VARCHAR, i.value::VARCHAR)) FROM CAST(UNNEST(godn.items) AS GAME_OPTION_DISPLAY_NAME_I18N_ITEM) AS i),
                go.is_visible,
                go.is_enabled,
                go.default_value
              ) ORDER BY gmo.rank)
              FROM game_option AS go
                INNER JOIN game_mode_option AS gmo USING (game_option_id)
                INNER JOIN game_option_display_name_i18n AS godn ON (godn.main_locale = go.main_locale AND godn.game_option_display_name_id = go.display_name)
              WHERE gmo.game_mode_id = gm.game_mode_id
            ), '{}')
          ) ORDER BY gbm.rank)
          FROM game_mode AS gm
            INNER JOIN game_build_mode AS gbm USING (game_mode_id)
            INNER JOIN game_mode_display_name_i18n AS gmdn ON (gmdn.main_locale = gm.main_locale AND gmdn.game_mode_display_name_id = gm.display_name)
          WHERE gbm.game_build_id = gb.game_build_id
        ), '{}') AS modes
      FROM versions AS gv
        INNER JOIN game_build AS gb USING (game_build_id)
        INNER JOIN game_display_name_i18n AS display_name USING (game_display_name_id, main_locale)
        INNER JOIN game_description_i18n AS description USING (game_description_id, main_locale)
        LEFT OUTER JOIN blob_i18n AS icon ON (icon.main_locale = gb.main_locale AND icon.blob_i18n_id = gb.icon)
        LEFT OUTER JOIN blob_i18n AS content_i18n ON (content_i18n.main_locale = gb.main_locale AND content_i18n.blob_i18n_id = gb.content_i18n)
        LEFT OUTER JOIN game_patcher AS patcher ON (patcher.game_patcher_id = gb.patcher);
  "#,
  )
    .bind(now)
    .bind(game.id)
    .bind(PgU32::from(build_version.major))
    .bind(PgU32::from(build_version.minor))
    .bind(PgU32::from(build_version.patch))
    .fetch_optional(&mut **tx)
    .await.map_err(WeakError::wrap)?;

  let r = row.ok_or_else(|| WeakError::new("game or build not found"))?;

  let build = {
    let mut display_name: Option<GameDisplayName> = None;
    let mut description: Option<GameDescription> = None;
    let mut icon: Option<BlobIdRef> = None;
    let mut i18n = BTreeMap::<LocaleId, ShortGameBuildI18n<_>>::new();
    for (l, v) in r.display_name.clone() {
      if l == r.main_locale {
        display_name = Some(v)
      } else {
        i18n.entry(l).or_default().display_name = Some(v);
      }
    }
    for (l, v) in r.description.clone() {
      if l == r.main_locale {
        description = Some(v)
      } else {
        i18n.entry(l).or_default().description = Some(v);
      }
    }
    for (l, v) in r.icon.clone().into_iter().flatten() {
      if l == r.main_locale {
        icon = Some(BlobIdRef::new(v))
      } else {
        i18n.entry(l).or_default().icon = Some(BlobIdRef::new(v));
      }
    }

    ShortGameBuild {
      version: SimpleSemVer {
        major: r.version_major.into(),
        minor: r.version_minor.into(),
        patch: r.version_patch.into(),
      },
      // created_at: r.version_created_at,
      git_commit_ref: r.git_commit_ref,
      main_locale: r.main_locale,
      display_name: display_name.expect("display_name should exist for the main locale"),
      description: description.expect("description should exist for the main locale"),
      icon,
      i18n,
    }
  };

  Ok((build, true))
}

#[async_trait]
impl<TyClock, TyDatabase, TyUuidGenerator> GameStore for PgGameStore<TyClock, TyDatabase, TyUuidGenerator>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
  TyUuidGenerator: UuidGeneratorRef,
{
  async fn create_game(&self, options: &CreateStoreGame) -> Result<Game<RefsOnly>, CreateStoreGameError> {
    let now = self.clock.clock().now();
    let game_id = GameId::from_uuid(self.uuid_generator.uuid_generator().next());
    let mut tx = self.database.begin().await.map_err(CreateStoreGameError::other)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      game_id: GameId,
    }

    // language=PostgreSQL
    let row: Row = sqlx::query_as::<_, Row>(
      r"
    INSERT INTO game(game_id, created_at, owner, main_channel_key)
    VALUES ($1::GAME_ID, $2::INSTANT, $3::USER_ID, $4::GAME_CHANNEL_KEY)
    RETURNING game_id;
  ",
    )
    .bind(game_id)
    .bind(now)
    .bind(options.owner.id)
    .bind(&options.channels.first().key)
    .fetch_one(&mut *tx)
    .await
    .unwrap();

    let game_id = row.game_id;

    let game_build = upsert_game_revision(
      &mut tx,
      now,
      self.uuid_generator.uuid_generator(),
      game_id,
      &options.build,
    )
    .await
    .map_err(|e| CreateStoreGameError::Other(WeakError::wrap(e)))?;

    let short_game_build = ShortGameBuild {
      version: game_build.version,
      git_commit_ref: game_build.git_commit_ref,
      main_locale: game_build.main_locale,
      display_name: game_build.display_name.clone(),
      description: game_build.description.clone(),
      icon: game_build.icon,
      i18n: game_build
        .i18n
        .iter()
        .map(|(l, v)| {
          (
            *l,
            ShortGameBuildI18n {
              display_name: v.display_name.clone(),
              description: v.description.clone(),
              icon: v.icon,
            },
          )
        })
        .collect(),
    };

    let mut channels: Vec<ShortGameChannel<_>> = Vec::new();
    for channel in options.channels.as_slice() {
      let store_channel = create_game_channel(
        &mut tx,
        now,
        GameIdRef::new(game_id),
        channel,
        options.owner.id,
        short_game_build.clone(),
      )
      .await
      .map_err(|e| CreateStoreGameError::Other(WeakError::wrap(e)))?;
      channels.push(store_channel);
    }

    tx.commit().await.map_err(CreateStoreGameError::other)?;

    let short_channel: &ShortGameChannel<_> = channels.first().expect("at least one channel exists");

    let active_channel = ActiveGameChannel {
      key: short_channel.key.clone(),
      is_enabled: short_channel.is_enabled,
      is_pinned: short_channel.is_pinned,
      publication_date: short_channel.publication_date,
      default_permission: short_channel.default_permission,
      sort_update_date: short_channel.sort_update_date,
      build: game_build,
    };

    Ok(Game {
      id: game_id,
      created_at: now,
      key: None,
      owner: options.owner,
      channels: GameChannelListing {
        offset: 0,
        limit: 1,
        count: 1,
        is_count_exact: true,
        active: active_channel,
        items: channels.into_iter().map(Some).collect(),
      },
    })
  }

  async fn create_version(&self, options: &CreateBuild) -> Result<GameBuild<RefsOnly>, CreateBuildError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(CreateBuildError::other)?;

    let game = resolve_game_ref(&mut tx, now, &options.game)
      .await
      .map_err(CreateBuildError::other)?;

    let game_revision = upsert_game_revision(
      &mut tx,
      now,
      self.uuid_generator.uuid_generator(),
      game.id,
      &options.build,
    )
    .await
    .map_err(|e| CreateBuildError::Other(WeakError::wrap(e)))?;

    tx.commit().await.map_err(CreateBuildError::other)?;

    Ok(game_revision)
  }

  async fn create_game_channel(
    &self,
    options: &CreateGameChannel,
  ) -> Result<ShortGameChannel<RefsOnly>, CreateGameChannelError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(CreateGameChannelError::other)?;

    let game = resolve_game_ref(&mut tx, now, &options.game)
      .await
      .map_err(CreateGameChannelError::other)?;

    let (build, _) = get_short_game_build(&mut tx, now, game, options.channel.version)
      .await
      .map_err(|e| CreateGameChannelError::Other(WeakError::wrap(e)))?;

    let channel = create_game_channel(&mut tx, now, game, &options.channel, options.actor.id, build)
      .await
      .map_err(|e| CreateGameChannelError::Other(WeakError::wrap(e)))?;

    tx.commit().await.map_err(CreateGameChannelError::other)?;

    Ok(channel)
  }

  async fn update_game_channel(
    &self,
    options: &UpdateStoreGameChannel,
  ) -> Result<(ActiveGameChannel<RefsOnly>, Instant), UpdateGameChannelError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(UpdateGameChannelError::other)?;

    let game_id = resolve_game_ref(&mut tx, now, &options.game)
      .await
      .map_err(|e| match e {
        ResolveGameRefError::GameNotFound => UpdateGameChannelError::GameNotFound(options.game.clone()),
        e => UpdateGameChannelError::other(e),
      })?;

    for patch in &options.patches {
      update_game_channel(&mut tx, now, game_id.id, &options.channel_key, patch, options.actor.id)
        .await
        .map_err(|e| UpdateGameChannelError::Other(WeakError::wrap(e)))?;
    }

    let bypass_view_check = options.if_owner.is_none();

    let (game, _is_visible_now) = get_game(
      &mut tx,
      now,
      now,
      game_id,
      Some(options.channel_key.clone()),
      Some(options.if_owner),
      false,
      bypass_view_check,
    )
    .await
    .map_err(|e| match e {
      InnerGetGameError::GameNotFound => unreachable!("game existence already checked earlier"),
      InnerGetGameError::Other(e) => UpdateGameChannelError::Other(e),
    })?;

    if let Some(if_owner) = options.if_owner {
      if game.owner != if_owner {
        return Err(UpdateGameChannelError::NotOwner(if_owner));
      }
    }

    tx.commit().await.map_err(UpdateGameChannelError::other)?;

    Ok((game.channels.active, now))
  }

  async fn get_short_games(
    &self,
    query: &GetStoreShortGames,
  ) -> Result<Listing<Option<ShortGame<RefsOnly>>>, GetStoreShortGamesError> {
    let now = query.now;
    let mut tx = self.database.begin().await.map_err(GetStoreShortGamesError::other)?;

    let games = get_games(
      &mut tx,
      query.time.unwrap_or(now),
      now,
      query.offset,
      query.limit,
      Some(query.actor),
      true,
      query.favorite,
      query.is_tester,
    )
    .await
    .map_err(|e| GetStoreShortGamesError::Other(WeakError::wrap(e)))?;

    tx.commit().await.map_err(GetStoreShortGamesError::other)?;

    let result = Listing {
      offset: games.offset,
      limit: games.limit,
      count: games.count,
      is_count_exact: games.is_count_exact,
      items: games
        .items
        .into_iter()
        .map(|(game, is_visible)| if is_visible { Some(game) } else { None })
        .collect(),
    };

    Ok(result)
  }

  async fn get_game(&self, query: &GetStoreGame) -> Result<Game<RefsOnly>, GetStoreGameError> {
    let now = query.now;
    let time = query.time.unwrap_or(now);
    let mut tx = self.database.begin().await.map_err(GetStoreGameError::other)?;

    let game = resolve_game_ref(&mut tx, time, &query.game)
      .await
      .map_err(GetStoreGameError::other)?;

    let (game, is_visible_now) = get_game(
      &mut tx,
      time,
      now,
      game,
      query.channel.clone(),
      Some(query.actor),
      !query.is_tester,
      query.is_tester,
    )
    .await
    .map_err(|e| match e {
      InnerGetGameError::GameNotFound => GetStoreGameError::GameNotFound,
      InnerGetGameError::Other(e) => GetStoreGameError::Other(e),
    })?;

    tx.commit().await.map_err(GetStoreGameError::other)?;

    if !is_visible_now {
      return Err(GetStoreGameError::PermissionRevoked);
    }

    Ok(game)
  }

  async fn set_game_favorite(&self, options: &StoreSetGameFavorite) -> Result<bool, StoreSetGameFavoriteError> {
    let now = self.clock.clock().now();
    let mut tx = self.database.begin().await.map_err(StoreSetGameFavoriteError::other)?;

    let game = resolve_game_ref(&mut tx, now, &options.game)
      .await
      .map_err(StoreSetGameFavoriteError::other)?;

    if options.favorite {
      // language=PostgreSQL
      sqlx::query(
        r"
            INSERT INTO user_favorite_game(period, user_id, game_id)
            VALUES (PERIOD($1::INSTANT, NULL), $2::USER_ID, $3::GAME_ID);
          ",
      )
      .bind(now)
      .bind(options.user.id)
      .bind(game.id)
      .execute(&mut *tx)
      .await
      .map_err(StoreSetGameFavoriteError::other)?;
    } else {
      // language=PostgreSQL
      sqlx::query(
        r"
            UPDATE user_favorite_game
            SET period = PERIOD(lower(period), $1::INSTANT)
            WHERE upper(period) IS NULL AND user_id = $2::USER_ID AND game_id = $3::GAME_ID;
          ",
      )
      .bind(now)
      .bind(options.user.id)
      .bind(game.id)
      .execute(&mut *tx)
      .await
      .map_err(StoreSetGameFavoriteError::other)?;
    }

    tx.commit().await.map_err(StoreSetGameFavoriteError::other)?;

    Ok(options.favorite)
  }

  async fn get_game_quests(&self, game: GameId, version: SimpleSemVer) -> Result<GameQuests<'_>, WeakError> {
    let mut tx = self.database.begin().await.map_err(WeakError::wrap)?;

    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      created_at: Instant,
      families: FamilyList,
    }

    // language=PostgreSQL
    let Row { created_at, families } = sqlx::query_as::<_, Row>(
      r"
      SELECT created_at, families
      FROM game_version INNER JOIN game_build USING (game_build_id)
      WHERE game_id = $1::GAME_ID
      AND version_major = $2::U32
      AND version_minor = $3::U32
      AND version_patch = $4::U32;
    ",
    )
    .bind(game)
    .bind(PgU32::from(version.major))
    .bind(PgU32::from(version.minor))
    .bind(PgU32::from(version.patch))
    .fetch_one(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    #[derive(Debug, sqlx::FromRow)]
    struct KeyRow {
      game_key: GameKey,
    }

    // NOTE: this assumes that a GameKey change is always paired with a new game version.
    // This is suboptimal, but we shouldn't need to look at the GameKey at all once we stop
    // hardcoding quests.
    // language=PostgreSQL
    let row: Option<KeyRow> = sqlx::query_as::<_, KeyRow>(
      r"SELECT game_key FROM game_key_history WHERE period @> $1::INSTANT AND game_id = $2::GAME_ID;",
    )
    .bind(created_at)
    .bind(game)
    .fetch_optional(&mut *tx)
    .await
    .map_err(WeakError::wrap)?;

    Ok(GameQuests {
      default_families: families,
      quests: row
        .as_ref()
        .and_then(|r| self.quests.get(&r.game_key))
        .unwrap_or_default(),
      uses_archived_hfest_items: row.is_some_and(|r| r.game_key.as_str() == "hammerfest"),
    })
  }
}

#[cfg(test)]
mod test {
  use super::PgGameStore;
  use crate::test::TestApi;
  use crate::test_game_store;
  use eternalfest_blob_store::pg::PgBlobStore;
  use eternalfest_buffer_store::fs::FsBufferStore;
  use eternalfest_core::blob::BlobStore;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::game::GameStore;
  use eternalfest_core::user::UserStore;
  use eternalfest_core::uuid::{Uuid4Generator, UuidGenerator};
  use eternalfest_db_schema::force_create_latest;
  use eternalfest_user_store::pg::PgUserStore;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api(
  ) -> TestApi<Arc<dyn BlobStore>, Arc<VirtualClock>, Arc<dyn GameStore>, Arc<dyn UserStore>, Arc<dyn UuidGenerator>>
  {
    let config = eternalfest_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let uuid_generator = Arc::new(Uuid4Generator);
    let data_root = config.data_root();
    let buffer_store = Arc::new(FsBufferStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator), data_root).await);
    let blob_store: Arc<dyn BlobStore> = Arc::new(
      PgBlobStore::new(buffer_store, clock.clone(), database.clone(), uuid_generator.clone())
        .await
        .expect("failed to create `PgBlobStore`"),
    );
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(clock.clone(), database.clone()));
    let game_store: Arc<dyn GameStore> =
      Arc::new(PgGameStore::new(clock.clone(), database, uuid_generator.clone()).await);

    TestApi {
      blob_store,
      clock,
      game_store,
      user_store,
      uuid_generator,
    }
  }

  test_game_store!(
    #[serial]
    || make_test_api().await
  );
}
