use async_trait::async_trait;
use eternalfest_core::core::{Instant, Listing, SimpleSemVer};
use eternalfest_core::game::requests::CreateGameChannel;
use eternalfest_core::game::store::{
  CreateBuild, CreateBuildError, CreateGameChannelError, CreateStoreGame, CreateStoreGameError, GetStoreGame,
  GetStoreGameError, GetStoreShortGames, GetStoreShortGamesError, StoreSetGameFavorite, StoreSetGameFavoriteError,
  UpdateGameChannelError, UpdateStoreGameChannel,
};
use eternalfest_core::game::{
  ActiveGameChannel, Game, GameBuild, GameId, GameQuests, GameStore, RefsOnly, ShortGame, ShortGameChannel,
};
use eternalfest_core::types::WeakError;

pub struct MemGameStore {}

impl MemGameStore {
  #[expect(clippy::new_without_default, reason = "this is just a stub")]
  pub fn new() -> Self {
    Self {}
  }
}

#[async_trait]
impl GameStore for MemGameStore {
  async fn create_game(&self, _options: &CreateStoreGame) -> Result<Game<RefsOnly>, CreateStoreGameError> {
    todo!()
  }

  async fn create_version(&self, _options: &CreateBuild) -> Result<GameBuild<RefsOnly>, CreateBuildError> {
    todo!()
  }

  async fn create_game_channel(
    &self,
    _options: &CreateGameChannel,
  ) -> Result<ShortGameChannel<RefsOnly>, CreateGameChannelError> {
    todo!()
  }

  async fn update_game_channel(
    &self,
    _options: &UpdateStoreGameChannel,
  ) -> Result<(ActiveGameChannel<RefsOnly>, Instant), UpdateGameChannelError> {
    todo!()
  }

  async fn get_short_games(
    &self,
    _options: &GetStoreShortGames,
  ) -> Result<Listing<Option<ShortGame<RefsOnly>>>, GetStoreShortGamesError> {
    todo!()
  }

  async fn get_game(&self, _options: &GetStoreGame) -> Result<Game<RefsOnly>, GetStoreGameError> {
    todo!()
  }

  async fn set_game_favorite(&self, _options: &StoreSetGameFavorite) -> Result<bool, StoreSetGameFavoriteError> {
    todo!()
  }

  async fn get_game_quests(&self, _game: GameId, _version: SimpleSemVer) -> Result<GameQuests<'_>, WeakError> {
    todo!()
  }
}
