use eternalfest_core::blob::{BlobStore, BlobStoreRef, CreateBlobOptions};
use eternalfest_core::clock::{Clock, VirtualClock};
use eternalfest_core::core::{BoundedVec, Duration, Instant, Listing};
use eternalfest_core::game::requests::CreateGameChannel;
use eternalfest_core::game::store::{
  CreateBuild, CreateStoreGame, GetStoreGame, GetStoreShortGames, UpdateStoreGameChannel,
};
use eternalfest_core::game::{
  ActiveGameChannel, Game, GameBuild, GameBuildI18n, GameCategory, GameChannelListing, GameChannelPatch,
  GameChannelPermission, GameEngine, GameRef, GameResource, GameStore, GameStoreRef, InputGameBuild, InputGameChannel,
  InputPeriodLower, ShortGame, ShortGameBuild, ShortGameBuildI18n, ShortGameChannel,
};
use eternalfest_core::indexmap::IndexMap;
use eternalfest_core::user::{ShortUser, UserIdRef, UserStore, UserStoreRef};
use eternalfest_core::uuid::{UuidGenerator, UuidGeneratorRef};
use eternalfest_core::{LocaleId, SyncRef};
use std::num::NonZeroU32;

#[macro_export]
macro_rules! test_game_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_create_game);
    register_test!($(#[$meta])*, $api, test_update_game);
    register_test!($(#[$meta])*, $api, test_get_games);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>
where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
  TyGameStore: GameStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  pub(crate) blob_store: TyBlobStore,
  pub(crate) clock: TyClock,
  pub(crate) game_store: TyGameStore,
  pub(crate) user_store: TyUserStore,
  pub(crate) uuid_generator: TyUuidGenerator,
}

pub(crate) async fn test_create_game<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>(
  api: TestApi<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>,
) where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
  TyGameStore: GameStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .upsert_from_etwin(&ShortUser {
      id: api.uuid_generator.uuid_generator().next().into(),
      display_name: "Alice".parse().unwrap(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let icon = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "image/png".parse().unwrap(),
      data: include_bytes!("../../../test-resources/games/sous-la-colline/icon.png").to_vec(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let music = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "audio/mp3".parse().unwrap(),
      data: include_bytes!("../../../test-resources/games/sous-la-colline/music/rourou.mp3").to_vec(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .game_store
    .game_store()
    .create_game(&CreateStoreGame {
      owner: alice.id.into(),
      key: None,
      build: InputGameBuild {
        version: "1.0.0".parse().unwrap(),
        git_commit_ref: None,
        main_locale: LocaleId::FrFr,
        display_name: "Sous la colline".parse().unwrap(),
        description: "Aidez Igor dans ses aventures".parse().unwrap(),
        icon: Some(icon.as_ref()),
        loader: "4.1.0".parse().unwrap(),
        engine: GameEngine::V96,
        patcher: None,
        debug: None,
        content: None,
        content_i18n: None,
        musics: vec![GameResource {
          blob: music.as_ref(),
          display_name: Some("Rourou.mp3".parse().unwrap()),
        }],
        modes: IndexMap::new(),
        families: "10".parse().unwrap(),
        category: GameCategory::Small,
        i18n: [(
          LocaleId::EnUs,
          GameBuildI18n {
            display_name: Some("Under the hill".parse().unwrap()),
            description: None,
            icon: None,
            content_i18n: None,
            modes: Default::default(),
          },
        )]
        .into_iter()
        .collect(),
      },
      channels: BoundedVec::new(vec![InputGameChannel {
        key: "main".parse().unwrap(),
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        is_pinned: false,
        publication_date: None,
        sort_update_date: None,
        version: "1.0.0".parse().unwrap(),
        patches: vec![],
      }])
      .unwrap(),
    })
    .await
    .unwrap();

  let expected = Game {
    id: actual.id,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    key: None,
    owner: UserIdRef::from(alice.id),
    channels: GameChannelListing {
      offset: 0,
      limit: 1,
      count: 1,
      is_count_exact: true,
      active: ActiveGameChannel {
        key: "main".parse().unwrap(),
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        publication_date: None,
        sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
        is_pinned: false,
        build: GameBuild {
          version: "1.0.0".parse().unwrap(),
          created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
          git_commit_ref: None,
          main_locale: LocaleId::FrFr,
          display_name: "Sous la colline".parse().unwrap(),
          description: "Aidez Igor dans ses aventures".parse().unwrap(),
          icon: Some(icon.id.into()),
          loader: "4.1.0".parse().unwrap(),
          engine: GameEngine::V96,
          patcher: None,
          debug: None,
          content: None,
          content_i18n: None,
          musics: vec![GameResource {
            blob: music.as_ref(),
            display_name: Some("Rourou.mp3".parse().unwrap()),
          }],
          modes: Default::default(),
          families: "10".parse().unwrap(),
          category: GameCategory::Small,
          i18n: [(
            LocaleId::EnUs,
            GameBuildI18n {
              display_name: Some("Under the hill".parse().unwrap()),
              description: None,
              icon: None,
              content_i18n: None,
              modes: Default::default(),
            },
          )]
          .into_iter()
          .collect(),
        },
      },
      items: vec![Some(ShortGameChannel {
        key: "main".parse().unwrap(),
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        publication_date: None,
        sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
        is_pinned: false,
        build: ShortGameBuild {
          version: "1.0.0".parse().unwrap(),
          git_commit_ref: None,
          main_locale: LocaleId::FrFr,
          display_name: "Sous la colline".parse().unwrap(),
          description: "Aidez Igor dans ses aventures".parse().unwrap(),
          icon: Some(icon.id.into()),
          i18n: [(
            LocaleId::EnUs,
            ShortGameBuildI18n {
              display_name: Some("Under the hill".parse().unwrap()),
              description: None,
              icon: None,
            },
          )]
          .into_iter()
          .collect(),
        },
      })],
    },
  };

  assert_eq!(actual, expected);

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .game_store
    .game_store()
    .get_game(&GetStoreGame {
      actor: Some(alice.id.into()),
      is_tester: alice.is_tester,
      now: api.clock.now(),
      time: None,
      game: GameRef::Id(actual.id.into()),
      channel: None,
    })
    .await
    .unwrap();

  assert_eq!(actual, expected);
}

pub(crate) async fn test_update_game<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>(
  api: TestApi<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>,
) where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
  TyGameStore: GameStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .upsert_from_etwin(&ShortUser {
      id: api.uuid_generator.uuid_generator().next().into(),
      display_name: "Alice".parse().unwrap(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let old_game = api
    .game_store
    .game_store()
    .create_game(&CreateStoreGame {
      owner: alice.id.into(),
      key: None,
      build: InputGameBuild {
        version: "1.0.0".parse().unwrap(),
        git_commit_ref: None,
        main_locale: LocaleId::FrFr,
        display_name: "Sous la colline".parse().unwrap(),
        description: "Aidez Igor dans ses aventures".parse().unwrap(),
        icon: None,
        loader: "4.1.0".parse().unwrap(),
        engine: GameEngine::V96,
        patcher: None,
        debug: None,
        content: None,
        content_i18n: None,
        musics: vec![],
        modes: IndexMap::new(),
        families: "10".parse().unwrap(),
        category: GameCategory::Small,
        i18n: [(
          LocaleId::EnUs,
          GameBuildI18n {
            display_name: Some("Under the hill".parse().unwrap()),
            description: None,
            icon: None,
            content_i18n: None,
            modes: Default::default(),
          },
        )]
        .into_iter()
        .collect(),
      },
      channels: BoundedVec::new(vec![InputGameChannel {
        key: "main".parse().unwrap(),
        is_enabled: false,
        default_permission: GameChannelPermission::None,
        is_pinned: false,
        publication_date: None,
        sort_update_date: None,
        version: "1.0.0".parse().unwrap(),
        patches: vec![],
      }])
      .unwrap(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let new_build = api
    .game_store
    .game_store()
    .create_version(&CreateBuild {
      game: old_game.id.into(),
      if_owner: Some(alice.id.into()),
      build: InputGameBuild {
        version: "1.0.1".parse().unwrap(),
        git_commit_ref: None,
        main_locale: LocaleId::FrFr,
        display_name: "Sous la colline".parse().unwrap(),
        description: "Aidez Igor dans ses aventures. Ravira petits et grands."
          .parse()
          .unwrap(),
        icon: None,
        loader: "4.1.0".parse().unwrap(),
        engine: GameEngine::V96,
        patcher: None,
        debug: None,
        content: None,
        content_i18n: None,
        musics: vec![],
        modes: IndexMap::new(),
        families: "10".parse().unwrap(),
        category: GameCategory::Small,
        i18n: [(
          LocaleId::EnUs,
          GameBuildI18n {
            display_name: Some("Under the hill".parse().unwrap()),
            description: None,
            icon: None,
            content_i18n: None,
            modes: Default::default(),
          },
        )]
        .into_iter()
        .collect(),
      },
    })
    .await
    .unwrap();

  assert_eq!(new_build.version.patch, 1);

  api.clock.advance_by(Duration::from_seconds(1));

  let (actual, actual_time) = api
    .game_store
    .game_store()
    .update_game_channel(&UpdateStoreGameChannel {
      actor: alice.id.into(),
      if_owner: Some(alice.id.into()),
      game: old_game.id.into(),
      channel_key: "main".parse().unwrap(),
      patches: vec![GameChannelPatch {
        period: InputPeriodLower { start: None, end: None },
        is_enabled: true,
        default_permission: GameChannelPermission::Play,
        version: new_build.version,
        is_pinned: false,
        publication_date: Some(api.clock.now()),
        sort_update_date: api.clock.now(),
      }],
    })
    .await
    .unwrap();

  assert_eq!(actual_time, Instant::ymd_hms(2021, 1, 1, 0, 0, 3));
  let expected = ActiveGameChannel {
    key: "main".parse().unwrap(),
    is_enabled: true,
    is_pinned: false,
    publication_date: Some(Instant::ymd_hms(2021, 1, 1, 0, 0, 3)),
    sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    default_permission: GameChannelPermission::Play,
    build: GameBuild {
      version: "1.0.1".parse().unwrap(),
      created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      git_commit_ref: None,
      main_locale: LocaleId::FrFr,
      display_name: "Sous la colline".parse().unwrap(),
      description: "Aidez Igor dans ses aventures. Ravira petits et grands."
        .parse()
        .unwrap(),
      icon: None,
      loader: "4.1.0".parse().unwrap(),
      engine: GameEngine::V96,
      patcher: None,
      debug: None,
      content: None,
      content_i18n: None,
      musics: vec![],
      modes: IndexMap::new(),
      families: "10".parse().unwrap(),
      category: GameCategory::Small,
      i18n: [(
        LocaleId::EnUs,
        GameBuildI18n {
          display_name: Some("Under the hill".parse().unwrap()),
          description: None,
          icon: None,
          content_i18n: None,
          modes: Default::default(),
        },
      )]
      .into_iter()
      .collect(),
    },
  };
  assert_eq!(actual, expected);
}

pub(crate) async fn test_get_games<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>(
  api: TestApi<TyBlobStore, TyClock, TyGameStore, TyUserStore, TyUuidGenerator>,
) where
  TyBlobStore: BlobStoreRef,
  TyClock: SyncRef<VirtualClock>,
  TyGameStore: GameStoreRef,
  TyUserStore: UserStoreRef,
  TyUuidGenerator: UuidGeneratorRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice = api
    .user_store
    .user_store()
    .upsert_from_etwin(&ShortUser {
      id: api.uuid_generator.uuid_generator().next().into(),
      display_name: "Alice".parse().unwrap(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let icon = api
    .blob_store
    .blob_store()
    .create_blob(&CreateBlobOptions {
      media_type: "image/png".parse().unwrap(),
      data: include_bytes!("../../../test-resources/games/sous-la-colline/icon.png").to_vec(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let game = api
    .game_store
    .game_store()
    .create_game(&CreateStoreGame {
      owner: alice.id.into(),
      key: None,
      build: InputGameBuild {
        version: "1.0.0".parse().unwrap(),
        git_commit_ref: None,
        main_locale: LocaleId::FrFr,
        display_name: "Sous la colline".parse().unwrap(),
        description: "Aidez Igor dans ses aventures".parse().unwrap(),
        icon: Some(icon.as_ref()),
        loader: "4.1.0".parse().unwrap(),
        engine: GameEngine::V96,
        patcher: None,
        debug: None,
        content: None,
        content_i18n: None,
        musics: vec![],
        modes: IndexMap::new(),
        families: "10".parse().unwrap(),
        category: GameCategory::Small,
        i18n: [(
          LocaleId::EnUs,
          GameBuildI18n {
            display_name: Some("Under the hill".parse().unwrap()),
            description: None,
            icon: None,
            content_i18n: None,
            modes: Default::default(),
          },
        )]
        .into_iter()
        .collect(),
      },
      channels: BoundedVec::new(vec![InputGameChannel {
        key: "main".parse().unwrap(),
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        is_pinned: false,
        publication_date: None,
        sort_update_date: None,
        version: "1.0.0".parse().unwrap(),
        patches: vec![],
      }])
      .unwrap(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let faille = api
    .game_store
    .game_store()
    .create_game(&CreateStoreGame {
      owner: alice.id.into(),
      key: None,
      build: InputGameBuild {
        version: "1.0.0".parse().unwrap(),
        git_commit_ref: None,
        main_locale: LocaleId::FrFr,
        display_name: "Faille éternelle".parse().unwrap(),
        description: "Pic pic pic".parse().unwrap(),
        icon: None,
        loader: "4.1.0".parse().unwrap(),
        engine: GameEngine::V96,
        patcher: None,
        debug: None,
        content: None,
        content_i18n: None,
        musics: vec![],
        modes: IndexMap::new(),
        families: "10,20".parse().unwrap(),
        category: GameCategory::Small,
        i18n: [(
          LocaleId::EnUs,
          GameBuildI18n {
            display_name: Some("Eternal rift".parse().unwrap()),
            description: None,
            icon: None,
            content_i18n: None,
            modes: Default::default(),
          },
        )]
        .into_iter()
        .collect(),
      },
      channels: BoundedVec::new(vec![InputGameChannel {
        key: "main".parse().unwrap(),
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        is_pinned: false,
        publication_date: None,
        sort_update_date: None,
        version: "1.0.0".parse().unwrap(),
        patches: vec![],
      }])
      .unwrap(),
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .game_store
    .game_store()
    .create_game_channel(&CreateGameChannel {
      actor: alice.id.into(),
      game: game.id.into(),
      channel: InputGameChannel {
        key: "beta".parse().unwrap(),
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        is_pinned: false,
        publication_date: None,
        sort_update_date: None,
        version: "1.0.0".parse().unwrap(),
        patches: vec![],
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  api
    .game_store
    .game_store()
    .create_version(&CreateBuild {
      game: game.id.into(),
      if_owner: Some(alice.id.into()),
      build: InputGameBuild {
        version: "1.0.1".parse().unwrap(),
        git_commit_ref: None,
        main_locale: LocaleId::FrFr,
        display_name: "Sous la colline".parse().unwrap(),
        description: "Aidez Igor dans ses aventures. Ravira petits et grands."
          .parse()
          .unwrap(),
        icon: Some(icon.as_ref()),
        loader: "4.1.0".parse().unwrap(),
        engine: GameEngine::V96,
        patcher: None,
        debug: None,
        content: None,
        content_i18n: None,
        musics: vec![],
        modes: IndexMap::new(),
        families: "10".parse().unwrap(),
        category: GameCategory::Small,
        i18n: [(
          LocaleId::EnUs,
          GameBuildI18n {
            display_name: Some("Under the hill".parse().unwrap()),
            description: None,
            icon: None,
            content_i18n: None,
            modes: Default::default(),
          },
        )]
        .into_iter()
        .collect(),
      },
    })
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let (actual, actual_time) = api
    .game_store
    .game_store()
    .update_game_channel(&UpdateStoreGameChannel {
      actor: alice.id.into(),
      if_owner: Some(alice.id.into()),
      game: game.id.into(),
      channel_key: "beta".parse().unwrap(),
      patches: vec![GameChannelPatch {
        period: InputPeriodLower { start: None, end: None },
        is_enabled: true,
        default_permission: GameChannelPermission::None,
        is_pinned: false,
        publication_date: None,
        sort_update_date: api.clock.now(),
        version: "1.0.1".parse().unwrap(),
      }],
    })
    .await
    .unwrap();

  assert_eq!(actual_time, Instant::ymd_hms(2021, 1, 1, 0, 0, 6));
  let expected = ActiveGameChannel {
    key: "beta".parse().unwrap(),
    is_enabled: true,
    is_pinned: false,
    publication_date: None,
    sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 6),
    default_permission: GameChannelPermission::None,
    build: GameBuild {
      version: "1.0.1".parse().unwrap(),
      created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
      git_commit_ref: None,
      main_locale: LocaleId::FrFr,
      display_name: "Sous la colline".parse().unwrap(),
      description: "Aidez Igor dans ses aventures. Ravira petits et grands."
        .parse()
        .unwrap(),
      icon: Some(icon.as_ref()),
      loader: "4.1.0".parse().unwrap(),
      engine: GameEngine::V96,
      patcher: None,
      debug: None,
      content: None,
      content_i18n: None,
      musics: vec![],
      modes: IndexMap::new(),
      families: "10".parse().unwrap(),
      category: GameCategory::Small,
      i18n: [(
        LocaleId::EnUs,
        GameBuildI18n {
          display_name: Some("Under the hill".parse().unwrap()),
          description: None,
          icon: None,
          content_i18n: None,
          modes: Default::default(),
        },
      )]
      .into_iter()
      .collect(),
    },
  };
  assert_eq!(actual, expected);

  api.clock.advance_by(Duration::from_seconds(1));

  let (actual, actual_time) = api
    .game_store
    .game_store()
    .update_game_channel(&UpdateStoreGameChannel {
      actor: alice.id.into(),
      if_owner: Some(alice.id.into()),
      game: game.id.into(),
      channel_key: "main".parse().unwrap(),
      patches: vec![GameChannelPatch {
        period: InputPeriodLower { start: None, end: None },
        is_enabled: false,
        default_permission: GameChannelPermission::Play,
        is_pinned: false,
        publication_date: Some(api.clock.now()),
        sort_update_date: api.clock.now(),
        version: "1.0.0".parse().unwrap(),
      }],
    })
    .await
    .unwrap();

  assert_eq!(actual_time, Instant::ymd_hms(2021, 1, 1, 0, 0, 7));
  let expected = ActiveGameChannel {
    key: "main".parse().unwrap(),
    is_enabled: false,
    is_pinned: false,
    publication_date: Some(Instant::ymd_hms(2021, 1, 1, 0, 0, 7)),
    sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 7),
    default_permission: GameChannelPermission::Play,
    build: GameBuild {
      version: "1.0.0".parse().unwrap(),
      created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
      git_commit_ref: None,
      main_locale: LocaleId::FrFr,
      display_name: "Sous la colline".parse().unwrap(),
      description: "Aidez Igor dans ses aventures".parse().unwrap(),
      icon: Some(icon.as_ref()),
      loader: "4.1.0".parse().unwrap(),
      engine: GameEngine::V96,
      patcher: None,
      debug: None,
      content: None,
      content_i18n: None,
      musics: vec![],
      modes: IndexMap::new(),
      families: "10".parse().unwrap(),
      category: GameCategory::Small,
      i18n: [(
        LocaleId::EnUs,
        GameBuildI18n {
          display_name: Some("Under the hill".parse().unwrap()),
          description: None,
          icon: None,
          content_i18n: None,
          modes: Default::default(),
        },
      )]
      .into_iter()
      .collect(),
    },
  };
  assert_eq!(actual, expected);

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .game_store
    .game_store()
    .get_short_games(&GetStoreShortGames {
      actor: Some(alice.id.into()),
      is_tester: false,
      offset: 0,
      limit: NonZeroU32::new(10).expect("constant value is non-zero"),
      favorite: false,
      now: api.clock.now(),
      time: None,
    })
    .await
    .unwrap();

  let expected = Listing {
    offset: 0,
    limit: NonZeroU32::new(10).expect("constant value is non-zero"),
    count: 2,
    is_count_exact: false,
    items: vec![
      Some(ShortGame {
        id: game.id,
        created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 2),
        key: None,
        owner: alice.id.into(),
        channels: Listing {
          offset: 0,
          limit: NonZeroU32::new(1).expect("constant vallue is non-zero"),
          count: 1,
          is_count_exact: false,
          items: vec![ShortGameChannel {
            key: "beta".parse().unwrap(),
            is_enabled: true,
            is_pinned: false,
            publication_date: None,
            sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 6),
            default_permission: GameChannelPermission::None,
            build: ShortGameBuild {
              version: "1.0.1".parse().unwrap(),
              git_commit_ref: None,
              main_locale: LocaleId::FrFr,
              display_name: "Sous la colline".parse().unwrap(),
              description: "Aidez Igor dans ses aventures. Ravira petits et grands."
                .parse()
                .unwrap(),
              icon: Some(icon.as_ref()),
              i18n: [(
                LocaleId::EnUs,
                ShortGameBuildI18n {
                  display_name: Some("Under the hill".parse().unwrap()),
                  description: None,
                  icon: None,
                },
              )]
              .into_iter()
              .collect(),
            },
          }],
        },
      }),
      Some(ShortGame {
        id: faille.id,
        created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
        key: None,
        owner: alice.id.into(),
        channels: Listing {
          offset: 0,
          limit: NonZeroU32::new(1).expect("constant value is non-zero"),
          count: 1,
          is_count_exact: false,
          items: vec![ShortGameChannel {
            key: "main".parse().unwrap(),
            is_enabled: true,
            is_pinned: false,
            publication_date: None,
            sort_update_date: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
            default_permission: GameChannelPermission::None,
            build: ShortGameBuild {
              version: "1.0.0".parse().unwrap(),
              git_commit_ref: None,
              main_locale: LocaleId::FrFr,
              display_name: "Faille éternelle".parse().unwrap(),
              description: "Pic pic pic".parse().unwrap(),
              icon: None,
              i18n: [(
                LocaleId::EnUs,
                ShortGameBuildI18n {
                  display_name: Some("Eternal rift".parse().unwrap()),
                  description: None,
                  icon: None,
                },
              )]
              .into_iter()
              .collect(),
            },
          }],
        },
      }),
    ],
  };

  assert_eq!(actual, expected);

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .game_store
    .game_store()
    .get_short_games(&GetStoreShortGames {
      actor: None,
      is_tester: false,
      offset: 0,
      limit: NonZeroU32::new(10).expect("constant value is non-zero"),
      favorite: false,
      now: api.clock.now(),
      time: None,
    })
    .await
    .unwrap();

  let expected = Listing {
    offset: 0,
    limit: NonZeroU32::new(10).expect("constant value is non-zero"),
    count: 0,
    is_count_exact: false,
    items: Vec::new(),
  };

  assert_eq!(actual, expected);
}
