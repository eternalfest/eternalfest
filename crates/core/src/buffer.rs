use async_trait::async_trait;
use eternaltwin_core::declare_new_uuid;
use eternaltwin_core::types::WeakError;
use futures::Stream;
use std::ops::Deref;
use std::pin::Pin;

declare_new_uuid! {
  pub struct BufferId(Uuid);
  pub type ParseError = BufferIdParseError;
  const SQL_NAME = "buffer_id";
}

#[derive(Debug, thiserror::Error)]
pub enum CreateBufferError {
  #[error("Failed to allocate a buffer of size {:?} because it is too large", .0)]
  MaxSize(u32),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl CreateBufferError {
  pub fn other<E>(e: E) -> Self
  where
    E: ::std::error::Error + Send + Sync + 'static,
  {
    CreateBufferError::Other(WeakError::wrap(e))
  }
}

#[derive(Debug, thiserror::Error)]
pub enum DeleteBufferError {
  #[error("Failed to delete: buffer not found for id {:?}", .0)]
  NotFound(BufferId),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, thiserror::Error)]
pub enum WriteBytesError {
  #[error("Failed to write: buffer not found for id {:?}", .0)]
  NotFound(BufferId),
  #[error("failed to write: input bytes too large")]
  MaxSize,
  #[error("write past end of buffer")]
  BufferOverflow,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl WriteBytesError {
  pub fn other<E>(e: E) -> Self
  where
    E: ::std::error::Error + Send + Sync + 'static,
  {
    WriteBytesError::Other(WeakError::wrap(e))
  }
}

#[derive(Debug, thiserror::Error)]
pub enum ReadStreamError {
  #[error("Failed to read: buffer not found for id {:?}", .0)]
  NotFound(BufferId),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Buffer {
  pub id: BufferId,
}

#[async_trait]
pub trait BufferStore: Send + Sync {
  async fn create_buffer(&self, size: u32) -> Result<BufferId, CreateBufferError>;
  async fn delete_buffer(&self, buffer_id: BufferId) -> Result<(), DeleteBufferError>;
  async fn delete_buffer_if_exists(&self, buffer_id: BufferId) -> Result<(), WeakError>;
  async fn write_bytes(&self, buffer_id: BufferId, offset: u32, bytes: &[u8]) -> Result<(), WriteBytesError>;
  async fn read_stream(&self, buffer_id: BufferId) -> Result<Vec<u8>, ReadStreamError>;
  async fn list(&self) -> Result<Pin<Box<dyn Send + Sync + Stream<Item = Result<Buffer, WeakError>> + '_>>, WeakError>;
}

/// Like [`Deref`], but the target has the bound [`BufferStore`]
pub trait BufferStoreRef: Send + Sync {
  type BufferStore: BufferStore + ?Sized;

  fn buffer_store(&self) -> &Self::BufferStore;
}

impl<TyRef> BufferStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: BufferStore,
{
  type BufferStore = TyRef::Target;

  fn buffer_store(&self) -> &Self::BufferStore {
    self.deref()
  }
}
