pub use eternaltwin_core::uuid::{Uuid4Generator, UuidGenerator};
use std::ops::Deref;

// todo: use impl from eternaltwin_core
/// Like [`Deref`], but the target has the bound [`UuidGenerator`]
pub trait UuidGeneratorRef: Send + Sync {
  type UuidGenerator: UuidGenerator + ?Sized;

  fn uuid_generator(&self) -> &Self::UuidGenerator;
}

impl<TyRef> UuidGeneratorRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: UuidGenerator,
{
  type UuidGenerator = TyRef::Target;

  fn uuid_generator(&self) -> &Self::UuidGenerator {
    self.deref()
  }
}
