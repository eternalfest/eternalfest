use std::collections::BTreeMap;

use eternaltwin_core::{
  declare_decimal_id,
  hammerfest::{ShortHammerfestUser, StoredHammerfestProfile},
};

use crate::game::{FamilyList, GameBuild, GameModeKey, GameOptionKey};

mod quest_db;

pub use quest_db::QuestDb;

pub use eternaltwin_core::hammerfest::{
  HammerfestItemId as ItemId, HammerfestItemIdParseError as ItemIdParseError, HammerfestQuestStatus as QuestStatus,
  HammerfestQuestStatusParseError as QuestStatusParseError,
};

declare_decimal_id! {
    pub struct FamilyId(u16);
    pub type ParseError = FamilyIdParseError;
    const BOUNDS = 0..10_000;
}

// Can't use `eternaltwin_core::hammerfest::HammerfestQuestId`, its bounds are too strict.
declare_decimal_id! {
  pub struct QuestId(u16);
  pub type ParseError = QuestIdParseError;
  const BOUNDS = 0..10_000;
}

#[derive(Debug)]
pub struct Quest {
  pub id: Option<QuestId>,
  pub require: Vec<(ItemId, u32)>,
  pub rewards: Vec<Reward>,
}

impl Quest {
  fn status_for_inner<const CHECK_NONE: bool>(&self, items: &BTreeMap<ItemId, u32>) -> QuestStatus {
    let complete = self.require.iter().try_fold(true, |cur, (item, qty_needed)| {
      let qty = items.get(item).copied().unwrap_or_default();
      match cur {
        true if qty >= *qty_needed => Ok(true),
        false if CHECK_NONE && qty == 0 => Ok(false),
        _ => Err(()),
      }
    });

    match complete {
      Ok(true) => QuestStatus::Complete,
      Ok(false) => QuestStatus::None,
      Err(()) => QuestStatus::Pending,
    }
  }

  pub fn status_for(&self, items: &BTreeMap<ItemId, u32>) -> QuestStatus {
    self.status_for_inner::<true>(items)
  }

  pub fn rewards_for<'a>(&'a self, items: &BTreeMap<ItemId, u32>) -> impl Iterator<Item = &'a Reward> {
    if let QuestStatus::Complete = self.status_for_inner::<false>(items) {
      self.rewards.iter()
    } else {
      [].iter()
    }
  }
}

#[derive(Clone, Debug)]
pub enum Reward {
  GiveFamily(FamilyId),
  RemoveFamily(FamilyId),
  GiveMode(GameModeKey),
  RemoveMode(GameModeKey),
  GiveOption(GameOptionKey),
  RemoveOption(GameOptionKey),
}

impl Reward {
  pub fn apply<K: crate::game::Kind>(&self, game: &mut GameBuild<K>) {
    match self {
      Self::GiveFamily(fam) => {
        game.families.0.insert(*fam);
      }
      Self::RemoveFamily(fam) => {
        game.families.0.remove(fam);
      }
      Self::GiveMode(m) => {
        if let Some(mode) = game.modes.get_mut(m) {
          mode.is_visible = true;
        }
      }
      Self::RemoveMode(m) => {
        if let Some(mode) = game.modes.get_mut(m) {
          mode.is_visible = false;
        }
      }
      Self::GiveOption(o) => {
        for option in game.modes.values_mut().filter_map(|m| m.options.get_mut(o)) {
          option.is_enabled = true;
          option.is_visible = true;
        }
      }
      Self::RemoveOption(o) => {
        for option in game.modes.values_mut().filter_map(|m| m.options.get_mut(o)) {
          option.is_enabled = false;
        }
      }
    }
  }

  pub fn apply_families(&self, families: &mut FamilyList) {
    match self {
      Self::GiveFamily(fam) => families.0.insert(*fam),
      Self::RemoveFamily(fam) => families.0.remove(fam),
      _ => false,
    };
  }
}

#[derive(Clone, Debug)]
pub struct RecoveredHfestProfile {
  pub hfest_user: ShortHammerfestUser,
  pub max_level: i32,
  pub best_score: i64,
  pub items: BTreeMap<ItemId, u32>,
  pub are_items_exact: bool,
}

impl RecoveredHfestProfile {
  pub fn empty(hfest_user: ShortHammerfestUser) -> Self {
    Self {
      hfest_user,
      max_level: 0,
      best_score: 0,
      items: Default::default(),
      are_items_exact: false,
    }
  }

  /// Recover an profile from an archived hammerfest user.
  ///
  /// If full profile info is missing, try to reconstruct a "minimal profile"
  /// from the partial info publicly available.
  pub fn recover_from(archived: eternaltwin_core::hammerfest::HammerfestUser, db: &QuestDb) -> Self {
    let mut this = Self::empty(ShortHammerfestUser {
      id: archived.id,
      server: archived.server,
      username: archived.username,
    });

    if let Some(items) = archived.inventory {
      this.items = items.latest.value;
      this.are_items_exact = true;
    }

    if let Some(profile) = archived.profile.map(|p| p.latest) {
      this.max_level = profile.value.best_level;
      this.best_score = profile.value.best_score.into();
      // Only use item estimation if there was no inventory.
      if !this.are_items_exact {
        this.estimate_items_from(&profile.value, db);
      }
    }

    this
  }

  fn estimate_items_from(&mut self, profile: &StoredHammerfestProfile, db: &QuestDb) {
    use eternaltwin_core::hammerfest::{HammerfestQuestId, HammerfestQuestStatus};

    let mut ensure = |id: ItemId, qty: u32| {
      let q = self.items.entry(id).or_default();
      *q = qty.max(*q);
    };

    // Unlocked items give lower bounds.
    for unlocked in &profile.items {
      if let Some(qty) = db.hammerfest_unlock_for(*unlocked) {
        ensure(*unlocked, qty);
      }
    }

    let quests = db.hammerfest_quests().iter().filter_map(|q| {
      q.id
        .and_then(|id| u8::try_from(id.0).ok())
        .and_then(|id| HammerfestQuestId::new(id).ok())
        .map(|id| (id, &q.require))
    });

    for (id, require) in quests {
      match profile.quests.get(&id).copied().unwrap_or(HammerfestQuestStatus::None) {
        // A completed quest gives a lower bound for every required item.
        HammerfestQuestStatus::Complete => {
          for (item, qty) in require {
            ensure(*item, *qty);
          }
        }
        // A pending quest with a single item type gives one copy of this item.
        HammerfestQuestStatus::Pending if require.len() == 1 => {
          let (item, _) = require.first().unwrap();
          ensure(*item, 1);
        }
        _ => (),
      }
    }
  }
}
