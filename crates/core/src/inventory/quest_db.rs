use std::collections::HashMap;
use std::sync::OnceLock;

use crate::game::GameKey;
use crate::inventory::{ItemId, Quest};

pub struct QuestDb {
  quests: HashMap<&'static str, Vec<Quest>>,
  hfest_unlocks: HashMap<ItemId, u32>,
}

impl QuestDb {
  pub fn hardcoded() -> &'static Self {
    static QUESTS: OnceLock<QuestDb> = OnceLock::new();
    QUESTS.get_or_init(|| {
      let mut this = Self {
        quests: HashMap::new(),
        hfest_unlocks: unlocks_hammerfest().collect(),
      };

      this.quests.insert("hammerfest", quests_hammerfest());
      this.quests.insert("otherworldly_well", quests_otherwordly_well());
      this.quests.insert("hackfest", quests_hackfest());

      this
    })
  }

  #[inline]
  pub fn hammerfest_quests(&self) -> &[Quest] {
    &self.quests["hammerfest"]
  }

  #[inline]
  pub fn get(&self, key: &GameKey) -> Option<&[Quest]> {
    self.quests.get(key.as_str()).map(|v| v.as_slice())
  }

  #[inline]
  pub fn hammerfest_unlock_for(&self, item: ItemId) -> Option<u32> {
    self.hfest_unlocks.get(&item).copied()
  }
}

fn unlocks_hammerfest() -> impl Iterator<Item = (ItemId, u32)> {
  // Special items
  let spec = (0..=117).map(|id| match id {
    // Carotte, Pioupiou
    102 | 112 => (id, 1),
    // Coeurs & Flocons
    103..=105 | 109..=111 => (id, 1),
    // Signes & Potions du Zodiaque
    40..=63 => (id, 1),
    // Téléphone-phone-phone
    10 => (id, 5),
    _ => (id, 10),
  });

  // Score items (without forbidden items)
  let score = (1000..=1238).map(|id| match id {
    // Clé du Bourru, Autorisation du Bois-Joli
    1193 | 1196 => (id, 1),
    // Sandy en kit
    1124 => (id, 3),
    1221 | 1223 => (id, 5),
    _ => (id, 10),
  });

  spec.chain(score).map(|(id, qty)| (ItemId::new(id).unwrap(), qty))
}

fn id<T: std::str::FromStr + 'static>(s: &str) -> T {
  s.parse()
    .unwrap_or_else(|_| panic!("invalid {}: {s}", std::any::type_name::<T>()))
}

fn quests_otherwordly_well() -> Vec<Quest> {
  use super::Reward::*;

  vec![
    Quest {
      id: None, // the game's `.toml` starts with all options enabled, so disable them
      require: Vec::new(),
      rewards: vec![RemoveOption(id("nightmare"))],
    },
    Quest {
      id: Some(id("5003")),
      require: vec![(id("1241"), 1)],
      rewards: vec![GiveOption(id("nightmare"))],
    },
  ]
}

fn quests_hackfest() -> Vec<Quest> {
  use super::Reward::*;

  vec![
    Quest {
      id: None, // the game's `.toml` starts with all modes enabled, so disable them
      require: Vec::new(),
      rewards: vec![RemoveMode(id("prelude")), RemoveMode(id("nightmare"))],
    },
    Quest {
      id: Some(id("1028")),
      require: vec![(id("1262"), 1)],
      rewards: vec![GiveMode(id("prelude"))],
    },
    Quest {
      id: Some(id("1038")),
      require: vec![(id("1253"), 1)],
      rewards: vec![GiveMode(id("nightmare"))],
    },
  ]
}

#[rustfmt::skip]
fn quests_hammerfest() -> Vec<Quest> {
  use super::Reward::*;

  vec![
    Quest {
      // Carotte -> all deluxe content, except deluxemulti & multitime (separate from "normal" quest to have two messages)
      id: Some(id("100")),
      require: vec![(id("102"), 1)],
      rewards: vec![
        GiveMode(id("deluxe")), GiveMode(id("bossrush")),
        GiveOption(id("set_ta_1")),
        GiveOption(id("set_soctime_short")), GiveOption(id("set_soctime_normal")), GiveOption(id("set_soctime_long")),
        GiveOption(id("soccerparty")), GiveOption(id("prolongation")), GiveOption(id("set_soc_alea")),
        GiveOption(id("set_soc_4")), GiveOption(id("set_soc_5")), GiveOption(id("set_soc_6")), GiveOption(id("set_soc_7")),
        GiveOption(id("set_soc_8")), GiveOption(id("set_soc_9")), GiveOption(id("set_soc_10")), GiveOption(id("set_soc_11")),
        GiveOption(id("set_soc_12")), GiveOption(id("set_soc_13")), GiveOption(id("set_soc_14")), GiveOption(id("set_soc_15")),
        GiveOption(id("set_soc_16")), GiveOption(id("set_soc_17")), GiveOption(id("set_soc_18")), GiveOption(id("set_soc_19")),
        GiveOption(id("set_soc_20")), GiveOption(id("set_soc_21")), GiveOption(id("set_soc_22")), GiveOption(id("set_soc_23")),
      ],
    },
    Quest {
      // Carotte + Miroir Bancal -> deluxemulti
      id: None,
      require: vec![(id("102"), 1), (id("1219"), 1)],
      rewards: vec![GiveMode(id("deluxemulti"))],
    },
    Quest {
      // Carotte + Miroir Bancal + 100 Shurikens -> multitime
      id: None,
      require: vec![(id("102"), 1), (id("1219"), 1), (id("1236"), 100)],
      rewards: vec![GiveMode(id("multitime"))],
    },
    Quest {
      // Custom quest: volleyfest helmet + forbidden items -> extra disguises
      id: Some(id("101")),
      require: vec![(id("115"), 1), (id("1186"), 1), (id("1187"), 1), (id("1188"), 1), (id("1189"), 1)],
      rewards: vec![GiveFamily(id("114"))],
    },

    Quest {
      id: Some(id("5")),
      require: vec![(id("36"), 5)],
      rewards: vec![GiveFamily(id("105"))],
    },
    Quest {
      id: Some(id("6")),
      require: vec![(id("1000"), 5), (id("1003"), 1), (id("1013"), 1), (id("1027"), 1), (id("1041"), 1), (id("1043"), 1)],
      rewards: vec![GiveFamily(id("1001"))],
    },
    Quest {
      id: Some(id("7")),
      require: vec![(id("1003"), 10),(id("1027"), 10), (id("1041"), 10), (id("1070"), 2), (id("1074"), 2)],
      rewards: vec![GiveFamily(id("1002"))],
    },
    Quest {
      id: Some(id("8")),
      require: vec![(id("1022"), 5), (id("1024"), 5), (id("1028"), 20), (id("1025"), 10)],
      rewards: vec![GiveFamily(id("1004"))],
    },
    Quest {
      id: Some(id("9")),
      require: vec![(id("1002"), 1), (id("1046"), 2), (id("1040"), 10)],
      rewards: vec![GiveFamily(id("1005"))],
    },
    Quest {
      id: Some(id("10")),
      require: vec![(id("4"), 1), (id("7"), 1), (id("13"), 1)],
      rewards: vec![GiveFamily(id("1")), GiveFamily(id("8")), GiveFamily(id("13")), GiveFamily(id("15"))],
    },
    Quest {
      id: Some(id("11")),
      require: vec![(id("106"), 1)],
      rewards: vec![GiveFamily(id("3"))],
    },
    Quest {
      id: Some(id("12")),
      require: vec![(id("1051"), 150)],
      rewards: vec![GiveFamily(id("1007"))],
    },
    Quest {
      id: Some(id("13")),
      require: vec![(id("107"), 1)],
      rewards: vec![GiveFamily(id("5"))],
    },
    Quest {
      id: Some(id("14")),
      require: vec![
        (id("1"), 10), (id("5"), 5), (id("11"), 5), (id("21"), 10), (id("22"), 5),
        (id("24"), 1), (id("25"), 1), (id("28"), 3), (id("38"), 1), (id("82"), 5),
      ],
      rewards: vec![GiveFamily(id("2"))],
    },
    Quest {
      id: Some(id("15")),
      require: vec![
        (id("1057"), 11), (id("1074"), 11), (id("1137"), 11), (id("1138"), 11), (id("1139"), 11),
        (id("1140"), 11), (id("1149"), 20), (id("1045"), 30), (id("1081"), 11),
      ],
      rewards: vec![GiveFamily(id("11")), GiveFamily(id("1014"))],
    },
    Quest {
      id: Some(id("16")),
      require: vec![(id("1142"), 20)],
      rewards: vec![GiveFamily(id("4")), GiveFamily(id("1015"))],
    },
    Quest {
      id: Some(id("17")),
      require: vec![(id("1046"), 5)],
      rewards: vec![GiveFamily(id("1010"))],
    },
    Quest {
      id: Some(id("18")),
      require: vec![(id("1008"), 30)],
      rewards: vec![GiveFamily(id("1003"))],
    },
    Quest {
      id: Some(id("19")),
      require: vec![(id("1001"), 20), (id("1055"), 20), (id("1056"), 20)],
      rewards: vec![GiveFamily(id("1012"))],
    },
    Quest {
      id: Some(id("20")),
      require: vec![(id("1009"), 10), (id("1010"), 5), (id("1011"), 1)],
      rewards: vec![GiveFamily(id("10")), GiveFamily(id("12"))],
    },
    Quest {
      id: Some(id("21")),
      require: vec![(id("1001"), 20), (id("1021"), 10), (id("1025"), 20), (id("1061"), 1), (id("1162"), 10)],
      rewards: vec![GiveFamily(id("1008"))],
    },
    Quest {
      id: Some(id("22")),
      require: vec![(id("1020"), 15), (id("1039"), 1), (id("1060"), 2), (id("1104"), 5), (id("1103"), 3)],
      rewards: vec![GiveFamily(id("1013"))],
    },
    Quest {
      id: Some(id("23")),
      require: vec![
        (id("1091"), 1), (id("1112"), 1), (id("1126"), 1), (id("1163"), 1),
        (id("1109"), 1), (id("1138"), 1), (id("1142"), 1),
      ],
      rewards: vec![GiveFamily(id("1017"))],
    },
    Quest {
      id: Some(id("24")),
      require: vec![
        (id("68"), 1), (id("1004"), 10), (id("1005"), 10), (id("1007"), 1), (id("1015"), 1),
        (id("1016"), 3), (id("1018"), 5), (id("1019"), 5), (id("1023"), 10), (id("1070"), 10),
      ],
      rewards: vec![GiveFamily(id("1016"))],
    },
    Quest {
      id: Some(id("25")),
      require: vec![(id("1069"), 10), (id("1081"), 10), (id("1094"), 10)],
      rewards: vec![GiveFamily(id("1006"))],
    },
    Quest {
      id: Some(id("26")),
      require: vec![(id("1031"), 10), (id("1057"), 10), (id("1137"), 1)],
      rewards: vec![GiveFamily(id("1011"))],
    },
    Quest {
      id: Some(id("27")),
      require: vec![(id("88"), 1), (id("99"), 1), (id("100"), 1), (id("1044"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("28")),
      require: vec![
        (id("1059"), 2), (id("1084"), 2), (id("1088"), 2), (id("1144"), 2),
        (id("1151"), 2), (id("1152"), 2), (id("1153"), 2), (id("1154"), 2),
      ],
      rewards: vec![GiveFamily(id("1009"))],
    },
    Quest {
      id: Some(id("29")),
      require: vec![(id("21"), 10), (id("26"), 10), (id("31"), 10), (id("85"), 10), (id("90"), 10)],
      rewards: vec![GiveFamily(id("1018"))],
    },
    Quest {
      id: Some(id("36")),
      require: vec![(id("1178"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("37")),
      require: vec![(id("1179"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("38")),
      require: vec![(id("1180"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("39")),
      require: vec![(id("1181"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("40")),
      require: vec![(id("1182"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("41")),
      require: vec![(id("1183"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("42")),
      require: vec![(id("1184"), 1)],
      rewards: vec![],
    },
    Quest {
      id: Some(id("43")),
      require: vec![(id("102"), 1)],
      rewards: vec![GiveFamily(id("108"))],
    },
    Quest {
      id: Some(id("44")),
      require: vec![(id("72"), 5), (id("91"), 5), (id("92"), 10)],
      rewards: vec![GiveFamily(id("109"))],
    },
    Quest {
      id: Some(id("45")),
      require: vec![(id("1053"), 10), (id("1054"), 10), (id("95"), 3)],
      rewards: vec![GiveFamily(id("110"))],
    },
    Quest {
      id: Some(id("46")),
      require: vec![(id("112"), 1)],
      rewards: vec![GiveFamily(id("111"))],
    },
    Quest {
      id: Some(id("47")),
      require: vec![(id("14"), 1), (id("15"), 1), (id("16"), 1), (id("17"), 1)],
      rewards: vec![GiveFamily(id("19")), GiveFamily(id("112"))],
    },
    Quest {
      id: Some(id("48")),
      require: vec![(id("113"), 1)],
      rewards: vec![GiveFamily(id("113"))],
    },
    Quest {
      id: Some(id("49")),
      require: vec![(id("1190"), 1)],
      rewards: vec![ GiveFamily(id("5000")), GiveFamily(id("1023")), GiveFamily(id("1025")), GiveFamily(id("1026"))],
    },
    Quest {
      id: Some(id("50")),
      require: vec![(id("1191"), 1)],
      rewards: vec![GiveFamily(id("5001"))],
    },
    Quest {
      id: Some(id("51")),
      require: vec![(id("1192"), 1)],
      rewards: vec![GiveFamily(id("5002"))],
    },
    Quest {
      id: Some(id("53")),
      require: vec![(id("1194"), 1)],
      rewards: vec![GiveFamily(id("5004"))],
    },
    Quest {
      id: Some(id("54")),
      require: vec![(id("1195"), 1)],
      rewards: vec![GiveFamily(id("5005"))],
    },
    Quest {
      id: Some(id("56")),
      require: vec![(id("1197"), 1)],
      rewards: vec![GiveFamily(id("5007"))],
    },
    Quest {
      id: Some(id("57")),
      require: vec![(id("1198"), 1)],
      rewards: vec![GiveFamily(id("5008"))],
    },
    Quest {
      id: Some(id("58")),
      require: vec![(id("1199"), 1)],
      rewards: vec![GiveFamily(id("5009"))],
    },
    Quest {
      id: Some(id("59")),
      require: vec![(id("1200"), 1)],
      rewards: vec![GiveFamily(id("5010"))],
    },
    Quest {
      id: Some(id("60")),
      require: vec![(id("1209"), 20), (id("1210"), 10), (id("1211"), 5), (id("1212"), 3), (id("1213"), 2)],
      rewards: vec![GiveFamily(id("1021"))],
    },
    Quest {
      id: Some(id("61")),
      require: vec![(id("1214"), 10), (id("1215"), 15), (id("1216"), 3), (id("1217"), 2), (id("1218"), 1)],
      rewards: vec![GiveFamily(id("1022"))],
    },
    Quest {
      id: Some(id("62")),
      require: vec![
        (id("1201"), 1), (id("1202"), 1), (id("1203"), 1), (id("1204"), 1),
        (id("1205"), 1), (id("1206"), 1),(id("1207"), 1), (id("1208"), 1),
      ],
      rewards: vec![GiveOption(id("boost"))],
    },
    Quest {
      id: Some(id("63")),
      require: vec![(id("1219"), 1)],
      rewards: vec![GiveOption(id("mirror"))],
    },
    Quest {
      id: Some(id("64")),
      require: vec![(id("1220"), 1)],
      rewards: vec![GiveOption(id("nightmare"))],
    },
    Quest {
      id: Some(id("65")),
      require: vec![(id("117"), 1)],
      rewards: vec![GiveFamily(id("5012"))],
    },
    Quest {
      id: Some(id("66")),
      require: vec![(id("116"), 1)],
      rewards: vec![GiveOption(id("kickcontrol"))],
    },
    Quest {
      id: Some(id("68")),
      require: vec![(id("1225"), 1)],
      rewards: vec![GiveOption(id("mirrormulti"))],
    },
    Quest {
      id: Some(id("69")),
      require: vec![(id("1226"), 1)],
      rewards: vec![GiveOption(id("nightmaremulti"))],
    },
    Quest {
      id: Some(id("70")),
      require: vec![(id("1227"), 2)],
      rewards: vec![GiveOption(id("lifesharing"))],
    },
    Quest {
      id: Some(id("71")),
      require: vec![(id("1228"), 1)],
      rewards: vec![GiveFamily(id("1030"))],
    },
    Quest {
      id: Some(id("72")),
      require: vec![
        (id("1229"), 5), (id("1230"), 2), (id("1231"), 7), (id("1232"), 7),
        (id("1233"), 1), (id("1234"), 1), (id("1235"), 1),
      ],
      rewards: vec![GiveOption(id("ninja"))],
    },
    Quest {
      id: Some(id("73")),
      require: vec![(id("1236"), 100)],
      rewards: vec![GiveMode(id("timeattack"))],
    },
    Quest {
      id: Some(id("74")),
      require: vec![(id("1237"), 1)],
      rewards: vec![GiveOption(id("bombexpert")), /* <give log="1"/> */],
    },
    Quest {
      id: Some(id("75")),
      require: vec![(id("1238"), 1)],
      rewards: vec![GiveFamily(id("5014"))],
    },

    // NOTE: Put these quests last: we need to ensure that the families they remove get removed
    // *after* any other quest gave them.
    // TODO: rework quest handling code to not have on rely on the order like that.

    Quest {
      id: Some(id("0")),
      require: vec![
        (id("40"), 1), (id("41"), 1), (id("42"), 1), (id("43"), 1), (id("44"), 1), (id("45"), 1),
        (id("46"), 1), (id("47"), 1), (id("48"), 1), (id("49"), 1), (id("50"), 1), (id("51"), 1),
      ],
      rewards: vec![GiveFamily(id("100")), GiveFamily(id("6")), RemoveFamily(id("5"))],
    },
    Quest {
      id: Some(id("1")),
      require: vec![
        (id("52"), 1), (id("53"), 1), (id("54"), 1), (id("55"), 1), (id("56"), 1), (id("57"), 1),
        (id("58"), 1), (id("59"), 1), (id("60"), 1), (id("61"), 1), (id("62"), 1), (id("63"), 1),
      ],
      rewards: vec![GiveFamily(id("101")), RemoveFamily(id("6"))],
    },
    Quest {
      id: Some(id("2")),
      require: vec![(id("103"), 1)],
      rewards: vec![GiveFamily(id("102")), RemoveFamily(id("7"))],
    },
    Quest {
      id: Some(id("3")),
      require: vec![(id("104"), 1)],
      rewards: vec![GiveFamily(id("103")), GiveFamily(id("9")), RemoveFamily(id("8"))],
    },
    Quest {
      id: Some(id("4")),
      require: vec![(id("105"), 1)],
      rewards: vec![GiveFamily(id("104")), RemoveFamily(id("9"))],
    },
    Quest {
      id: Some(id("30")),
      require: vec![(id("68"), 10)],
      rewards: vec![RemoveFamily(id("13")), GiveFamily(id("14")), GiveFamily(id("106"))],
    },
    Quest {
      id: Some(id("31")),
      require: vec![(id("26"), 10)],
      rewards: vec![RemoveFamily(id("14")), GiveFamily(id("107"))],
    },
    Quest {
      id: Some(id("32")),
      require: vec![(id("109"), 1)],
      rewards: vec![RemoveFamily(id("15")), GiveFamily(id("16")), /* <give tokens="5"/> */],
    },
    Quest {
      id: Some(id("33")),
      require: vec![(id("110"), 2)],
      rewards: vec![RemoveFamily(id("16")), GiveFamily(id("17")), /* <give tokens="10"/> */],
    },
    Quest {
      id: Some(id("34")),
      require: vec![(id("111"), 2)],
      rewards: vec![RemoveFamily(id("17")), /* <give tokens="20"/> */],
    },
    Quest {
      id: Some(id("35")),
      require: vec![(id("10"), 5)],
      rewards: vec![RemoveFamily(id("18")), /* <give bank="25"/> */],
    },
    Quest {
      id: Some(id("52")),
      require: vec![(id("1193"), 1)],
      rewards: vec![GiveFamily(id("5003")), RemoveFamily(id("1021"))],
    },
    Quest {
      id: Some(id("55")),
      require: vec![(id("1196"), 1)],
      rewards: vec![RemoveFamily(id("1022")), GiveFamily(id("5006"))],
    },
    Quest {
      id: Some(id("67")),
      require: vec![(id("1221"), 5), (id("1222"), 10), (id("1223"), 5), (id("1224"), 3)],
      rewards: vec![RemoveFamily(id("1028")), GiveMode(id("multicoop"))],
    },
  ]
}
