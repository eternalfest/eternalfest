use crate::blob::{Blob, BlobDigest, BlobId, BlobIdRef, MediaType};
use crate::core::{Instant, Listing, SimpleSemVer};
use crate::digest::{DigestSha1, DigestSha1FromBytesError, ParseDigestSha1Error};
use crate::game::requests::CreateGameChannel;
use crate::game::store::{
  CreateBuild, CreateBuildError, CreateGameChannelError, CreateStoreGame, CreateStoreGameError, GetStoreGame,
  GetStoreGameError, GetStoreShortGames, GetStoreShortGamesError, StoreSetGameFavorite, StoreSetGameFavoriteError,
  UpdateGameChannelError, UpdateStoreGameChannel,
};
use crate::inventory::{FamilyId, FamilyIdParseError};
use crate::user::ShortUser;
use arrayvec::ArrayString;
use async_trait::async_trait;
use core::fmt;
use eternaltwin_core::core::{LocaleId, PeriodLower};
use eternaltwin_core::types::WeakError;
use eternaltwin_core::user::{UserId, UserIdRef};
use eternaltwin_core::{declare_new_enum, declare_new_string, declare_new_uuid};
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
use indexmap::IndexMap;
pub use serde_json::Value as JsonValue;
#[cfg(feature = "sqlx")]
use sqlx::{database, postgres, Database, Postgres};
use std::collections::{BTreeMap, BTreeSet, HashMap};
use std::ops::Deref;
use std::str::FromStr;
use thiserror::Error;

mod kinds {
  use core::fmt::Debug;
  use core::hash::Hash;
  #[cfg(feature = "serde")]
  use serde::{Deserialize, Serialize};

  pub trait Sealed {}
  impl Sealed for Resolved {}
  impl Sealed for RefsOnly {}

  // TODO: don't have as many traits?
  pub trait Kind: Sealed + Copy + Debug + Eq + Ord + Hash {
    #[cfg(feature = "serde")]
    type Blob: Clone + Debug + Eq + Ord + Hash + for<'de> Deserialize<'de> + Serialize;
    #[cfg(not(feature = "serde"))]
    type Blob: Clone + Debug + Eq + Ord + hash::Hash;

    #[cfg(feature = "serde")]
    type User: Clone + Debug + Eq + for<'de> Deserialize<'de> + Serialize;
    #[cfg(not(feature = "serde"))]
    type User: Clone + Debug + Eq + Ord + hash::Hash;
  }

  #[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
  pub enum Resolved {}

  impl Kind for Resolved {
    type Blob = super::Blob;
    type User = super::ShortUser;
  }

  #[derive(Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
  pub enum RefsOnly {}

  impl Kind for RefsOnly {
    type Blob = super::BlobIdRef;
    type User = super::UserIdRef;
  }
}

pub use kinds::{Kind, RefsOnly, Resolved};

pub mod requests {
  use crate::core::{BoundedVec, Request};
  use crate::game::{
    Game, GameBuild, GameChannelKey, GameChannelPatch, GameKey, GameListItem, GameRef, InputGameBuild,
    InputGameChannel, Resolved,
  };
  use core::num::NonZeroU32;
  use eternaltwin_core::core::{Instant, Listing};
  use eternaltwin_core::types::WeakError;
  use eternaltwin_core::user::UserIdRef;
  #[cfg(feature = "serde")]
  use eternaltwin_serde_tools::{Deserialize, Serialize};

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct GetGames {
    pub offset: u32,
    pub limit: NonZeroU32,
    pub favorite: bool,
    pub time: Option<Instant>,
  }

  impl Request for GetGames {
    /// List of games, with tombstones
    ///
    /// If the user did not have the permission to view the game at time `t`, it
    /// is not in the listing.
    /// If the user had the permission to the game:
    /// - If it still has the permission, game
    /// - If it no longer has the permission (or the game was deleted), tombstone
    ///
    /// A user has the permission to view the game if at the time he:
    /// - is an admin (Eternalfest group)
    /// - is the owner of the game
    /// - can view any of the channels
    ///
    /// If the user could view only channel x in the past and now can only view
    /// channel y, it results in a situation. Where he can view the game but
    /// not the corresponding channels.
    type Response = Result<Listing<GameListItem>, WeakError>;
  }

  #[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct GetGame {
    pub game: GameRef,
    pub channel: Option<GameChannelKey>,
    pub time: Option<Instant>,
  }

  impl Request for GetGame {
    /// Get a game by id or key
    type Response = Result<Game<Resolved>, WeakError>;
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct CreateGame {
    pub owner: Option<UserIdRef>,
    pub key: Option<GameKey>,
    pub build: InputGameBuild,
    pub channels: BoundedVec<InputGameChannel, 1, 10>,
  }

  impl Request for CreateGame {
    /// Create a new game
    type Response = Result<Game<Resolved>, WeakError>;
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct CreateGameBuild {
    pub game: GameRef,
    pub build: InputGameBuild,
  }

  impl Request for CreateGameBuild {
    /// Create a new game build
    type Response = Result<GameBuild<Resolved>, WeakError>;
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct SetGameFavorite {
    pub game: GameRef,
    pub favorite: bool,
  }

  impl Request for SetGameFavorite {
    type Response = Result<bool, WeakError>;
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct CreateGameChannel {
    pub actor: UserIdRef,
    pub game: GameRef,
    pub channel: InputGameChannel,
  }

  impl Request for CreateGameChannel {
    /// Create a new game channel
    type Response = Result<GameChannelKey, WeakError>;
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct UpdateGameChannel {
    pub actor: Option<UserIdRef>,
    pub game: GameRef,
    pub channel_key: GameChannelKey,
    // TODO: Versioning token to detect conflicts
    pub patches: Vec<GameChannelPatch>,
  }

  impl Request for UpdateGameChannel {
    /// Create a new game
    type Response = Result<GameChannelKey, WeakError>;
  }
}

pub mod store {
  use crate::core::{BoundedVec, Instant};
  use crate::game::{GameChannelKey, GameChannelPatch, GameKey, GameRef, InputGameBuild, InputGameChannel};
  use eternaltwin_core::types::WeakError;
  use eternaltwin_core::user::UserIdRef;
  #[cfg(feature = "serde")]
  use eternaltwin_serde_tools::{Deserialize, Serialize};
  use std::num::NonZeroU32;

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct CreateStoreGame {
    pub owner: UserIdRef,
    pub key: Option<GameKey>,
    pub build: InputGameBuild,
    pub channels: BoundedVec<InputGameChannel, 1, 10>,
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum CreateStoreGameError {
    #[error("trying to create a game with an already used key")]
    KeyAlreadyInUse,
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl CreateStoreGameError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct CreateBuild {
    pub game: GameRef,
    /// If `Some(_)`, only create the build if the provided user is an owner of the game
    pub if_owner: Option<UserIdRef>,
    pub build: InputGameBuild,
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum CreateBuildError {
    #[error("the provided user reference {0:?} is not an owner of the game")]
    NotOwner(UserIdRef),
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl CreateBuildError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum CreateGameChannelError {
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl CreateGameChannelError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct UpdateStoreGameChannel {
    pub actor: UserIdRef,
    /// If `Some(_)`, only apply the update if the provided user is an owner of the game
    pub if_owner: Option<UserIdRef>,
    pub game: GameRef,
    pub channel_key: GameChannelKey,
    // TODO: Versioning token to detect conflicts
    pub patches: Vec<GameChannelPatch>,
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum UpdateGameChannelError {
    #[error("no game found for the provided reference {0:?}")]
    GameNotFound(GameRef),
    #[error("no game channel found for the provided reference ({0:?}, {1:?})")]
    GameChannelNotFound(GameRef, GameChannelKey),
    #[error("the provided user reference {0:?} is not an owner of the game")]
    NotOwner(UserIdRef),
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl UpdateGameChannelError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct GetStoreShortGames {
    /// - `Some`: Use user's permissions
    /// - `None`: Guest permissions
    // TODO: Expose `system` access (full permissions)
    pub actor: Option<UserIdRef>,
    pub is_tester: bool,
    pub favorite: bool,
    pub offset: u32,
    pub limit: NonZeroU32,
    pub now: Instant,
    pub time: Option<Instant>,
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum GetStoreShortGamesError {
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl GetStoreShortGamesError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct GetStoreGame {
    pub actor: Option<UserIdRef>,
    pub is_tester: bool,
    pub now: Instant,
    pub time: Option<Instant>,
    pub game: GameRef,
    pub channel: Option<GameChannelKey>,
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum GetStoreGameError {
    #[error("game not found")]
    GameNotFound,
    #[error("revoked view game permission")]
    PermissionRevoked,
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl GetStoreGameError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }

  #[derive(Clone, Debug, PartialEq, Eq)]
  #[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
  pub struct StoreSetGameFavorite {
    pub user: UserIdRef,
    pub game: GameRef,
    pub favorite: bool,
  }

  #[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
  pub enum StoreSetGameFavoriteError {
    #[error("revoked view game permssion")]
    PermissionRevoked,
    #[error(transparent)]
    Other(#[from] WeakError),
  }

  impl StoreSetGameFavoriteError {
    pub fn other<E>(e: E) -> Self
    where
      E: ::std::error::Error + Send + Sync + 'static,
    {
      Self::Other(WeakError::wrap(e))
    }
  }
}

/// A game (Some) or a tombstone (None).
///
/// In the future, the tombstone may provide more details (game id, reason).
/// In particular the reason should allow to know if it was influenced by
/// the future (e.g. permission revocation) or not.
pub type GameListItem = Option<ShortGame<Resolved>>;

// /// A game represents:
// /// - a set of game channels that may share progress and be
// ///   scheduled together
// /// - a set of game versions
// /// - a schedule mapping game versions to channels over time
// /// - an owner
#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "Game"),
  serde(bound = "")
)]
pub struct Game<K: Kind> {
  /// Game id
  pub id: GameId,
  /// Creation time (not necessarily when it was publicly accessible)
  pub created_at: Instant,
  /// Game key (client controlled name)
  pub key: Option<GameKey>,
  /// Owner of the game (full management permissions)
  pub owner: K::User,
  /// The list of channels, restricted to the one with the highest priority
  ///
  /// `channels.limit == 1`
  /// `channels.offset` points to the first non-tombstone entry.
  /// If it is not `0`, it means that the permissions were changed and you can
  /// no longer view the original channel.
  pub channels: GameChannelListing<K>,
}

impl<K: Kind> Game<K> {
  pub const fn as_ref(&self) -> GameIdRef {
    GameIdRef::new(self.id)
  }

  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    self.channels.blobs()
  }

  pub fn users(&self) -> impl Iterator<Item = &K::User> {
    std::iter::once(&self.owner)
  }
}

impl Game<RefsOnly> {
  /// Panics if `blobs` or `users` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>, users: &HashMap<UserId, ShortUser>) -> Game<Resolved> {
    Game {
      id: self.id,
      created_at: self.created_at,
      key: self.key,
      owner: users.get(&self.owner.id).expect("user was resolved").clone(),
      channels: self.channels.resolve(blobs),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct GameChannelListing<K: Kind> {
  pub offset: u32,
  pub limit: u32,
  pub count: u32,
  pub is_count_exact: bool,
  pub active: ActiveGameChannel<K>,
  pub items: Vec<Option<ShortGameChannel<K>>>,
}

impl<K: Kind> GameChannelListing<K> {
  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    let active = self.active.blobs();
    let channels = self.items.iter().flatten().flat_map(ShortGameChannel::blobs);
    active.chain(channels)
  }
}

impl GameChannelListing<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> GameChannelListing<Resolved> {
    GameChannelListing {
      offset: self.offset,
      limit: self.limit,
      count: self.count,
      is_count_exact: self.is_count_exact,
      active: self.active.resolve(blobs),
      items: self.items.into_iter().map(|c| c.map(|c| c.resolve(blobs))).collect(),
    }
  }
}
#[derive(Clone, Debug, PartialEq, Eq)]
#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "GameChannel"),
  serde(bound = "")
)]
pub struct ShortGameChannel<K: Kind> {
  pub key: GameChannelKey,
  pub is_enabled: bool,
  pub is_pinned: bool,
  pub publication_date: Option<Instant>,
  pub sort_update_date: Instant,
  pub default_permission: GameChannelPermission,
  pub build: ShortGameBuild<K>,
}

impl<K: Kind> ShortGameChannel<K> {
  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    self.build.blobs()
  }
}

impl ShortGameChannel<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> ShortGameChannel<Resolved> {
    ShortGameChannel {
      key: self.key,
      is_enabled: self.is_enabled,
      is_pinned: self.is_pinned,
      publication_date: self.publication_date,
      sort_update_date: self.sort_update_date,
      default_permission: self.default_permission,
      build: self.build.resolve(blobs),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct ShortGameBuild<K: Kind> {
  pub version: SimpleSemVer,
  pub git_commit_ref: Option<GitCommitRefSha1>,
  pub main_locale: LocaleId,
  pub display_name: GameDisplayName,
  pub description: GameDescription,
  pub icon: Option<K::Blob>,
  #[cfg_attr(feature = "serde", serde(default))]
  pub i18n: BTreeMap<LocaleId, ShortGameBuildI18n<K>>,
}

impl<K: Kind> ShortGameBuild<K> {
  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    let opt = self.icon.as_ref().into_iter();
    let i18n = self.i18n.values().flat_map(ShortGameBuildI18n::blobs);
    opt.chain(i18n)
  }
}

impl ShortGameBuild<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> ShortGameBuild<Resolved> {
    ShortGameBuild {
      version: self.version,
      git_commit_ref: self.git_commit_ref,
      main_locale: self.main_locale,
      display_name: self.display_name,
      description: self.description,
      icon: self
        .icon
        .map(|blob| blobs.get(&blob.id).expect("icon was resolved").clone()),
      i18n: self
        .i18n
        .into_iter()
        .map(|(l, i18n)| (l, i18n.resolve(blobs)))
        .collect(),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "Game"),
  serde(bound = "")
)]
pub struct ShortGame<K: Kind> {
  /// Game id
  pub id: GameId,
  /// Creation time (not necessarily when it was publicly accessible)
  pub created_at: Instant,
  /// Game key (client controlled name)
  pub key: Option<GameKey>,
  /// Owner of the game (full management permissions)
  pub owner: K::User,
  /// The list of channels, restricted to the one with the highest priority
  ///
  /// `channels.limit == 1`
  /// `channels.offset` points to the first non-tombstone entry.
  /// If it is not `0`, it means that the permissions were changed and you can
  /// no longer view the original channel.
  pub channels: Listing<ShortGameChannel<K>>,
}

impl<K: Kind> ShortGame<K> {
  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    self.channels.items.iter().flat_map(ShortGameChannel::blobs)
  }

  pub fn users(&self) -> impl Iterator<Item = &K::User> {
    std::iter::once(&self.owner)
  }
}

impl ShortGame<RefsOnly> {
  /// Panics if `blobs` or `users` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>, users: &HashMap<UserId, ShortUser>) -> ShortGame<Resolved> {
    ShortGame {
      id: self.id,
      created_at: self.created_at,
      key: self.key,
      owner: users.get(&self.owner.id).expect("user was resolved").clone(),
      channels: self.channels.map(|c| c.resolve(blobs)),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct GameBuild<K: Kind> {
  pub version: SimpleSemVer,
  pub created_at: Instant,
  pub git_commit_ref: Option<GitCommitRefSha1>,
  pub main_locale: LocaleId,
  pub display_name: GameDisplayName,
  pub description: GameDescription,
  pub icon: Option<K::Blob>,
  pub loader: SimpleSemVer,
  pub engine: GameEngine<K>,
  pub patcher: Option<GamePatcher<K>>,
  /// Debug info (e.g. obfuscation map)
  pub debug: Option<K::Blob>,
  pub content: Option<K::Blob>,
  pub content_i18n: Option<K::Blob>,
  pub musics: Vec<GameResource<K>>,
  pub modes: IndexMap<GameModeKey, GameModeSpec>,
  pub families: FamilyList,
  pub category: GameCategory,
  #[cfg_attr(feature = "serde", serde(default))]
  pub i18n: BTreeMap<LocaleId, GameBuildI18n<K>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
pub enum ResolveGameBuildError {
  #[error("missing blob {0:?}")]
  Blob(BlobId),
}

impl GameBuild<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> GameBuild<Resolved> {
    GameBuild {
      version: self.version,
      created_at: self.created_at,
      git_commit_ref: self.git_commit_ref,
      main_locale: self.main_locale,
      display_name: self.display_name,
      description: self.description,
      icon: self.icon.map(|b| blobs.get(&b.id).expect("icon was resolved").clone()),
      loader: self.loader,
      engine: match self.engine {
        GameEngine::V96 => GameEngine::V96,
        GameEngine::Custom(engine) => GameEngine::Custom(CustomGameEngine {
          blob: blobs.get(&engine.blob.id).expect("engine was resolved").clone(),
        }),
      },
      patcher: self.patcher.map(|p| GamePatcher {
        blob: blobs.get(&p.blob.id).expect("patcher was resolved").clone(),
        framework: p.framework,
        meta: p.meta,
      }),
      debug: self
        .debug
        .map(|b| blobs.get(&b.id).expect("debug was resolved").clone()),
      content: self
        .content
        .map(|b| blobs.get(&b.id).expect("content was resolved").clone()),
      content_i18n: self
        .content_i18n
        .map(|b| blobs.get(&b.id).expect("content_i18n was resolved").clone()),
      musics: self
        .musics
        .into_iter()
        .map(|gr| GameResource {
          blob: blobs.get(&gr.blob.id).expect("music was resolved").clone(),
          display_name: gr.display_name,
        })
        .collect(),
      modes: self.modes,
      families: self.families,
      category: self.category,
      i18n: self
        .i18n
        .into_iter()
        .map(|(l, i18n)| (l, i18n.resolve(blobs)))
        .collect(),
    }
  }
}

impl<K: Kind> GameBuild<K> {
  pub fn to_short(&self) -> ShortGameBuild<K> {
    ShortGameBuild {
      version: self.version,
      git_commit_ref: self.git_commit_ref,
      main_locale: self.main_locale,
      display_name: self.display_name.clone(),
      description: self.description.clone(),
      icon: self.icon.clone(),
      i18n: self.i18n.iter().map(|(l, v)| (*l, v.to_short())).collect(),
    }
  }

  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    let opts = [
      self.icon.as_ref(),
      match &self.engine {
        GameEngine::Custom(engine) => Some(&engine.blob),
        GameEngine::V96 => None,
      },
      self.patcher.as_ref().map(|p| &p.blob),
      self.debug.as_ref(),
      self.content.as_ref(),
      self.content_i18n.as_ref(),
    ]
    .into_iter()
    .flatten();

    let musics = self.musics.iter().map(|m| &m.blob);
    let i18n = self.i18n.values().flat_map(GameBuildI18n::blobs);

    opts.chain(musics).chain(i18n)
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct InputGameBuild {
  pub version: SimpleSemVer,
  pub git_commit_ref: Option<GitCommitRefSha1>,
  pub main_locale: LocaleId,
  pub display_name: GameDisplayName,
  pub description: GameDescription,
  pub icon: Option<BlobIdRef>,
  pub loader: SimpleSemVer,
  pub engine: GameEngine<RefsOnly>,
  pub patcher: Option<GamePatcher<RefsOnly>>,
  /// Debug info (e.g. obfuscation map)
  pub debug: Option<BlobIdRef>,
  pub content: Option<BlobIdRef>,
  pub content_i18n: Option<BlobIdRef>,
  pub musics: Vec<GameResource<RefsOnly>>,
  pub modes: IndexMap<GameModeKey, GameModeSpec>,
  pub families: FamilyList,
  pub category: GameCategory,
  #[cfg_attr(feature = "serde", serde(default))]
  pub i18n: BTreeMap<LocaleId, GameBuildI18n<RefsOnly>>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct InputPeriodLower {
  /// If missing, `now`
  pub start: Option<Instant>,
  /// If missing, +inf
  pub end: Option<Instant>,
}

impl InputPeriodLower {
  pub const NOW_TO_FOREVER: Self = Self { start: None, end: None };

  pub fn resolve_from(self, time: Instant) -> PeriodLower {
    let start = self.start.unwrap_or(time);
    PeriodLower::new(start, self.end)
  }
}

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "GameChannel")
)]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GameChannelKeyRef {
  pub key: GameChannelKey,
}

impl GameChannelKeyRef {
  pub const fn new(key: GameChannelKey) -> Self {
    Self { key }
  }
}

#[derive(Clone, Debug, PartialEq, Eq)]
#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "GameChannel"),
  serde(bound = "")
)]
pub struct ActiveGameChannel<K: Kind> {
  pub key: GameChannelKey,
  pub is_enabled: bool,
  pub is_pinned: bool,
  pub publication_date: Option<Instant>,
  pub sort_update_date: Instant,
  pub default_permission: GameChannelPermission,
  pub build: GameBuild<K>,
}

impl<K: Kind> ActiveGameChannel<K> {
  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    self.build.blobs()
  }
}

impl ActiveGameChannel<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> ActiveGameChannel<Resolved> {
    ActiveGameChannel {
      key: self.key,
      is_enabled: self.is_enabled,
      is_pinned: self.is_pinned,
      publication_date: self.publication_date,
      sort_update_date: self.sort_update_date,
      default_permission: self.default_permission,
      build: self.build.resolve(blobs),
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct InputGameChannel {
  pub key: GameChannelKey,
  pub is_enabled: bool,
  pub default_permission: GameChannelPermission,
  pub is_pinned: bool,
  pub publication_date: Option<Instant>,
  /// `None` is `now`
  pub sort_update_date: Option<Instant>,
  pub version: SimpleSemVer,
  pub patches: Vec<GameChannelPatch>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GameChannelPatch {
  pub period: InputPeriodLower,
  pub is_enabled: bool,
  pub default_permission: GameChannelPermission,
  pub is_pinned: bool,
  pub publication_date: Option<Instant>,
  pub sort_update_date: Instant,
  pub version: SimpleSemVer,
}

declare_new_uuid! {
  pub struct GameId(Uuid);
  pub type ParseError = GameIdParseError;
  const SQL_NAME = "game_id";
}

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "Game")
)]
#[derive(Clone, Copy, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameIdRef {
  pub id: GameId,
}

impl GameIdRef {
  pub const fn new(id: GameId) -> Self {
    Self { id }
  }
}

impl From<GameId> for GameIdRef {
  fn from(id: GameId) -> Self {
    Self::new(id)
  }
}

declare_new_string! {
  pub struct GameKey(String);
  pub type ParseError = GameKeyParseError;
  const PATTERN = r"^[a-z][a-z_0-9]{0,99}$";
  const SQL_NAME = "game_key";
}

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "Game")
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameKeyRef {
  pub key: GameKey,
}

impl GameKeyRef {
  pub const fn new(key: GameKey) -> Self {
    Self { key }
  }
}

impl From<GameKey> for GameKeyRef {
  fn from(key: GameKey) -> Self {
    Self { key }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(untagged))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum GameRef {
  Id(GameIdRef),
  Key(GameKeyRef),
}

impl GameRef {
  pub const fn id(id: GameId) -> Self {
    Self::Id(GameIdRef::new(id))
  }

  pub const fn key(key: GameKey) -> Self {
    Self::Key(GameKeyRef::new(key))
  }

  pub const fn split(&self) -> (Option<GameIdRef>, Option<&GameKeyRef>) {
    let mut id: Option<GameIdRef> = None;
    let mut key: Option<&GameKeyRef> = None;
    match self {
      Self::Id(r) => id = Some(*r),
      Self::Key(r) => key = Some(r),
    };
    (id, key)
  }

  pub const fn split_deref(&self) -> (Option<GameId>, Option<&GameKey>) {
    let mut id: Option<GameId> = None;
    let mut key: Option<&GameKey> = None;
    match self {
      Self::Id(r) => id = Some(r.id),
      Self::Key(r) => key = Some(&r.key),
    };
    (id, key)
  }
}

impl<K: Kind> From<&'_ Game<K>> for GameRef {
  fn from(game: &Game<K>) -> Self {
    Self::Id(game.as_ref())
  }
}

impl From<GameIdRef> for GameRef {
  fn from(id: GameIdRef) -> Self {
    Self::Id(id)
  }
}

impl From<GameId> for GameRef {
  fn from(id: GameId) -> Self {
    Self::Id(id.into())
  }
}

impl From<GameKeyRef> for GameRef {
  fn from(key: GameKeyRef) -> Self {
    Self::Key(key)
  }
}

impl From<GameKey> for GameRef {
  fn from(key: GameKey) -> Self {
    Self::Key(key.into())
  }
}

#[derive(Debug, Error)]
#[error("invalid forum section id ({id}) or key ({key})")]
pub struct GameRefParseError {
  pub id: GameIdParseError,
  pub key: GameKeyParseError,
}

impl FromStr for GameRef {
  type Err = GameRefParseError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    match GameId::from_str(s) {
      Ok(r) => Ok(r.into()),
      Err(id) => match GameKey::from_str(s) {
        Ok(r) => Ok(r.into()),
        Err(key) => Err(GameRefParseError { id, key }),
      },
    }
  }
}

declare_new_string! {
  pub struct GameDisplayName(String);
  pub type ParseError = GameDisplayNameError;
  const PATTERN = r"^\S(?:.{0,62}\S)?$";
  const SQL_NAME = "game_display_name";
}

declare_new_string! {
  pub struct GameDescription(String);
  pub type ParseError = GameDescriptionError;
  const PATTERN = r"^\S(?s:.{0,998}\S)?$";
  const SQL_NAME = "game_description";
}

declare_new_string! {
  pub struct GameChannelKey(String);
  pub type ParseError = GameChannelKeyParseError;
  const PATTERN = r"^[a-z_][a-z_0-9]{0,99}$";
  const SQL_NAME = "game_channel_key";
}

declare_new_string! {
  pub struct GameOptionDisplayName(String);
  pub type ParseError = GameOptionDisplayNameError;
  const PATTERN = r"^\S(?:.{0,62}\S)?$";
  const SQL_NAME = "game_option_display_name";
}

declare_new_string! {
  pub struct GameOptionKey(String);
  pub type ParseError = GameOptionKeyParseError;
  const PATTERN = r"^[a-z_][a-z_0-9]{0,99}$";
  const SQL_NAME = "game_option_key";
}

declare_new_string! {
  pub struct GameModeDisplayName(String);
  pub type ParseError = GameModeDisplayNameError;
  const PATTERN = r"^\S(?:.{0,62}\S)?$";
  const SQL_NAME = "game_mode_display_name";
}

declare_new_string! {
  pub struct GameModeKey(String);
  pub type ParseError = GameModeKeyParseError;
  const PATTERN = r"^[a-z_][a-z_0-9]{0,99}$";
  const SQL_NAME = "game_mode_key";
}

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(try_from = "&str", into = "ArrayString<40>")
)]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GitCommitRefSha1(DigestSha1);

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
#[error(transparent)]
pub struct GitCommitRefSha1FromBytesError(#[from] pub DigestSha1FromBytesError);

#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
#[error(transparent)]
pub struct ParseGitCommitRefSha1Error(#[from] pub ParseDigestSha1Error);

impl GitCommitRefSha1 {
  pub fn as_slice(&self) -> &[u8] {
    self.0.as_slice()
  }

  pub fn from_bytes(bytes: &[u8]) -> Result<Self, GitCommitRefSha1FromBytesError> {
    Ok(Self(DigestSha1::from_bytes(bytes)?))
  }

  pub fn from_hex(input: &str) -> Result<Self, ParseGitCommitRefSha1Error> {
    Ok(Self(DigestSha1::from_hex(input)?))
  }

  pub fn hex(&self) -> ArrayString<40> {
    self.0.hex()
  }
}

#[cfg(feature = "sqlx")]
impl sqlx::Type<Postgres> for GitCommitRefSha1 {
  fn type_info() -> postgres::PgTypeInfo {
    postgres::PgTypeInfo::with_name("git_commit_ref")
  }

  fn compatible(ty: &postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info() || <&[u8] as sqlx::Type<Postgres>>::compatible(ty)
  }
}

#[cfg(feature = "sqlx")]
impl<'r, Db: Database> sqlx::Decode<'r, Db> for GitCommitRefSha1
where
  &'r [u8]: sqlx::Decode<'r, Db>,
{
  fn decode(
    value: <Db as database::HasValueRef<'r>>::ValueRef,
  ) -> Result<GitCommitRefSha1, Box<dyn std::error::Error + 'static + Send + Sync>> {
    let value: &[u8] = <&[u8] as sqlx::Decode<Db>>::decode(value)?;
    Ok(GitCommitRefSha1::from_bytes(value)?)
  }
}

#[cfg(feature = "sqlx")]
impl<'q, Db: Database> sqlx::Encode<'q, Db> for GitCommitRefSha1
where
  Vec<u8>: sqlx::Encode<'q, Db>,
{
  fn encode_by_ref(&self, buf: &mut <Db as database::HasArguments<'q>>::ArgumentBuffer) -> sqlx::encode::IsNull {
    self.as_slice().to_vec().encode(buf)
  }
}

impl TryFrom<&[u8]> for GitCommitRefSha1 {
  type Error = GitCommitRefSha1FromBytesError;

  fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
    Self::from_bytes(value)
  }
}

impl TryFrom<&str> for GitCommitRefSha1 {
  type Error = ParseGitCommitRefSha1Error;

  fn try_from(value: &str) -> Result<Self, Self::Error> {
    Self::from_hex(value)
  }
}

impl From<GitCommitRefSha1> for ArrayString<40> {
  fn from(value: GitCommitRefSha1) -> Self {
    value.hex()
  }
}

impl FromStr for GitCommitRefSha1 {
  type Err = ParseGitCommitRefSha1Error;

  fn from_str(input: &str) -> Result<Self, Self::Err> {
    Self::from_hex(input)
  }
}

impl fmt::Display for GitCommitRefSha1 {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    self.hex().fmt(f)
  }
}

declare_new_enum!(
  /// Permissions for a game channel:
  /// - None: No permission at all (can't even see the channel)
  /// - View: View the channel, but can't start runs
  /// - Play: Start regular runs and save results
  /// - Debug: Start debug runs
  /// - Manage: Full permissions, update the channel schedule, etc.
  pub enum GameChannelPermission {
    #[str("None")]
    None,
    #[str("View")]
    View,
    #[str("Play")]
    Play,
    #[str("Debug")]
    Debug,
    #[str("Manage")]
    Manage,
  }
  pub type ParseError = GameChannelPermissionParseError;
  const SQL_NAME = "game_channel_permission";
);

declare_new_enum!(
  pub enum GameCategory {
    #[str("Big")]
    Big,
    #[str("Small")]
    Small,
    #[str("Puzzle")]
    Puzzle,
    #[str("Challenge")]
    Challenge,
    #[str("Fun")]
    Fun,
    #[str("Lab")]
    Lab,
    #[str("Other")]
    Other,
  }
  pub type ParseError = GameCategoryParseError;
  // TODO: Rename to `game_category`
  const SQL_NAME = "game_category2";
);
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameVersion {
  pub major: u32,
  pub minor: u32,
  pub patch: u32,
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct ShortGameBuildI18n<K: Kind> {
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub display_name: Option<GameDisplayName>,
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub description: Option<GameDescription>,
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub icon: Option<K::Blob>,
}

impl<K: Kind> Default for ShortGameBuildI18n<K> {
  fn default() -> Self {
    Self {
      display_name: None,
      description: None,
      icon: None,
    }
  }
}

impl<K: Kind> ShortGameBuildI18n<K> {
  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    self.icon.as_ref().into_iter()
  }
}

impl ShortGameBuildI18n<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> ShortGameBuildI18n<Resolved> {
    ShortGameBuildI18n {
      display_name: self.display_name,
      description: self.description,
      icon: self
        .icon
        .map(|blob| blobs.get(&blob.id).expect("icon was resolved").clone()),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct GameBuildI18n<K: Kind> {
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub display_name: Option<GameDisplayName>,
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub description: Option<GameDescription>,
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub icon: Option<K::Blob>,
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub content_i18n: Option<K::Blob>,
  #[cfg_attr(feature = "serde", serde(default))]
  pub modes: BTreeMap<GameModeKey, GameModeSpecI18n>,
}

impl<K: Kind> Default for GameBuildI18n<K> {
  fn default() -> Self {
    Self {
      display_name: None,
      description: None,
      icon: None,
      content_i18n: None,
      modes: Default::default(),
    }
  }
}

impl<K: Kind> GameBuildI18n<K> {
  pub fn to_short(&self) -> ShortGameBuildI18n<K> {
    ShortGameBuildI18n {
      display_name: self.display_name.clone(),
      description: self.description.clone(),
      icon: self.icon.clone(),
    }
  }

  pub fn blobs(&self) -> impl Iterator<Item = &K::Blob> {
    let opts = [&self.icon, &self.content_i18n];
    opts.into_iter().flat_map(Option::as_ref)
  }
}

impl GameBuildI18n<RefsOnly> {
  /// Panics if `blobs` is missing values present in `self`.
  pub fn resolve(self, blobs: &HashMap<BlobId, Blob>) -> GameBuildI18n<Resolved> {
    GameBuildI18n {
      display_name: self.display_name,
      description: self.description,
      icon: self
        .icon
        .map(|blob| blobs.get(&blob.id).expect("icon was resolved").clone()),
      content_i18n: self
        .content_i18n
        .map(|blob| blobs.get(&blob.id).expect("content_i18n was resolved").clone()),
      modes: self.modes,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GameModeSpec {
  pub display_name: GameModeDisplayName,
  pub is_visible: bool,
  pub options: IndexMap<GameOptionKey, GameOptionSpec>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct GameModeSpecI18n {
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub display_name: Option<GameModeDisplayName>,
  #[cfg_attr(feature = "serde", serde(default))]
  pub options: BTreeMap<GameOptionKey, GameOptionSpecI18n>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct GameOptionSpec {
  pub display_name: GameOptionDisplayName,
  pub is_visible: bool,
  pub is_enabled: bool,
  pub default_value: bool,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, Default)]
pub struct GameOptionSpecI18n {
  #[cfg_attr(feature = "serde", serde(skip_serializing_if = "Option::is_none"))]
  pub display_name: Option<GameOptionDisplayName>,
}

declare_new_uuid! {
  pub struct GameRevisionId(Uuid);
  pub type ParseError = GameRevisionIdParseError;
  const SQL_NAME = "game_build_id";
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(tag = "type"))]
#[cfg_attr(feature = "serde", serde(bound = ""))]
pub enum GameEngine<K: Kind> {
  /// Use Motion-Twin's latest game engine (version 96).
  V96,
  /// Use a custom game engine.
  Custom(CustomGameEngine<K>),
}

impl<K: Kind> GameEngine<K> {
  pub const fn custom(blob: K::Blob) -> Self {
    Self::Custom(CustomGameEngine { blob })
  }

  pub fn as_custom(&self) -> Option<&CustomGameEngine<K>> {
    match self {
      Self::V96 => None,
      Self::Custom(engine) => Some(engine),
    }
  }
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct CustomGameEngine<K: Kind> {
  pub blob: K::Blob,
}

declare_new_string! {
  pub struct GameResourceDisplayName(String);
  pub type ParseError = GameFileDisplayNameError;
  // language=regexp
  const PATTERN = r"^\S(?:.{0,98}\S)?$";
  const SQL_NAME = "game_resource_display_name";
}

#[derive(Clone, Debug, PartialEq, Eq)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct GamePatcher<K: Kind> {
  pub blob: K::Blob,
  pub framework: PatcherFramework,
  /// Optional metadata about this build (e.g. dependencies)
  pub meta: Option<JsonValue>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct PatcherFramework {
  pub name: PatcherFrameworkName,
  pub version: SimpleSemVer,
}

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(bound = ""))]
pub struct GameResource<K: Kind> {
  pub blob: K::Blob,
  pub display_name: Option<GameResourceDisplayName>,
}

/// Game assets have deduplicated key, but the download URL must go through a game channel for permissions
/// /api/games/<gameId>/channel/<channelId>/assets/<key>/raw
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GameAsset {
  /// The key is generated by the server based on data
  pub key: String,
  pub media_type: MediaType,
  pub byte_size: u32,
  pub digest: BlobDigest,
  pub display_name: Option<GameResourceDisplayName>,
}

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct FamilyList(pub BTreeSet<FamilyId>);

impl FromStr for FamilyList {
  type Err = FamilyIdParseError;

  fn from_str(s: &str) -> Result<Self, Self::Err> {
    s.split_terminator(',')
      .map(FamilyId::from_str)
      .collect::<Result<_, _>>()
      .map(Self)
  }
}

impl fmt::Display for FamilyList {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    let mut sep = "";
    for id in &self.0 {
      sep.fmt(f)?;
      id.with_str(|s| s.fmt(f))?;
      sep = ",";
    }
    Ok(())
  }
}

// TODO: we should deduplicate this by providing a `impl_serde_from_str` macro in `serde_tools`
#[cfg(feature = "serde")]
impl serde::Serialize for FamilyList {
  fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
  where
    S: serde::Serializer,
  {
    serializer.serialize_str(&self.to_string())
  }
}

#[cfg(feature = "serde")]
impl<'de> serde::Deserialize<'de> for FamilyList {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    struct Visitor;
    impl<'de> serde::de::Visitor<'de> for Visitor {
      type Value = FamilyList;

      fn expecting(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        fmt.write_str("a string for a valid FamilyList")
      }

      fn visit_str<E: serde::de::Error>(self, value: &str) -> ::std::result::Result<Self::Value, E> {
        value.parse().map_err(E::custom)
      }
    }

    deserializer.deserialize_str(Visitor)
  }
}

// TODO: we should deduplicate this by providing a `impl_sqlx_from_str` macro in `postgres_tools`
#[cfg(feature = "sqlx")]
impl sqlx::Type<sqlx::Postgres> for FamilyList {
  fn type_info() -> sqlx::postgres::PgTypeInfo {
    sqlx::postgres::PgTypeInfo::with_name("families_string")
  }

  fn compatible(ty: &sqlx::postgres::PgTypeInfo) -> bool {
    *ty == Self::type_info() || <&str as sqlx::Type<::sqlx::Postgres>>::compatible(ty)
  }
}

#[cfg(feature = "sqlx")]
impl<'r, Db: sqlx::Database> sqlx::Decode<'r, Db> for FamilyList
where
  &'r str: sqlx::Decode<'r, Db>,
{
  fn decode(
    value: <Db as sqlx::database::HasValueRef<'r>>::ValueRef,
  ) -> std::result::Result<Self, Box<dyn std::error::Error + 'static + Send + Sync>> {
    let value: &str = <&str as sqlx::Decode<Db>>::decode(value)?;
    Ok(value.parse()?)
  }
}

// Can't implement generically over `sqlx::Database` because of lifetime issues.
#[cfg(feature = "sqlx")]
impl sqlx::Encode<'_, sqlx::Postgres> for FamilyList {
  fn encode_by_ref(&self, buf: &mut sqlx::postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
    self.to_string().encode(buf)
  }
}

declare_new_string! {
  pub struct PatcherFrameworkName(String);
  pub type ParseError = PatcherFrameworkNameError;
  const PATTERN = r"^[a-z][a-z0-9]{0,31}$";
  const SQL_NAME = "patcher_framework_name";
}

pub struct GameQuests<'a> {
  pub default_families: FamilyList,
  pub quests: &'a [crate::inventory::Quest],
  pub uses_archived_hfest_items: bool,
}

#[async_trait]
pub trait GameStore: Send + Sync {
  async fn create_game(&self, options: &CreateStoreGame) -> Result<Game<RefsOnly>, CreateStoreGameError>;

  async fn create_version(&self, options: &CreateBuild) -> Result<GameBuild<RefsOnly>, CreateBuildError>;

  async fn create_game_channel(
    &self,
    options: &CreateGameChannel,
  ) -> Result<ShortGameChannel<RefsOnly>, CreateGameChannelError>;

  async fn update_game_channel(
    &self,
    options: &UpdateStoreGameChannel,
  ) -> Result<(ActiveGameChannel<RefsOnly>, Instant), UpdateGameChannelError>;

  async fn get_short_games(
    &self,
    options: &GetStoreShortGames,
  ) -> Result<Listing<Option<ShortGame<RefsOnly>>>, GetStoreShortGamesError>;

  async fn get_game(&self, options: &GetStoreGame) -> Result<Game<RefsOnly>, GetStoreGameError>;

  async fn set_game_favorite(&self, options: &StoreSetGameFavorite) -> Result<bool, StoreSetGameFavoriteError>;

  async fn get_game_quests(&self, game: GameId, version: SimpleSemVer) -> Result<GameQuests<'_>, WeakError>;
}

/// Like [`Deref`], but the target has the bound [`GameStore`]
pub trait GameStoreRef: Send + Sync {
  type GameStore: GameStore + ?Sized;

  fn game_store(&self) -> &Self::GameStore;
}

impl<TyRef> GameStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: GameStore,
{
  type GameStore = TyRef::Target;

  fn game_store(&self) -> &Self::GameStore {
    self.deref()
  }
}

#[cfg(test)]
mod test {
  use crate::game::requests::CreateGame;
  use crate::game::InputGameBuild;
  use crate::game::Resolved;
  use std::fs;

  mod create_game {
    mod sous_la_colline {
      use super::super::*;
      use crate::blob::BlobIdRef;
      use crate::core::BoundedVec;
      use crate::game::{
        GameBuildI18n, GameCategory, GameChannelPermission, GameEngine, GameModeSpec, GameModeSpecI18n, GameOptionSpec,
        GameOptionSpecI18n, InputGameChannel,
      };
      use eternaltwin_core::core::LocaleId;
      use eternaltwin_core::user::UserIdRef;

      fn value() -> CreateGame {
        CreateGame {
          owner: Some(UserIdRef::new("00000000-0000-0000-0001-000000000001".parse().unwrap())),
          key: Some("hill".parse().unwrap()),
          build: InputGameBuild {
            version: "1.2.3".parse().unwrap(),
            git_commit_ref: Some("0123456789abcdef0123456789abcdef01234567".parse().unwrap()),
            main_locale: LocaleId::FrFr,
            display_name: "Sous la colline".parse().unwrap(),
            description: "Contrée emblématique d'Eternalfest qui ravira les débutants et nostalgiques\u{202f}!"
              .parse()
              .unwrap(),
            icon: Some(BlobIdRef::new("00000000-0000-0000-0002-000000000001".parse().unwrap())),
            loader: "4.1.0".parse().unwrap(),
            engine: GameEngine::V96,
            patcher: None,
            debug: None,
            content: None,
            content_i18n: Some(BlobIdRef::new("00000000-0000-0000-0002-000000000002".parse().unwrap())),
            musics: vec![],
            modes: [
              (
                "solo".parse().unwrap(),
                GameModeSpec {
                  display_name: "Aventure".parse().unwrap(),
                  is_visible: true,
                  options: [(
                    "boost".parse().unwrap(),
                    GameOptionSpec {
                      display_name: "Tornade".parse().unwrap(),
                      is_visible: true,
                      is_enabled: true,
                      default_value: false,
                    },
                  )]
                  .into_iter()
                  .collect(),
                },
              ),
              (
                "multi".parse().unwrap(),
                GameModeSpec {
                  display_name: "Multicoopératif".parse().unwrap(),
                  is_visible: true,
                  options: [(
                    "lifesharing".parse().unwrap(),
                    GameOptionSpec {
                      display_name: "Partage de vies".parse().unwrap(),
                      is_visible: true,
                      is_enabled: true,
                      default_value: false,
                    },
                  )]
                  .into_iter()
                  .collect(),
                },
              ),
            ]
            .into_iter()
            .collect(),
            families: "10".parse().unwrap(),
            category: GameCategory::Small,
            i18n: [(
              LocaleId::EnUs,
              GameBuildI18n {
                display_name: Some("Under the hill".parse().unwrap()),
                description: Some(
                  "Hallmark Eternalfest game for beginners and nostalgics\u{202f}!"
                    .parse()
                    .unwrap(),
                ),
                icon: None,
                content_i18n: Some(BlobIdRef::new("00000000-0000-0000-0002-000000000002".parse().unwrap())),
                modes: [
                  (
                    "solo".parse().unwrap(),
                    GameModeSpecI18n {
                      display_name: Some("Adventure".parse().unwrap()),
                      options: [(
                        "boost".parse().unwrap(),
                        GameOptionSpecI18n {
                          display_name: Some("Tornado".parse().unwrap()),
                        },
                      )]
                      .into_iter()
                      .collect(),
                    },
                  ),
                  (
                    "multi".parse().unwrap(),
                    GameModeSpecI18n {
                      display_name: Some("Multiplayer".parse().unwrap()),
                      options: [(
                        "lifesharing".parse().unwrap(),
                        GameOptionSpecI18n {
                          display_name: Some("Life sharing".parse().unwrap()),
                        },
                      )]
                      .into_iter()
                      .collect(),
                    },
                  ),
                ]
                .into_iter()
                .collect(),
              },
            )]
            .into_iter()
            .collect(),
          },
          channels: BoundedVec::new(vec![InputGameChannel {
            key: "main".parse().unwrap(),
            is_enabled: false,
            default_permission: GameChannelPermission::None,
            is_pinned: false,
            publication_date: None,
            sort_update_date: None,
            version: "1.2.3".parse().unwrap(),
            patches: vec![],
          }])
          .unwrap(),
        }
      }

      #[cfg(feature = "serde")]
      #[test]
      fn read() {
        let s = fs::read_to_string("../../test-resources/core/game/create-game/sous-la-colline/value.json").unwrap();
        let actual: CreateGame = serde_json::from_str(&s).unwrap();
        let expected = value();
        assert_eq!(actual, expected);
      }

      #[cfg(feature = "serde")]
      #[test]
      fn write() {
        let value = value();
        let actual: String = serde_json::to_string_pretty(&value).unwrap();
        let expected =
          fs::read_to_string("../../test-resources/core/game/create-game/sous-la-colline/value.json").unwrap();
        assert_eq!(&actual, expected.trim());
      }
    }
  }

  mod game {
    mod beta {
      use super::super::*;
      use crate::game::{
        ActiveGameChannel, Game, GameBuild, GameCategory, GameChannelListing, GameChannelPermission, GameEngine,
        GameId, ShortGameBuild, ShortGameChannel,
      };
      use crate::user::ShortUser;
      use eternaltwin_core::core::{Instant, LocaleId};
      use eternaltwin_core::user::UserId;
      use std::collections::BTreeMap;
      use std::str::FromStr;

      fn value() -> Game<Resolved> {
        Game {
          id: GameId::from_str("00000000-0000-0000-0003-000000000002").unwrap(),
          created_at: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
          key: None,
          owner: ShortUser {
            id: UserId::from_str("00000000-0000-0000-0001-000000000001").unwrap(),
            display_name: "Alice".parse().unwrap(),
          },
          channels: GameChannelListing {
            offset: 0,
            limit: 10,
            count: 3,
            is_count_exact: false,
            active: ActiveGameChannel {
              key: "beta".parse().unwrap(),
              is_enabled: true,
              is_pinned: false,
              publication_date: None,
              sort_update_date: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
              default_permission: GameChannelPermission::None,
              build: GameBuild {
                version: "0.1.2".parse().unwrap(),
                created_at: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
                git_commit_ref: None,
                main_locale: LocaleId::FrFr,
                display_name: "Beta".parse().unwrap(),
                description: "Beta game".parse().unwrap(),
                icon: None,
                loader: "4.1.0".parse().unwrap(),
                engine: GameEngine::V96,
                patcher: None,
                debug: None,
                content: None,
                content_i18n: None,
                musics: vec![],
                modes: Default::default(),
                families: "1,2,3".parse().unwrap(),
                category: GameCategory::Big,
                i18n: BTreeMap::new(),
              },
            },
            items: vec![
              None,
              Some(ShortGameChannel {
                key: "beta".parse().unwrap(),
                is_enabled: true,
                is_pinned: false,
                publication_date: None,
                sort_update_date: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
                default_permission: GameChannelPermission::None,
                build: ShortGameBuild {
                  version: "0.1.2".parse().unwrap(),
                  git_commit_ref: None,
                  main_locale: LocaleId::FrFr,
                  display_name: "Beta".parse().unwrap(),
                  description: "Beta game".parse().unwrap(),
                  icon: None,
                  i18n: BTreeMap::new(),
                },
              }),
              Some(ShortGameChannel {
                key: "dev".parse().unwrap(),
                is_enabled: true,
                is_pinned: false,
                publication_date: None,
                sort_update_date: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
                default_permission: GameChannelPermission::None,
                build: ShortGameBuild {
                  version: "0.1.2".parse().unwrap(),
                  git_commit_ref: None,
                  main_locale: LocaleId::FrFr,
                  display_name: "Beta".parse().unwrap(),
                  description: "Beta game".parse().unwrap(),
                  icon: None,
                  i18n: BTreeMap::new(),
                },
              }),
            ],
          },
        }
      }

      #[cfg(feature = "serde")]
      #[test]
      fn read() {
        let s = fs::read_to_string("../../test-resources/core/game/game/beta/value.json").unwrap();
        let actual: Game<Resolved> = serde_json::from_str(&s).unwrap();
        let expected = value();
        assert_eq!(actual, expected);
      }

      #[cfg(feature = "serde")]
      #[test]
      fn write() {
        let value = value();
        let actual: String = serde_json::to_string_pretty(&value).unwrap();
        let expected = fs::read_to_string("../../test-resources/core/game/game/beta/value.json").unwrap();
        assert_eq!(&actual, expected.trim());
      }
    }
  }

  mod short_game_listing {
    mod alpha_beta {
      use super::super::*;
      use crate::core::Listing;
      use crate::game::{GameChannelPermission, GameId, ShortGame, ShortGameBuild, ShortGameChannel};
      use crate::user::ShortUser;
      use eternaltwin_core::core::{Instant, LocaleId};
      use eternaltwin_core::user::UserId;
      use std::collections::BTreeMap;
      use std::num::NonZeroU32;
      use std::str::FromStr;

      fn value() -> Listing<Option<ShortGame<Resolved>>> {
        Listing {
          offset: 0,
          limit: NonZeroU32::new(10).expect("constant value is non-zero"),
          count: 3,
          is_count_exact: false,
          items: vec![
            Some(ShortGame {
              id: GameId::from_str("00000000-0000-0000-0003-000000000001").unwrap(),
              created_at: Instant::ymd_hms(2022, 1, 1, 0, 0, 1),
              key: None,
              owner: ShortUser {
                id: UserId::from_str("00000000-0000-0000-0001-000000000001").unwrap(),
                display_name: "Alice".parse().unwrap(),
              },
              channels: Listing {
                offset: 0,
                limit: NonZeroU32::new(1).expect("constant value is non-zero"),
                count: 2,
                is_count_exact: false,
                items: vec![ShortGameChannel {
                  key: "main".parse().unwrap(),
                  is_enabled: true,
                  is_pinned: false,
                  publication_date: None,
                  sort_update_date: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
                  default_permission: GameChannelPermission::None,
                  build: ShortGameBuild {
                    version: "1.2.3".parse().unwrap(),
                    git_commit_ref: None,
                    main_locale: LocaleId::FrFr,
                    display_name: "Alpha".parse().unwrap(),
                    description: "Alpha game".parse().unwrap(),
                    icon: None,
                    i18n: BTreeMap::new(),
                  },
                }],
              },
            }),
            Some(ShortGame {
              id: GameId::from_str("00000000-0000-0000-0003-000000000002").unwrap(),
              created_at: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
              key: None,
              owner: ShortUser {
                id: UserId::from_str("00000000-0000-0000-0001-000000000001").unwrap(),
                display_name: "Alice".parse().unwrap(),
              },
              channels: Listing {
                offset: 1,
                limit: NonZeroU32::new(1).expect("constant value is non-zero"),
                count: 3,
                is_count_exact: false,
                items: vec![ShortGameChannel {
                  key: "beta".parse().unwrap(),
                  is_enabled: true,
                  is_pinned: false,
                  publication_date: None,
                  sort_update_date: Instant::ymd_hms(2022, 1, 1, 0, 0, 2),
                  default_permission: GameChannelPermission::None,
                  build: ShortGameBuild {
                    version: "0.1.2".parse().unwrap(),
                    git_commit_ref: None,
                    main_locale: LocaleId::FrFr,
                    display_name: "Beta".parse().unwrap(),
                    description: "Beta game".parse().unwrap(),
                    icon: None,
                    i18n: BTreeMap::new(),
                  },
                }],
              },
            }),
            None,
          ],
        }
      }

      #[cfg(feature = "serde")]
      #[test]
      fn read() {
        let s = fs::read_to_string("../../test-resources/core/game/short-game-listing/alpha-beta/value.json").unwrap();
        let actual: Listing<Option<ShortGame<Resolved>>> = serde_json::from_str(&s).unwrap();
        let expected = value();
        assert_eq!(actual, expected);
      }

      #[cfg(feature = "serde")]
      #[test]
      fn write() {
        let value = value();
        let actual: String = serde_json::to_string_pretty(&value).unwrap();
        let expected =
          fs::read_to_string("../../test-resources/core/game/short-game-listing/alpha-beta/value.json").unwrap();
        assert_eq!(&actual, expected.trim());
      }
    }
  }

  mod game_description {
    mod ololzd2 {
      use super::super::*;
      use crate::game::GameDescription;
      use std::str::FromStr;

      fn value() -> GameDescription {
        GameDescription::from_str("Suite à la contrée au succès mondial et acclamée par toutes les critiques, découvrez Ololzd2, la suite ambitieuse qui va causer débats et controverse sur le lore !\nVoyagez avec Igor à travers les différentes timelines du monde d'Ololzd pour comprendre quels sont les événements qui ont conduit aux conflits observés dans Ololzd.\n\nArriverez-vous à recréer le sceau d'Ololzd pour prévenir le cataclysme qui a englouti Ololzd, ou échouerez-vous dans cette quête, ce qui lancera ce monde dans la course inévitable qui amènera à sa perte ?\n\nSeul vous êtes capable d'une telle prouesse... Il est désormais temps de braver le terrible labyrinthe d'Ololzd2 !\n\n(Ou vous pouvez appuyer sur la touche 'k' si la pression d'une telle mission est trop grande)").unwrap()
      }

      #[cfg(feature = "serde")]
      #[test]
      fn read() {
        let s = fs::read_to_string("../../test-resources/core/game/game-description/ololzd2/value.json").unwrap();
        let actual: GameDescription = serde_json::from_str(&s).unwrap();
        let expected = value();
        assert_eq!(actual, expected);
      }

      #[cfg(feature = "serde")]
      #[test]
      fn write() {
        let value = value();
        let actual: String = serde_json::to_string_pretty(&value).unwrap();
        let expected =
          fs::read_to_string("../../test-resources/core/game/game-description/ololzd2/value.json").unwrap();
        assert_eq!(&actual, expected.trim());
      }
    }
  }
}
