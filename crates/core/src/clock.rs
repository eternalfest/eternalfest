pub use eternaltwin_core::clock::{
  Clock, ClockRef, DynScheduler, ErasedScheduler, Scheduler, SchedulerRef, SystemClock, VirtualClock,
};
