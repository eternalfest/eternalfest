use crate::core::Instant;

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct EternalfestOauthStateClaims {
  /**
   * String used for XSRF protection.
   */
  #[cfg_attr(feature = "serde", serde(rename = "rfp"))]
  pub request_forgery_protection: String,
  #[cfg_attr(feature = "serde", serde(rename = "a"))]
  pub action: EternalfestOauthStateAction,
  #[cfg_attr(
    feature = "serde",
    serde(rename = "iat", with = "eternaltwin_core::oauth::serde_posix_timestamp")
  )]
  pub issued_at: Instant,
  #[cfg_attr(feature = "serde", serde(rename = "as"))]
  pub authorization_server: String,
  #[cfg_attr(
    feature = "serde",
    serde(rename = "exp", with = "eternaltwin_core::oauth::serde_posix_timestamp")
  )]
  pub expiration_time: Instant,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", serde(tag = "type"))]
pub enum EternalfestOauthStateAction {
  #[cfg_attr(feature = "serde", serde(rename = "Login"))]
  Login,
}
