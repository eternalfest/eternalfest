use crate::core::Instant;
use crate::types::WeakError;
use crate::user::{ShortUser, User, UserIdRef};
use async_trait::async_trait;
use core::ops::Deref;
use eternaltwin_core::{declare_new_enum, declare_new_uuid};
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize), serde(tag = "type"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum AuthContext {
  System(SystemAuthContext),
  Guest(GuestAuthContext),
  User(UserAuthContext),
}

impl AuthContext {
  pub const fn guest() -> Self {
    Self::Guest(GuestAuthContext {
      scope: AuthScope::Default,
    })
  }

  /// If the auth context corresponds to a user, return it; otherwise return `None`.
  pub const fn as_user(&self) -> Option<&UserAuthContext> {
    match self {
      Self::User(acx) => Some(acx),
      _ => None,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "System"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct SystemAuthContext {}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "Guest"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GuestAuthContext {
  pub scope: AuthScope,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[cfg_attr(feature = "serde", serde(tag = "type", rename = "User"))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserAuthContext {
  pub scope: AuthScope,
  pub user: ShortUser,
  pub is_administrator: bool,
  pub is_tester: bool,
}

declare_new_enum!(
  pub enum AuthScope {
    #[str("Default")]
    Default,
  }
  pub type ParseError = AuthScopeParseError;
);

declare_new_uuid! {
  pub struct SessionId(Uuid);
  pub type ParseError = SessionIdParseError;
  const SQL_NAME = "session_id";
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CreateSessionOptions {
  pub user: UserIdRef,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawSession {
  pub id: SessionId,
  pub user: UserIdRef,
  pub created_at: Instant,
  pub updated_at: Instant,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct Session {
  pub id: SessionId,
  // TODO: ShortUser
  pub user: UserIdRef,
  pub created_at: Instant,
  pub updated_at: Instant,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserAndSession {
  pub user: User,
  pub is_administrator: bool,
  pub is_tester: bool,
  pub session: Session,
}

impl UserAndSession {
  pub fn to_auth_context(&self) -> UserAuthContext {
    UserAuthContext {
      scope: AuthScope::Default,
      user: self.user.to_short(),
      is_administrator: self.is_administrator,
      is_tester: self.is_tester,
    }
  }
}

#[async_trait]
pub trait AuthStore: Send + Sync {
  async fn create_session(&self, options: &CreateSessionOptions) -> Result<RawSession, WeakError>;

  async fn get_and_touch_session(&self, session: SessionId) -> Result<Option<RawSession>, WeakError>;
}

/// Like [`Deref`], but the target has the bound [`AuthStore`]
pub trait AuthStoreRef: Send + Sync {
  type AuthStore: AuthStore + ?Sized;

  fn auth_store(&self) -> &Self::AuthStore;
}

impl<TyRef> AuthStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: AuthStore,
{
  type AuthStore = TyRef::Target;

  fn auth_store(&self) -> &Self::AuthStore {
    self.deref()
  }
}
