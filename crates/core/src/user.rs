use crate::core::Instant;
use async_trait::async_trait;
use eternaltwin_core::types::WeakError;
pub use eternaltwin_core::user::UserDisplayName;
pub use eternaltwin_core::user::UserId;
pub use eternaltwin_core::user::UserIdParseError;
pub use eternaltwin_core::user::UserIdRef;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::ops::Deref;

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "User")
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct User {
  pub id: UserId,
  pub display_name: UserDisplayName,
  pub created_at: Instant,
  pub updated_at: Instant,
  pub is_administrator: bool,
  pub is_tester: bool,
}

impl User {
  pub fn to_short(&self) -> ShortUser {
    ShortUser {
      id: self.id,
      display_name: self.display_name.clone(),
    }
  }
}

#[cfg_attr(
  feature = "serde",
  derive(Serialize, Deserialize),
  serde(tag = "type", rename = "User")
)]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ShortUser {
  pub id: UserId,
  pub display_name: UserDisplayName,
}

impl ShortUser {
  pub const fn as_ref(&self) -> UserIdRef {
    UserIdRef::new(self.id)
  }
}

impl From<eternaltwin_core::user::ShortUser> for ShortUser {
  fn from(user: eternaltwin_core::user::ShortUser) -> Self {
    Self {
      id: user.id,
      display_name: user.display_name.current.value,
    }
  }
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UserListing {
  pub offset: u32,
  pub limit: u32,
  pub count: u32,
  pub items: Vec<User>,
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct RawGetUserOptions {
  pub id: UserId,
  pub now: Instant,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum StoreUpsertFromEtwinError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum RawGetUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct GetUsersOptions {
  pub id: HashSet<UserId>,
  pub now: Instant,
  pub time: Option<Instant>,
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum StoreGetShortUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum StoreGetShortUsersError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum StoreGetUsersError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum StoreUpdateUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct UpdateUserOptions {
  pub user_id: UserId,
  pub is_tester: bool,
}

#[async_trait]
pub trait UserStore: Send + Sync {
  async fn upsert_from_etwin(&self, user: &ShortUser) -> Result<User, StoreUpsertFromEtwinError>;

  async fn get_user(&self, options: &RawGetUserOptions) -> Result<User, RawGetUserError>;

  async fn get_short_user(&self, options: &RawGetUserOptions) -> Result<ShortUser, StoreGetShortUserError>;

  async fn get_short_users(
    &self,
    options: &GetUsersOptions,
  ) -> Result<HashMap<UserId, ShortUser>, StoreGetShortUsersError>;

  async fn get_users(&self) -> Result<UserListing, StoreGetUsersError>;

  async fn update_user(&self, options: &UpdateUserOptions) -> Result<User, StoreUpdateUserError>;
}

/// Like [`Deref`], but the target has the bound [`UserStore`]
pub trait UserStoreRef: Send + Sync {
  type UserStore: UserStore + ?Sized;

  fn user_store(&self) -> &Self::UserStore;
}

impl<TyRef> UserStoreRef for TyRef
where
  TyRef: Deref + Send + Sync,
  TyRef::Target: UserStore,
{
  type UserStore = TyRef::Target;

  fn user_store(&self) -> &Self::UserStore {
    self.deref()
  }
}
