use arrayvec::ArrayString;
use core::fmt;
#[cfg(feature = "sqlx")]
use sqlx::{database, postgres, Database, Postgres};
use std::error::Error;
use std::str::FromStr;
use thiserror::Error;

// TODO: move this to eternaltwin_serde_tools?
#[cfg(feature = "serde")]
struct StringVisitorFn<'a, V, E> {
  expected: &'a str,
  visitor: &'a (dyn Fn(&str) -> Result<V, E> + 'a),
}

impl<'de, V, E: Error> serde::de::Visitor<'de> for StringVisitorFn<'_, V, E> {
  type Value = V;

  #[inline(always)]
  fn expecting(&self, f: &mut fmt::Formatter) -> std::fmt::Result {
    f.write_str(self.expected)
  }

  #[inline(always)]
  fn visit_borrowed_str<EE: serde::de::Error>(self, v: &'de str) -> Result<Self::Value, EE> {
    (self.visitor)(v).map_err(|e| EE::custom(e))
  }
}

macro_rules! define_hex_digest {
  (
    name = $name:literal,
    sql_name = $sql_name:literal,
    ty = $ty:ident,
    ty_byte_error = $ty_byte_error:ident,
    ty_parse_error = $ty_parse_error:ident,
    byte_len = $byte_len:literal,
    hex_len = $hex_len:literal,
  ) => {
    const _: () = assert!($byte_len * 2 == $hex_len);

    #[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct $ty([u8; $byte_len]);

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
    #[error("invalid {} digest size, expected = {}, actual = {0}", $name, $byte_len)]
    pub struct $ty_byte_error(pub usize);

    #[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash, Error)]
    pub enum $ty_parse_error {
      #[error("wrong length, expected {} hex characters as input, actual = {0}", $hex_len)]
      Length(usize),
      #[error("input is not a lowercase hex string: found {character:?} at offset {offset}")]
      InvalidCharacter { character: char, offset: usize },
    }

    impl $ty {
      pub fn as_slice(&self) -> &[u8] {
        &self.0
      }

      pub fn from_bytes(bytes: &[u8]) -> Result<Self, $ty_byte_error> {
        match <[u8; $byte_len]>::try_from(bytes) {
          Ok(a) => Ok(Self(a)),
          Err(_) => Err($ty_byte_error(bytes.len())),
        }
      }

      pub fn from_hex(input: &str) -> Result<Self, $ty_parse_error> {
        let mut bytes = [0u8; $byte_len];
        match hex::decode_to_slice(input, &mut bytes) {
          Ok(()) => Ok(Self(bytes)),
          Err(hex::FromHexError::InvalidHexCharacter { c, index }) => Err($ty_parse_error::InvalidCharacter {
            character: c,
            offset: index,
          }),
          Err(hex::FromHexError::OddLength | hex::FromHexError::InvalidStringLength) => {
            Err($ty_parse_error::Length(input.len()))
          }
        }
      }

      pub fn hex(&self) -> ArrayString<$hex_len> {
        let mut out = [0u8; $hex_len];
        hex::encode_to_slice(self.0, &mut out).expect("encoding to hex always succeeds");
        ArrayString::from_byte_string(&out).expect("converting the hex byte string to an array string always succeeds")
      }
    }

    impl TryFrom<&[u8]> for $ty {
      type Error = $ty_byte_error;

      fn try_from(value: &[u8]) -> Result<Self, Self::Error> {
        Self::from_bytes(value)
      }
    }

    impl TryFrom<&str> for $ty {
      type Error = $ty_parse_error;

      fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::from_hex(value)
      }
    }

    impl From<$ty> for ArrayString<$hex_len> {
      fn from(value: $ty) -> Self {
        value.hex()
      }
    }

    impl FromStr for $ty {
      type Err = $ty_parse_error;

      fn from_str(input: &str) -> Result<Self, Self::Err> {
        Self::from_hex(input)
      }
    }

    impl fmt::Debug for $ty {
      fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_tuple(stringify!($ty)).field(&self.hex()).finish()
      }
    }

    impl fmt::Display for $ty {
      fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        fmt::Display::fmt(&self.hex(), f)
      }
    }

    #[cfg(feature = "serde")]
    impl serde::Serialize for $ty {
      fn serialize<S: serde::Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
        self.hex().serialize(serializer)
      }
    }

    #[cfg(feature = "serde")]
    impl<'de> serde::Deserialize<'de> for $ty {
      fn deserialize<D: serde::Deserializer<'de>>(deserializer: D) -> Result<Self, D::Error> {
        deserializer.deserialize_str(StringVisitorFn {
          expected: concat!("a ", $name, " digest"),
          visitor: &Self::from_hex,
        })
      }
    }

    #[cfg(feature = "sqlx")]
    impl sqlx::Type<Postgres> for $ty {
      fn type_info() -> postgres::PgTypeInfo {
        postgres::PgTypeInfo::with_name($sql_name)
      }

      fn compatible(ty: &postgres::PgTypeInfo) -> bool {
        *ty == Self::type_info() || <&[u8] as sqlx::Type<Postgres>>::compatible(ty)
      }
    }

    #[cfg(feature = "sqlx")]
    impl<'r, Db: Database> sqlx::Decode<'r, Db> for $ty
    where
      &'r [u8]: sqlx::Decode<'r, Db>,
    {
      fn decode(
        value: <Db as database::HasValueRef<'r>>::ValueRef,
      ) -> Result<Self, Box<dyn Error + 'static + Send + Sync>> {
        let value: &[u8] = <&[u8] as sqlx::Decode<Db>>::decode(value)?;
        Ok(Self::from_bytes(value)?)
      }
    }

    #[cfg(feature = "sqlx")]
    impl<'q, Db: Database> sqlx::Encode<'q, Db> for $ty
    where
      Vec<u8>: sqlx::Encode<'q, Db>,
    {
      fn encode_by_ref(&self, buf: &mut <Db as database::HasArguments<'q>>::ArgumentBuffer) -> sqlx::encode::IsNull {
        self.as_slice().to_vec().encode(buf)
      }
    }
  };
}

define_hex_digest! {
  name = "sha1",
  sql_name = "digest_sha1",
  ty = DigestSha1,
  ty_byte_error = DigestSha1FromBytesError,
  ty_parse_error = ParseDigestSha1Error,
  byte_len = 20,
  hex_len = 40,
}

define_hex_digest! {
  name = "sha2",
  sql_name = "digest_sha2",
  ty = DigestSha2,
  ty_byte_error = DigestSha2FromBytesError,
  ty_parse_error = ParseDigestSha2Error,
  byte_len = 32,
  hex_len = 64,
}

impl DigestSha2 {
  pub fn digest(data: &[u8]) -> Self {
    use sha2::{Digest, Sha256};
    Self(Sha256::digest(data).into())
  }
}

define_hex_digest! {
  name = "sha3",
  sql_name = "digest_sha3",
  ty = DigestSha3,
  ty_byte_error = DigestSha3FromBytesError,
  ty_parse_error = ParseDigestSha3Error,
  byte_len = 32,
  hex_len = 64,
}

impl DigestSha3 {
  pub fn digest(data: &[u8]) -> Self {
    use sha3::{Digest, Sha3_256};
    Self(Sha3_256::digest(data).into())
  }
}

#[cfg(test)]
mod test {
  mod digest_sha1 {
    use crate::digest::{DigestSha1, ParseDigestSha1Error};

    #[test]
    fn from_hex_ok() {
      let actual = DigestSha1::from_hex("27b664227bd281744d1449568fc6edb06361e747");
      #[rustfmt::skip]
        let expected = Ok(DigestSha1([
        0x27, 0xb6, 0x64, 0x22, 0x7b, 0xd2, 0x81, 0x74,
        0x4d, 0x14, 0x49, 0x56, 0x8f, 0xc6, 0xed, 0xb0,
        0x63, 0x61, 0xe7, 0x47,
      ]));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_empty() {
      let actual = DigestSha1::from_hex("");
      let expected = Err(ParseDigestSha1Error::Length(0));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_too_long() {
      let actual = DigestSha1::from_hex("27b664227bd281744d1449568fc6edb06361e7470000");
      let expected = Err(ParseDigestSha1Error::Length(44));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_odd_length() {
      let actual = DigestSha1::from_hex("27b664227bd281744d1449568fc6edb06361e7470");
      let expected = Err(ParseDigestSha1Error::Length(41));
      assert_eq!(actual, expected);
    }

    #[test]
    fn from_hex_invalid_char() {
      let actual = DigestSha1::from_hex("27!664227bd281744d1449568fc6edb06361e747");
      let expected = Err(ParseDigestSha1Error::InvalidCharacter {
        character: '!',
        offset: 2,
      });
      assert_eq!(actual, expected);
    }
  }
}
