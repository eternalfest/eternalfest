use async_trait::async_trait;
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::Instant;
use eternalfest_core::user::{
  GetUsersOptions, RawGetUserError, RawGetUserOptions, ShortUser, StoreGetShortUserError, StoreGetShortUsersError,
  StoreGetUsersError, StoreUpdateUserError, StoreUpsertFromEtwinError, UpdateUserOptions, User, UserDisplayName,
  UserId, UserListing, UserStore,
};
use std::collections::HashMap;
use std::sync::RwLock;

struct StoreState {
  users: HashMap<UserId, RawUser>,
}

#[derive(Debug)]
struct RawUser {
  id: UserId,
  display_name: UserDisplayName,
  created_at: Instant,
  updated_at: Instant,
  is_administrator: bool,
  is_tester: bool,
}

impl StoreState {
  fn new() -> Self {
    Self { users: HashMap::new() }
  }

  pub(crate) fn upsert_from_etwin(
    &mut self,
    now: Instant,
    user: &ShortUser,
  ) -> Result<User, StoreUpsertFromEtwinError> {
    let is_administrator = self.users.is_empty();
    let user = self.users.entry(user.id).or_insert_with(|| RawUser {
      id: user.id,
      display_name: user.display_name.clone(),
      created_at: now,
      updated_at: now,
      is_administrator,
      is_tester: is_administrator,
    });

    Ok(User {
      id: user.id,
      display_name: user.display_name.clone(),
      created_at: user.created_at,
      updated_at: user.updated_at,
      is_administrator: user.is_administrator,
      is_tester: user.is_tester,
    })
  }
}

pub struct MemUserStore<TyClock>
where
  TyClock: ClockRef,
{
  clock: TyClock,
  state: RwLock<StoreState>,
}

impl<TyClock> MemUserStore<TyClock>
where
  TyClock: ClockRef,
{
  pub fn new(clock: TyClock) -> Self {
    Self {
      clock,
      state: RwLock::new(StoreState::new()),
    }
  }
}

#[async_trait]
impl<TyClock> UserStore for MemUserStore<TyClock>
where
  TyClock: ClockRef,
{
  async fn upsert_from_etwin(&self, user: &ShortUser) -> Result<User, StoreUpsertFromEtwinError> {
    let now = self.clock.clock().now();
    let mut state = self.state.write().unwrap();
    state.upsert_from_etwin(now, user)
  }

  async fn get_user(&self, options: &RawGetUserOptions) -> Result<User, RawGetUserError> {
    let state = self.state.read().unwrap();
    let user = state.users.get(&options.id).ok_or(RawGetUserError::NotFound)?;
    Ok(User {
      id: user.id,
      display_name: user.display_name.clone(),
      created_at: user.created_at,
      updated_at: user.updated_at,
      is_administrator: user.is_administrator,
      is_tester: user.is_tester,
    })
  }

  async fn get_short_user(&self, _options: &RawGetUserOptions) -> Result<ShortUser, StoreGetShortUserError> {
    todo!()
  }

  async fn get_short_users(
    &self,
    _options: &GetUsersOptions,
  ) -> Result<HashMap<UserId, ShortUser>, StoreGetShortUsersError> {
    todo!()
  }

  async fn get_users(&self) -> Result<UserListing, StoreGetUsersError> {
    todo!()
  }

  async fn update_user(&self, _options: &UpdateUserOptions) -> Result<User, StoreUpdateUserError> {
    todo!()
  }
}

#[cfg(test)]
mod test {
  use crate::mem::MemUserStore;
  use crate::test::TestApi;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::user::UserStore;
  use std::sync::Arc;

  fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn UserStore>> {
    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let user_store: Arc<dyn UserStore> = Arc::new(MemUserStore::new(clock.clone()));

    TestApi { clock, user_store }
  }

  test_user_store!(|| make_test_api());
}
