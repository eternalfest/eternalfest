use async_trait::async_trait;
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::core::Instant;
use eternalfest_core::types::WeakError;
use eternalfest_core::user::{
  GetUsersOptions, RawGetUserError, RawGetUserOptions, ShortUser, StoreGetShortUserError, StoreGetShortUsersError,
  StoreGetUsersError, StoreUpdateUserError, StoreUpsertFromEtwinError, UpdateUserOptions, User, UserDisplayName,
  UserId, UserListing, UserStore,
};
use eternalfest_core::SyncRef;
use futures::TryStreamExt;
use sqlx::postgres::PgPool;
use sqlx::types::Uuid;
use std::collections::HashMap;
use std::convert::TryFrom;

pub struct PgUserStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  clock: TyClock,
  database: TyDatabase,
}

impl<TyClock, TyDatabase> PgUserStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  pub fn new(clock: TyClock, database: TyDatabase) -> Self {
    Self { clock, database }
  }
}

#[async_trait]
impl<TyClock, TyDatabase> UserStore for PgUserStore<TyClock, TyDatabase>
where
  TyClock: ClockRef,
  TyDatabase: SyncRef<PgPool>,
{
  async fn upsert_from_etwin(&self, user: &ShortUser) -> Result<User, StoreUpsertFromEtwinError> {
    let now = self.clock.clock().now();
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      created_at: Instant,
      updated_at: Instant,
      is_administrator: bool,
      is_tester: bool,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as(
      r"
      WITH administrator_exists AS (SELECT 1 FROM users WHERE is_administrator)
      INSERT INTO users (
        user_id, created_at, updated_at, display_name,
        is_administrator,
        is_tester,
        email, username, password_hash
      )
      VALUES (
        $1::USER_ID, $3::INSTANT, $3::INSTANT, $2::USER_DISPLAY_NAME,
        (NOT EXISTS (SELECT 1 FROM administrator_exists)),
        (NOT EXISTS (SELECT 1 FROM administrator_exists)),
        NULL, NULL, NULL
      )
      ON CONFLICT (user_id)
        DO UPDATE SET
        display_name = $2::USER_DISPLAY_NAME
      RETURNING created_at, updated_at, is_administrator, is_tester;
      ",
    )
    .bind(user.id)
    .bind(&user.display_name)
    .bind(now)
    .fetch_one(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    Ok(User {
      id: user.id,
      display_name: user.display_name.clone(),
      created_at: row.created_at,
      updated_at: row.updated_at,
      is_administrator: row.is_administrator,
      is_tester: row.is_tester,
    })
  }

  async fn get_user(&self, options: &RawGetUserOptions) -> Result<User, RawGetUserError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      display_name: UserDisplayName,
      created_at: Instant,
      updated_at: Instant,
      is_administrator: bool,
      is_tester: bool,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as(
      r"
      SELECT user_id, display_name, created_at, updated_at, is_administrator, is_tester
      FROM users
      WHERE users.user_id = $1::USER_ID;
      ",
    )
    .bind(options.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?
    .ok_or(RawGetUserError::NotFound)?;

    Ok(User {
      id: row.user_id,
      display_name: row.display_name,
      created_at: row.created_at,
      updated_at: row.updated_at,
      is_administrator: row.is_administrator,
      is_tester: row.is_tester,
    })
  }

  async fn get_short_user(&self, options: &RawGetUserOptions) -> Result<ShortUser, StoreGetShortUserError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      display_name: UserDisplayName,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as(
      r"
      SELECT user_id, display_name
      FROM users
      WHERE users.user_id = $1::USER_ID;
      ",
    )
    .bind(options.id)
    .fetch_optional(&*self.database)
    .await
    .map_err(WeakError::wrap)?
    .ok_or(StoreGetShortUserError::NotFound)?;

    Ok(ShortUser {
      id: row.user_id,
      display_name: row.display_name,
    })
  }

  async fn get_short_users(
    &self,
    options: &GetUsersOptions,
  ) -> Result<HashMap<UserId, ShortUser>, StoreGetShortUsersError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      display_name: UserDisplayName,
    }
    let ids: Vec<Uuid> = options.id.iter().map(|id| id.into_uuid()).collect();
    let mut users: HashMap<UserId, ShortUser> = HashMap::new();

    // language=PostgreSQL
    let mut rows = sqlx::query_as::<_, Row>(
      r"
      SELECT user_id, display_name
      FROM users
      WHERE users.user_id = ANY($1::USER_ID[]);
      ",
    )
    .bind(ids.as_slice())
    .fetch(&*self.database);

    while let Some(row) = rows.try_next().await.map_err(WeakError::wrap)? {
      users.insert(
        row.user_id,
        ShortUser {
          id: row.user_id,
          display_name: row.display_name,
        },
      );
    }

    for id in &options.id {
      if !users.contains_key(id) {
        return Err(StoreGetShortUsersError::Other(WeakError::new(format!(
          "UserId not found: {}",
          id.to_hex()
        ))));
      }
    }

    Ok(users)
  }

  async fn get_users(&self) -> Result<UserListing, StoreGetUsersError> {
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      display_name: UserDisplayName,
      created_at: Instant,
      updated_at: Instant,
      is_administrator: bool,
      is_tester: bool,
    }

    // language=PostgreSQL
    let rows = sqlx::query_as::<_, Row>(
      r"
      SELECT user_id, display_name, created_at, updated_at, is_administrator, is_tester
      FROM users;
      ",
    )
    .fetch_all(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    let users: Vec<User> = rows
      .into_iter()
      .map(|row: Row| User {
        id: row.user_id,
        display_name: row.display_name,
        created_at: row.created_at,
        updated_at: row.updated_at,
        is_administrator: row.is_administrator,
        is_tester: row.is_tester,
      })
      .collect();

    let len = u32::try_from(users.len()).expect("UserLenOverflow");

    Ok(UserListing {
      offset: 0,
      limit: len,
      count: len,
      items: users,
    })
  }

  async fn update_user(&self, options: &UpdateUserOptions) -> Result<User, StoreUpdateUserError> {
    let now = self.clock.clock().now();
    #[derive(Debug, sqlx::FromRow)]
    struct Row {
      user_id: UserId,
      display_name: UserDisplayName,
      created_at: Instant,
      updated_at: Instant,
      is_administrator: bool,
      is_tester: bool,
    }
    // language=PostgreSQL
    let row: Row = sqlx::query_as(
      r"
      UPDATE users
      SET is_tester = $2::BOOLEAN, updated_at = $3::INSTANT
      WHERE user_id = $1::USER_ID
      RETURNING user_id, display_name, created_at, updated_at, is_administrator, is_tester;
      ",
    )
    .bind(options.user_id)
    .bind(options.is_tester)
    .bind(now)
    .fetch_one(&*self.database)
    .await
    .map_err(WeakError::wrap)?;

    Ok(User {
      id: row.user_id,
      display_name: row.display_name,
      created_at: row.created_at,
      updated_at: row.updated_at,
      is_administrator: row.is_administrator,
      is_tester: row.is_tester,
    })
  }
}

#[cfg(test)]
mod test {
  use super::PgUserStore;
  use crate::test::TestApi;
  use eternalfest_core::clock::VirtualClock;
  use eternalfest_core::core::Instant;
  use eternalfest_core::user::UserStore;
  use eternalfest_db_schema::force_create_latest;
  use serial_test::serial;
  use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
  use sqlx::PgPool;
  use std::sync::Arc;

  async fn make_test_api() -> TestApi<Arc<VirtualClock>, Arc<dyn UserStore>> {
    let config = eternalfest_config::Config::for_test();
    let admin_database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.admin_user.value)
          .password(&config.postgres.admin_password.value),
      )
      .await
      .unwrap();
    force_create_latest(&admin_database, true).await.unwrap();
    admin_database.close().await;

    let database: PgPool = PgPoolOptions::new()
      .max_connections(5)
      .connect_with(
        PgConnectOptions::new()
          .host(&config.postgres.host.value)
          .port(config.postgres.port.value)
          .database(&config.postgres.name.value)
          .username(&config.postgres.user.value)
          .password(&config.postgres.password.value),
      )
      .await
      .unwrap();
    let database = Arc::new(database);

    let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
    let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(clock.clone(), database));

    TestApi { clock, user_store }
  }

  test_user_store!(
    #[serial]
    || make_test_api().await
  );
}
