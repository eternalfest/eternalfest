use eternalfest_core::clock::{Clock, VirtualClock};
use eternalfest_core::core::Instant;
use eternalfest_core::user::{RawGetUserError, RawGetUserOptions, UserId, UserStore, UserStoreRef};
use eternalfest_core::SyncRef;

#[macro_export]
macro_rules! test_user_store {
  ($(#[$meta:meta])* || $api:expr) => {
    register_test!($(#[$meta])*, $api, test_get_missing_user);
    // register_test!($(#[$meta])*, $api, test_upsert_from_etwin);
    // register_test!($(#[$meta])*, $api, test_register_the_admin_and_retrieve_short);
    // register_test!($(#[$meta])*, $api, test_register_the_admin_and_retrieve_default);
    // register_test!($(#[$meta])*, $api, test_register_the_admin_and_retrieve_complete);
    // register_test!($(#[$meta])*, $api, test_update_display_name_after_creation);
    // register_test!($(#[$meta])*, $api, test_update_locked_display_name);
    // register_test!($(#[$meta])*, $api, test_update_display_after_unlock);
    // register_test!($(#[$meta])*, $api, test_update_display_name_then_username);
    // register_test!($(#[$meta])*, $api, test_update_locked_display_name_after_update);
    // register_test!($(#[$meta])*, $api, test_update_display_name_afte_multiple_unlocks);
    // register_test!($(#[$meta])*, $api, test_hard_delete_user);
  };
}

macro_rules! register_test {
  ($(#[$meta:meta])*, $api:expr, $test_name:ident) => {
    #[tokio::test]
    $(#[$meta])*
    async fn $test_name() {
      crate::test::$test_name($api).await;
    }
  };
}

pub(crate) struct TestApi<TyClock, TyUserStore>
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  pub(crate) clock: TyClock,
  pub(crate) user_store: TyUserStore,
}

pub(crate) async fn test_get_missing_user<TyClock, TyUserStore>(api: TestApi<TyClock, TyUserStore>)
where
  TyClock: SyncRef<VirtualClock>,
  TyUserStore: UserStoreRef,
{
  api.clock.advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let actual = api
    .user_store
    .user_store()
    .get_user(&RawGetUserOptions {
      id: UserId::from_u128(123),
      now: api.clock.now(),
      time: None,
    })
    .await;
  let expected = Err(RawGetUserError::NotFound);
  assert_eq!(actual, expected);
}
