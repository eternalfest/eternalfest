use eternalfest_core::auth::{
  AuthContext, AuthScope, AuthStore, AuthStoreRef, CreateSessionOptions, RawSession, Session, SessionId,
  UserAndSession, UserAuthContext,
};
use eternalfest_core::core::Duration;
use eternalfest_core::oauth::{EternalfestOauthStateAction, EternalfestOauthStateClaims};
use eternalfest_core::types::WeakError;
use eternalfest_core::user::{RawGetUserOptions, ShortUser, UserId, UserStore, UserStoreRef};
use eternaltwin_client::{EtwinAuth, EtwinClient as EternaltwinClient};
use eternaltwin_core::auth::AuthContext as EtwinAuthContext;
use eternaltwin_core::clock::{Clock, ClockRef};
use eternaltwin_core::core::{Instant, SecretBytes};
use eternaltwin_core::oauth::RfcOauthAccessToken;
use eternaltwin_core::password::Password;
use eternaltwin_oauth_client::RfcOauthClient;
use serde::{Deserialize, Serialize};
use std::borrow::Cow;
use std::collections::HashSet;
use std::sync::Arc;
use url::Url;

#[derive(Debug, Serialize, Deserialize)]
pub struct AuthenticateHttpOptions {
  authorization_header: Option<String>,
  session_cookie: Option<String>,
}

pub struct AuthService<TyAuthStore, TyClock, TyEternaltwinClient, TyEternaltwinOauthClient, TyUserStore>
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyEternaltwinClient: EternaltwinClient,
  TyEternaltwinOauthClient: RfcOauthClient,
  TyUserStore: UserStoreRef,
{
  auth_store: TyAuthStore,
  clock: TyClock,
  eternaltwin_client: TyEternaltwinClient,
  eternaltwin_oauth_client: TyEternaltwinOauthClient,
  user_store: TyUserStore,
  internal_auth_key: SecretBytes,
  jwt_secret_key: SecretBytes,
}

pub type DynAuthService = AuthService<
  Arc<dyn AuthStore>,
  Arc<dyn Clock>,
  Arc<dyn EternaltwinClient>,
  Arc<dyn RfcOauthClient>,
  Arc<dyn UserStore>,
>;

#[derive(Debug, thiserror::Error)]
pub enum ReadOauthStateError {
  #[error("invalid JWT")]
  Jwt(#[from] jsonwebtoken::errors::Error),
  #[error("expired token: now = {now}, iat = {issued_at}, exp = {expiration_time}")]
  Time {
    now: Instant,
    issued_at: Instant,
    expiration_time: Instant,
  },
}

impl<TyAuthStore, TyClock, TyEternaltwinClient, TyEternaltwinOauthClient, TyUserStore>
  AuthService<TyAuthStore, TyClock, TyEternaltwinClient, TyEternaltwinOauthClient, TyUserStore>
where
  TyAuthStore: AuthStoreRef,
  TyClock: ClockRef,
  TyEternaltwinClient: EternaltwinClient,
  TyEternaltwinOauthClient: RfcOauthClient,
  TyUserStore: UserStoreRef,
{
  pub fn new(
    auth_store: TyAuthStore,
    clock: TyClock,
    eternaltwin_client: TyEternaltwinClient,
    eternaltwin_oauth_client: TyEternaltwinOauthClient,
    user_store: TyUserStore,
    internal_auth_key: SecretBytes,
    jwt_secret_key: SecretBytes,
  ) -> Self {
    Self {
      auth_store,
      clock,
      eternaltwin_client,
      eternaltwin_oauth_client,
      user_store,
      internal_auth_key,
      jwt_secret_key,
    }
  }

  /// Authenticate an internal request
  pub fn authenticate_internal(&self, key: &[u8]) -> bool {
    key == self.internal_auth_key.as_slice()
  }

  /// Authenticate a third-party client through an Eternalfest access token.
  pub async fn authenticate_access_token(&self, token: &str) -> Result<AuthContext, WeakError> {
    if token.starts_with('[') {
      // JSON token used for internal auth, should be replaced eventually by a JWT.
      let (key, acx) = serde_json::from_str::<(Cow<str>, AuthContext)>(token).map_err(WeakError::wrap)?;
      if key.as_ref().as_bytes() == self.internal_auth_key.as_slice() {
        return Ok(acx);
      } else {
        return Err(WeakError::new("invalid internal auth key"));
      }
    }
    Err(WeakError::new("invalid token format"))
  }

  pub async fn eternaltwin_oauth_code(&self, rfc_oauth_code: &str) -> Result<UserAndSession, WeakError> {
    let token = self
      .eternaltwin_oauth_client
      .get_access_token(rfc_oauth_code)
      .await
      .map_err(WeakError::wrap)?;
    self.eternaltwin_oauth(&token).await
  }

  pub async fn eternaltwin_oauth(&self, token: &RfcOauthAccessToken) -> Result<UserAndSession, WeakError> {
    let auth = EtwinAuth::Token(token.access_token.to_string());
    let etwin_acx: eternaltwin_core::auth::AuthContext =
      self.eternaltwin_client.get_self(&auth).await.map_err(WeakError::wrap)?;

    let etwin_user: eternaltwin_core::user::ShortUser = match etwin_acx {
      eternaltwin_core::auth::AuthContext::AccessToken(acx) => acx.user,
      acx => return Err(WeakError::new(format!("non-user eternaltwin acx: {acx:?}"))),
    };

    let user = self
      .user_store
      .user_store()
      .upsert_from_etwin(&ShortUser {
        id: etwin_user.id,
        display_name: etwin_user.display_name.current.value.clone(),
      })
      .await
      .map_err(WeakError::wrap)?;

    let session = self.create_session(user.id).await?;

    let is_administrator = user.is_administrator;
    let is_tester = user.is_tester;

    Ok(UserAndSession {
      user,
      is_administrator,
      is_tester,
      session,
    })
  }

  pub async fn authenticate_for_test(
    &self,
    id: UserId,
    display_name: &eternaltwin_core::user::UserDisplayName,
  ) -> Result<UserAndSession, WeakError> {
    let user = self
      .user_store
      .user_store()
      .upsert_from_etwin(&ShortUser {
        id,
        display_name: display_name.clone(),
      })
      .await
      .map_err(WeakError::wrap)?;

    let session = self.create_session(user.id).await?;

    let is_administrator = user.is_administrator;
    let is_tester = user.is_tester;

    Ok(UserAndSession {
      user,
      is_administrator,
      is_tester,
      session,
    })
  }

  pub async fn session(&self, session_id: SessionId) -> Result<UserAndSession, WeakError> {
    let now = self.clock.clock().now();
    let session = self.auth_store.auth_store().get_and_touch_session(session_id).await?;
    let session: RawSession = match session {
      Some(s) => s,
      None => return Err(WeakError::new("SessionNotFound")),
    };

    let user = self
      .user_store
      .user_store()
      .get_user(&RawGetUserOptions {
        id: session.user.id,
        now,
        time: None,
      })
      .await
      .map_err(WeakError::wrap)?;
    let is_administrator = user.is_administrator;
    let is_tester = user.is_tester;

    let session = Session {
      id: session.id,
      user: session.user,
      created_at: session.created_at,
      updated_at: session.updated_at,
    };

    Ok(UserAndSession {
      user,
      is_administrator,
      is_tester,
      session,
    })
  }

  pub async fn create_session(&self, user_id: UserId) -> Result<Session, WeakError> {
    let session = self
      .auth_store
      .auth_store()
      .create_session(&CreateSessionOptions { user: user_id.into() })
      .await?;
    let session = Session {
      id: session.id,
      user: session.user,
      created_at: session.created_at,
      updated_at: session.updated_at,
    };

    Ok(session)
  }

  pub async fn authenticate_credentials(&self, user: &str, password: &str) -> Result<AuthContext, WeakError> {
    let acx = self
      .eternaltwin_client
      .get_self(&EtwinAuth::Credentials {
        username: user
          .parse()
          .map_err::<WeakError, _>(|_| WeakError::new("InvalidLogin"))?,
        password: Password(password.as_bytes().to_vec()),
      })
      .await
      .map_err::<WeakError, _>(|_| WeakError::new("BadCredentials"))?;
    match acx {
      EtwinAuthContext::User(acx) => {
        let user = self
          .user_store
          .user_store()
          .upsert_from_etwin(&acx.user.into())
          .await
          .map_err(WeakError::wrap)?;
        Ok(AuthContext::User(UserAuthContext {
          scope: AuthScope::Default,
          user: ShortUser {
            id: user.id,
            display_name: user.display_name,
          },
          is_administrator: user.is_administrator,
          is_tester: user.is_tester,
        }))
      }
      acx => Err(WeakError::new(format!("UnexpectedAuthContextType: {acx:?}"))),
    }
  }

  pub async fn authenticate_session(&self, session_id: SessionId) -> Result<UserAndSession, WeakError> {
    self.session(session_id).await
  }

  pub async fn request_eternaltwin_auth(&self, action: EternalfestOauthStateAction) -> Result<Url, WeakError> {
    const ALL_ETERNALTWIN_SCOPES: [&str; 1] = ["base"];

    /// Max duration of the JWT (15 min)
    static TOKEN_LIFETIME: Duration = Duration::from_minutes(15);

    let now = self.clock.clock().now();
    let key = jsonwebtoken::EncodingKey::from_secret(self.jwt_secret_key.as_slice());

    let jwt_header = jsonwebtoken::Header::new(jsonwebtoken::Algorithm::HS256);
    let claims = EternalfestOauthStateClaims {
      request_forgery_protection: "TODO".to_string(),
      action,
      issued_at: now,
      authorization_server: self.eternaltwin_oauth_client.authorization_server().to_string(),
      expiration_time: now + TOKEN_LIFETIME,
    };

    let state = jsonwebtoken::encode(&jwt_header, &claims, &key).map_err(WeakError::wrap)?;

    Ok(
      self
        .eternaltwin_oauth_client
        .authorization_uri(&ALL_ETERNALTWIN_SCOPES.join(","), &state),
    )
  }

  /// Read the eternalfest client state (round-tripped through a third-party provider)
  pub fn read_oauth_state(&self, state: &str) -> Result<EternalfestOauthStateClaims, ReadOauthStateError> {
    let now = self.clock.clock().now();
    let key = jsonwebtoken::DecodingKey::from_secret(self.jwt_secret_key.as_slice());
    let mut validation = jsonwebtoken::Validation::new(jsonwebtoken::Algorithm::HS256);
    // TODO: validate expiration (the `validation` updates below disable it)!
    validation.required_spec_claims = HashSet::new();
    validation.leeway = 0;
    validation.validate_exp = false;
    let token = jsonwebtoken::decode::<EternalfestOauthStateClaims>(state, &key, &validation)?;
    if !(token.claims.issued_at <= now && now < token.claims.expiration_time) {
      return Err(ReadOauthStateError::Time {
        now,
        issued_at: token.claims.issued_at,
        expiration_time: token.claims.expiration_time,
      });
    }

    Ok(token.claims)
  }
}
