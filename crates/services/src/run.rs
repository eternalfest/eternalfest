use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{Blob, BlobId, BlobStore, BlobStoreRef, GetBlobsOptions};
use eternalfest_core::clock::Clock;
use eternalfest_core::core::SimpleSemVer;
use eternalfest_core::game::store::{GetStoreGame, GetStoreGameError};
use eternalfest_core::game::{
  Game, GameBuild, GameChannelKey, GameChannelKeyRef, GameChannelPermission, GameId, GameIdRef, GameModeKey, GameRef,
  GameStore, GameStoreRef, RefsOnly, Resolved,
};
use eternalfest_core::run::{
  CreateRunOptions, GetUserItems, GetUserItemsError, Leaderboard, LeaderboardEntry, LeaderboardEntryRun, Run, RunId,
  RunIdRef, RunStart, RunStore, RunStoreRef, SetRunResultOptions, StoreCreateRun, StoreCreateRunError,
  StoreGetLeaderboard, StoreGetLeaderboardError, StoreGetRun, StoreGetRunError, StoreLeaderboard, StoreSetRunResult,
  StoreSetRunResultError, StoreStartRun, StoreStartRunError,
};
use eternalfest_core::types::WeakError;
use eternalfest_core::user::{
  GetUsersOptions, RawGetUserOptions, StoreGetShortUserError, StoreGetShortUsersError, UserStore, UserStoreRef,
};
use eternaltwin_core::clock::ClockRef;
use eternaltwin_core::core::Instant;
use eternaltwin_core::hammerfest::HammerfestItemId;
use eternaltwin_core::user::{UserId, UserIdRef};
use std::collections::{BTreeMap, HashMap, HashSet};
use std::sync::Arc;

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
enum InternalGetGameError {
  #[error("leaderboard access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("not implemented: legacy version retrieval")]
  NotImplementedLegacyVersionRetrieval,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<GetStoreGameError> for InternalGetGameError {
  fn from(value: GetStoreGameError) -> Self {
    match value {
      GetStoreGameError::GameNotFound => InternalGetGameError::GameNotFound,
      GetStoreGameError::PermissionRevoked => InternalGetGameError::Forbidden,
      GetStoreGameError::Other(e) => InternalGetGameError::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum CreateRunError {
  #[error("leaderboard access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("failed to create run in store")]
  InternalStoreCreateRun(#[from] StoreCreateRunError),
  #[error("failed to get user in store")]
  InternalStoreGetShortUser(#[from] StoreGetShortUserError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<InternalGetGameError> for CreateRunError {
  fn from(value: InternalGetGameError) -> Self {
    match value {
      InternalGetGameError::Forbidden => Self::Forbidden,
      InternalGetGameError::GameNotFound => Self::GameNotFound,
      InternalGetGameError::ChannelNotFound => Self::ChannelNotFound,
      e @ InternalGetGameError::NotImplementedLegacyVersionRetrieval => Self::Other(WeakError::wrap(e)),
      InternalGetGameError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetRunError {
  #[error("leaderboard access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("run not found")]
  RunNotFound,
  #[error("failed to get user in store")]
  InternalStoreGetShortUser(#[from] StoreGetShortUserError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<InternalGetGameError> for GetRunError {
  fn from(value: InternalGetGameError) -> Self {
    match value {
      InternalGetGameError::Forbidden => Self::Forbidden,
      InternalGetGameError::GameNotFound => Self::GameNotFound,
      InternalGetGameError::ChannelNotFound => Self::ChannelNotFound,
      e @ InternalGetGameError::NotImplementedLegacyVersionRetrieval => Self::Other(WeakError::wrap(e)),
      InternalGetGameError::Other(e) => Self::Other(e),
    }
  }
}

impl From<StoreGetRunError> for GetRunError {
  fn from(value: StoreGetRunError) -> Self {
    match value {
      StoreGetRunError::NotFound => Self::RunNotFound,
      StoreGetRunError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum StartRunError {
  #[error("leaderboard access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("failed to start run in store")]
  InternalStoreStartRun(#[from] StoreStartRunError),
  #[error("failed to get user items")]
  InternalGetUserItems(#[from] GetUserItemsError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<InternalGetGameError> for StartRunError {
  fn from(value: InternalGetGameError) -> Self {
    match value {
      InternalGetGameError::Forbidden => Self::Forbidden,
      InternalGetGameError::GameNotFound => Self::GameNotFound,
      InternalGetGameError::ChannelNotFound => Self::ChannelNotFound,
      e @ InternalGetGameError::NotImplementedLegacyVersionRetrieval => Self::Other(WeakError::wrap(e)),
      InternalGetGameError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum SetRunResultError {
  #[error("leaderboard access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("failed to store run result items")]
  InternalStoreSetRunResult(#[from] StoreSetRunResultError),
  #[error("failed to get user in store")]
  InternalStoreGetShortUser(#[from] StoreGetShortUserError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<InternalGetGameError> for SetRunResultError {
  fn from(value: InternalGetGameError) -> Self {
    match value {
      InternalGetGameError::Forbidden => Self::Forbidden,
      InternalGetGameError::GameNotFound => Self::GameNotFound,
      InternalGetGameError::ChannelNotFound => Self::ChannelNotFound,
      e @ InternalGetGameError::NotImplementedLegacyVersionRetrieval => Self::Other(WeakError::wrap(e)),
      InternalGetGameError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetLeaderboardError {
  #[error("leaderboard access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("internal error while retrieving game")]
  InternalGetGame(GetStoreGameError),
  #[error("internal error while retrieving leaderboard")]
  InternalGetLeaderboard(#[from] StoreGetLeaderboardError),
  #[error("failed to get users from store")]
  InternalStoreGetShortUsers(#[from] StoreGetShortUsersError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<GetStoreGameError> for GetLeaderboardError {
  fn from(value: GetStoreGameError) -> Self {
    match value {
      GetStoreGameError::GameNotFound => Self::GameNotFound,
      GetStoreGameError::PermissionRevoked => Self::Forbidden,
      e @ GetStoreGameError::Other(_) => Self::InternalGetGame(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetUserInventoryError {
  #[error("inventory access forbidden")]
  Forbidden,
  #[error("game not found")]
  GameNotFound,
  #[error("channel not found")]
  ChannelNotFound,
  #[error("internal error while retrieving game")]
  InternalGetGame(GetStoreGameError),
  #[error("internal error while retrieving inventory")]
  InternalGetInventory(#[from] GetUserItemsError),
  #[error("failed to get users from store")]
  InternalStoreGetShortUsers(#[from] StoreGetShortUsersError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<GetStoreGameError> for GetUserInventoryError {
  fn from(value: GetStoreGameError) -> Self {
    match value {
      GetStoreGameError::GameNotFound => Self::GameNotFound,
      GetStoreGameError::PermissionRevoked => Self::Forbidden,
      e @ GetStoreGameError::Other(_) => Self::InternalGetGame(e),
    }
  }
}

pub struct RunService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStoreRef,
  TyClock: ClockRef,
  TyGameStore: GameStoreRef,
  TyRunStore: RunStoreRef,
  TyUserStore: UserStoreRef,
{
  #[allow(unused)]
  blob_store: TyBlobStore,
  clock: TyClock,
  game_store: TyGameStore,
  run_store: TyRunStore,
  user_store: TyUserStore,
}

pub type DynRunService =
  RunService<Arc<dyn BlobStore>, Arc<dyn Clock>, Arc<dyn GameStore>, Arc<dyn RunStore>, Arc<dyn UserStore>>;

impl<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
  RunService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStoreRef,
  TyClock: ClockRef,
  TyGameStore: GameStoreRef,
  TyRunStore: RunStoreRef,
  TyUserStore: UserStoreRef,
{
  pub fn new(
    blob_store: TyBlobStore,
    clock: TyClock,
    game_store: TyGameStore,
    run_store: TyRunStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      blob_store,
      clock,
      game_store,
      run_store,
      user_store,
    }
  }

  async fn retrieve_game(
    &self,
    acx: &AuthContext,
    game: GameIdRef,
    channel: &GameChannelKey,
    version: SimpleSemVer,
    now: Instant,
  ) -> Result<(Game<RefsOnly>, UserIdRef), InternalGetGameError> {
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (u.user.as_ref(), u.is_tester || u.is_administrator),
      _ => return Err(InternalGetGameError::Forbidden),
    };
    let game = self
      .game_store
      .game_store()
      .get_game(&GetStoreGame {
        actor: Some(actor),
        is_tester,
        now,
        time: None,
        game: game.into(),
        channel: Some(channel.clone()),
      })
      .await?;

    // Do permission checks.
    if game.channels.active.key != *channel || !game.channels.active.is_enabled {
      return Err(InternalGetGameError::ChannelNotFound);
    }
    if game.channels.active.build.version != version {
      return Err(InternalGetGameError::NotImplementedLegacyVersionRetrieval);
    }
    if !(is_tester || game.owner == actor || game.channels.active.default_permission >= GameChannelPermission::Play) {
      return Err(InternalGetGameError::Forbidden);
    }

    Ok((game, actor))
  }

  async fn resolve_game_build(&self, store_game: Game<RefsOnly>) -> Result<GameBuild<Resolved>, WeakError> {
    // Resolve blobs
    let blobs: HashSet<_> = store_game.blobs().map(|b| b.id).collect();
    let blobs = self
      .blob_store
      .blob_store()
      .get_blobs(&GetBlobsOptions { id: blobs, time: None })
      .await
      .map_err(WeakError::wrap)?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    Ok(store_game.channels.active.build.resolve(&blobs))
  }

  pub async fn create_run(&self, acx: &AuthContext, req: &CreateRunOptions) -> Result<Run, CreateRunError> {
    let now = self.clock.clock().now();
    let (game, actor) = self
      .retrieve_game(acx, req.game, &req.channel, req.version, now)
      .await?;

    let run = self
      .run_store
      .run_store()
      .create_run(&StoreCreateRun {
        game: req.game,
        channel: req.channel.clone(),
        version: req.version,
        user: actor,
        mode: req.game_mode.clone(),
        options: req.game_options.clone(),
        settings: req.settings,
      })
      .await?;

    // TODO: Reuse the auth-context instead of querying the DB
    let user = self
      .user_store
      .user_store()
      .get_short_user(&RawGetUserOptions {
        id: run.user.id,
        time: None,
        now,
      })
      .await?;

    Ok(Run {
      id: run.id,
      created_at: run.created_at,
      started_at: run.started_at,
      result: run.result,
      game: run.game,
      channel: GameChannelKeyRef::new(run.channel),
      build: self.resolve_game_build(game).await.map_err(CreateRunError::Other)?,
      user,
      game_mode: run.mode,
      game_options: run.options,
      settings: run.settings,
    })
  }

  pub async fn get_run(&self, acx: &AuthContext, run_id: RunId) -> Result<Run, GetRunError> {
    let now = self.clock.clock().now();
    let run = self
      .run_store
      .run_store()
      .get_run(&StoreGetRun {
        run: RunIdRef::new(run_id),
      })
      .await?;

    let (game, _) = self
      .retrieve_game(acx, run.game, &run.channel, run.version, now)
      .await?;

    let user = self
      .user_store
      .user_store()
      .get_short_user(&RawGetUserOptions {
        id: run.user.id,
        time: None,
        now,
      })
      .await?;

    Ok(Run {
      id: run.id,
      created_at: run.created_at,
      started_at: run.started_at,
      result: run.result,
      game: run.game,
      channel: GameChannelKeyRef::new(run.channel),
      build: self.resolve_game_build(game).await.map_err(GetRunError::Other)?,
      user,
      game_mode: run.mode,
      game_options: run.options,
      settings: run.settings,
    })
  }

  pub async fn start_run(&self, acx: &AuthContext, run_id: RunId) -> Result<RunStart, StartRunError> {
    let actor = match acx {
      AuthContext::User(u) => u.user.as_ref(),
      _ => return Err(StartRunError::Forbidden),
    };

    let run_start = self
      .run_store
      .run_store()
      .start_run(&StoreStartRun {
        run: RunIdRef::new(run_id),
        if_user: actor,
      })
      .await?;

    // Fetch items and apply server-side quests
    let game_quests = self
      .game_store
      .game_store()
      .get_game_quests(run_start.game.id, run_start.version)
      .await
      .map_err(WeakError::wrap)?;

    let mut families = game_quests.default_families;
    let items = self
      .run_store
      .run_store()
      .get_user_items(&GetUserItems {
        user: run_start.created_by,
        game: run_start.game,
        channel: run_start.channel,
        include_archived_hfest_items: game_quests.uses_archived_hfest_items,
        until: run_start.started_at,
      })
      .await?;

    for reward in game_quests.quests.iter().flat_map(|q| q.rewards_for(&items)) {
      reward.apply_families(&mut families);
    }

    Ok(RunStart {
      run: run_start.run,
      key: run_start.key,
      families,
      items,
    })
  }

  pub async fn set_run_result(
    &self,
    acx: &AuthContext,
    run_id: RunId,
    result: SetRunResultOptions,
  ) -> Result<Run, SetRunResultError> {
    let now = self.clock.clock().now();
    let AuthContext::User(actor) = acx else {
      return Err(SetRunResultError::Forbidden);
    };

    let run = self
      .run_store
      .run_store()
      .set_run_result(&StoreSetRunResult {
        run: RunIdRef::new(run_id),
        if_user: actor.user.id.into(),
        is_victory: result.is_victory,
        max_level: result.max_level,
        scores: result.scores,
        items: result.items,
        stats: result.stats,
      })
      .await?;

    let (game, _) = self
      .retrieve_game(acx, run.game, &run.channel, run.version, now)
      .await?;

    // TODO: Reuse the auth-context instead of querying the DB
    let user = self
      .user_store
      .user_store()
      .get_short_user(&RawGetUserOptions {
        id: run.user.id,
        time: None,
        now,
      })
      .await?;

    Ok(Run {
      id: run.id,
      created_at: run.created_at,
      started_at: run.started_at,
      result: run.result,
      game: run.game,
      channel: GameChannelKeyRef::new(run.channel.clone()),
      build: self.resolve_game_build(game).await.map_err(SetRunResultError::Other)?,
      user,
      game_mode: run.mode,
      game_options: run.options,
      settings: run.settings,
    })
  }

  pub async fn get_user_items(
    &self,
    acx: &AuthContext,
    user_id: UserId,
    game_id: GameId,
    until: Option<Instant>,
  ) -> Result<BTreeMap<HammerfestItemId, u32>, GetUserInventoryError> {
    let now = self.clock.clock().now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (Some(u.user.as_ref()), u.is_tester || u.is_administrator),
      _ => (None, false),
    };
    let game = self
      .game_store
      .game_store()
      .get_game(&GetStoreGame {
        actor,
        is_tester,
        now,
        // Note: we don't use `until` here, to always get the latest game version
        // TODO: is this actually what we want?
        time: None,
        game: GameRef::id(game_id),
        channel: None,
      })
      .await
      .map_err(|_| GetUserInventoryError::GameNotFound)?;
    if !game.channels.active.is_enabled {
      return Err(GetUserInventoryError::ChannelNotFound);
    }

    let quests = self
      .game_store
      .game_store()
      .get_game_quests(game.id, game.channels.active.build.version)
      .await?;
    let items = self
      .run_store
      .run_store()
      .get_user_items(&GetUserItems {
        user: UserIdRef::new(user_id),
        channel: game.channels.active.key,
        game: GameIdRef::new(game_id),
        include_archived_hfest_items: quests.uses_archived_hfest_items,
        until: until.unwrap_or(now),
      })
      .await
      .map_err(GetUserInventoryError::InternalGetInventory)?;
    Ok(items)
  }

  pub async fn get_leaderboard(
    &self,
    acx: &AuthContext,
    game_ref: GameRef,
    game_mode: GameModeKey,
  ) -> Result<Leaderboard, GetLeaderboardError> {
    let now = self.clock.clock().now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (u.user.as_ref(), u.is_tester || u.is_administrator),
      _ => return Err(GetLeaderboardError::Forbidden),
    };
    let game = self
      .game_store
      .game_store()
      .get_game(&GetStoreGame {
        actor: Some(actor),
        is_tester,
        now,
        time: None,
        game: game_ref,
        channel: None,
      })
      .await?;
    if !game.channels.active.is_enabled {
      return Err(GetLeaderboardError::ChannelNotFound);
    }
    let leaderboard: StoreLeaderboard = self
      .run_store
      .run_store()
      .get_leaderboard(&StoreGetLeaderboard {
        game: GameIdRef::new(game.id),
        channel: game.channels.active.key,
        mode: game_mode,
      })
      .await?;
    let mut users: HashSet<UserId> = HashSet::new();
    for result in &leaderboard.results {
      users.insert(result.user.id);
    }
    let users = self
      .user_store
      .user_store()
      .get_short_users(&GetUsersOptions {
        id: users,
        time: None,
        now,
      })
      .await?;
    Ok(Leaderboard {
      game: leaderboard.game,
      channel: leaderboard.channel,
      mode: leaderboard.mode,
      results: leaderboard
        .results
        .into_iter()
        .map(|store_entry| LeaderboardEntry {
          score: store_entry.score,
          user: users.get(&store_entry.user.id).expect("user must be resolved").clone(),
          run: LeaderboardEntryRun {
            id: store_entry.run.id,
            max_level: store_entry.run.max_level,
            game_options: store_entry.run.game_options,
          },
        })
        .collect(),
    })
  }
}
