use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{Blob, BlobId, BlobStore, BlobStoreRef, GetBlobsError, GetBlobsOptions};
use eternalfest_core::clock::Clock;
use eternalfest_core::core::Listing;
use eternalfest_core::game::requests::{
  CreateGame, CreateGameBuild, GetGame, GetGames, SetGameFavorite, UpdateGameChannel,
};
use eternalfest_core::game::store::{
  CreateBuild, CreateStoreGame, CreateStoreGameError, GetStoreGame, GetStoreGameError, GetStoreShortGames,
  GetStoreShortGamesError, StoreSetGameFavorite, StoreSetGameFavoriteError,
  UpdateGameChannelError as StoreUpdateGameChannelError, UpdateStoreGameChannel,
};
use eternalfest_core::game::{
  ActiveGameChannel, Game, GameBuild, GameChannelKey, GameListItem, GameRef, GameStore, GameStoreRef,
  ResolveGameBuildError, Resolved, ShortGame,
};
use eternalfest_core::run::{GetUserItems, GetUserItemsError, RunStore, RunStoreRef};
use eternalfest_core::user::{GetUsersOptions, StoreGetShortUsersError, UserIdRef, UserStore, UserStoreRef};
use eternaltwin_core::clock::ClockRef;
use eternaltwin_core::types::WeakError;
use serde::{Deserialize, Serialize};
use std::collections::{HashMap, HashSet};
use std::sync::Arc;

pub struct GameService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStoreRef,
  TyClock: ClockRef,
  TyGameStore: GameStoreRef,
  TyRunStore: RunStoreRef,
  TyUserStore: UserStoreRef,
{
  #[allow(unused)]
  blob_store: TyBlobStore,
  clock: TyClock,
  game_store: TyGameStore,
  run_store: TyRunStore,
  user_store: TyUserStore,
}

pub type DynGameService =
  GameService<Arc<dyn BlobStore>, Arc<dyn Clock>, Arc<dyn GameStore>, Arc<dyn RunStore>, Arc<dyn UserStore>>;

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, thiserror::Error, Serialize, Deserialize)]
pub enum UpdateGameChannelError {
  #[error("missing permission for game channel update")]
  Forbidden,
  #[error("game not found {0:?}")]
  GameNotFound(GameRef),
  #[error("game channel not found ({0:?}, {1:?})")]
  GameChannelNotFound(GameRef, GameChannelKey),
  #[error("internal error while reading game blobs")]
  GetBlobs(#[from] GetBlobsError),
  #[error("internal error while resolving game build")]
  ResolveGameBuild(#[from] ResolveGameBuildError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum CreateGameError {
  #[error("current actor is not allowed to create games")]
  Forbidden,
  #[error("failed to create game in `GameStore`")]
  InternalCreateStoreGame(#[from] CreateStoreGameError),
  #[error("failed to get blobs from `BlobStore`")]
  InternalGetBlobs(#[from] GetBlobsError),
  #[error("failed to get users from `UserStore`")]
  InternalGetShortUsers(#[from] StoreGetShortUsersError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum CreateBuildError {
  #[error("current actor is not allowed to create games")]
  Forbidden,
  #[error("failed to create build in `GameStore`")]
  InternalCreateBuild(#[from] eternalfest_core::game::store::CreateBuildError),
  #[error("failed to get blobs from `BlobStore`")]
  InternalGetBlobs(#[from] GetBlobsError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetGameError {
  #[error("game not found")]
  NotFound,
  #[error("revoked view game permission")]
  PermissionRevoked,
  #[error("internal error while retrieving game")]
  InternalGetGame(GetStoreGameError),
  #[error("failed to get blobs from `BlobStore`")]
  InternalGetBlobs(#[from] GetBlobsError),
  #[error("failed to get users from `UserStore`")]
  InternalGetShortUsers(#[from] StoreGetShortUsersError),
  #[error("failed to get user items")]
  InternalGetUserItems(#[from] GetUserItemsError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<GetStoreGameError> for GetGameError {
  fn from(value: GetStoreGameError) -> Self {
    match value {
      GetStoreGameError::GameNotFound => Self::NotFound,
      GetStoreGameError::PermissionRevoked => Self::PermissionRevoked,
      e @ GetStoreGameError::Other(_) => Self::InternalGetGame(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetGamesError {
  #[error("internal error while retrieving short games")]
  InternalGetShortGames(#[from] GetStoreShortGamesError),
  #[error("failed to get blobs from `BlobStore`")]
  InternalGetBlobs(#[from] GetBlobsError),
  #[error("failed to get users from `UserStore`")]
  InternalGetShortUsers(#[from] StoreGetShortUsersError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum SetGameFavoriteError {
  #[error("setting game favorite is forbidden")]
  Forbidden,
  #[error("internal error while set favorite status for game")]
  InternalSetGameFavorite(#[from] StoreSetGameFavoriteError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
  GameService<TyBlobStore, TyClock, TyGameStore, TyRunStore, TyUserStore>
where
  TyBlobStore: BlobStoreRef,
  TyClock: ClockRef,
  TyGameStore: GameStoreRef,
  TyRunStore: RunStoreRef,
  TyUserStore: UserStoreRef,
{
  pub fn new(
    blob_store: TyBlobStore,
    clock: TyClock,
    game_store: TyGameStore,
    run_store: TyRunStore,
    user_store: TyUserStore,
  ) -> Self {
    Self {
      blob_store,
      clock,
      game_store,
      run_store,
      user_store,
    }
  }

  pub async fn create_game(&self, acx: &AuthContext, req: &CreateGame) -> Result<Game<Resolved>, CreateGameError> {
    let authorized_owner: Option<UserIdRef> = match acx {
      AuthContext::System(_) => match req.owner {
        Some(o) => Some(o),
        None => {
          return Err(CreateGameError::Other(WeakError::new(
            "Missing owner for system creation",
          )))
        }
      },
      AuthContext::Guest(_) => None,
      AuthContext::User(ref acx) => {
        let owner: UserIdRef = req.owner.unwrap_or_else(|| UserIdRef::from(acx.user.id));
        if acx.is_administrator || acx.user.id == owner.id {
          Some(owner)
        } else {
          None
        }
      }
    };
    let owner = match authorized_owner {
      Some(o) => o,
      None => return Err(CreateGameError::Forbidden),
    };
    let options = CreateStoreGame {
      owner,
      key: req.key.clone(),
      build: req.build.clone(),
      channels: req.channels.clone(),
    };
    let game = self.game_store.game_store().create_game(&options).await?;

    let users: HashSet<_> = game.users().map(|u| u.id).collect();
    let users = self
      .user_store
      .user_store()
      .get_short_users(&GetUsersOptions {
        id: users,
        now: game.created_at,
        time: None,
      })
      .await?;

    let blobs: HashSet<_> = game.blobs().map(|b| b.id).collect();
    let blobs = self
      .blob_store
      .blob_store()
      .get_blobs(&GetBlobsOptions { id: blobs, time: None })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    Ok(game.resolve(&blobs, &users))
  }

  pub async fn create_build(
    &self,
    acx: &AuthContext,
    req: &CreateGameBuild,
  ) -> Result<GameBuild<Resolved>, CreateBuildError> {
    let if_owner: Option<UserIdRef> = match acx {
      AuthContext::System(_) => None,
      AuthContext::Guest(_) => return Err(CreateBuildError::Forbidden),
      AuthContext::User(ref acx) => {
        if acx.is_administrator {
          None
        } else {
          Some(acx.user.as_ref())
        }
      }
    };
    let options = CreateBuild {
      game: req.game.clone(),
      if_owner,
      build: req.build.clone(),
    };
    let store_build = self.game_store.game_store().create_version(&options).await?;
    let blobs: HashSet<_> = store_build.blobs().map(|b| b.id).collect();
    let blobs = self
      .blob_store
      .blob_store()
      .get_blobs(&GetBlobsOptions { id: blobs, time: None })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    Ok(store_build.resolve(&blobs))
  }

  pub async fn update_channel(
    &self,
    acx: &AuthContext,
    req: &UpdateGameChannel,
  ) -> Result<ActiveGameChannel<Resolved>, UpdateGameChannelError> {
    // TODO: Add some check to restrict changes to channels that are public (or will be public)
    let (actor, if_owner): (UserIdRef, Option<UserIdRef>) = match acx {
      AuthContext::System(_) => return Err(UpdateGameChannelError::Forbidden),
      AuthContext::Guest(_) => return Err(UpdateGameChannelError::Forbidden),
      AuthContext::User(ref acx) => {
        let actor = acx.user.as_ref();
        if acx.is_administrator {
          (actor, None)
        } else {
          (actor, Some(actor))
        }
      }
    };
    let store_command = UpdateStoreGameChannel {
      actor,
      game: req.game.clone(),
      channel_key: req.channel_key.clone(),
      if_owner,
      patches: req.patches.clone(),
    };
    let (store_channel, _) = match self.game_store.game_store().update_game_channel(&store_command).await {
      Ok(res) => res,
      Err(StoreUpdateGameChannelError::GameNotFound(game)) => return Err(UpdateGameChannelError::GameNotFound(game)),
      Err(StoreUpdateGameChannelError::GameChannelNotFound(game, channel)) => {
        return Err(UpdateGameChannelError::GameChannelNotFound(game, channel))
      }
      Err(StoreUpdateGameChannelError::NotOwner(_)) => return Err(UpdateGameChannelError::Forbidden),
      Err(StoreUpdateGameChannelError::Other(e)) => return Err(UpdateGameChannelError::Other(WeakError::new(e))),
    };
    let blobs: HashSet<_> = store_channel.blobs().map(|b| b.id).collect();
    let blobs = self
      .blob_store
      .blob_store()
      .get_blobs(&GetBlobsOptions { id: blobs, time: None })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    Ok(store_channel.resolve(&blobs))
  }

  pub async fn get_game(&self, acx: &AuthContext, req: &GetGame) -> Result<Game<Resolved>, GetGameError> {
    let now = self.clock.clock().now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (Some(u.user.as_ref()), u.is_tester || u.is_administrator),
      _ => (None, false),
    };

    let store_game = self
      .game_store
      .game_store()
      .get_game(&GetStoreGame {
        actor,
        now,
        is_tester,
        time: req.time,
        game: req.game.clone(),
        channel: req.channel.clone(),
      })
      .await?;

    let users: HashSet<_> = store_game.users().map(|u| u.id).collect();
    let users = self
      .user_store
      .user_store()
      .get_short_users(&GetUsersOptions {
        id: users,
        now,
        time: req.time,
      })
      .await?;

    let blobs: HashSet<_> = store_game.blobs().map(|b| b.id).collect();
    let blobs = self
      .blob_store
      .blob_store()
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        time: req.time,
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    let mut game = store_game.resolve(&blobs, &users);
    // There's no good reason to do this, except to maintain parity with previous code.
    // TODO: keep the channel list.
    game.channels.items.clear();

    // Apply server-side quests (code duplicated with `RunService`, it'd be nice to get rid of that)
    let build = &mut game.channels.active.build;
    let quests = self
      .game_store
      .game_store()
      .get_game_quests(game.id, build.version)
      .await
      .map_err(GetGameError::Other)?;
    if let Some(user) = actor.filter(|_| !quests.quests.is_empty()) {
      let items = self
        .run_store
        .run_store()
        .get_user_items(&GetUserItems {
          user: user.id.into(),
          game: game.id.into(),
          channel: game.channels.active.key.clone(),
          include_archived_hfest_items: quests.uses_archived_hfest_items,
          until: req.time.unwrap_or(now),
        })
        .await?;

      for reward in quests.quests.iter().flat_map(|q| q.rewards_for(&items)) {
        reward.apply(build);
      }
    }

    Ok(game)
  }

  pub async fn get_games(&self, acx: &AuthContext, req: &GetGames) -> Result<Listing<GameListItem>, GetGamesError> {
    let now = self.clock.clock().now();
    let (actor, is_tester) = match acx {
      AuthContext::User(u) => (Some(u.user.as_ref()), u.is_tester || u.is_administrator),
      _ => (None, false),
    };

    let store_games = self
      .game_store
      .game_store()
      .get_short_games(&GetStoreShortGames {
        actor,
        is_tester,
        favorite: req.favorite,
        offset: req.offset,
        limit: req.limit,
        now,
        time: req.time,
      })
      .await?;

    let users = self
      .user_store
      .user_store()
      .get_short_users(&GetUsersOptions {
        id: store_games
          .items
          .iter()
          .flatten()
          .flat_map(|g| g.users())
          .map(|u| u.id)
          .collect(),
        now,
        time: req.time,
      })
      .await?;

    let blobs: HashSet<_> = store_games
      .items
      .iter()
      .flatten()
      .flat_map(ShortGame::blobs)
      .map(|b| b.id)
      .collect();
    let blobs = self
      .blob_store
      .blob_store()
      .get_blobs(&GetBlobsOptions {
        id: blobs,
        time: req.time,
      })
      .await?;
    let blobs: HashMap<BlobId, Blob> = blobs
      .into_iter()
      .map(|(blob_id, blob_result)| (blob_id, blob_result.expect("blob referenced by game must exist")))
      .collect();

    Ok(store_games.map(|g| g.map(|g| g.resolve(&blobs, &users))))
  }

  pub async fn set_game_favorite(
    &self,
    acx: &AuthContext,
    req: &SetGameFavorite,
  ) -> Result<bool, SetGameFavoriteError> {
    let user = match acx {
      AuthContext::User(ref acx) => Some(acx.user.as_ref()),
      _ => None,
    };
    let user = user.ok_or(SetGameFavoriteError::Forbidden)?;

    let options = StoreSetGameFavorite {
      user,
      game: req.game.clone(),
      favorite: req.favorite,
    };
    self.game_store.game_store().set_game_favorite(&options).await?;
    Ok(req.favorite)
  }
}
