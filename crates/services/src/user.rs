use eternalfest_core::auth::AuthContext;
use eternalfest_core::clock::{Clock, ClockRef};
use eternalfest_core::user::{
  GetUsersOptions, RawGetUserError, RawGetUserOptions, ShortUser, StoreGetShortUserError, StoreGetShortUsersError,
  StoreGetUsersError, StoreUpsertFromEtwinError, UpdateUserOptions, User, UserId, UserListing, UserStore, UserStoreRef,
};
use eternaltwin_core::core::Instant;
use eternaltwin_core::types::WeakError;
use std::collections::HashMap;
use std::sync::Arc;

pub struct UserService<TyClock, TyUserStore>
where
  TyClock: ClockRef,
  TyUserStore: UserStoreRef,
{
  clock: TyClock,
  user_store: TyUserStore,
}

pub type DynUserService = UserService<Arc<dyn Clock>, Arc<dyn UserStore>>;

#[derive(Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct GetUser {
  pub id: UserId,
  pub time: Option<Instant>,
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum UpsertFromEtwinError {
  #[error("internal error while upserting Eternaltwin user")]
  InternalUpsertFromEtwin(#[from] StoreUpsertFromEtwinError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum GetUserError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<RawGetUserError> for GetUserError {
  fn from(value: RawGetUserError) -> Self {
    match value {
      RawGetUserError::NotFound => Self::NotFound,
      RawGetUserError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum GetUserRefError {
  #[error("user not found")]
  NotFound,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<StoreGetShortUserError> for GetUserRefError {
  fn from(value: StoreGetShortUserError) -> Self {
    match value {
      StoreGetShortUserError::NotFound => Self::NotFound,
      StoreGetShortUserError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum GetUserRefsError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<StoreGetShortUsersError> for GetUserRefsError {
  fn from(value: StoreGetShortUsersError) -> Self {
    match value {
      StoreGetShortUsersError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum GetUsersError {
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl From<StoreGetUsersError> for GetUsersError {
  fn from(value: StoreGetUsersError) -> Self {
    match value {
      StoreGetUsersError::Other(e) => Self::Other(e),
    }
  }
}

#[derive(Debug, Clone, PartialEq, Eq, serde::Serialize, serde::Deserialize, thiserror::Error)]
pub enum UpdateUserError {
  #[error("user not found")]
  NotFound,
  #[error("forbidden")]
  Forbidden,
  #[error(transparent)]
  Other(#[from] WeakError),
}

impl<TyClock, TyUserStore> UserService<TyClock, TyUserStore>
where
  TyClock: ClockRef,
  TyUserStore: UserStoreRef,
{
  pub fn new(clock: TyClock, user_store: TyUserStore) -> Self {
    Self { clock, user_store }
  }

  pub async fn upsert_from_etwin(&self, _acx: &AuthContext, user: &ShortUser) -> Result<User, UpsertFromEtwinError> {
    Ok(self.user_store.user_store().upsert_from_etwin(user).await?)
  }

  pub async fn get_user(&self, _acx: &AuthContext, query: &GetUser) -> Result<User, GetUserError> {
    let now = self.clock.clock().now();
    self
      .user_store
      .user_store()
      .get_user(&RawGetUserOptions {
        id: query.id,
        now,
        time: query.time,
      })
      .await
      .map_err(GetUserError::from)
  }

  pub async fn get_user_ref(
    &self,
    _acx: &AuthContext,
    options: &RawGetUserOptions,
  ) -> Result<ShortUser, GetUserRefError> {
    Ok(self.user_store.user_store().get_short_user(options).await?)
  }

  pub async fn get_user_refs(
    &self,
    _acx: &AuthContext,
    options: &GetUsersOptions,
  ) -> Result<HashMap<UserId, ShortUser>, GetUserRefsError> {
    Ok(self.user_store.user_store().get_short_users(options).await?)
  }

  pub async fn get_users(&self, _acx: &AuthContext) -> Result<UserListing, GetUsersError> {
    Ok(self.user_store.user_store().get_users().await?)
  }

  pub async fn update_user(&self, acx: &AuthContext, options: &UpdateUserOptions) -> Result<User, UpdateUserError> {
    let authorized = match acx {
      AuthContext::System(_) => true,
      AuthContext::Guest(_) => false,
      AuthContext::User(ref user) => user.is_administrator,
    };
    if !authorized {
      return Err(UpdateUserError::Forbidden);
    }
    Ok(
      self
        .user_store
        .user_store()
        .update_user(options)
        .await
        .map_err(WeakError::wrap)?,
    )
  }
}
