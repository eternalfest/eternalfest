use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{
  Blob, BlobStore, BlobStoreRef, CreateBlobOptions, CreateStoreBlobError, CreateStoreUploadSessionError,
  CreateUploadSessionOptions, FullBlob, GetBlobDataOptions, GetBlobOptions, GetStoreBlobDataError, GetStoreBlobError,
  UploadError, UploadOptions, UploadSession,
};
use eternalfest_core::clock::Clock;
use eternaltwin_core::clock::ClockRef;
use eternaltwin_core::types::WeakError;
use std::sync::Arc;

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum CreateBlobError {
  #[error("current actor is not allowed to create blobs")]
  Forbidden,
  #[error("failed to create blob in `BlobStore`")]
  InternalCreateStoreBlob(#[from] CreateStoreBlobError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetBlobError {
  #[error("current actor is not allowed to read blobs")]
  Forbidden,
  #[error("failed to get blob in `BlobStore`")]
  InternalGetStoreBlob(#[from] GetStoreBlobError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum GetBlobDataError {
  #[error("current actor is not allowed to read blob data")]
  Forbidden,
  #[error("failed to get blob data in `BlobStore`")]
  InternalGetStoreBlobData(#[from] GetStoreBlobDataError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum CreateUploadSessionError {
  #[error("current actor is not allowed to create upload sessions")]
  Forbidden,
  #[error("failed to create upload session in `BlobStore`")]
  InternalCreateStoreUploadSession(#[from] CreateStoreUploadSessionError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

#[derive(Debug, Clone, PartialEq, Eq, thiserror::Error)]
pub enum UpdateUploadSessionError {
  #[error("current actor is not allowed to upload data to sessions")]
  Forbidden,
  #[error("failed to upload data to session in `BlobStore`")]
  InternalUpload(#[from] UploadError),
  #[error(transparent)]
  Other(#[from] WeakError),
}

pub struct BlobService<TyBlobStore, TyClock>
where
  TyBlobStore: BlobStoreRef,
  TyClock: ClockRef,
{
  blob_store: TyBlobStore,
  #[allow(unused)]
  clock: TyClock,
}

pub type DynBlobService = BlobService<Arc<dyn BlobStore>, Arc<dyn Clock>>;

impl<TyBlobStore, TyClock> BlobService<TyBlobStore, TyClock>
where
  TyBlobStore: BlobStoreRef,
  TyClock: ClockRef,
{
  pub fn new(blob_store: TyBlobStore, clock: TyClock) -> Self {
    Self { blob_store, clock }
  }

  pub async fn create_blob(&self, acx: &AuthContext, cmd: &CreateBlobOptions) -> Result<Blob, CreateBlobError> {
    let is_authorized: bool = match acx {
      AuthContext::System(_) => true,
      AuthContext::Guest(_) => false,
      AuthContext::User(ref acx) => acx.is_tester,
    };
    if !is_authorized {
      return Err(CreateBlobError::Forbidden);
    }
    Ok(self.blob_store.blob_store().create_blob(cmd).await?)
  }

  pub async fn get_blob(&self, _acx: &AuthContext, query: &GetBlobOptions) -> Result<Blob, GetBlobError> {
    Ok(self.blob_store.blob_store().get_blob(query).await?)
  }

  pub async fn get_blob_data(
    &self,
    _acx: &AuthContext,
    query: &GetBlobDataOptions,
  ) -> Result<FullBlob, GetBlobDataError> {
    Ok(self.blob_store.blob_store().get_blob_data(query).await?)
  }

  pub async fn create_upload_session(
    &self,
    acx: &AuthContext,
    cmd: &CreateUploadSessionOptions,
  ) -> Result<UploadSession, CreateUploadSessionError> {
    let is_authorized: bool = match acx {
      AuthContext::System(_) => true,
      AuthContext::Guest(_) => false,
      AuthContext::User(ref acx) => acx.is_tester,
    };
    if !is_authorized {
      return Err(CreateUploadSessionError::Forbidden);
    }
    Ok(self.blob_store.blob_store().create_upload_session(cmd).await?)
  }

  pub async fn update_upload_session(
    &self,
    acx: &AuthContext,
    cmd: &UploadOptions,
  ) -> Result<UploadSession, UpdateUploadSessionError> {
    let is_authorized: bool = match acx {
      AuthContext::System(_) => true,
      AuthContext::Guest(_) => false,
      AuthContext::User(ref acx) => acx.is_tester,
    };
    if !is_authorized {
      return Err(UpdateUploadSessionError::Forbidden);
    }
    Ok(self.blob_store.blob_store().upload(cmd).await?)
  }
}
