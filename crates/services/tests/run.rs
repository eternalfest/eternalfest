use eternaltwin_client::http::HttpEternaltwinClient;
use serial_test::serial;
use sqlx::postgres::{PgConnectOptions, PgPoolOptions};
use sqlx::PgPool;
use std::str::FromStr;
use std::sync::Arc;

use crate::shared::create_sous_la_colline;
use eternalfest_auth_store::pg::PgAuthStore;
use eternalfest_blob_store::pg::PgBlobStore;
use eternalfest_buffer_store::fs::FsBufferStore;
use eternalfest_core::auth::{AuthContext, AuthStore, UserAuthContext};
use eternalfest_core::blob::{BlobStore, BlobStoreRef};
use eternalfest_core::buffer::BufferStore;
use eternalfest_core::clock::VirtualClock;
use eternalfest_core::core::{BoundedVec, Duration, Instant};
use eternalfest_core::game::{GameChannelKeyRef, GameStore};
use eternalfest_core::run::{CreateRunOptions, Run, RunResult, RunSettings, RunStart, RunStore, SetRunResultOptions};
use eternalfest_core::user::UserStore;
use eternalfest_core::uuid::Uuid4Generator;
use eternalfest_db_schema::force_create_latest;
use eternalfest_game_store::pg::PgGameStore;
use eternalfest_run_store::pg::PgRunStore;
use eternalfest_services::auth::{AuthService, DynAuthService};
use eternalfest_services::game::{DynGameService, GameService};
use eternalfest_services::run::{DynRunService, RunService};
use eternalfest_user_store::pg::PgUserStore;
use eternaltwin_core::clock::Clock;
use eternaltwin_core::core::{LocaleId, SecretBytes};
use eternaltwin_core::user::UserId;
use eternaltwin_oauth_client::mem::MemRfcOauthClient;

mod shared;

async fn make_test_api() -> TestApi {
  let config = eternalfest_config::Config::for_test();
  let admin_database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.postgres.host.value)
        .port(config.postgres.port.value)
        .database(&config.postgres.name.value)
        .username(&config.postgres.admin_user.value)
        .password(&config.postgres.admin_password.value),
    )
    .await
    .unwrap();
  force_create_latest(&admin_database, true).await.unwrap();
  admin_database.close().await;

  let etwin_url = "http://eternaltwin.localhost".parse().unwrap();

  let database: PgPool = PgPoolOptions::new()
    .max_connections(5)
    .connect_with(
      PgConnectOptions::new()
        .host(&config.postgres.host.value)
        .port(config.postgres.port.value)
        .database(&config.postgres.name.value)
        .username(&config.postgres.user.value)
        .password(&config.postgres.password.value),
    )
    .await
    .unwrap();
  let database = Arc::new(database);

  let clock = Arc::new(VirtualClock::new(Instant::ymd_hms(2020, 1, 1, 0, 0, 0)));
  let auth_internal_key = SecretBytes::new(b"dev_secret".to_vec());
  let jwt_secret = SecretBytes::new(b"dev_secret".to_vec());

  let uuid_generator = Arc::new(Uuid4Generator);
  let buffer_store: Arc<dyn BufferStore> =
    Arc::new(FsBufferStore::new(Arc::clone(&clock), Arc::clone(&uuid_generator), config.data_root()).await);
  let blob_store: Arc<dyn BlobStore> = Arc::new(
    PgBlobStore::new(
      Arc::clone(&buffer_store),
      Arc::clone(&clock),
      Arc::clone(&database),
      Arc::clone(&uuid_generator),
    )
    .await
    .expect("failed to create `PgBlobStore`"),
  );
  let game_store: Arc<dyn GameStore> =
    Arc::new(PgGameStore::new(Arc::clone(&clock), Arc::clone(&database), Arc::clone(&uuid_generator)).await);
  let user_store: Arc<dyn UserStore> = Arc::new(PgUserStore::new(Arc::clone(&clock), Arc::clone(&database)));
  let run_store: Arc<dyn RunStore> = Arc::new(
    PgRunStore::new(
      Arc::clone(&clock),
      Arc::clone(&database),
      Arc::clone(&uuid_generator),
      Clone::clone(&etwin_url),
    )
    .expect("failed to create `PgRunStore`"),
  );
  let game = Arc::new(GameService::new(
    Arc::clone(&blob_store),
    Arc::clone(&clock) as Arc<dyn Clock>,
    Arc::clone(&game_store),
    Arc::clone(&run_store),
    Arc::clone(&user_store),
  ));

  let auth_store: Arc<dyn AuthStore> = Arc::new(PgAuthStore::new(
    Arc::clone(&clock),
    Arc::clone(&database),
    Arc::clone(&uuid_generator),
  ));

  // TODO: `with_etwin_test_server`, etc.
  let eternaltwin_client = HttpEternaltwinClient::new(Arc::clone(&clock), etwin_url);
  let eternaltwin_oauth_client = MemRfcOauthClient::new();

  let auth: Arc<DynAuthService> = Arc::new(AuthService::new(
    auth_store,
    Arc::clone(&clock) as Arc<dyn Clock>,
    Arc::new(eternaltwin_client),
    Arc::new(eternaltwin_oauth_client),
    Arc::clone(&user_store),
    auth_internal_key,
    jwt_secret,
  ));

  let run: Arc<DynRunService> = Arc::new(RunService::new(
    Arc::clone(&blob_store),
    Arc::clone(&clock) as Arc<dyn Clock>,
    game_store,
    run_store,
    user_store,
  ));

  TestApi {
    auth,
    blob: blob_store,
    clock,
    game,
    run,
  }
}

struct TestApi {
  pub(crate) auth: Arc<DynAuthService>,
  pub(crate) blob: Arc<dyn BlobStore>,
  pub(crate) clock: Arc<VirtualClock>,
  pub(crate) game: Arc<DynGameService>,
  pub(crate) run: Arc<DynRunService>,
  // pub(crate) user: Arc<DynUserService>,
}

#[tokio::test]
#[serial]
async fn test_negative_score() {
  inner_test_negative_score(make_test_api().await).await;
}

async fn inner_test_negative_score(api: TestApi) {
  api.clock.as_ref().advance_to(Instant::ymd_hms(2021, 1, 1, 0, 0, 0));
  let alice_id = UserId::from_str("00000000-0000-0000-0001-000000000001").unwrap();
  let alice_acx: UserAuthContext = api
    .auth
    .as_ref()
    .authenticate_for_test(alice_id, &"Alice".parse().unwrap())
    .await
    .unwrap()
    .to_auth_context();
  let alice_acx = AuthContext::User(alice_acx);

  api.clock.advance_by(Duration::from_seconds(1));

  let game = create_sous_la_colline(&api.clock, api.blob.blob_store(), &api.game, &alice_acx).await;
  let build = game.channels.active.build;

  api.clock.advance_by(Duration::from_seconds(1));

  let run = api
    .run
    .as_ref()
    .create_run(
      &alice_acx,
      &CreateRunOptions {
        game: game.id.into(),
        channel: game.channels.active.key,
        version: build.version,
        user: alice_id.into(),
        game_mode: "solo".parse().unwrap(),
        game_options: vec![],
        settings: RunSettings {
          detail: false,
          shake: false,
          sound: false,
          music: false,
          volume: 0,
          locale: LocaleId::FrFr,
        },
      },
    )
    .await
    .unwrap();

  api.clock.advance_by(Duration::from_seconds(1));

  let run_start = api.run.as_ref().start_run(&alice_acx, run.id).await.unwrap();

  {
    let expected = RunStart {
      run: run.id.into(),
      key: Default::default(),
      families: "1,2,3,4".parse().unwrap(),
      items: Default::default(),
    };

    assert_eq!(run_start, expected);
  }

  api.clock.advance_by(Duration::from_seconds(1));

  let actual = api
    .run
    .as_ref()
    .set_run_result(
      &alice_acx,
      run.id,
      SetRunResultOptions {
        is_victory: true,
        max_level: 2,
        scores: BoundedVec::new(vec![-1000]).unwrap(),
        items: Default::default(),
        stats: Default::default(),
      },
    )
    .await
    .unwrap();

  let expected = Run {
    id: run.id,
    created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 3),
    started_at: Some(Instant::ymd_hms(2021, 1, 1, 0, 0, 4)),
    result: Some(RunResult {
      created_at: Instant::ymd_hms(2021, 1, 1, 0, 0, 5),
      is_victory: true,
      max_level: 2,
      scores: BoundedVec::new(vec![-1000]).unwrap(),
      items: Default::default(),
      stats: Default::default(),
    }),
    game: game.id.into(),
    channel: GameChannelKeyRef::new("main".parse().unwrap()),
    build: build.clone(),
    user: alice_acx.as_user().unwrap().user.clone(),
    game_mode: "solo".parse().unwrap(),
    game_options: vec![],
    settings: RunSettings {
      detail: false,
      shake: false,
      sound: false,
      music: false,
      volume: 0,
      locale: LocaleId::FrFr,
    },
  };
  assert_eq!(actual, expected);
}
