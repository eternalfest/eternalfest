use eternalfest_core::auth::AuthContext;
use eternalfest_core::blob::{Blob, BlobStore, CreateBlobOptions};
use eternalfest_core::core::{BoundedVec, Duration};
use eternalfest_core::game::requests::CreateGame;
use eternalfest_core::game::{
  Game, GameBuildI18n, GameCategory, GameChannelPermission, GameEngine, GameModeSpec, GameModeSpecI18n, GameOptionKey,
  GameOptionSpec, GameOptionSpecI18n, GamePatcher, GameResource, InputGameBuild, InputGameChannel, JsonValue,
  PatcherFramework, Resolved,
};
use eternalfest_services::game::DynGameService;
use eternaltwin_core::clock::VirtualClock;
use eternaltwin_core::core::LocaleId;
use once_cell::sync::Lazy;
use std::path::{Path, PathBuf};
use std::str::FromStr;

pub async fn upload_test_resource<P: AsRef<Path>>(blob: &dyn BlobStore, _acx: &AuthContext, path: P) -> Blob {
  pub static RESOURCES_ROOT: Lazy<PathBuf> =
    Lazy::new(|| PathBuf::from(std::env::var("CARGO_MANIFEST_DIR").unwrap()).join("../../test-resources"));
  let path = RESOURCES_ROOT.join(path);
  let path = path.as_path();
  let media_type = match path.extension().map(|e| e.to_str().expect("malformed file extension")) {
    Some("json") => "application/json",
    Some("mp3") => "audio/mp3",
    Some("png") => "image/png",
    Some("swf") => "application/x-shockwave-flash",
    Some("xml") => "application/xml",
    Some(_) => panic!("unknown file extension"),
    None => panic!("file extension not found"),
  };
  let data = match std::fs::read(path) {
    Ok(data) => data,
    Err(e) => panic!("failed to read {path:?}: {e:?}"),
  };
  blob
    .create_blob(&CreateBlobOptions {
      media_type: media_type.parse().expect("failed to parse media type"),
      data,
    })
    .await
    .expect("failed to create blob")
}

pub async fn create_sous_la_colline(
  clock: &VirtualClock,
  blob: &dyn BlobStore,
  game: &DynGameService,
  acx: &AuthContext,
) -> Game<Resolved> {
  let icon: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/icon.png").await;
  let icon_en: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/icon.en-US.png").await;
  let engine: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/game.swf").await;
  let patcher: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/patchman.swf").await;
  let debug: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/debug.json").await;
  let content: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/game.xml").await;
  let music: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/music/rourou.mp3").await;
  let lang: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/lang.fr-FR.xml").await;
  let lang_en: Blob = upload_test_resource(blob, acx, "games/sous-la-colline/lang.en-US.xml").await;

  clock.advance_by(Duration::from_seconds(1));

  game
    .create_game(
      acx,
      &CreateGame {
        owner: None,
        key: None,
        build: InputGameBuild {
          version: "1.0.0".parse().unwrap(),
          git_commit_ref: Some("ca11ab1ef01dab1ef005ba11ba5eba11b01dface".parse().unwrap()),
          main_locale: LocaleId::FrFr,
          display_name: "Sous la colline".parse().unwrap(),
          description: "Aidez Igor".parse().unwrap(),
          icon: Some(icon.as_ref()),
          loader: "4.1.0".parse().unwrap(),
          engine: GameEngine::custom(engine.as_ref()),
          patcher: Some(GamePatcher {
            blob: patcher.as_ref(),
            framework: PatcherFramework {
              name: "patchman".parse().unwrap(),
              version: "0.10.11".parse().unwrap(),
            },
            meta: Some(JsonValue::Object(Default::default())),
          }),
          debug: Some(debug.as_ref()),
          content: Some(content.as_ref()),
          content_i18n: Some(lang.as_ref()),
          musics: vec![GameResource {
            blob: music.as_ref(),
            display_name: Some("Chanson de rou²".parse().unwrap()),
          }],
          modes: [(
            "solo".parse().unwrap(),
            GameModeSpec {
              display_name: "Aventure".parse().unwrap(),
              is_visible: true,
              options: [(
                GameOptionKey::from_str("boost").unwrap(),
                GameOptionSpec {
                  display_name: "Tornade".parse().unwrap(),
                  is_visible: true,
                  is_enabled: false,
                  default_value: true,
                },
              )]
              .into_iter()
              .collect(),
            },
          )]
          .into_iter()
          .collect(),
          families: "1,2,3,4".parse().unwrap(),
          category: GameCategory::Lab,
          i18n: [(
            LocaleId::EnUs,
            GameBuildI18n {
              display_name: Some("Under the hill".parse().unwrap()),
              description: Some("Help Igor".parse().unwrap()),
              icon: Some(icon_en.as_ref()),
              content_i18n: Some(lang_en.as_ref()),
              modes: [(
                "solo".parse().unwrap(),
                GameModeSpecI18n {
                  display_name: Some("Adventure".parse().unwrap()),
                  options: [(
                    "boost".parse().unwrap(),
                    GameOptionSpecI18n {
                      display_name: Some("Tornado".parse().unwrap()),
                    },
                  )]
                  .into_iter()
                  .collect(),
                },
              )]
              .into_iter()
              .collect(),
            },
          )]
          .into_iter()
          .collect(),
        },
        channels: BoundedVec::new(vec![InputGameChannel {
          key: "main".parse().unwrap(),
          is_enabled: true,
          default_permission: GameChannelPermission::None,
          is_pinned: false,
          publication_date: None,
          sort_update_date: None,
          version: "1.0.0".parse().unwrap(),
          patches: vec![],
        }])
        .unwrap(),
      },
    )
    .await
    .unwrap()
}
