# Eternalfest

Main Eternalfest repository.

This README is not up-to-date and needs some work...

Upgrade crates using [cargo-edit](https://github.com/killercup/cargo-edit):

```
cargo upgrade --incompatible
```

## Get Started

```sh
npm run start
```

- `yarn up '*' '@!(eternalfest)/*'`: Update all Typescript dependencies.

## Database config

[See Eternaltwin documentation](https://eternaltwin.org/docs/db)
