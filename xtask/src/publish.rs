use clap::Parser;
use std::error::Error;
use std::fs;
use std::path::PathBuf;
use std::process::Command;
use std::thread::sleep;
use std::time::Duration;

const CRATES: [&str; 12] = [
  "config",
  "db_schema",
  "core",
  "user_store",
  "buffer_store",
  "blob_store",
  "file_store",
  "game_store",
  "auth_store",
  "run_store",
  "services",
  "cli",
];

const NPM_PACKAGES: [&str; 1] = [
  // "core",
  // "local-config",
  // "eternalfest-db",
  // "native",
  // "rest-server",
  "eternaldev",
];

/// Arguments to the `publish` task.
#[derive(Debug, Parser)]
pub struct PublishArgs {
  #[clap(long)]
  skip_rust: bool,
}

pub fn publish(args: &PublishArgs) -> Result<(), Box<dyn Error>> {
  let working_dir = std::env::current_dir()?;
  let crates_dir = working_dir.join("crates");
  let packages_dir = working_dir.join("packages");

  if !args.skip_rust {
    for c in CRATES {
      eprintln!("Publishing: {c}");
      let mut cmd = Command::new("cargo");
      cmd.current_dir(crates_dir.join(c));
      cmd.arg("publish");
      let s = cmd.status()?;
      assert!(s.success());
      sleep(Duration::from_secs(30));
    }
  }

  for p in NPM_PACKAGES {
    eprintln!("Publishing: {p}");
    let dir = packages_dir.join(p);
    match p {
      "eternalfest-db" => publish_npm_eternalfest_db(dir)?,
      "native" => publish_npm_native(dir)?,
      _ => {
        let mut cmd = Command::new("yarn");
        cmd.current_dir(dir);
        cmd.arg("npm").arg("publish");
        let s = cmd.status()?;
        assert!(s.success());
        sleep(Duration::from_secs(30));
      }
    }
  }

  Ok(())
}

fn publish_npm_native(pkg_dir: PathBuf) -> Result<(), Box<dyn Error>> {
  let dev_toml = fs::read_to_string(pkg_dir.join("native/Cargo.toml"))?;
  let dev_package_json = fs::read_to_string(pkg_dir.join("package.json"))?;

  let publish_toml = dev_toml.replace("\n# [workspace]\n", "\n[workspace]\n");
  assert_ne!(publish_toml, dev_toml);
  let publish_package_json = dev_package_json.replace("\"//install\"", "\"install\"");
  assert_ne!(publish_package_json, dev_package_json);

  fs::write(pkg_dir.join("native/Cargo.toml"), publish_toml)?;
  fs::write(pkg_dir.join("package.json"), publish_package_json)?;

  let mut cmd = Command::new("yarn");
  cmd.current_dir(&pkg_dir);
  cmd.arg("pack");
  let s = cmd.status()?;
  assert!(s.success());
  let mut cmd = Command::new("npm");
  cmd.current_dir("/");
  cmd.arg("publish").arg(pkg_dir.join("package.tgz"));
  let s = cmd.status()?;
  assert!(s.success());
  sleep(Duration::from_secs(30));

  fs::remove_file(pkg_dir.join("native/Cargo.lock"))?;
  fs::remove_file(pkg_dir.join("package.tgz"))?;
  fs::write(pkg_dir.join("native/Cargo.toml"), dev_toml)?;
  fs::write(pkg_dir.join("package.json"), dev_package_json)?;
  Ok(())
}

fn publish_npm_eternalfest_db(pkg_dir: PathBuf) -> Result<(), Box<dyn Error>> {
  let scripts = pkg_dir.join("scripts");
  let scripts_tmp = pkg_dir.join("scripts-tmp");
  fs::rename(&scripts, &scripts_tmp)?;
  fs::create_dir(&scripts)?;
  let mut options = fs_extra::dir::CopyOptions::new();
  options.content_only = true;
  fs_extra::dir::copy(&scripts_tmp, &scripts, &options)?;

  let mut cmd = Command::new("yarn");
  cmd.current_dir(&pkg_dir);
  cmd.arg("npm").arg("publish");
  let s = cmd.status()?;
  assert!(s.success());
  sleep(Duration::from_secs(30));

  fs::remove_dir_all(&scripts)?;
  fs::rename(&scripts_tmp, &scripts)?;
  Ok(())
}
