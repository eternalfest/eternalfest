use clap::Parser;
use std::error::Error;
use xtask::PublishArgs;

#[derive(Debug, Parser)]
#[clap(author = "Charles \"Demurgos\" Samborski")]
struct CliArgs {
  #[clap(subcommand)]
  task: Task,
}

#[derive(Debug, Parser)]
enum Task {
  /// Publish the CLI and all its dependencies
  #[clap(name = "publish")]
  Publish(PublishArgs),
}

/// Arguments to the `docs` task.
#[derive(Debug, Parser)]
struct DocsArgs {}

/// Arguments to the `kotlin` task.
#[derive(Debug, Parser)]
struct KotlinArgs {}

fn main() {
  let args: CliArgs = CliArgs::parse();

  let res = match &args.task {
    Task::Publish(ref args) => publish(args),
  };

  match res {
    Ok(_) => std::process::exit(0),
    Err(_) => res.unwrap(),
  }
}

fn publish(args: &PublishArgs) -> Result<(), Box<dyn Error>> {
  xtask::publish(args)
}
