--- Checks that the `run_result.score` field is an array of between 1 and 100 Sint32. (before, this was 1-2)
CREATE OR REPLACE FUNCTION test_run_result_scores(
  IN scores JSON
)
  RETURNS BOOLEAN
  LANGUAGE SQL
  IMMUTABLE
AS
$$
SELECT
    json_typeof($1) = 'array'
    AND 1 <= json_array_length($1)
    AND json_array_length($1) <= 100
    AND false NOT IN (
    SELECT
      (
          json_typeof(score.value) = 'number'
          AND -2147483648 <= score.value::TEXT::INT
          AND score.value::TEXT::INT < 2147483648
        ) AS is_ok
    FROM json_array_elements($1) AS score
  );
$$;

-- Indexes to speed up items & leaderboard queries.
CREATE INDEX idx_runs_by_user ON runs(user_id, game_id);
CREATE INDEX idx_runs_by_game ON runs(game_id, user_id);
