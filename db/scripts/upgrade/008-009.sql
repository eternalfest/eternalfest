COMMENT ON SCHEMA public IS '{"version": "V009"}';

ALTER TABLE games
  ADD COLUMN key VARCHAR(32) NULL,
  ADD CONSTRAINT game_key__uniq UNIQUE(key);
