-- CREATE DOMAIN buffer_id AS VARCHAR(64);

CREATE TABLE buffer (
  -- Internal buffer id (never exposed publicly)
  buffer_id BUFFER_ID NOT NULL,
  period PERIOD_LOWER NOT NULL,
  size U32 NOT NULL,
  digest_sha2_256 BYTEA NOT NULL,
  digest_sha3_256 BYTEA NOT NULL,
  -- Predicate indicating that the buffer is in active use
  _alive BOOLEAN NOT NULL,
  -- If `_alive`, must be `NULL`
  -- If `NOT _alive`, indicates when the buffer was freed (must be after `UPPER(period)`)
  freed_at INSTANT NULL,
  PRIMARY KEY (buffer_id),
  CONSTRAINT check_digest_sha2_256 CHECK (LENGTH(digest_sha2_256) = 32),
  CONSTRAINT check_digest_sha3_256 CHECK (LENGTH(digest_sha3_256) = 32),
  CONSTRAINT alive CHECK (
    (_alive AND UPPER(period) IS NULL AND freed_at IS NULL)
      OR (NOT _alive AND UPPER(period) IS NOT NULL)
  ),
  CONSTRAINT freed CHECK (
    freed_at IS NULL OR (freed_at IS NOT NULL AND NOT _alive AND freed_at >= UPPER(period))
  ),
  UNIQUE(buffer_id, period)
--   EXCLUDE USING gist (size WITH =, digest_sha2_256 WITH =, digest_sha3_256 WITH =, period WITH &&),
);
COMMENT ON TABLE buffer IS 'Untyped immutable byte array.';
COMMENT ON COLUMN buffer.period IS 'Validity period for this buffer. A buffer can not be reused once it is freed.';

CREATE INDEX idx_buffer_digest ON buffer(size, digest_sha2_256, digest_sha3_256, _alive);
CREATE INDEX idx_buffer_id ON buffer(buffer_id);

CREATE TABLE blob (
  blob_id BLOB_ID NULL,
  period PERIOD_LOWER NOT NULL,
  buffer_id buffer_id NOT NULL,
  _buffer_period PERIOD_LOWER NOT NULL,
  media_type media_type NOT NULL,
--   _alive BOOLEAN NOT NULL,
  PRIMARY KEY (blob_id),
--   CONSTRAINT alive CHECK ((_alive AND UPPER(period) IS NULL) OR (NOT _alive AND UPPER(period) IS NOT NULL)),
  CONSTRAINT buffer_alive CHECK (_buffer_period @> period),
  CONSTRAINT blob_buffer FOREIGN KEY (buffer_id, _buffer_period) REFERENCES buffer(buffer_id, period) ON DELETE RESTRICT ON UPDATE CASCADE
);
