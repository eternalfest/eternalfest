UPDATE buffer AS bf
SET period = PERIOD(COALESCE((SELECT LOWER(bo.period) FROM blob AS bo WHERE bo.buffer_id = bf.buffer_id LIMIT 1), LOWER(period)), UPPER(period));

DELETE FROM buffer WHERE LOWER(period) = '2010-01-01T00:00:00';

WITH new_values AS (
  SELECT buffer_id, first_value(buffer_id) over w AS new_buffer_id, first_value(period) over w AS new_period
  FROM buffer
  WINDOW w AS (
    PARTITION BY size, digest_sha2_256, digest_sha3_256
    ORDER BY LOWER(period) ASC, buffer_id ASC
    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )
)
UPDATE blob
SET
  buffer_id = (SELECT new_buffer_id FROM new_values WHERE new_values.buffer_id = blob.buffer_id),
  _buffer_period = (SELECT new_period FROM new_values WHERE new_values.buffer_id = blob.buffer_id);


WITH index_buffer AS (
  SELECT buffer_id, row_number() over w AS rn, period
  FROM buffer
  WINDOW w AS (
    PARTITION BY size, digest_sha2_256, digest_sha3_256
    ORDER BY LOWER(period) ASC, buffer_id ASC
    ROWS BETWEEN UNBOUNDED PRECEDING AND UNBOUNDED FOLLOWING
    )
),
new_values AS (
  SELECT
    buffer_id,
    (CASE rn WHEN 1 THEN period ELSE PERIOD('2010-01-01T00:00:00'::TIMESTAMP + (rn * interval '1 sec'), '2010-01-01T00:00:00'::TIMESTAMP + ((rn + 1) * interval '1 sec')) END) AS period,
    (rn = 1) AS _alive
  FROM index_buffer
)
UPDATE buffer
SET
  period = (SELECT period FROM new_values WHERE new_values.buffer_id = buffer.buffer_id),
  _alive = (SELECT _alive FROM new_values WHERE new_values.buffer_id = buffer.buffer_id);

ALTER TABLE buffer
  ADD EXCLUDE USING gist (size WITH =, digest_sha2_256 WITH =, digest_sha3_256 WITH =, period WITH &&);

ALTER TABLE files
  DROP CONSTRAINT file_blob___fk,
  ADD CONSTRAINT file__blob__fk FOREIGN KEY (blob_id) REFERENCES blob (blob_id) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE upload_sessions
  DROP CONSTRAINT upload_session_blob__fk,
  ADD CONSTRAINT upload_session__blob__fk FOREIGN KEY (blob_id) REFERENCES blob (blob_id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE blob_i18n_item
  DROP CONSTRAINT blob_i18n_item__blob__fk,
  ADD CONSTRAINT blob_i18n_item__blob__fk FOREIGN KEY (blob_id) REFERENCES blob (blob_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE game_build
  DROP CONSTRAINT game_build__custom_engine__fk,
  ADD CONSTRAINT game_build__custom_engine__fk FOREIGN KEY (custom_engine) REFERENCES blob (blob_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE game_build
  DROP CONSTRAINT game_build__debug__fk,
  ADD CONSTRAINT game_build__debug__fk FOREIGN KEY (debug) REFERENCES blob (blob_id) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE game_build
  DROP CONSTRAINT game_build__content__fk,
  ADD CONSTRAINT game_build__content__fk FOREIGN KEY (content) REFERENCES blob (blob_id) ON DELETE RESTRICT ON UPDATE RESTRICT;
